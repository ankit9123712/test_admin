<?php

namespace common\models;
use Yii;
use backend\modules\api\v1\models\Users;
use backend\modules\api\v1\models\Notification;
use backend\models\Smslog;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '/var/www/html/admin.englishmonk.com/vendor/twilio/sdk/src/Twilio/autoload.php';
use Twilio\Rest\Client;

/**
 * This is the model class for table "template".
 *
 * @property int $templateID
 * @property string $templateName
 * @property string $templateConstantCode
 * @property string $templateSubject
 * @property string $templateEmailText
 * @property string $templateSMSText
 * @property string $templateNotificationText
 * @property string $templateStatus
 * @property string $templateCreatedDate
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['templateEmailText'], 'required'],
            [['templateEmailText', 'templateStatus'], 'string'],
            [['templateCreatedDate'], 'safe'],
            [['templateName'], 'string', 'max' => 100],
            [['templateConstantCode'], 'string', 'max' => 6],
            [['templateSubject'], 'string', 'max' => 250],
            [['templateSMSText', 'templateNotificationText'], 'string', 'max' => 1000],
            [['templateConstantCode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'templateID' => 'Template I D',
            'templateName' => 'Template Name',
            'templateConstantCode' => 'Template Constant Code',
            'templateSubject' => 'Template Subject',
            'templateEmailText' => 'Template Email Text',
            'templateSMSText' => 'Template S M S Text',
            'templateNotificationText' => 'Template Notification Text',
            'templateStatus' => 'Template Status',
            'templateCreatedDate' => 'Template Created Date',
        ];
    }


    public function sendSMS($country="+91",$mobileNumber,$message)
    {
		$sid = 'AC0a44e5ceb1e9ef037c1882d5af1ede99';
		$token = '9e3b741415af7449b5323d36bc008793';
		$client = new Client($sid, $token);
		if(!empty($mobileNumber))
		{
			try {
			// Use the client to do fun stuff like send text messages!
			$client->messages->create(
				// the number you'd like to send the message to
				"+91".$mobileNumber,
				[
					// A Twilio phone number you purchased at twilio.com/console
					"messagingServiceSid" => "MG78175562bd6a619f22122e1cea6f2347", 
					// the body of the text message you'd like to send
					'body' => $message
				]
			); 
			}catch(\Services_Twilio_RestException $e)
			{
				$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
				//echo $e->getCode() . ' : ' . $e->getMessage()."<br>";
			}
			catch(Twilio\Exceptions\RestException $e)
			{
				$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
				//echo $e->getCode() . ' : ' . $e->getMessage()."<br>";
			}
			catch(Exception $e){
				//echo $e->getCode() . ' : ' . $e->getMessage()."<br>";
				$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
			}
			catch(Twilio\Version\Exception $e){
				//echo $e->getCode() . ' : ' . $e->getMessage()."<br>";
				$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
			}
			//$this->sendFax($mobileNumber);
		}
		
		//$Smslog = new Smslog;
		//$Smslog->AddSMSLog($mobileNumber,$message,$res);
		
    }
	
	
	
	public function push($userDeviceID,$notificationType,$notificationMessageText,$notificationReferenceKey,$userID,$userDeviceType,$ContentID,$notificationTitle,$update=false,$notifcationID=0)
    {
        $arrname = "";
        $ApiKey = "";
		$ApiKey = "AAAAh55gMgI:APA91bF5tltWiQmss7h8rjlCp5leaI6dK832g2QgZxevbzAbycbc74GrZOKnBmuzYmM_iKElZIy_mi-KlvkVTzRw9tmVSWngXRvQgPtr-WLO2hQESHxCLAFL4Uh69WUtd5qkwK5HK7yB";
		
		
        $badgeCount = "1";
        $sound = "default";
       

        if(strtolower($userDeviceType) == 'iphone')
        {   
            $arrname = 'notification';
        }
        if(strtolower($userDeviceType) == 'android')
        {
            $arrname = 'data';
        }
        
        if(!empty($userDeviceID))
        {
            $curl = curl_init();

			 $data = array(
				 'notification' => array(
				 'body'=>$notificationMessageText,
				 'type' => $notificationType,
				 'title' => $notificationTitle,
				 'priority' => 'high',
				 'msg' => $notificationMessageText,
				 'msgType' => "SM",
				 'msgKey' => "1",
				 'badge' => $badgeCount,
				 'sound' => $sound,
				 'RefKey' => $notificationReferenceKey
				 ),
				 'data' => array(
				 'body'=>$notificationMessageText,
				 'type' => $notificationType,
				 'title' => ucwords(strtolower($notificationTitle)),
				 'priority' => 'high',
				 'msg' => $notificationMessageText,
				 'msgType' => "SM",
				 'msgKey' => "1",
				 'badge' => $badgeCount,
				 'sound' => $sound,
				 'RefKey' => $notificationReferenceKey
				 ),
				'to' => "$userDeviceID"
			 );
            

            // print_r(json_encode($data)); die;
                        // print_r($data); die;
              curl_setopt_array($curl, array(
              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_SSL_VERIFYPEER => false,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($data),
              CURLOPT_HTTPHEADER => array(
                 "authorization: key=$ApiKey",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 8a84102f-7aab-6c69-6aca-a34b89ccf20b"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
//        print_R($response);die;        
            curl_close($curl);
            $ResponseDecode = json_decode($response);
            if($ResponseDecode->success == 1)
            {
               $notificationResponse = $response;
                $notificationStatus = 'Yes';
            }
            else
            {
                $notificationResponse = $response;
                $notificationStatus = 'No';
            }            
        }
        else 
        {
            $notificationStatus = 'No';
            $notificationResponse = 'Device Type Empty.';
			
        }
			$Notification = new Notification();
			if(!empty($notifcationID))	
			{
				$Notification = Notification::find()->where("notificationID=".$notifcationID)->one();
			}
            
            $Notification->userID = $userID;
            $Notification->userDeviceType = $userDeviceType;
            $Notification->userDeviceID = $userDeviceID;
            $Notification->notificationType = $notificationType;
            $Notification->notificationTitle = $notificationTitle;
            $Notification->notificationMessageText = $notificationMessageText;
			$Notification->notificationResponse = $notificationResponse;
			$Notification->notificationSendDate = date('Y-m-d');
			$Notification->notificationSendTime = date('H:i:s');
			$Notification->notificationStatus = "Yes";			
            $Notification->notificationReadStatus = "No";
            $Notification->notificationReferenceKey = $notificationReferenceKey;
            
            if($Notification->save(false))
            {
                return true;
            }
    }
	 public function Sendemail($from,$subject,$to,$content,$name="")
	 {
	 /*
	 
		date_default_timezone_set('Etc/UTC');
		
		//Create a new PHPMailer instance
		$mail = new PHPMailer(true);
	 
		//Tell PHPMailer to use SMTP
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Host = 'tls://smtp.gmail.com:587';
		$mail->Port = 587;
		$mail->SMTPSecure = 'tls';

		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);
		$mail->SMTPAuth = true;
		$mail->Username = "skent.qc@gmail.com";
		$mail->Password = "fipl3178";
		//Set who the message is to be sent from
		//$mail->setFrom($from);
	 $mail->SetFrom($from, $name);
		//Set an alternative reply-to address
		$mail->addReplyTo($from);
		//Set who the message is to be sent to
		$mail->addAddress($to);
		//Set the subject line
		$mail->Subject = $subject;
		$mail->isHTML(true);
		//Replace the plain text body with one created manually
		$mail->Body = $content;

	 
	 
		if (!$mail->send()) {
			// echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			// echo "Message sent!";
			
		}
	 //echo "<pre>";
	 //print_r($mail); //die;*/
	 return true;
	 
	}
	
	public function GetTemplate($templateConstantCode)
    {   
		$Res = Template::find()->where('templateConstantCode = "'.$templateConstantCode.'"')->asArray()->one();
		return $Res;
       
    }
	
	public function GetUserDetails_Receiver($userID)
    {
		$Res = Users::find()->select('users.userID AS Receiver_userID,
									users.userFullName AS Receiver_userFullName,
									users.userEmail AS Receiver_userEmail,
									users.userCountryCode AS Receiver_userCountryCode,
									users.userMobile AS Receiver_userMobile,
									users.userProfilePicture AS Receiver_userProfilePicture,
									users.userDeviceType AS Receiver_userDeviceType,
									users.userDeviceID AS Receiver_userDeviceID,
									users.userType AS Receiver_userType')->where('users.userID = "'.$userID.'"')->asArray()->one();
		return $Res;
    }
		public function MakeImageName($tempName)
    {
        $Image_Array = getimagesize($tempName);
        $mime = @explode("/",$Image_Array['mime']);
        $Res = $Image_Array[1]."X".$Image_Array[0]."XADMIN".date("YmdHis").".".$mime[1];
        return $Res;
        
    }
	
	
}
