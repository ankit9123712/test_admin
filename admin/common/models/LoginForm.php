<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $Admin_Email;
    public $Admin_Password;
    public $rememberMe = false;
	public $verifyCode;
	public $captcha;
    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['Admin_Email', 'Admin_Password','captcha'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['Admin_Password', 'validatePassword'],
			['captcha', 'captcha', 'captchaAction' => 'site/captcha']
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->Admin_Password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            /*  if(!empty($this->getUser()))
            {
                $_SESSION['LoginUserID'] = $this->getUser()['Admin_ID'];
                $_SESSION['LoginUserFullName'] = $this->getUser()['Admin_Name'];
                $_SESSION['LoginUserType'] = "Admin";
            }*/

            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->Admin_Email);
        }

        return $this->_user;
    }
}
