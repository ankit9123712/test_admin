<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=englishmonkdb.cq5hklvvycmt.ap-south-1.rds.amazonaws.com;dbname=englishmonk',
            'username' => 'monkadmin',
            'password' => 'Bw4Acaee4CJWFEV1X9Po',  
            'charset' => 'utf8',
			'enableSchemaCache' => true,			
             'attributes' => [
                PDO::NULL_TO_STRING => true,
				PDO::MYSQL_ATTR_USE_BUFFERED_QUERY=> true,
//                PDO::ATTR_EMULATE_PREPARES=> false,
//                PDO::ATTR_STRINGIFY_FETCHES=>false
            ]
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
		'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
    ],
];
