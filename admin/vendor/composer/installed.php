<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'yiisoft/yii2-app-advanced',
  ),
  'versions' => 
  array (
    '2amigos/yii2-tinymce-widget' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a58b7a59a1508f4251a8cea9e010d31c9733bde4',
    ),
    'almasaeed2010/adminlte' => 
    array (
      'pretty_version' => 'v2.x-dev',
      'version' => '2.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '86990d5b48f5b9d747ee14d67df7bb200ffc6f85',
    ),
    'behat/gherkin' => 
    array (
      'pretty_version' => 'v4.8.0',
      'version' => '4.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2391482cd003dfdc36b679b27e9f5326bd656acd',
    ),
    'bower-asset/bootstrap' => 
    array (
      'pretty_version' => 'v3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '68b0d231a13201eb14acd3dc84e51543d16e5f7e',
    ),
    'bower-asset/inputmask' => 
    array (
      'pretty_version' => '3.3.11',
      'version' => '3.3.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e670ad62f50c738388d4dcec78d2888505ad77b',
    ),
    'bower-asset/jquery' => 
    array (
      'pretty_version' => '3.5.1',
      'version' => '3.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4c0e4becb8263bb5b3e6dadc448d8e7305ef8215',
    ),
    'bower-asset/punycode' => 
    array (
      'pretty_version' => 'v1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '38c8d3131a82567bfef18da09f7f4db68c84f8a3',
    ),
    'bower-asset/yii2-pjax' => 
    array (
      'pretty_version' => '2.0.7.1',
      'version' => '2.0.7.1',
      'aliases' => 
      array (
      ),
      'reference' => 'aef7b953107264f00234902a3880eb50dafc48be',
    ),
    'bryglen/yii2-sendgrid' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '2eda790e9b4284be56058042c7e3fc20fae0ec5f',
    ),
    'cebe/markdown' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.2.x-dev',
      ),
      'reference' => '2b2461bed9e15305486319ee552bafca75d1cdaa',
    ),
    'cebe/yii2-gravatar' => 
    array (
      'pretty_version' => '1.1',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c9c01bd14c9bdee9e5ae1ef1aad23f80c182c057',
    ),
    'codeception/base' => 
    array (
      'pretty_version' => '2.5.x-dev',
      'version' => '2.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'aace5bab5593c93d8473b620f70754135a1eb4f0',
    ),
    'codeception/phpunit-wrapper' => 
    array (
      'pretty_version' => '7.8.2',
      'version' => '7.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cafed18048826790c527843f9b85e8cc79b866f1',
    ),
    'codeception/stub' => 
    array (
      'pretty_version' => '2.x-dev',
      'version' => '2.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'bf8c7903fa8b66fe184a076fc589d3afc7b32e82',
    ),
    'codeception/verify' => 
    array (
      'pretty_version' => '0.3.3',
      'version' => '0.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d649dda453cd814dadc4bb053060cd2c6bb4b4c',
    ),
    'dmstr/yii2-adminlte-asset' => 
    array (
      'pretty_version' => '2.7.0-beta1',
      'version' => '2.7.0.0-beta1',
      'aliases' => 
      array (
      ),
      'reference' => '789360570d53c88d446c0193947e269267bfbba8',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.5.x-dev',
      'version' => '1.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6410c4b8352cb64218641457cef64997e6b784fb',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.3.x-dev',
      'version' => '1.3.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '59bfb3b9be04237be4cd1afea9bbb58794c25ce8',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.x-dev',
      'version' => '3.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'c81f18a3efb941d8c4d2e025f6183b5c6d697307',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
    ),
    'fakerphp/faker' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.15.x-dev',
      ),
      'reference' => '048548601222b86745a48a546871b5f4ac0bc081',
    ),
    'fedemotta/yii2-cronjob' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '01f99a808f7ff0ad7faa52711b0b49ed1a0e9992',
    ),
    'filipajdacic/yii2-twilio' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'bbb3c1d5d22e415e51da994b4603e8ce71e54842',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'dc960a912984efb74d0a90222870c72c87f10c91',
    ),
    'imagine/imagine' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2e18be6e930ca169e4f921ef73ebfc061bf55d8',
    ),
    'kartik-v/bootstrap-fileinput' => 
    array (
      'pretty_version' => 'v5.2.0',
      'version' => '5.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '26e3161293c15c97c679a70699f333935132c06b',
    ),
    'kartik-v/yii2-krajee-base' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '9b6bc8335e8e561e3953826efefd57b1df7bdce0',
    ),
    'kartik-v/yii2-mpdf' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '941daa73572370556710e003fae9342dd6429232',
    ),
    'kartik-v/yii2-widget-datepicker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.4.x-dev',
      ),
      'reference' => '98257627814abf52aa8c1e6dc020d8eb6c1c39b5',
    ),
    'kartik-v/yii2-widget-datetimepicker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.4.x-dev',
      ),
      'reference' => 'f64114bff520ea67f48e2a4e6372915968eb2c69',
    ),
    'kartik-v/yii2-widget-fileinput' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.1.x-dev',
      ),
      'reference' => 'e15d65047af23a3cf25e47e8c23444e750a27f2c',
    ),
    'kartik-v/yii2-widget-select2' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.2.x-dev',
      ),
      'reference' => '05ed365ea5ed3555646db27dfb9beea79757a054',
    ),
    'kartik-v/yii2-widget-timepicker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '27847bf0a3178d8ce526fe8a0f97832956ce8e45',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '2.0.x-dev',
      'version' => '2.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '9999f1432fae467bc93c53f357105b4c31bb994c',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '361c0f545c3172ee26c3d596a0aa03f0cef65e6a',
    ),
    'moonlandsoft/yii2-phpexcel' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'ccf28ff8ce2c665a7769dd6516098b4a35c8c309',
    ),
    'mpdf/mpdf' => 
    array (
      'pretty_version' => 'v8.0.10',
      'version' => '8.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '1333a962cd2f7ae1a127b7534b7734b58179186f',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.x-dev',
      ),
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.7.7',
      'version' => '1.7.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd178027d1e679832db9f38248fcc7200647dc2b7',
    ),
    'nterms/yii2-pagesize-widget' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'ccb2147379deeb79533e73d0ecd4a275b4ddf5b4',
    ),
    'opis/closure' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.6.x-dev',
      ),
      'reference' => '06e2ebd25f2869e54a306dda991f7db58066f7f6',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '45a2ec53a73c70ce41d55cedef9063630abaf1b6',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => 'cf8df60735d98fd18070b7cab0019ba0831e219c',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.x-dev',
      ),
      'reference' => '8719cc12e2a57ec3432a76989bb4ef773ac75b63',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6759f2268deb9f329812679e9dcb2d0083b2a30b',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '9256f12d8fb0cd0500f93b19e18c356906cbed3d',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c55269cb06911575a126dc225a05c0e4626e5fb4',
    ),
    'phpspec/php-diff' => 
    array (
      'pretty_version' => 'v1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc1156187f9f6c8395886fe85ed88a0a245d72e9',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be1996ed8adc35c3fd795488a653f4b518be70ea',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '6.1.4',
      'version' => '6.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '807e6013b00af69b6c5d9ceb4282d0393dbb9d8d',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.x-dev',
      'version' => '2.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '4b49fb70f067272b659ef0174ff9ca40fdaa6357',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.x-dev',
      'version' => '2.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2454ae1765516d20c4ffe103d85a58a9a3bd5662',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '3.1.x-dev',
      'version' => '3.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '472b687829041c24b25f475e14c2f38a09edf1c2',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '7.5.20',
      'version' => '7.5.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '9467db479d1b0487c99733bb1e7944d32deded2c',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.x-dev',
      'version' => '1.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '22b2ef5687f43679481615605d7a15c557ce85b1',
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '36fa03d50ff82abcae81860bdaf4ed9a1510c7cd',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'efd67d1dc14a7ef4fc4e518e7dee91c271d524e4',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.1.x-dev',
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '5a7b96b1dda5d957e01bc1bfe77dcca09c5a7474',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'rmrevin/yii2-fontawesome' => 
    array (
      'pretty_version' => '3.x-dev',
      'version' => '3.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'a1b448cef7e03a0b5a6286276bef004efa83f323',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.x-dev',
      'version' => '1.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1de8cd5c010cb153fcd68b8d0f64606f523f7619',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1071dfcef776a57013124ff35e1fc41ccd294758',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '14f72dd46eaf2f2293cbe79c93cc0bc43161a211',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.x-dev',
      'version' => '4.2.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'd47bbbad83711771f167c72d4e3f25f7fcc1f8b0',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.x-dev',
      'version' => '3.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6b853149eab67d4da22291d36f5b0631c0fd856e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'e67f6d32ebd0c749cf9d1dbd9f226c727043cdf2',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.x-dev',
      'version' => '1.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '9b8772b9cbd456ab45d4a598d2dd1a1bced6363d',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '367dcba38d6e1977be014dc4b22f47a484dac7fb',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.x-dev',
      'version' => '2.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '31d35ca87926450c44eae7e2611d45a7a65ea8b3',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'select2/select2' => 
    array (
      'pretty_version' => 'dev-develop',
      'version' => 'dev-develop',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '0a30b0b3e67843c09b0bcc4d01e65b72d9b1279f',
    ),
    'sendgrid/sendgrid' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '83cec4aa7a86064c8ea30cc7180b1b558b0ac7e9',
    ),
    'sendgrid/sendgrid-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'sendgrid/smtpapi' => 
    array (
      'pretty_version' => '0.6.7',
      'version' => '0.6.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cf63bd353c25f775a1fb2654a0a3fff22f573a37',
    ),
    'sendgrid/smtpapi-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'setasign/fpdi' => 
    array (
      'pretty_version' => 'v2.3.6',
      'version' => '2.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '6231e315f73e4f62d72b73f3d6d78ff0eed93c31',
    ),
    'stripe/stripe-php' => 
    array (
      'pretty_version' => 'v7.83.0',
      'version' => '7.83.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb7244c7334ad8bf30d31bb4972d5aff57df1563',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '6.2.x-dev',
      ),
      'reference' => '23e9ffaf6e8c716bce31bde1be708a405b1fbdf9',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => '4.4.x-dev',
      'version' => '4.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '4c8b42b4aae93517e8f67d68c5cbe69413e3e3c1',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => '4.4.x-dev',
      'version' => '4.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '36bbd079b69b94bcc9c9c9e1e37ca3b1e7971625',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => '3.4.x-dev',
      'version' => '3.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'da3d9da2ce0026771f5fe64cb332158f1bd2bc33',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.4.x-dev',
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => '3.4.x-dev',
      'version' => '3.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'ef97bcfbae5b384b4ca6c8d57b617722f15241a6',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => '4.4.x-dev',
      'version' => '4.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'c352647244bd376bf7d31efbd5401f13f50dad0c',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '84e23fdcd2517bf37aecbd16967e83f0caee25a7',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => '4.4.x-dev',
      'version' => '4.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '67b77716f517e3f864759232e1201e7aa2ab0e82',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.23.x-dev',
      ),
      'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.23.x-dev',
      ),
      'reference' => 'adbd54f5a933d3cf1e5ebaff2c1599b7815b98a7',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.23.x-dev',
      ),
      'reference' => '780e28060e530479bf1e4a303ccf185e537f8d66',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.23.x-dev',
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.23.x-dev',
      ),
      'reference' => '9ad2f3c9de0273812c616fdf96070a129c3defcb',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.23.x-dev',
      ),
      'reference' => '95695b83b8ecb15450a6cabfc2e352beb17cce6b',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.23.x-dev',
      ),
      'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.23.x-dev',
      ),
      'reference' => 'eca0bf41ed421bed1b57c4958bab16aa86b757d0',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.4.x-dev',
      ),
      'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => '4.4.x-dev',
      'version' => '4.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1c2fd24147961525eaefb65b11987cab75adab59',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75a63c33a8577608444246075ea0af0d052e452a',
    ),
    'tinymce/tinymce' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'd2358148183b3887b9d7285b6207a8345da2adc6',
    ),
    'tpmanc/yii2-imagick' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'be5f3c1dc1b1f18acde1f4672bf6d4dc77f2fd1a',
    ),
    'twilio/sdk' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '3438bcdb42a8fabcfaa081670d5924b868016148',
    ),
    'wbraganca/yii2-dynamicform' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'bebb469055dc8f8f50b586eefe69d847ead7c11b',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.10.x-dev',
      ),
      'reference' => 'f07851c5b43e4cb502c24068620e9af6cbdd953f',
    ),
    'yii2tech/crontab' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '5150b2f165bbfa6d6ab9d36cbdc693460d7e9e2d',
    ),
    'yii2tech/spreadsheet' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'aff93d4eca719e8cba80663dbfcd08dc0e740319',
    ),
    'yiisoft/yii2' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '4953f3cc44ece651a8efdbfe84525d6df3a5aa7d',
    ),
    'yiisoft/yii2-app-advanced' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'yiisoft/yii2-bootstrap' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => 'd90d116f0c1790e33032dae636e445e6c370ec03',
    ),
    'yiisoft/yii2-composer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => 'b994e9678e34c2d17008c42a29fedb29d9ebf80b',
    ),
    'yiisoft/yii2-debug' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '8c6b8b100ec9a510b252a1cb293dbeab216c555e',
    ),
    'yiisoft/yii2-faker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '360beb2d702c82ea02a5334c11b12d9a60563269',
    ),
    'yiisoft/yii2-gii' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '6b40893f87c1dd970896ced6c198ff2416df353e',
    ),
    'yiisoft/yii2-imagine' => 
    array (
      'pretty_version' => '2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b103b1b1deb786d4d5fe955898ec866dbee5c1b4',
    ),
    'yiisoft/yii2-redis' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => 'ead6d1f1a99c171449e42fe0e00bfc3fcf3a27a6',
    ),
    'yiisoft/yii2-swiftmailer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.1.x-dev',
      ),
      'reference' => 'd7d02545995e75574eae13184ded2984edba2728',
    ),
  ),
);
