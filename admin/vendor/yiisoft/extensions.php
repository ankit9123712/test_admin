<?php

$vendorDir = dirname(__DIR__);

return array (
  '2amigos/yii2-tinymce-widget' => 
  array (
    'name' => '2amigos/yii2-tinymce-widget',
    'version' => '1.1.3.0',
    'alias' => 
    array (
      '@dosamigos/tinymce' => $vendorDir . '/2amigos/yii2-tinymce-widget/src',
    ),
  ),
  'bryglen/yii2-sendgrid' => 
  array (
    'name' => 'bryglen/yii2-sendgrid',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@bryglen/sendgrid' => $vendorDir . '/bryglen/yii2-sendgrid',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'cebe/yii2-gravatar' => 
  array (
    'name' => 'cebe/yii2-gravatar',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@cebe/gravatar' => $vendorDir . '/cebe/yii2-gravatar/cebe/gravatar',
    ),
  ),
  'dmstr/yii2-adminlte-asset' => 
  array (
    'name' => 'dmstr/yii2-adminlte-asset',
    'version' => '2.7.0.0-beta1',
    'alias' => 
    array (
      '@dmstr' => $vendorDir . '/dmstr/yii2-adminlte-asset',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '3.9999999.9999999.9999999-dev',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'fedemotta/yii2-cronjob' => 
  array (
    'name' => 'fedemotta/yii2-cronjob',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@fedemotta/cronjob' => $vendorDir . '/fedemotta/yii2-cronjob',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker/src',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker/src',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput/src',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker/src',
    ),
  ),
  'moonlandsoft/yii2-phpexcel' => 
  array (
    'name' => 'moonlandsoft/yii2-phpexcel',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@moonland/phpexcel' => $vendorDir . '/moonlandsoft/yii2-phpexcel',
    ),
  ),
  'nterms/yii2-pagesize-widget' => 
  array (
    'name' => 'nterms/yii2-pagesize-widget',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@nterms/pagesize' => $vendorDir . '/nterms/yii2-pagesize-widget',
    ),
  ),
  'wbraganca/yii2-dynamicform' => 
  array (
    'name' => 'wbraganca/yii2-dynamicform',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@wbraganca/dynamicform' => $vendorDir . '/wbraganca/yii2-dynamicform/src',
    ),
  ),
  'yii2tech/crontab' => 
  array (
    'name' => 'yii2tech/crontab',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii2tech/crontab' => $vendorDir . '/yii2tech/crontab/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.3.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-redis' => 
  array (
    'name' => 'yiisoft/yii2-redis',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/redis' => $vendorDir . '/yiisoft/yii2-redis/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yii2tech/spreadsheet' => 
  array (
    'name' => 'yii2tech/spreadsheet',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii2tech/spreadsheet' => $vendorDir . '/yii2tech/spreadsheet/src',
    ),
  ),
);
