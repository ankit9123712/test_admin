<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Language;
class LanguageController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
	public function actionIndex()
    {
		return $this->render('index');
		\Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
		return $result;
    }
	
	
	public function actionGetLanguageList()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
			   extract($jsonParams[0]);    
			   if(!empty($apiType))
               {
				    if(!empty($apiVersion))
					{
						$Language = new Language;
						$Res = $Language->GetLanguageList(); 
						if(!empty($Res))
						{
							$result[0]['data'] = $Res;
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");			
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");			
						}
				   }
				   else
				   {
						$Required = 'apiVersion';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				   }
			   }
			   else
			   {
				    $Required = 'apiType';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);   
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}
	public function actionListLabels()
	{
		
		$ApiLog = new ApiLog;         
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;			
		$postParams = \Yii::$app->request->post();	
		$jsonParams = \yii\helpers\Json::decode($postParams["json"]);
		extract($jsonParams[0]);		
				$status = "Active";
				$query = new \yii\db\Query();
				$res= $query->select("*")
					->from('languagelabel')                        
						->WHERE("langLabelStatus = '".$status."' AND langLabelModule='".$langLabelModule."' AND languageID=".$languageID)							
						->all();
				if(count($res)>0)
				{	

					$valArray = [];
					$finalArray = [];
					for($i=0;$i<count($res);$i++)
					{
						$key = $res[$i]['langLabelKey'];
						$label_msg = $res[$i]['langLabelValue'];

						array_push($valArray, array($key=> $label_msg));

						$finalArray[0][$res[$i]['langLabelKey']] = $valArray[$i][$key];
					}
			
					$result[0]['status'] = true;
					$result[0]['info'] = "RECORDFOUND";
					$result[0]['data'] = $finalArray;
				}
				else
				{
					$result[0]['status'] = false;
					$result[0]['info'] = "NORECORD";							
				}       				
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result;				
	}
	public function actionListMsgs()
	{
		$ApiLog = new ApiLog;         
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;			
		$postParams = \Yii::$app->request->post();	
		$jsonParams = \yii\helpers\Json::decode($postParams["json"]);
		extract($jsonParams[0]);						
				$status = "Active";
				$query = new \yii\db\Query();
				$res= $query->select("*")
					->from('languagemsgs')                        
						->WHERE("langMsgStatus = '".$status."' AND languageID=".$languageID)							
						->all();
				if(count($res)>0)
				{				
					$valArray = [];
					$finalArray = [];
					for($i=0;$i<count($res);$i++)
					{
						$key = $res[$i]['langMsgKey'];
						$label_msg = $res[$i]['langMsgValue'];

						array_push($valArray, array($key=> $label_msg));

						$finalArray[0][$res[$i]['langMsgKey']] = $valArray[$i][$key];
					}			
					$result[0]['status'] = true;
					$result[0]['info'] = "RECORDFOUND";
					$result[0]['data'] = $finalArray;
				}
				else
				{
					$result[0]['status'] = false;
					$result[0]['info'] = "NORECORD";							
				}       				
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result;	
	}

}
