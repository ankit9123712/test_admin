<?php
namespace backend\modules\api\v1\controllers;use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\City;

class CityController extends \yii\web\Controller
{
   
    public $enableCsrfValidation = false;
	public function actionIndex()
    {
		return $this->render('index');
		\Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		return $result;
    }	public function actionGetCityList()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
			   $page = "0";
               $pagesize = "10";	
			   $searchWord="";
			   $page="0";
			   $pagesize="10";			
			   extract($jsonParams[0]);    
			   if(!empty($apiType))
               {
				    if(!empty($apiVersion))
					{
						$City = new City;
						$Res = $City->GetCityList($countryID,$stateID,$pagesize,$page,$searchWord); 
						if(!empty($Res))
						{
							$result[0]['data'] = $Res;
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");			
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");			
						}
				   }
				   else
				   {
						$Required = 'apiVersion';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				   }
			   }
			   else
			   {
				    $Required = 'apiType';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
		return $result; 
		
	}



}
