<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Forum;
use backend\modules\api\v1\models\Forummessages;
use backend\modules\api\v1\models\Monkcategory;
use backend\modules\api\v1\models\Monkchat;
use backend\modules\api\v1\models\Monkchatmessages;
use common\models\Template;
use Yii;
use backend\modules\api\v1\models\Notification;
use backend\modules\api\v1\models\Settings;
use backend\modules\api\v1\models\Faculty;
use backend\modules\api\v1\models\Users;
class ForumController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
        header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
        return $result;
    }
	public function actionAddEditForum()
	{
		$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$forumID="0";
			$forumFiles="";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($forumName)  && !empty($forumSubject) && !empty($AssiDescription) && !empty($forumPic)  && !empty($moduleID) )
	        { 
				   
				   $Forum = new Forum;
				   if(!empty($forumID))
				   {
					   $Forum = Forum::find()->where('forumID='.$forumID)->one();
				   }
				   $Forum->facultyID = $loginfacultyID;
				   $Forum->moduleID = $moduleID;
				   $Forum->forumPic = $forumPic;
				   $Forum->forumFiles = $forumFiles;
				   $Forum->AssiDescription = $AssiDescription;
				   $Forum->forumSubject = $forumSubject;
				   $Forum->forumName = $forumName;
				   $Forum->save(false);
				   $forumID=$Forum->forumID;
				   
				   $res = $Forum->GetForumList($moduleID,$loginfacultyID,"",0,10,$forumID);  
				   $cnt = $Forum->GetForumCount($moduleID,$loginfacultyID,"",$forumID);  
				   
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['count'] = $cnt;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
        return $result; 
	}
	public function actionForumStatusUpdate()
	{
		$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			
			
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($forumID) && !empty($forumStatus)  )
	        { 
				   
				   $Forum = new Forum;
				   if(!empty($forumID))
				   {
					   $Forum = Forum::find()->where('forumID='.$forumID)->one();
				   }
				   $Forum->forumStatus = $forumStatus;
				   
				   $Forum->save(false);
				   $forumID=$Forum->forumID;
				   
				   $res = $Forum->GetForumList(@$moduleID,$loginfacultyID,"",0,10,$forumID);  
				   
				   
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);   
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
        return $result; 
	}
	public function actionGetForums()
	{
		$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
			$usertype="";
			$moduleID="";
			$facultyID="";
			$searchWord="";
			$forumID="0";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID) && !empty($usertype))
	        { 
				   $Forum = new Forum;
				   $res = $Forum->GetForumList($moduleID,$facultyID,$usertype,$page,$pagesize,$forumID,$searchWord);  
				    $cnt = $Forum->GetForumCount($moduleID,$facultyID,"",$forumID,$searchWord); 
				   
				   
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['count'] = $cnt;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
        return $result; 
	}
	public function actionForumAddMessage()
	{
		$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($forumID) && !empty($forummessMessage) )
	        { 
				   $formRes = Forum::find()->where("forumID=".$forumID)->one();
				   
				   $Forummessages = new Forummessages;
				   $Forummessages->userID= $loginuserID;
				   $Forummessages->forumID= $forumID;
				   $Forummessages->facultyID= $facultyID;
				   $Forummessages->forummessMessage= $forummessMessage;
				   $Forummessages->forummessAttachment= $forummessAttachment;
				   $Forummessages->save(false);
				   $res = $Forummessages->GetForumMessageList($forumID,$page,$pagesize);  
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
						
						$this->SendNotification($formRes->facultyID,false,"000012","","forum-message",$forumID,"");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
        return $result; 
		 
	}
	public function actionGetForumMessage()
	{
		$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($forumID) )
	        { 
				   $Forummessages = new Forummessages;
				  
				   $res = $Forummessages->GetForumMessageList($forumID,$page,$pagesize);  
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);    
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
        return $result; 
	}
	
	//monk cateory
	public function actionGetMonkcategoryList()
    {
    	$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
			$searchWord="";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID) )
	        { 
				   $Monkcategory = new Monkcategory;
				   $res = $Monkcategory->GetMonkcategoryList($searchWord,$page,$pagesize);  
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);  
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
        return $result; 
			
    }
	//monk chat create
	//monk chat edit
	public function actionCreateMonkChat()
	{
		$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
			$monkchatPic="";
			$monkchatFiles="";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID) && !empty($monkcatID) && !empty($monkchatName) && !empty($monkchatSubject) && !empty($monkchatDescription) )
	        { 
				   $Monkchat = new Monkchat;
				   $Monkchat->userID= $loginuserID;
				   $Monkchat->monkcatID= $monkcatID;
				   $Monkchat->monkchatName= $monkchatName;
				   $Monkchat->monkchatPic= $monkchatPic;
				   $Monkchat->monkchatSubject= $monkchatSubject;
				   $Monkchat->monkchatDescription= $monkchatDescription;
				   $Monkchat->monkchatFiles= $monkchatFiles;
				   $Monkchat->save(false);
				   $monkchatID = $Monkchat->monkchatID;
				   $res = $Monkchat->GetMonkchatList($monkcatID,$monkchatID,$page,$pagesize);  
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
        return $result; 
		 
	}
	
	//monk chat list
	public function actionGetMonkchat()
	{
		$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
			$monkchatID="";
			$monkcatID="";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID) )
	        { 
				   $Monkchat = new Monkchat;
				   $res = $Monkchat->GetMonkchatList($monkcatID,$monkchatID,$page,$pagesize);  
				   
				   
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);   
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
        return $result; 
	}
	//monk chat add message
	public function actionMonkchatAddMessage()
	{
		$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($monkchatID) && !empty($monkchatmessMessage) )
	        { 
				   $Monkchatmessages = new Monkchatmessages;
				   $Monkchatmessages->userID= $loginuserID;
				   $Monkchatmessages->monkchatID= $monkchatID;
				   $Monkchatmessages->monkchatmessMessage= $monkchatmessMessage;
				   $Monkchatmessages->monkchatmessAttachment= $monkchatmessAttachment;
				   $Monkchatmessages->save(false);
				   $res = $Monkchatmessages->GetMonkchatMessageList($monkchatID,$page,$pagesize);  
				   $formRes = Monkchat::find()->where("monkchatID=".$monkchatID)->one();
				   $this->SendNotification($formRes->userID,false,"000013","","monkchat-message","",$monkchatID);
				   //die;
				   if(!empty($res))
				   {
						
						
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
						
						
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
        return $result; 
		 
	}
	
	//monk chat messages
	public function actionGetMonkchatMessage()
	{
		$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($monkchatID) )
	        { 
				   $Monkchatmessages = new Monkchatmessages;
				  
				   $res = $Monkchatmessages->GetMonkchatMessageList($monkchatID,$page,$pagesize);  
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);  
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
        return $result; 
	}
	
	private function SendNotification($userID,$isAdmin,$templateConstant,$templateConstantAdmin,$notiType="",$forumID="",$monkchatID="",$replyUserID="")
	{
		//return true;
		$template = new Template();																
		$settings = new Settings();
		$settings_arr = Settings::find()->select('settingsEmailFrom,settingsEmailTo,settingsEmailGateway,settingEmailID,settingsEmailPass,settingsSSL,settingsTLS,settingEmailPort,settingSenderName')->where(['settingsID' => '1'])->asArray()->all();	
		if(!empty($monkchatID))
			$User_Res = Users::find()->where('userID = "'.$userID.'"')->one();
		else
			$User_Res = Faculty::find()->where('facultyID = "'.$userID.'"')->one();
		
		$reply_Res = Users::find()->where('userID = "'.$replyUserID.'"')->one();
		$Forum_Res = Forum::find()->where('forumID = "'.$forumID.'"')->one();
		$Monkchat_Res = Monkchat::find()->where('monkchatID = "'.$monkchatID.'"')->one();		
		/*OTP*/
		$templateArr = $template->GetTemplate($templateConstant);
		//print_r($templateArr);die;
		$subject= $templateArr["templateSubject"];
		$mailStr= $templateArr["templateEmailText"];
		$smsStr= $templateArr["templateSMSText"];
		$pushStr= $templateArr["templateNotificationText"];
		$fieldArray	= array();
		//print_r($User_Res);die;
		$ContentID=$userID;
		if(!empty($Forum_Res))
		{
			$fieldArray["forumName"]=$Forum_Res->forumName;
			$fieldArray["forumSubject"]=$Forum_Res->forumSubject;
			$ContentID = $forumID;
			$userEmail = $User_Res->facultyEmail;						
			$userMobile = $User_Res->facultyMobile;
			$userCountryCode = "+91";
			$appType="Faculty";
			$userDeviceID=$User_Res->facultyDeviceID;
			$userDeviceType = $User_Res->facultyDeviceType;
			$fieldArray["userFullName"]=$User_Res->facultyFullName;
			$fieldArray["username"]=$User_Res->facultyFullName;
			
		}
		if(!empty($Monkchat_Res))
		{
			$fieldArray["monkchatName"]=$Monkchat_Res->monkchatName;
			$ContentID = $monkchatID;
			$userEmail = $User_Res->userEmail;						
			$userMobile = $User_Res->userMobile;
			$userCountryCode = $User_Res->userCountryCode;
			$appType="Customer";
			$fieldArray["userOTP"]=$User_Res->userOTP;
			$fieldArray["userFullName"]=$User_Res->userFullName;
			$fieldArray["username"]=$User_Res->userFullName;
			$userDeviceID=$User_Res->userDeviceID;
			$userDeviceType = $User_Res->userDeviceType;
		}
			
		
		
		
		/*Replace Key with Array Value*/
		foreach($fieldArray as $key => $value)	
		{			
			$mailStr=str_replace("###".$key."###",$value,$mailStr);
			$subject = str_replace("###".$key."###",$value,$subject);
			$smsStr = str_replace("###".$key."###",$value,$smsStr);
			$pushStr = str_replace("###".$key."###",$value,$pushStr);
		}			
		if(!empty($mailStr)) 
			$template->Sendemail($settings_arr[0]["settingsEmailFrom"],$subject,$userEmail,$mailStr,$settings_arr[0]["settingSenderName"]);
		if(!empty($smsStr)) 
			$template->sendSMS($userCountryCode, $userMobile,$smsStr);
		if(!empty($pushStr))
			$template->push($userDeviceID,$notiType,$pushStr,$ContentID,$userID,$userDeviceType,$ContentID,$subject,false,"",0,$appType);
		
		
		if($isAdmin)
		{
			$templateArr = $template->GetTemplate($templateConstantAdmin);
			$subject= $templateArr["templateSubject"];
			$mailStr= $templateArr["templateEmailText"];
			$smsStr= $templateArr["templateSMSText"];
			$pushStr= $templateArr["templateNotificationText"];
			
			/*Replace Key with Array Value*/
			foreach($fieldArray as $key => $value)	
			{			
				$mailStr=str_replace("###".$key."###",$value,$mailStr);
				$subject = str_replace("###".$key."###",$value,$subject);
			}			
			if(!empty($mailStr)) 
				$template->Sendemail($settings_arr[0]["settingsEmailFrom"],$subject,$userEmail,$mailStr,$settings_arr[0]["settingSenderName"]);
			if(!empty($smsStr)) 
				$template->sendSMS("91", $userMobile,$smsStr);
			if(!empty($pushStr)) 
				$template->push($User_Res->userDeviceID,"Admin",$pushStr,$userID,$userID,$User_Res->userDeviceType,$ContentID,$subject,false,"",0,$appType);		
		
		}
		//echo $mailStr."<Br>".$smsStr."<BR>".$pushStr."<BR>";	
		//die;
		
	}
	
}
