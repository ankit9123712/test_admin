<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Livesessions;
use backend\modules\api\v1\models\Users;
use backend\modules\api\v1\models\Livesessiondetails;
use Yii;
class LivesessionsController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
        header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
        return $result;
    }
	public function actionGetLiveSessions()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				$future="false";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) )
				{
					$Livesessions = new Livesessions;
					
					$resSessions = $Livesessions->GetSessionsList($loginuserID,$page,$pagesize,$future,"",$shift);
					if(!empty($resSessions))
					{
						$result[0]['sessions'] = $resSessions;
						//$result[0]['visiteddata'] = $resGetCoursechaptersList;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	
	public function actionUpdateLiveSessions()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page=0;
				$pagesize=10;
				$future="false";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($liveID) && !empty($livedetails) )
				{
					$details = new Livesessiondetails;
					$Livesessions = new Livesessions;
					Yii::$app->db->createCommand("DELETE FROM livesessiondetails WHERE liveID = '".$liveID."'")->execute();
					if(!empty($livedetails))
					{
						for($i=0;$i<count($livedetails);$i++) 
						{
							$detailsRes = new Livesessiondetails;
							$detailsRes->liveID	= $liveID;
							$detailsRes->sessiondetailType	= $livedetails[$i]["sessiondetailType"];
							$detailsRes->lessonID	= $livedetails[$i]["lessonID"];
							$detailsRes->moduleID	= $livedetails[$i]["moduleID"];
							$detailsRes->sessiondetailURL	= $livedetails[$i]["sessiondetailURL"];
							$detailsRes->save(false);
						}
					}
					$mastRes = Livesessions::find()->where("liveID	=".$liveID	)->one();
					if(!empty($mastRes))
					{
						$mastRes->liveHomeworkDate = date('Y-m-d')	;
						$mastRes->save(false);
					}
					
					
					$resSessions = $Livesessions->GetSessionsList(0,$page,$pagesize,$future,$loginfacultyID,"",$liveID);
					if(!empty($resSessions))
					{
						$result[0]['sessions'] = $resSessions;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);     
header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	
	
	public function actionGetFacultyLiveSessions()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				$future="false";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) )
				{
					$Livesessions = new Livesessions;
					
					$resSessions = $Livesessions->GetSessionsList(0,$page,$pagesize,$future,$loginfacultyID);
					if(!empty($resSessions))
					{
						$result[0]['sessions'] = $resSessions;
						//$result[0]['visiteddata'] = $resGetCoursechaptersList;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
		return $result; 
		
	}
}
