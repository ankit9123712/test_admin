<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Feedback;
class FeedbackController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
        header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
        return $result;
    }
	//feedbacklist
	public function actionGetFeedbackList()
    {
    	$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID))
	        { 
				   $Feedback = new Feedback;
				   $res = $Feedback->GetFeedbackList($loginuserID,$page,$pagesize);  
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
        return $result; 
			
    }
	// add feedback
	public function actionAddFeedback()
    {
    	$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID) && !empty($feedbackName) && !empty($feedbackEmail) && !empty($feedbackFeedback))
	        { 
				   $Feedback = new Feedback;
				   $Feedback->userID= $loginuserID;
				   $Feedback->feedbackName= $feedbackName;
				   $Feedback->feedbackEmail= $feedbackEmail;
				   $Feedback->feedbackFeedback= $feedbackFeedback;
				   $Feedback->save(false);
				   $res = $Feedback->GetFeedbackList($loginuserID,$page,"1");  
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
        return $result; 
			
    }
}
