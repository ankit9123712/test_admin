<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Usersubscriptions;
use backend\modules\api\v1\models\Users;
use backend\modules\api\v1\models\Offer;
use Yii;
class UsersubscriptionsController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		return $result;
	}
	

	public function actionGetMySubscriptions()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID))
				{
					$Usersubscriptions = new Usersubscriptions;
					//List of course
					$resSubscription = $Usersubscriptions->GetUserSubscriptionplanList($loginuserID,$page,$pagesize);
					if(!empty($resSubscription))
					{
						$result[0]['data'] = $resSubscription;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionAddMySubscriptions()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";
				$usersubscriptionBatch="Morning";	
				$usersubscriptionShift="Mon-Wed-Fri";
				$usersubscriptionDiscount=0;
				$usersubscriptionCoponCode	="";			
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($planID) && !empty($usersubscriptionStartDate) && !empty($usersubscriptionEndDate) )
				{
					$Usersubscriptions = new Usersubscriptions;
					if(!empty($usersubscriptionAmount))
						$tRes = $Usersubscriptions->StripeBlockAmount($loginuserID,$cardtoken,$usersubscriptionAmount);
					else
					{
						$tRes = array();
						$tRes[0]="true";
						$tRes[1]="freetoken";
						$tRes[2]="100% discount";
					}
						
					//print_r($tRes);die;
					if($tRes[0]=="true")
					{
						$Usersubscriptions->subscriptionID	 = $planID;
						$Usersubscriptions->usersubscriptionBatch	 = $usersubscriptionBatch;
						$Usersubscriptions->usersubscriptionShift	 = $usersubscriptionShift;
						$Usersubscriptions->userID = $loginuserID;
						$Usersubscriptions->usersubscriptionStartDate = $usersubscriptionStartDate;
						$Usersubscriptions->usersubscriptionCoponCode =$usersubscriptionCoponCode;
						$Usersubscriptions->usersubscriptionDiscount =$usersubscriptionDiscount;
						$Usersubscriptions->usersubscriptionEndDate = $usersubscriptionEndDate;
						$Usersubscriptions->usersubscriptionAmount = $usersubscriptionAmount;
						$Usersubscriptions->usersubscriptionChargeToken = $tRes[1];
						$Usersubscriptions->usersubscriptionCustomer = $tRes[2];
						
						//$Usersubscriptions->subscriptionStatus = "Active";
						$Usersubscriptions->save(false);
						
						
						$users = new Users;
						$userRes =  Users::find()->where('userID='.$loginuserID)->one();
						if(!empty($userRes))
						{
							$userRes->userType = "Premium";
							$userRes->save(false);
						}
						
						
						
						
						//List of course
						$resSubscription = $Usersubscriptions->GetUserSubscriptionplanList($loginuserID,$page,$pagesize);
						$result[0]['data'] = $resSubscription;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","SUBSCRIPTION_ADDED");	
					}
					else
					{
						
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");	
					}						

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionGetOffers()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				 		
				$searchkeyword="";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion))
				{
					
					
					$offers = new Offer;
					$offerRes =  $offers->GetOffer($searchkeyword) ;
					 
					
					$result[0]['data'] = $offerRes;
					$result[0]['status'] = 'true';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","SUBSCRIPTION_ADDED");									

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	public function actionValidateOffers()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				 		
				$amount="";
				$offerValue = "";
				extract($jsonParams[0]);    
				if(!empty($offerValue))
				{
					$offerIDRes = Offer::find()->where("offerName='".$offerValue."'")->one();
					if(!empty($offerIDRes))
						$offerID = $offerIDRes->offerID;
				}
				
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($offerID) && !empty($amount))
				{
					
					
					$offers = new Offer;
					$result =  $offers->VerifyOffer($offerID,$amount) ;
					 
					
					/*$result[0]['data'] = $offerRes;
					$result[0]['status'] = 'true';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","SUBSCRIPTION_ADDED");	*/								

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = "Invalid coupon code.";//$Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	/*-------------- User Home Sreen Data Get Code End -----------------*/
}
