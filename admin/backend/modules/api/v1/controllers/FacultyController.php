<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Faculty;
use backend\modules\api\v1\models\Settings;
use backend\modules\api\v1\models\City;
use backend\modules\api\v1\models\Notification;
use backend\modules\api\v1\models\Courses;
use backend\modules\api\v1\models\Coursesubjects;
use backend\modules\api\v1\models\Coursemodule;
use backend\modules\api\v1\models\Exams;
use backend\modules\api\v1\models\Lesson;
use backend\modules\api\v1\models\Lessonfiles;
use backend\modules\api\v1\models\Userexams;
use backend\modules\api\v1\models\Userlessions;
use backend\modules\api\v1\models\Useranswers;
use backend\modules\api\v1\models\Users;
use common\models\Template;
use Yii;
class FacultyController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
		return $result;
	}
	public function actionOtpVerification()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$stateID = "0";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($facultyOTP) && !empty($facultyDeviceID) )
				{			

					$Faculty = new Faculty;
					$Faculty_Res = Faculty::find()->select('facultyID,facultyVerified')->where('facultyID = "'.$loginfacultyID.'" AND facultyOTP = "'.$facultyOTP.'"')->one();
					if(!empty($Faculty_Res))
					{           		
						
						


						$Faculty_Res->facultyID = $loginfacultyID;
						$Faculty_Res->facultyDeviceID = $facultyDeviceID;
						if($Faculty_Res->facultyVerified = "No")
						{
							$Faculty_Res->facultyVerified = "Yes";
						}
						$Faculty_Res->facultyOTP = "";
						$Faculty_Res->save(false);
						$result[0]['data'] = $Faculty->GetFacultyDetails($loginfacultyID);
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPVERIFICATION_SUCCESS");
					}
					else
					{
						/*------------------- Check Mater Otp for verificaion code start --------------------------*/
						
						$Settings = new Settings;
						$Settings_Res = $Settings->GetUserSettings();
						
						/*if($Settings_Res[0]['settingsMasterOtp'] == $facultyOTP)
						{	
							$result[0]['data'] = $Faculty->GetUserDetails($loginfacultyID);
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPVERIFICATION_SUCCESS");

							$Faculty_Res = Faculty::find()->select('facultyID,facultyVerified')->where('facultyID = "'.$loginfacultyID.'"')->one();
							$Faculty_Res->facultyID = $loginfacultyID;
							$Faculty_Res->facultyDeviceID = $facultyDeviceID;
							if($Faculty_Res->facultyVerified = "No")
							{
								$Faculty_Res->facultyVerified = "Yes";
							}
							$Faculty_Res->facultyOTP = "";
							$Faculty_Res->save(false);
							
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPVERIFICATION_ERROR");
						}*/
						
						/*------------------- Check Mater Otp for verificaion code end --------------------------*/
						
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPVERIFICATION_ERROR");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
								

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}

	public function actionOtpResend()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$stateID = "0";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($facultyMobile) )
				{

					$Faculty = new Faculty;
					$Faculty_Res = Faculty::find()->select('facultyID')->where('facultyID = "'.$loginfacultyID.'" AND facultyMobile = "'.$facultyMobile.'"')->one();
					if(!empty($Faculty_Res))
					{   
						
						$facultyOTP = $Apilog->GenerateOTP(4);
						$Faculty_Res->facultyID = $loginfacultyID;
						$Faculty_Res->facultyOTP = $facultyOTP;
						$Faculty_Res->save(false);
						$this->SendNotification($userID,false,"000002","");
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPRESEND_SUCCESS");
						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}

								
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	public function actionFacultyLoginOtp()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$facultyCountryCode = "0";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($facultyMobile) && !empty($facultyDeviceID)  )
				{

					$Faculty = new Faculty;
					$Faculty_Res = Faculty::find()->select('facultyID')->where('facultyMobile = "'.$facultyMobile.'"')->one();
					if(!empty($Faculty_Res))
					{   
						$facultyID = $Faculty_Res->facultyID;
						$facultyOTP = $Apilog->GenerateOTP(4);
						$Faculty_Res->facultyID = $facultyID;
						$Faculty_Res->facultyOTP = $facultyOTP;
						$Faculty_Res->save(false);
						$res = array();
						$res[0]["facultyID"]= $facultyID;
						$res[0]["facultyOTP"]= $facultyOTP;
						$result[0]['status'] = 'true';
						//$result[0]['data']= $res;
						$this->SendNotification($userID,false,"000002","");
						$result[0]['data'] = $Faculty->GetFacultyDetails($facultyID);
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPRESEND_SUCCESS");
						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}

								
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	public function actionFacultyDetails()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$stateID = "0";
				extract($jsonParams[0]);
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) )
				{
					$Faculty = new Faculty;
					
					$res = $Faculty->GetFacultyDetails($loginfacultyID);  

					$result[0]['data']=$res;
					//$result[0]['data']["OTP"]=$res;
					$result[0]['status'] = 'true';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
					

				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);    
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	public function actionFacultyLoginPassword()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$stateID = "0";
				extract($jsonParams[0]);
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($facultyPassword) && !empty($facultyMobile) && !empty($facultyDeviceID) )
				{
					$Faculty = new Faculty;
					$Faculty_Res = Faculty::find()->select('facultyID,facultyMobile,facultyPassword')->where('facultyMobile = "'.$facultyMobile.'" ')->one();
					$Faculty_Res2 = Faculty::find()->select('facultyID,facultyMobile,facultyPassword')->where('facultyEmail = "'.$facultyMobile.'"')->one();
					//print_r($Faculty_Res);
					//die;
					if(!empty($Faculty_Res) )
					{   
						if(1 == \Yii::$app->security->validatePassword($facultyPassword,$Faculty_Res->facultyPassword))
						{
							$res = $Faculty->GetFacultyDetails($Faculty_Res->facultyID);  

							$result[0]['data']=$res;
							//$result[0]['data']["OTP"]=$res;
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","INVALIDUSERPASS");
						}
							
					}
					else
					{
						if(!empty($Faculty_Res2) )
						{ 
							if(1 == \Yii::$app->security->validatePassword($facultyPassword,$Faculty_Res2->facultyPassword))
							{
								$res = $Faculty->GetFacultyDetails($Faculty_Res2->facultyID);  

								$result[0]['data']=$res;
								//$result[0]['data']["OTP"]=$res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","INVALIDUSERPASS");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
						}
					}

				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);  
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}

	/*-------------- Faculty Home Sreen Data Get Code Start -----------------*/

	public function actionFacultyHome()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				$facultyModules= "Reading";	
				$statusFilter= "";	
				$unassigned="";
				$lessonType="";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) )
				{
					$leasson = new Lesson;
					
					//banners 
					$unassignedRes = array();// $leasson->GetCoursesList(0, $page,$pagesize,0, "", 0, "True",$facultyModules);
					$assignedRes = $leasson->GetCoursesList(0, $page,$pagesize,0, "", $loginfacultyID, "",$facultyModules,$lessonType,$statusFilter);
					$cnt=$leasson->GetCoursesListCount(0, $page,$pagesize,0, "", $loginfacultyID, "",$facultyModules,$lessonType,$statusFilter);
					
					$result[0]['unassigned'] = $unassignedRes;	
					$result[0]['count'] = $cnt;
					$result[0]['assigned'] = $assignedRes;	
					$result[0]['status'] = 'true';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);   
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	/*-------------- Faculty Home Sreen Data Get Code End -----------------*/

	public function actionFacultyUpdateProfile()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  				
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($facultyFullName) && !empty($facultyEmail) && !empty($facultyMobile) && !empty($facultyDeviceType) && !empty($facultyDeviceID) )
				{

					$Faculty_Res = Faculty::find()->where('facultyID = "'.$loginfacultyID.'"')->one();
					if(!empty($Faculty_Res))
					{
						$Usres = new Faculty;
						$DuplicationRes = $Usres->CheckFacultyDetailsForDuplication($facultyEmail,$facultyMobile,$loginfacultyID);
						if($DuplicationRes == 'true')
						{

							$Errors = "";
							
							$Faculty_Res->facultyFullName = $facultyFullName;
							$Faculty_Res->facultyEmail = $facultyEmail;
							$Faculty_Res->facultyMobile = $facultyMobile;
							$Faculty_Res->facultyAddress = $facultyAddress;
							$Faculty_Res->facultyPincode = $facultyPincode;
							$Faculty_Res->facultyGender = $facultyGender;
							
							$Faculty_Res->countryID = $countryID;
							$Faculty_Res->stateID = $stateID;
							$Faculty_Res->cityID = $cityID;
							
							$Faculty_Res->facultyDeviceType = $facultyDeviceType;
							$Faculty_Res->facultyDeviceID = $facultyDeviceID;
							
							$Faculty_Res->facultyDOB = $facultyDOB;
							 							
							
							$Faculty_Res->facultyProfilePicture = $facultyProfilePicture;
							
							
							
							
							
							$Faculty_Res->save(false);
							$Errors = $Faculty_Res->getErrors();
							if(empty($Errors))
							{ 
								$facultyID = $Faculty_Res->facultyID;
								$res = $Usres->GetFacultyDetails($facultyID);  
								if(!empty($res))
								{
									$result[0]['data'] = $res;
									$result[0]['status'] = 'true';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PROFILEUPDATE_SUCCESS");
								}
								else
								{
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
								}
							}
							else
							{
								//print_r($Errors);
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
							}

						}		
						else
						{
							
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","EMAIL_Exist");
							
						}
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
																	

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);      
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	
	public function actionFacultyUpdateProfilePicture()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($facultyProfilePicture)  )
				{
					$Faculty_Res = Faculty::find()->where('facultyID = "'.$loginfacultyID.'"')->one();
					if(!empty($Faculty_Res))
					{
						$Errors = "";
						$Faculty_Res->facultyProfilePicture = $facultyProfilePicture;
						$Faculty_Res->save(false);
						$Errors = $Faculty_Res->getErrors();
						if(empty($Errors))
						{ 
							$facultyID = $Faculty_Res->facultyID;
							$Usres = new Faculty;
							$res = $Usres->GetFacultyDetails($facultyID);  
							if(!empty($res))
							{
								$result[0]['data'] = $res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PROFILEPICTUREUPDATE_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] =$Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}

						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
								
							

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}

	public function actionFacultyUpdateDeviceToken()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($facultyDeviceID) && !empty($facultyDeviceType)  )
				{

					$Faculty_Res = Faculty::find()->where('facultyID = "'.$loginfacultyID.'"')->one();
					if(!empty($Faculty_Res))
					{
						$Errors = "";
						$Faculty_Res->facultyDeviceID = $facultyDeviceID;
						$Faculty_Res->facultyDeviceType = $facultyDeviceType;
						$Faculty_Res->save(false);
						$Errors = $Faculty_Res->getErrors();
						if(empty($Errors))
						{ 
							$facultyID = $Faculty_Res->facultyID;
							$Usres = new Faculty;
							$res = $Usres->GetFacultyDetails($facultyID);  
							if(!empty($res))
							{
								$Settings = new Settings;
								$facultysettings = $Settings->GetFacultySettings();  
								$result[0]['data'] = $res;
								$result[0]['facultysettings'] = $facultysettings;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","DEVICETOKENUPDATE_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}
						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = INVALIDDATA::ISMISSING;
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
							

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);     
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}

	public function actionFacultyUpdateSettings()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($facultyTestNotify) && !empty($facultyResultNotify) && !empty($loginfacultyID) && !empty($facultyLiveLectureNotify) && !empty($facultyNewsNotify) && !empty($facultyEventsNotify) && !empty($facultyNoticeNotify) && !empty($facultyAssignmentNotify) && !empty($facultyForumNotify) && !empty($facultyAdminNotify) )
				{
					 

					$Faculty_Res = Faculty::find()->where('facultyID = "'.$loginfacultyID.'"')->one();
					if(!empty($Faculty_Res))
					{
						$Errors = "";
						$Faculty_Res->languageID = $languageID;
						$Faculty_Res->facultyTestNotify = $facultyTestNotify;
						$Faculty_Res->facultyResultNotify = $facultyResultNotify;
						$Faculty_Res->facultyNewsNotify	 = $facultyNewsNotify	;
						$Faculty_Res->facultyEventsNotify = $facultyEventsNotify;
						$Faculty_Res->facultyLiveLectureNotify = $facultyLiveLectureNotify;
						$Faculty_Res->facultyNoticeNotify = $facultyNoticeNotify;
						$Faculty_Res->facultyAssignmentNotify	 = $facultyAssignmentNotify	;
						$Faculty_Res->facultyForumNotify = $facultyForumNotify;
						$Faculty_Res->facultyAdminNotify = $facultyAdminNotify;
						$Faculty_Res->save(false);
						$Errors = $Faculty_Res->getErrors();
						if(empty($Errors))
						{ 
							$facultyID = $Faculty_Res->facultyID;
							$Usres = new Faculty;
							$res = $Usres->GetFacultyDetails($facultyID);  
							if(!empty($res))
							{
								$result[0]['data'] = $res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","USERUPDATESETTINGS_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}

						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","ISMISSING");
					}
									
				}
				else
				{
					$Required = 'apiType';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}


	public function actionCheckFacultyDuplication()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$loginfacultyID = "";
				$facultyEmail = "";
				$facultyMobile = "";
				extract($jsonParams[0]);    
				if(!empty($apiType))
				{
					if(!empty($languageID))
					{
						if(!empty($apiVersion))
						{
							if((!empty($facultyEmail)) || (empty($facultyMobile)))
							{
								$Faculty = new Faculty;
								$DuplicationRes = $Faculty->CheckFacultyDetailsForDuplication($facultyEmail,$facultyMobile,$loginfacultyID);

								if($DuplicationRes == 'true')
								{
									$result[0]['status'] = 'true';
									$result[0]['message'] = '';
								}
								else if($DuplicationRes == 'facultyEmail')	
								{
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","EMAIL_Exist");
								}
								else
								{
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","MOBILE_Exist");
								}
							}
							else
							{
								$Required = 'facultyEmail OR facultyMobile';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
							}
							
						}
						else
						{
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
						}
					}
					else
					{
						$Required = 'languageID';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
					}
				}
				else
				{
					$Required = 'apiType';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);   
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}

	public function actionFacultyForgotPassword()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  			  
			   extract($jsonParams[0]);    
			    
				   $Faculty_Res = Faculty::find()->select('facultyID,facultyMobile ')->where("(facultyMobile = '".$facultyMobile."' OR facultyEmail='".$facultyMobile."')")->one();
			   				   
			   if(!empty($Faculty_Res))
				{ 
					$Faculty = new Faculty();
					$facultyOTP = $Apilog->GenerateOTP(4);
					
					$Errors = "";
					//$facultyPassword = \Yii::$app->security->generatePasswordHash($facultyPassword);		
					$Faculty_Res->facultyOTP = $facultyOTP;
					$Faculty_Res->save(false);
					$Errors = $Faculty_Res->getErrors();
					if(empty($Errors))
					{ 
						$facultyID = $Faculty_Res->facultyID;
										
						/*------------- Send OTP Code Start --------------*/

						
						$this->SendNotification($facultyID,false,"000003","");
						/*------------- Send OTP Code End --------------*/
						
				
						$result[0]['data'][0]['facultyID'] = (string)$facultyID;
						$result[0]['data'][0]['facultyMobile'] = (string)$Faculty_Res->facultyMobile;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","SENT_OTP");
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}
						 
				}
				else
				{
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","USERNOTREGISTRED");
				}	
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);  
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	public function actionResetPassword()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
						
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($facultyNewPassword) && !empty($loginfacultyID))
				{
					$Faculty_Res = Faculty::find()->select('facultyID,facultyPassword')->where('facultyID = "'.$loginfacultyID.'"')->one();
					if(!empty($Faculty_Res))
					{ 
						 
						$Errors = "";
						$facultyPassword = \Yii::$app->security->generatePasswordHash($facultyNewPassword);		
						$Faculty_Res->facultyPassword = $facultyPassword;
						$Faculty_Res->save(false);
						$Errors = $Faculty_Res->getErrors();
						if(empty($Errors))
						{ 
							$Faculty = new Faculty;
							$result[0]['data'] = $Faculty->GetFacultyDetails($loginfacultyID);
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PASSWORDCHANGE_SUCCESS");
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}
						
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	public function actionChangePassword()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
						
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($facultyCurrentPassword) && !empty($facultyNewPassword) && !empty($loginfacultyID))
				{
					$Faculty_Res = Faculty::find()->select('facultyID,facultyPassword')->where('facultyID = "'.$loginfacultyID.'"')->one();
					if(!empty($Faculty_Res))
					{ 
						 if(1 == \Yii::$app->security->validatePassword($facultyCurrentPassword,$Faculty_Res->facultyPassword))
						 {
							$Errors = "";
							$facultyPassword = \Yii::$app->security->generatePasswordHash($facultyNewPassword);		
							$Faculty_Res->facultyPassword = $facultyPassword;
							$Faculty_Res->save(false);
							$Errors = $Faculty_Res->getErrors();
							if(empty($Errors))
							{ 
								$Faculty = new Faculty;
								$result[0]['data'] = $Faculty->GetFacultyDetails($loginfacultyID);
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PASSWORDCHANGE_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
							}
						 }
						 else
						 {
							 $result[0]['status'] = 'false';
							 $result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CURRENTPASSWORD_ERROR");
						 }
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);   
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
		
	}
	public function actionGetFacultyPracticeTest()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page=0;	
				$pagesize=10	;	
				$isPending="Pending";
				$submittedDate="";	
				$sortKey="";
				extract($jsonParams[0]);    
				if(!empty($apiType)  && !empty($loginfacultyID))
				{
					
					$lessions = new Userlessions;
					$lessRes = $lessions->getTestSummary($loginfacultyID,$page,$pagesize,$isPending,$submittedDate,$sortKey);
					$cnt = $lessions->getTestSummaryCount($loginfacultyID,$page,$pagesize,$isPending,$submittedDate);
					if(!empty($lessRes)) 
					{		 
						$result[0]['data'] = $lessRes;
						$result[0]['count'] = $cnt;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
	}
	public function actionGetFacultyPracticeTestQuestions()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page=0;	
				$pagesize=10	;			
				extract($jsonParams[0]);    
				if(!empty($apiType)  && !empty($loginfacultyID) && !empty($lessionID) && !empty($userID))
				{
					
					$lessions = new Userlessions;
					$lessRes = $lessions->getTestQuestions($loginfacultyID,$lessionID,$userID,$page,$pagesize);
					
					if(!empty($lessRes)) 
					{		 
						$result[0]['data'] = $lessRes;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);   
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
	}
	public function actionUpdatePracticeTestQuestions()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
			 
				$lessonID=0;
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($userID) && (!empty($lessonID) || !empty($examID)))
				{
					$Useranswers = new Useranswers;	
					if(!empty($answers))
					{
						for($i=0;$i<count($answers);$i++) 
						{
							$answerRes = Useranswers::find()->where("queID = '".$answers[$i]['queID']."' AND userID=".$userID." AND examID=".$examID." AND lessionID=".$lessonID )->one();
							

							//print_r($answerRes)	;die;
							
							$answerRes->answerIsCorrect = $answers[$i]['answerIsCorrect'];
							$answerRes->answerIsVerified = "Verified";
							$answerRes->answerMark = @$answers[$i]['answerMark'];
							$answerRes->answerGrade = @$answers[$i]['answerGrade'];
							$answerRes->answerNotes = @$answers[$i]['answerNotes'];
							$answerRes->facultyID = $loginfacultyID;
							$answerRes->save(false);
									
						}
					}
					/*Add in answers*/
					/*Check if answer auto correct is possible or not*/
					if(!empty($lessonID))
					{
						$userLess = new Userlessions;
						$userLessRes = Userlessions::find()->where("lessionID = '".$lessonID."' AND userID=".$userID)->one();
						
						$userLessRes->userlessionEvaluationDate = date('Y-m-d H:i:s');
						$userLessRes->save(false);
						
						$this->SendNotification($userID,false,"000010","","lesson-evalution",$lessonID,"");
					}
					if(!empty($examID))
					{
						$Userexams = new Userexams;
						$userLessRes = Userexams::find()->where("examID = '".$examID."' AND userID=".$userID)->one();
						if(empty($userLessRes))
						{
							$userLessRes = new Userexams;
						}
						$userLessRes->userexamDate = date('Y-m-d');
						$userLessRes->userexamGrade	 = "";
						$userLessRes->examID = $examID;
						$userLessRes->userexamVerifiedBy = $loginfacultyID;	
						$userLessRes->userexamEvaluationDate = date('Y-m-d H:i:s');						
						$userLessRes->userexamVerifiedDate = date('Y-m-d H:i:s');
						$userLessRes->userID = $userID;
						$userLessRes->save(false);
						$this->SendNotification($userID,false,"000010","","exam-evalution","",$examID);
					}
					
					//List of course
					 
						//$result[0]['questions'] = $resCourses;
						//$result[0]['totalQuestions'] = count($resCourses);
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					 

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);     
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
	
	}

	public function actionGetFacultyExamTest()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page=0;	
				$pagesize=10	;	
				$isPending="Pending";	
				$examSubmittedDate="";
				extract($jsonParams[0]);    
				if(!empty($apiType)  && !empty($loginfacultyID))
				{
					
					$lessions = new Userexams;
					$lessRes = $lessions->getTestSummary($loginfacultyID,$page,$pagesize,$isPending,$examSubmittedDate);
					$cnt = $lessions->getTestSummaryCount($loginfacultyID,$page,$pagesize,$isPending,$examSubmittedDate);
					if(!empty($lessRes)) 
					{		 
						$result[0]['data'] = $lessRes;
						$result[0]['count'] = $cnt;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);    
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
	}
	
	
	public function actionGetFacultyExamQuestions()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page=0;	
				$pagesize=10	;			
				$evalute="Pending";
				$examSubmittedDate="";
				extract($jsonParams[0]);    
				if(!empty($apiType)  && !empty($loginfacultyID) && !empty($examID) && !empty($userID))
				{
					
					$lessions = new Userexams;
					
					/*$lRes = Userexams::find()->where("examID=".$examID." AND userID=".$userID )->one();
					if($lRes->userexamVerifiedBy >0)
					{
						$evalute="Verified";
					}
					*/
					$lessRes = $lessions->getTestQuestions($loginfacultyID,$examID,$userID,$page,$pagesize,$evalute,$examSubmittedDate);
					
					if(!empty($lessRes)) 
					{		 
						$result[0]['data'] = $lessRes;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
	}
	public function actionFacultyReleaseResult()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				 
				extract($jsonParams[0]);    
				if(!empty($apiType)  && !empty($loginfacultyID) && !empty($examID) && !empty($userID))
				{
					
					$lessions = new Userexams;
					
					$lRes = Userexams::find()->where("examID=".$examID." AND userID=".$userID )->one();
					if(!empty($lRes))
					{
						$lRes->userexamReleaseResult="Yes";
						$lRes->save(false);
						
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
header("Access-Control-Allow-Headers: Content-Type, origin");
header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
	}
	private function SendNotification($userID,$isAdmin,$templateConstant,$templateConstantAdmin,$notiType="",$lessionID="",$examID="",$facultyID="")
	{
		//return true;
		$template = new Template();																
		$settings = new Settings();
		$settings_arr = Settings::find()->select('settingsEmailFrom,settingsEmailTo,settingsEmailGateway,settingEmailID,settingsEmailPass,settingsSSL,settingsTLS,settingEmailPort,settingSenderName')->where(['settingsID' => '1'])->asArray()->all();	
		$User_Res = Users::find()->where('userID = "'.$userID.'"')->one();
		$Lesson_Res = Lesson::find()->where('lessonID = "'.$lessionID.'"')->one();
		
		$Exam_Res = Exams::find()->where('examID = "'.$examID.'"')->one();
		$faculty_Res = Faculty::find()->where('facultyID = "'.$facultyID.'"')->one();		
		/*OTP*/
		$templateArr = $template->GetTemplate($templateConstant);
		//print_r($templateArr);die;
		$subject= $templateArr["templateSubject"];
		$mailStr= $templateArr["templateEmailText"];
		$smsStr= $templateArr["templateSMSText"];
		$pushStr= $templateArr["templateNotificationText"];
		$fieldArray	= array();
		$fieldArray["userOTP"]=$User_Res->userOTP;
		$fieldArray["userFullName"]=$User_Res->userFullName;
		$fieldArray["username"]=$User_Res->userFullName;
		$userEmail = $User_Res->userEmail;						
		$userMobile = $User_Res->userMobile;
		$userCountryCode = $User_Res->userCountryCode;	
		
		if(!empty($faculty_Res))
		{
			$fieldArray["userOTP"]=$faculty_Res->facultyOTP;
			$fieldArray["userFullName"]=$faculty_Res->facultyFullName;
			$fieldArray["username"]=$faculty_Res->facultyFullName;
			$userEmail = $faculty_Res->facultyEmail;						
			$userMobile = $faculty_Res->facultyMobile;
			$userCountryCode = "+91";	
		}
		
		if(!empty($Exam_Res))
		{
			$fieldArray["examName"]=$Exam_Res->examName;
			$ContentID= $examID;
		}
		if(!empty($Lesson_Res))
		{
			$module_Res = Coursemodule::find()->where('moduleID = "'.$Lesson_Res->moduleID.'"')->one();
			$fieldArray["LessonName"]=$Lesson_Res->LessonName;
			$fieldArray["moduleName"]=$module_Res->moduleName;
			$ContentID= $lessionID;
		}
		
		
		$appType="Customer";
		
		/*Replace Key with Array Value*/
		foreach($fieldArray as $key => $value)	
		{			
			$mailStr=str_replace("###".$key."###",$value,$mailStr);
			$subject = str_replace("###".$key."###",$value,$subject);
			$smsStr = str_replace("###".$key."###",$value,$smsStr);
			$pushStr = str_replace("###".$key."###",$value,$pushStr);
		}			
		if(!empty($mailStr)) 
			$template->Sendemail($settings_arr[0]["settingsEmailFrom"],$subject,$userEmail,$mailStr,$settings_arr[0]["settingSenderName"]);
		if(!empty($smsStr)) 
			$template->sendSMS($userCountryCode, $userMobile,$smsStr);
		if(!empty($pushStr))
			$template->push($User_Res->userDeviceID,$notiType,$pushStr,$ContentID,$userID,$User_Res->userDeviceType,$ContentID,$subject,false,"",0,$appType);
		
		
		if($isAdmin)
		{
			$templateArr = $template->GetTemplate($templateConstantAdmin);
			$subject= $templateArr["templateSubject"];
			$mailStr= $templateArr["templateEmailText"];
			$smsStr= $templateArr["templateSMSText"];
			$pushStr= $templateArr["templateNotificationText"];
			
			/*Replace Key with Array Value*/
			foreach($fieldArray as $key => $value)	
			{			
				$mailStr=str_replace("###".$key."###",$value,$mailStr);
				$subject = str_replace("###".$key."###",$value,$subject);
			}			
			if(!empty($mailStr)) 
				$template->Sendemail($settings_arr[0]["settingsEmailFrom"],$subject,$userEmail,$mailStr,$settings_arr[0]["settingSenderName"]);
			if(!empty($smsStr)) 
				$template->sendSMS("91", $userMobile,$smsStr);
			if(!empty($pushStr)) 
				$template->push($User_Res->userDeviceID,"Admin",$pushStr,$userID,$userID,$User_Res->userDeviceType,$ContentID,$subject,false,"",0,$appType);		
		//echo $mailStr."<Br>".$smsStr."<BR>".$pushStr."<BR>";	
		}
		//die;
		
	}
	
}
