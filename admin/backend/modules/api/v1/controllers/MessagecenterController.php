<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Messagecenter;
class MessagecenterController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
        
        return $result;
    }
	//feedbacklist
	public function actionGetMessagecenterList()
    {
    	$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			$page=0;
			$pagesize="10";
			$messagecenterType="";
			$messagecenterTo="";
            extract($jsonParams[0]);
        	if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID) && !empty($messagecenterTo))
	        { 
				   $Messagecenter = new Messagecenter;
				   $res = $Messagecenter->GetMessagecenterList($messagecenterType,$messagecenterTo,$page,$pagesize);  
				   if(!empty($res))
				   {
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
				   }
				   else
				   {		                        
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
				   }
		         
	        }
	        else
	        {
				$Required = 'Required data ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
        return $result; 
			
    }

}
