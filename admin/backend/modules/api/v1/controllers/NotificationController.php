<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Notification;
use backend\modules\api\v1\models\Settings;
use common\models\Template;
use backend\modules\api\v1\models\Livesessions;
use backend\modules\api\v1\models\Users;
use backend\modules\api\v1\models\Livesessiondetails;
use Yii;
class NotificationController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
       // $result[0]['message'] = messages_constant::JSON_ERROR;
	   header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
        return $result;
    }

    public function actionGetNotificationList()
    {
        $Apilog = new Apilog; 
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
        $postParams = \Yii::$app->request->post();    
        
        if ($Apilog->is_json($postParams["json"]))
        {           
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
            if(count($jsonParams[0])>0)
            {  
            	$page="0";
            	$pagesize = "20";

               extract($jsonParams[0]);    
               if(!empty($apiType))
               {
	                if(!empty($languageID))
	                {
		                if(!empty($apiVersion))
		                {
		                	if(!empty($loginuserID))
		                	{

			                   $Notification = new Notification;
			                   $res = $Notification->GetNotificationList($loginuserID,$page,$pagesize);  
			                   if(!empty($res))
			                   {
			                        $result[0]['data'] = $res;
			                        $result[0]['status'] = 'true';
			                        $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
			                   }
			                   else
			                   {
			                        $result[0]['status'] = 'false';
			                        $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
			                   }
		                    }
			                else
			                {
			                    $Required = 'loginuserID';
			                    $result[0]['status'] = 'false';
			                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			                }
		               
		                }
		                else
		                {
		                    $Required = 'apiVersion';
		                    $result[0]['status'] = 'false';
		                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
		                }
	                }
	                else
	                {
	                    $Required = 'languageID';
	                    $result[0]['status'] = 'false';
	                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	                }
                }
                else
                {
                    $Required = 'apiType';
                    $result[0]['status'] = 'false';
                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
                }

            }
        }
        $requestString= $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);  
header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
        return $result; 
			
    }

    public function actionUpdateNotificationReadStatus()
    {
        $Apilog = new Apilog; 
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
        $postParams = \Yii::$app->request->post();    
        
        if ($Apilog->is_json($postParams["json"]))
        {           
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
            if(count($jsonParams[0])>0)
            {  
               extract($jsonParams[0]);    
               if(!empty($apiType))
               {
	                if(!empty($languageID))
	                {
		                if(!empty($apiVersion))
		                {
		                	if(!empty($loginuserID))
		                	{
		                		if(!empty($notificationID))
		                		{
				                   $Notification = new Notification;
				                   $result = $Notification->UpdateNotificationReadStatus($loginuserID,$notificationID,$languageID);  
			                    }
				                else
				                {
									$Notification = new Notification;
				                   $result = $Notification->UpdateNotificationReadStatus($loginuserID,$notificationID,$languageID);  
				                    /*$Required = 'notificationID';
				                    $result[0]['status'] = 'false';
				                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");*/
				                }
			                 }
			                else
			                {
			                    $Required = 'loginuserID';
			                    $result[0]['status'] = 'false';
			                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			                }
		               
		                }
		                else
		                {
		                    $Required = 'apiVersion';
		                    $result[0]['status'] = 'false';
		                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
		                }
	                }
	                else
	                {
	                    $Required = 'languageID';
	                    $result[0]['status'] = 'false';
	                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	                }
                }
                else
                {
                    $Required = 'apiType';
                    $result[0]['status'] = 'false';
                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
                }

            }
        }
        $requestString= $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);   
header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
        return $result; 
			
    }

    public function actionDeleteNotification()
    {
        $Apilog = new Apilog; 
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
        $postParams = \Yii::$app->request->post();    
        
        if ($Apilog->is_json($postParams["json"]))
        {           
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
            if(count($jsonParams[0])>0)
            {  
               extract($jsonParams[0]);    
               if(!empty($apiType))
               {
	                if(!empty($languageID))
	                {
		                if(!empty($apiVersion))
		                {
		                	if(!empty($loginuserID))
		                	{
		                		if(!empty($notificationID))
		                		{
				                   $Notification = new Notification;
				                   $result = $Notification->DeleteNotification($loginuserID,$notificationID,$languageID);  
			                    }
				                else
				                {
				                    $Required = 'notificationID';
				                    $result[0]['status'] = 'false';
				                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				                }
			                 }
			                else
			                {
			                    $Required = 'loginuserID';
			                    $result[0]['status'] = 'false';
			                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			                }
		               
		                }
		                else
		                {
		                    $Required = 'apiVersion';
		                    $result[0]['status'] = 'false';
		                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
		                }
	                }
	                else
	                {
	                    $Required = 'languageID';
	                    $result[0]['status'] = 'false';
	                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	                }
                }
                else
                {
                    $Required = 'apiType';
                    $result[0]['status'] = 'false';
                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
                }

            }
        }
        $requestString= $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);  
header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
        return $result; 
			
    }
	
	public function actionClearLogsCronJob()
	{
		//Select * FROM apilog WHERE apiCallDate <= DATE_ADD('2019-08-30', INTERVAL -15 DAY)
		$settings = new Settings();
		$settings_arr = Settings::find()->select('settingsLogDeleteDays')->where(['settingsID' => '1'])->asArray()->all();	
		$sql= "DELETE FROM apilog WHERE apiCallDate <= DATE_ADD('".date('Y-m-d')."', INTERVAL -".$settings_arr[0]["settingsLogDeleteDays"]." DAY)";
		
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql)->execute();
	}
	public function actionFreeUpgradeJob()
	{
		//return true;
		$template = new Template();																
		$settings = new Settings();
		$settings_arr = Settings::find()->select('settingsEmailFrom,settingsEmailTo,settingsEmailGateway,settingEmailID,settingsEmailPass,settingsSSL,settingsTLS,settingEmailPort,settingSenderName,settingsFreeNotification')->where(['settingsID' => '1'])->asArray()->all();	
		$templateArr = $template->GetTemplate("000050");
		$subject= $templateArr["templateSubject"];
		$mailStr= $templateArr["templateEmailText"];
		$smsStr= $templateArr["templateSMSText"];
		$pushStr= $templateArr["templateNotificationText"];
		
		$sql = "SELECT userID,userDeviceType,userType,userDeviceID, userFullName, userMobile, userEmail FROM users
				where userType='Free' AND date(userCreatedDate) < DATE_SUB(CURDATE(), INTERVAL ".$settings_arr[0]["settingsFreeNotification"]." DAY)";
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$processData = $command->queryAll();
		$fieldArray	= array();
		for ($x = 0; $x <= count($processData)-1; $x++)	
		{
			$fieldArray	= $processData[$x];
			//print_r($fieldArray);die;
			foreach($fieldArray as $key => $value)	
			{			
				$mailStr2=str_replace("###".$key."###",$value,$mailStr);
				$subject2 = str_replace("###".$key."###",$value,$subject);
				$smsStr2 = str_replace("###".$key."###",$value,$smsStr);
				$pushStr2 = str_replace("###".$key."###",$value,$pushStr);
			}
			extract($processData[$x]);
			$template->push($userDeviceID,"free-upgradde",$pushStr2,$userID,$userID,$userDeviceType,"",$subject2,true,"",0);
		
		}
		
		//Select * FROM apilog WHERE apiCallDate <= DATE_ADD('2019-08-30', INTERVAL -15 DAY)
		
		
		//000014
		
	}
	public function actionHomeworkJob()
	{
		// return true;
		$template = new Template();																
		$settings = new Settings();
		$settings_arr = Settings::find()->select('settingsEmailFrom,settingsEmailTo,settingsEmailGateway,settingEmailID,settingsEmailPass,settingsSSL,settingsTLS,settingEmailPort,settingSenderName,settingsHomeworkReminder')->where(['settingsID' => '1'])->asArray()->all();	
		$templateArr = $template->GetTemplate("000014");
		$subject= $templateArr["templateSubject"];
		$mailStr= $templateArr["templateEmailText"];
		$smsStr= $templateArr["templateSMSText"];
		$pushStr= $templateArr["templateNotificationText"];
		
		$sql = "SELECT users.userID,userDeviceType,userType,userDeviceID, userFullName, 	
				userMobile, userEmail,liveName,livesessions.liveID FROM users
				inner join usersubscriptions on usersubscriptions.userID = users.userID
				inner join livesessions on livesessions.liveBatch = usersubscriptions.usersubscriptionBatch
				inner join livesessiondetails on livesessions.liveID = livesessiondetails.liveID
				where userType='Premium' AND sessiondetailType = 'Lesson' AND date(liveCreatedDate) < CURDATE()
				AND users.userID IN (SELECT userID from userlessions WHERE userlessionCreatedDate > DATE_SUB(CURDATE(), INTERVAL ".$settings_arr[0]["settingsHomeworkReminder"]." HOUR))
				group by users.userID
";
		echo $sql;die;	
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$processData = $command->queryAll();
		$fieldArray	= array();
		for ($x = 0; $x <= count($processData)-1; $x++)	
		{
			$fieldArray	= $processData[$x];
			//print_r($fieldArray);die;
			foreach($fieldArray as $key => $value)	
			{			
				$mailStr=str_replace("###".$key."###",$value,$mailStr);
				$subject = str_replace("###".$key."###",$value,$subject);
				$smsStr = str_replace("###".$key."###",$value,$smsStr);
				$pushStr = str_replace("###".$key."###",$value,$pushStr);
			}
			extract($processData[$x]);
			$template->push($userDeviceID,"homework-reminder",$pushStr,$liveID,$userID,$userDeviceType,"",$subject,true,"",0);
		
		}
		
		//Select * FROM apilog WHERE apiCallDate <= DATE_ADD('2019-08-30', INTERVAL -15 DAY)
		
		
		//000014
		
	}
	public function actionLiveSessionJob()
	{
		// return true;
		$template = new Template();																
		$settings = new Settings();
		$settings_arr = Settings::find()->select('settingsEmailFrom,settingsEmailTo,settingsEmailGateway,settingEmailID,settingsEmailPass,settingsSSL,settingsTLS,settingEmailPort,settingSenderName,settingsLiveSessionReminder')->where(['settingsID' => '1'])->asArray()->all();	
		$templateArr = $template->GetTemplate("000015");
		$subject= $templateArr["templateSubject"];
		$mailStr= $templateArr["templateEmailText"];
		$smsStr= $templateArr["templateSMSText"];
		$pushStr= $templateArr["templateNotificationText"];
		
		//$resSessions = $Livesessions->GetSessionsList($loginuserID,$page,$pagesize,$future,"",$shift);
		
		$Where = 	' WHERE liveStatus = "Active"  ';//AND coursePublished="Yes"';
		$Where.= " AND liveStartDate = '".date('Y-m-d')."'";
		
		$now = date("His");//or date("H:i:s")
		$mStart = '060000';//or '13:00:00'
		$mEnd = '125959';//or '17:00:00'
		$nStart = '120100';//or '13:00:00'
		$nEnd = '165959';//or '17:00:00'
		$eStart = '160100';//or '13:00:00'
		$eEnd = '235959';//or '17:00:00'
		if($now >= $mStart && $now <= $mEnd){
			$shift="Morning";
		}
		if($now >= $nStart && $now <= $nEnd){
			$shift="Afternoon";
		}
		if($now >= $eStart && $now <= $eEnd){
			$shift="Evening";
		}
	    $Where.= " AND livesessions.liveBatch = '".$shift."'";
		$cDay = date("D");
		
		$Where.= " AND ( (DATE_FORMAT(liveStartDate,'%a') = '".$cDay."')) ";
		$Where.= " AND liveStartTime <= DATE_ADD(NOW(), INTERVAL ".$settings_arr[0]["settingsLiveSessionReminder"]." HOUR)";
		$para = 'SELECT livesessions.*,moduleName, facultyFullName	';				
		$from = ' FROM livesessions';
		$innerJoin = " inner join coursemodule on coursemodule.moduleID = livesessions.moduleID inner join faculty on faculty.facultyID = livesessions.facultyID  ";
		$sql = $para.$from.$innerJoin.$Where;
		
 

		//echo $sql;die;	
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$res = $command->queryAll();
		$fieldArray	= array();
		for ($x = 0; $x <= count($res)-1; $x++)	
		{
			$fieldArray	= $res[$x];
			//print_r($fieldArray);die;
			extract($res[$x]);
			$uSQL = "SELECT users.*,usersubscriptionShift FROM users INNER JOIN usersubscriptions ON usersubscriptions.userID = users.userID WHERE usersubscriptionEndDate >='". date('Y-m-d')."' AND usersubscriptionShift LIKE '%".$cDay."%' group by users.userID ";
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand($uSQL);
			$processData = $command->queryAll();
			//echo $uSQL;die;
			for ($j = 0; $j <= count($processData)-1; $j++)	
			{
				foreach($fieldArray as $key => $value)	
				{			
					$mailStr=str_replace("###".$key."###",$value,$mailStr);
					$subject = str_replace("###".$key."###",$value,$subject);
					$smsStr = str_replace("###".$key."###",$value,$smsStr);
					$pushStr = str_replace("###".$key."###",$value,$pushStr);
				}
				extract($processData[$j]);
				$template->push($userDeviceID,"liveclass-reminder",$pushStr,$liveID,$userID,$userDeviceType,"",$subject,true,"",0);
			}			
		}
		
		//Select * FROM apilog WHERE apiCallDate <= DATE_ADD('2019-08-30', INTERVAL -15 DAY)
		
		
		//000014
		
	}
	public function actionNotifcationCronJob()
	{ 
	
		$where = " WHERE 1 =1 AND notificationStatus='No' ";
		$join = '    ';				
		$para = ' Select *   ';				
		$groupby = "  "			;
		$from = ' FROM notification';	      
		$sql = $para.$from.$join.$where.$groupby;
		//	echo $sql; die; 
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$processData = $command->queryAll();
		$template = new Template();	
		for ($x = 0; $x <= count($processData)-1; $x++)	
		{
			extract($processData[$x]);
			$template->push($userDeviceID,$notificationType,$notificationMessageText,$notificationReferenceKey,$userID,$userDeviceType,0,$notificationTitle,true,$notificationID);
			//($userDeviceID,$notificationType,$notificationMessageText,$notificationReferenceKey,$userID,$userDeviceType,"",$notificationTitle,true,"",$notificationID,$notificationMessageTextArabic,$notificationTitleArabic);
			
			//($userDeviceID,$notificationType,$notificationMessageText,$notificationReferenceKey,$userID,$userDeviceType,$ContentID,$notificationTitle)
		
		}
		
	}
}
