<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Lesson;
use backend\modules\api\v1\models\Lessonfiles;
use backend\modules\api\v1\models\Questions;
use backend\modules\api\v1\models\Useranswers;
use backend\modules\api\v1\models\Userlessions;
use backend\modules\api\v1\models\Exams;
use backend\modules\api\v1\models\Userexams;
use backend\modules\api\v1\models\Bands;
use backend\modules\api\v1\models\Coursemodule;
// use backend\modules\api\v1\models\Examtimer;
use Yii;
class LessonController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
        header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
        return $result;
    }
	public function actionGetCourses()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				$searchkeyword="";
				$usertype="";
				$lessionID="0";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) )
				{
					$Lesson = new Lesson;
					$Lessonfiles = new Lessonfiles;
					//List of course
					$resCourses = $Lesson->GetCoursesList($moduleID,$page,$pagesize,$loginuserID,$searchkeyword,0,"","",$usertype,"",$lessionID);
					//$resVisit = $Uservisitedcourses->GetVisitedCoursesList($loginuserID);
//					$resGetCoursechaptersList = $Lessonfiles->GetCoursechaptersList(0,$loginuserID,"","True");
					if(!empty($resCourses))
					{
						$result[0]['lessons'] = $resCourses;
						//$result[0]['visiteddata'] = $resGetCoursechaptersList;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);             
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}
	//get questions for practice
	public function actionGetQuestions()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				
				$examID=0;
				$lessionID=0;
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($loginuserID) && !empty($apiVersion) && (!empty($lessionID) || !empty($examID) ))
				{
					$Questions = new Questions;
					
					//List of course
					$resCourses = $Questions->GetQuestionList($lessionID,$examID);
					if(!empty($resCourses))
					{
						$result[0]['questions'] = $resCourses;
						$result[0]['totalQuestions'] = count($resCourses);
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}
	//submit answers
	public function actionSubmitAnswers()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
			 
				$lessonID=0;
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID)  && !empty($totalAnswered) && !empty($totalQuestions) && (!empty($lessonID) || !empty($examID)))
				{
					$Useranswers = new Useranswers;
					$answerFor="Lession";
					if(!empty($examID))
					  $answerFor="Exams";

					$moduleID=0;
					$queVerificationRequred="Yes";
					if(!empty($answers))
					{
						for($i=0;$i<count($answers);$i++) 
						{
							$answerRes = Useranswers::find()->where("queID = '".$answers[$i]['queID']."' AND userID=".$loginuserID." AND examID=".$examID." AND lessionID=".$lessonID )->one();
							if(empty($answerRes))
							{
								$answerRes = new Useranswers;
							}
							$answerRes->queID = $answers[$i]['queID'];						
							$answerRes->answerAnswer = @$answers[$i]['answerAnswer'];
							$answerRes->answerCorrectAnswer = $answers[$i]['answerCorrectAnswer'];
							$answerRes->answerIsCorrect = @$answers[$i]['answerIsCorrect'];
							$answerRes->userID = $loginuserID;
							$answerRes->answerFor = $answerFor;
							$answerRes->examID = $examID;
							$answerRes->lessionID = $lessonID;
							
							$qRes = Questions::find()->where("queID=".$answers[$i]['queID'])->one();
							//echo $qRes->queVerificationRequred;
							//print_r($qRes);die;
							$moduleID= $qRes->moduleID;
							$queVerificationRequred=$qRes->queVerificationRequred;
							if($qRes->queVerificationRequred=="No")
								$answerRes->answerIsVerified = "Verified";
							else
								$answerRes->answerIsVerified = "Pending";
							$answerRes->save(false);
									
						}
					}
					/*Add in answers*/
					/*Check if answer auto correct is possible or not*/
					if(!empty($lessonID))
					{
						
						$userLess = new Userlessions;
						$userLessRes = Userlessions::find()->where("lessionID = '".$lessonID."' AND userID=".$loginuserID)->one();
						if(empty($userLessRes))
						{
							$userLessRes = new Userlessions;
						}
						$userLessRes->userlessionCorrectAnswers = $userlessionCorrectAnswers;
						$userLessRes->userlessionWrongAnswers = $userlessionWrongAnswers;
						$userLessRes->lessionID = $lessonID;
						$userLessRes->userlessionQuestions = $totalQuestions;
						$userLessRes->userlessionAnswered = $totalAnswered;
						$userLessRes->userID = $loginuserID;						
						$userLessRes->save(false);
						
						$r=$userLess->updateLessonBands($loginuserID,$lessonID);
					}
					if(!empty($examID))
					{
						$Userexams = new Userexams;
						$userLessRes = Userexams::find()->where("examID = '".$examID."' AND userID=".$loginuserID)->one();
						if(empty($userLessRes))
						{
							$userLessRes = new Userexams;
						}
						
						//get Faculty for gradding
						$facID =0;
						$SQL=" SELECT userexams.facultyID, count(DISTINCT (userexamID)) as cnt FROM faculty
						LEFT JOIN userexams  on faculty.facultyID = userexams.facultyID
						LEFT JOIN lessonfaculty on userexams.facultyID = lessonfaculty.facultyID
						WHERE (facultyStatus='Active' AND (lessonfacultyType='Grading' OR lessonfacultyType='Both')) AND userexamReleaseResult = 'No'
						group by faculty.facultyID, userexams.facultyID,lessonfaculty.facultyID
						order by count(userexamID) ASC ";
						$connection = Yii::$app->getDb();
						$command = $connection->createCommand($SQL);
						$resF = $command->queryAll();
						//echo $SQL;die;
						if(count($resF)>0)
						{
						   for($i=0;count($resF)>$i; $i++) 
							{ 
								
								$facID = $resF[$i]["facultyID"];
								break;
							}
						}
						if(empty($facID))
						{
							$SQL=" SELECT lessonfaculty.facultyID FROM faculty
							LEFT JOIN lessonfaculty on faculty.facultyID = lessonfaculty.facultyID
							WHERE (facultyStatus='Active' AND (lessonfacultyType='Grading' OR lessonfacultyType='Both')) 
							group by faculty.facultyID, lessonfaculty.facultyID";
							$connection = Yii::$app->getDb();
							$command = $connection->createCommand($SQL);
							$resF = $command->queryAll();
							//echo $SQL;die;
							if(count($resF)>0)
							{
							   for($i=0;count($resF)>$i; $i++) 
								{ 
									
									$facID = $resF[$i]["facultyID"];
									break;
								}
							}	
						}
						
						
						$userLessRes->userexamDate = date('Y-m-d');
						$userLessRes->userexamGrade	 = "";
						$userLessRes->examID = $examID;
						$userLessRes->facultyID = $facID;
						$userLessRes->userexamVerifiedBy = "0";						
						$userLessRes->userID = $loginuserID;
						$userLessRes->save(false);
						
						$r=$Userexams->updateExamBands($loginuserID,$examID);
						
					}
					
					//List of course
					$Questions = new Questions;
					$resCourses = $Questions->GetQuestionAnswerList($lessonID,$examID,$loginuserID);
					$examRes = Userexams::find()->where('examID='.$examID.' AND userID='.$loginuserID)->asArray()->one();

					$lessRes = Userlessions::find()->where('lessionID='.$lessonID.' AND userID='.$loginuserID)->asArray()->one();
					$result[0]['questions'] = $resCourses;
					$result[0]["lesson"] = $lessRes;
					$result[0]["exam"] = $examRes;
					$result[0]['totalQuestions'] = count($resCourses);
					$result[0]['status'] = 'true';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					 

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);    
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}
	public function actionGetSubmittedAnswers()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				
				$examID=0;
				$lessionID=0;
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($loginuserID) && !empty($apiVersion) && (!empty($lessionID) || !empty($examID) ))
				{
					$Questions = new Questions;
					$userLess = new Userlessions;
					$Userexams = new Userexams;
					$r=$userLess->updateLessonBands($loginuserID,$lessionID);
					$r=$Userexams->updateExamBands($loginuserID,$examID);	
					//List of course
					$resCourses = $Questions->GetQuestionAnswerList($lessionID,$examID,$loginuserID);
					$examRes = Userexams::find()->where('examID='.$examID.' AND userID='.$loginuserID)->asArray()->one();
				//lessionID
				//coursemodule
				$lessRes = Userlessions::find()->select('userlessions.*,lesson.moduleID')->join('inner join','lesson','lesson.LessonID=userlessions.lessionID')->join('inner join','coursemodule','coursemodule.moduleID=lesson.moduleID')->where('lessionID='.$lessionID.' AND userID='.$loginuserID)->asArray()->one();
				
					if(!empty($resCourses))
					{
						$result[0]['questions'] = $resCourses;
						$result[0]["lesson"] = $lessRes;
						//print_r($lessRes);die;
						if($lessRes["moduleID"]=="7" || $lessRes["moduleID"]=="12")
						{
							$result[0]["lesson"]["extramessage"] = "Please upgrade to premium to get evaluated this answers with faculty.";
							$result[0]["lesson"]["paidextramessage"] = "Please use premium mode's lessons to get evaluated this answers with Faculty.";							
						}
						else
						{
							$result[0]["lesson"]["extramessage"] = "";
							$result[0]["lesson"]["paidextramessage"] = "";	
						}

						$result[0]["exam"] = $examRes;
						$result[0]['totalQuestions'] = count($resCourses);
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");		
		return $result; 
	}
	
	//get exams
	public function actionGetExams()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				
				$examID=0;
				$lessionID=0;
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($loginuserID) && !empty($apiVersion) )
				{
					$Exams = new Exams;
					
					//List of course
					$resCourses = $Exams->GetExamsList($loginuserID);
					if(!empty($resCourses))
					{
						$result[0]['questions'] = $resCourses;
						$result[0]['totalQuestions'] = count($resCourses);
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);      
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}
	// Assign to faculty
	public function actionAssignFaculty()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$facultyModules="Reading";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($lessionID)  )
				{
					$lessionRes = Lesson::find()->where('facultyID = "0" AND LessonID="'.$lessionID.'"')->one();
					$leasson = new Lesson;
					if(!empty($lessionRes))
					{
						$Errors = "";
						$lessionRes->facultyID = $loginfacultyID;
						$lessionRes->save(false);
						$Errors = $lessionRes->getErrors();
						if(empty($Errors))
						{ 
							$unassignedRes = $leasson->GetCoursesList(0, 0,"10",0, "", 0, "True",$facultyModules);
							$assignedRes = $leasson->GetCoursesList(0, 0,"10",0, "", $loginfacultyID, "",$facultyModules);
							if(!empty($unassignedRes) || !empty($assignedRes))
							{
								$result[0]['unassigned'] = $unassignedRes;	
								$result[0]['assigned'] = $assignedRes;	
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PROFILEPICTUREUPDATE_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] =$Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}

						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
								
							

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}
	// update progress
	public function actionUpdateProgress()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$facultyModules="Reading";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginfacultyID) && !empty($lessionID) && !empty($LessonProgress)  )
				{
					$lessionRes = Lesson::find()->where('facultyID = "'.$loginfacultyID.'" AND LessonID="'.$lessionID.'"')->one();
					$leasson = new Lesson;
					if(!empty($lessionRes))
					{
						$Errors = "";
						$lessionRes->LessonProgress = $LessonProgress;
						$lessionRes->save(false);
						$Errors = $lessionRes->getErrors();
						if(empty($Errors))
						{ 
							$unassignedRes = $leasson->GetCoursesList(0, 0,"10",0, "", 0, "True",$facultyModules);
							$assignedRes = $leasson->GetCoursesList(0, 0,"10",0, "", $loginfacultyID, "",$facultyModules);
							if(!empty($unassignedRes) || !empty($assignedRes))
							{
								$result[0]['unassigned'] = $unassignedRes;	
								$result[0]['assigned'] = $assignedRes;	
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PROFILEPICTUREUPDATE_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] =$Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}

						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
								
							

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}
	// edit lession
	public function actionEditLesson()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  				
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($moduleID) && !empty($LessonNo) && !empty($LessonType) && !empty($LessonFileType) && (!empty($LessonFile) || !empty($LessonFileURL)) && !empty($loginfacultyID) && !empty($LessonProgress) && !empty($LessonDescription) && !empty($LessonName) && !empty($lessionID))
				{

					$Lesson_Res = Lesson::find()->where('facultyID = "'.$loginfacultyID.'" AND LessonID="'.$lessionID.'"')->one();
					if(!empty($Lesson_Res))
					{
							$Errors = "";							
							$Lesson_Res->moduleID = $moduleID;
							$Lesson_Res->LessonNo = $LessonNo;
							$Lesson_Res->LessonType = $LessonType;
							$Lesson_Res->LessonFileType = $LessonFileType;
							$Lesson_Res->LessonFile = $LessonFile;
							$Lesson_Res->LessonFileURL = $LessonFileURL;
							$Lesson_Res->facultyID = $loginfacultyID;
							$Lesson_Res->LessonProgress = $LessonProgress;
							$Lesson_Res->LessonDescription = $LessonDescription;$Lesson_Res->LessonName = $LessonName;
							$Lesson_Res->save(false);
							$Errors = $Lesson_Res->getErrors();
							if(empty($Errors))
							{ 
								$LessonID = $Lesson_Res->LessonID;
								
								$result[0]['data'] = $LessonID;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PROFILEUPDATE_SUCCESS");
								
							}
							else
							{
								//print_r($Errors);
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
							}

						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
																	

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
		return $result; 
		
	}
	
	// add lession
	public function actionAddLesson()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  				
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($moduleID) && !empty($LessonNo) && !empty($LessonType) && !empty($LessonFileType) && (!empty($LessonFile) || !empty($LessonFileURL)) && !empty($loginfacultyID) && !empty($LessonProgress) && !empty($LessonDescription) && !empty($LessonName) )
				{

					$Lesson_Res = new Lesson;
					 
							$Errors = "";							
							$Lesson_Res->moduleID = $moduleID;
							$Lesson_Res->LessonNo = $LessonNo;
							$Lesson_Res->LessonType = $LessonType;
							$Lesson_Res->LessonFileType = $LessonFileType;
							$Lesson_Res->LessonFile = $LessonFile;
							$Lesson_Res->LessonFileURL = $LessonFileURL;
							$Lesson_Res->facultyID = $loginfacultyID;
							$Lesson_Res->LessonProgress = $LessonProgress;
							$Lesson_Res->LessonDescription = $LessonDescription;$Lesson_Res->LessonName = $LessonName;
							$Lesson_Res->save(false);
							$Errors = $Lesson_Res->getErrors();
							if(empty($Errors))
							{ 
								$LessonID = $Lesson_Res->LessonID;
								
								$result[0]['data'] = $LessonID;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PROFILEUPDATE_SUCCESS");
								
							}
							else
							{
								//print_r($Errors);
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
							}

						
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
																	

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion); 
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}

	public function actionSetExamTime()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$userexamReadingTimer = "0";
				$userexamListeningTimer = "0";	
				$userexamWritingTimer="0";
				$userexamSpeakingTimer="0";
				$userexamMainTimer="0";
				$userexamReadingSubmitted = "No";
				$userexamListeningSubmitted = "No";	
				$userexamWritingSubmitted="No";
				$userexamSpeakingSubmitted="No";
				
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($examID) )
				{
					$uExam = new Userexams;
					$examRes = Userexams::find()->where("examID = '".$examID."' AND userID=".$loginuserID)->one();
					if(empty($examRes))
						$examRes=$uExam;
					if(!empty($examRes))
					{
						$examRes->examID = $examID;
						$examRes->userID = $loginuserID;
						$examRes->userexamDate  = date('Y-m-d');
						$examRes->userexamReadingTimer = $userexamReadingTimer;
						$examRes->userexamListeningTimer = $userexamListeningTimer;
						$examRes->userexamWritingTimer = $userexamWritingTimer;
						$examRes->userexamSpeakingTimer = $userexamSpeakingTimer;
						$examRes->userexamMainTimer = $userexamMainTimer;
						$examRes->userexamReadingSubmitted = $userexamReadingSubmitted;
						$examRes->userexamListeningSubmitted = $userexamListeningSubmitted;
						$examRes->userexamWritingSubmitted	 = $userexamWritingSubmitted	;
						$examRes->userexamSpeakingSubmitted	 = $userexamSpeakingSubmitted	;
						$examRes->save(false);
						
						$res = Userexams::find()->where("examID = '".$examID."' AND userID=".$loginuserID)->asArray()->all();
						
						$result[0]['data'] = $res[0];
						
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);   
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}
	
	public function actionGetExamTime()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				 
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($examID) )
				{
					$uExam = new Userexams;
					$examRes = Userexams::find()->where("examID = '".$examID."' AND userID=".$loginuserID)->asArray()->all();
					
					if(!empty($examRes))
					{
						 
						//print_r($examRes);die;
						 
						$result[0]['data'] = $examRes[0];
						
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);     
		header("Access-Control-Allow-Headers: Content-Type, origin");
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");
		header("Access-Control-Max-Age: 60");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");	
		return $result; 
		
	}
	
}
