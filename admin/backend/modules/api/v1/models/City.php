<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $cityID
 * @property int $countryID
 * @property int $stateID
 * @property string $cityName
 * @property string $cityRemarks
 * @property string $cityStatus
 * @property string $cityCreatedDate
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryID', 'stateID'], 'integer'],
            [['cityName'], 'required'],
            [['cityStatus'], 'string'],
            [['cityCreatedDate'], 'safe'],
            [['cityName'], 'string', 'max' => 100],
            [['cityRemarks'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cityID' => 'City ID',
            'countryID' => 'Country ID',
            'stateID' => 'State ID',
            'cityName' => 'City Name',
            'cityRemarks' => 'City Remarks',
            'cityStatus' => 'City Status',
            'cityCreatedDate' => 'City Created Date',
        ];
    }
	public function GetCityList($countryID=0,$stateID=0,$pagesize="10",$page="0",$searchWord)
    {
		$Where = ' WHERE cityStatus = "Active"  ';
		if(!empty($countryID))
		{
			$Where .= ' AND countryID="'.$countryID.'"';
		}
		if(!empty($stateID))
		{
			$Where .= ' AND stateID="'.$stateID.'"';
		}
		if(!empty($searchWord))
	   {
		   $Where.= " AND cityName LIKE '".$searchWord."%'";
	   }
		$para = 'SELECT city.*';
		$from = ' FROM city';
        $ORDERBY = " city.cityName";
        $order = " ORDER BY $ORDERBY ASC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
