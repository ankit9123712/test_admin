<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "userlessions".
 *
 * @property int $userlessionID
 * @property int $userID
 * @property int $lessionID
 * @property int $userlessionQuestions
 * @property int $userlessionAnswered
 * @property int $userlessionCorrectAnswers
 * @property int $userlessionWrongAnswers
 */
class Userlessions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userlessions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'lessionID', 'userlessionQuestions', 'userlessionAnswered', 'userlessionCorrectAnswers', 'userlessionWrongAnswers'], 'required'],
            [['userID', 'lessionID', 'userlessionQuestions', 'userlessionAnswered', 'userlessionCorrectAnswers', 'userlessionWrongAnswers'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userlessionID' => 'Userlession ID',
            'userID' => 'User ID',
            'lessionID' => 'Lession ID',
            'userlessionQuestions' => 'Userlession Questions',
            'userlessionAnswered' => 'Userlession Answered',
            'userlessionCorrectAnswers' => 'Userlession Correct Answers',
            'userlessionWrongAnswers' => 'Userlession Wrong Answers',
        ];
    }
	public function getTestSummary($factultyID,$page=0,$pagesize=10,$isPending="Pending",$submittedDate="",$sortKey="")
	{
		$cond="";
		if(!empty($submittedDate))
		{
			$cond.=" AND date(userlessionCreatedDate)='".$submittedDate."'";
		}
		if(!empty($sortKey))
		{
			$orderBy= $sortKey;
		}
		else
		{
			$orderBy= " userlessionID DESC ";
		}
		
		$sql = " Select lessonfaculty.facultyID, lessonfaculty.lessionID ,userlessionQuestions,userlessionAnswered, userFullName,userProfilePicture, LessonName, userlessionCreatedDate, userlessions.userID, userlessionEvaluationDate, LessonNo, LessonType,moduleName, count(answerID) as answers FROM lessonfaculty
		INNER JOIN userlessions on userlessions.lessionID  = lessonfaculty.lessionID
		INNER JOIN useranswers on userlessions.userID = useranswers.userID AND useranswers.lessionID = userlessions.lessionID
		INNER JOIN users on userlessions.userID = users.userID
		INNER JOIN lesson on lesson.LessonID = useranswers.lessionID
		INNER JOIN coursemodule on coursemodule.moduleID = lesson.moduleID
		WHERE lessonfacultyType = 'Grading' AND lesson.moduleID IN (7,9,12,13) AND answerIsVerified='".$isPending."' AND lessonfaculty.facultyID='".$factultyID."' ".$cond."
		Group by userlessions.lessionID, useranswers.userID order by ".$orderBy;
		$LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize; 
		$sql = $sql.$LIMIT.$OFFSET;
//echo $sql;
 $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
		{
		   for($i=0;count($result)>$i; $i++) 
		{ 
			
			$Re[$i] = $result[$i];
		}
		}
		else
		{
			$Re = array();
		}
return $Re;
	}
	
	public function getTestQuestions($factultyID,$lessionID	, $userID, $page=0,$pagesize=10)
	{
		$sql = " Select useranswers.*,questions.queQuestion,questions.queSolution,queType
			FROM 
			useranswers
			INNER JOIN questions on questions.queID = useranswers.queID 
			WHERE useranswers.lessionID	='".$lessionID	."' AND useranswers.userID = '".$userID."'
Group by useranswers.queID ";
//echo $sql;
 $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
		{
		   for($i=0;count($result)>$i; $i++) 
		{ 
			
			$Re[$i] = $result[$i];
		}
		}
		else
		{
			$Re = array();
		}
return $Re;
	}
	public function getTestSummaryCount($factultyID,$page=0,$pagesize=10,$isPending="Pending",$submittedDate="")
	{
		$cond="";
		if(!empty($submittedDate))
		{
			$cond.=" AND date(userlessionCreatedDate)='".$submittedDate."'";
		}
		$sql = " Select lessonfaculty.facultyID, lessonfaculty.lessionID ,userlessionQuestions,userlessionAnswered, userFullName,userProfilePicture, LessonName, userlessionCreatedDate, userlessions.userID, userlessionEvaluationDate, LessonNo, LessonType,moduleName, count(answerID) as answers FROM lessonfaculty
		INNER JOIN userlessions on userlessions.lessionID  = lessonfaculty.lessionID
		INNER JOIN useranswers on userlessions.userID = useranswers.userID AND useranswers.lessionID = userlessions.lessionID
		INNER JOIN users on userlessions.userID = users.userID
		INNER JOIN lesson on lesson.LessonID = useranswers.lessionID
		INNER JOIN coursemodule on coursemodule.moduleID = lesson.moduleID
		WHERE lessonfacultyType = 'Grading' AND lesson.moduleID IN (7,9,12,13)  AND answerIsVerified='Pending' AND lessonfaculty.facultyID='".$factultyID."' ".$cond."
		Group by userlessions.lessionID, useranswers.userID ";
//echo $sql;die;
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		$i=0;
		$Re[$i]["pending"] =0;
		if(count($result)>0)
		{
		    
			$cnt = count($result);
			$Re[$i]["pending"] = $cnt;
		}
		 
		$sql = " Select lessonfaculty.facultyID, lessonfaculty.lessionID ,userlessionQuestions,userlessionAnswered, userFullName,userProfilePicture, LessonName, userlessionCreatedDate, userlessions.userID, userlessionEvaluationDate, LessonNo, LessonType,moduleName, count(answerID) as answers FROM lessonfaculty
		INNER JOIN userlessions on userlessions.lessionID  = lessonfaculty.lessionID
		INNER JOIN useranswers on userlessions.userID = useranswers.userID AND useranswers.lessionID = userlessions.lessionID
		INNER JOIN users on userlessions.userID = users.userID
		INNER JOIN lesson on lesson.LessonID = useranswers.lessionID
		INNER JOIN coursemodule on coursemodule.moduleID = lesson.moduleID
		WHERE lessonfacultyType = 'Grading'  AND lesson.moduleID IN (7,9,12,13) AND answerIsVerified='Verified' AND lessonfaculty.facultyID='".$factultyID."'
		Group by userlessions.lessionID, useranswers.userID ";
//echo $sql;
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		$Re[$i]["completed"]=0;
		if(count($result)>0)
		{
		    
			$cnt = count($result);
			$Re[$i]["completed"] = $cnt;
		}
		
		
		
		return $Re;
	}
	public function updateLessonBands($loginuserID=0,$lessonID=0)
	{
		if(empty($lessonID))
			return true; 
		$lessRes = Lesson::find()->where("LessonID=".$lessonID)->one();
		$moduleID= $lessRes->moduleID;
		$userLessRes = Userlessions::find()->where('lessionID='.$lessonID.' AND userID='.$loginuserID)->one();
//print_r($userLessRes);die;
		if(empty($userLessRes))
			return true;
		
		if($moduleID==6 || $moduleID==8 || $moduleID==10 || $moduleID==11 )
		{
			$queSQL= " select distinct questions.queID from  coursemodule
			inner join questions on coursemodule.moduleID = questions.moduleID inner join useranswers on useranswers.queID = questions.queID where questions.lessionID = ".$lessonID." AND coursemodule.moduleID=".$moduleID;
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand($queSQL);
			$result = $command->queryAll();
			$questionCount = count($result);
			//echo $queSQL;die;
			$ansSQL = "select answerID from  coursemodule inner join questions on 
					coursemodule.moduleID = questions.moduleID inner join useranswers on useranswers.queID = questions.queID where questions.lessionID = ".$lessonID." AND answerIsCorrect='Yes' AND userID='".$loginuserID."' AND coursemodule.moduleID=".$moduleID;
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand($ansSQL);
			$result = $command->queryAll();
			$correctAnsCount = count($result);
			
			//echo $ansSQL."<BR>".$questionCount."<BR>".$correctAnsCount;die;
			$readingPer = round((($correctAnsCount*100)/$questionCount),2);
			$bandRes = Bands::find()->where($readingPer." BETWEEN bandMin AND bandMax")->one();
			
			$readingBand=$bandRes->bandBand;
			
			$userLessRes->userlessionCorrectAnswers	=$correctAnsCount;
			$userLessRes->userlessionBand	=$readingBand;
			$userLessRes->save(false);
	
		}
		
		if($moduleID==9 || $moduleID==13 )
		{
		
		
			/*Writing*/
			$ansSQL = "select avg(  IF(answerGrade > 0, answerGrade, 0)   ) as avgBand from  coursemodule 			
					inner join 	questions on coursemodule.moduleID = questions.moduleID inner join useranswers on useranswers.queID = questions.queID where questions.lessionID = ".$lessonID." AND userID='".$loginuserID."' AND answerIsVerified='Verified'";
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand($ansSQL);
			$result = $command->queryAll();
			$correctAnsCount = count($result);
			//echo $ansSQL;die;
			$writingBand=0;
			if($correctAnsCount>0)
			{	
				//echo 	
				//print_r($result);die;
				if(!empty($result[0]["avgBand"]))
				{
					$writingBand = $result[0]["avgBand"];
					if($writingBand < 9)
					{
						$bandRes = Bands::find()->where("bandBand>=".$writingBand)->one();
						$writingBand = $bandRes->bandBand;
					}	
					else
						$writingBand = 9;
				}
			}
			$userLessRes->userlessionBand	=$writingBand;
			$userLessRes->save(false);
		}
		
		
		
	}

	
}
