<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "userexams".
 *
 * @property int $userexamID
 * @property int $examID
 * @property string $userexamDate
 * @property string $userexamGrade
 * @property int $userexamVerifiedBy
 * @property string $userexamVerifiedDate
 * @property string $userexamVerifiedNotes
 * @property int $userID
 */
class Userexams extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userexams';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['examID', 'userexamDate', 'userexamGrade', 'userexamVerifiedBy', 'userexamVerifiedDate'], 'required'],
            [['examID', 'userexamVerifiedBy', 'userID'], 'integer'],
            [['userexamDate', 'userexamVerifiedDate'], 'safe'],
            [['userexamGrade'], 'string', 'max' => 15],
            [['userexamVerifiedNotes'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userexamID' => 'Userexam ID',
            'examID' => 'Exam ID',
            'userexamDate' => 'Userexam Date',
            'userexamGrade' => 'Userexam Grade',
            'userexamVerifiedBy' => 'Userexam Verified By',
            'userexamVerifiedDate' => 'Userexam Verified Date',
            'userexamVerifiedNotes' => 'Userexam Verified Notes',
            'userID' => 'User ID',
        ];
    }
	public function getTestSummary($factultyID,$page=0,$pagesize=10,$isPending="Pending",$examSubmittedDate="")
	{
		$where="";
		if(!empty($examSubmittedDate))
			$where= " AND DATE(useranswers.answerSubmittedDate)	='".$examSubmittedDate."'";
		
		$sql = " Select faculty.facultyID, userexamDate, userexamGrade, userexamVerifiedBy, userexamVerifiedDate, examName, userexamVerifiedNotes, userexams.userID, userexamEvaluationDate, moduleName, count(answerID) as answers,userexamID, userexams.examID, userFullName,userProfilePicture,userexamReleaseResult FROM userexams
		INNER JOIN users on userexams.userID = users.userID
		INNER JOIN useranswers on userexams.userID = useranswers.userID AND useranswers.examID = userexams.examID
		INNER JOIN exams on exams.examID = useranswers.examID
		INNER JOIN questions on useranswers.queID = questions.queID
		INNER JOIN coursemodule on coursemodule.moduleID = questions.moduleID
		INNER JOIN faculty on userexams.facultyID  = faculty.facultyID
		WHERE answerIsVerified='".$isPending."' AND coursemodule.moduleID IN (7,9,12,13)  AND userexams.facultyID='".$factultyID."'".$where."
		Group by userexams.userexamID, useranswers.userID ORDER BY userexamID DESC ";
		$LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize; 
		$sql = $sql.$LIMIT.$OFFSET;
//echo $sql;
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
		{
		   for($i=0;count($result)>$i; $i++) 
		{ 
			
			$Re[$i] = $result[$i];
		}
		}
		else
		{
			$Re = array();
		}
		return $Re;
	}
	
	public function getTestSummaryCount($factultyID,$page=0,$pagesize=10,$isPending="Pending",$examSubmittedDate="")
	{
		$where="";
		if(!empty($examSubmittedDate))
			$where= " AND DATE(useranswers.answerSubmittedDate)	='".$examSubmittedDate."'";
		
		$sql = " Select faculty.facultyID, userexamDate, userexamGrade, userexamVerifiedBy, userexamVerifiedDate, examName, userexamVerifiedNotes, userexams.userID, userexamEvaluationDate, moduleName, count(answerID) as answers,userexamID,userFullName FROM userexams
		INNER JOIN users on userexams.userID = users.userID
		INNER JOIN useranswers on userexams.userID = useranswers.userID AND useranswers.examID = userexams.examID
		INNER JOIN exams on exams.examID = useranswers.examID
		INNER JOIN questions on useranswers.queID = questions.queID
		INNER JOIN coursemodule on coursemodule.moduleID = questions.moduleID
		INNER JOIN faculty on userexams.facultyID  = faculty.facultyID
		WHERE answerIsVerified='Pending'  AND userexams.facultyID='".$factultyID."'".$where."
		Group by userexams.userexamID, useranswers.userID ";
//echo $sql;die;
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		$Re[0]["pending"] ="0";
		if(count($result)>0)
		{
		    $cnt=count($result);
			
			$Re[0]["pending"] = $cnt ;
		}
		 
		$sql = " Select faculty.facultyID, userexamDate, userexamGrade, userexamVerifiedBy, userexamVerifiedDate, examName, userexamVerifiedNotes, userexams.userID, userexamEvaluationDate, moduleName, count(answerID) as answers,userexamID, userexams.examID, userFullName,userProfilePicture FROM userexams
		INNER JOIN users on userexams.userID = users.userID
		INNER JOIN useranswers on userexams.userID = useranswers.userID AND useranswers.examID = userexams.examID
		INNER JOIN exams on exams.examID = useranswers.examID
		INNER JOIN questions on useranswers.queID = questions.queID
		INNER JOIN coursemodule on coursemodule.moduleID = questions.moduleID
		INNER JOIN faculty on userexams.facultyID  = faculty.facultyID
		WHERE answerIsVerified='Verified' AND coursemodule.moduleID IN (7,9,12,13)  AND userexams.facultyID='".$factultyID."'".$where."
		Group by userexams.userexamID, useranswers.userID ORDER BY userexamID DESC ";
//echo $sql;
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		$Re[0]["completed"] =0;
		if(count($result)>0)
		{
		    
			$cnt=count($result);
			$Re[0]["completed"] = $cnt;
		}
		 
		
		
		return $Re;
	}
	public function getTestQuestions($factultyID,$examID, $userID, $page=0,$pagesize=10,$evalute="Pending",$examSubmittedDate)
	{
		$where=" WHERE 1 =1  AND questions.moduleID IN (7,9,12,13)  ";
		if(!empty($examID))
			$where.= " AND useranswers.examID	=".$examID;
		if(!empty($userID))
			$where.= " AND useranswers.userID	=".$userID;
		if(!empty($evalute))
			$where.= " AND useranswers.answerIsVerified	='".$evalute."'";
		if(!empty($examSubmittedDate))
			$where.= " AND DATE(useranswers.answerSubmittedDate)	='".$examSubmittedDate."'";
		
		$sql = " Select useranswers.*,questions.queQuestion,questions.queSolution,queType
			FROM 
			useranswers
			INNER JOIN questions on questions.queID = useranswers.queID ".$where." Group by useranswers.queID ";
//echo $sql;
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
		{
		   for($i=0;count($result)>$i; $i++) 
		{ 
			
			$Re[$i] = $result[$i];
		}
		}
		else
		{
			$Re = array();
		}
		return $Re;
	}
	
	public function updateExamBands($loginuserID=0,$examID=0)
	{
		if(empty($examID))
			return true;
		 /*
		 moduleID	moduleName
			8		Reading
			9		Writing
			11		Listening
			13		Speaking
		 */
		//module's correct answers/total question and generate % and find Band from that 
		
		$listeningBand=0;
		$readingBand=0;
		$writingBand=0;
		$speakingBand=0;
		/*reading */
		$queSQL= " select questions.queID from  coursemodule
				inner join questions on coursemodule.moduleID = questions.moduleID inner join useranswers on useranswers.queID = questions.queID where questions.examID = ".$examID." AND coursemodule.moduleID= 8";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($queSQL);
        $result = $command->queryAll();
		$questionCount = count($result);
		
		$ansSQL = "select answerID from  coursemodule inner join questions on 
				coursemodule.moduleID = questions.moduleID inner join useranswers on useranswers.queID = questions.queID where questions.examID = ".$examID." AND answerIsCorrect='Yes' AND userID='".$loginuserID."' AND coursemodule.moduleID= 8";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($ansSQL);
        $result = $command->queryAll();
		$correctAnsCount = count($result);
		if($correctAnsCount > 0)
		{
			$readingPer = round((($correctAnsCount*100)/$questionCount),2);


			$bandRes = Bands::find()->where($readingPer." BETWEEN bandMin AND bandMax")->one();
			
				$readingBand=$bandRes->bandBand;
			$examRes = Userexams::find()->where('examID='.$examID.' AND userID='.$loginuserID)->one();
			$examRes->userexamReadingBand=$readingBand;

			$examRes->save(false);
		}
		/*Listing*/
		$queSQL= " select questions.queID ,coursemodule.moduleID from  coursemodule
				inner join questions on coursemodule.moduleID = questions.moduleID inner join useranswers on useranswers.queID = questions.queID where questions.examID = ".$examID." AND coursemodule.moduleID= 11";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($queSQL);
        $result = $command->queryAll();
		$questionCount = count($result);
		
		$ansSQL = "select answerID from  coursemodule inner join questions on 
				coursemodule.moduleID = questions.moduleID inner join useranswers on useranswers.queID = questions.queID where questions.examID = ".$examID." AND answerIsCorrect='Yes' AND userID='".$loginuserID."' AND coursemodule.moduleID= 11";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($ansSQL);
        $result = $command->queryAll();
		$correctAnsCount = count($result);
		if($correctAnsCount > 0)
		{	$listenPer = round((($correctAnsCount*100)/$questionCount),2);
		
			$bandRes = Bands::find()->where($listenPer." BETWEEN bandMin AND bandMax")->one();
			
			$listeningBand=$bandRes->bandBand;
			
			$examRes = Userexams::find()->where('examID='.$examID.' AND userID='.$loginuserID)->one();
			 
			$examRes->userexamListeningBand=$listeningBand;
			$examRes->save(false);
		}
		/*Writing*/
		$ansSQL = "select avg(  IF(answerGrade > 0, answerGrade, 0)   ) as avgBand from  coursemodule 			
				inner join 	questions on coursemodule.moduleID = questions.moduleID inner join useranswers on useranswers.queID = questions.queID where questions.examID = ".$examID." AND coursemodule.moduleID=9 AND userID='".$loginuserID."' AND answerIsVerified='Verified'";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($ansSQL);
        $result = $command->queryAll();
		$correctAnsCount = count($result);
		$writingBand=0;
		if($correctAnsCount>0)
		{
			if(!empty($result[0]["avgBand"]))
			{
				$writingBand = $result[0]["avgBand"];
				if($writingBand < 9)
				{
					$bandRes = Bands::find()->where("bandBand>=".$writingBand)->one();
					$writingBand = $bandRes->bandBand;
				}	
				else
					$writingBand = 9;
			}
			$examRes = Userexams::find()->where('examID='.$examID.' AND userID='.$loginuserID)->one();
			 
			$examRes->userexamWritingBand=$writingBand;
			$examRes->save(false);
		}
		//$examRes->userexamWritingBand=$writingBand;
		//$examRes->save(false);
		/*Speaking*/
		$ansSQL = "select avg(  IF(answerGrade > 0, answerGrade, 0)   ) as avgBand from  coursemodule 			
				inner join 	questions on coursemodule.moduleID = questions.moduleID inner join useranswers on useranswers.queID = questions.queID where questions.examID = ".$examID."  AND coursemodule.moduleID=13 AND userID='".$loginuserID."' AND answerIsVerified='Verified'";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($ansSQL);
        $result = $command->queryAll();
		$correctAnsCount = count($result);
		$speakingBand=0;
		if($correctAnsCount>0)
		{
			if(!empty($result[0]["avgBand"]))
			{
				$speakingBand = $result[0]["avgBand"];
				if($speakingBand < 9)
				{
					$bandRes = Bands::find()->where("bandBand>=".$speakingBand)->one();
					$speakingBand = $bandRes->bandBand;
				}	
				else
					$speakingBand = 9;
			}
			$examRes = Userexams::find()->where('examID='.$examID.' AND userID='.$loginuserID)->one();
			$examRes->userexamSpeakingBand=$speakingBand;
			$examRes->save(false);
		}
		
		/*update Band in userexam */
		$totalBand = $readingBand+$writingBand+$listeningBand+$speakingBand;
		$totalAvgBand = $totalBand/4;
		$finalBand=0;
		if($totalAvgBand < 9)
		{
			$bandRes = Bands::find()->where("bandBand>=".$totalAvgBand)->one();
			$finalBand = $bandRes->bandBand;
		}	
		else
			$finalBand = 9;
		
		if($writingBand>0 && $speakingBand>0)
		{
			$examRes->userexamOverallBand=$finalBand;
			$examRes->save(false);
		}
		
		
		
	}
}
