<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "messagecenter".
 *
 * @property int $messagecenterID
 * @property string $messagecenterType
 * @property string $messagecenterTitle
 * @property string $messagecenter
 * @property string $messagecenterTo
 * @property string $messagecenterStartDate
 * @property string $messagecenterEndDate
 * @property string $messagecenterStatus
 * @property string $messagecenterCreatedDate
 */
class Messagecenter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'messagecenter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['messagecenterType', 'messagecenterTitle', 'messagecenter', 'messagecenterStartDate', 'messagecenterEndDate'], 'required'],
            [['messagecenterType', 'messagecenterTo', 'messagecenterStatus'], 'string'],
            [['messagecenterStartDate', 'messagecenterEndDate', 'messagecenterCreatedDate'], 'safe'],
            [['messagecenterTitle'], 'string', 'max' => 100],
            [['messagecenter'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'messagecenterID' => 'Messagecenter ID',
            'messagecenterType' => 'Messagecenter Type',
            'messagecenterTitle' => 'Messagecenter Title',
            'messagecenter' => 'Messagecenter',
            'messagecenterTo' => 'Messagecenter To',
            'messagecenterStartDate' => 'Messagecenter Start Date',
            'messagecenterEndDate' => 'Messagecenter End Date',
            'messagecenterStatus' => 'Messagecenter Status',
            'messagecenterCreatedDate' => 'Messagecenter Created Date',
        ];
    }
		public function GetMessagecenterList($messagecenterType,$messagecenterTo,$page="0",$pagesize="10")
    {
		$Where = " WHERE messagecenterStatus='Active' AND messagecenterStartDate<='".date('Y-m-d')."' AND messagecenterEndDate>='".date('Y-m-d')."'";
		if(!empty($messagecenterTo))
		{
			$Where .= " AND (messagecenterTo='".$messagecenterTo." Users' OR messagecenterTo='All')";
		}
		if(!empty($messagecenterType))
		{
			$Where .= ' AND messagecenterType="'.$messagecenterType.'"';
		}
		 
		$para = 'SELECT messagecenter.*';
		$from = ' FROM messagecenter';
        $ORDERBY = " messagecenter.messagecenterEndDate";
        $order = " ORDER BY $ORDERBY ASC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
