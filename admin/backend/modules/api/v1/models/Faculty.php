<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "faculty".
 *
 * @property int $facultyID
 * @property string $facultyFullName
 * @property string $facultyMobile
 * @property string $facultyEmail
 * @property string $facultyPassword MD5 Encrypted
 * @property string $facultyBirthDate
 * @property string $facultyGender
 * @property string $facultyDOB
 * @property string $facultyJoinDate
 * @property int $moduleID
 * @property string $facultyProfilePicture
 * @property string $facultyAddress
 * @property string $facultyPincode
 * @property int $countryID
 * @property int $stateID
 * @property int $cityID
 * @property string $facultyDeviceType
 * @property string $facultyDeviceID IP if its web
 * @property string $facultyReferKey
 * @property string $facultyVerified
 * @property string $facultyStatus
 * @property string $facultyCreatedDate
 * @property string $facultyOTP
 * @property string $facultySignupOTPVerified
 */
class Faculty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faculty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['facultyFullName', 'facultyMobile', 'facultyEmail', 'facultyPassword', 'facultyBirthDate', 'facultyGender', 'facultyDOB', 'facultyJoinDate', 'moduleID', 'facultyPincode', 'countryID', 'stateID', 'cityID', 'facultyDeviceType', 'facultyDeviceID'], 'required'],
            [['facultyBirthDate', 'facultyDOB', 'facultyJoinDate', 'facultyCreatedDate'], 'safe'],
            [['facultyGender', 'facultyVerified', 'facultyStatus', 'facultySignupOTPVerified'], 'string'],
            [['moduleID', 'countryID', 'stateID', 'cityID'], 'integer'],
            [['facultyFullName', 'facultyEmail', 'facultyPassword', 'facultyProfilePicture'], 'string', 'max' => 100],
            [['facultyMobile', 'facultyPincode', 'facultyDeviceType', 'facultyReferKey'], 'string', 'max' => 10],
            [['facultyAddress'], 'string', 'max' => 300],
            [['facultyDeviceID'], 'string', 'max' => 500],
            [['facultyOTP'], 'string', 'max' => 6],
            [['facultyMobile'], 'unique'],
            [['facultyEmail'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'facultyID' => 'Faculty ID',
            'facultyFullName' => 'Faculty Full Name',
            'facultyMobile' => 'Faculty Mobile',
            'facultyEmail' => 'Faculty Email',
            'facultyPassword' => 'Faculty Password',
            'facultyBirthDate' => 'Faculty Birth Date',
            'facultyGender' => 'Faculty Gender',
            'facultyDOB' => 'Faculty Dob',
            'facultyJoinDate' => 'Faculty Join Date',
            'moduleID' => 'Module ID',
            'facultyProfilePicture' => 'Faculty Profile Picture',
            'facultyAddress' => 'Faculty Address',
            'facultyPincode' => 'Faculty Pincode',
            'countryID' => 'Country ID',
            'stateID' => 'State ID',
            'cityID' => 'City ID',
            'facultyDeviceType' => 'Faculty Device Type',
            'facultyDeviceID' => 'Faculty Device ID',
            'facultyReferKey' => 'Faculty Refer Key',
            'facultyVerified' => 'Faculty Verified',
            'facultyStatus' => 'Faculty Status',
            'facultyCreatedDate' => 'Faculty Created Date',
            'facultyOTP' => 'Faculty Otp',
            'facultySignupOTPVerified' => 'Faculty Signup Otp Verified',
        ];
    }
	 public function CheckFacultyDetailsForDuplication($facultyEmail="",$facultyMobile="",$facultyID="")
    {
        if(!empty($facultyEmail))
        {
            if(empty($facultyID))
            {
                $Exist_facultyEmail = Faculty::find()->where("facultyEmail = '".$facultyEmail."'")->all();
            }
            else 
            {
                $Exist_facultyEmail = Faculty::find()->where("facultyEmail = '".$facultyEmail."' and facultyID <> '".$facultyID."'")->all();
            }
           
            if(count($Exist_facultyEmail) == 0)
            {
                if(!empty($facultyMobile))
                {
                    if(empty($facultyID))
                    {
                        $Exist_facultyMobile = Faculty::find()->where("facultyMobile = '".$facultyMobile."'")->all();
                    }
                    else 
                    {
                        $Exist_facultyMobile = Faculty::find()->where("facultyMobile = '".$facultyMobile."' and facultyID <> '".$facultyID."'")->all();
                    }

                    if(count($Exist_facultyMobile) == 0)
                    {
                       return 'true';
                    }
                    else
                    {
                        return 'facultyMobile';
                    }
                }
                else
                {
                    return 'true';
                }
            }
            else
            {
                return 'facultyEmail';
            }
        }
        else
        {
            if(!empty($facultyMobile))
            {
                if(empty($facultyID))
                {
                    $Exist_facultyMobile = Faculty::find()->where("facultyMobile = '".$facultyMobile."'")->all();
                }
                else 
                {
                    $Exist_facultyMobile = Faculty::find()->where("facultyMobile = '".$facultyMobile."' and facultyID <> '".$facultyID."'")->all();
                }

                if(count($Exist_facultyMobile) == 0)
                {
                    return 'true';
                }
                else
                {
                    return 'facultyMobile';
                }
            }
            else
            {
               return 'true';
            }
        }
    }

    public function GetFacultyDetails($FacultyID)
    {
      // $Where = 'facultys.facultyStatus = "Active" AND facultyaddress.addressType = "Registered"';
	   $Where = ' WHERE faculty.facultyStatus = "Active" ';
       $Re = array();
      
       if(!empty($FacultyID))
       {
           $Where .= ' AND faculty.FacultyID = "'.$FacultyID.'"';
       }
	    
		$para = 'SELECT faculty.*,countryName,cityName,stateName  ';
		$from = ' FROM faculty';
        $ORDERBY = " faculty.facultyID";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = "";//" LIMIT $pagesize";
        $OFFSET ="";// " OFFSET ".$page*$pagesize;      
		$join = '   left join country on country.countryID = faculty.countryID					
					left join state on state.stateID= faculty.stateID  
					left join city on city.cityID= faculty.cityID   
					';		

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
	   
       if(count($result)>0)
       {
		   //$Facultyubscriptions = new Facultyubscriptions;
		   $Settings = new Settings;
           for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				//$resSubscription = $Facultyubscriptions->GetFacultySubscriptionplanList($FacultyID,0,10);
				// $Re[$i]['mySubscriptions'] = $resSubscription;
				 $Re[$i]['settings'] = $Settings->GetFacultySettings();
			
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re;   
    }
}
