<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "lessonfiles".
 *
 * @property int $LessonfileID
 * @property int $LessonID
 * @property string $LessonfileFileType
 * @property string $LessonfileFile
 * @property string $Lessonfile
 * @property string $LessonfileStatus
 */
class Lessonfiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lessonfiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LessonID', 'LessonfileFileType', 'LessonfileFile', 'LessonfileStatus'], 'required'],
            [['LessonID'], 'integer'],
            [['LessonfileFileType', 'LessonfileStatus'], 'string'],
            [['LessonfileFile', 'Lessonfile'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LessonfileID' => 'Lessonfile ID',
            'LessonID' => 'Lesson ID',
            'LessonfileFileType' => 'Lessonfile File Type',
            'LessonfileFile' => 'Lessonfile File',
            'Lessonfile' => 'Lessonfile',
            'LessonfileStatus' => 'Lessonfile Status',
        ];
    }
	public function GetChapterfilesList($LessonID=0,$chapterfileFileType="")
    {
	   $where='LessonfileStatus = "Active"'	;
	   if(!empty($LessonID))
	   {
		   $where.=' AND LessonID="'.$LessonID.'"';
	   }
	  // $where.=' AND chapterfileFileType="'.$chapterfileFileType.'"';
	   $result = Lessonfiles::find()->select('*')->where($where)->asArray()->all();
	   if(count($result)>0)
       {
           //$Apilog = new Apilog;
		   //$ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				//$Re[$i]['chapterfileFile'] = $ImagePathArray['chapterfileFile'].$result[$i]['chapterfileFile'];
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
