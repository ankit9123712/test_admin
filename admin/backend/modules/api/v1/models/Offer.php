<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "offer".
 *
 * @property int $offerID
 * @property string $offerName
 * @property string $offerImage
 * @property string $offerDiscountType
 * @property float $offerDiscountValue
 * @property string $offerStartDate
 * @property string $offerEndDate
 * @property string $offerCodeUse
 * @property int $offerMaxLimit
 * @property string $offerCodeType
 * @property int $offerCodeQty
 * @property float $offerSalePrice
 * @property string $offerDescription
 * @property string $offerStatus
 * @property string $offerCreatedDate
 */
class Offer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['offerName', 'offerImage', 'offerDiscountType', 'offerDiscountValue', 'offerStartDate', 'offerEndDate', 'offerCodeUse', 'offerMaxLimit', 'offerCodeType', 'offerCodeQty', 'offerSalePrice', 'offerDescription'], 'required'],
            [['offerDiscountType', 'offerCodeUse', 'offerCodeType', 'offerStatus'], 'string'],
            [['offerDiscountValue', 'offerSalePrice'], 'number'],
            [['offerStartDate', 'offerEndDate', 'offerCreatedDate'], 'safe'],
            [['offerMaxLimit', 'offerCodeQty'], 'integer'],
            [['offerName'], 'string', 'max' => 100],
            [['offerImage'], 'string', 'max' => 300],
            [['offerDescription'], 'string', 'max' => 2000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'offerID' => 'Offer ID',
            'offerName' => 'Offer Name',
            'offerImage' => 'Offer Image',
            'offerDiscountType' => 'Offer Discount Type',
            'offerDiscountValue' => 'Offer Discount Value',
            'offerStartDate' => 'Offer Start Date',
            'offerEndDate' => 'Offer End Date',
            'offerCodeUse' => 'Offer Code Use',
            'offerMaxLimit' => 'Offer Max Limit',
            'offerCodeType' => 'Offer Code Type',
            'offerCodeQty' => 'Offer Code Qty',
            'offerSalePrice' => 'Offer Sale Price',
            'offerDescription' => 'Offer Description',
            'offerStatus' => 'Offer Status',
            'offerCreatedDate' => 'Offer Created Date',
        ];
    }
	public function GetOffer($searchkeyword="")
	{
		
		$Where = ' offerExclusive="No" AND offer.offerStatus = "Active" AND (DATE(NOW()) BETWEEN `offer`.`offerStartDate` AND `offer`.`offerEndDate`)';
		$Re = array();
		
		if(!empty($searchkeyword))
		{
			$Where .= ' AND (offerName LIKE "%'.$search_keyword.'%")';
		}
		
		$result = array();
		$query = new \yii\db\Query;
		$result = $query->select('*')
		->from('offer')
		->where($Where)
		->GROUPBY('offer.offerID')
		->ORDERBY('offer.offerID ASC')
		->all();
		//echo $query->createCommand()->sql; die;
		if(count($result)>0)
		{	
			$Re = $result;
		}	
		else
		{
			$Re = $result;
		}
		return $Re;   
	}
	public function VerifyOffer($offerID,$amount)
	{
		$Apilog = new Apilog;
		$Couponoffer_Res = Offer::find()->where('offerID = "'.$offerID.'" AND offerStatus = "Active"')->asArray()->one();
		if(!empty($Couponoffer_Res))
		{
			//print_r($Couponoffer_Res); die;
			$currentDate = date("Y-m-d");
			if($currentDate >= $Couponoffer_Res['offerStartDate'])
			{
				if($Couponoffer_Res['offerEndDate'] >= $currentDate)
				{
					if($Couponoffer_Res['offerDiscountType']=="Percentage")
					{
						$result[0]['status'] = 'true';
						$result[0]['message'] = '';	
						$result[0]['data'][0]['discount'] =  round((($Couponoffer_Res['offerDiscountValue'] * $amount) /100),2);
					}
					else
					{
						$result[0]['status'] = 'true';
						$result[0]['message'] = '';	
						$result[0]['data'][0]['discount'] =  $Couponoffer_Res['offerDiscountValue'];
					}
					
					
					
					
					
				}
				else
				{
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OFFEREXPIRED");
				}
			}
			else
			{
				$result[0]['status'] = 'false';
				$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OFFERNOTSTARTED");
			}
		}
		else
		{
			$result[0]['status'] = 'false';
			$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","COUPONCODE_INVALID");
		}
		return $result;
		
	}
}
