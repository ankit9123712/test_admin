<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "ieltsfiles".
 *
 * @property int $ieltsfileID
 * @property string $ieltsfileType
 * @property string $ieltsfileName
 * @property string $ieltsfileStatus
 */
class Ieltsfiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ieltsfiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ieltsfileType', 'ieltsfileName'], 'required'],
            [['ieltsfileType', 'ieltsfileStatus'], 'string'],
            [['ieltsfileName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ieltsfileID' => 'Ieltsfile ID',
            'ieltsfileType' => 'Ieltsfile Type',
            'ieltsfileName' => 'Ieltsfile Name',
            'ieltsfileStatus' => 'Ieltsfile Status',
        ];
    }
	public function GetFilesList()
    {
	   $where = " ieltsfileStatus = 'Active'";
	    
	   
	   $result = Ieltsfiles::find()->select('*')->where($where)->orderby('ieltsfileID DESC')->asArray()->all();
	   if(count($result)>0)
       {
           //$Apilog = new Apilog;
		   //$ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
             //   $Re[$i]['countryFlagImage'] = $ImagePathArray['countryFlagImage'].$result[$i]['countryFlagImage'];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
