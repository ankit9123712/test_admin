<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "livesessiondetails".
 *
 * @property int $sessiondetailID
 * @property int $liveID
 * @property string $sessiondetailType
 * @property int $lessonID
 * @property int $moduleID
 * @property string|null $sessiondetailURL
 */
class Livesessiondetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'livesessiondetails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sessiondetailID', 'liveID', 'sessiondetailType'], 'required'],
            [['sessiondetailID', 'liveID', 'lessonID', 'moduleID'], 'integer'],
            [['sessiondetailType'], 'string'],
            [['sessiondetailURL'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sessiondetailID' => 'Sessiondetail ID',
            'liveID' => 'Live ID',
            'sessiondetailType' => 'Sessiondetail Type',
            'lessonID' => 'Lesson ID',
            'moduleID' => 'Module ID',
            'sessiondetailURL' => 'Sessiondetail Url',
        ];
    }
	public function GetDetailsList($liveID)
    {
	   $Where = 	' WHERE 1=1  ';//AND coursePublished="Yes"';
	   
	   if(!empty($liveID))
		   $Where.= ' AND livesessiondetails.liveID = '.$liveID;
	      
	   
	    $para = 'SELECT livesessiondetails.*,moduleName, LessonName,LessonNo	';				
         
		$from = ' FROM livesessiondetails';
		$ORDERBY = " sessiondetailID ";
		$groupby = " GROUP BY sessiondetailID";
		$order = " ORDER BY $ORDERBY ASC ";
		$innerJoin = " LEFT join coursemodule on coursemodule.moduleID = livesessiondetails.moduleID LEFT join lesson on lesson.LessonID = livesessiondetails.lessonID ";
		
		
		$sql = $para.$from.$innerJoin.$Where.$groupby.$order;
		
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$result = $command->queryAll();
	   
	   
		
	   /*$result = Livesessions::find()->select('*')->join('inner join','coursemodule','coursemodule.moduleID')->where($where)->limit($pagesize)->orderby('liveStartDate DESC, liveStartTime ASC')->offset($pagesize*$page)->asArray()->all();*/
	   if(count($result)>0)
       {
           
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
