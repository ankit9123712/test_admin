<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "monkchatmessages".
 *
 * @property int $monkchatmessID
 * @property int $monkchatID
 * @property int $userID
 * @property string $monkchatmessMessage
 * @property string $monkchatmessAttachment
 * @property string $monkchatmessCreatedDateTime
 */
class Monkchatmessages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monkchatmessages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['monkchatID', 'monkchatmessMessage'], 'required'],
            [['monkchatID', 'userID'], 'integer'],
            [['monkchatmessCreatedDateTime'], 'safe'],
            [['monkchatmessMessage', 'monkchatmessAttachment'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'monkchatmessID' => 'Monkchatmess ID',
            'monkchatID' => 'Monkchat ID',
            'userID' => 'User ID',
            'monkchatmessMessage' => 'Monkchatmess Message',
            'monkchatmessAttachment' => 'Monkchatmess Attachment',
            'monkchatmessCreatedDateTime' => 'Monkchatmess Created Date Time',
        ];
    }
	public function GetMonkchatMessageList($monkchatID,$page="0",$pagesize="10")
    {
		$Re = array();
		$Where = " WHERE monkchatStatus='Active' ";
		if(!empty($monkchatID))
		{
			$Where .= " AND (monkchatmessages.monkchatID='".$monkchatID."')";
		}
		 
		 
		$para = 'SELECT monkchatmessages.*,creator.userFullName as creatorName,creator.userProfilePicture as creatorPicture, users.userFullName, users.userProfilePicture, users.userUniChoice1, users.userUniChoice2, users.userUniChoice3, users.userIELTSMonth, users.userIELTSYear, users.userIELTSCity	';
		$from = ' FROM monkchatmessages';
		$join = ' inner join monkchat on monkchat.monkchatID = monkchatmessages.monkchatID  
		LEFT join users creator  on creator.userID = monkchat.userID 
		LEFT join users on users.userID = monkchatmessages.userID ';
        $ORDERBY = " monkchatmessages.monkchatmessCreatedDateTime";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
       {
		       for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				
			
           }
	   }
	   else
	   {
		   $Re = array();
	   }
   
		
		
	   
       return $Re; 
    }
}
