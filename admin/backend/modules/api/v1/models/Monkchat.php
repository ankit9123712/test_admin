<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "monkchat".
 *
 * @property int $monkchatID
 * @property string $monkchatName
 * @property string $monkchatSubject
 * @property string $monkchatDescription
 * @property string $monkchatFiles
 * @property string $monkchatPic
 * @property string $monkchatStatus
 * @property string $monkchatCreatedDate
 * @property int $monkcatID
 */
class Monkchat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monkchat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['monkchatName', 'monkchatSubject', 'monkchatDescription', 'monkchatFiles', 'monkchatPic', 'monkcatID'], 'required'],
            [['monkchatDescription', 'monkchatStatus'], 'string'],
            [['monkchatCreatedDate'], 'safe'],
            [['monkcatID'], 'integer'],
            [['monkchatName', 'monkchatSubject'], 'string', 'max' => 100],
            [['monkchatFiles'], 'string', 'max' => 4000],
            [['monkchatPic'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'monkchatID' => 'Monkchat ID',
            'monkchatName' => 'Monkchat Name',
            'monkchatSubject' => 'Monkchat Subject',
            'monkchatDescription' => 'Monkchat Description',
            'monkchatFiles' => 'Monkchat Files',
            'monkchatPic' => 'Monkchat Pic',
            'monkchatStatus' => 'Monkchat Status',
            'monkchatCreatedDate' => 'Monkchat Created Date',
            'monkcatID' => 'Monkcat ID',
        ];
    }
	public function GetMonkchatList($monkcatID=0,$monkchatID=0,$page="0",$pagesize="10")
    {
		$Re = array();
		$Where = " WHERE monkchatStatus='Active' AND monkcatStatus='Active' ";
		if(!empty($monkchatID))
		{
			$Where .= " AND (monkchat.monkchatID='".$monkchatID."')";
		}
		if(!empty($monkcatID))
		{
			$Where .= ' AND monkchat.monkcatID="'.$monkcatID.'"';
		}
		 
		$para = 'SELECT monkchat.*,monkcategory.monkcatName, userFullName, userProfilePicture, userUniChoice1, userUniChoice2,userUniChoice3, userIELTSMonth, userIELTSYear,userIELTSCity	';
		$from = ' FROM monkchat';
		$join = ' inner join monkcategory on monkcategory.monkcatID	 = monkchat.monkcatID	 
		inner join users on users.userID = monkchat.userID';
        $ORDERBY = " monkchat.monkchatCreatedDate";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
       {
		       for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				//$resSubscription = $Usersubscriptions->GetUserSubscriptionplanList($UserID,0,10);
				// $Re[$i]['mySubscriptions'] = $resSubscription;
				 $Re[$i]['count'] = $countMsgs = Monkchatmessages::find()->where('monkchatID='.$result[$i]["monkchatID"])->count();
			
           }
	   }
	   else
	   {
		   $Re = array();
	   }
   
		
		
	   
       return $Re; 
    }
}
