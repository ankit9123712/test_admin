<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $userID
 * @property string $userFullName
 * @property string $userCountryCode
 * @property string $userMobile
 * @property string $userEmail
 * @property string $userPassword MD5 Encrypted
 * @property int $userAge
 * @property string $userGender
 * @property int $deletemoduleID
 * @property int $countryID
 * @property int $stateID
 * @property int $cityID
 * @property string $userProfilePicture
 * @property string $userUniChoice1
 * @property string $userUniChoice2
 * @property string $userUniChoice3
 * @property string $userLiveLectureNotify
 * @property string $userTestNotify
 * @property string $userResultNotify
 * @property string $userAddress
 * @property int $languageID
 * @property string $userDeviceType
 * @property string $userDeviceID IP if its web
 * @property string $userReferKey
 * @property string $userVerified
 * @property string $userNewsNotify
 * @property string $userEventsNotify
 * @property string $userNoticeNotify
 * @property string $userAssignmentNotify
 * @property string $userForumNotify
 * @property string $userAdminNotify
 * @property string $userType
 * @property string $userStatus
 * @property string $userOTP
 * @property string $userCreatedDate
 * @property string $userSignupOTPVerified
 * @property string $userSecurityToken
 * @property string $useTokenExpirtyDate
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userFullName', 'userCountryCode', 'userMobile', 'userEmail', 'userPassword', 'userAge', 'userGender', 'deletemoduleID', 'countryID', 'stateID', 'cityID', 'languageID', 'userDeviceType', 'userDeviceID'], 'required'],
            [['userAge', 'deletemoduleID', 'countryID', 'stateID', 'cityID', 'languageID'], 'integer'],
            [['userGender', 'userLiveLectureNotify', 'userTestNotify', 'userResultNotify', 'userVerified', 'userNewsNotify', 'userEventsNotify', 'userNoticeNotify', 'userAssignmentNotify', 'userForumNotify', 'userAdminNotify', 'userType', 'userStatus', 'userSignupOTPVerified'], 'string'],
            [['userCreatedDate', 'useTokenExpirtyDate'], 'safe'],
            [['userFullName', 'userEmail', 'userPassword', 'userProfilePicture', 'userUniChoice1', 'userUniChoice2', 'userUniChoice3', 'userSecurityToken'], 'string', 'max' => 100],
            [['userCountryCode'], 'string', 'max' => 5],
            [['userMobile', 'userDeviceType', 'userReferKey'], 'string', 'max' => 10],
            [['userAddress'], 'string', 'max' => 300],
            [['userDeviceID'], 'string', 'max' => 500],
            [['userOTP'], 'string', 'max' => 6],
            [['userMobile'], 'unique'],
            [['userEmail'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userID' => 'User ID',
            'userFullName' => 'User Full Name',
            'userCountryCode' => 'User Country Code',
            'userMobile' => 'User Mobile',
            'userEmail' => 'User Email',
            'userPassword' => 'User Password',
            'userAge' => 'User Age',
            'userGender' => 'User Gender',
            'deletemoduleID' => 'Deletemodule ID',
            'countryID' => 'Country ID',
            'stateID' => 'State ID',
            'cityID' => 'City ID',
            'userProfilePicture' => 'User Profile Picture',
            'userUniChoice1' => 'User Uni Choice1',
            'userUniChoice2' => 'User Uni Choice2',
            'userUniChoice3' => 'User Uni Choice3',
            'userLiveLectureNotify' => 'User Live Lecture Notify',
            'userTestNotify' => 'User Test Notify',
            'userResultNotify' => 'User Result Notify',
            'userAddress' => 'User Address',
            'languageID' => 'Language ID',
            'userDeviceType' => 'User Device Type',
            'userDeviceID' => 'User Device ID',
            'userReferKey' => 'User Refer Key',
            'userVerified' => 'User Verified',
            'userNewsNotify' => 'User News Notify',
            'userEventsNotify' => 'User Events Notify',
            'userNoticeNotify' => 'User Notice Notify',
            'userAssignmentNotify' => 'User Assignment Notify',
            'userForumNotify' => 'User Forum Notify',
            'userAdminNotify' => 'User Admin Notify',
            'userType' => 'User Type',
            'userStatus' => 'User Status',
            'userOTP' => 'User Otp',
            'userCreatedDate' => 'User Created Date',
            'userSignupOTPVerified' => 'User Signup Otp Verified',
            'userSecurityToken' => 'User Security Token',
            'useTokenExpirtyDate' => 'Use Token Expirty Date',
        ];
    }
	 public function CheckUserDetailsForDuplication($userEmail="",$userMobile="",$userID="")
    {
        if(!empty($userEmail))
        {
            if(empty($userID))
            {
                $Exist_userEmail = Users::find()->where("userEmail = '".$userEmail."'")->all();
            }
            else 
            {
                $Exist_userEmail = Users::find()->where("userEmail = '".$userEmail."' and userID <> '".$userID."'")->all();
            }
           
            if(count($Exist_userEmail) == 0)
            {
                if(!empty($userMobile))
                {
                    if(empty($userID))
                    {
                        $Exist_userMobile = Users::find()->where("userMobile = '".$userMobile."'")->all();
                    }
                    else 
                    {
                        $Exist_userMobile = Users::find()->where("userMobile = '".$userMobile."' and userID <> '".$userID."'")->all();
                    }

                    if(count($Exist_userMobile) == 0)
                    {
                       return 'true';
                    }
                    else
                    {
                        return 'userMobile';
                    }
                }
                else
                {
                    return 'true';
                }
            }
            else
            {
                return 'userEmail';
            }
        }
        else
        {
            if(!empty($userMobile))
            {
                if(empty($userID))
                {
                    $Exist_userMobile = Users::find()->where("userMobile = '".$userMobile."'")->all();
                }
                else 
                {
                    $Exist_userMobile = Users::find()->where("userMobile = '".$userMobile."' and userID <> '".$userID."'")->all();
                }

                if(count($Exist_userMobile) == 0)
                {
                    return 'true';
                }
                else
                {
                    return 'userMobile';
                }
            }
            else
            {
               return 'true';
            }
        }
    }

    public function GetUserDetails($UserID)
    {
      // $Where = 'users.userStatus = "Active" AND useraddress.addressType = "Registered"';
	   $Where = ' WHERE users.userStatus = "Active" ';
       $Re = array();
      
       if(!empty($UserID))
       {
           $Where .= ' AND users.UserID = "'.$UserID.'"';
       }
	    
		$para = 'SELECT users.*,countryName,cityName,stateName,languageName ';
		$from = ' FROM users';
        $ORDERBY = " users.userID";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = "";//" LIMIT $pagesize";
        $OFFSET ="";// " OFFSET ".$page*$pagesize;      
		$join = '   left join country on country.countryID = users.countryID					
					left join state on state.stateID= users.stateID  
					left join city on city.cityID= users.cityID   
					LEFT Join language ON language.languageID = users.languageID					';		

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
	   
       if(count($result)>0)
       {
		   $Usersubscriptions = new Usersubscriptions;
		   $Settings = new Settings;
           for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				$resSubscription = $Usersubscriptions->GetUserSubscriptionplanList($UserID,0,10);
				$Re[$i]['mySubscriptions'] = $resSubscription;
				$Re[$i]['settings'] = $Settings->GetUserSettings();
				$Re[$i]['sharemessage'] = "Share The English Monk with your friends. https://play.google.com/store/apps/details?id=com.englishmonk";
			
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re;   
    }
}
