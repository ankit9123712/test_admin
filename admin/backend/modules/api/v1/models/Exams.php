<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "exams".
 *
 * @property int $examID
 * @property int $moduleID
 * @property string $examName
 * @property string $examContains
 * @property string $examSections
 * @property string $examDuration
 * @property string $queIDs
 * @property int $examTotalQs
 * @property int $examQualifyingMarks
 * @property double $examCorrectAnswer
 * @property double $examWrongAnswer
 * @property string $examInstruction
 * @property string $examStatus
 */
class Exams extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exams';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['moduleID', 'examName', 'examDuration', 'queIDs', 'examTotalQs', 'examQualifyingMarks', 'examCorrectAnswer', 'examWrongAnswer', 'examInstruction', 'examStatus'], 'required'],
            [['moduleID', 'examTotalQs', 'examQualifyingMarks'], 'integer'],
            [['examDuration'], 'safe'],
            [['examCorrectAnswer', 'examWrongAnswer'], 'number'],
            [['examInstruction', 'examStatus'], 'string'],
            [['examName', 'examContains', 'examSections'], 'string', 'max' => 100],
            [['queIDs'], 'string', 'max' => 3000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'examID' => 'Exam ID',
            'moduleID' => 'Module ID',
            'examName' => 'Exam Name',
            'examContains' => 'Exam Contains',
            'examSections' => 'Exam Sections',
            'examDuration' => 'Exam Duration',
            'queIDs' => 'Que I Ds',
            'examTotalQs' => 'Exam Total Qs',
            'examQualifyingMarks' => 'Exam Qualifying Marks',
            'examCorrectAnswer' => 'Exam Correct Answer',
            'examWrongAnswer' => 'Exam Wrong Answer',
            'examInstruction' => 'Exam Instruction',
            'examStatus' => 'Exam Status',
        ];
    }
	//get exam list
	public function GetExamsList($loginuserID)
    {
	   $where = 	'examStatus = "Active" ';//AND coursePublished="Yes"';
	    
	   
	   
		
	   $result = Exams::find()->select('*')->where($where)->orderby('examID DESC')->asArray()->all();
	   if(count($result)>0)
       {
           //$Lessonfiles = new Lessonfiles;
		   //$userLess = new Userlessions;
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$Re[$i]["modules"]= $this->getExamModules($result[$i]["examID"],$result[$i]["queIDs"]);
				
				$UserexamsRes = Userexams::find()->where("examID = '".$result[$i]["examID"]."' AND userID=".$loginuserID)->one();
				$Re[$i]["examgiven"]= "No";
				$Re[$i]["userexamReadingSubmitted"]= "No";
				$Re[$i]["userexamListeningSubmitted"]= "No";
				$Re[$i]["userexamWritingSubmitted"]= "No";
				$Re[$i]["userexamSpeakingSubmitted"]= "No";
				if(!empty($UserexamsRes))
				{
					$Re[$i]["userexamReadingSubmitted"]= $UserexamsRes->userexamReadingSubmitted;
					$Re[$i]["userexamListeningSubmitted"]= $UserexamsRes->userexamListeningSubmitted;
					$Re[$i]["userexamWritingSubmitted"]= $UserexamsRes->userexamWritingSubmitted;
					$Re[$i]["userexamSpeakingSubmitted"]= $UserexamsRes->userexamSpeakingSubmitted;
					$Re[$i]["examgiven"]= "Yes";
				}
				/*$Re[$i]["lessionfiles"] = $Lessonfiles->GetChapterfilesList($result[$i]["LessonID"]) ;
				
				$userLessRes = Userlessions::find()->where("lessionID = '".$result[$i]["LessonID"]."' AND userID=".$loginuserID)->one();
			//	echo "lessionID = '".$result[$i]["LessonID"]."' AND userID=".$loginuserID;
			//	print_r($userLessRes);die;
				$Re[$i]["progress"]= "N/A";
				if(!empty($userLessRes))
				{
					$Re[$i]["progress"] =  round((($userLessRes->userlessionCorrectAnswers*100)/$userLessRes->userlessionQuestions));
				}*/
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
	private function getExamModules($examID,$queIDs)
	{
		$Re = array();
		$Where = " WHERE moduleStatus='Active' ";
		if(!empty($examID))
		{
			
			$Where .= " AND questions.examID =".$examID;
		}
		
		$para = 'SELECT coursemodule.moduleName, coursemodule.moduleID	';
		$from = ' FROM coursemodule';
		$join = ' inner join questions on questions.moduleID = coursemodule.moduleID ';
		
        $ORDERBY = " coursemodule.moduleName";
        $order = " ORDER BY $ORDERBY DESC";
                  

        $sql = $para.$from.$join.$Where." GROUP BY coursemodule.moduleID ".$order;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
       {
		       for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				$Re[$i]= $this->getExamQuestions($examID,$result[$i]["moduleID"]);
			
           }
	   }
	   else
	   {
		   $Re = array();
	   }
	   return $Re;
	}
	private function getExamQuestions($examID,$moduleID)
	{
		$Re = array();
		$Where = " WHERE queStatus='Active' ";
		if(!empty($examID))
		{
			
			$Where .= " AND questions.examID = ".$examID;
		}
		if(!empty($moduleID))
		{
			
			$Where .= " AND questions.moduleID = ".$moduleID;
		}
		$para = 'SELECT questions.*	';
		$from = ' FROM questions';
		$join = '  ';
		
        $ORDERBY = " moduleID ASC, queQuestion";
        $order = " ORDER BY $ORDERBY ASC";
                  

        $sql = $para.$from.$join.$Where.$order;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
       {
		       for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				
			
           }
	   }
	   else
	   {
		   $Re = array();
	   }
	   return $Re;
	}
}
