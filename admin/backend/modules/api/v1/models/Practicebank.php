<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "practicebank".
 *
 * @property int $PracticeBankID
 * @property int $moduleID
 * @property int $PracticeBankNo
 * @property string $PracticeBankName
 * @property string $PracticeBankDescription
 * @property string $PracticeBankType
 * @property string $PracticeBankQuestionFile
 * @property string $PracticeBankAnswerFile
 * @property string $PracticeBankStatus
 * @property string $PracticeBankCreatedDate
 */
class Practicebank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'practicebank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['moduleID', 'PracticeBankNo', 'PracticeBankName', 'PracticeBankDescription', 'PracticeBankType', 'PracticeBankQuestionFile', 'PracticeBankAnswerFile'], 'required'],
            [['moduleID', 'PracticeBankNo'], 'integer'],
            [['PracticeBankDescription', 'PracticeBankType', 'PracticeBankStatus'], 'string'],
            [['PracticeBankCreatedDate'], 'safe'],
            [['PracticeBankName'], 'string', 'max' => 100],
            [['PracticeBankQuestionFile', 'PracticeBankAnswerFile'], 'string', 'max' => 300],
            [['moduleID', 'PracticeBankName'], 'unique', 'targetAttribute' => ['moduleID', 'PracticeBankName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PracticeBankID' => 'Practice Bank ID',
            'moduleID' => 'Module ID',
            'PracticeBankNo' => 'Practice Bank No',
            'PracticeBankName' => 'Practice Bank Name',
            'PracticeBankDescription' => 'Practice Bank Description',
            'PracticeBankType' => 'Practice Bank Type',
            'PracticeBankQuestionFile' => 'Practice Bank Question File',
            'PracticeBankAnswerFile' => 'Practice Bank Answer File',
            'PracticeBankStatus' => 'Practice Bank Status',
            'PracticeBankCreatedDate' => 'Practice Bank Created Date',
        ];
    }
	public function GetPracticebankList($loginuserID,$moduleID,$page="0",$pagesize="10")
    {
		$Where = ' WHERE PracticeBankStatus="Active" ';
		if(!empty($moduleID))
		{
			$Where .= ' AND moduleID="'.$moduleID.'"';
		}
		 
		$para = 'SELECT practicebank.*';
		$from = ' FROM practicebank';
        $ORDERBY = " practicebank.PracticeBankID";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
