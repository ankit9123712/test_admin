<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "template".
 *
 * @property int $templateID
 * @property string $templateName
 * @property string $templateActionName
 * @property string $templatePush
 * @property string $templateEmail
 * @property string $templateSMS
 * @property string $templateConstantCode
 * @property string $templateSubject
 * @property string $templateEmailText
 * @property string $templateSMSText
 * @property string $templateNotificationText
 * @property string $templateStatus
 * @property string $templateCreatedDate
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['templateID', 'templateActionName', 'templatePush', 'templateEmail', 'templateSMS', 'templateEmailText'], 'required'],
            [['templateID'], 'integer'],
            [['templatePush', 'templateEmail', 'templateSMS', 'templateEmailText', 'templateStatus'], 'string'],
            [['templateCreatedDate'], 'safe'],
            [['templateName', 'templateActionName'], 'string', 'max' => 100],
            [['templateConstantCode'], 'string', 'max' => 6],
            [['templateSubject'], 'string', 'max' => 250],
            [['templateSMSText', 'templateNotificationText'], 'string', 'max' => 1000],
            [['templateConstantCode'], 'unique'],
            [['templateID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'templateID' => 'Template ID',
            'templateName' => 'Template Name',
            'templateActionName' => 'Template Action Name',
            'templatePush' => 'Template Push',
            'templateEmail' => 'Template Email',
            'templateSMS' => 'Template Sms',
            'templateConstantCode' => 'Template Constant Code',
            'templateSubject' => 'Template Subject',
            'templateEmailText' => 'Template Email Text',
            'templateSMSText' => 'Template Sms Text',
            'templateNotificationText' => 'Template Notification Text',
            'templateStatus' => 'Template Status',
            'templateCreatedDate' => 'Template Created Date',
        ];
    }
	public function SendNotification($templateConstantCode,$languageID,$Receiver_userID,$Receiver_Type,$Sender_userID="",$Sender_Type,$Extra_Para_Array=array(),$Extra_Para_Array_Replace = array())
	{
        $GetTemplate_Res = $this->GetTemplate($templateConstantCode,$languageID);
        if(!empty($GetTemplate_Res))
        {
            $Settings =  new Settings();
            $GetSettingsData_Res = $Settings->GetUserSettings();
			

            $Receiver_Info_Array = $this->GetDetails_Receiver($Receiver_userID,$Receiver_Type);
            $Sender_Info_Array = $this->GetDetails_Sender($Sender_userID,$Sender_Type);

            $User_Info_Array = array_merge($Receiver_Info_Array,$Sender_Info_Array);
            $Full_Para_Array = array_merge($User_Info_Array,$Extra_Para_Array);
			
            /*check email needs to send*/ 
            
            if(!empty($GetTemplate_Res['templateEmailText']))
            {
                if(!empty($Full_Para_Array['Receiver_Email']))
                {
                        $templateEmailText = $GetTemplate_Res['templateEmailText'];
                        if(!empty($Full_Para_Array['Receiver_Email']))
                        {
                            foreach($Full_Para_Array as $key => $value)  
                            {           
                                $templateEmailText=str_replace("###".$key."###",$value,$templateEmailText);
                            }
							//echo $templateEmailText; die;
                            $From = $GetSettingsData_Res['settingsEmailFrom'];
                            $To = $Full_Para_Array['Receiver_Email'];
                            $Subject = $GetTemplate_Res['templateSubject'];
                            //$this->SendEmail($From,$To,$Subject,$templateEmailText);
                        }

                }
                
            }
            
            if(!empty($GetTemplate_Res['templateSMSText']))
            {
				
                $templateSMSText = $GetTemplate_Res['templateSMSText'];
                
                if(!empty($Full_Para_Array['Receiver_Mobile']))
                {   
                    foreach($Full_Para_Array as $key => $value)  
                    {           
                        $templateSMSText=str_replace("###".$key."###",$value,$templateSMSText);
                    }
                    
					//echo $templateSMSText; die;
					
                    $phone = @trim($Full_Para_Array['Receiver_CountryCode']).@trim($Full_Para_Array['Receiver_Mobile']);
                    //$this->sendSMS($phone,$templateSMSText);
					
                }
                
            }
			
            /*check SMS needs to send*/
            
            /*check Push needs to send*/

            if(!empty($GetTemplate_Res['templateNotificationText']))
            {
                
                //$CheckPushSettings = $this->CheckPushSettings($Full_Para_Array);
                 $CheckPushSettings = "Yes";
                if($CheckPushSettings == "Yes")
                {
                    $templateNotificationText = $GetTemplate_Res['templateNotificationText'];
                    
                    foreach($Full_Para_Array as $key => $value)  
                    {           
                        $templateNotificationText=str_replace("###".$key."###",$value,$templateNotificationText);
                    }

                    $Full_Para_Array['Receiver_Type'] = $Receiver_Type;
                    $Full_Para_Array['notificationMessageText'] = $templateNotificationText;
                    $Full_Para_Array['notificationTitle'] = $GetTemplate_Res['templateName'];
                    $Full_Para_Array['notificationContentID'] = $GetTemplate_Res['templateID'];
                    $this->sendPush($Full_Para_Array);
                } 
            }
            
        }
    }
	
	public function GetDetails_Sender($Sender_userID,$Sender_Type)
    {
		if($Sender_Type == 'Driver')
		{
			$result = array();
			$query = new \yii\db\Query();
			$result = $query->select(['drivers.driverID AS Sender_ID',
				'drivers.driverName AS Sender_Fullname',
				'drivers.driverEmail AS Sender_Email',
				'drivers.driverProfilePic AS Sender_ProfilePicture',
				'drivers.driverMobile AS Sender_Mobile',
				'drivers.driverCountryCode AS Sender_CountryCode',
				'drivers.driverDeviceType AS Sender_DeviceType',
				'drivers.driverDeviceID AS Sender_DeviceID',
				'drivers.driverEmailNotification AS Sender_EmailNotification',
				'drivers.driverPushNotification AS Sender_PushNotification'
				])
				->from('drivers')
				->where('drivers.driverID = "'.$Sender_userID.'"')
				->one();
		}
		else if($Sender_Type == 'User')
		{
			$result = array();
			$query = new \yii\db\Query();
			$result = $query->select(['users.userID AS Sender_ID',
				'users.userFullName AS Sender_Fullname',
				'users.userEmail AS Sender_Email',
				'users.userProfilePicture AS Sender_ProfilePicture',
				'users.userMobile AS Sender_Mobile',
				'users.userCountryCode AS Sender_CountryCode',
				'users.userDeviceType AS Sender_DeviceType',
				'users.userDeviceID AS Sender_DeviceID',
				'users.userEmailNotification AS Sender_EmailNotification',
				'users.userPushNotification AS Sender_PushNotification'
				])
				->from('users')
				->where('users.userID = "'.$Sender_userID.'"')
				->one();
		}
		else if($Sender_Type == 'Admin')
		{
			$result = array(
			"Sender_ID"=>"0",
			"Sender_Fullname"=>"Cab1",
			"Sender_Email"=>"testlast11@gmail.com",
			"Sender_Mobile"=>"7894561231",
			"Sender_CountryCode"=>"+91",
			"Sender_DeviceType"=>"Web",
			"Sender_DeviceID"=>"dsadasasdds",
			"Sender_EmailNotification"=>"Yes",
			"Sender_PushNotification"=>"Yes",
			);
		}
		
        return $result;
    }
	
	public function GetDetails_Receiver($Receiver_userID,$Receiver_Type)
    {
		if($Receiver_Type == 'Driver')
		{
			$result = array();
			$query = new \yii\db\Query();
			$result = $query->select(['drivers.driverID AS Receiver_ID',
				'drivers.driverName AS Receiver_Fullname',
				'drivers.driverEmail AS Receiver_Email',
				'drivers.driverProfilePic AS Receiver_ProfilePicture',
				'drivers.driverMobile AS Receiver_Mobile',
				'drivers.driverCountryCode AS Receiver_CountryCode',
				'drivers.driverDeviceType AS Receiver_DeviceType',
				'drivers.driverDeviceID AS Receiver_DeviceID',
				'drivers.driverEmailNotification AS Receiver_EmailNotification',
				'drivers.driverPushNotification AS Receiver_PushNotification'
				])
				->from('drivers')
				->where('drivers.driverID = "'.$Receiver_userID.'"')
				->one();
		}
		else if($Receiver_Type == 'User')
		{
			$result = array();
			$query = new \yii\db\Query();
			$result = $query->select(['users.userID AS Receiver_ID',
				'users.userFullName AS Receiver_Fullname',
				'users.userEmail AS Receiver_Email',
				'users.userProfilePicture AS Receiver_ProfilePicture',
				'users.userMobile AS Receiver_Mobile',
				'users.userCountryCode AS Receiver_CountryCode',
				'users.userDeviceType AS Receiver_DeviceType',
				'users.userDeviceID AS Receiver_DeviceID',
				'users.userEmailNotification AS Receiver_EmailNotification',
				'users.userPushNotification AS Receiver_PushNotification'
				])
				->from('users')
				->where('users.userID = "'.$Receiver_userID.'"')
				->one();
		}
		
        return $result;
    }
	
	
	 public function GetTemplate($templateConstantCode,$languageID="")
    {   

        $Where = "1=1";
        if(!empty($templateConstantCode))
        {
            $Where .= ' AND template.templateConstantCode ="'.$templateConstantCode.'" AND template.templateStatus = "Active"';
        }

        /*if(!empty($languageID))
        {
            $Where .= ' AND template_language.languageID ="'.$languageID.'"';
        }*/


        $result = array();
        $query = new \yii\db\Query();
        $result = $query->select(['template.templateID',
                                    'template.templateConstantCode',
                                    'template.templateName',
                                    'template.templateSubject',
                                    'template.templateEmailText',
                                    'template.templateSMSText',
                                    'template.templateNotificationText'
                                ])
                ->from('template')
                //->JOIN('INNER JOIN','template_language','template.templateID = template_language.templateID')
                ->where($Where)
                ->one();        
        return $result;
    }
	
	 public function sendPush($Full_Para_Array)
    {

        $notificationRefUserData = "";
         //print_r($Full_Para_Array); die;
        
        extract($Full_Para_Array);

        /*$Users = Users::find()->select('userPushBaseCount,userPushNotificationSound')->Where('userID = "'.$userID.'"')->asArray()->one();
        
        $badge = $Users['userPushBaseCount'];

        if($Users['userPushNotificationSound'] == "Yes")
        {
            $sound = "default";
        }
        else
        {
            $sound = "";
        }*/
		
		$sound = "";
		$badge = 0;
		$ApiKey = "";
		$notificationType = str_replace(' ', '',$notificationTitle);
		
		if($Receiver_Type == 'User')
		{
			//$ApiKey = "AIzaSyC10be8mreLfW4ibXt9MFGxPiaDIOz8Pcg";
			$ApiKey = "AIzaSyA6Ie1WqKXcUlbGOMtjyRdMsa8ARcf_WeY";
		}
		else if($Receiver_Type == 'Driver')
		{
			//$ApiKey = "AIzaSyCy1Ilp3kxQ-IyQv3F411CmyhWthNR7dSg";
			$ApiKey = "AIzaSyA6Ie1WqKXcUlbGOMtjyRdMsa8ARcf_WeY";

		}
		
        if(strtolower($Receiver_DeviceType) == 'iphone')
        {
            $arrname = 'notification';
        }
		
        if(strtolower($Receiver_DeviceType) == 'android')
        {
            $arrname = 'data';
        }
        
        if(!empty($Receiver_DeviceID))
        {
			
              $curl = curl_init();
			  
			 $data = array(
				  $arrname => array(
				 'body'=>$notificationMessageText,
				 'type' => $notificationType,
				 'title' => $notificationTitle,
				 'priority' => 'high',
				 'msg' => $notificationMessageText,
				 'msgType' => "SM",
				 'msgKey' => "1",
				 'time_to_live' => "60",
				 'badge' => $badge,
				 'sound' => $sound,
				 'RefKey' => $notificationReferenceKey,
				 'RefData' => unserialize($notificationReferenceData)
				 ),
				'to' => "$Receiver_DeviceID"
			 );
            
			//print_r($data); die;
			
			
             //print_r(json_encode($data)); die;
                         //print_r($data); die;
              curl_setopt_array($curl, array(
              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_SSL_VERIFYPEER => false,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($data),
              CURLOPT_HTTPHEADER => array(
                 "authorization: key=$ApiKey",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 8a84102f-7aab-6c69-6aca-a34b89ccf20b"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
        //print_R($err);die;        
            curl_close($curl);
            $ResponseDecode = json_decode($response);
			//print_r($response); die;
            if($ResponseDecode->success == 1)
            {
               $Full_Para_Array['notificationResponse'] = $response;
                $Full_Para_Array['notificationStatus'] = 'Yes';
            }
            else
            {
                $Full_Para_Array['notificationResponse'] = $response;
                $Full_Para_Array['notificationStatus'] = 'No';
            }            
        }
        else
        {
            $Full_Para_Array['notificationStatus'] = 'No';
            $Full_Para_Array['notificationResponse'] = 'Device Type Empty.';
        }
        $Full_Para_Array['notificationType'] = $notificationType;
		$Notification = new Notification();
		$Notification->AddNotification($Full_Para_Array);
        
    }
	
	public function sendSMS($phone="",$smslogMessage="")
    {
            $msg=urlencode($smslogMessage);
            $sendmsg =  "http://japi.instaalerts.zone/httpapi/QueryStringReceiver?ver=1.0&key=ktTjivDwNbauw2KL/9fL2A==&encrpt=0&dest=$phone&send=CABTXI&text=$msg";
            $ch= curl_init();  
            curl_setopt($ch, CURLOPT_URL, $sendmsg);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result=curl_exec($ch);
			
			$Smslog = new Smslog;
			$Smslog->smslogMobile = $phone;
			$Smslog->smslogMessage = $smslogMessage;
			$Smslog->smslogResponse = $result;
			$Smslog->save(false);
    }
	
	public function SendEmail($From,$To,$Subject,$Html)
    {
        if((!empty($From)) && (!empty($To)) && (!empty($Subject)) && (!empty($Html)))
        {
			
			$email = new \SendGrid\Mail\Mail(); 
			$email->setFrom($From);
			$email->setSubject($Subject);
			$email->addTo($To);
			$email->addContent("text/html", $Html);
			/*$email->addContent(
				"text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
			);*/
			
			//$sendgrid = new \SendGrid(getenv('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'));
			$sendgrid = new \SendGrid('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');
			try {
				$response = $sendgrid->send($email);
				//print_r($response);die;
				//print_r($response->statusCode()) . "\n";
				//print_r($response->headers());
				//print_r($response->body()) . "\n";
			} catch (Exception $e) {
				//echo 'Caught exception: '. $e->getMessage() ."\n";
			}
					// die;
        }
        
    }

}
