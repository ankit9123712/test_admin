<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "lesson".
 *
 * @property int $LessonID
 * @property int $moduleID
 * @property int $LessonNo
 * @property string $LessonName
 * @property string $LessonDescription
 * @property string $lessionMarkingSystem
 * @property string $lessionQuickTips
 * @property string $LessonType
 * @property string $LessonFileType
 * @property string $LessonFile
 * @property string $LessonFileURL
 * @property int $VideoFileCount
 * @property int $ContentFileCount
 * @property string $LessonStatus
 * @property string $LessonCreatedDate
 */
class Lesson extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['moduleID', 'LessonNo', 'LessonName', 'LessonDescription', 'LessonType', 'LessonFile', 'LessonFileURL'], 'required'],
            [['moduleID', 'LessonNo', 'VideoFileCount', 'ContentFileCount'], 'integer'],
            [['LessonDescription', 'lessionMarkingSystem', 'lessionQuickTips', 'LessonType', 'LessonFileType', 'LessonStatus'], 'string'],
            [['LessonCreatedDate'], 'safe'],
            [['LessonName', 'LessonFile'], 'string', 'max' => 100],
            [['LessonFileURL'], 'string', 'max' => 300],
            [['moduleID', 'LessonName'], 'unique', 'targetAttribute' => ['moduleID', 'LessonName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LessonID' => 'Lesson ID',
            'moduleID' => 'Module ID',
            'LessonNo' => 'Lesson No',
            'LessonName' => 'Lesson Name',
            'LessonDescription' => 'Lesson Description',
            'lessionMarkingSystem' => 'Lession Marking System',
            'lessionQuickTips' => 'Lession Quick Tips',
            'LessonType' => 'Lesson Type',
            'LessonFileType' => 'Lesson File Type',
            'LessonFile' => 'Lesson File',
            'LessonFileURL' => 'Lesson File Url',
            'VideoFileCount' => 'Video File Count',
            'ContentFileCount' => 'Content File Count',
            'LessonStatus' => 'Lesson Status',
            'LessonCreatedDate' => 'Lesson Created Date',
        ];
    }
	public function GetCoursesList($moduleID=0, $page="0",$pagesize="10",$loginuserID=0,$searchkeyword="",$facultyID=0,$unassigned="",$facultyModules="",$usertype="",$statusFilter="",$lessionID=0)
    {
	   $where = 	"  1=1 ";//AND coursePublished="Yes"';
	   if(!empty($moduleID))
		   $where.= ' AND lesson.moduleID = '.$moduleID;
	   if(!empty($usertype))
		   $where.= " AND LessonType = '".$usertype."'";
	   if(!empty($lessionID))
		   $where.= " AND lesson.LessonID = '".$lessionID."'";
	   if(!empty($statusFilter))
		   $where.= " AND LessonStatus = '".$statusFilter."'";
	   if(!empty($searchkeyword))	
		   $where.= " AND (LessonName LIKE '".$searchkeyword."%' OR LessonDescription LIKE '".$searchkeyword."%')";
	   if(!empty($facultyID))	
	   { $where.= " AND lessonfaculty.facultyID='".$facultyID."'";
		   $where.= " AND lessonfaculty.lessonfacultyType='Teaching'";
	   }
	   else
	   {
		   $where.= " AND LessonStatus='Active' ";
	   }
	   if(!empty($unassigned))	
		   $where.= " AND facultyID='0'";
	   if(!empty($facultyModules))	
	   {
		   if($facultyModules=="Reading")
			$where.= " AND lesson.moduleID IN (6,8)";
		   if($facultyModules=="Writing")
			$where.= " AND lesson.moduleID IN (7,9)";
		   if($facultyModules=="Listening")
			$where.= " AND lesson.moduleID IN (11,10)";
		   if($facultyModules=="Speaking")
			$where.= " AND lesson.moduleID IN (12,13)";
	   }
		   
		//echo $where;
	   $result = Lesson::find()->select('lesson.*,moduleName')->join('LEFT join','lessonfaculty','lessonfaculty.lessionID = lesson.LessonID')->join('LEFT join','coursemodule','coursemodule.moduleID = lesson.moduleID')->where($where)->limit($pagesize)->orderby('LessonNo ASC')->offset($pagesize*$page)->asArray()->all();
	   if(count($result)>0)
       {
           $Lessonfiles = new Lessonfiles;
		   $userLess = new Userlessions;
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$Re[$i]["lessionfiles"] = $Lessonfiles->GetChapterfilesList($result[$i]["LessonID"]) ;
				
				$userLessRes = Userlessions::find()->where("lessionID = '".$result[$i]["LessonID"]."' AND userID=".$loginuserID)->one();
			//	echo "lessionID = '".$result[$i]["LessonID"]."' AND userID=".$loginuserID;
			//	print_r($userLessRes);die;
				$Re[$i]["progress"]= "N/A";
				$Re[$i]["userlessionQuestions"] = "0";
				$Re[$i]["userlessionCorrectAnswers"] = "0";
				$Re[$i]["marks"] = "0";
				$Re[$i]["extramessage"] = "";
				if(!empty($userLessRes))
				{
					$Re[$i]["progress"] = 100;
					$Re[$i]["userlessionQuestions"] = (string)$userLessRes->userlessionQuestions;
					$Re[$i]["userlessionCorrectAnswers"] = (string)$userLessRes->userlessionCorrectAnswers;
					$Re[$i]["marks"] = (string)($userLessRes->userlessionCorrectAnswers*1);
					if($result[$i]["moduleID"]=="7" || $result[$i]["moduleID"]=="12") 
					{
						$Re[$i]["extramessage"] = "Please upgrade to premium to get evaluated this answers with faculty.";
						$Re[$i]["paidextramessage"] = "Please subscribe to Premium plan and get your answer evaluated by real teachers.";
					}
				}
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
	public function GetCoursesListCount($moduleID=0, $page="0",$pagesize="10",$loginuserID=0,$searchkeyword="",$facultyID=0,$unassigned="",$facultyModules="",$usertype="",$statusFilter="")
    {
	    $where = 	"  1=1 ";//AND coursePublished="Yes"';
	   if(!empty($moduleID))
		   $where.= ' AND lesson.moduleID = '.$moduleID;
	   if(!empty($usertype))
		   $where.= " AND LessonType = '".$usertype."'";
	    if(!empty($statusFilter))
		   $where.= " AND LessonStatus = '".$statusFilter."'";
	   if(!empty($searchkeyword))	
		   $where.= " AND (LessonName LIKE '".$searchkeyword."%' OR LessonDescription LIKE '".$searchkeyword."%')";
	   if(!empty($facultyID))	
	   { $where.= " AND lessonfaculty.facultyID='".$facultyID."'";
		   $where.= " AND lessonfaculty.lessonfacultyType='Teaching'";
	   }
	   else
	   {
		   $where.= " AND LessonStatus='Active' ";
	   }
	   if(!empty($unassigned))	
		   $where.= " AND facultyID='0'";
	   if(!empty($facultyModules))	
	   {
		   if($facultyModules=="Reading")
			$where.= " AND lesson.moduleID IN (6,8)";
		   if($facultyModules=="Writing")
			$where.= " AND lesson.moduleID IN (7,9)";
		   if($facultyModules=="Listening")
			$where.= " AND lesson.moduleID IN (11,10)";
		   if($facultyModules=="Speaking")
			$where.= " AND lesson.moduleID IN (12,13)";
	   }
		   
		//echo $where;
	   $result = Lesson::find()->select('lesson.*,moduleName')->join('LEFT join','lessonfaculty','lessonfaculty.lessionID = lesson.LessonID')->join('LEFT join','coursemodule','coursemodule.moduleID = lesson.moduleID')->where($where)->orderby('LessonNo ASC')->asArray()->all();
	   if(count($result)>0)
       {
           $Re = count($result);
       }
       else
       {
           $Re = 0;
       }
       return $Re; 
    }
}
