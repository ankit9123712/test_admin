<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property string $notificationID
 * @property int $notificationReferenceKey
 * @property int $userID
 * @property string $userDeviceType
 * @property string $userDeviceID
 * @property string $notificationType
 * @property string $notificationTitle
 * @property string $notificationMessageText
 * @property string $notificationResponse
 * @property string $notificationSendDate
 * @property string $notificationSendTime
 * @property string $notificationStatus
 * @property string $notificationReadStatus
 * @property string $notificationCreatedDate
 * @property string $notificationData
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notificationReferenceKey', 'userID'], 'integer'],
            [['notificationSendDate', 'notificationSendTime', 'notificationCreatedDate'], 'safe'],
            [['notificationStatus', 'notificationReadStatus', 'notificationData'], 'string'],
            [['userDeviceType'], 'string', 'max' => 10],
            [['userDeviceID', 'notificationTitle'], 'string', 'max' => 500],
            [['notificationType'], 'string', 'max' => 50],
            [['notificationMessageText', 'notificationResponse'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'notificationID' => 'Notification ID',
            'notificationReferenceKey' => 'Notification Reference Key',
            'userID' => 'User ID',
            'userDeviceType' => 'User Device Type',
            'userDeviceID' => 'User Device ID',
            'notificationType' => 'Notification Type',
            'notificationTitle' => 'Notification Title',
            'notificationMessageText' => 'Notification Message Text',
            'notificationResponse' => 'Notification Response',
            'notificationSendDate' => 'Notification Send Date',
            'notificationSendTime' => 'Notification Send Time',
            'notificationStatus' => 'Notification Status',
            'notificationReadStatus' => 'Notification Read Status',
            'notificationCreatedDate' => 'Notification Created Date',
            'notificationData' => 'Notification Data',
        ];
    }
	   public function GetNotificationList($loginuserID,$page,$pagesize)
    {
        $Where = '1=1';
        $Re = array();
      
        if(!empty($loginuserID))
        {
            $Where .= ' AND notification.userID = "'.$loginuserID.'"';
        }
      
        $result = array();
        $query = new \yii\db\Query();
        $result = $query->select([
            'notification.notificationID',
            'notification.notificationType',
            'notification.notificationTitle',
            'notification.notificationReferenceKey',
            'notification.notificationMessageText',
            'notification.notificationReadStatus',
            'notification.notificationSendDate',
            'notification.notificationSendTime'
            ])
        ->from('notification')
        ->where($Where)
        ->limit($pagesize)
		->orderBy([
            'notification.notificationSendDate'=>SORT_DESC,
            'notification.notificationSendTime' => SORT_DESC,
        ])
        ->offset($pagesize*$page)
        ->all();
        
        if(count($result)>0)
        {
            $Re = $result;
        }
        else
        {
            $Re = $result;
        }
        return $Re;
    }

     public function UpdateNotificationReadStatus($loginuserID,$notificationID,$languageID)
    {   
        $Errors = "";
        $result = array();
		$Apilog = new Apilog; 
        if(!empty($notificationID))// != -1)
        {
            $data = Notification::find()->where(['notificationID'=>$notificationID,'userID'=>$loginuserID])->one();
            if(!empty($data))
            {
                $data->notificationReadStatus = "Yes";
                $data->save(false);
                $Errors = $data->getErrors();
                if(empty($Errors))
                { 
                    $result[0]['status'] = 'true';
                    $result[0]['message'] = '';
                }
                else
                {
                    $result[0]['status'] = 'false';
                    $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
                }
            }
            else
            {
                $result[0]['status'] = 'false';
                $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"INVALIDDATA");
            }


        }
        else
        {
            $data = Notification::find()->where(['userID'=>$loginuserID])->all();
            if(count($data)>0)
            {
                Yii::$app->db->createCommand("UPDATE notification SET notificationReadStatus='Yes' WHERE notification.userID = '".$loginuserID."'")->execute();

                $result[0]['status'] = 'true';
                $result[0]['message'] = '';
                
            }
            else
            {
                $result[0]['status'] = 'false';
                $result[0]['message'] = messages_constant::INVALIDDATA;
            }
        }
        return $result; 
    }

    public function DeleteNotification($loginuserID,$notificationID,$languageID)
    {  

        $Errors = "";
        $result = array();
		$Apilog = new Apilog; 
        $data = Notification::find()->where(['notificationID'=>$notificationID,'userID'=>$loginuserID])->one();
        if(!empty($data))
        {
            $Delete = Notification::deleteAll('notificationID = :notificationID',['notificationID' => $notificationID]);   
            if($Delete == 1)
            {  
                $result[0]['status'] = 'true';
                $result[0]['message'] = '';
            }
            else
            {
                $result[0]['status'] = 'false';
                $result[0]['message'] = '';
            }
        }
        else
        {
            $result[0]['status'] = 'false';
            $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"INVALIDDATA");
        }
        return $result; 
    }
	
}
