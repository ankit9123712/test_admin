<?php

namespace backend\modules\api\v1\models;

use Yii;
use Stripe\Stripe;  
use Stripe\Charge;
/**
 * This is the model class for table "usersubscriptions".
 *
 * @property int $usersubscriptionID
 * @property int $userID
 * @property int $subscriptionID
 * @property string $usersubscriptionAmount
 * @property string $usersubscriptionCoponCode
 * @property string $usersubscriptionStartDate
 * @property string $usersubscriptionEndDate
 * @property string $usersubscriptionCreatedDate
 */
class Usersubscriptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usersubscriptions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'subscriptionID', 'usersubscriptionAmount', 'usersubscriptionCoponCode', 'usersubscriptionStartDate', 'usersubscriptionEndDate'], 'required'],
            [['userID', 'subscriptionID'], 'integer'],
            [['usersubscriptionAmount'], 'number'],
            [['usersubscriptionStartDate', 'usersubscriptionEndDate', 'usersubscriptionCreatedDate'], 'safe'],
            [['usersubscriptionCoponCode'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'usersubscriptionID' => 'Usersubscription ID',
            'userID' => 'User ID',
            'subscriptionID' => 'Subscription ID',
            'usersubscriptionAmount' => 'Usersubscription Amount',
            'usersubscriptionCoponCode' => 'Usersubscription Copon Code',
            'usersubscriptionStartDate' => 'Usersubscription Start Date',
            'usersubscriptionEndDate' => 'Usersubscription End Date',
            'usersubscriptionCreatedDate' => 'Usersubscription Created Date',
        ];
    }
	public function GetUserSubscriptionplanList($userID,$page=0,$pagesize=10)
    {
	   $where = 	' 1 = 1 ';
	   if(!empty($userID))
		   $where.= ' AND userID = '.$userID;
	 
		
	   $result = Usersubscriptions::find()->select('usersubscriptions.*,planName,planDescription,planPrice,planDuration,planDiscountPrice ')->join('inner join','subscriptionplan','subscriptionplan.planID = usersubscriptions.subscriptionID	')->where($where)->limit($pagesize)->orderby('usersubscriptionID DESC')->offset($pagesize*$page)->asArray()->all();
	   if(count($result)>0)
       {

		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];             
           }
       }
       else
       {
       		$result[0]['usersubscriptionEndDate'] = date('Y-m-d',strtotime("-1 days"));
           	$Re = $result;
       }
       return $Re; 
    }
	public function StripeBlockAmount($userID,$cardToken,$price)
	{
		$Settings = new Settings();
		$Res = Settings::find()->where('settingsID=1')->asArray()->all();
		//print_r($Res); die; 
				
		if($Res[0]["settingPGMode"]=="Sandbox")
			$Settings_StripeKey=$Res[0]["settingsPGSandboxCustomerAuth"];
		else
			$Settings_StripeKey=$Res[0]["settingPGLiveCustomerAuth"];

        $amount = round($price*100);
      
         
		 
				
		$Error_Msg = "";
		$Registeruser = new Users();
		$Registeruser_Result = $Registeruser->GetUserDetails($userID);

		$email = trim($Registeruser_Result[0]['userEmail']);
		$description = $Registeruser_Result[0]['userFullName']."(".$Registeruser_Result[0]['userID'].") Englishmonk payment".

		$Error_Msg = "";

		\Stripe\Stripe::setApiKey($Settings_StripeKey);

		try {
		  
			$Customer_Create_On_Stripe = \Stripe\Customer::create(array(
			 "source" => $cardToken,
			 "email" => $email,
			 "description" => $description
			 ));

		} catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];
		 
		 $Error_Msg = 'Message is:' . $err['message'];

		} catch (\Stripe\Error\RateLimit $e) {
		  $Error_Msg = "Too many requests made to the API too quickly.";
		} catch (\Stripe\Error\InvalidRequest $e) {
		  $Error_Msg = "Invalid parameters were supplied to Stripe's API.";
		} catch (\Stripe\Error\Authentication $e) {
		  $Error_Msg = "Authentication with Stripe's API failed
		  (maybe you changed API keys recently).";

		} catch (\Stripe\Error\ApiConnection $e) {
		  $Error_Msg = "Network communication with Stripe failed.";
		} catch (\Stripe\Error\Base $e) {
		  $Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
		} catch (Exception $e) {
		  $Error_Msg = "Something else happened, completely unrelated to Stripe.";
		}
		
		
		try 
		{
		  
		  $Amount_Blocked_On_Stripe = \Stripe\Charge::create(array(
			"amount" => $amount,
			"currency" => "usd",
			"description" => "Amoun charged for payment of subscription.",
			"capture" => false,
			"customer" => $Customer_Create_On_Stripe->id,
			));

		  } catch(\Stripe\Error\Card $e) {
			// Since it's a decline, \Stripe\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
		   
		   $Error_Msg = 'Message is:' . $err['message'];

		  } catch (\Stripe\Error\RateLimit $e) {
			$Error_Msg = "Too many requests made to the API too quickly.";
		  } catch (\Stripe\Error\InvalidRequest $e) {
			$Error_Msg = "Invalid parameters were supplied to Stripe's API.";
		  } catch (\Stripe\Error\Authentication $e) {
			$Error_Msg = "Authentication with Stripe's API failed
			(maybe you changed API keys recently).";

		  } catch (\Stripe\Error\ApiConnection $e) {
			$Error_Msg = "Network communication with Stripe failed.";
		  } catch (\Stripe\Error\Base $e) {
			$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
		  } catch (Exception $e) {
			$Error_Msg = "Something else happened, completely unrelated to Stripe.";
		  }
			 
		if(empty($Error_Msg))
		{
		  $Res2 = array('0' => 'true', '1' =>$Amount_Blocked_On_Stripe->id, '2' =>$Customer_Create_On_Stripe->id);
		}
		return $Res2;
			
		
	}
}
