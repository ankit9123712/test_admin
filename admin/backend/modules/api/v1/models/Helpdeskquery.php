<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "helpdeskquery".
 *
 * @property int $helpdeskqueryID
 * @property int $userID
 * @property string $heldeskqueryQuery
 * @property string $helpdeskqueryStatus
 * @property string $helpdeskqueryCreatedDate
 */
class Helpdeskquery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helpdeskquery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'heldeskqueryQuery'], 'required'],
            [['userID'], 'integer'],
            [['helpdeskqueryStatus'], 'string'],
            [['helpdeskqueryCreatedDate'], 'safe'],
            [['heldeskqueryQuery'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'helpdeskqueryID' => 'Helpdeskquery ID',
            'userID' => 'User ID',
            'heldeskqueryQuery' => 'Heldeskquery Query',
            'helpdeskqueryStatus' => 'Helpdeskquery Status',
            'helpdeskqueryCreatedDate' => 'Helpdeskquery Created Date',
        ];
    }
}
