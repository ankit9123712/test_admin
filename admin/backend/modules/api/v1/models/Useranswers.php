<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "useranswers".
 *
 * @property int $answerID
 * @property int $userID
 * @property int $queID
 * @property string $answerAnswer
 * @property string $answerCorrectAnswer
 * @property string $answerIsCorrect
 * @property string $answerIsVerified
 * @property int $facultyID
 * @property string $answerSubmittedDate
 */
class Useranswers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'useranswers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'queID', 'answerAnswer', 'answerCorrectAnswer'], 'required'],
            [['userID', 'queID', 'facultyID'], 'integer'],
            [['answerIsCorrect', 'answerIsVerified'], 'string'],
            [['answerSubmittedDate'], 'safe'],
            [['answerAnswer', 'answerCorrectAnswer'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'answerID' => 'Answer ID',
            'userID' => 'User ID',
            'queID' => 'Que ID',
            'answerAnswer' => 'Answer Answer',
            'answerCorrectAnswer' => 'Answer Correct Answer',
            'answerIsCorrect' => 'Answer Is Correct',
            'answerIsVerified' => 'Answer Is Verified',
            'facultyID' => 'Faculty ID',
            'answerSubmittedDate' => 'Answer Submitted Date',
        ];
    }
}
