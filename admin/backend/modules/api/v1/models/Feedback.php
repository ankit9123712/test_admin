<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $feedbackID
 * @property string $feedbackName
 * @property string $feedbackEmail
 * @property string $feedbackFeedback
 * @property int $userID
 * @property string $feedbackDate
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['feedbackName', 'feedbackEmail', 'feedbackFeedback', 'userID'], 'required'],
            [['userID'], 'integer'],
            [['feedbackDate'], 'safe'],
            [['feedbackName', 'feedbackEmail'], 'string', 'max' => 100],
            [['feedbackFeedback'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'feedbackID' => 'Feedback ID',
            'feedbackName' => 'Feedback Name',
            'feedbackEmail' => 'Feedback Email',
            'feedbackFeedback' => 'Feedback Feedback',
            'userID' => 'User ID',
            'feedbackDate' => 'Feedback Date',
        ];
    }
	public function GetFeedbackList($loginuserID,$page="0",$pagesize="10")
    {
		$Where = ' WHERE 1=1  ';
		if(!empty($loginuserID))
		{
			$Where .= ' AND userID="'.$loginuserID.'"';
		}
		 
		$para = 'SELECT feedback.*';
		$from = ' FROM feedback';
        $ORDERBY = " feedback.feedbackID";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
