<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property int $languageID
 * @property string $languageName
 * @property string $languageStatus
 * @property string $languageCreatedDate
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['languageName'], 'required'],
            [['languageStatus'], 'string'],
            [['languageCreatedDate'], 'safe'],
            [['languageName'], 'string', 'max' => 100],
            [['languageName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'languageID' => 'Language ID',
            'languageName' => 'Language Name',
            'languageStatus' => 'Language Status',
            'languageCreatedDate' => 'Language Created Date',
        ];
    }
	 public function GetLanguageList()
    {
        $Res = Language::find()->select('languageID,languageName')->where('languageStatus = "Active"')->asArray()->all();
        return $Res;
    }
}
