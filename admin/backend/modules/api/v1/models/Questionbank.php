<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "questionbank".
 *
 * @property int $questionbankID
 * @property string $questionbankName
 * @property string $questionbankDescription
 * @property string $questionbankQuestionFile
 * @property string $questionbankAnswerFile
 * @property string $questionbankStatus
 * @property string $questionbankCreatedDate
 * @property int $moduleID
 */
class Questionbank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questionbank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['questionbankName', 'questionbankDescription', 'questionbankQuestionFile', 'questionbankAnswerFile', 'moduleID'], 'required'],
            [['questionbankDescription', 'questionbankStatus'], 'string'],
            [['questionbankCreatedDate'], 'safe'],
            [['moduleID'], 'integer'],
            [['questionbankName'], 'string', 'max' => 100],
            [['questionbankQuestionFile', 'questionbankAnswerFile'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'questionbankID' => 'Questionbank ID',
            'questionbankName' => 'Questionbank Name',
            'questionbankDescription' => 'Questionbank Description',
            'questionbankQuestionFile' => 'Questionbank Question File',
            'questionbankAnswerFile' => 'Questionbank Answer File',
            'questionbankStatus' => 'Questionbank Status',
            'questionbankCreatedDate' => 'Questionbank Created Date',
            'moduleID' => 'Module ID',
        ];
    }
	public function GetQuestionbankList($loginuserID,$moduleID,$page="0",$pagesize="10")
    {
		$Where = ' WHERE questionbankStatus="Active" ';
		if(!empty($moduleID))
		{
			$Where .= ' AND moduleID="'.$moduleID.'"';
		}
		 
		$para = 'SELECT questionbank.*';
		$from = ' FROM questionbank';
        $ORDERBY = " questionbank.questionbankID";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
