<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "apilog".
 *
 * @property int $apiID
 * @property string $apiName
 * @property string $apiIP
 * @property string $apiRequest
 * @property string $apiResponse
 * @property string $apiType
 * @property string $apiVersion
 * @property string $apiCallDate
 */
class Apilog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apilog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['apiName', 'apiResponse', 'apiType', 'apiVersion'], 'required'],
            [['apiRequest', 'apiResponse', 'apiType'], 'string'],
            [['apiCallDate'], 'safe'],
            [['apiName'], 'string', 'max' => 200],
            [['apiIP'], 'string', 'max' => 50],
            [['apiVersion'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'apiID' => 'Api ID',
            'apiName' => 'Api Name',
            'apiIP' => 'Api Ip',
            'apiRequest' => 'Api Request',
            'apiResponse' => 'Api Response',
            'apiType' => 'Api Type',
            'apiVersion' => 'Api Version',
            'apiCallDate' => 'Api Call Date',
        ];
    }
	public function is_json($string,$return_data = false) 
    {
	   $data = json_decode($string);
       return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }

    public function AddApiLog($action,$requestString,$responseString,$apiType="",$apiVersion="")
    {   
        $ip = $_SERVER['REMOTE_ADDR'];
        $ip1 = GetHostByName($ip);
        $model = new Apilog();
        $model->apiName = $action;
        $model->apiRequest = serialize($requestString);
        $model->apiResponse = ""; serialize(json_encode($responseString));
        $model->apiType = $apiType;
        $model->apiIP = $ip1;
        $model->apiVersion = $apiVersion;
        $model->apiCallDate = date('Y-m-d H:i:s');
        $model->save(false);       
    }

	
	public function GetErrorSuccessMsg($languageID="1",$langMsgKey="")
    {
        $where = ' langMsgStatus = "Active"';
        if(!empty($languageID))
        {
            $where .= ' AND languageID = "'.$languageID.'"';
        }
        if(!empty($langMsgKey))
        {
            $where .= ' AND langMsgKey = "'.$langMsgKey.'"';
        }

        $LanguageErrorSuccess_Res = Languagemsgs::find()->select('langMsgValue')->where($where)->asArray()->one();
        if(!empty($LanguageErrorSuccess_Res))
        {
            $Res = $LanguageErrorSuccess_Res['langMsgValue'];
        }
        else
        {
            $Res = "";
        }
        return $Res;
    }

	public function GenerateOTP($n) 
	{ 
		$generator = "1357902468"; 
		$result = ""; 
		for($i = 1; $i <= $n; $i++) 
		{ 
			$result .= substr($generator, (rand()%(strlen($generator))), 1); 
		}
		//$result = "1234";
		return $result; 
	} 
	
	public function ImagePathArray()
	{
		$baseurl = 'http://13.126.208.24/backend/web/uploads/';
		
		$res = array(
		'countryFlagImage'=>$baseurl.'flag/',
		'userProfilePicture'=>$baseurl.'users/',
		'courseImage'=>$baseurl.'course/',
		'bannerFile'=>$baseurl.'banners/',
		'brandImage'=>$baseurl.'brand/',
		'coursesubjIcon'=>$baseurl.'subject/',
		'coursechapterFile'=>$baseurl.'chapter/',
		'chapterfileFile'=>$baseurl.'chapter/',
		'cmspageImage'=>$baseurl.'cmspage/',
		'blogImage'=>$baseurl.'blog/'		
		);
		return $res;
	}
	
	public function generateRandomString($alpha = true, $nums = true, $usetime = false, $string = '', $length = 120) 
	{
		$alpha = ($alpha == true) ? 'abcdefghijklmnopqrstuvwxyz' : '';
		$nums = ($nums == true) ? '1234567890' : '';

		if ($alpha == true || $nums == true || !empty($string)) 
		{
			if ($alpha == true) {
				$alpha = $alpha;
				$alpha .= strtoupper($alpha);
			} 
		}
		$randomstring = '';
		$totallength = $length;
		for ($na = 0; $na < $totallength; $na++) 
		{
				$var = (bool)rand(0,1);
				if ($var == 1 && $alpha == true) {
					$randomstring .= $alpha[(rand() % mb_strlen($alpha))];
				} else {
					$randomstring .= $nums[(rand() % mb_strlen($nums))];
				}
		}
		if($usetime == true) 
		{
			$randomstring = $randomstring.time();
		}
		return($randomstring);
	}
	public function generateUniqueReferCode($length) 
    {           

        $randomString = Yii::$app->getSecurity()->generateRandomString($length);
        $UsersRes = Users::find()->select('userID')->where(['userReferKey'=>$randomString])->asArray()->one();
        if(empty($UsersRes))
            return $randomString;
        else
            return $this->generateUniqueReferCode($length);

    }

}
