<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "livesessions".
 *
 * @property int $liveID
 * @property int $facultyID
 * @property string $liveName
 * @property string $liveStartDate
 * @property string $liveStartTime
 * @property string $liveEndTime
 * @property int $moduleID
 * @property string $liveMeetingID
 * @property string $liveMeetingPassword
 * @property string $liveMeetingURL
 * @property string $liveStatus
 * @property string $liveCreatedDate
 */
class Livesessions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'livesessions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['facultyID', 'liveName', 'liveStartDate', 'liveStartTime', 'liveEndTime', 'moduleID', 'liveMeetingID', 'liveMeetingPassword', 'liveMeetingURL'], 'required'],
            [['facultyID', 'moduleID'], 'integer'],
            [['liveStartDate', 'liveStartTime', 'liveEndTime', 'liveCreatedDate'], 'safe'],
            [['liveStatus'], 'string'],
            [['liveName'], 'string', 'max' => 100],
            [['liveMeetingID'], 'string', 'max' => 15],
            [['liveMeetingPassword'], 'string', 'max' => 25],
            [['liveMeetingURL'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'liveID' => 'Live ID',
            'facultyID' => 'Faculty ID',
            'liveName' => 'Live Name',
            'liveStartDate' => 'Live Start Date',
            'liveStartTime' => 'Live Start Time',
            'liveEndTime' => 'Live End Time',
            'moduleID' => 'Module ID',
            'liveMeetingID' => 'Live Meeting ID',
            'liveMeetingPassword' => 'Live Meeting Password',
            'liveMeetingURL' => 'Live Meeting Url',
            'liveStatus' => 'Live Status',
            'liveCreatedDate' => 'Live Created Date',
        ];
    }
	public function GetSessionsList($loginuserID=0, $page="0",$pagesize="10",$future="false",$loginfacultyID=0,$shift="")
    {
	   $Where = 	' WHERE liveStatus = "Active"  ';//AND coursePublished="Yes"';
	   if(!empty(@$moduleID))
		   $Where.= ' AND livesessions.moduleID = '.$moduleID;
	   if($future=="true")
		   $Where.= " AND liveStartDate >= '".date('Y-m-d')."'";
	   else
		   $Where.= " AND liveStartDate < '".date('Y-m-d')."'";
	   if(!empty($loginfacultyID))
		   $Where.= ' AND livesessions.facultyID = '.$loginfacultyID;
	   if(!empty($liveID))
		   $Where.= ' AND livesessions.liveID = '.$liveID;
	   if(!empty($loginuserID))
	   {
		   $Where.= " AND livesessions.liveBatch = '".$shift."'";
		   
		   $userRes = Usersubscriptions::find()->where("userID='".$loginuserID."' AND usersubscriptionEndDate >='". date('Y-m-d')."'")->one();
		   //echo "userID='".$loginuserID."' AND usersubscriptionEndDate >='". date('Y-m-d')."'";
		   //print_r($userRes);die;
		   if(!empty($userRes))
		   {
			   if($userRes->usersubscriptionShift=="Mon-Wed-Fri")
			   {
				  $Where.= " AND ( (DATE_FORMAT(liveStartDate,'%a') = 'Mon') OR 
(DATE_FORMAT(liveStartDate,'%a') = 'Wed') OR (DATE_FORMAT(liveStartDate,'%a') = 'Fri')) ";
			   }
			   else if($userRes->usersubscriptionShift=="Tue-Thu-Sat")
			   {
				  
				$Where.= " AND ( (DATE_FORMAT(liveStartDate,'%a') = 'Tue') OR 
(DATE_FORMAT(liveStartDate,'%a') = 'Thu') OR (DATE_FORMAT(liveStartDate,'%a') = 'Sat')) ";	
			   }
		   }
		   else
		   {
			  // echo "sds";
		   }
		   //$Where.= " AND livesessions.liveBatch = '".$shift."'";
		   //die;
	   }
		   
	   
	    $para = 'SELECT livesessions.*,moduleName, facultyFullName	';				
         
		$from = ' FROM livesessions';
		if($future=="true")
			$ORDERBY = " liveStartDate ASC, liveStartTime ASC";
		else	
			$ORDERBY = " liveStartDate DESC, liveStartTime ASC";
		$groupby = " GROUP BY liveID";
		$order = " ORDER BY $ORDERBY ";
		$OFFSET = " OFFSET ".$page*$pagesize; 
		$LIMIT = " LIMIT $pagesize";
		
		//if($future=="true")
		//	$LIMIT = " LIMIT 1";
		
        
		$innerJoin = " inner join coursemodule on coursemodule.moduleID = livesessions.moduleID inner join faculty on faculty.facultyID = livesessions.facultyID ";
		
		
		$sql = $para.$from.$innerJoin.$Where.$groupby.$order.$LIMIT.$OFFSET;
		///echo $sql;die;
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$result = $command->queryAll();
	   
	   
		
	   /*$result = Livesessions::find()->select('*')->join('inner join','coursemodule','coursemodule.moduleID')->where($where)->limit($pagesize)->orderby('liveStartDate DESC, liveStartTime ASC')->offset($pagesize*$page)->asArray()->all();*/
	   if(count($result)>0)
       {
           $Lessonfiles = new Lessonfiles;
		   $Livesessiondetails = new Livesessiondetails;
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$Re[$i]["complated"]	= "No";
				if($result[$i]["liveStartDate"] > date('Y-m-d'))
				  $Re[$i]["complated"]	= "Yes";
			    else if($result[$i]["liveStartDate"] = date('Y-m-d') && $result[$i]["liveEndTime"] > date('H:i:s'))
				  $Re[$i]["complated"]	= "Yes";
			    
				$Re[$i]["sessiondetails"] = $Livesessiondetails->GetDetailsList($result[$i]["liveID"]);
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
	
}
