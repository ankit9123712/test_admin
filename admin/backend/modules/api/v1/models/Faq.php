<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property int $faqID
 * @property int $faqtypeID
 * @property string $faqQuestion
 * @property string $faqAnswer
 * @property int $faqPosition
 * @property string $faqStatus
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['faqtypeID', 'faqQuestion', 'faqAnswer', 'faqStatus'], 'required'],
            [['faqtypeID', 'faqPosition'], 'integer'],
            [['faqAnswer', 'faqStatus'], 'string'],
            [['faqQuestion'], 'string', 'max' => 200],
            [['faqtypeID', 'faqQuestion'], 'unique', 'targetAttribute' => ['faqtypeID', 'faqQuestion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'faqID' => 'Faq ID',
            'faqtypeID' => 'Faqtype ID',
            'faqQuestion' => 'Faq Question',
            'faqAnswer' => 'Faq Answer',
            'faqPosition' => 'Faq Position',
            'faqStatus' => 'Faq Status',
        ];
    }
	public function GetFaqList($faqtypeID=0)
    {   
        $para = 'SELECT faqID,faqQuestion,faqAnswer,faqtypeName	';				
        $Where = ' WHERE faqStatus="Active" ';
		if(!empty($faqtypeID))
			$Where.=" AND faq.faqtypeID=".$faqtypeID;
		$from = ' FROM faq';
		$ORDERBY = " faqPosition ";
		$groupby = " GROUP BY faq.faqID";
		$order = " ORDER BY $ORDERBY ASC";
		$innerJoin = " inner join faqtype on faqtype.faqtypeID = faq.faqtypeID ";
		
		
		$sql = $para.$from.$innerJoin.$Where.$groupby.$order;
		
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$result = $command->queryAll();
		return $result;
    }
}
