<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "forummessages".
 *
 * @property int $forummessID
 * @property int $forumID
 * @property int $userID
 * @property int $facultyID
 * @property string $forummessMessage
 * @property string $forummessAttachment
 * @property string $forummessCreatedDateTime
 */
class Forummessages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forummessages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['forumID', 'forummessMessage'], 'required'],
            [['forumID', 'userID', 'facultyID'], 'integer'],
            [['forummessCreatedDateTime'], 'safe'],
            [['forummessMessage', 'forummessAttachment'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'forummessID' => 'Forummess ID',
            'forumID' => 'Forum ID',
            'userID' => 'User ID',
            'facultyID' => 'Faculty ID',
            'forummessMessage' => 'Forummess Message',
            'forummessAttachment' => 'Forummess Attachment',
            'forummessCreatedDateTime' => 'Forummess Created Date Time',
        ];
    }
	public function GetForumMessageList($forumID,$page="0",$pagesize="10")
    {
		$Re = array();
		$Where = " WHERE forumStatus='Active' ";
		if(!empty($forumID))
		{
			$Where .= " AND (forummessages.forumID='".$forumID."')";
		}
		 
		 
		$para = 'SELECT forummessages.*,faculty.facultyFullName,userFullName,userProfilePicture	, users.userUniChoice1, users.userUniChoice2, users.userUniChoice3, users.userIELTSMonth, users.userIELTSYear, users.userIELTSCity	';
		$from = ' FROM forummessages';
		$join = ' inner join forum on forum.forumID = forummessages.forumID  
		LEFT join faculty on faculty.facultyID = forummessages.facultyID 
		LEFT join users on users.userID = forummessages.userID ';
        $ORDERBY = " forummessages.forummessCreatedDateTime";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
       {
		       for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				
			
           }
	   }
	   else
	   {
		   $Re = array();
	   }
   
		
		
	   
       return $Re; 
    }
}
