<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "coursemodule".
 *
 * @property int $moduleID
 * @property string $moduleName
 * @property string $moduleType
 * @property string $moduleRemarks
 * @property string $moduleStatus
 * @property string $moduleCreatedDate
 */
class Coursemodule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coursemodule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['moduleName', 'moduleType', 'moduleStatus'], 'required'],
            [['moduleType', 'moduleStatus'], 'string'],
            [['moduleCreatedDate'], 'safe'],
            [['moduleName'], 'string', 'max' => 100],
            [['moduleRemarks'], 'string', 'max' => 200],
            [['moduleName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'moduleID' => 'Module ID',
            'moduleName' => 'Module Name',
            'moduleType' => 'Module Type',
            'moduleRemarks' => 'Module Remarks',
            'moduleStatus' => 'Module Status',
            'moduleCreatedDate' => 'Module Created Date',
        ];
    }
	public function GetCoursemoduleList($searchWord="",$moduleType="",$page="0",$pagesize="10")
    {
		$Where = ' WHERE 1=1  ';
		if(!empty($searchWord))
		{
			$Where .= ' AND moduleName LIKE "'.$searchWord.'%"';
		}
		if(!empty($moduleType))
		{
			$Where .= ' AND moduleType = "'.$moduleType.'"';
		} 
		$para = 'SELECT coursemodule.*';
		$from = ' FROM coursemodule';
        $ORDERBY = " coursemodule.moduleID";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
