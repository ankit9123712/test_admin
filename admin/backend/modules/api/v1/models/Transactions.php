<?php

namespace backend\modules\api\v1\models;

use Yii;
// Sandbox

define('PAYMENT_userName',"api-wshndn-srt-62");
define('PAYMENT_password',"466cLgl1VNQH81lsMUCBJB1vABSR22SV");
define('PAYMENT_accountId',"2975001");
define('PAYMENT_URL',"https://sandbox-secure.zift.io/gates/xurl");

define('PAYMENT_credit_holderType',"P");
define('PAYMENT_Our_AcountNo',"1584001");
//https://sandbox-portal.zift.io/gates/onboarding
define('PAYMENT_Acount_url',"https://sandbox-portal.zift.io/gates/onboarding?requestType=create");
define('PAYMENT_profileId',"2913490177");
define('PAYMENT_resellerId',"62");
define('PAYMENT_portfolioId',"200");
define('PAYMENT_OnBoardingUser',"api-wshndn-sro-62");
define('PAYMENT_OnBoardingPass',"9KcNss0KTngR25dMKt7fACLG7RP54ErQ");
define('PAYMENT_processingConfigurationScript',"proxy-unipay");
define('PAYMENT_notificationPolicy',"X");
define('PAYMENT_feeTemplateId',"200002");


// Live
/*
define('PAYMENT_userName',"api-wsnoe_r32");
define('PAYMENT_password',"CeQ60neLSPV4VHCctSZoc5n5jVrF9rak");
define('PAYMENT_accountId',"728001");
define('PAYMENT_URL',"https://secure.ziftpay.com/gates/xurl");

define('PAYMENT_credit_holderType',"P");
define('PAYMENT_Our_AcountNo',"728001");

define('PAYMENT_Acount_url',"https://manage.ziftpay.com/gates/onboarding?requestType=create");
define('PAYMENT_profileId',"25430363482");
define('PAYMENT_resellerId',"32");
define('PAYMENT_portfolioId',"200");
define('PAYMENT_feeTemplateId',"200065");

*/
require_once('/var/www/html/stripe-php-7.63.0/init.php');

define('HandlingChargePercentage',"0.10");
define('ProcessingChargePercentage',"0.029");
define('ProcessingCharge',"0.30");

define('PrefundAdditionalAmount',"1000");
use Stripe\Stripe;  
use Stripe\Charge;

/*

Management Portal 
User: contact@wishndone.com 
Pass: ContOm51200* 
URL: https://manage.ziftpay.com/ 

Integration API Credentials 
User: api-wsnoe_r32 
Pass: CeQ60neLSPV4VHCctSZoc5n5jVrF9rak 

Additional Information for Transaction API calls 
URL: https://secure.ziftpay.com/gates/xurl 
Account ID: 728001 

Additional Information for Onboarding API 
URL: https://manage.ziftpay.com/gates/onboarding 
resellerId: 32 
portfolioId: 200 
profileId: 25430363482 
feeTemplateId: 200065 
processingConfigurationScript: zift10 
merchantProfile: SSSSM 
notificationPolicy: X
 
*/
/**
 * This is the model class for table "transactions".
 *
 * @property int $transactionID
 * @property int $userID
 * @property string $transactionOrder_ID Razorpay order id
 * @property string $transactions_Payment_ID Razorpay payment id
 * @property string $transactions_Signature Razorpay signature
 * @property float $transactionTXNAMOUNT
 * @property string $transactionStatus
 * @property string $transactionPayTime
 * @property string $transactionCreatedDate
 * @property string $transactionPAYMENT_TYPE_ID
 * @property string $transactionPAYMENTMODE
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'transactionOrder_ID', 'transactions_Payment_ID', 'transactions_Signature', 'transactionTXNAMOUNT', 'transactionPayTime', 'transactionPAYMENTMODE'], 'required'],
            [['userID'], 'integer'],
            [['transactionTXNAMOUNT'], 'number'],
            [['transactionStatus'], 'string'],
            [['transactionPayTime', 'transactionCreatedDate'], 'safe'],
            [['transactionOrder_ID', 'transactions_Payment_ID', 'transactions_Signature'], 'string', 'max' => 100],
            [['transactionPAYMENT_TYPE_ID'], 'string', 'max' => 10],
            [['transactionPAYMENTMODE'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'transactionID' => 'Transaction ID',
            'userID' => 'User ID',
            'transactionOrder_ID' => 'Transaction Order ID',
            'transactions_Payment_ID' => 'Transactions Payment ID',
            'transactions_Signature' => 'Transactions Signature',
            'transactionTXNAMOUNT' => 'Transaction Txnamount',
            'transactionStatus' => 'Transaction Status',
            'transactionPayTime' => 'Transaction Pay Time',
            'transactionCreatedDate' => 'Transaction Created Date',
            'transactionPAYMENT_TYPE_ID' => 'Transaction Payment Type ID',
            'transactionPAYMENTMODE' => 'Transaction Paymentmode',
        ];
    }
	public function CreateStripeCustomer($cardToken,$name, $email, $phone)
	{
		$Settings = new Settings();
		$Res = Settings::find()->where('settingsID=1')->asArray()->all();
	 
				
		if($Res[0]["settingPGMode"]=="Sandbox")
			$Settings_StripeKey=$Res[0]["settingsPGSandboxSecret"];
		else
			$Settings_StripeKey=$Res[0]["settingsPGLiveSecret"];
		
		
		
			\Stripe\Stripe::setApiKey($Settings_StripeKey);
			$customerID="";
			$cardID="";
			try {
				 $CustomerCreate = \Stripe\Customer::create(
					array("email" => $email,
					"phone" => $phone,
					"name" => $name,
					"source"=>$cardToken)
				 );
				 $customerID = $CustomerCreate->id;//cus_I9ebTctFPckqoi
				 //$customerID;
				 //print_r($CustomerCreate);die;

				  } catch(\Stripe\Error\Card $e) {
					// Since it's a decline, \Stripe\Error\Card will be caught
					$body = $e->getJsonBody();
					$err  = $body['error'];
				   
				  //echo  $Error_Msg = 'Message is:' . $err['message'];

				  } catch (\Stripe\Error\RateLimit $e) {
					$Error_Msg = "Too many requests made to the API too quickly.";
				  } catch (\Stripe\Error\InvalidRequest $e) 
				  {
					$Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					$msg= $e->getMessage();
					//var_dump($msg);
					//echo strpos($msg,"such customer");
					if(strpos($msg,$cardToken) >0)
					{
						$Error_Msg="";
					}
					if(strpos($msg,"such customer") >0)
					{
						 $Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					}					
					
				  } catch (\Stripe\Error\Authentication $e) {
					$Error_Msg = "Authentication with Stripe's API failed
					(maybe you changed API keys recently).";

				  } catch (\Stripe\Error\ApiConnection $e) {
					$Error_Msg = "Network communication with Stripe failed.";
				  } catch (\Stripe\Error\Base $e) {
					$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
				  } catch (Exception $e) {
					$Error_Msg = "Something else happened, completely unrelated to Stripe.";
				  }
				 if(empty($Error_Msg)) //attach card to customer
				 {
					 
				 }
		
			return array('status' => 'true', 'customerID' =>$customerID);
		
	}
	public function StripeBlockAmount($userID,$cardToken,$price)
	{
		$Settings = new Settings();
		$Res = Settings::find()->where('settingsID=1')->asArray()->all();
		//print_r($Res); die; 
				
		if($Res[0]["settingPGMode"]=="Sandbox")
			$Settings_StripeKey=$Res[0]["settingsPGSandboxSecret"];
		else
			$Settings_StripeKey=$Res[0]["settingsPGLiveSecret"];

        $amount = round($price*100);
      
        $RegisterUserStripe_Exist = Registeruserstripe::find()->where("RegisterUserStripe_Register_User_ID = '".$userID."'")->all();
		//print_r($RegisterUserStripe_Exist);die;
		if(!empty($RegisterUserStripe_Exist))
		{
				//echo "sd"; 
				\Stripe\Stripe::setApiKey($Settings_StripeKey);
				
				try {

					 $Customer_Retrive = \Stripe\Customer::retrieve($RegisterUserStripe_Exist[0]["RegisterUserStripe_Stripe_Customer_ID"]);
					 
					 //print_r($Customer_Retrive);die; 
					 
					 //customer.sources.create
					 
					// $Customer_Card_Create = $Customer_Retrive->sources->create(array("source" => $cardToken));
					$Error_Msg="Nothing";

				  } catch(\Stripe\Error\Card $e) {
					// Since it's a decline, \Stripe\Error\Card will be caught
					$body = $e->getJsonBody();
					$err  = $body['error'];
				   
				  //echo  $Error_Msg = 'Message is:' . $err['message'];

				  } catch (\Stripe\Error\RateLimit $e) {
					$Error_Msg = "Too many requests made to the API too quickly.";
				  } catch (\Stripe\Error\InvalidRequest $e) 
				  {
					$Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					$msg= $e->getMessage();
					//echo strpos($msg,"such customer");
					if(strpos($msg,$cardToken) >0)
					{
						$Error_Msg="";
					}
					if(strpos($msg,"such customer") >0)
					{
						 $Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					}					
					
				  } catch (\Stripe\Error\Authentication $e) {
					$Error_Msg = "Authentication with Stripe's API failed
					(maybe you changed API keys recently).";

				  } catch (\Stripe\Error\ApiConnection $e) {
					$Error_Msg = "Network communication with Stripe failed.";
				  } catch (\Stripe\Error\Base $e) {
					$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
				  } catch (Exception $e) {
					$Error_Msg = "Something else happened, completely unrelated to Stripe.";
				  }
				 //var_dump($e)	;
				 //die;
				  if(empty($Error_Msg))
				  {
					 //echo "<pre>";
					// print_r($Customer_Card_Create->id); die;

					try {
				  
					  $Amount_Blocked_On_Stripe = \Stripe\Charge::create(array(
						"amount" => $amount,
						"currency" => "usd",
						"description" => "Amoun bloked for payment of appointment.",
						"capture" => false,
						"customer" => $Customer_Retrive->id,
						));

					  } catch(\Stripe\Error\Card $e) {
						// Since it's a decline, \Stripe\Error\Card will be caught
						$body = $e->getJsonBody();
						$err  = $body['error'];
					   
					   $Error_Msg = 'Message is:' . $err['message'];

					  } catch (\Stripe\Error\RateLimit $e) {
						$Error_Msg = "Too many requests made to the API too quickly.";
					  } catch (\Stripe\Error\InvalidRequest $e) {
						$Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					  } catch (\Stripe\Error\Authentication $e) {
						$Error_Msg = "Authentication with Stripe's API failed
						(maybe you changed API keys recently).";

					  } catch (\Stripe\Error\ApiConnection $e) {
						$Error_Msg = "Network communication with Stripe failed.";
					  } catch (\Stripe\Error\Base $e) {
						$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
					  } catch (Exception $e) {
						$Error_Msg = "Something else happened, completely unrelated to Stripe.";
					  }

					  if(empty($Error_Msg))
					  { 

						$Res2 = array('0' => 'true', '1' =>$Amount_Blocked_On_Stripe->id, '2' =>$RegisterUserStripe_Exist[0]["RegisterUserStripe_Stripe_Customer_ID"]);

					  }
					  else
					  {
						  $Res2 = array('0' => 'false', '1' =>$Error_Msg);
					  }
				   	
              }
			  else
              {
				  //echo "sdsd";
                    $Error_Msg = "";
                    $Registeruser = new Users();
                    $Registeruser_Result = $Registeruser->GetUserDetails($userID);

                    $email = trim($Registeruser_Result[0]['userEmail']);
                    $description = $Registeruser_Result[0]['userFirstName']."(".$Registeruser_Result[0]['userID'].") Jobco payment".

					$Error_Msg = "";

					\Stripe\Stripe::setApiKey($Settings_StripeKey);

					try {
					  
						$Customer_Create_On_Stripe = \Stripe\Customer::create(array(
						 "source" => $cardToken,
						 "email" => $email,
						 "description" => $description
						 ));

					} catch(\Stripe\Error\Card $e) {
					  // Since it's a decline, \Stripe\Error\Card will be caught
					  $body = $e->getJsonBody();
					  $err  = $body['error'];
					 
					 $Error_Msg = 'Message is:' . $err['message'];

					} catch (\Stripe\Error\RateLimit $e) {
					  $Error_Msg = "Too many requests made to the API too quickly.";
					} catch (\Stripe\Error\InvalidRequest $e) {
					  $Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					} catch (\Stripe\Error\Authentication $e) {
					  $Error_Msg = "Authentication with Stripe's API failed
					  (maybe you changed API keys recently).";

					} catch (\Stripe\Error\ApiConnection $e) {
					  $Error_Msg = "Network communication with Stripe failed.";
					} catch (\Stripe\Error\Base $e) {
					  $Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
					} catch (Exception $e) {
					  $Error_Msg = "Something else happened, completely unrelated to Stripe.";
					}
					//echo $Error_Msg;
					if(empty($Error_Msg))
					{
						$RegisterUserStripe_Register_User_ID = $userID;
						$RegisterUserStripe_Stripe_Customer_ID = $Customer_Create_On_Stripe->id;

						$Registeruserstripe_Res = Registeruserstripe::find()->where("RegisterUserStripe_Register_User_ID = $userID")->one();

						
						$Registeruserstripe_Res->RegisterUserStripe_Register_User_ID = $userID;
						$Registeruserstripe_Res->RegisterUserStripe_Stripe_Customer_ID = $RegisterUserStripe_Stripe_Customer_ID;
						
						$Registeruserstripe_Res->save(false);
						$Errors = $Registeruserstripe_Res->getErrors();
						
						if(empty($Errors))
						{
							 

							 \Stripe\Stripe::setApiKey($Settings_StripeKey);

							try {
							  
								  $Amount_Blocked_On_Stripe = \Stripe\Charge::create(array(
									"amount" => $amount,
									"currency" => "usd",
									"description" => "Amoun bloked for payment of appointment.",
									"capture" => false,
									"customer" => $RegisterUserStripe_Stripe_Customer_ID,
									));

							} catch(\Stripe\Error\Card $e) {
							  // Since it's a decline, \Stripe\Error\Card will be caught
							  $body = $e->getJsonBody();
							  $err  = $body['error'];
							 
							 $Error_Msg = 'Message is:' . $err['message'];

							} catch (\Stripe\Error\RateLimit $e) {
							  $Error_Msg = "Too many requests made to the API too quickly.";
							} catch (\Stripe\Error\InvalidRequest $e) {
							  $Error_Msg = "Invalid parameters were supplied to Stripe's API.";
							} catch (\Stripe\Error\Authentication $e) {
							  $Error_Msg = "Authentication with Stripe's API failed
							  (maybe you changed API keys recently).";

							} catch (\Stripe\Error\ApiConnection $e) {
							  $Error_Msg = "Network communication with Stripe failed.";
							} catch (\Stripe\Error\Base $e) {
							  $Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
							} catch (Exception $e) {
							  $Error_Msg = "Something else happened, completely unrelated to Stripe.";
							}

							if(empty($Error_Msg))
							{

							  $Res2 = array('0' => 'true', '1' =>$Amount_Blocked_On_Stripe->id, '2' =>$RegisterUserStripe_Stripe_Customer_ID);

							}
							else
							{
								$Res2 = array('0' => 'false', '1' =>$Error_Msg);
							}
						}
						else
						{
						   
						  $Res2 = array('0' => 'false', '1' =>'Stripe Customer ID save time error.');

						}


					}
					else
					{

					  $Res2 = array('0' => 'false', '1' =>$Error_Msg);
						
					}
              }
			  return $Res2;
			
		}
	}
	public function ListAllCards($customerToken)
	{
		$Settings = new Settings();
		$Res = Settings::find()->where('settingsID=1')->asArray()->all();
	 
				
		if($Res[0]["settingPGMode"]=="Sandbox")
			$Settings_StripeKey=$Res[0]["settingsPGSandboxSecret"];
		else
			$Settings_StripeKey=$Res[0]["settingsPGLiveSecret"];
		
		
		
			\Stripe\Stripe::setApiKey($Settings_StripeKey);
			$customerID="";
			$cardID="";
			try {
				 $CustomerCreate = \Stripe\Customer::allSources($customerToken,
					array("object" => "card")
				 );
				 $res = $CustomerCreate;//cus_I9ebTctFPckqoi
				 //$customerID;
				// print_r($CustomerCreate);die;

				  } catch(\Stripe\Error\Card $e) {
					// Since it's a decline, \Stripe\Error\Card will be caught
					$body = $e->getJsonBody();
					$err  = $body['error'];
				   
				  //echo  $Error_Msg = 'Message is:' . $err['message'];

				  } catch (\Stripe\Error\RateLimit $e) {
					$Error_Msg = "Too many requests made to the API too quickly.";
				  } catch (\Stripe\Error\InvalidRequest $e) 
				  {
					$Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					$msg= $e->getMessage();
					//var_dump($msg);
					//echo strpos($msg,"such customer");
					if(strpos($msg,$cardToken) >0)
					{
						$Error_Msg="";
					}
					if(strpos($msg,"such customer") >0)
					{
						 $Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					}					
					
				  } catch (\Stripe\Error\Authentication $e) {
					$Error_Msg = "Authentication with Stripe's API failed
					(maybe you changed API keys recently).";

				  } catch (\Stripe\Error\ApiConnection $e) {
					$Error_Msg = "Network communication with Stripe failed.";
				  } catch (\Stripe\Error\Base $e) {
					$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
				  } catch (Exception $e) {
					$Error_Msg = "Something else happened, completely unrelated to Stripe.";
				  }
				 if(empty($Error_Msg)) //attach card to customer
				 {
					 
				 }
		
			return $CustomerCreate;//array('status' => 'true', 'customerID' =>$customerID);
	}
	public function GetCard($customerToken,$cardToken)
	{
		$Settings = new Settings();
		$Res = Settings::find()->where('settingsID=1')->asArray()->all();
	 
				
		if($Res[0]["settingPGMode"]=="Sandbox")
			$Settings_StripeKey=$Res[0]["settingsPGSandboxSecret"];
		else
			$Settings_StripeKey=$Res[0]["settingsPGLiveSecret"];
		
		
		
			\Stripe\Stripe::setApiKey($Settings_StripeKey);
			$customerID="";
			$cardID="";
			try {
				 $CustomerCreate = \Stripe\Customer::retrieveSource($customerToken,
					$cardToken
				 );
				 $res = $CustomerCreate;//cus_I9ebTctFPckqoi
				 //$customerID;
				// print_r($CustomerCreate);die;

				  } catch(\Stripe\Error\Card $e) {
					// Since it's a decline, \Stripe\Error\Card will be caught
					$body = $e->getJsonBody();
					$err  = $body['error'];
				   
				  //echo  $Error_Msg = 'Message is:' . $err['message'];

				  } catch (\Stripe\Error\RateLimit $e) {
					$Error_Msg = "Too many requests made to the API too quickly.";
				  } catch (\Stripe\Error\InvalidRequest $e) 
				  {
					$Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					$msg= $e->getMessage();
					//var_dump($msg);
					//echo strpos($msg,"such customer");
					if(strpos($msg,$cardToken) >0)
					{
						$Error_Msg="";
					}
					if(strpos($msg,"such customer") >0)
					{
						 $Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					}					
					
				  } catch (\Stripe\Error\Authentication $e) {
					$Error_Msg = "Authentication with Stripe's API failed
					(maybe you changed API keys recently).";

				  } catch (\Stripe\Error\ApiConnection $e) {
					$Error_Msg = "Network communication with Stripe failed.";
				  } catch (\Stripe\Error\Base $e) {
					$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
				  } catch (Exception $e) {
					$Error_Msg = "Something else happened, completely unrelated to Stripe.";
				  }
				 if(empty($Error_Msg)) //attach card to customer
				 {
					 
				 }
		
			return $CustomerCreate;//array('status' => 'true', 'customerID' =>$customerID);
	}
	
	public function UpdateCard($customerToken,$cardToken,$name,$exp_month,$exp_year)
	{
		$Settings = new Settings();
		$Res = Settings::find()->where('settingsID=1')->asArray()->all();
	 
				
		if($Res[0]["settingPGMode"]=="Sandbox")
			$Settings_StripeKey=$Res[0]["settingsPGSandboxSecret"];
		else
			$Settings_StripeKey=$Res[0]["settingsPGLiveSecret"];
		
		
		
			\Stripe\Stripe::setApiKey($Settings_StripeKey);
			$customerID="";
			$cardID="";
			try {
				 $CustomerCreate = \Stripe\Customer::updateSource($customerToken,$cardToken,
					array("name" => $name,"exp_month" => $exp_month,"exp_year" => $exp_year)
				 );
				 $res = $CustomerCreate;//cus_I9ebTctFPckqoi
				 //$customerID;
				// print_r($CustomerCreate);die;

				  } catch(\Stripe\Error\Card $e) {
					// Since it's a decline, \Stripe\Error\Card will be caught
					$body = $e->getJsonBody();
					$err  = $body['error'];
				   
				  //echo  $Error_Msg = 'Message is:' . $err['message'];

				  } catch (\Stripe\Error\RateLimit $e) {
					$Error_Msg = "Too many requests made to the API too quickly.";
				  } catch (\Stripe\Error\InvalidRequest $e) 
				  {
					$Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					$msg= $e->getMessage();
					//var_dump($msg);
					//echo strpos($msg,"such customer");
					if(strpos($msg,$cardToken) >0)
					{
						$Error_Msg="";
					}
					if(strpos($msg,"such customer") >0)
					{
						 $Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					}					
					
				  } catch (\Stripe\Error\Authentication $e) {
					$Error_Msg = "Authentication with Stripe's API failed
					(maybe you changed API keys recently).";

				  } catch (\Stripe\Error\ApiConnection $e) {
					$Error_Msg = "Network communication with Stripe failed.";
				  } catch (\Stripe\Error\Base $e) {
					$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
				  } catch (Exception $e) {
					$Error_Msg = "Something else happened, completely unrelated to Stripe.";
				  }
				 if(empty($Error_Msg)) //attach card to customer
				 {
					 
				 }
		
			return $CustomerCreate;//array('status' => 'true', 'customerID' =>$customerID);
	}

	public function DeleteCard($customerToken,$cardToken)
	{
		$Settings = new Settings();
		$Res = Settings::find()->where('settingsID=1')->asArray()->all();
	 
				
		if($Res[0]["settingPGMode"]=="Sandbox")
			$Settings_StripeKey=$Res[0]["settingsPGSandboxSecret"];
		else
			$Settings_StripeKey=$Res[0]["settingsPGLiveSecret"];
		
		
		
			\Stripe\Stripe::setApiKey($Settings_StripeKey);
			$customerID="";
			$cardID="";
			try {
				 $CustomerCreate = \Stripe\Customer::deleteSource($customerToken,$cardToken,
					array()
				 );
				 $res = $CustomerCreate;//cus_I9ebTctFPckqoi
				 //$customerID;
				// print_r($CustomerCreate);die;

				  } catch(\Stripe\Error\Card $e) {
					// Since it's a decline, \Stripe\Error\Card will be caught
					$body = $e->getJsonBody();
					$err  = $body['error'];
				   
				  //echo  $Error_Msg = 'Message is:' . $err['message'];

				  } catch (\Stripe\Error\RateLimit $e) {
					$Error_Msg = "Too many requests made to the API too quickly.";
				  } catch (\Stripe\Error\InvalidRequest $e) 
				  {
					$Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					$msg= $e->getMessage();
					//var_dump($msg);
					//echo strpos($msg,"such customer");
					if(strpos($msg,$cardToken) >0)
					{
						$Error_Msg="";
					}
					if(strpos($msg,"such customer") >0)
					{
						 $Error_Msg = "Invalid parameters were supplied to Stripe's API.";
					}					
					
				  } catch (\Stripe\Error\Authentication $e) {
					$Error_Msg = "Authentication with Stripe's API failed
					(maybe you changed API keys recently).";

				  } catch (\Stripe\Error\ApiConnection $e) {
					$Error_Msg = "Network communication with Stripe failed.";
				  } catch (\Stripe\Error\Base $e) {
					$Error_Msg = "Display a very generic error to the user, and maybe send yourself an email.";
				  } catch (Exception $e) {
					$Error_Msg = "Something else happened, completely unrelated to Stripe.";
				  }
				 if(empty($Error_Msg)) //attach card to customer
				 {
					 
				 }
		
			return $CustomerCreate;//array('status' => 'true', 'customerID' =>$customerID);
	}

	
/*	function MakeInitialPayment($user_id)
{
    require __DIR__ . '/../api/db_config.php';

    $query_chk = $mysqli->query("SELECT * from `user_banking_details` WHERE `user_id`='" . $user_id . "';");
    $row = $query_chk->fetch_assoc();

    $account_no = $row['banking_card_no'];
    $accountAccessory = $row['banking_card_expire_month'] . substr($row['banking_card_expire_year'], 2);

    $query_payment = $mysqli->query("INSERT INTO `payments` (`id`, `user_id`, `payment_type`, `type`, `post_id`, `offer_id`,`transaction_typ`, `name`, `account_no`, `account_accessory`, `amount`, `job_amount`, `job_duration`, `post_tax_amount`, `post_processing_amount`, `total`, `url`, `response_status`, `response`, `refund_main_id`, `refund_status`, `transactionId`, `status`, `created`, `reponse_dtstamp`) VALUES (NULL, '" . $user_id . "', 'Payment', 'Verify', '0', '0', 'Credit', '" . $row['banking_name'] . "', '" . $account_no . "', '" . $accountAccessory . "', '', '', '', '', '', '100', '', '', '', '0', 'N', '', 'Y', NOW(), NOW());");
    $pay_id = $mysqli->insert_id;
    $ex_code = "0000000000";
    $transactionCode = substr_replace($ex_code, $pay_id, strlen($ex_code) - strlen($pay_id));
    $customerAccountCode = substr_replace($ex_code, $user_id, strlen($ex_code) - strlen($user_id));

    $url = PAYMENT_URL . "?requestType=sale&userName=" . PAYMENT_userName . "&password=" . PAYMENT_password . "&accountId=" . PAYMENT_accountId . "&amount=100&accountType=R&transactionIndustryType=RE&holderType=P&holderName=" . urlencode($row['banking_name']) . "&accountNumber=" . $account_no . "&accountAccessory=" . $accountAccessory . "&street=&city=&state=&zipCode=&customerAccountCode=" . $customerAccountCode . "&transactionCode=" . $transactionCode;

    $postdata = http_build_query(
        array(
            'requestType' => 'sale',
            'userName' => PAYMENT_userName,
            'password' => PAYMENT_password,
            'accountId' => PAYMENT_accountId,
            'amount' => 100,
            'accountType' => 'R',
            'transactionIndustryType' => 'RE',
            'holderType' => PAYMENT_credit_holderType,
            'holderName' => urlencode($row['withdrawal_name']),
            'accountNumber' => $account_no,
            'accountAccessory' => $accountAccessory,
            'street' => '',
            'city' => '',
            'state' => '',
            'zipCode' => '',
            'customerAccountCode' => $customerAccountCode,
            'transactionCode' => $transactionCode,
        )
    );

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );

    $context  = stream_context_create($opts);

    $response = file_get_contents(PAYMENT_URL, false, $context);

    //$response = file_get_contents($url);
    $response_tmp = $response;
    parse_str($response, $response_ary);
    $mysqli->query("UPDATE `payments` SET `url`='" . $url . "', `response_status`='" . $response_ary['providerResponseMessage'] . "', `transactionId`='" . $response_ary['transactionId'] . "', `response`='" . $response_tmp . "', `reponse_dtstamp`=NOW() WHERE `id`='" . $pay_id . "';");
    $response_ary['pay_id'] = $pay_id;
    return $response_ary;
}

function MakeAdditionalPayment($user_id,$amt,$post_id,$offer_id)
{
    require __DIR__ . '/../api/db_config.php';

    $query_chk = $mysqli->query("SELECT * from `user_banking_details` WHERE `user_id`='" . $user_id . "';");
    $row = $query_chk->fetch_assoc();

    $account_no = $row['banking_card_no'];
    $accountAccessory = $row['banking_card_expire_month'] . substr($row['banking_card_expire_year'], 2);

    $query_payment = $mysqli->query("INSERT INTO `payments` (`id`, `user_id`, `payment_type`, `type`, `post_id`, `offer_id`,`transaction_typ`, `name`, `account_no`, `account_accessory`, `amount`, `job_amount`, `job_duration`, `post_tax_amount`, `post_processing_amount`, `total`, `url`, `response_status`, `response`, `refund_main_id`, `refund_status`, `transactionId`, `status`, `created`, `reponse_dtstamp`) VALUES (NULL, '" . $user_id . "', 'Payment', 'Post', '".$post_id."', '".$offer_id."', 'Credit', '" . $row['banking_name'] . "', '" . $account_no . "', '" . $accountAccessory . "', '', '', '', '', '', '".$amt."', '', '', '', '0', 'N', '', 'Y', NOW(), NOW());");
    $pay_id = $mysqli->insert_id;
    $ex_code = "0000000000";
    $transactionCode = substr_replace($ex_code, $pay_id, strlen($ex_code) - strlen($pay_id));
    $customerAccountCode = substr_replace($ex_code, $user_id, strlen($ex_code) - strlen($user_id));

    $url = PAYMENT_URL . "requestType=sale&userName=" . PAYMENT_userName . "&password=" . PAYMENT_password . "&accountId=" . PAYMENT_accountId . "&amount=".$amt."&accountType=R&transactionIndustryType=RE&holderType=P&holderName=" . urlencode($row['banking_name']) . "&accountNumber=" . $account_no . "&accountAccessory=" . $accountAccessory . "&street=&city=&state=&zipCode=&customerAccountCode=" . $customerAccountCode . "&transactionCode=" . $transactionCode;

    $postdata = http_build_query(
        array(
            'requestType' => 'sale',
            'userName' => PAYMENT_userName,
            'password' => PAYMENT_password,
            'accountId' => PAYMENT_accountId,
            'amount' => $amt,
            'accountType' => 'R',
            'transactionIndustryType' => 'RE',
            'holderType' => 'P',
            'holderName' => urlencode($row['banking_name']),
            'accountNumber' => $account_no,
            'accountAccessory' => $accountAccessory,
            'street' => '',
            'city' => '',
            'state' => '',
            'zipCode' => '',
            'customerAccountCode' => $customerAccountCode,
            'transactionCode' => $transactionCode,
        )
    );

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );

    $context  = stream_context_create($opts);

    $response = file_get_contents(PAYMENT_URL, false, $context);

    //$response = file_get_contents($url);
    $response_tmp = $response;
    parse_str($response, $response_ary);
    $mysqli->query("UPDATE `payments` SET `url`='" . $url . "', `response_status`='" . $response_ary['providerResponseMessage'] . "', `transactionId`='" . $response_ary['transactionId'] . "', `response`='" . $response_tmp . "', `reponse_dtstamp`=NOW() WHERE `id`='" . $pay_id . "';");
    $response_ary['pay_id'] = $pay_id;
    return $response_ary;
}

function refundAmount($pay_id, $user_id)
{
    require __DIR__ . '/../api/db_config.php';

    $query_pay = $mysqli->query("SELECT * from `payments` WHERE `id`='" . $pay_id . "';");
    $row_pay = $query_pay->fetch_assoc();

    $query_payment = $mysqli->query("INSERT INTO `payments` (`id`, `user_id`, `payment_type`, `type`, `post_id`, `offer_id`,`transaction_typ`, `name`, `account_no`, `account_accessory`, `amount`, `job_amount`, `job_duration`, `post_tax_amount`, `post_processing_amount`, `total`, `url`, `response_status`, `response`, `refund_main_id`, `refund_status`, `transactionId`, `status`, `created`, `reponse_dtstamp`) VALUES (NULL, '" . $user_id . "', 'Refund', '" . $row_pay['type'] . "', '" . $row_pay['post_id'] . "', '" . $row_pay['offer_id'] . "', '', '', '', '', '', '', '', '', '', '" . $row_pay['total'] . "', '', '', '', '" . $pay_id . "', 'Y', '', 'Y', NOW(), NOW());");
    $pay_id = $mysqli->insert_id;
    $ex_code = "0000000000";
    $transactionCode = substr_replace($ex_code, $pay_id, strlen($ex_code) - strlen($pay_id));
    $customerAccountCode = substr_replace($ex_code, $user_id, strlen($ex_code) - strlen($user_id));
    $response_ary = array();
    $url = PAYMENT_URL . "requestType=refund&userName=" . PAYMENT_userName . "&password=" . PAYMENT_password . "&accountId=" . PAYMENT_accountId . "&transactionId=" . $row_pay['transactionId'];

    $postdata = http_build_query(
        array(
            'requestType' => 'refund',
            'userName' => PAYMENT_userName,
            'password' => PAYMENT_password,
            'accountId' => PAYMENT_accountId,
            'transactionId' => $row_pay['transactionId']
        )
    );

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );

    $context  = stream_context_create($opts);

    $response = file_get_contents(PAYMENT_URL, false, $context);

    //$response = file_get_contents($url);
    $response_tmp = $response;
    parse_str($response, $response_ary);
    $mysqli->query("UPDATE `payments` SET `url`='" . $url . "', `response_status`='" . $response_ary['responseCode'] . "', `transactionId`='" . $response_ary['transactionId'] . "', `response`='" . $response_tmp . "', `reponse_dtstamp`=NOW() WHERE `id`='" . $pay_id . "';");

    return $response_ary;

}
*/
public function CreatePgAccount($professionalID)
{
    
	$Professions = new Professionals;
	
	$profInfo = $Professions->GetProfessionalDetails($professionalID);
 

 

    $para_ary=array();
    $para_ary['userName']=PAYMENT_OnBoardingUser;
    $para_ary['password']=PAYMENT_OnBoardingPass;
	$para_ary['profileId']=PAYMENT_profileId;
    $para_ary['resellerId']=PAYMENT_resellerId;
	$para_ary['portfolioId']=PAYMENT_portfolioId;
	$para_ary['feeTemplateId']=PAYMENT_feeTemplateId;
	$para_ary['processingConfigurationScript']=PAYMENT_processingConfigurationScript;
	//$para_ary['merchantType']='M';
    $para_ary['merchantType']='A';
	$para_ary['merchantId']='';
	$para_ary['notifyURL']='';
	$para_ary['cancelURL']='';
    $para_ary['returnURL']='';
    $para_ary['returnURLPolicy']='page';
    $para_ary['merchantProfile']='SSSSM';
    $para_ary['merchantCreationPolicy']='A';
    $para_ary['isEmbedded']='1';
    $para_ary['pageFormat']='';
    
	
    $para_ary['notificationPolicy']='X';
    $para_ary['owner.firstName']=$profInfo[0]['professionalFirstName'];
    $para_ary['owner.lastName']=$profInfo[0]['professionalLastName'];
    $para_ary['owner.email']=$profInfo[0]['professionalEmail'];
    $para_ary['owner.street1']=$profInfo[0]["Address"][0]['addressAddressLine1'];
    $para_ary['owner.street2']=$profInfo[0]["Address"][0]['addressAddressLine2'];
    $para_ary['owner.city']=$profInfo[0]["Address"][0]['cityName'];
    $para_ary['owner.state']= substr($profInfo[0]["Address"][0]['stateName'],0,2);
    $para_ary['owner.zipCode']=substr($profInfo[0]["Address"][0]['zipCode'],0,5);
    $para_ary['owner.countryCode']=substr($profInfo[0]["Address"][0]['countryName'],0,2);
    $para_ary['owner.birthDate']="19670210";
    $para_ary['owner.socialSecurity']=substr($profInfo[0]['professionalSSN'],0,9);
    $para_ary['owner.phone']=$profInfo[0]['professionalMobile'];
	
    $para_ary['business.businessName']=$profInfo[0]['professionalFirstName']." ".$profInfo[0]['professionalLastName'];
    $para_ary['business.legalName']=$profInfo[0]['professionalFirstName']." ".$profInfo[0]['professionalLastName'];
    $para_ary['business.ownershipStructureType']='C';
    $para_ary['business.street1']=$profInfo[0]["Address"][0]['addressAddressLine1'];
    $para_ary['business.street2']=$profInfo[0]["Address"][0]['addressAddressLine2'];
    $para_ary['business.city']=$profInfo[0]["Address"][0]['cityName'];
    $para_ary['business.state']=substr($profInfo[0]["Address"][0]['stateName'],0,2);
    $para_ary['business.zipCode']=substr($profInfo[0]["Address"][0]['zipCode'],0,5);
    $para_ary['business.countryCode']=substr($profInfo[0]["Address"][0]['countryName'],0,2);
    $para_ary['business.descriptorPhone']=$profInfo[0]['professionalMobile'];
    $para_ary['business.paymentCardDescriptor']=substr(str_replace(" ","",$profInfo[0]['professionalFirstName'].$profInfo[0]['professionalLastName']), 0, 9)."-".$profInfo[0]['professionalMobile'];
    $para_ary['business.directDebitDescriptor']=substr(str_replace(" ","",$profInfo[0]['professionalFirstName'].$profInfo[0]['professionalLastName']), 0, 9)."-".$profInfo[0]['professionalMobile'];
    $para_ary['business.taxId']="123456789";//substr($profInfo[0]['professionalSSN'],0,9);
    $para_ary['business.webSite']='';
    $para_ary['business.email']=$profInfo[0]['professionalEmail'];
    $para_ary['business.description']=$profInfo[0]["ServiceTypes"][0]['serviceName'];
    $para_ary['business.merchantCategoryCode']='7399';
    $para_ary['business.currencyCode']='USD';
    $para_ary['business.contactPhone']=$profInfo[0]['professionalMobile'];
    $para_ary['business.driverLicense']='';
    $para_ary['business.driverLicenseCountryCode']='';
    $para_ary['business.driverLicenseState']='';
    $para_ary['business.registrationCountryCode']='';
    $para_ary['business.registrationState']='';
    $para_ary['business.registrationYear']='';
    $para_ary['business.workHours']='';
    $para_ary['business.timeZoneCode']='MST';
    $para_ary['business.relationshipBeginDate']='';
    $para_ary['estimates.annualDirectDebitVolume']=1000000;
    $para_ary['estimates.annualCardsVolume']=1000000;
    $para_ary['estimates.avgDirectDebitTransactionAmount']=400000;
    $para_ary['estimates.avgCardsTransactionAmount']=100;
    $para_ary['estimates.maxTransactionAmount']=100;
	
    $para_ary['deposit.bankName']=$profInfo[0]["Bank"][0]['professionalbankName'];
    $para_ary['deposit.holderName']=$profInfo[0]["Bank"][0]['professionalbankAccName'];
    $para_ary['deposit.routingNumber']=$profInfo[0]["Bank"][0]['professionalbankRouting'];
    $para_ary['deposit.accountNumber']=$profInfo[0]["Bank"][0]['professionalbankAccNumber'];
    $para_ary['deposit.accountType']='C';
    $urlPara='';
    foreach($para_ary as $para_ary_k=>$para_ary_v){
        $urlPara .="&".$para_ary_k."=".urlencode($para_ary_v);
    }
    

    //echo 
	$url = PAYMENT_Acount_url.$urlPara;
	//die;

    $response = $this->url_get_contents($url);
    $response_tmp = $response;
	//print_r($response);die;
    parse_str($response, $response_ary);
	//print_r($response_ary);die;
	if(!empty($response_ary['accountId']))
		$accountId=$response_ary['accountId'];
	else
		$accountId="0";
	$pgAccount = new Professionalpgaccount;
	$pgAccount->professionalID = $professionalID;
	$pgAccount->pgAccountID = $accountId;
	$pgAccount->pgUrl = $url;
	$pgAccount->pgResponse = $response_tmp;
	$pgAccount->pgRequest = serialize($para_ary);
	$pgAccount->save(false);
	
	//Yii::$app->db->createCommand("INSERT INTO `professionalpgaccount` (`professionalID`, `pgAccountID`, `pgUrl`, `pgResponse`, `pgRequest`, `pgCreatedDate`) VALUES ('".$professionalID."', '".$response_ary['accountId']."', '".$url."', '".$response_tmp."', '".serialize($para_ary)."', NOW());")->execute();
     

	
    return true; //$response_ary;
}

public function MakePayment($orderID,$accountType="Creditcard",$accountNumber="4111111111111111",$accountAccessory="0422",$csc="123",$holderName="",$phone="",$email="",$amount="120",$paymentType="Order")
{
 
 
	if($accountType=="Creditcard")
	{
		$accType="R";
	}
	
	$sandboxAmount = $amount*100;
	if ($sandboxAmount > 5000)
		$sandboxAmount= 5000;
	
	/*sale-auth*/
	$para_ary=array();
	
	$para_ary['requestType']='sale-auth';
	$para_ary['userName']=PAYMENT_userName;
	$para_ary['password']=PAYMENT_password;
	$para_ary['accountId']=PAYMENT_accountId;
	$para_ary['amount']=$sandboxAmount; // in cents
	$para_ary['accountType']=$accType;  /* Required: Payment account type R - Credit Card C - ACH checking account S - ACH Savings account*/
	$para_ary['transactionIndustryType']='RE';
	$para_ary['accountNumber']=$accountNumber; //Required: Credit card number or bank account number
	$para_ary['accountAccessory']=$accountAccessory; //Required: Card expiration date or routing number
	$para_ary['csc']=$csc; //Optional:  CSC/CVV code from a credit card
	$para_ary['holderName']=$holderName; //Optional: Name on card
	$para_ary['phone']=$phone; //Optional: Cardholder's phone number.  No punctuation or separators
	$para_ary['email']=$email; //optional email
	$para_ary['street']='';
	$para_ary['city']='';
	$para_ary['state']='';
	$para_ary['zipCode']='';
	$para_ary['countryCode']='US';
	$urlPara='';
	foreach($para_ary as $para_ary_k=>$para_ary_v){
        $urlPara .="&".$para_ary_k."=".urlencode($para_ary_v);
    }
    

    

    $response = $this->url_post_contents(PAYMENT_URL,$urlPara);
    $response_tmp = $response;
	//print_r($response);die;
	parse_str($response, $response_ary);
	//print_r($response_ary);die;
	
	if(!empty($response_ary['accountId']))
	{
		$Orderpayments = new Orderpayments;
		$Orderpayments->orderID = $orderID;
		$Orderpayments->paymentApprovalcode =$response_ary['approvalCode'];
		$Orderpayments->paymentAccountNumberMasked =$response_ary['accountNumberMasked'];
		$Orderpayments->paymentTransactionId =$response_ary['transactionId'];
		$Orderpayments->paymentToken =$response_ary['token'];
		$Orderpayments->paymentAccountID =$response_ary['accountId'];
		$Orderpayments->paymentUrl = PAYMENT_URL;
		$Orderpayments->paymentRequest= serialize($para_ary);
		$Orderpayments->paymentResponse = $response_tmp;
		$Orderpayments->paymentAmount = $amount;
		$Orderpayments->paymentType = $paymentType;
		
		
		
		//get transation id
		$transactionId = $response_ary['transactionId'];
		
		/*capture*/
		$para_ary=array();
		$para_ary['requestType']='capture';
		$para_ary['userName']=PAYMENT_userName;
		$para_ary['password']=PAYMENT_password;
		$para_ary['accountId']=PAYMENT_accountId;
		$para_ary['amount']=$sandboxAmount; // in cents
		$para_ary['transactionId']=$transactionId;  
		$urlPara='';
		foreach($para_ary as $para_ary_k=>$para_ary_v)
		{
        $urlPara .="&".$para_ary_k."=".urlencode($para_ary_v);
		}
    

		//echo 
		$url = PAYMENT_URL.$urlPara;
		//die;

		$response = $this->url_post_contents(PAYMENT_URL,$urlPara);
		$response_tmp = $response;
		//print_r($response);die;
		parse_str($response, $response_ary);
		//print_r($response_ary);die;
		if(!empty($response_ary['accountId']))
		{
			$Orderpayments->save(false);
			return $response_ary;
		}
		else
		{
			return $response_ary;	
		}
	}
	else
	{
		return $response_ary;
	}
		
}

public function RefundPayment($orderID,$amount="50",$paymentType="Order")
{
	//echo $amount;die;
	$orderpayments = new Orderpayments;
	$orderpaymentID = Orderpayments::find()->where('orderID ='.$orderID)->one();
	if(!empty($orderpaymentID))
	{
		 
		
		$sandboxAmount = $amount*100;
		if ($sandboxAmount > 100)
			$sandboxAmount= 100;
		
		/*sale-auth*/
		$para_ary=array();
		
		$para_ary['requestType']='refund';
		$para_ary['userName']=PAYMENT_userName;
		$para_ary['password']=PAYMENT_password;
		$para_ary['accountId']=PAYMENT_accountId;
		$para_ary['amount']=$sandboxAmount; // in cents
		$para_ary['transactionId']=$orderpaymentID->paymentTransactionId;  
		$urlPara='';
		foreach($para_ary as $para_ary_k=>$para_ary_v){
			$urlPara .="&".$para_ary_k."=".urlencode($para_ary_v);
		}
		$response = $this->url_post_contents(PAYMENT_URL,$urlPara);
		$response_tmp = $response;
		//print_r($response);die;
		parse_str($response, $response_ary);
		//print_r($response_ary);die;
		
		if(!empty($response_ary['accountId']))
		{
			$Orderpayments = new Orderpayments;
			$Orderpayments->orderID = $orderID;
			$Orderpayments->paymentApprovalcode =$response_ary['cycleCode'];
			$Orderpayments->paymentAccountNumberMasked =$orderpaymentID->paymentAccountNumberMasked;
			$Orderpayments->paymentTransactionId =$response_ary['transactionId'];
			$Orderpayments->paymentToken ="";
			$Orderpayments->paymentAccountID =$response_ary['accountId'];
			$Orderpayments->paymentUrl = PAYMENT_URL;
			$Orderpayments->paymentRequest= serialize($para_ary);
			$Orderpayments->paymentResponse = $response_tmp;
			$Orderpayments->paymentAmount = $amount;
			$Orderpayments->paymentType = $paymentType;
			$Orderpayments->paymentMode = "Refund";
			$Orderpayments->save(false);
			return $response_ary;
			 
		}
		else
		{
			return $response_ary;
		}
	}
	
		
}


public function PayMerchant($professionalID)
{
	$pgAccount = new Professionalpgaccount;
	$bank = new Professionalbank;
	//get merchant ID
	$pgMerchant = Professionalpgaccount::find()->where('professionalID ='.$professionalID)->one();
	if(!empty($pgMerchant))
		$merchantID = $pgMerchant->pgAccountID;
	
	//get bank details
	$bankDetails = Professionalbank::find()->where('professionalID ='.$professionalID)->one();
	if(!empty($bankDetails))
	{
		$professionalbankAccName	 = $bankDetails->professionalbankAccName	;
		$professionalbankAccNumber	 = $bankDetails->professionalbankAccNumber	;
		$professionalbankName	 = $bankDetails->professionalbankName	;
		$professionalbankRouting	 = $bankDetails->professionalbankRouting	;
	}
	
	if(!empty($professionalbankAccName))
	{
		$requestType="Sale";
		$para_ary=array();
	
		$para_ary['requestType']=$requestType;
		$para_ary['userName']=PAYMENT_userName;
		$para_ary['password']=PAYMENT_password;
		$para_ary['accountId']=PAYMENT_accountId;
		$para_ary['amount']=$sandboxAmount; // in cents
		$para_ary['accountType']=$accType;  /* Required: Payment account type R - Credit Card C - ACH checking account S - ACH Savings account*/
		$para_ary['transactionIndustryType']='RE';
		$para_ary['accountNumber']=$accountNumber; //Required: Credit card number or bank account number
		$para_ary['accountAccessory']=$accountAccessory; //Required: Card expiration date or routing number
		$para_ary['csc']=$csc; //Optional:  CSC/CVV code from a credit card
		$para_ary['holderName']=$holderName; //Optional: Name on card
		$para_ary['phone']=$phone; //Optional: Cardholder's phone number.  No punctuation or separators
		$para_ary['email']=$email; //optional email
		$para_ary['street']='';
		$para_ary['city']='';
		$para_ary['state']='';
		$para_ary['zipCode']='';
		$para_ary['countryCode']='US';
		$para_ary['holderType']=PAYMENT_credit_holderType;
		
		//customerAccountCode
		//transactionCode
		$urlPara='';
		foreach($para_ary as $para_ary_k=>$para_ary_v){
			$urlPara .="&".$para_ary_k."=".urlencode($para_ary_v);
		}
		
		
		

		$response = $this->url_post_contents(PAYMENT_URL,$urlPara);
		$response_tmp = $response;
		//print_r($response);die;
		parse_str($response, $response_ary);
		//print_r($response_ary);die;
	}
}

 /*
function SendPayment($user_id, $typ, $id, $amt)
{
    require __DIR__ . '/../api/db_config.php';

    $query_chk = $mysqli->query("SELECT * from `user_banking_details` WHERE `user_id`='" . $user_id . "';");
    $row = $query_chk->fetch_assoc();

    $account_no = $row['withdrawal_account_no'];
    $accountAccessory = $row['withdrawal_account_accessory'];
    $paymentAccountType = PAYMENT_credit_accountType;


    $query_payment = $mysqli->query("INSERT INTO `payments` (`id`, `user_id`, `payment_type`, `type`, `type_id`,`transaction_typ`, `name`, `account_no`, `account_accessory`, `amount`, `url`, `response_status`, `response`, `refund_main_id`, `refund_status`, `transactionId`, `status`, `created`, `reponse_dtstamp`) VALUES (NULL, '" . $user_id . "', '" . PAYMENT_credit_requestType . "', '" . $typ . "', '" . $id . "', '" . $row['banking_typ'] . "', '" . $row['banking_name'] . "', '" . $account_no . "', '" . $accountAccessory . "', '" . $amt . "', '', '', '', '0', 'Y', '', 'Y', NOW(), NOW());");
    $pay_id = $mysqli->insert_id;
    $ex_code = "0000000000";
    $transactionCode = substr_replace($ex_code, $pay_id, strlen($ex_code) - strlen($pay_id));
    $customerAccountCode = substr_replace($ex_code, $user_id, strlen($ex_code) - strlen($user_id));

    $url = PAYMENT_URL . "requestType=" . PAYMENT_credit_requestType . "&userName=" . PAYMENT_userName . "&password=" . PAYMENT_password . "&accountId=" . PAYMENT_accountId . "&amount=" . $amt . "&accountType=" . $paymentAccountType . "&transactionIndustryType=" . PAYMENT_credit_transactionIndustryType . "&holderType=" . PAYMENT_credit_holderType . "&holderName=" . urlencode($row['withdrawal_name']) . "&accountNumber=" . $account_no . "&accountAccessory=" . $accountAccessory . "&street=&city=&state=&zipCode=&customerAccountCode=" . $customerAccountCode . "&transactionCode=" . $transactionCode;

    $postdata = http_build_query(
        array(
            'requestType' => 'sale',
            'userName' => PAYMENT_userName,
            'password' => PAYMENT_password,
            'accountId' => PAYMENT_accountId,
            'amount' => $amt,
            'accountType' => $paymentAccountType,
            'transactionIndustryType' => 'RE',
            'holderType' => PAYMENT_credit_holderType,
            'holderName' => urlencode($row['withdrawal_name']),
            'accountNumber' => $account_no,
            'accountAccessory' => $accountAccessory,
            'street' => '',
            'city' => '',
            'state' => '',
            'zipCode' => '',
            'customerAccountCode' => $customerAccountCode,
            'transactionCode' => $transactionCode,
        )
    );

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );

    $context  = stream_context_create($opts);

    $response = file_get_contents(PAYMENT_URL, false, $context);

    //$response = file_get_contents($url);

    parse_str($response, $response_ary);
    $mysqli->query("UPDATE `payments` SET `url`='" . $url . "', `response_status`='" . $response_ary['providerResponseMessage'] . "', `transactionId`='" . $response_ary['transactionId'] . "', `response`='" . $response . "', `reponse_dtstamp`=NOW() WHERE `id`='" . $pay_id . "';");


    return $response_ary;
}

function paymentCheck($user_id)
{
    require __DIR__ . '/../api/db_config.php';

    $query_chk = $mysqli->query("SELECT * from `user_banking_details` WHERE `user_id`='" . $user_id . "';");
    $row = $query_chk->fetch_assoc();
    return $row;
}
*/
	public function url_get_contents ($Url) {
    if (!function_exists('curl_init')){ 
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
public function url_post_contents ($Url,$params) {
	//print_r($params);die;
    if (!function_exists('curl_init')){ 
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
}
