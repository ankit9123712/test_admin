<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "forum".
 *
 * @property int $forumID
 * @property string $forumName
 * @property string $forumSubject
 * @property string $AssiDescription
 * @property string $forumFiles
 * @property string $forumPic
 * @property string $forumStatus
 * @property string $forumCreatedDate
 * @property int $moduleID
 * @property int $facultyID
 */
class Forum extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forum';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['forumName', 'forumSubject', 'AssiDescription', 'forumFiles', 'forumPic', 'moduleID'], 'required'],
            [['AssiDescription', 'forumStatus'], 'string'],
            [['forumCreatedDate'], 'safe'],
            [['moduleID', 'facultyID'], 'integer'],
            [['forumName', 'forumSubject'], 'string', 'max' => 100],
            [['forumFiles'], 'string', 'max' => 4000],
            [['forumPic'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'forumID' => 'Forum ID',
            'forumName' => 'Forum Name',
            'forumSubject' => 'Forum Subject',
            'AssiDescription' => 'Assi Description',
            'forumFiles' => 'Forum Files',
            'forumPic' => 'Forum Pic',
            'forumStatus' => 'Forum Status',
            'forumCreatedDate' => 'Forum Created Date',
            'moduleID' => 'Module ID',
            'facultyID' => 'Faculty ID',
        ];
    }
	public function GetForumList($moduleID,$facultyID,$usertype,$page="0",$pagesize="10",$forumID=0,$searchWord="")
    {
		$Re = array();
		$Where = " WHERE forumStatus='Active' ";
		if(!empty($facultyID))
		{
			$Where .= " AND (forum.facultyID='".$facultyID."')";
		}
		if(!empty($searchWord))
		{
			$Where .= " AND (forum.forumName LIKE '".$searchWord."%')";
		}
		if(!empty($forumID))
		{
			$Where .= " AND (forum.forumID='".$forumID."')";
		}
		if(!empty($moduleID))
		{
			$Where .= ' AND forum.moduleID="'.$moduleID.'"';
		}
		if(!empty($usertype))
		{
			$Where .= ' AND moduleType="'.$usertype.'"';
		} 
		$para = 'SELECT forum.*,faculty.facultyFullName,coursemodule.moduleName, coursemodule.moduleType	';
		$from = ' FROM forum';
		$join = ' inner join faculty on faculty.facultyID = forum.facultyID ';
		$join .= ' inner join coursemodule on coursemodule.moduleID = forum.moduleID ';
        $ORDERBY = " forum.forumCreatedDate";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		
		if(count($result)>0)
       {
		       for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				//$resSubscription = $Usersubscriptions->GetUserSubscriptionplanList($UserID,0,10);
				// $Re[$i]['mySubscriptions'] = $resSubscription;
				 $Re[$i]['count'] = $countMsgs = Forummessages::find()->where('forumID='.$result[$i]["forumID"])->count();
			
           }
	   }
	   else
	   {
		   $Re = array();
	   }
   
		
		
	   
       return $Re; 
    }
	public function GetForumCount($moduleID,$facultyID,$usertype,$forumID=0,$searchWord="")
    {
		$Re = array();
		$Where = " WHERE forumStatus='Active' ";
		if(!empty($facultyID))
		{
			$Where .= " AND (forum.facultyID='".$facultyID."')";
		}
		if(!empty($searchWord))
		{
			$Where .= " AND (forum.forumName LIKE '".$searchWord."%')";
		}
		if(!empty($forumID))
		{
			$Where .= " AND (forum.forumID='".$forumID."')";
		}
		if(!empty($moduleID))
		{
			$Where .= ' AND forum.moduleID="'.$moduleID.'"';
		}
		if(!empty($usertype))
		{
			$Where .= ' AND moduleType="'.$usertype.'"';
		} 
		$para = 'SELECT count(forum.forumID) as cnt 	';
		$from = ' FROM forum';
		$join = ' inner join faculty on faculty.facultyID = forum.facultyID ';
		$join .= ' inner join coursemodule on coursemodule.moduleID = forum.moduleID ';
        $ORDERBY = " forum.forumCreatedDate";
        $order = " ORDER BY $ORDERBY DESC";
        
        $sql = $para.$from.$join.$Where.$order;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		$cnt="0";
		if(count($result)>0)
       {
		       for($i=0;count($result)>$i; $i++) 
			   { 
					
					$cnt = $result[$i]["cnt"];
					 
				
			   }
	   }
	    
   
		
		
	   
       return $cnt; 
    }
}
