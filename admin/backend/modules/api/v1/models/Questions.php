<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property string $queID
 * @property int $moduleID
 * @property int $lessionID
 * @property int $examID
 * @property string $queFor
 * @property string $queType
 * @property int $queDifficultyevel
 * @property string $queQuestion
 * @property string $queSolution
 * @property string $queDisplayType
 * @property string $queStatus
 * @property string $queOption1
 * @property string $queOption2
 * @property string $queOption3
 * @property string $queOption4
 * @property string $queCorrectAns
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['moduleID', 'queFor', 'queType', 'queDifficultyevel', 'queQuestion', 'queSolution', 'queDisplayType', 'queStatus'], 'required'],
            [['moduleID', 'lessionID', 'examID', 'queDifficultyevel'], 'integer'],
            [['queFor', 'queType', 'queDisplayType', 'queStatus'], 'string'],
            [['queQuestion', 'queSolution'], 'string', 'max' => 500],
            [['queOption1', 'queOption2', 'queOption3', 'queOption4'], 'string', 'max' => 50],
            [['queCorrectAns'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'queID' => 'Que ID',
            'moduleID' => 'Module ID',
            'lessionID' => 'Lession ID',
            'examID' => 'Exam ID',
            'queFor' => 'Que For',
            'queType' => 'Que Type',
            'queDifficultyevel' => 'Que Difficultyevel',
            'queQuestion' => 'Que Question',
            'queSolution' => 'Que Solution',
            'queDisplayType' => 'Que Display Type',
            'queStatus' => 'Que Status',
            'queOption1' => 'Que Option1',
            'queOption2' => 'Que Option2',
            'queOption3' => 'Que Option3',
            'queOption4' => 'Que Option4',
            'queCorrectAns' => 'Que Correct Ans',
        ];
    }
	public function GetQuestionList($LessonID=0,$examID=0)
    {
	   $where='queStatus = "Active"'	;
	   if(!empty($LessonID))
	   {
		   $where.=' AND lessionID="'.$LessonID.'"';
	   }
	   if(!empty($examID))
	   {
		   $where.=' AND examID="'.$examID.'"';
	   }
	  // $where.=' AND chapterfileFileType="'.$chapterfileFileType.'"';
	   $result = Questions::find()->select('*')->where($where)->asArray()->all();
	   if(count($result)>0)
       {
            
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				 
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
	public function GetQuestionAnswerList($LessonID=0,$examID=0,$loginuserID=0)
    {
	   $where='useranswers.userID = '.$loginuserID;
	   if(!empty($LessonID))
	   {
		   $where.=' AND useranswers.lessionID="'.$LessonID.'"';
	   }
	   if(!empty($examID))
	   {
		   $where.=' AND useranswers.examID="'.$examID.'"';
	   }
	  // $where.=' AND chapterfileFileType="'.$chapterfileFileType.'"';
	   $result = Questions::find()->select('*')->join('inner join','useranswers', 'useranswers.queID = questions.queID')->where($where)->orderBy('questions.queID ASC')->asArray()->all();
	   if(count($result)>0)
       {
           
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
