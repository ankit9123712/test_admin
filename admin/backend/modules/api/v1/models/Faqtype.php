<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "faqtype".
 *
 * @property int $faqtypeID
 * @property string $faqtypeName
 * @property string $faqtypeRemarks
 * @property string $faqtypeStatus
 */
class Faqtype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faqtype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['faqtypeName', 'faqtypeStatus'], 'required'],
            [['faqtypeStatus'], 'string'],
            [['faqtypeName'], 'string', 'max' => 100],
            [['faqtypeRemarks'], 'string', 'max' => 400],
            [['faqtypeName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'faqtypeID' => 'Faqtype ID',
            'faqtypeName' => 'Faqtype Name',
            'faqtypeRemarks' => 'Faqtype Remarks',
            'faqtypeStatus' => 'Faqtype Status',
        ];
    }
}
