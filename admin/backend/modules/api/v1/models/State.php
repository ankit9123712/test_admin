<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property int $stateID
 * @property int $countryID
 * @property string $stateName
 * @property string $stateStatus
 * @property string $stateCreatedDate
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryID'], 'integer'],
            [['stateName'], 'required'],
            [['stateStatus'], 'string'],
            [['stateCreatedDate'], 'safe'],
            [['stateName'], 'string', 'max' => 100],
            [['countryID', 'stateName'], 'unique', 'targetAttribute' => ['countryID', 'stateName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'stateID' => 'State I D',
            'countryID' => 'Country I D',
            'stateName' => 'State Name',
            'stateStatus' => 'State Status',
            'stateCreatedDate' => 'State Created Date',
        ];
    }

    public function GetStateList($countryID="",$searchWord="",$page="0",$pagesize="10")
    { 
		$Where = 'state.stateStatus = "Active" AND country.countryStatus = "Active"';
        $Re = array();
        if(!empty($countryID))
        {
            $Where .= ' AND state.countryID = "'.$countryID.'"';
        }
		if(!empty($searchWord))
	   {
		   $Where.= " AND stateName LIKE '".$searchWord."%'";
	   }
        $result = array();
        $query = new \yii\db\Query();
        $result = $query->select('state.stateName,
					state.countryID,
					state.stateID')
        ->from('state')
        ->JOIN('INNER JOIN','country','state.countryID = country.countryID')
        ->where($Where)
        ->GROUPBY('state.stateID')
        ->ORDERBY('state.stateName ASC')
        ->all();
        return $result;
    }

    public function GetStateID($countryID="1",$stateName)
    {   
        $State_Res = State::find()->select('stateID')->where('UPPER(stateName) = UPPER("'.$stateName.'")')->asArray()->one();
        if(!empty($State_Res))
        {
            $res = $State_Res['stateID'];
        }
        else
        {
            $Errors = "";
            $State =  new State;
            $State->countryID = $countryID;
            $State->stateName = strtoupper($stateName);
            $State->stateStatus = "Active";
            $State->save();
            $Errors = $State->getErrors();
            if(empty($Errors))
            {
                $res = $State->stateID;
            }
            else
            {
                $res = "0";
            }
        }
        return $res;

    }
}
