<?php
namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "monkcategory".
 *
 * @property int $monkcatID
 * @property string $monkcatName
 * @property string $monkcatStatus
 * @property string $monkcatCreatedDate
 */
class Monkcategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monkcategory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['monkcatName'], 'required'],
            [['monkcatStatus'], 'string'],
            [['monkcatCreatedDate'], 'safe'],
            [['monkcatName'], 'string', 'max' => 100],
            [['monkcatName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'monkcatID' => 'Monkcat ID',
            'monkcatName' => 'Monkcat Name',
            'monkcatStatus' => 'Monkcat Status',
            'monkcatCreatedDate' => 'Monkcat Created Date',
        ];
    }
	public function GetMonkcategoryList($searchWord="",$page="0",$pagesize="10")
    {
		$Where = ' WHERE 1=1 AND monkcatStatus="Active" ';
		if(!empty($searchWord))
		{
			$Where .= ' AND monkcatName LIKE "'.$searchWord.'%"';
		}
		
		$para = 'SELECT monkcategory.*';
		$from = ' FROM monkcategory';
        $ORDERBY = " monkcategory.monkcatID";
        $order = " ORDER BY $ORDERBY ASC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       if(count($result)>0)
       {
		       for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				//$resSubscription = $Usersubscriptions->GetUserSubscriptionplanList($UserID,0,10);
				// $Re[$i]['mySubscriptions'] = $resSubscription;
				 $Re[$i]['count'] = Monkchat::find()->where('monkcatID='.$result[$i]["monkcatID"])->count();
			
           }
	   }
	   else
	   {
		   $Re = array();
	   }
   
		
		
	   
       return $Re; 
    }
}
