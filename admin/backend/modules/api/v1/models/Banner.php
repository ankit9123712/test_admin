<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property int $bannerID
 * @property string $bannerScreen
 * @property string $bannerName
 * @property string $bannerFileType
 * @property string $bannerFile
 * @property string $bannerRemarks
 * @property string $bannerStatus
 * @property int $bannerPosition
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bannerScreen', 'bannerName', 'bannerFile', 'bannerStatus'], 'required'],
            [['bannerFileType', 'bannerStatus'], 'string'],
            [['bannerPosition'], 'integer'],
            [['bannerScreen'], 'string', 'max' => 50],
            [['bannerName', 'bannerFile'], 'string', 'max' => 100],
            [['bannerRemarks'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bannerID' => 'Banner ID',
            'bannerScreen' => 'Banner Screen',
            'bannerName' => 'Banner Name',
            'bannerFileType' => 'Banner File Type',
            'bannerFile' => 'Banner File',
            'bannerRemarks' => 'Banner Remarks',
            'bannerStatus' => 'Banner Status',
            'bannerPosition' => 'Banner Position',
        ];
    }
	public function GetBannerList()
    {   
        $where = "bannerStatus = 'Active'";
        $Res = Banner::find()->select('bannerID,bannerName,bannerFileType,bannerFile,bannerPosition,bannerRemarks')->where($where)->ORDERBY('bannerPosition ASC')->asArray()->all();
        return $Res;
    }
}
