<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "cmspage".
 *
 * @property int $cmspageID
 * @property string $cmspageConstantCode
 * @property string $cmspageName
 * @property string $cmspageContents
 * @property string $cmspageIsSystemPage
 * @property string $cmspageStatus
 * @property string $cmspageCreatedDate
 */
class Cmspage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cmspage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cmspageConstantCode', 'cmspageContents'], 'required'],
            [['cmspageContents', 'cmspageIsSystemPage', 'cmspageStatus'], 'string'],
            [['cmspageCreatedDate'], 'safe'],
            [['cmspageConstantCode'], 'string', 'max' => 20],
            [['cmspageName'], 'string', 'max' => 100],
            [['cmspageName'], 'unique'],
            [['cmspageConstantCode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cmspageID' => 'Cmspage ID',
            'cmspageConstantCode' => 'Cmspage Constant Code',
            'cmspageName' => 'Cmspage Name',
            'cmspageContents' => 'Cmspage Contents',
            'cmspageIsSystemPage' => 'Cmspage Is System Page',
            'cmspageStatus' => 'Cmspage Status',
            'cmspageCreatedDate' => 'Cmspage Created Date',
        ];
    }
	public function GetCmspage($cmspageConstantCode)
    {   
        $where = "cmspageStatus = 'Active'";
        if(!empty($cmspageConstantCode))
        {
            $where .= " AND cmspageConstantCode = '".$cmspageConstantCode."'";
        }
        
        $Res = Cmspage::find()->select('cmspageName,cmspageContents')->where($where)->asArray()->all();
        return $Res;
    }
}
