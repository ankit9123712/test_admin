<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "bands".
 *
 * @property int $bandID
 * @property float $bandMin
 * @property float $bandMax
 * @property float $bandBand
 */
class Bands extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bands';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bandMin', 'bandMax', 'bandBand'], 'required'],
            [['bandMin', 'bandMax', 'bandBand'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bandID' => 'Band ID',
            'bandMin' => 'Band Min',
            'bandMax' => 'Band Max',
            'bandBand' => 'Band Band',
        ];
    }
}
