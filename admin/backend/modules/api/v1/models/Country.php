<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $countryID
 * @property string $countryName
 * @property string $countryISO3Code
 * @property string $countryISO2Code
 * @property string $countryCurrencyCode
 * @property string $countryDialCode
 * @property string $countryFlagImage
 * @property string $countryRemark  
 * @property string $countryStatus
 * @property string $countryCreatedDate
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryName', 'countryISO3Code', 'countryISO2Code', 'countryCurrencyCode', 'countryDialCode', 'countryFlagImage'], 'required'],
            [['countryStatus'], 'string'],
            [['countryCreatedDate'], 'safe'],
            [['countryName', 'countryFlagImage'], 'string', 'max' => 100],
            [['countryISO3Code'], 'string', 'max' => 3],
            [['countryISO2Code'], 'string', 'max' => 2],
            [['countryCurrencyCode'], 'string', 'max' => 10],
            [['countryDialCode'], 'string', 'max' => 5],
            [['countryRemark'], 'string', 'max' => 200],
            [['countryName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'countryID' => 'Country ID',
            'countryName' => 'Country Name',
            'countryISO3Code' => 'Country Iso3 Code',
            'countryISO2Code' => 'Country Iso2 Code',
            'countryCurrencyCode' => 'Country Currency Code',
            'countryDialCode' => 'Country Dial Code',
            'countryFlagImage' => 'Country Flag Image',
            'countryRemark' => 'Country Remark',
            'countryStatus' => 'Country Status',
            'countryCreatedDate' => 'Country Created Date',
        ];
    }
	public function GetCountryList($searchWord="",$page="0",$pagesize="10")
    {
	   $where = " countryStatus = 'Active'";
	   if(!empty($searchWord))
	   {
		   $where.= " AND countryName LIKE '".$searchWord."%'";
	   }
	   
	   $result = Country::find()->select('countryID,countryName,countryISO2Code,countryCurrencySymbol,countryDialCode,countryFlagImage,countryShortCode')->where($where)->orderby('countryName ASC')->asArray()->all();
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   $ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
                $Re[$i]['countryFlagImage'] = $ImagePathArray['countryFlagImage'].$result[$i]['countryFlagImage'];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
