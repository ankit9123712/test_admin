<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "chapterfavorite".
 *
 * @property int $chapterfavoriteID
 * @property int $userID
 */
class Chapterfavorite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chapterfavorite';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID'], 'required'],
            [['userID'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'chapterfavoriteID' => 'Chapterfavorite I D',
            'userID' => 'User I D',
        ];
    }
}
