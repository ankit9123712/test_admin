<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "userexamquestions".
 *
 * @property string $queID
 * @property int $userexamID
 * @property int $courseID
 * @property int $coursesubjID
 * @property int $coursechapterID
 * @property string $queType
 * @property int $queDifficultyevel
 * @property string $queQuestion
 * @property string $queSolution
 * @property string $queDisplayType
 * @property string $queStatus
 * @property int $queOldqueID
 * @property string $queOption1
 * @property string $queOption2
 * @property string $queOption3
 * @property string $queOption4
 * @property string $queCorrectAns
 * @property string $queFlag
 * @property string $queReadTime
 * @property string $queAnswered
 * @property string $queAnsweredOption
 * @property string $queAnsweredCorrect
 * @property double $queMarks
 */
class Userexamquestions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userexamquestions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userexamID', 'courseID', 'coursesubjID', 'coursechapterID', 'queType', 'queDifficultyevel', 'queQuestion', 'queStatus', 'queOldqueID'], 'required'],
            [['userexamID', 'courseID', 'coursesubjID', 'coursechapterID', 'queDifficultyevel', 'queOldqueID'], 'integer'],
            [['queType', 'queDisplayType', 'queStatus', 'queAnswered', 'queAnsweredCorrect'], 'string'],
            [['queReadTime'], 'safe'],
            [['queMarks'], 'number'],
            [['queQuestion', 'queSolution'], 'string', 'max' => 500],
            [['queOption1', 'queOption2', 'queOption3', 'queOption4', 'queFlag'], 'string', 'max' => 50],
            [['queCorrectAns', 'queAnsweredOption'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'queID' => 'Que I D',
            'userexamID' => 'Userexam I D',
            'courseID' => 'Course I D',
            'coursesubjID' => 'Coursesubj I D',
            'coursechapterID' => 'Coursechapter I D',
            'queType' => 'Que Type',
            'queDifficultyevel' => 'Que Difficultyevel',
            'queQuestion' => 'Que Question',
            'queSolution' => 'Que Solution',
            'queDisplayType' => 'Que Display Type',
            'queStatus' => 'Que Status',
            'queOldqueID' => 'Que Oldque I D',
            'queOption1' => 'Que Option1',
            'queOption2' => 'Que Option2',
            'queOption3' => 'Que Option3',
            'queOption4' => 'Que Option4',
            'queCorrectAns' => 'Que Correct Ans',
            'queFlag' => 'Que Flag',
            'queReadTime' => 'Que Read Time',
            'queAnswered' => 'Que Answered',
            'queAnsweredOption' => 'Que Answered Option',
            'queAnsweredCorrect' => 'Que Answered Correct',
            'queMarks' => 'Que Marks',
        ];
    }
	public function GetQuestions($userexamID=0)
    {

	   $Where = ' WHERE 1=1 ';
       $Re = array();
      
       if(!empty($userexamID))
       {
           $Where .= ' AND userexamID = "'.$userexamID.'"';
       }
	   
	  
	    
		$para = 'SELECT userexamquestions.*  ';
		$from = ' FROM userexamquestions';
        $ORDERBY = " userexamquestions.queID";
        $order = " ORDER BY $ORDERBY ASC";
        $LIMIT = "";//" LIMIT $pagesize";
        $OFFSET ="";// " OFFSET ".$page*$pagesize;      
		$join = '  ';		

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
	   if(count($result)>0)
       {
           
		   for($i=0;count($result)>$i; $i++) 
           { 
				
				
					
					$Re[$i] = $result[$i];
					
					
				

             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
