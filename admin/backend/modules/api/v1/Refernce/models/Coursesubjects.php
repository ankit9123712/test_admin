<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Coursechapters;
use backend\modules\api\v1\models\Exams;

/**
 * This is the model class for table "coursesubjects".
 *
 * @property int $coursesubjID
 * @property int $courseID
 * @property string $coursesubjName
 * @property string $coursesubjIcon
 * @property string $coursesubjColor
 * @property string $coursesubjDescription
 * @property int $coursesubjChapters
 * @property string $coursesubjStatus
 * @property string $coursesubjCreatedDate
 */
class Coursesubjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coursesubjects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['courseID', 'coursesubjName', 'coursesubjIcon', 'coursesubjColor', 'coursesubjDescription'], 'required'],
            [['courseID', 'coursesubjChapters'], 'integer'],
            [['coursesubjDescription', 'coursesubjStatus'], 'string'],
            [['coursesubjCreatedDate'], 'safe'],
            [['coursesubjName'], 'string', 'max' => 100],
            [['coursesubjIcon'], 'string', 'max' => 255],
            [['coursesubjColor'], 'string', 'max' => 20],
            [['courseID', 'coursesubjName'], 'unique', 'targetAttribute' => ['courseID', 'coursesubjName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coursesubjID' => 'Coursesubj I D',
            'courseID' => 'Course I D',
            'coursesubjName' => 'Coursesubj Name',
            'coursesubjIcon' => 'Coursesubj Icon',
            'coursesubjColor' => 'Coursesubj Color',
            'coursesubjDescription' => 'Coursesubj Description',
            'coursesubjChapters' => 'Coursesubj Chapters',
            'coursesubjStatus' => 'Coursesubj Status',
            'coursesubjCreatedDate' => 'Coursesubj Created Date',
        ];
    }
	public function GetCoursesubjectsList($courseID=0,$loginuserID=0,$mycourse="")
    {
	   $where='coursesubjStatus = "Active"'	;
	   if(!empty($courseID))
	   {
		   $where.=' AND courseID="'.$courseID.'"';
	   }
	   if(!empty($mycourse))
		   $where.= " AND courseID IN (SELECT courseID from usercourses WHERE userID=".$loginuserID.")";
	   $result = Coursesubjects::find()->select('*')->where($where)->orderby('coursesubjName ASC')->asArray()->all();
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   $Coursechapters = new Coursechapters;
		   $Exams = new Exams;
		   $ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$Re[$i]["chapters"] = $Coursechapters->GetCoursechaptersList($result[$i]["coursesubjID"],$loginuserID) ;
				//$Re[$i]["exams"] = $Exams->GetExam($result[$i]["courseID"],$result[$i]["coursesubjID"],"","",0,10,$loginuserID) ;
				$Re[$i]["exams"] = $Exams->getPracticeLevelExams(0,$result[$i]["coursesubjID"],0,$loginuserID) ;
				$Re[$i]['coursesubjIcon'] = $ImagePathArray['coursesubjIcon'].$result[$i]['coursesubjIcon'];
				$myRes = $Coursechapters->GetCoursechaptersCount($result[$i]["coursesubjID"]);
				if(!empty($myRes))
					$Re[$i]["coursesubjChapters"] = $myRes[0]["TotalChapters"]  ;
				else
					$Re[$i]["coursesubjChapters"] = "0" ;
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
