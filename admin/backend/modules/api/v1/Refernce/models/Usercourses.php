<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "usercourses".
 *
 * @property int $usercourseID
 * @property int $userID
 * @property int $courseID
 */
class Usercourses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usercourses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'courseID'], 'required'],
            [['userID', 'courseID'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'usercourseID' => 'Usercourse I D',
            'userID' => 'User I D',
            'courseID' => 'Course I D',
        ];
    }
}
