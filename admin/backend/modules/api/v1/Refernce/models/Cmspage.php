<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "cmspage".
 *
 * @property int $cmspageID
 * @property string $cmspageName
 * @property string $cmspageImage
 * @property string $cmspageContents
 * @property string $cmspageIsSystemPage
 * @property string $cmspageStatus
 * @property string $cmspageCreatedDate
 */
class Cmspage extends \yii\db\ActiveRecord
{
    const SCENARIO_LIST = 'list';
    public $languageID;
    public $apiType;
    public $apiVersion;
	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cmspage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['cmspageImage', 'cmspageContents'], 'required'],
            [['cmspageContents', 'cmspageIsSystemPage', 'cmspageStatus'], 'string'],
            [['cmspageCreatedDate'], 'safe'],
            [['cmspageName', 'cmspageImage'], 'string', 'max' => 100],
            [['cmspageName'], 'unique'],
        ];
    }
	
	 public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['list'] = ['cmspageName','languageID','apiType','apiVersion'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cmspageID' => 'Cmspage ID',
            'cmspageName' => 'Cmspage Name',
            'cmspageImage' => 'Cmspage Image',
            'cmspageContents' => 'Cmspage Contents',
            'cmspageIsSystemPage' => 'Cmspage Is System Page',
            'cmspageStatus' => 'Cmspage Status',
            'cmspageCreatedDate' => 'Cmspage Created Date',
        ];
    }
	public function GetCmspage($cmspageName)
    {   
        $where = "cmspageStatus = 'Active'";
        if(!empty($cmspageName))
        {
            $where .= " AND cmspageName = '".$cmspageName."'";
        }
        
        $Res = Cmspage::find()->select('cmspageName,cmspageContents')->where($where)->asArray()->all();
        return $Res;
    }
}
