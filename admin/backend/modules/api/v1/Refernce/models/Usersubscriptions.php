<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "usersubscriptions".
 *
 * @property int $subscriptionID
 * @property int $userID
 * @property int $planID
 * @property string $subscriptionStartDate
 * @property string $subscriptionEndDate
 * @property string $subscriptionStatus
 */
class Usersubscriptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usersubscriptions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'planID', 'subscriptionStartDate', 'subscriptionEndDate', 'subscriptionStatus'], 'required'],
            [['userID', 'planID'], 'integer'],
            [['subscriptionStartDate', 'subscriptionEndDate'], 'safe'],
            [['subscriptionStatus'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subscriptionID' => 'Subscription I D',
            'userID' => 'User I D',
            'planID' => 'Plan I D',
            'subscriptionStartDate' => 'Subscription Start Date',
            'subscriptionEndDate' => 'Subscription End Date',
            'subscriptionStatus' => 'Subscription Status',
        ];
    }
	public function GetUserSubscriptionplanList($userID,$page=0,$pagesize=10)
    {
	   $where = 	' 1 = 1 ';
	   if(!empty($userID))
		   $where.= ' AND userID = '.$userID;
	 
		
	   $result = Usersubscriptions::find()->select('usersubscriptions.*,planName,planDescription,planPrice,planDuration ')->join('inner join','subscriptionplan','subscriptionplan.planID = usersubscriptions.planID')->where($where)->limit($pagesize)->orderby('subscriptionEndDate ASC')->offset($pagesize*$page)->asArray()->all();
	   if(count($result)>0)
       {

		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
