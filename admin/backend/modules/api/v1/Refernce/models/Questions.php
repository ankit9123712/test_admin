<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property string $queID
 * @property int $courseID
 * @property int $coursesubjID
 * @property int $coursechapterID
 * @property string $queType
 * @property int $queDifficultyevel
 * @property string $queQuestion
 * @property string $queSolution
 * @property string $queDisplayType
 * @property string $queStatus
 * @property string $queOption1
 * @property string $queOption2
 * @property string $queOption3
 * @property string $queOption4
 * @property string $queCorrectAns
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['courseID', 'coursesubjID', 'coursechapterID', 'queType', 'queDifficultyevel', 'queQuestion', 'queSolution', 'queDisplayType', 'queStatus'], 'required'],
            [['courseID', 'coursesubjID', 'coursechapterID', 'queDifficultyevel'], 'integer'],
            [['queType', 'queDisplayType', 'queStatus'], 'string'],
            [['queQuestion', 'queSolution'], 'string', 'max' => 500],
            [['queOption1', 'queOption2', 'queOption3', 'queOption4', 'queOption5', 'queOption6'], 'string', 'max' => 500],
            [['queCorrectAns'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'queID' => 'Que I D',
            'courseID' => 'Course I D',
            'coursesubjID' => 'Coursesubj I D',
            'coursechapterID' => 'Coursechapter I D',
            'queType' => 'Que Type',
            'queDifficultyevel' => 'Que Difficultyevel',
            'queQuestion' => 'Que Question',
            'queSolution' => 'Que Solution',
            'queDisplayType' => 'Que Display Type',
            'queStatus' => 'Que Status',
            'queOption1' => 'Que Option1',
            'queOption2' => 'Que Option2',
            'queOption3' => 'Que Option3',
            'queOption4' => 'Que Option4',
            'queCorrectAns' => 'Que Correct Ans',
        ];
    }
}
