<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Chapterfiles;
use backend\modules\api\v1\models\Chapternotes;
use backend\modules\api\v1\models\Chapterfavorite;
use backend\modules\api\v1\models\Chapterlike;
use backend\modules\api\v1\models\Exams;
/**
 * This is the model class for table "coursechapters".
 *
 * @property int $coursechapterID
 * @property int $courseID
 * @property int $coursesubjID
 * @property int $coursechapterNo
 * @property string $coursechapterName
 * @property string $coursechapterDescription
 * @property string $coursechapterFile
 * @property string $coursechapterFileURL
 * @property string $coursechapterStatus
 * @property string $coursechapterCreatedDate
 */
class Coursechapters extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coursechapters';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['courseID', 'coursesubjID', 'coursechapterNo', 'coursechapterName', 'coursechapterDescription', 'coursechapterFile', 'coursechapterFileURL'], 'required'],
            [['courseID', 'coursesubjID', 'coursechapterNo'], 'integer'],
            [['coursechapterDescription', 'coursechapterStatus'], 'string'],
            [['coursechapterCreatedDate'], 'safe'],
            [['coursechapterName', 'coursechapterFile'], 'string', 'max' => 100],
            [['coursechapterFileURL'], 'string', 'max' => 300],
            [['courseID', 'coursesubjID', 'coursechapterName'], 'unique', 'targetAttribute' => ['courseID', 'coursesubjID', 'coursechapterName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coursechapterID' => 'Coursechapter I D',
            'courseID' => 'Course I D',
            'coursesubjID' => 'Coursesubj I D',
            'coursechapterNo' => 'Coursechapter No',
            'coursechapterName' => 'Coursechapter Name',
            'coursechapterDescription' => 'Coursechapter Description',
            'coursechapterFile' => 'Coursechapter File',
            'coursechapterFileURL' => 'Coursechapter File U R L',
            'coursechapterStatus' => 'Coursechapter Status',
            'coursechapterCreatedDate' => 'Coursechapter Created Date',
        ];
    }
	public function GetCoursechaptersList($coursesubjID=0,$loginuserID=0,$isFavorite="",$isVisited="")
    {
	   $where='coursechapterStatus = "Active"'	;
	   if(!empty($coursesubjID))
	   {
		   $where.=' AND coursechapters.coursesubjID="'.$coursesubjID.'"';
	   }
	   if(!empty($isFavorite))
	   {
		   $where.=' AND coursechapters.coursechapterID IN (SELECT coursechapterID FROM chapterfavorite WHERE userID='.$loginuserID.')';
	   }
	   if(!empty($isVisited))
	   {
		   $where.=' AND coursechapters.coursechapterID IN (SELECT courseID FROM uservisitedcourses WHERE userID='.$loginuserID.')';
	   }
	   
	   $join = '   inner join coursesubjects on coursesubjects.coursesubjID = Coursechapters.coursesubjID	';
	   $result = Coursechapters::find()->select('coursechapters.*,coursesubjName,courseName,courses.courseID')->join('inner join','coursesubjects',' coursesubjects.coursesubjID = coursechapters.coursesubjID' )->join('inner join','courses',' coursesubjects.courseID = courses.courseID' )->where($where)->orderby('coursechapterNo ASC')->asArray()->all();
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   $Chapterfiles = new Chapterfiles;
		   $Chapternotes = new Chapternotes;
		   $Exams = new Exams;
		   $ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$videoRes = $Chapterfiles->GetChapterfilesList($result[$i]["coursechapterID"],"Video") ;
				$contentsRes = $Chapterfiles->GetChapterfilesList($result[$i]["coursechapterID"],"Contents") ;
				$Re[$i]["chapterVideofiles"] = $videoRes;
				$Re[$i]["chapterContentsfiles"] = $contentsRes;
				$Re[$i]["coursechapeterVideoFileCount"]= count($videoRes);
				$Re[$i]["coursechapeterContentFileCount"]= count($contentsRes);
				$Re[$i]["exams"] = $Exams->getPracticeLevelExams(0,0,$result[$i]["coursechapterID"],$loginuserID) ;
				$Re[$i]['coursechapterFile'] = $ImagePathArray['coursechapterFile'].$result[$i]['coursechapterFile'];
				$Re[$i]['coursechapterLikes']="No";
				$Re[$i]['coursechapterFavorite']="No";
				$Re[$i]['coursechapterNotes']="0";
				// check chapter liked?
				$Chapterlike_Res = Chapterlike::find()->where('userID = "'.$loginuserID.'" AND coursechapterID="'.$result[$i]["coursechapterID"].'"')->one();
				if(!empty($Chapterlike_Res))
				{
					$Re[$i]['coursechapterLikes']="Yes";
				}
				// check chapter favorite?
				$Chapterfavorite_Res = Chapterfavorite::find()->where('userID = "'.$loginuserID.'" AND coursechapterID="'.$result[$i]["coursechapterID"].'"')->one();
				if(!empty($Chapterfavorite_Res))
				{
					$Re[$i]['coursechapterFavorite']="Yes";
				}
				// get chapter notes count?
				$resChapternotes = $Chapternotes->GetChapternotesList($result[$i]["coursechapterID"],$loginuserID,0,"1000");
				if(count($resChapternotes)>0)
					$Re[$i]['coursechapterNotes']=count($resChapternotes);
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
	public function GetCoursechaptersCount($coursesubjID=0)
    {
	   $where='coursechapterStatus = "Active"'	;
	   if(!empty($coursesubjID))
	   {
		   $where.=' AND coursechapters.coursesubjID="'.$coursesubjID.'"';
	   }
	   
	   $result = Coursechapters::find()->select('count(coursechapterID) as TotalChapters')->where($where)->orderby('coursechapterNo ASC')->asArray()->all();
	   if(count($result)>0)
       {
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
