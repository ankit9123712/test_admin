<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "uservisitedcourses".
 *
 * @property int $coursevisitID
 * @property int $courseID
 * @property int $userID
 */
class Uservisitedcourses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uservisitedcourses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['courseID', 'userID'], 'required'],
            [['courseID', 'userID'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coursevisitID' => 'Coursevisit I D',
            'courseID' => 'Course I D',
            'userID' => 'User I D',
        ];
    }
	public function GetVisitedCoursesList($loginuserID=0)
    {
	   $where = 	'courseStatus = "Active" AND coursePublished="Yes"';
	   if(!empty($loginuserID))
		   $where.= " AND courseID IN (SELECT courseID from uservisitedcourses WHERE userID=".$loginuserID.")";
	   
		
	   $result = Courses::find()->select('*')->where($where)->orderby('courseName ASC')->asArray()->all();
	   if(count($result)>0)
       {
           $Coursesubjects = new Coursesubjects;
		   $Apilog = new Apilog;
		   $ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$Re[$i]["subjects"] = $Coursesubjects->GetCoursesubjectsList($result[$i]["courseID"],$loginuserID) ;
				$Re[$i]['courseImage'] = $ImagePathArray['courseImage'].$result[$i]['courseImage'];
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
