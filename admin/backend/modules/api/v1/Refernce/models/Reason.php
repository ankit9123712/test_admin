<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "reason".
 *
 * @property int $reasonID
 * @property string $reasonName
 * @property string $reasonStatus
 * @property string $reasonCreatedDate
 */
class Reason extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reason';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reasonName', 'reasonStatus'], 'required'],
            [['reasonStatus'], 'string'],
            [['reasonCreatedDate'], 'safe'],
            [['reasonName'], 'string', 'max' => 100],
            [['reasonName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reasonID' => 'Reason I D',
            'reasonName' => 'Reason Name',
            'reasonStatus' => 'Reason Status',
            'reasonCreatedDate' => 'Reason Created Date',
        ];
    }
	public function GetReasonList()
    {
	   $result = Reason::find()->select('*')->where('reasonStatus = "Active"')->orderby('reasonName ASC')->asArray()->all();
	   if(count($result)>0)
       {

		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
