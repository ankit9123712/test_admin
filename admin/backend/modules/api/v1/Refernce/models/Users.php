<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Settings;
use backend\modules\api\v1\models\Usersubscriptions;
/**
 * This is the model class for table "users".
 *
 * @property int $userID
 * @property string $userFullname
 * @property string $userEmail
 * @property string $userPassword MD5 Encrypted
 * @property string $userName
 * @property string $userMobile
 * @property string $userLocation
 * @property string $userLattitude
 * @property string $userLongitutde
 * @property string $userCountryCode
 * @property string $userGoogleID
 * @property string $userFacebookID
 * @property string $countryId
 * @property string $userAbout
 * @property int $professionID
 * @property string $userBirthDate
 * @property string $userGender
 * @property int $userHomeCityID
 * @property int $userCurrentCityID
 * @property int $userNationalityID
 * @property string $userHeight Store in CM
 * @property string $userHeightUnit
 * @property string $userWeight Store in Grams
 * @property string $userWeightUnit
 * @property string $userBodyColour
 * @property string $userEyesColour
 * @property string $userHairColour
 * @property string $userAdditionalInformation
 * @property string $userProfilePicture
 * @property string $userProfileVideo
 * @property string $userCoverPhoto
 * @property int $userStars
 * @property int $userFans
 * @property int $userLike
 * @property int $userNutural
 * @property int $userDontLike
 * @property int $userFavourite
 * @property string $userLastLogin
 * @property string $userCreatedDate
 * @property string $userStatus
 * @property string $userDeviceType
 * @property string $userDeviceID IP if its web
 * @property string $userType
 * @property string $userAllowNotification
 * @property string $userEmailNotification
 * @property string $userPushNotification
 * @property string $referKey
 * @property string $userLanguageID
 * @property string $userVerificationCode
 * @property string $contactSync
 * @property int $userTotalFollower
 * @property int $userTotalFollowing
 * @property int $userTotalFriend
 * @property string $userVerified
 * @property string $userSignupWith
 * @property int $bloodgroupID
 * @property string $userBloodDonor
 * @property string $hobbiesIDs
 * @property string $userHomeCityText
 * @property string $userCurrentCityText
 * @property string $userLanguageIDs
 * @property string $userMediaSettings
 * @property string $userWYLTRNIYAILIFUL Would you like to receive notification if your account is logged in from unknown location?
 * @property string $userAUTSYWYEI Allow users to search you with your Email Id?
 * @property string $userPushNotificationSound
 * @property string $userWYLTRNFSPRO Would you like to receive notification for suggested places, restaurants, offers, shops etc?
 * @property int $userPushBaseCount
 * @property int $userFriendRequestPushBaseCount
 * @property int $userChatPushBaseCount
 * @property double $userTotalPoint
 * @property string $userReferCode
 * @property string $userSingupReferCode
 * @property string $userOTP
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userPassword', 'userMobile', 'userLocation', 'userLattitude', 'userLongitutde', 'userCountryCode', 'userHomeCityID', 'userCurrentCityID', 'userHeightUnit', 'userWeightUnit', 'userBodyColour', 'userEyesColour', 'userHairColour', 'userAdditionalInformation', 'userProfilePicture', 'userCoverPhoto', 'userStatus', 'userDeviceType', 'userDeviceID', 'userType', 'userVerificationCode', 'userOTP'], 'required'],
            [['userAbout', 'userStatus', 'userType', 'userAllowNotification', 'userEmailNotification', 'userPushNotification', 'contactSync', 'userVerified', 'userBloodDonor', 'userWYLTRNIYAILIFUL', 'userAUTSYWYEI', 'userPushNotificationSound', 'userWYLTRNFSPRO'], 'string'],
            [['professionID', 'userHomeCityID', 'userCurrentCityID', 'userNationalityID', 'userStars', 'userFans', 'userLike', 'userNutural', 'userDontLike', 'userFavourite', 'userTotalFollower', 'userTotalFollowing', 'userTotalFriend', 'bloodgroupID', 'userPushBaseCount', 'userFriendRequestPushBaseCount', 'userChatPushBaseCount'], 'integer'],
            [['userBirthDate', 'userLastLogin', 'userCreatedDate'], 'safe'],
            [['userHeight', 'userWeight', 'userTotalPoint'], 'number'],
            [['userFullname', 'userName'], 'string', 'max' => 250],
            [['userEmail', 'userPassword', 'userProfilePicture', 'userProfileVideo', 'userCoverPhoto', 'userLanguageID', 'hobbiesIDs', 'userLanguageIDs'], 'string', 'max' => 100],
            [['userMobile', 'userDeviceType', 'userVerificationCode', 'userSignupWith', 'userReferCode', 'userSingupReferCode'], 'string', 'max' => 10],
            [['userLocation'], 'string', 'max' => 400],
            [['userLattitude', 'userLongitutde', 'userHeightUnit', 'userWeightUnit'], 'string', 'max' => 20],
            [['userCountryCode'], 'string', 'max' => 5],
            [['userGoogleID', 'userFacebookID', 'countryId'], 'string', 'max' => 150],
            [['userGender', 'userBodyColour', 'userEyesColour', 'userHairColour', 'userMediaSettings'], 'string', 'max' => 50],
            [['userAdditionalInformation'], 'string', 'max' => 4000],
            [['userDeviceID', 'referKey', 'userHomeCityText', 'userCurrentCityText'], 'string', 'max' => 500],
            [['userOTP'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userID' => 'User ID',
            'userFullname' => 'User Fullname',
            'userEmail' => 'User Email',
            'userPassword' => 'User Password',
            'userName' => 'User Name',
            'userMobile' => 'User Mobile',
            'userLocation' => 'User Location',
            'userLattitude' => 'User Lattitude',
            'userLongitutde' => 'User Longitutde',
            'userCountryCode' => 'User Country Code',
            'userGoogleID' => 'User Google ID',
            'userFacebookID' => 'User Facebook ID',
            'countryId' => 'Country ID',
            'userAbout' => 'User About',
            'professionID' => 'Profession ID',
            'userBirthDate' => 'User Birth Date',
            'userGender' => 'User Gender',
            'userHomeCityID' => 'User Home City ID',
            'userCurrentCityID' => 'User Current City ID',
            'userNationalityID' => 'User Nationality ID',
            'userHeight' => 'User Height',
            'userHeightUnit' => 'User Height Unit',
            'userWeight' => 'User Weight',
            'userWeightUnit' => 'User Weight Unit',
            'userBodyColour' => 'User Body Colour',
            'userEyesColour' => 'User Eyes Colour',
            'userHairColour' => 'User Hair Colour',
            'userAdditionalInformation' => 'User Additional Information',
            'userProfilePicture' => 'User Profile Picture',
            'userProfileVideo' => 'User Profile Video',
            'userCoverPhoto' => 'User Cover Photo',
            'userStars' => 'User Stars',
            'userFans' => 'User Fans',
            'userLike' => 'User Like',
            'userNutural' => 'User Nutural',
            'userDontLike' => 'User Dont Like',
            'userFavourite' => 'User Favourite',
            'userLastLogin' => 'User Last Login',
            'userCreatedDate' => 'User Created Date',
            'userStatus' => 'User Status',
            'userDeviceType' => 'User Device Type',
            'userDeviceID' => 'User Device ID',
            'userType' => 'User Type',
            'userAllowNotification' => 'User Allow Notification',
            'userEmailNotification' => 'User Email Notification',
            'userPushNotification' => 'User Push Notification',
            'referKey' => 'Refer Key',
            'userLanguageID' => 'User Language ID',
            'userVerificationCode' => 'User Verification Code',
            'contactSync' => 'Contact Sync',
            'userTotalFollower' => 'User Total Follower',
            'userTotalFollowing' => 'User Total Following',
            'userTotalFriend' => 'User Total Friend',
            'userVerified' => 'User Verified',
            'userSignupWith' => 'User Signup With',
            'bloodgroupID' => 'Bloodgroup ID',
            'userBloodDonor' => 'User Blood Donor',
            'hobbiesIDs' => 'Hobbies I Ds',
            'userHomeCityText' => 'User Home City Text',
            'userCurrentCityText' => 'User Current City Text',
            'userLanguageIDs' => 'User Language I Ds',
            'userMediaSettings' => 'User Media Settings',
            'userWYLTRNIYAILIFUL' => 'User Wyltrniyailiful',
            'userAUTSYWYEI' => 'User Autsywyei',
            'userPushNotificationSound' => 'User Push Notification Sound',
            'userWYLTRNFSPRO' => 'User Wyltrnfspro',
            'userPushBaseCount' => 'User Push Base Count',
            'userFriendRequestPushBaseCount' => 'User Friend Request Push Base Count',
            'userChatPushBaseCount' => 'User Chat Push Base Count',
            'userTotalPoint' => 'User Total Point',
            'userReferCode' => 'User Refer Code',
            'userSingupReferCode' => 'User Singup Refer Code',
            'userOTP' => 'User Otp',
        ];
    }
	 public function CheckUserDetailsForDuplication($userEmail="",$userMobile="",$userID="")
    {
        if(!empty($userEmail))
        {
            if(empty($userID))
            {
                $Exist_userEmail = Users::find()->where("userEmail = '".$userEmail."'")->all();
            }
            else 
            {
                $Exist_userEmail = Users::find()->where("userEmail = '".$userEmail."' and userID <> '".$userID."'")->all();
            }
           
            if(count($Exist_userEmail) == 0)
            {
                if(!empty($userMobile))
                {
                    if(empty($userID))
                    {
                        $Exist_userMobile = Users::find()->where("userMobile = '".$userMobile."'")->all();
                    }
                    else 
                    {
                        $Exist_userMobile = Users::find()->where("userMobile = '".$userMobile."' and userID <> '".$userID."'")->all();
                    }

                    if(count($Exist_userMobile) == 0)
                    {
                       return 'true';
                    }
                    else
                    {
                        return 'userMobile';
                    }
                }
                else
                {
                    return 'true';
                }
            }
            else
            {
                return 'userEmail';
            }
        }
        else
        {
            if(!empty($userMobile))
            {
                if(empty($userID))
                {
                    $Exist_userMobile = Users::find()->where("userMobile = '".$userMobile."'")->all();
                }
                else 
                {
                    $Exist_userMobile = Users::find()->where("userMobile = '".$userMobile."' and userID <> '".$userID."'")->all();
                }

                if(count($Exist_userMobile) == 0)
                {
                    return 'true';
                }
                else
                {
                    return 'userMobile';
                }
            }
            else
            {
               return 'true';
            }
        }
    }

    public function GetUserDetails($UserID)
    {
      // $Where = 'users.userStatus = "Active" AND useraddress.addressType = "Registered"';
	   $Where = ' WHERE users.userStatus = "Active" ';
       $Re = array();
      
       if(!empty($UserID))
       {
           $Where .= ' AND users.UserID = "'.$UserID.'"';
       }
	    
		$para = 'SELECT users.*,streamName,coursestdName,institute.instituteName ';
		$from = ' FROM users';
        $ORDERBY = " users.userID";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = "";//" LIMIT $pagesize";
        $OFFSET ="";// " OFFSET ".$page*$pagesize;      
		$join = '   inner join stream on stream.streamID = users.streamID					
					inner join coursestd on coursestd.coursestdID= users.coursestdID   
					LEFT Join institute ON institute.instituteID = users.instituteID					';		

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
	   
       if(count($result)>0)
       {
		   $Usersubscriptions = new Usersubscriptions;
		   $Settings = new Settings;
           for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
				$resSubscription = $Usersubscriptions->GetUserSubscriptionplanList($UserID,0,10);
				 $Re[$i]['mySubscriptions'] = $resSubscription;
				 $Re[$i]['settings'] = $Settings->GetUserSettings();
			
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re;   
    }
}
