<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "chapternotes".
 *
 * @property int $chapternoteID
 * @property int $userID
 * @property string $chapternoteNote
 */
class Chapternotes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chapternotes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'chapternoteNote'], 'required'],
            [['userID'], 'integer'],
            [['chapternoteNote'], 'string', 'max' => 2000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'chapternoteID' => 'Chapternote I D',
            'userID' => 'User I D',
            'chapternoteNote' => 'Chapternote Note',
        ];
    }
	public function GetChapternotesList($coursechapterID=0,$loginuserID="",$page="0",$pagesize="10")
    {
	   $where = 	' 1=1 ';
	   if(!empty($coursechapterID))
		   $where.= ' AND coursechapterID = '.$coursechapterID;
	   if(!empty($loginuserID))
		   $where.= " AND userID = '".$loginuserID."'";
		
	   $result = Chapternotes::find()->select('*')->where($where)->limit($pagesize)->orderby('chapternoteID DESC')->offset($pagesize*$page)->asArray()->all();
	   if(count($result)>0)
       {

		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
