<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $settingsID
 * @property string $settingsEmailFrom
 * @property string $settingsEmailTo
 * @property string $settingsEmailGateway
 * @property string $settingEmailID
 * @property string $settingsEmailPass
 * @property string $settingsSSL
 * @property string $settingsTLS
 * @property int $settingEmailPort
 * @property string $settingSenderName
 * @property string $settingPGMode
 * @property string $settingsPGSandboxUrl
 * @property string $settingPGSandboxCustomerKey
 * @property string $settingsPGSandboxCustomerAuth
 * @property string $settingsPGLiveUrl
 * @property string $settingPGLiveCustomerKey
 * @property string $settingPGLiveCustomerAuth
 * @property int $settingsUserResetPinLinkExpHr
 * @property int $settingsLogDeleteDays
 * @property string $settingsDeliveryCharges
 * @property string $settingsFreeDeliveryAbove
 * @property string $settingsTollFree
 * @property int $settingsOrderReturnTimeMinutes
 * @property string $settingsMasterOtp
 * @property string $settingsCompanyGST
 * @property string $settingAddress
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['settingsID', 'settingsEmailGateway', 'settingEmailID', 'settingsEmailPass', 'settingsSSL', 'settingsTLS', 'settingEmailPort', 'settingSenderName', 'settingPGMode', 'settingsPGSandboxUrl', 'settingPGSandboxCustomerKey', 'settingsPGSandboxCustomerAuth', 'settingsPGLiveUrl', 'settingPGLiveCustomerKey', 'settingPGLiveCustomerAuth', 'settingsDeliveryCharges', 'settingsFreeDeliveryAbove'], 'required'],
            [['settingsID', 'settingEmailPort', 'settingsUserResetPinLinkExpHr', 'settingsLogDeleteDays', 'settingsOrderReturnTimeMinutes'], 'integer'],
            [['settingsSSL', 'settingsTLS', 'settingPGMode'], 'string'],
            [['settingsDeliveryCharges', 'settingsFreeDeliveryAbove'], 'number'],
            [['settingsEmailFrom', 'settingsEmailTo', 'settingsEmailGateway', 'settingEmailID', 'settingPGSandboxCustomerKey', 'settingsPGSandboxCustomerAuth', 'settingPGLiveCustomerKey', 'settingPGLiveCustomerAuth'], 'string', 'max' => 100],
            [['settingsEmailPass', 'settingsTollFree'], 'string', 'max' => 20],
            [['settingSenderName'], 'string', 'max' => 50],
            [['settingsPGSandboxUrl', 'settingsPGLiveUrl'], 'string', 'max' => 250],
            [['settingsMasterOtp'], 'string', 'max' => 6],
            [['settingsCompanyGST'], 'string', 'max' => 25],
            [['settingAddress'], 'string', 'max' => 255],
            [['settingsID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'settingsID' => 'Settings ID',
            'settingsEmailFrom' => 'Settings Email From',
            'settingsEmailTo' => 'Settings Email To',
            'settingsEmailGateway' => 'Settings Email Gateway',
            'settingEmailID' => 'Setting Email ID',
            'settingsEmailPass' => 'Settings Email Pass',
            'settingsSSL' => 'Settings Ssl',
            'settingsTLS' => 'Settings Tls',
            'settingEmailPort' => 'Setting Email Port',
            'settingSenderName' => 'Setting Sender Name',
            'settingPGMode' => 'Setting Pg Mode',
            'settingsPGSandboxUrl' => 'Settings Pg Sandbox Url',
            'settingPGSandboxCustomerKey' => 'Setting Pg Sandbox Customer Key',
            'settingsPGSandboxCustomerAuth' => 'Settings Pg Sandbox Customer Auth',
            'settingsPGLiveUrl' => 'Settings Pg Live Url',
            'settingPGLiveCustomerKey' => 'Setting Pg Live Customer Key',
            'settingPGLiveCustomerAuth' => 'Setting Pg Live Customer Auth',
            'settingsUserResetPinLinkExpHr' => 'Settings User Reset Pin Link Exp Hr',
            'settingsLogDeleteDays' => 'Settings Log Delete Days',
            'settingsDeliveryCharges' => 'Settings Delivery Charges',
            'settingsFreeDeliveryAbove' => 'Settings Free Delivery Above',
            'settingsTollFree' => 'Settings Toll Free',
            'settingsOrderReturnTimeMinutes' => 'Settings Order Return Time Minutes',
            'settingsMasterOtp' => 'Settings Master Otp',
            'settingsCompanyGST' => 'Settings Company Gst',
            'settingAddress' => 'Setting Address',
        ];
    }
	public function GetUserSettings()
    {   
        $Res = Settings::find()->select('*')->where('1=1')->asArray()->all();
        return $Res;
    }
}
