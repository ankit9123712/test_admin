<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "rewardshistory".
 *
 * @property int $rewardID
 * @property int $userID
 * @property string $rewardType
 * @property int $rewardPoints
 * @property string $rewardDate
 * @property string $rewardNote
 * @property string $rewardTitle
 * @property string $rewardSubType
 */
class Rewardshistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rewardshistory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'rewardType', 'rewardPoints', 'rewardDate', 'rewardNote', 'rewardTitle', 'rewardSubType'], 'required'],
            [['userID', 'rewardPoints'], 'integer'],
            [['rewardType', 'rewardSubType'], 'string'],
            [['rewardDate'], 'safe'],
            [['rewardNote', 'rewardTitle'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rewardID' => 'Reward I D',
            'userID' => 'User I D',
            'rewardType' => 'Reward Type',
            'rewardPoints' => 'Reward Points',
            'rewardDate' => 'Reward Date',
            'rewardNote' => 'Reward Note',
            'rewardTitle' => 'Reward Title',
            'rewardSubType' => 'Reward Sub Type',
        ];
    }
	 public function GetRewardsList($UserID,$page="0",$pagesize="10",$rewardType="")
    {
      // $Where = 'users.userStatus = "Active" AND useraddress.addressType = "Registered"';
	   $Where = ' WHERE 1=1 ';
       $Re = array();
      
       if(!empty($UserID))
       {
           $Where .= ' AND userID = "'.$UserID.'"';
       }
	   if(!empty($rewardType))
       {
           $Where .= ' AND rewardType = "'.$rewardType.'"';
       }
	    
		$para = 'SELECT rewardshistory.*';
		$from = ' FROM rewardshistory';
        $ORDERBY = " rewardID ";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;      
		$join = '  				';		

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
	   
       if(count($result)>0)
       {
		  
           for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
		
			
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re;   
    }
	 public function GetRewardsTotal($UserID,$page="0",$pagesize="10",$rewardType="")
    {
      // $Where = 'users.userStatus = "Active" AND useraddress.addressType = "Registered"';
	   $Where = ' WHERE 1=1 ';
       $Re = array();
      
       if(!empty($UserID))
       {
           $Where .= ' AND userID = "'.$UserID.'"';
       }
	   if(!empty($rewardType))
       {
           $Where .= ' AND rewardType = "'.$rewardType.'"';
       }
	    
		$para = 'SELECT sum(rewardPoints) as Total';
		$from = ' FROM rewardshistory';
        $ORDERBY = "  ";
        $order = "  ";
        $LIMIT = "  ";
        $OFFSET = " ";      
		$join = '  				';		

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
	   
       if(count($result)>0)
       {
		  
           for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
		
			
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re;   
    }
}
