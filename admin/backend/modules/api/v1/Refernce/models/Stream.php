<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "stream".
 *
 * @property int $streamID
 * @property string $streamName
 * @property string $streamStatus
 * @property string $streamCreatedDate
 */
class Stream extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stream';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamName', 'streamStatus'], 'required'],
            [['streamStatus'], 'string'],
            [['streamCreatedDate'], 'safe'],
            [['streamName'], 'string', 'max' => 100],
            [['streamName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'streamID' => 'Stream I D',
            'streamName' => 'Stream Name',
            'streamStatus' => 'Stream Status',
            'streamCreatedDate' => 'Stream Created Date',
        ];
    }
	public function GetStreamList($pagesize="10",$page="0")
    {
		$Where = ' WHERE streamStatus = "Active"  ';
		
		$para = 'SELECT stream.*';
		$from = ' FROM stream';
        $ORDERBY = " stream.streamName";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
