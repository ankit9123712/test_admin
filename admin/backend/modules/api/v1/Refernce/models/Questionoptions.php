<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "questionoptions".
 *
 * @property string $queoptionID
 * @property string $queID
 * @property string $queoptionOption
 * @property string $queoptionCorrect
 */
class Questionoptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questionoptions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['queID', 'queoptionOption', 'queoptionCorrect'], 'required'],
            [['queID'], 'integer'],
            [['queoptionCorrect'], 'string'],
            [['queoptionOption'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'queoptionID' => 'Queoption I D',
            'queID' => 'Que I D',
            'queoptionOption' => 'Queoption Option',
            'queoptionCorrect' => 'Queoption Correct',
        ];
    }
}
