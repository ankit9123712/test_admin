<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "chapterfiles".
 *
 * @property int $chapterfileID
 * @property int $coursechapterID
 * @property string $chapterfileFileType
 * @property string $chapterfileFile
 * @property string $chapterfileStatus
 */
class Chapterfiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chapterfiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coursechapterID', 'chapterfileFileType', 'chapterfileFile', 'chapterfileStatus'], 'required'],
            [['coursechapterID'], 'integer'],
            [['chapterfileFileType', 'chapterfileStatus'], 'string'],
            [['chapterfileFile'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'chapterfileID' => 'Chapterfile I D',
            'coursechapterID' => 'Coursechapter I D',
            'chapterfileFileType' => 'Chapterfile File Type',
            'chapterfileFile' => 'Chapterfile File',
            'chapterfileStatus' => 'Chapterfile Status',
        ];
    }
	public function GetChapterfilesList($coursechapterID=0,$chapterfileFileType)
    {
	   $where='chapterfileStatus = "Active"'	;
	   if(!empty($coursechapterID))
	   {
		   $where.=' AND coursechapterID="'.$coursechapterID.'"';
	   }
	   $where.=' AND chapterfileFileType="'.$chapterfileFileType.'"';
	   $result = Chapterfiles::find()->select('*')->where($where)->asArray()->all();
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   $ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$Re[$i]['chapterfileFile'] = $ImagePathArray['chapterfileFile'].$result[$i]['chapterfileFile'];
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
