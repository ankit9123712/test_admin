<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "userexamsreschedule".
 *
 * @property int $rescheduleID
 * @property int $userID
 * @property int $examID
 * @property string $rescheduleDate
 * @property string $rescheduleReason
 */
class Userexamsreschedule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userexamsreschedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'examID', 'rescheduleDate'], 'required'],
            [['userID', 'examID'], 'integer'],
            [['rescheduleDate'], 'safe'],
            [['rescheduleReason'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rescheduleID' => 'Reschedule I D',
            'userID' => 'User I D',
            'examID' => 'Exam I D',
            'rescheduleDate' => 'Reschedule Date',
            'rescheduleReason' => 'Reschedule Reason',
        ];
    }
}
