<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "chapterlike".
 *
 * @property int $chapterlikeID
 * @property int $userID
 */
class Chapterlike extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chapterlike';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID'], 'required'],
            [['userID'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'chapterlikeID' => 'Chapterlike I D',
            'userID' => 'User I D',
        ];
    }
}
