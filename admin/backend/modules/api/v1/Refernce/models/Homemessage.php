<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "homemessage".
 *
 * @property int $messageID
 * @property string $messageMsg
 * @property string $messageStatus
 */
class Homemessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'homemessage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['messageMsg', 'messageStatus'], 'required'],
            [['messageStatus'], 'string'],
            [['messageMsg'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'messageID' => 'Message I D',
            'messageMsg' => 'Message Msg',
            'messageStatus' => 'Message Status',
        ];
    }
	public function GetHomemessageList($loginuserID)
    {
		$where = 	'messageStatus = "Active"';
		$where.= " AND streamID IN (SELECT streamID from users WHERE userID=".$loginuserID.")";
		$where.= " AND coursestdID IN (SELECT coursestdID from users WHERE userID=".$loginuserID.")";
		$where.= " AND instituteID IN (SELECT instituteID from users WHERE userID=".$loginuserID.")";
			
	   $result = Homemessage::find()->select('*')->where($where)->orderby('messageMsg ASC')->asArray()->all();
	   if(count($result)>0)
       {
          
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
