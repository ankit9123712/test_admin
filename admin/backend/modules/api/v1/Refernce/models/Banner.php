<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property int $bannerID
 * @property string $bannerName
 * @property string $bannerFileType
 * @property string $bannerFile
 * @property string $bannerRemarks
 * @property string $bannerStatus
 * @property int $bannerPosition
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bannerName', 'bannerFile', 'bannerStatus'], 'required'],
            [['bannerFileType', 'bannerStatus'], 'string'],
            [['bannerPosition'], 'integer'],
            [['bannerName', 'bannerFile'], 'string', 'max' => 100],
            [['bannerRemarks'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bannerID' => 'Banner I D',
            'bannerName' => 'Banner Name',
            'bannerFileType' => 'Banner File Type',
            'bannerFile' => 'Banner File',
            'bannerRemarks' => 'Banner Remarks',
            'bannerStatus' => 'Banner Status',
            'bannerPosition' => 'Banner Position',
        ];
    }
	public function GetBannerList()
    {
	   $result = Banner::find()->select('*')->where('bannerStatus = "Active"')->orderby('bannerPosition ASC')->asArray()->all();
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   $ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
                $Re[$i]['bannerFile'] = $ImagePathArray['bannerFile'].$result[$i]['bannerFile'];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
