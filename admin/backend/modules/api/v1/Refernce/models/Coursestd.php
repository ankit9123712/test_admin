<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "coursestd".
 *
 * @property int $coursestdID
 * @property int $streamID
 * @property string $coursestdName
 * @property string $coursestdDuration
 * @property string $coursestdRemarks
 * @property string $coursestdStatus
 * @property string $coursestdCreatedDate
 */
class Coursestd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coursestd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamID', 'coursestdName', 'coursestdDuration', 'coursestdStatus'], 'required'],
            [['streamID'], 'integer'],
            [['coursestdStatus'], 'string'],
            [['coursestdCreatedDate'], 'safe'],
            [['coursestdName'], 'string', 'max' => 100],
            [['coursestdDuration'], 'string', 'max' => 20],
            [['coursestdRemarks'], 'string', 'max' => 200],
            [['coursestdName', 'streamID'], 'unique', 'targetAttribute' => ['coursestdName', 'streamID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coursestdID' => 'Coursestd I D',
            'streamID' => 'Stream I D',
            'coursestdName' => 'Coursestd Name',
            'coursestdDuration' => 'Coursestd Duration',
            'coursestdRemarks' => 'Coursestd Remarks',
            'coursestdStatus' => 'Coursestd Status',
            'coursestdCreatedDate' => 'Coursestd Created Date',
        ];
    }
	public function GetCoursestdList($streamID=0,$pagesize="10",$page="0")
    {
		$Where = ' WHERE coursestdStatus = "Active"  ';
		if(!empty($streamID))
		{
			$Where .= ' AND streamID="'.$streamID.'"';
		}
		
		$para = 'SELECT coursestd.*';
		$from = ' FROM coursestd';
        $ORDERBY = " coursestd.coursestdName";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
