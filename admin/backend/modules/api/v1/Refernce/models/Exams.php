<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Userexams;
use backend\modules\api\v1\models\Course;
use backend\modules\api\v1\models\Coursesubjects;
use backend\modules\api\v1\models\Coursechapters;
/**
 * This is the model class for table "exams".
 *
 * @property int $examID
 * @property int $streamID
 * @property int $coursestdID
 * @property string $examType
 * @property int $courseID
 * @property int $coursesubjID
 * @property string $coursechapterIDs
 * @property string $examName
 * @property string $examDuration
 * @property string $examStartDate
 * @property string $examStartTime
 * @property string $examEndDate
 * @property string $examEndTime
 * @property int $examQsL1
 * @property int $examQsL1SubID
 * @property int $examQsL2
 * @property int $examQsL2SubID
 * @property int $examQsL3
 * @property int $examQsL3SubID
 * @property int $examTotalQs
 * @property int $examQualifyingMarks
 * @property int $examMaxAttempt
 * @property int $examMaxDaysReschedule
 * @property double $examCorrectAnswer
 * @property double $examWrongAnswer
 * @property string $examInstruction
 * @property string $examStatus
 */
class Exams extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exams';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamID', 'coursestdID', 'examType', 'courseID', 'coursesubjID', 'coursechapterIDs', 'examName', 'examDuration', 'examStartDate', 'examStartTime', 'examEndDate', 'examEndTime', 'examQsL1', 'examQsL1SubID', 'examQsL2', 'examQsL2SubID', 'examQsL3', 'examQsL3SubID', 'examTotalQs', 'examQualifyingMarks', 'examMaxAttempt', 'examMaxDaysReschedule', 'examCorrectAnswer', 'examWrongAnswer', 'examInstruction', 'examStatus'], 'required'],
            [['streamID', 'coursestdID', 'courseID', 'coursesubjID', 'examQsL1', 'examQsL1SubID', 'examQsL2', 'examQsL2SubID', 'examQsL3', 'examQsL3SubID', 'examTotalQs', 'examQualifyingMarks', 'examMaxAttempt', 'examMaxDaysReschedule'], 'integer'],
            [['examType', 'examInstruction', 'examStatus'], 'string'],
            [['examDuration', 'examStartDate', 'examStartTime', 'examEndDate', 'examEndTime'], 'safe'],
            [['examCorrectAnswer', 'examWrongAnswer'], 'number'],
            [['coursechapterIDs'], 'string', 'max' => 500],
            [['examName'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'examID' => 'Exam I D',
            'streamID' => 'Stream I D',
            'coursestdID' => 'Coursestd I D',
            'examType' => 'Exam Type',
            'courseID' => 'Course I D',
            'coursesubjID' => 'Coursesubj I D',
            'coursechapterIDs' => 'Coursechapter I Ds',
            'examName' => 'Exam Name',
            'examDuration' => 'Exam Duration',
            'examStartDate' => 'Exam Start Date',
            'examStartTime' => 'Exam Start Time',
            'examEndDate' => 'Exam End Date',
            'examEndTime' => 'Exam End Time',
            'examQsL1' => 'Exam Qs L1',
            'examQsL1SubID' => 'Exam Qs L1 Sub I D',
            'examQsL2' => 'Exam Qs L2',
            'examQsL2SubID' => 'Exam Qs L2 Sub I D',
            'examQsL3' => 'Exam Qs L3',
            'examQsL3SubID' => 'Exam Qs L3 Sub I D',
            'examTotalQs' => 'Exam Total Qs',
            'examQualifyingMarks' => 'Exam Qualifying Marks',
            'examMaxAttempt' => 'Exam Max Attempt',
            'examMaxDaysReschedule' => 'Exam Max Days Reschedule',
            'examCorrectAnswer' => 'Exam Correct Answer',
            'examWrongAnswer' => 'Exam Wrong Answer',
            'examInstruction' => 'Exam Instruction',
            'examStatus' => 'Exam Status',
        ];
    }
	public function GetExam($courseID=0,$coursesubjID="",$coursechapterIDs="",$examType="",$page="0",$pagesize="10",$loginuserID=0)
    {

	   $Where = ' WHERE examStatus = "Active" ';
       $Re = array();
      
       if(!empty($courseID))
       {
           $Where .= ' AND courseID = "'.$courseID.'"';
       }
	   if(!empty($coursesubjID))
       {
           $Where .= ' AND coursesubjID = "'.$coursesubjID.'"';
       }
	   if(!empty($coursechapterIDs))
       {
           $Where .= ' AND coursechapterIDs in ("'.$coursechapterIDs.'")';
       }
	    
		$para = 'SELECT exams.*  ';
		$from = ' FROM exams';
        $ORDERBY = " exams.examID";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = "";//" LIMIT $pagesize";
        $OFFSET ="";// " OFFSET ".$page*$pagesize;      
		$join = '  ';		

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
	   if(count($result)>0)
       {
          
		   for($i=0;count($result)>$i; $i++) 
           { 
				extract($result[$i])	;
				if($examType !="Full Length")
				{
					
					$Re[$i] = $result[$i];
					if(empty($coursesubjID) && empty($coursechapterIDs) && !empty($courseID))
						$Re[$i]["examtoshowat"]="Course";
					if(!empty($coursesubjID) && empty($coursechapterIDs) && !empty($courseID))
						$Re[$i]["examtoshowat"]="Subject";
					if(!empty($coursesubjID) && !empty($coursechapterIDs) && !empty($courseID))						
						$Re[$i]["examtoshowat"]="Chapter";
					
					
				}
				

             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
	public function GetMyTest($page="0",$pagesize="10",$loginuserID=0)
    {

	   $Where = ' WHERE subscriptionEndDate >= NOW() AND examID NOT IN (SELECT examID from userexams where userexamGiven="Yes" AND userID='.$loginuserID.') ';
       $Re = array();
      
       if(!empty($loginuserID))
       {
           $Where .= ' AND users.userID = "'.$loginuserID.'"';
       }
	  
	    $Where .= ' AND examEndDate >=	 "'.date('Y-m-d').'"';
		$para = 'SELECT exams.*  ';
		$from = ' FROM exams ';
        $ORDERBY = " exams.examStartDate";
        $order = " ORDER BY $ORDERBY ASC";
        $LIMIT = "";//" LIMIT $pagesize";
        $OFFSET ="";// " OFFSET ".$page*$pagesize;      
		$join = '  inner join users on users.streamID = exams.streamID
inner join usersubscriptions on usersubscriptions.userID = users.userID ';		

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
      //  echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
	   if(count($result)>0)
       {
            $Userexams = new Userexams;
		   for($i=0;count($result)>$i; $i++) 
           { 
				
				
					//echo "sdsd";die;
					$Re[$i] = $result[$i];
					$Userexams_Res = Userexams::find()->where('userID = "'.$loginuserID.'" AND examID="'.$result[$i]["examID"].'"')->one();
					
					if(!empty($Userexams_Res))					
						$Re[$i]["examgiven"]=$Userexams_Res->userexamGiven;
					else
						$Re[$i]["examgiven"]="No";
					//print_r($Userexams_Res);die;
					
				

             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
	public function getPracticeLevelExams($courseID=0,$coursesubjID=0,$coursechapterID=0,$loginuserID=0)
	{
		$Re = array();
		
		
		$Where = " 1 =1 ";
		if(!empty($courseID))
			$Where.= " AND courseID=".$courseID;
		if(!empty($coursesubjID))
			$Where.= " AND coursesubjID=".$coursesubjID;
		if(!empty($coursechapterID))
			$Where.= " AND coursechapterID IN (".$coursechapterID.")";
		//echo $Where;
		$res = Questions::find()->where($Where)->asArray()->all();
		if(empty($res))
		{
			
		}
		else
		{
			$startDate = date('Y-m-d');
			$startTime=date('H:i:s');
			$totalQs = count($res);
			$Re[0]["examID"] = "0";
			$Re[0]["streamID"] = "";
			$Re[0]["coursestdID"] = "";
			$Re[0]["examType"] = "Practice";
			$Re[0]["courseID"] = $courseID;
			$Re[0]["coursesubjID"] = $coursesubjID;
			$Re[0]["coursechapterIDs"] = $coursechapterID;
			$Re[0]["examName"] = "";
			$Re[0]["examContains"] = "";
			$Re[0]["examSections"] = "";
			$Re[0]["examDuration"] = "";
			$Re[0]["examStartDate"] = $startDate;
			$Re[0]["examStartTime"] = $startTime;
			$Re[0]["examEndDate"] = date('Y-m-d', strtotime('+1 day', strtotime($startDate)));
			$Re[0]["examEndTime"] = date('H:i:s', strtotime('+1 hour', strtotime($startTime)));
			$Re[0]["examQsL1SubID"] = "";			
			$Re[0]["examQsL2SubID"] = "";
			$Re[0]["examQsL3SubID"] = "";
			$Re[0]["examMaxAttempt"] = "";
			$Re[0]["examMaxDaysReschedule"] = "";
			$Re[0]["examCorrectAnswer"] = "2.00";
			$Re[0]["examWrongAnswer"] = "0.00";
			$Re[0]["examInstruction"] = "";
			$Re[0]["examStatus"] = "Active";
			if(empty($coursesubjID) && empty($coursechapterID) && !empty($courseID))
			{
				$Re[0]["examtoshowat"]="Course";
				$my_Res = Courses::find()->where('courseID = "'.$courseID.'"')->one();
				$Re[0]["examName"]="Practice Course: ".$my_Res->courseName;
				
				if($totalQs > 50)
					$totalQs = 50;
			}
			if(!empty($coursesubjID) && empty($courseID))
			{
				$Re[0]["examtoshowat"]="Subject";
				$my_Res = Coursesubjects::find()->where('coursesubjID = "'.$coursesubjID.'"')->one();
				$Re[0]["examName"]="Practice Subject: ".$my_Res->coursesubjName;
				if($totalQs > 25)
					$totalQs = 25;

			}
			if(empty($coursesubjID) && !empty($coursechapterID) && empty($courseID))						
			{
				
				$Re[0]["examtoshowat"]="Chapter";
				$my_Res = Coursechapters::find()->where('coursechapterID = "'.$coursechapterID.'"')->one();
				$Re[0]["examName"]="Practice Chapter: ".$my_Res->coursechapterName;
				if($totalQs > 15)
					$totalQs = 15;

			}
			$Re[0]["examTotalQs"] = $totalQs;
			$Re[0]["examQsL1"] = intval(($totalQs*50)/100);
			$Re[0]["examQsL2"] = intval(($totalQs*30)/100);			
			$Re[0]["examQsL3"] = $totalQs - $Re[0]["examQsL1"] - $Re[0]["examQsL2"];			
			$Re[0]["examQualifyingMarks"] = intval(($totalQs*35)/50);
		}
		return $Re; 
	}
}
