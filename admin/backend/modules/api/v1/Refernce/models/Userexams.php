<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Userexamquestions;
/**
 * This is the model class for table "userexams".
 *
 * @property int $userexamID
 * @property int $examID
 * @property int $userID
 * @property string $userexamName
 * @property string $userexamDuration
 * @property int $userexamTotalQs
 * @property int $userexamQualifyingMarks
 * @property double $userexamCorrectAnswer
 * @property double $userexamWrongAnswer
 * @property string $userexamInstruction
 * @property string $userexamDate
 * @property double $userexamMarks
 * @property double $userexamPercentage
 * @property int $userexamRank
 * @property string $userexamStatus
 * @property string $userexamGrade
 * @property int $userexamAttempted
 * @property int $userexamCorrect
 * @property int $userexamIncorrect
 * @property double $userexamRewardPoint
 * @property int $userexamSkipped
 */
class Userexams extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userexams';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['examID', 'userID', 'userexamDuration', 'userexamTotalQs', 'userexamQualifyingMarks', 'userexamCorrectAnswer', 'userexamWrongAnswer',  'userexamDate', 'userexamMarks', 'userexamPercentage', 'userexamRank', 'userexamStatus'], 'required'],
            [['examID', 'userID', 'userexamTotalQs', 'userexamQualifyingMarks', 'userexamRank', 'userexamAttempted', 'userexamCorrect', 'userexamIncorrect', 'userexamSkipped'], 'integer'],
            [['userexamDuration', 'userexamDate'], 'safe'],
            [['userexamCorrectAnswer', 'userexamWrongAnswer', 'userexamMarks', 'userexamPercentage', 'userexamRewardPoint'], 'number'],
            [['userexamInstruction', 'userexamStatus'], 'string'],
            [['userexamName'], 'string', 'max' => 100],
            [['userexamGrade'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userexamID' => 'Userexam I D',
            'examID' => 'Exam I D',
            'userID' => 'User I D',
            'userexamName' => 'Userexam Name',
            'userexamDuration' => 'Userexam Duration',
            'userexamTotalQs' => 'Userexam Total Qs',
            'userexamQualifyingMarks' => 'Userexam Qualifying Marks',
            'userexamCorrectAnswer' => 'Userexam Correct Answer',
            'userexamWrongAnswer' => 'Userexam Wrong Answer',
            'userexamInstruction' => 'Userexam Instruction',
            'userexamDate' => 'Userexam Date',
            'userexamMarks' => 'Userexam Marks',
            'userexamPercentage' => 'Userexam Percentage',
            'userexamRank' => 'Userexam Rank',
            'userexamStatus' => 'Userexam Status',
            'userexamGrade' => 'Userexam Grade',
            'userexamAttempted' => 'Userexam Attempted',
            'userexamCorrect' => 'Userexam Correct',
            'userexamIncorrect' => 'Userexam Incorrect',
            'userexamRewardPoint' => 'Userexam Reward Point',
            'userexamSkipped' => 'Userexam Skipped',
        ];
    }
	public function GetResult($userexamID=0,$userexamType="",$page="0",$pagesize="10",$loginuserID=0,$showDetails="")
    {

	   $Where = ' WHERE 1=1 AND userexamGiven="Yes" ';
       $Re = array();
      
       if(!empty($userexamID))
       {
           $Where .= ' AND userexamID = "'.$userexamID.'"';
       }
	   if(!empty($userexamType))
       {
           $Where .= ' AND userexamType = "'.$userexamType.'"';
       }
	   if(!empty($loginuserID) )
       {
           $Where .= ' AND userID = "'.$loginuserID.'"';
       }
	  
	    
		$para = 'SELECT userexams.*  ';
		$from = ' FROM userexams';
        $ORDERBY = " userexams.userexamID";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = "";//" LIMIT $pagesize";
        $OFFSET ="";// " OFFSET ".$page*$pagesize;      
		$join = '  ';		

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
	   if(count($result)>0)
       {
           $Userexamquestions = new Userexamquestions;
		   for($i=0;count($result)>$i; $i++) 
           { 
				
				
					
					$Re[$i] = $result[$i];
					if(!empty($showDetails))
					{
						$Re[$i]["questions"]=$Userexamquestions->GetQuestions($result[$i]["userexamID"]);
					}
					
					
				

             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
	
}
