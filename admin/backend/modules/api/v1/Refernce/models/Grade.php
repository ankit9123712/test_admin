<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "grade".
 *
 * @property int $gradeID
 * @property string $gradeName
 * @property int $gradeMin
 * @property int $gradeMax
 */
class Grade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gradeName', 'gradeMin', 'gradeMax'], 'required'],
            [['gradeMin', 'gradeMax'], 'integer'],
            [['gradeName'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'gradeID' => 'Grade I D',
            'gradeName' => 'Grade Name',
            'gradeMin' => 'Grade Min',
            'gradeMax' => 'Grade Max',
        ];
    }
}
