<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $feedbackID
 * @property string $feedbackName
 * @property string $feedbackEmail
 * @property string $feedbackFeedback
 * @property int $userID
 * @property string $feedbackDate
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['feedbackName', 'feedbackEmail', 'feedbackFeedback', 'userID'], 'required'],
            [['userID'], 'integer'],
            [['feedbackDate'], 'safe'],
            [['feedbackName', 'feedbackEmail'], 'string', 'max' => 100],
            [['feedbackFeedback'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'feedbackID' => 'Feedback I D',
            'feedbackName' => 'Feedback Name',
            'feedbackEmail' => 'Feedback Email',
            'feedbackFeedback' => 'Feedback Feedback',
            'userID' => 'User I D',
            'feedbackDate' => 'Feedback Date',
        ];
    }
}
