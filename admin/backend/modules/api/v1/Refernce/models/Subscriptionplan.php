<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "subscriptionplan".
 *
 * @property int $planID
 * @property int $streamID
 * @property int $coursestdID
 * @property string $planName
 * @property string $planDescription
 * @property string $planPrice
 * @property string $planDiscountPrice
 * @property int $planDuration
 * @property string $planStatus
 * @property string $planCreatedDate
 */
class Subscriptionplan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscriptionplan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamID', 'coursestdID', 'planName', 'planDescription', 'planPrice', 'planDiscountPrice', 'planDuration', 'planStatus'], 'required'],
            [['streamID', 'coursestdID', 'planDuration'], 'integer'],
            [['planDescription', 'planStatus'], 'string'],
            [['planPrice', 'planDiscountPrice'], 'number'],
            [['planCreatedDate'], 'safe'],
            [['planName'], 'string', 'max' => 100],
            [['streamID', 'coursestdID', 'planName'], 'unique', 'targetAttribute' => ['streamID', 'coursestdID', 'planName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'planID' => 'Plan I D',
            'streamID' => 'Stream I D',
            'coursestdID' => 'Coursestd I D',
            'planName' => 'Plan Name',
            'planDescription' => 'Plan Description',
            'planPrice' => 'Plan Price',
            'planDiscountPrice' => 'Plan Discount Price',
            'planDuration' => 'Plan Duration',
            'planStatus' => 'Plan Status',
            'planCreatedDate' => 'Plan Created Date',
        ];
    }
	public function GetSubscriptionplanList($streamID,$coursestdID,$page=0,$pagesize=10)
    {
	   $where = 	'planStatus = "Active" ';
	   if(!empty($streamID))
		   $where.= ' AND streamID = '.$streamID;
	   if(!empty($coursestdID))
		   $where.= " AND coursestdID = '".$coursestdID."'";
		
	   $result = Subscriptionplan::find()->select('*')->where($where)->limit($pagesize)->orderby('planName ASC')->offset($pagesize*$page)->asArray()->all();
	   if(count($result)>0)
       {

		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];             
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
