<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Coursesubjects;
use backend\modules\api\v1\models\Exams;
/**
 * This is the model class for table "courses".
 *
 * @property int $courseID  
 * @property int $streamID
 * @property int $coursestdID
 * @property string $courseName
 * @property string $courseImage
 * @property int $courseDuration
 * @property int $courseMinAge
 * @property int $courseMaxAge
 * @property string $courseFree
 * @property string $courseDescription
 * @property string $courseEligiblity
 * @property string $courseCareer
 * @property string $coursePublished
 * @property string $courseStatus
 * @property string $courseCreatedDate
 */
class Courses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamID', 'coursestdID', 'courseName', 'courseImage', 'courseDuration', 'courseMinAge', 'courseMaxAge', 'courseFree', 'courseDescription', 'courseEligiblity', 'courseCareer'], 'required'],
            [['streamID', 'coursestdID', 'courseDuration', 'courseMinAge', 'courseMaxAge'], 'integer'],
            [['courseFree', 'courseDescription', 'courseEligiblity', 'courseCareer', 'coursePublished', 'courseStatus'], 'string'],
            [['courseCreatedDate'], 'safe'],
            [['courseName', 'courseImage'], 'string', 'max' => 100],
            [['streamID', 'coursestdID', 'courseName'], 'unique', 'targetAttribute' => ['streamID', 'coursestdID', 'courseName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'courseID' => 'Course I D',
            'streamID' => 'Stream I D',
            'coursestdID' => 'Coursestd I D',
            'courseName' => 'Course Name',
            'courseImage' => 'Course Image',
            'courseDuration' => 'Course Duration',
            'courseMinAge' => 'Course Min Age',
            'courseMaxAge' => 'Course Max Age',
            'courseFree' => 'Course Free',
            'courseDescription' => 'Course Description',
            'courseEligiblity' => 'Course Eligiblity',
            'courseCareer' => 'Course Career',
            'coursePublished' => 'Course Published',
            'courseStatus' => 'Course Status',
            'courseCreatedDate' => 'Course Created Date',
        ];
    }
	public function GetCoursesList($courseID=0,$courseFree="",$page="0",$pagesize="10",$loginuserID=0,$mycourse="")
    {
	   $where = 	'courseStatus = "Active" AND coursePublished="Yes"';
	   if(!empty($courseID))
		   $where.= ' AND courseID = '.$courseID;
	   if(!empty($courseFree))
		   $where.= " AND courseFree = '".$courseFree."'";
	   if(!empty($mycourse))
		   $where.= " AND courseID IN (SELECT courseID from usercourses WHERE userID=".$loginuserID.")";
	   if($courseFree=="Free")
	   {
		   $where.= " AND streamID IN (SELECT streamID from users WHERE userID=".$loginuserID.")";
		   $where.= " AND coursestdID IN (SELECT coursestdID from users WHERE userID=".$loginuserID.")";
	   }
	   else
	   {
		    $where.= " AND streamID IN (SELECT streamID from users WHERE userID=".$loginuserID.")";
			$where.= " AND coursestdID IN (SELECT coursestdID from users WHERE userID=".$loginuserID.")";
	   }
	   
	   
		
	   $result = Courses::find()->select('*')->where($where)->limit($pagesize)->orderby('courseName ASC')->offset($pagesize*$page)->asArray()->all();
	   if(count($result)>0)
       {
           $Coursesubjects = new Coursesubjects;
		   $Exams = new Exams;
		   $Apilog = new Apilog;
		   $ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$Re[$i]["subjects"] = $Coursesubjects->GetCoursesubjectsList($result[$i]["courseID"],$loginuserID) ;
				$Re[$i]['courseImage'] = $ImagePathArray['courseImage'].$result[$i]['courseImage'];
				//$Re[$i]["exams"] = $Exams->GetExam($result[$i]["courseID"],"","","",0,10,$loginuserID) ;
				$Re[$i]["exams"] = $Exams->getPracticeLevelExams($result[$i]["courseID"],0,0,$loginuserID) ;
             //GetExam($courseID=0,$coursesubjID="",$coursechapterIDs="",$examType="",$page="0",$pagesize="10",$loginuserID=0)
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
