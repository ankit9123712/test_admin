<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "doctorpromotionrequest".
 *
 * @property int $promoreqID
 * @property int $hospitalID
 * @property int $doctorID
 * @property string $promoreqConcatName
 * @property string $promoreqContactNo
 * @property string $promoreqEmail
 * @property string $promoreqInfo
 * @property string $promoreqStatus
 * @property string $promoreqCreatedDate
 */
class Doctorpromotionrequest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctorpromotionrequest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospitalID', 'doctorID', 'promoreqConcatName', 'promoreqContactNo', 'promoreqEmail', 'promoreqInfo'], 'required'],
            [['hospitalID', 'doctorID'], 'integer'],
            [['promoreqStatus'], 'string'],
            [['promoreqCreatedDate'], 'safe'],
            [['promoreqConcatName', 'promoreqEmail'], 'string', 'max' => 100],
            [['promoreqContactNo'], 'string', 'max' => 20],
            [['promoreqInfo'], 'string', 'max' => 2000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'promoreqID' => 'Promoreq ID',
            'hospitalID' => 'Hospital ID',
            'doctorID' => 'Doctor ID',
            'promoreqConcatName' => 'Promoreq Concat Name',
            'promoreqContactNo' => 'Promoreq Contact No',
            'promoreqEmail' => 'Promoreq Email',
            'promoreqInfo' => 'Promoreq Info',
            'promoreqStatus' => 'Promoreq Status',
            'promoreqCreatedDate' => 'Promoreq Created Date',
        ];
    }
}
