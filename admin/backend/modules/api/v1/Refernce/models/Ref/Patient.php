<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "patient".
 *
 * @property int $patientID
 * @property string $patientEmail
 * @property string $patientPassword
 * @property string $patientFirstName
 * @property string $patientLastName
 * @property string $patientDOB
 * @property string $patientCountryCode
 * @property string $patientMobile
 * @property string $patientGender
 * @property int $countryID
 * @property int $nationalityID
 * @property string $patientVerification
 * @property string $patientStatus
 * @property string $patientUpcomingPush
 * @property string $patientStatusPush
 * @property string $patientRatingPush
 * @property string $patientOfferPush
 * @property string $patientAdminPush
 * @property int $languageID
 * @property string $patientCreatedDate
 */
class Patient extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['patientEmail', 'patientPassword', 'patientFirstName', 'patientLastName', 'patientDOB', 'patientCountryCode', 'patientMobile', 'patientGender', 'countryID', 'nationalityID', 'patientVerification', 'patientStatus', 'languageID'], 'required'],
            [['patientDOB', 'patientCreatedDate'], 'safe'],
            [['patientGender', 'patientVerification', 'patientStatus', 'patientUpcomingPush', 'patientStatusPush', 'patientRatingPush', 'patientOfferPush', 'patientAdminPush','patientProfilePicture'], 'string'],
            [['countryID', 'nationalityID', 'languageID'], 'integer'],
            [['patientEmail', 'patientFirstName', 'patientLastName'], 'string', 'max' => 100],
            [['patientPassword'], 'string', 'max' => 300],
            [['patientCountryCode'], 'string', 'max' => 5],
            [['patientMobile'], 'string', 'max' => 20],
            [['patientEmail'], 'unique'],
            [['patientMobile'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'patientID' => 'Patient ID',
            'patientEmail' => 'Patient Email',
            'patientPassword' => 'Patient Password',
            'patientFirstName' => 'Patient First Name',
            'patientLastName' => 'Patient Last Name',
            'patientDOB' => 'Patient Dob',
            'patientCountryCode' => 'Patient Country Code',
            'patientMobile' => 'Patient Mobile',
            'patientGender' => 'Patient Gender',
            'countryID' => 'Country ID',
            'nationalityID' => 'Nationality ID',
            'patientVerification' => 'Patient Verification',
            'patientStatus' => 'Patient Status',
            'patientUpcomingPush' => 'Patient Upcoming Push',
            'patientStatusPush' => 'Patient Status Push',
            'patientRatingPush' => 'Patient Rating Push',
            'patientOfferPush' => 'Patient Offer Push',
            'patientAdminPush' => 'Patient Admin Push',
            'languageID' => 'Language ID',
            'patientCreatedDate' => 'Patient Created Date',
        ];
    }
	
	public function CheckPatientDetailsForDuplication($patientEmail="",$patientMobile="",$patientID="")
    {
        if(!empty($patientEmail))
        {
            if(empty($patientID))
            {
                $Exist_patientEmail = Patient::find()->where("patientEmail = '".$patientEmail."'")->all();
            }
            else 
            {
                $Exist_patientEmail = Patient::find()->where("patientEmail = '".$patientEmail."' and patientID <> '".$patientID."'")->all();
            }
           
            if(count($Exist_patientEmail) == 0)
            {
                if(!empty($patientMobile))
                {
                    if(empty($patientID))
                    {
                        $Exist_patientMobile = Patient::find()->where("patientMobile = '".$patientMobile."'")->all();
                    }
                    else 
                    {
                        $Exist_patientMobile = Patient::find()->where("patientMobile = '".$patientMobile."' and patientID <> '".$patientID."'")->all();
                    }

                    if(count($Exist_patientMobile) == 0)
                    {
                       return 'true';
                    }
                    else
                    {
                        return 'patientMobile';
                    }
                }
                else
                {
                    return 'true';
                }
            }
            else
            {
                return 'patientEmail';
            }
        }
        else
        {
            if(!empty($patientMobile))
            {
                if(empty($patientID))
                {
                    $Exist_patientMobile = Patient::find()->where("patientMobile = '".$patientMobile."'")->all();
                }
                else 
                {
                    $Exist_patientMobile = Patient::find()->where("patientMobile = '".$patientMobile."' and patientID <> '".$patientID."'")->all();
                }

                if(count($Exist_patientMobile) == 0)
                {
                    return 'true';
                }
                else
                {
                    return 'patientMobile';
                }
            }
            else
            {
               return 'true';
            }
        }
    }
	
	public function GetPatientDetails($patientID,$patientID="")
    {
       $Where = 'patient.patientStatus = "Active"';
	   
       $Re = array();
	   
       if(!empty($patientID))
       {
           $Where .= ' AND patient.patientID = "'.$patientID.'"';
       }

       $result = array();
       $query = new \yii\db\Query;
       $result = $query->select('patient.patientID,
					patient.patientEmail,
					patient.patientPassword,
					patient.patientFirstName,
					patient.patientLastName,
					patient.patientDOB,
					patient.patientCountryCode,
					patient.patientMobile,
					patient.patientGender,
					patient.countryID,
					patient.nationalityID,
					patient.patientVerification,
					patient.patientUpcomingPush,
					patient.patientStatusPush,
					patient.patientRatingPush,
					patient.patientOfferPush,
					patient.patientAdminPush,
					patient.languageID,
					patient.patientCreatedDate,
					patient.patientDeviceType,
					patient.patientDeviceID,
					patientProfilePicture,
					country.countryName,
					country.countryNameArabic,
					nationality.nationalityName,
					nationality.nationalityNameArabic')
       ->from('patient')
       ->JOIN('LEFT JOIN','country','patient.countryID = country.countryID')
       ->JOIN('LEFT JOIN','nationality','patient.nationalityID = nationality.nationalityID')
       ->where($Where)
       ->GROUPBY('patient.patientID')
       ->all();
       //echo $query->createCommand()->sql; die;
       if(count($result)>0)
       {
		   //$Apilog = new Apilog;
		   //$Settings = new Settings;
		   //$ImagePathArray = $Apilog->ImagePathArray();
		   //$Useraddress = new Useraddress;
           for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
                //$Re[$i]['userProfilePicture'] = $ImagePathArray['userProfilePicture'].$result[$i]['userProfilePicture'];
				//$Re[$i]['Delivery'] = $Useraddress->GetUserAddress("",$result[$i]['userID'],"Delivery");
                /*$Re[$i]['Billing'] = $Useraddress->GetUserAddress("",$result[$i]['userID'],"Billing");*/
                //$Re[$i]['settings'] = $Settings->GetSettingsApp();
                
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re;   
    }
	
}
