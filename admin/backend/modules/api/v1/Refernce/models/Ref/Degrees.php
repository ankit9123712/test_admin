<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "degrees".
 *
 * @property int $degreeID
 * @property string $degreeName
 * @property string $degreeNameArabic
 * @property string $degreeRemarks
 * @property string $degreeStatus
 * @property string $degreeCreatedDate
 */
class Degrees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'degrees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['degreeName', 'degreeNameArabic', 'degreeStatus'], 'required'],
            [['degreeStatus'], 'string'],
            [['degreeCreatedDate'], 'safe'],
            [['degreeName', 'degreeNameArabic'], 'string', 'max' => 100],
            [['degreeRemarks'], 'string', 'max' => 200],
            [['degreeName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'degreeID' => 'Degree ID',
            'degreeName' => 'Degree Name',
            'degreeNameArabic' => 'Degree Name Arabic',
            'degreeRemarks' => 'Degree Remarks',
            'degreeStatus' => 'Degree Status',
            'degreeCreatedDate' => 'Degree Created Date',
        ];
    }
	public function GetDegreesList($countryID=0,$pagesize="10",$page="0")
    {
		//echo $countryID;
	  
	   $result = Degrees::find()->select('*')->where('degreeStatus = "Active" ')->limit($pagesize)->offset($page*$pagesize)->orderby('degreeName ASC')->asArray()->all();
   
   
   
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   //$ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
                //$Re[$i]['countryFlagImage'] = $ImagePathArray['countryFlagImage'].$result[$i]['countryFlagImage'];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
