<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "doctorpromotion".
 *
 * @property int $doctorpromotionID
 * @property int $countryID
 * @property int $cityID
 * @property int $hospitalID
 * @property int $doctorID
 * @property int $specialityID
 * @property string $subspecialityIDs
 * @property string $doctorpromotionStartDate
 * @property string $doctorpromotionEndDate
 * @property string $doctorpromotionStatus
 * @property string $doctorpromotionCreatedDate
 */
class Doctorpromotion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctorpromotion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryID', 'cityID', 'hospitalID', 'doctorID', 'specialityID'], 'integer'],
            [['subspecialityIDs', 'doctorpromotionStartDate', 'doctorpromotionEndDate'], 'required'],
            [['doctorpromotionStartDate', 'doctorpromotionEndDate', 'doctorpromotionCreatedDate'], 'safe'],
            [['doctorpromotionStatus'], 'string'],
            [['subspecialityIDs'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doctorpromotionID' => 'Doctorpromotion ID',
            'countryID' => 'Country ID',
            'cityID' => 'City ID',
            'hospitalID' => 'Hospital ID',
            'doctorID' => 'Doctor ID',
            'specialityID' => 'Speciality ID',
            'subspecialityIDs' => 'Subspeciality I Ds',
            'doctorpromotionStartDate' => 'Doctorpromotion Start Date',
            'doctorpromotionEndDate' => 'Doctorpromotion End Date',
            'doctorpromotionStatus' => 'Doctorpromotion Status',
            'doctorpromotionCreatedDate' => 'Doctorpromotion Created Date',
        ];
    }
}
