<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "insuranceclass".
 *
 * @property int $insuranceclassID
 * @property int $insurancecompID
 * @property string $insuranceclassName
 * @property string $insuranceclassNameArabic
 */
class Insuranceclass extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insuranceclass';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insurancecompID', 'insuranceclassName', 'insuranceclassNameArabic'], 'required'],
            [['insurancecompID'], 'integer'],
            [['insuranceclassName', 'insuranceclassNameArabic'], 'string', 'max' => 100],
            [['insurancecompID', 'insuranceclassName'], 'unique', 'targetAttribute' => ['insurancecompID', 'insuranceclassName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insuranceclassID' => 'Insuranceclass ID',
            'insurancecompID' => 'Insurancecomp ID',
            'insuranceclassName' => 'Insuranceclass Name',
            'insuranceclassNameArabic' => 'Insuranceclass Name Arabic',
        ];
    }
	public function GetInsuranceclassList($insurancecompID,$pagesize="10",$page="0")
    {
		//echo $countryID;
	  
	   $result = Insuranceclass::find()->select('*')->where('insurancecompID="'.$insurancecompID.'"')->orderby('insuranceclassName ASC')->asArray()->all();
   
   
   
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
