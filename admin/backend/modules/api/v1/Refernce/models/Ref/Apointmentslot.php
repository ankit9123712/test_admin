<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "apointmentslot".
 *
 * @property int $aptslotID
 * @property string $aptslotName
 * @property string $aptslotNameArabic
 * @property string $aptslotRemarks
 * @property string $aptslotStatus
 * @property string $aptslotCreatedDate
 */
class Apointmentslot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apointmentslot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aptslotName', 'aptslotNameArabic', 'aptslotStatus'], 'required'],
            [['aptslotStatus'], 'string'],
            [['aptslotCreatedDate'], 'safe'],
            [['aptslotName', 'aptslotNameArabic'], 'string', 'max' => 100],
            [['aptslotRemarks'], 'string', 'max' => 200],
            [['aptslotName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'aptslotID' => 'Aptslot ID',
            'aptslotName' => 'Aptslot Name',
            'aptslotNameArabic' => 'Aptslot Name Arabic',
            'aptslotRemarks' => 'Aptslot Remarks',
            'aptslotStatus' => 'Aptslot Status',
            'aptslotCreatedDate' => 'Aptslot Created Date',
        ];
    }
	public function GetApointmentslotList($pagesize="10",$page="0")
    {
		//echo $countryID;
	  
	   $result = Apointmentslot::find()->select('*')->where('aptslotStatus = "Active" ')->limit($pagesize)->offset($page*$pagesize)->orderby('aptslotName ASC')->asArray()->all();
   
   
   
	   if(count($result)>0)
       {
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
