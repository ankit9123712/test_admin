<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "languagemsgs".
 *
 * @property int $langMsgID
 * @property int $languageID
 * @property string $langMsgKey
 * @property string $langMsgValue
 * @property string $langMsgStatus
 */
class Languagemsgs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'languagemsgs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['languageID', 'langMsgKey', 'langMsgValue'], 'required'],
            [['languageID'], 'integer'],
            [['langMsgStatus'], 'string'],
            [['langMsgKey'], 'string', 'max' => 250],
            [['langMsgValue'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'langMsgID' => 'Lang Msg I D',
            'languageID' => 'Language I D',
            'langMsgKey' => 'Lang Msg Key',
            'langMsgValue' => 'Lang Msg Value',
            'langMsgStatus' => 'Lang Msg Status',
        ];
    }
}
