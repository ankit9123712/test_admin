<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "subspeciality".
 *
 * @property int $subspecialityID
 * @property int $specialityID
 * @property string $subspecialityName
 * @property string $subspecialityNameArabic
 * @property string $subspecialityRemarks
 * @property string $subspecialityStatus
 * @property string $subspecialityCreatedDate
 */
class Subspeciality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subspeciality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['specialityID'], 'integer'],
            [['subspecialityName', 'subspecialityNameArabic'], 'required'],
            [['subspecialityStatus'], 'string'],
            [['subspecialityCreatedDate'], 'safe'],
            [['subspecialityName', 'subspecialityNameArabic'], 'string', 'max' => 100],
            [['subspecialityRemarks'], 'string', 'max' => 200],
            [['subspecialityName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subspecialityID' => 'Subspeciality ID',
            'specialityID' => 'Speciality ID',
            'subspecialityName' => 'Subspeciality Name',
            'subspecialityNameArabic' => 'Subspeciality Name Arabic',
            'subspecialityRemarks' => 'Subspeciality Remarks',
            'subspecialityStatus' => 'Subspeciality Status',
            'subspecialityCreatedDate' => 'Subspeciality Created Date',
        ];
    }
	public function GetSubspecialityList($specialityID=0,$pagesize="10",$page="0",$subspecialityIDs="")
    {
		//echo $countryID;
	   if($specialityID==0)		
	   {
	   $result = Subspeciality::find()->select('*')->where('subspecialityStatus = "Active"')->limit($pagesize)->offset($page*$pagesize)->orderby('subspecialityName ASC')->asArray()->all();
	   }
   else
	   $result = Subspeciality::find()->select('*')->where('subspecialityStatus = "Active" AND specialityID="'.$specialityID.'"')->limit($pagesize)->offset($page*$pagesize)->orderby('subspecialityName ASC')->asArray()->all();
   
		$Where = ' WHERE subspecialityStatus="Active" ';
		if(!empty($specialityID))		
		{
			$Where .= ' AND specialityID='.$specialityID;
		}
		if(!empty($subspecialityIDs))		
		{
			$Where .= " AND subspecialityID in (".$subspecialityIDs.") ";
		}
		
		$para = ' SELECT * ';
				
		$Re = array();
		$from = ' FROM subspeciality';		
		$order = " ORDER BY subspecialityName ASC";
		$groupby = " Group by subspecialityID";
		$sql = $para.$from.$join.$Where.$groupby.$order;
		//echo $sql; die;
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$result = $command->queryAll();
   
   
   
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   //$ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
                //$Re[$i]['countryFlagImage'] = $ImagePathArray['countryFlagImage'].$result[$i]['countryFlagImage'];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
