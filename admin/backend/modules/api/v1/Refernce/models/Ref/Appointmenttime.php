<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "appointmenttime".
 *
 * @property int $appointmenttimeID
 * @property int $appointmentID
 * @property string $appointmenttimeDay
 * @property string $appointmenttimeStart
 * @property string $appointmenttimeEnd
 * @property string $appointmenttimeSlot
 */
class Appointmenttime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appointmenttime';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['appointmentID', 'appointmenttimeDay', 'appointmenttimeStart', 'appointmenttimeEnd'], 'required'],
            [['appointmentID'], 'integer'],
            [['appointmenttimeStart', 'appointmenttimeEnd'], 'safe'],
            [['appointmenttimeSlot'], 'string'],
            [['appointmenttimeDay'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'appointmenttimeID' => 'Appointmenttime ID',
            'appointmentID' => 'Appointment ID',
            'appointmenttimeDay' => 'Appointmenttime Day',
            'appointmenttimeStart' => 'Appointmenttime Start',
            'appointmenttimeEnd' => 'Appointmenttime End',
            'appointmenttimeSlot' => 'Appointmenttime Slot',
        ];
    }
}
