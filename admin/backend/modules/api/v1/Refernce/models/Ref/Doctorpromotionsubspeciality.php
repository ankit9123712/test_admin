<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "doctorpromotionsubspeciality".
 *
 * @property string $doctorpromotionsubspecialityID
 * @property int $doctorpromotionID
 * @property int $subspecialityID
 * @property string $doctorpromotionsubspecialityCreatedDate
 */
class Doctorpromotionsubspeciality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctorpromotionsubspeciality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctorpromotionID', 'subspecialityID'], 'integer'],
            [['doctorpromotionsubspecialityCreatedDate'], 'safe'],
            [['doctorpromotionID', 'subspecialityID'], 'unique', 'targetAttribute' => ['doctorpromotionID', 'subspecialityID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doctorpromotionsubspecialityID' => 'Doctorpromotionsubspeciality ID',
            'doctorpromotionID' => 'Doctorpromotion ID',
            'subspecialityID' => 'Subspeciality ID',
            'doctorpromotionsubspecialityCreatedDate' => 'Doctorpromotionsubspeciality Created Date',
        ];
    }
}
