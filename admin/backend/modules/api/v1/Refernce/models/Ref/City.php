<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $cityID
 * @property int $countryID
 * @property int $stateID
 * @property string $cityName
 * @property string $cityNameArabic
 * @property string $cityRemarks
 * @property string $cityStatus
 * @property string $cityCreatedDate
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryID', 'stateID'], 'integer'],
            [['cityName', 'cityNameArabic'], 'required'],
            [['cityStatus'], 'string'],
            [['cityCreatedDate'], 'safe'],
            [['cityName', 'cityNameArabic'], 'string', 'max' => 100],
            [['cityRemarks'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cityID' => 'City ID',
            'countryID' => 'Country ID',
            'stateID' => 'State ID',
            'cityName' => 'City Name',
            'cityNameArabic' => 'City Name Arabic',
            'cityRemarks' => 'City Remarks',
            'cityStatus' => 'City Status',
            'cityCreatedDate' => 'City Created Date',
        ];
    }
	public function GetCityList($countryID=0,$pagesize="10",$page="0")
    {
		$Where = ' WHERE cityStatus = "Active"  ';
		if($countryID==0)
		{
			$Where .= ' AND countryID="'.$countryID.'"';
		}
		$para = 'SELECT city.*';
		$from = ' FROM city';
        $ORDERBY = " city.cityName";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
