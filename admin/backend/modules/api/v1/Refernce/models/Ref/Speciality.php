<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "speciality".
 *
 * @property int $specialityID
 * @property string $specialityName
 * @property string $specialityNameArabic
 * @property string $specialityRemarks
 * @property string $specialityStatus
 * @property string $specialityCreatedDate
 */
class Speciality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'speciality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['specialityName', 'specialityNameArabic'], 'required'],
            [['specialityStatus'], 'string'],
            [['specialityCreatedDate'], 'safe'],
            [['specialityName', 'specialityNameArabic'], 'string', 'max' => 100],
            [['specialityRemarks'], 'string', 'max' => 200],
            [['specialityName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'specialityID' => 'Speciality ID',
            'specialityName' => 'Speciality Name',
            'specialityNameArabic' => 'Speciality Name Arabic',
            'specialityRemarks' => 'Speciality Remarks',
            'specialityStatus' => 'Speciality Status',
            'specialityCreatedDate' => 'Speciality Created Date',
        ];
    }
	public function GetSpecialityList($pagesize="10",$page="0")
    {
	
	   $result = Speciality::find()->select('*')->where('specialityStatus = "Active" ')->limit($pagesize)->offset($page*$pagesize)->orderby('specialityName ASC')->asArray()->all();
   
   
   
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
