<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Hospital;
use backend\modules\api\v1\models\Doctorspeciality;
use backend\modules\api\v1\models\Doctortime;

/**
 * This is the model class for table "doctor".
 *
 * @property int $doctorID
 * @property int $hospitalID
 * @property string $doctorFirstName
 * @property string $doctorLastName
 * @property string $doctorFirstNameArabic
 * @property string $doctorLastNameArabic
 * @property int $degreeID
 * @property int $nationalityID
 * @property string $doctorGender
 * @property string $doctorProfileImage
 * @property string $doctorStatus
 * @property int $doctorRatingCount
 * @property string $doctorRatingAvg
 * @property string $doctorCreatedDate
 * @property string $specialityIDs
 */
class Doctor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospitalID', 'doctorFirstName', 'doctorLastName', 'doctorFirstNameArabic', 'doctorLastNameArabic', 'degreeID', 'nationalityID', 'doctorGender', 'doctorStatus', 'specialityIDs'], 'required'],
            [['hospitalID', 'degreeID', 'nationalityID', 'doctorRatingCount'], 'integer'],
            [['doctorGender', 'doctorStatus'], 'string'],
            [['doctorRatingAvg'], 'number'],
            [['doctorCreatedDate'], 'safe'],
            [['doctorFirstName', 'doctorLastName', 'doctorFirstNameArabic', 'doctorLastNameArabic', 'doctorProfileImage'], 'string', 'max' => 100],
            [['specialityIDs'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doctorID' => 'Doctor ID',
            'hospitalID' => 'Hospital ID',
            'doctorFirstName' => 'Doctor First Name',
            'doctorLastName' => 'Doctor Last Name',
            'doctorFirstNameArabic' => 'Doctor First Name Arabic',
            'doctorLastNameArabic' => 'Doctor Last Name Arabic',
            'degreeID' => 'Degree ID',
            'nationalityID' => 'Nationality ID',
            'doctorGender' => 'Doctor Gender',
            'doctorProfileImage' => 'Doctor Profile Image',
            'doctorStatus' => 'Doctor Status',
            'doctorRatingCount' => 'Doctor Rating Count',
            'doctorRatingAvg' => 'Doctor Rating Avg',
            'doctorCreatedDate' => 'Doctor Created Date',
            'specialityIDs' => 'Speciality I Ds',
        ];
    }
	/*

- subspeciality
- degree
- hospitals
- insurance providers
- location
- gender
	
	*/
	public function GetDoctorsList($loginuserID=0,$countryID=0, $cityID=0, $specialityID=0, $rating="", $subspecialityIDs="", $degreeIDs="", $hospitalIDs="",$insurancecompIDs="" ,$doctorGender="", $pagesize="0", $page="10", $sortBy="normal",$type="",$doctorID=0,$miniList="false")
    {
		
       $Where = ' WHERE hospitalStatus = "Active" AND hospitalVerification="Approve" AND specialityStatus = "Active"  AND cityStatus="Active" AND countryStatus="Active" AND degreeStatus="Active" AND subspecialityStatus="Active" AND nationalityStatus="Active" ';
		
		if($type=="mydoctor")
	    {
			$join = '   INNER Join degrees ON degrees.degreeID = doctor.degreeID
						INNER Join doctorspeciality ON doctorspeciality.doctorID = doctor.doctorID
						INNER Join speciality ON speciality.specialityID = doctorspeciality.specialityID
						INNER Join subspeciality ON subspeciality.specialityID = doctorspeciality.specialityID  
						INNER Join nationality ON nationality.nationalityID = doctor.nationalityID	
						INNER Join patientfavoriite ON patientfavoriite.doctorID = doctor.doctorID							
						INNER Join hospital ON hospital.hospitalID = doctor.hospitalID
						INNER Join city ON city.cityID = hospital.cityID
						INNER Join country ON country.countryID = hospital.countryID
						LEFT Join hospitalinsurance ON hospitalinsurance.hospitalID = hospital.hospitalID
						LEFT Join insurancecompany ON insurancecompany.insurancecompID = hospitalinsurance.insurancecompID ';					   
	    }
		else
		{
			$join = '   INNER Join degrees ON degrees.degreeID = doctor.degreeID
						INNER Join doctorspeciality ON doctorspeciality.doctorID = doctor.doctorID
						INNER Join speciality ON speciality.specialityID = doctorspeciality.specialityID
						INNER Join subspeciality ON subspeciality.specialityID = doctorspeciality.specialityID  
						INNER Join nationality ON nationality.nationalityID = doctor.nationalityID					
						INNER Join hospital ON hospital.hospitalID = doctor.hospitalID
						INNER Join city ON city.cityID = hospital.cityID
						INNER Join country ON country.countryID = hospital.countryID
						LEFT Join hospitalinsurance ON hospitalinsurance.hospitalID = hospital.hospitalID
						LEFT Join insurancecompany ON insurancecompany.insurancecompID = hospitalinsurance.insurancecompID		
						';
		}
		$para = 'SELECT doctor.*,nationalityName,specialityName,specialityNameArabic,subspecialityName, subspecialityNameArabic,degrees.degreeName, degrees.degreeNameArabic,countryFlagImage ';
				
	   
		
       if(!empty($countryID))
       {
            $Where .= ' AND hospital.countryID = "'.$countryID.'"';
       }
	   if(!empty($cityID))
       {
            $Where .= ' AND hospital.cityID = "'.$cityID.'"';
       }       
        if(!empty($specialityID))
       {
            $Where .= ' AND doctorspeciality.specialityID = "'.$specialityID.'"';
       }
      
       if(!empty($subspecialityIDs))
       {
            $Where .= ' AND doctorspeciality.subspecialityID IN ('.$subspecialityIDs.')';
       }
	   if(!empty($degreeIDs))
       {
            $Where .= ' AND doctor.degreeID IN ('.$degreeIDs.')';
       }
       if(!empty($hospitalIDs))
       {
            $Where .= ' AND doctor.hospitalID IN ('.$hospitalIDs.')';
       }
	   if(!empty($rating))
       {
            $Where .= ' AND doctor.doctorRatingAvg >= '.$doctorRatingAvg;
       }
	   if(!empty($doctorGender))
       {
            $Where .= ' AND doctor.doctorGender = "'.$doctorGender.'"';
       }
	   if(!empty($insurancecompIDs))
       {
            $Where .= ' AND hospitalinsurance.insurancecompID IN ('.$insurancecompIDs.')';
       }
	   if($type=="mydoctor")
	   {
		   $Where .= ' AND patientfavoriite.patientID = "'.$loginuserID.'"';
       }
	 if(!empty($doctorID))
       {
            $Where .= ' AND doctor.doctorID = "'.$doctorID.'"';
       }
	   if($type=="Active")
	   {
		   $Where .= ' AND doctorStatus = "Active"';
       }
	   if($type=="Deleted")
	   {
		   $Where .= ' AND doctorStatus = "Inactive"';
       }
	   if($type=="")
	   {
		   $Where .= ' AND doctorStatus = "Active"';
       }
	   

       $Re = array();

        $from = ' FROM doctor';	      
        $groupby = " GROUP BY doctor.doctorID";
		$order = " ORDER BY doctor.doctorRatingAvg DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$join.$Where.$groupby.$order.$LIMIT.$OFFSET;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

		if(count($result)>0)
        {
           $doctorSpeciality = new Doctorspeciality;
		   $Doctortime = new Doctortime;
		   $Hospital = new Hospital;
		   for($i=0;count($result)>$i; $i++) 
           { 

				$value = rand(90, 150);
				$Re[$i] = $result[$i];
				$Re[$i]["price"] = $value;
				//- doctor speciality
				$Re[$i]["speciality"]= $doctorSpeciality->GetDoctorSpecialityList($result[$i]["doctorID"]);
				//- doctors timeslots
				$Re[$i]["timeslots"]= $Doctortime->GetDoctorTimeList($result[$i]["doctorID"]);	
				//- doctor hospitals
				$Re[$i]["hospitals"]= $Hospital->GetHospitalList(0,0,0,"","10","0","normal",$result[$i]["hospitalID"]);	 
				
				//$Re[$i]["hospitals"]= $hosResult		;
				$Re[$i]["IsFavorite"]= "No";
				$favSQL =" SELECT * from patientfavoriite WHERE patientID='".$loginuserID."' AND  doctorID= ".$result[$i]["doctorID"];
				$connection = Yii::$app->getDb();
				$command = $connection->createCommand($favSQL);
				$favResult = $command->queryAll();
				if(count($favResult) >0 )
				{
					$Re[$i]["IsFavorite"]= "Yes";
				}
				
           }
        }
        else
        {
           $Re = $result;
        }
        return $Re;
    }
}
