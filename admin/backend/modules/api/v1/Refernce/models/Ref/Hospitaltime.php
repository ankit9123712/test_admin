<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "hospitaltime".
 *
 * @property int $hospitaltimeID
 * @property int $hospitalID
 * @property string $hospitaltimeDay
 * @property string $hospitaltimeDayArabic
 * @property string $hospitaltimeShift
 * @property string $hospitaltimeStart
 * @property string $hospitaltimeEnd
 * @property string $hospitaltimeContactNumber
 * @property string $hospitaltimeCreatedDate
 */
class Hospitaltime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hospitaltime';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospitalID', 'hospitaltimeDay', 'hospitaltimeDayArabic', 'hospitaltimeShift', 'hospitaltimeStart', 'hospitaltimeEnd', 'hospitaltimeContactNumber'], 'required'],
            [['hospitalID'], 'integer'],
            [['hospitaltimeStart', 'hospitaltimeEnd', 'hospitaltimeCreatedDate'], 'safe'],
            [['hospitaltimeDay', 'hospitaltimeDayArabic', 'hospitaltimeShift'], 'string', 'max' => 20],
            [['hospitaltimeContactNumber'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hospitaltimeID' => 'Hospitaltime ID',
            'hospitalID' => 'Hospital ID',
            'hospitaltimeDay' => 'Hospitaltime Day',
            'hospitaltimeDayArabic' => 'Hospitaltime Day Arabic',
            'hospitaltimeShift' => 'Hospitaltime Shift',
            'hospitaltimeStart' => 'Hospitaltime Start',
            'hospitaltimeEnd' => 'Hospitaltime End',
            'hospitaltimeContactNumber' => 'Hospitaltime Contact Number',
            'hospitaltimeCreatedDate' => 'Hospitaltime Created Date',
        ];
    }
	public function GetHospitalTimeList($hospitalID)
    {
		
		$Where = ' WHERE hospitalID	 = '.$hospitalID	;
		$para = ' SELECT hospitaltime.*';
				
		$Re = array();
        $from = ' FROM hospitaltime';		
		$order = " ORDER BY hospitaltimeDay ASC";
        $sql = $para.$from.$Where.$order;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
        $Re = $result;
        return $Re;
    }
}
