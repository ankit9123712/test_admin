<?php

namespace backend\modules\api\v1\models;use backend\modules\api\v1\models\Patient;

use Yii;

/**
 * This is the model class for table "offers".
 *
 * @property int $offersID
 * @property int $countryID
 * @property string $offersFor
 * @property string $offersAdvertiserName
 * @property string $offersAdvertiserNameArabic
 * @property int $specialityID
 * @property string $offersCode
 * @property string $offersBanner
 * @property string $offersStartDate
 * @property string $offersEndDate
 * @property string $offersExclusive
 * @property string $offersSendtoUsers
 * @property int $offersCountryID
 * @property int $nationalityID
 * @property string $offersGender
 * @property string $offersRemarks
 * @property string $offersStatus
 * @property string $offersCreatedDate
 */
class Offers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryID', 'specialityID', 'offersCountryID', 'nationalityID'], 'integer'],
            [['offersFor', 'offersAdvertiserName', 'offersAdvertiserNameArabic', 'offersCode', 'offersBanner', 'offersStartDate', 'offersEndDate', 'offersExclusive', 'offersSendtoUsers', 'offersGender'], 'required'],
            [['offersFor', 'offersExclusive', 'offersSendtoUsers', 'offersGender', 'offersStatus'], 'string'],
            [['offersStartDate', 'offersEndDate', 'offersCreatedDate'], 'safe'],
            [['offersAdvertiserName', 'offersAdvertiserNameArabic', 'offersBanner'], 'string', 'max' => 100],
            [['offersCode'], 'string', 'max' => 10],
            [['offersRemarks'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'offersID' => 'Offers ID',
            'countryID' => 'Country ID',
            'offersFor' => 'Offers For',
            'offersAdvertiserName' => 'Offers Advertiser Name',
            'offersAdvertiserNameArabic' => 'Offers Advertiser Name Arabic',
            'specialityID' => 'Speciality ID',
            'offersCode' => 'Offers Code',
            'offersBanner' => 'Offers Banner',
            'offersStartDate' => 'Offers Start Date',
            'offersEndDate' => 'Offers End Date',
            'offersExclusive' => 'Offers Exclusive',
            'offersSendtoUsers' => 'Offers Sendto Users',
            'offersCountryID' => 'Offers Country ID',
            'nationalityID' => 'Nationality ID',
            'offersGender' => 'Offers Gender',
            'offersRemarks' => 'Offers Remarks',
            'offersStatus' => 'Offers Status',
            'offersCreatedDate' => 'Offers Created Date',
        ];
    }
	public function GetOffersList($loginuserID,$nationalityID,$offersGender,$countryID,$specialityID,$degreeID,$hospitalID,$pagesize,$page)
    {
		//echo $countryID;
		$todayDate = date('Y-m-d');
		$where = "  1=1 AND  '".$todayDate."' Between offersStartDate AND offersEndDate AND offersStatus='Active' "	;		
		if($nationalityID>0)
		{
			$where .=" AND (nationalityID = '".$nationalityID."' OR nationalityID='0') ";
		}
		if(empty(!$offersGender))
		{
			$where .=" AND (offersGender = '".$offersGender."' OR offersGender='') ";
		}
		if(empty(!$countryID))
		{
			$where .=" AND (countryID = '".$countryID."' OR countryID='0') ";
		}
		if(empty(!$specialityID))
		{
			$where .=" AND ((specialityID in ('".$specialityID."') OR specialityID='0')) ";
		}
		
	  //echo $where;
	  //die;
	   $result = Offers::find()->select('*')->where($where)->limit($pagesize)->offset($page*$pagesize)->orderby('offersEndDate ASC')->asArray()->all();
   
   
   
	   if(count($result)>0)
       {

		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
