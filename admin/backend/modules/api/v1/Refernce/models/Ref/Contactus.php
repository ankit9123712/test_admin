<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "contactus".
 *
 * @property int $contactusID
 * @property string $contactusName
 * @property string $contactusEmail
 * @property string $contactusMobile
 * @property string $contactusSubject
 * @property string $contactusRemarks
 * @property string $contactusStatus
 * @property string $contactusCreatedDate
 */
class Contactus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contactus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contactusName', 'contactusEmail', 'contactusMobile', 'contactusSubject', 'contactusStatus'], 'required'],
            [['contactusStatus'], 'string'],
            [['contactusCreatedDate'], 'safe'],
            [['contactusName', 'contactusEmail', 'contactusSubject'], 'string', 'max' => 100],
            [['contactusMobile'], 'string', 'max' => 20],
            [['contactusRemarks'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'contactusID' => 'Contactus ID',
            'contactusName' => 'Contactus Name',
            'contactusEmail' => 'Contactus Email',
            'contactusMobile' => 'Contactus Mobile',
            'contactusSubject' => 'Contactus Subject',
            'contactusRemarks' => 'Contactus Remarks',
            'contactusStatus' => 'Contactus Status',
            'contactusCreatedDate' => 'Contactus Created Date',
        ];
    }
}
