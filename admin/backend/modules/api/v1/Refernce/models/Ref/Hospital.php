<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Hospitalinsurance;
use backend\modules\api\v1\models\Hospitaltime;
use backend\modules\api\v1\models\Hospitalpeciality;
/**
 * This is the model class for table "hospital".
 *
 * @property int $hospitalID
 * @property string $hospitalName
 * @property string $hospitalNameArabic
 * @property int $countryID
 * @property int $cityID
 * @property string $hospitalLogo
 * @property string $hospitalContactName
 * @property string $hospitalContactNumber
 * @property string $hospitalEmail
 * @property int $directionID
 * @property string $hospitalAddress
 * @property string $hospitalLatitude
 * @property string $hospitalLongitude
 * @property string $hospitalPaymentType
 * @property string $hospitalVerification
 * @property string $hospitalStatus
 * @property string $hospitalCreatedDate
 * @property string $specialityIDs
 */
class Hospital extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hospital';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospitalName', 'hospitalNameArabic', 'countryID', 'cityID', 'hospitalContactName', 'hospitalContactNumber', 'directionID', 'hospitalAddress', 'hospitalStatus', 'specialityIDs'], 'required'],
            [['countryID', 'cityID', 'directionID'], 'integer'],
            [['hospitalPaymentType', 'hospitalVerification', 'hospitalStatus'], 'string'],
            [['hospitalCreatedDate'], 'safe'],
            [['hospitalName', 'hospitalNameArabic', 'hospitalLogo', 'hospitalContactName', 'hospitalEmail'], 'string', 'max' => 100],
            [['hospitalContactNumber'], 'string', 'max' => 50],
            [['hospitalAddress'], 'string', 'max' => 500],
            [['hospitalLatitude', 'hospitalLongitude'], 'string', 'max' => 20],
            [['specialityIDs'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hospitalID' => 'Hospital ID',
            'hospitalName' => 'Hospital Name',
            'hospitalNameArabic' => 'Hospital Name Arabic',
            'countryID' => 'Country ID',
            'cityID' => 'City ID',
            'hospitalLogo' => 'Hospital Logo',
            'hospitalContactName' => 'Hospital Contact Name',
            'hospitalContactNumber' => 'Hospital Contact Number',
            'hospitalEmail' => 'Hospital Email',
            'directionID' => 'Direction ID',
            'hospitalAddress' => 'Hospital Address',
            'hospitalLatitude' => 'Hospital Latitude',
            'hospitalLongitude' => 'Hospital Longitude',
            'hospitalPaymentType' => 'Hospital Payment Type',
            'hospitalVerification' => 'Hospital Verification',
            'hospitalStatus' => 'Hospital Status',
            'hospitalCreatedDate' => 'Hospital Created Date',
            'specialityIDs' => 'Speciality I Ds',
        ];
    }
	  public function GetHospitalList($countryID=0,$cityID=0,$specialityID=0,$specialityIDs="",$pagesize="10",$page="0",$sortBy="normal",$hospitalID=0)
    {
		
       $Where = ' WHERE hospitalStatus = "Active" AND specialityStatus = "Active"  ';
		$join = '   Inner Join hospitalpeciality ON hospitalpeciality.hospitalID = hospital.hospitalID
					Inner Join country ON country.countryID = hospital.countryID
					Inner Join city ON city.cityID = hospital.cityID
					Inner Join direction ON direction.directionID = hospital.directionID
					Inner Join speciality ON speciality.specialityID = hospitalpeciality.specialityID                     ';
		$para = 'SELECT hospital.*, `speciality`.`specialityName`, countryName, countryNameArabic, cityName, cityNameArabic, directionName, directionNameArabic';
			// country, city, direction 	
	   
		
       if(!empty($countryID))
       {
            $Where .= ' AND hospital.countryID = "'.$countryID.'"';
       }
	   if(!empty($hospitalID))
       {
            $Where .= ' AND hospital.hospitalID = "'.$hospitalID.'"';
       }
	   if(!empty($cityID))
       {
            $Where .= ' AND hospital.cityID = "'.$cityID.'"';
       }       
        if(!empty($specialityID))
       {
            $Where .= ' AND hospitalpeciality.specialityID = "'.$specialityID.'"';
       }
      
       if(!empty($categoryIDs))
       {
            $Where .= ' AND hospitalpeciality.specialityID IN ('.$specialityIDs.')';
       }
       

       $Re = array();

        $from = ' FROM hospital';		
        //$ORDERBY = " hospital.hospitalName";
        $groupby = " GROUP BY hospital.hospitalID";
		if($sortBy=="normal")
			$order = " ORDER BY hospital.hospitalName ASC";
		else
			$order = " ORDER BY hospital.hospitalID DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$join.$Where.$groupby.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		if(count($result)>0)
        {
			$Hospitaltime = new Hospitaltime;
			$Hospitalpeciality = new Hospitalpeciality;
			$Hospitalinsurance = new Hospitalinsurance;
			for($i=0;count($result)>$i; $i++) 
			{ 
				$Re[$i] = $result[$i];
				$hospitalID = $result[$i]["hospitalID"];
				//hospital speciality
				//hospital subspeciality
				$hosSpRes= $Hospitalpeciality->GetHospitalSpecialityList($hospitalID);
				$Re[$i]["hospitalpeciality"] = $hosSpRes;
				
				//hospitaltime				
				$timeRes = $Hospitaltime->GetHospitalTimeList($hospitalID);
				$Re[$i]["hospitalTime"] = $timeRes;
				
				//hospital insurance providers, insurance classes
				$InsRes = $Hospitalinsurance->GetHospitalInsuranceList($hospitalID);
				$Re[$i]["Hospitalinsurance"] = $InsRes;
				
				
			}	
		
		}
        else
        {
           $Re = $result;
        }

           
       
       //return $result;   
	  return $Re;
    }
	
}
