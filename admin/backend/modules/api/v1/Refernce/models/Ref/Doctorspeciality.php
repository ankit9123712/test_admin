<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Subspeciality;

/**
 * This is the model class for table "doctorspeciality".
 *
 * @property int $doctorspecialityID
 * @property int $doctorID
 * @property int $specialityID
 * @property int $subspecialityID
 * @property string $doctorspecialityCreatedDate
 */
class Doctorspeciality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctorspeciality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctorID', 'specialityID', 'subspecialityID'], 'required'],
            [['doctorID', 'specialityID', 'subspecialityID'], 'integer'],
            [['doctorspecialityCreatedDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doctorspecialityID' => 'Doctorspeciality ID',
            'doctorID' => 'Doctor ID',
            'specialityID' => 'Speciality ID',
            'subspecialityID' => 'Subspeciality ID',
            'doctorspecialityCreatedDate' => 'Doctorspeciality Created Date',
        ];
    }
	public function GetDoctorSpecialityList($doctorID)
    {
		
		$Where = ' WHERE speciality.specialityStatus="Active" AND doctorspeciality.doctorID = '.$doctorID;
		$join = '   INNER JOIN speciality on speciality.specialityID = doctorspeciality.specialityID ';
		$para = ' SELECT speciality.specialityName, speciality.specialityNameArabic,doctorspeciality.specialityID , group_concat(subspecialityID) as subspecialityID';
				
		$Re = array();
        $from = ' FROM doctorspeciality';		
		$order = " ORDER BY speciality.specialityName ASC";
		$groupby = " Group by doctorspeciality.specialityID";
        $sql = $para.$from.$join.$Where.$groupby.$order;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		if(count($result)>0)
        { 
		   $subSpeciality = new Subspeciality;	
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$Re[$i]["Subspeciality"]= $subSpeciality->GetSubspecialityList($result[$i]["specialityID"],"10","0",$result[$i]["subspecialityID"]); 

           }
        }
        else
        {
           $Re = $result;
        }
        return $Re;
    }

    public function DeleteDoctorspeciality($doctorID)
    {
        $Doctorspeciality_Res = Doctorspeciality::find()->where(['doctorID'=>$doctorID])->one();
        if(!empty($Doctorspeciality_Res))
        {
             $Delete = Doctorspeciality::deleteAll('doctorID = :doctorID', [':doctorID' => $doctorID]);
        }
    }
}
