<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Insuranceclass;

/**
 * This is the model class for table "insurancecompany".
 *
 * @property int $insurancecompID
 * @property int $countryID
 * @property string $insurancecompName
 * @property string $insurancecompNameArabic
 * @property string $insurancecompRemark
 * @property string $insuranceclassIDs
 * @property string $insurancecompStatus
 * @property string $insurancecompCreatedDate
 */
class Insurancecompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurancecompany';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryID'], 'integer'],
            [['insurancecompName', 'insurancecompNameArabic', 'insuranceclassIDs', 'insurancecompStatus'], 'required'],
            [['insurancecompStatus'], 'string'],
            [['insurancecompCreatedDate'], 'safe'],
            [['insurancecompName', 'insurancecompNameArabic'], 'string', 'max' => 100],
            [['insurancecompRemark'], 'string', 'max' => 200],
            [['insuranceclassIDs'], 'string', 'max' => 500],
            [['countryID', 'insurancecompName'], 'unique', 'targetAttribute' => ['countryID', 'insurancecompName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insurancecompID' => 'Insurancecomp ID',
            'countryID' => 'Country ID',
            'insurancecompName' => 'Insurancecomp Name',
            'insurancecompNameArabic' => 'Insurancecomp Name Arabic',
            'insurancecompRemark' => 'Insurancecomp Remark',
            'insuranceclassIDs' => 'Insuranceclass I Ds',
            'insurancecompStatus' => 'Insurancecomp Status',
            'insurancecompCreatedDate' => 'Insurancecomp Created Date',
        ];
    }
	public function GetInsurancecompanyList($countryID=0,$pagesize="10",$page="0")
    {
		//echo $countryID;
	  
	   $result = Insurancecompany::find()->select('*')->where('insurancecompStatus = "Active" ')->limit($pagesize)->offset($page*$pagesize)->orderby('insurancecompName ASC')->asArray()->all();
   
   
   
	   if(count($result)>0)
       {
           $Insuranceclass = new Insuranceclass;
		   
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
                $Re[$i]['insuranceclass'] = $Insuranceclass->GetInsuranceclassList($result[$i]["insurancecompID"]);
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }

}
