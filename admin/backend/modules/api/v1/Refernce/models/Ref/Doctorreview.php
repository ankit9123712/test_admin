<?php

namespace backend\modules\api\v1\models;

use Yii;
use backend\modules\api\v1\models\Hospital;
use backend\modules\api\v1\models\Doctorspeciality;
use backend\modules\api\v1\models\Doctortime;
use backend\modules\api\v1\models\Doctor;
/**
 * This is the model class for table "doctorreview".
 *
 * @property int $doctorreviewID
 * @property int $doctorID
 * @property int $patientID
 * @property string $doctorreviewReview
 * @property string $doctorreviewDate
 * @property string $doctorreviewPatientApproved
 * @property string $doctorreviewReply
 * @property string $doctorreviewReplyDate
 * @property string $doctorreviewApproved
 * @property string $doctorreviewCreatedDate
 */
class Doctorreview extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctorreview';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctorreviewID', 'doctorID', 'patientID', 'doctorreviewReview', 'doctorreviewDate'], 'required'],
            [['doctorreviewID', 'doctorID', 'patientID'], 'integer'],
            [['doctorreviewDate', 'doctorreviewReplyDate', 'doctorreviewCreatedDate'], 'safe'],
            [['doctorreviewPatientApproved', 'doctorreviewApproved'], 'string'],
            [['doctorreviewReview', 'doctorreviewReply'], 'string', 'max' => 1000],
            [['doctorreviewID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doctorreviewID' => 'Doctorreview ID',
            'doctorID' => 'Doctor ID',
            'patientID' => 'Patient ID',
            'doctorreviewReview' => 'Doctorreview Review',
            'doctorreviewDate' => 'Doctorreview Date',
            'doctorreviewPatientApproved' => 'Doctorreview Patient Approved',
            'doctorreviewReply' => 'Doctorreview Reply',
            'doctorreviewReplyDate' => 'Doctorreview Reply Date',
            'doctorreviewApproved' => 'Doctorreview Approved',
            'doctorreviewCreatedDate' => 'Doctorreview Created Date',
        ];
    }
	public function GetDoctorReviewList($doctorID,$pagesize="0",$page="10",$hospitalID=0,$doctorreviewID=0)
    {
		
		$Where = ' WHERE doctorreviewPatientApproved="Approved" ';//AND doctorreview.doctorID = '.$doctorID;
		if(!empty($hospitalID))
			$Where .= ' AND doctor.hospitalID= '.$hospitalID;
		if(!empty($doctorID))
			$Where .= ' AND doctor.doctorID= '.$doctorID;
		if(!empty($doctorreviewID))
			$Where .= ' AND doctorreviewID= '.$doctorreviewID;
		
		$join = '   INNER JOIN patient on patient.patientID = doctorreview.patientID 
					INNER JOIN doctor on doctor.doctorID = doctorreview.doctorID 
					INNER JOIN appointment on appointment.appointmentID = doctorreview.appointmentID ';
		$para = ' SELECT doctorreview.*, patientFirstName,patientLastName,patientProfilePicture, doctorFirstName,doctorLastName, doctorFirstNameArabic, doctorLastNameArabic, doctorProfileImage, appointmentConfirmDateTime, hospitalID';
		$LIMIT = " LIMIT $pagesize";
		$OFFSET = " OFFSET ".$page*$pagesize;                
		
		$Re = array();
        $from = ' FROM doctorreview';		
		$order = " ORDER BY doctorreview.doctorreviewDate DESC";
		$sql = $para.$from.$join.$Where.$order.$LIMIT.$OFFSET;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
        if(count($result)>0)
        {
           $doctorSpeciality = new Doctorspeciality;
		   $Doctortime = new Doctortime;
		   $Hospital = new Hospital;
		   for($i=0;count($result)>$i; $i++) 
           { 
				$value = rand(90, 150);
				$Re[$i] = $result[$i];
				$Re[$i]["price"] = $value;
				//- doctor speciality
				$Re[$i]["speciality"]= $doctorSpeciality->GetDoctorSpecialityList($result[$i]["doctorID"]);
				//- doctors timeslots
				$Re[$i]["timeslots"]= $Doctortime->GetDoctorTimeList($result[$i]["doctorID"]);	
				//- doctor hospitals
				$Re[$i]["hospitals"]= $Hospital->GetHospitalList(0,0,0,"","10","0","normal",$result[$i]["hospitalID"]);	 
				
				//$Re[$i]["hospitals"]= $hosResult		;
				$Re[$i]["IsFavorite"]= "No";
				
				
           }
        }
        else
        {
           $Re = $result;
        }
        return $Re;
        return $Re;
    }
}
