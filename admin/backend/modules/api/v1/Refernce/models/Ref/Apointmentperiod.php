<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "apointmentperiod".
 *
 * @property int $aptperiodID
 * @property string $aptperiodName
 * @property string $aptperiodNameArabic
 * @property string $aptperiodRemarks
 * @property string $aptperiodStatus
 * @property string $aptperiodCreatedDate
 */
class Apointmentperiod extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apointmentperiod';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aptperiodName', 'aptperiodNameArabic', 'aptperiodStatus'], 'required'],
            [['aptperiodStatus'], 'string'],
            [['aptperiodCreatedDate'], 'safe'],
            [['aptperiodName', 'aptperiodNameArabic'], 'string', 'max' => 100],
            [['aptperiodRemarks'], 'string', 'max' => 200],
            [['aptperiodName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'aptperiodID' => 'Aptperiod ID',
            'aptperiodName' => 'Aptperiod Name',
            'aptperiodNameArabic' => 'Aptperiod Name Arabic',
            'aptperiodRemarks' => 'Aptperiod Remarks',
            'aptperiodStatus' => 'Aptperiod Status',
            'aptperiodCreatedDate' => 'Aptperiod Created Date',
        ];
    }
	public function GetApointmentperiodList($pagesize="10",$page="0")
    {
		//echo $countryID;
	  
	   $result = Apointmentperiod::find()->select('*')->where('aptperiodStatus = "Active" ')->limit($pagesize)->offset($page*$pagesize)->orderby('aptperiodName ASC')->asArray()->all();
   
   
   
	   if(count($result)>0)
       {
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
