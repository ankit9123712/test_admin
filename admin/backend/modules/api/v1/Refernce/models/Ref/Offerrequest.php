<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "offerrequest".
 *
 * @property int $offerrequestID
 * @property int $countryID
 * @property int $cityID
 * @property string $offerrequestName
 * @property string $offerrequestEmail
 * @property string $offerrequestMobile
 * @property string $offerrequestSubject
 * @property string $offerrequestRemarks
 * @property string $offerrequestStatus
 * @property string $offerrequestCreatedDate
 */
class Offerrequest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offerrequest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryID', 'cityID', 'offerrequestName', 'offerrequestEmail', 'offerrequestMobile', 'offerrequestSubject', 'offerrequestStatus'], 'required'],
            [['countryID', 'cityID'], 'integer'],
            [['offerrequestStatus'], 'string'],
            [['offerrequestCreatedDate'], 'safe'],
            [['offerrequestName', 'offerrequestEmail', 'offerrequestSubject'], 'string', 'max' => 100],
            [['offerrequestMobile'], 'string', 'max' => 20],
            [['offerrequestRemarks'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'offerrequestID' => 'Offerrequest ID',
            'countryID' => 'Country ID',
            'cityID' => 'City ID',
            'offerrequestName' => 'Offerrequest Name',
            'offerrequestEmail' => 'Offerrequest Email',
            'offerrequestMobile' => 'Offerrequest Mobile',
            'offerrequestSubject' => 'Offerrequest Subject',
            'offerrequestRemarks' => 'Offerrequest Remarks',
            'offerrequestStatus' => 'Offerrequest Status',
            'offerrequestCreatedDate' => 'Offerrequest Created Date',
        ];
    }
}
