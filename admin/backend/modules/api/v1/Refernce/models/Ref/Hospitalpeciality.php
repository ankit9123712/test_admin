<?php

namespace backend\modules\api\v1\models;

use Yii;use backend\modules\api\v1\models\Subspeciality;

/**
 * This is the model class for table "hospitalpeciality".
 *
 * @property int $hospitalpecialityID
 * @property int $hospitalID
 * @property int $specialityID
 * @property string $hospitalpecialityCreatedDate
 */
class Hospitalpeciality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hospitalpeciality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospitalID', 'specialityID'], 'integer'],
            [['hospitalpecialityCreatedDate'], 'safe'],
            [['hospitalID', 'specialityID'], 'unique', 'targetAttribute' => ['hospitalID', 'specialityID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hospitalpecialityID' => 'Hospitalpeciality ID',
            'hospitalID' => 'Hospital ID',
            'specialityID' => 'Speciality ID',
            'hospitalpecialityCreatedDate' => 'Hospitalpeciality Created Date',
        ];
    }
	public function GetHospitalSpecialityList($hospitalID)
    {
		
		$Where = ' WHERE speciality.specialityStatus="Active" AND hospitalpeciality.hospitalID = '.$hospitalID;
		$join = '   INNER JOIN speciality on speciality.specialityID = hospitalpeciality.specialityID ';
		$para = ' SELECT speciality.specialityName, speciality.specialityNameArabic,hospitalpeciality.specialityID ';
				
		$Re = array();
        $from = ' FROM hospitalpeciality';		
		$order = " ORDER BY speciality.specialityName ASC";
        $sql = $para.$from.$join.$Where.$groupby.$order;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		if(count($result)>0)
        { 
		   $subSpeciality = new Subspeciality;	
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$Re[$i]["Subspeciality"]= $subSpeciality->GetSubspecialityList($result[$i]["specialityID"],"100","0"); 

           }
        }
        else
        {
           $Re = $result;
        }
        return $Re;
	}
}
