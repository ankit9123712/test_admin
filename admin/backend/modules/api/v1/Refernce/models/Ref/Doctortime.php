<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "doctortime".
 *
 * @property int $doctortimeID
 * @property int $doctorID
 * @property string $doctortimeDay
 * @property string $doctortimeDayArabic
 * @property string $doctortimeShift
 * @property string $doctortimeStart
 * @property string $doctortimeEnd
 * @property string $doctortimeContactNumber
 * @property string $doctortimeCreatedDate
 */
class Doctortime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctortime';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctorID', 'doctortimeDay', 'doctortimeDayArabic', 'doctortimeShift', 'doctortimeStart', 'doctortimeEnd', 'doctortimeContactNumber'], 'required'],
            [['doctorID'], 'integer'],
            [['doctortimeStart', 'doctortimeEnd', 'doctortimeCreatedDate'], 'safe'],
            [['doctortimeDay', 'doctortimeDayArabic', 'doctortimeShift'], 'string', 'max' => 20],
            [['doctortimeContactNumber'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doctortimeID' => 'Doctortime ID',
            'doctorID' => 'Doctor ID',
            'doctortimeDay' => 'Doctortime Day',
            'doctortimeDayArabic' => 'Doctortime Day Arabic',
            'doctortimeShift' => 'Doctortime Shift',
            'doctortimeStart' => 'Doctortime Start',
            'doctortimeEnd' => 'Doctortime End',
            'doctortimeContactNumber' => 'Doctortime Contact Number',
            'doctortimeCreatedDate' => 'Doctortime Created Date',
        ];
    }
	public function GetDoctorTimeList($doctorID)
    {
		
		$Where = ' WHERE doctortime.doctorID = '.$doctorID;
		$para = ' SELECT doctortimeID,doctortimeDay,doctortimeDayArabic,doctortimeShift	,doctortimeStart ,doctortimeEnd,doctortimeContactNumber';
				
		$Re = array();
        $from = ' FROM doctortime';		
		$order = " ORDER BY doctortime.doctortimeDay ASC";
        $sql = $para.$from.$Where.$order;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
        $Re = $result;
        return $Re;
    }
}
