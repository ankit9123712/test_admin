<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $countryID
 * @property string $countryName
 * @property string $countryNameArabic
 * @property string $countryISO3Code
 * @property string $countryISO2Code
 * @property string $countryCurrencyCode
 * @property string $countryDialCode
 * @property string $countryFlagImage
 * @property string $countryStatus
 * @property string $countryRemarks
 * @property string $countryCreatedDate
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryName', 'countryNameArabic', 'countryISO3Code', 'countryISO2Code', 'countryCurrencyCode', 'countryDialCode', 'countryFlagImage'], 'required'],
            [['countryStatus'], 'string'],
            [['countryCreatedDate'], 'safe'],
            [['countryName', 'countryNameArabic', 'countryFlagImage'], 'string', 'max' => 100],
            [['countryISO3Code'], 'string', 'max' => 3],
            [['countryISO2Code'], 'string', 'max' => 2],
            [['countryCurrencyCode'], 'string', 'max' => 10],
            [['countryDialCode'], 'string', 'max' => 5],
            [['countryRemarks'], 'string', 'max' => 200],
            [['countryName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'countryID' => 'Country ID',
            'countryName' => 'Country Name',
            'countryNameArabic' => 'Country Name Arabic',
            'countryISO3Code' => 'Country Iso3 Code',
            'countryISO2Code' => 'Country Iso2 Code',
            'countryCurrencyCode' => 'Country Currency Code',
            'countryDialCode' => 'Country Dial Code',
            'countryFlagImage' => 'Country Flag Image',
            'countryStatus' => 'Country Status',
            'countryRemarks' => 'Country Remarks',
            'countryCreatedDate' => 'Country Created Date',
        ];
    }
	
	 public function GetCountryList()
    {
	   $result = Country::find()->select('countryID,countryName,countryNameArabic,countryISO3Code,countryISO2Code,countryCurrencyCode,countryDialCode,countryFlagImage,countryRemarks')->where('countryStatus = "Active"')->orderby('countryName ASC')->asArray()->all();
	   if(count($result)>0)
       {
           $Apilog = new Apilog;
		   $ImagePathArray = $Apilog->ImagePathArray();
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
                $Re[$i]['countryFlagImage'] = $ImagePathArray['countryFlagImage'].$result[$i]['countryFlagImage'];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
