<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property int $faqID
 * @property int $faqtopicID
 * @property string $faqQuestion
 * @property string $faqAnswer
 * @property string $faqStatus
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['faqtopicID'], 'integer'],
            [['faqQuestion', 'faqAnswer', 'faqStatus'], 'required'],
            [['faqAnswer', 'faqStatus'], 'string'],
            [['faqQuestion'], 'string', 'max' => 200],
            [['faqtopicID', 'faqQuestion'], 'unique', 'targetAttribute' => ['faqtopicID', 'faqQuestion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'faqID' => 'Faq ID',
            'faqtopicID' => 'Faqtopic ID',
            'faqQuestion' => 'Faq Question',
            'faqAnswer' => 'Faq Answer',
            'faqStatus' => 'Faq Status',
        ];
    }
	public function GetFaqList()
    {   
        $para = 'SELECT faqID,faqQuestion,faqAnswer	';				
        $Where = ' WHERE faqStatus="Active" ';
		$from = ' FROM faq';
		$ORDERBY = " faqQuestion";
		$groupby = " GROUP BY faq.faqID";
		$order = " ORDER BY $ORDERBY ASC";
		
		
		
		$sql = $para.$from.$Where.$groupby.$order;
		
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$result = $command->queryAll();
		return $result;
    }
}
