<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "patientdependent".
 *
 * @property int $patientdepenID
 * @property int $patientID
 * @property string $patientdepenFirstName
 * @property string $patientdepenLastName
 * @property string $patientdepenDOB
 * @property string $patientdepenGender
 * @property int $countryID
 * @property int $nationalityID
 * @property string $patientdepenCreatedDate
 */
class Patientdependent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patientdependent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['patientID', 'patientdepenFirstName', 'patientdepenLastName', 'patientdepenDOB', 'patientdepenGender', 'countryID', 'nationalityID'], 'required'],
            [['patientID', 'countryID', 'nationalityID'], 'integer'],
            [['patientdepenDOB', 'patientdepenCreatedDate'], 'safe'],
            [['patientdepenGender'], 'string'],
            [['patientdepenFirstName', 'patientdepenLastName'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'patientdepenID' => 'Patientdepen ID',
            'patientID' => 'Patient ID',
            'patientdepenFirstName' => 'Patientdepen First Name',
            'patientdepenLastName' => 'Patientdepen Last Name',
            'patientdepenDOB' => 'Patientdepen Dob',
            'patientdepenGender' => 'Patientdepen Gender',
            'countryID' => 'Country ID',
            'nationalityID' => 'Nationality ID',
            'patientdepenCreatedDate' => 'Patientdepen Created Date',
        ];
    }
	public function GetPatientdependentDetails($patientID)
    {
       $Where = ' 1 = 1 ';
	   
       $Re = array();
	   
       if(!empty($patientID))
       {
           $Where .= ' AND patientID = "'.$patientID.'"';
       }

       $result = array();
       $query = new \yii\db\Query;
       $result = $query->select('patientdependent.*,
					country.countryName,
					country.countryNameArabic,
					nationality.nationalityName,
					nationality.nationalityNameArabic')
       ->from('patientdependent')
       ->JOIN('LEFT JOIN','country','patientdependent.countryID = country.countryID')
       ->JOIN('LEFT JOIN','nationality','patientdependent.nationalityID = nationality.nationalityID')
       ->where($Where)
       ->GROUPBY('patientdependent.patientdepenID')
       ->all();
       //echo $query->createCommand()->sql; die;
       if(count($result)>0)
       {
		   
           for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
               
                
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re;   
    }
	
}
