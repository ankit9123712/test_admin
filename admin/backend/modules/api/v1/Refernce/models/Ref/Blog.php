<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property int $blogID
 * @property string $blogTitle
 * @property string $blogDate
 * @property string $blogImage
 * @property string $blogDescription
 * @property string $blogStatus
 * @property string $blogCreatedDate
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['blogTitle', 'blogDate', 'blogImage', 'blogDescription', 'blogStatus'], 'required'],
            [['blogDate', 'blogCreatedDate'], 'safe'],
            [['blogDescription', 'blogStatus'], 'string'],
            [['blogTitle'], 'string', 'max' => 250],
            [['blogImage'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'blogID' => 'Blog ID',
            'blogTitle' => 'Blog Title',
            'blogDate' => 'Blog Date',
            'blogImage' => 'Blog Image',
            'blogDescription' => 'Blog Description',
            'blogStatus' => 'Blog Status',
            'blogCreatedDate' => 'Blog Created Date',
        ];
    }
	public function GetBlogList($pagesize="10",$page="0")
    {
		//echo $countryID;
	  
	   $result = Blog::find()->select('*')->where('blogStatus = "Active" ')->limit($pagesize)->offset($page*$pagesize)->orderby('blogDate,blogTitle ASC')->asArray()->all();
   
   
   
	   if(count($result)>0)
       {
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
