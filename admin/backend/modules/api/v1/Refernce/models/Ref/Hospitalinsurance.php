<?php

namespace backend\modules\api\v1\models;

use Yii;use backend\modules\api\v1\models\Insuranceclass;

/**
 * This is the model class for table "hospitalinsurance".
 *
 * @property int $hospitalinsuranceID
 * @property int $hospitalID
 * @property int $insurancecompID
 * @property int $insuranceclassID
 * @property string $hospitalinsuranceCreatedDate
 */
class Hospitalinsurance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hospitalinsurance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospitalinsuranceID', 'hospitalID', 'insurancecompID', 'insuranceclassID'], 'required'],
            [['hospitalinsuranceID', 'hospitalID', 'insurancecompID', 'insuranceclassID'], 'integer'],
            [['hospitalinsuranceCreatedDate'], 'safe'],
            [['hospitalinsuranceID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hospitalinsuranceID' => 'Hospitalinsurance ID',
            'hospitalID' => 'Hospital ID',
            'insurancecompID' => 'Insurancecomp ID',
            'insuranceclassID' => 'Insuranceclass ID',
            'hospitalinsuranceCreatedDate' => 'Hospitalinsurance Created Date',
        ];
    }
	public function GetHospitalInsuranceList($hospitalID)
    {
		
		$Where = ' WHERE insurancecompany.insurancecompStatus="Active" AND hospitalinsurance.hospitalID = '.$hospitalID;
		$join = '   INNER JOIN insurancecompany on insurancecompany.insurancecompID = hospitalinsurance.insurancecompID ';
		$para = ' SELECT insurancecompany.insurancecompName, insurancecompany.insurancecompNameArabic,hospitalinsurance.insurancecompID ';
				
		$Re = array();
        $from = ' FROM hospitalinsurance';		
		$order = " ORDER BY insurancecompany.insurancecompName ASC";
        $sql = $para.$from.$join.$Where.$groupby.$order;
        //echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		if(count($result)>0)
        { 
		   
		   for($i=0;count($result)>$i; $i++) 
           { 
                $Re[$i] = $result[$i];
				$hospitalID = $result[$i]["hospitalID"];
				$insurancecompID = $result[$i]["insurancecompID"];
				
				$clsSQL =" SELECT insuranceclassName,insuranceclassNameArabic, hospitalinsurance.insurancecompID From hospitalinsurance
inner join insuranceclass on insuranceclass.insuranceclassID = hospitalinsurance.insuranceclassID 
WHERE hospitalinsurance.insurancecompID = ".$insurancecompID." AND hospitalID=".$hospitalID;
				$connection = Yii::$app->getDb();
				$command = $connection->createCommand($clsSQL);
				$clsResult = $command->queryAll();
				if(count($clsResult)>0)
				{
					$Re[$i]["insuranceclass"] = $clsResult;	
				}
				else
				{
					$tmp =  array();
					$Re[$i]["insuranceclass"] = $tmp;
				}
				

           }
        }
        else
        {
           $Re = $result;
        }
        return $Re;
	}
}
