<?php

namespace backend\modules\api\v1\models;

use Yii;use backend\modules\api\v1\models\Doctorspeciality;

/**
 * This is the model class for table "appointment".
 *
 * @property int $appointmentID
 * @property int $patientID
 * @property int $patientdepenID
 * @property int $doctorID
 * @property string $appointmentPeriod
 * @property string $appointmentSlot
 * @property string $appointmentByCalendar
 * @property string $appointmentPayment
 * @property int $insurancecompID
 * @property int $insuranceclassName
 * @property string $appointmentOfferCode
 * @property string $appointmentStatus
 * @property string $appointmentCharge
 * @property string $appointmentCreatedDate
 */
class Appointment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appointment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['patientID', 'doctorID', 'appointmentPeriod', 'appointmentSlot', 'appointmentByCalendar', 'appointmentPayment', 'insurancecompID', 'insuranceclassName', 'appointmentOfferCode', 'appointmentCharge'], 'required'],
            [['patientID', 'patientdepenID', 'doctorID', 'insurancecompID', 'insuranceclassName'], 'integer'],
            [['appointmentByCalendar', 'appointmentPayment', 'appointmentStatus'], 'string'],
            [['appointmentCharge'], 'number'],
            [['appointmentCreatedDate'], 'safe'],
            [['appointmentPeriod'], 'string', 'max' => 50],
            [['appointmentSlot'], 'string', 'max' => 20],
            [['appointmentOfferCode'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'appointmentID' => 'Appointment ID',
            'patientID' => 'Patient ID',
            'patientdepenID' => 'Patientdepen ID',
            'doctorID' => 'Doctor ID',
            'appointmentPeriod' => 'Appointment Period',
            'appointmentSlot' => 'Appointment Slot',
            'appointmentByCalendar' => 'Appointment By Calendar',
            'appointmentPayment' => 'Appointment Payment',
            'insurancecompID' => 'Insurancecomp ID',
            'insuranceclassName' => 'Insuranceclass Name',
            'appointmentOfferCode' => 'Appointment Offer Code',
            'appointmentStatus' => 'Appointment Status',
            'appointmentCharge' => 'Appointment Charge',
            'appointmentCreatedDate' => 'Appointment Created Date',
        ];
    }
	 public function GetAppointmentDetails($patientID=0,$pagesize="10",$page="0",$sortBy="normal",$hospitalID=0)
    {
		
       $Where = ' WHERE 1= 1  ';
		$join = '   inner join patient on appointment.patientID = patient.patientID
					left join patientdependent on patientdependent.patientdepenID = appointment.patientdepenID
					inner join doctor on doctor.doctorID = appointment.doctorID
					INNER Join degrees ON degrees.degreeID = doctor.degreeID
					INNER Join nationality ON nationality.nationalityID = doctor.nationalityID
					inner join hospital on doctor.hospitalID= hospital.hospitalID   
					INNER Join country ON country.countryID = hospital.countryID					';
		$para = ' SELECT appointment.*, patientFirstName, patientLastName, patientdepenFirstName,patientdepenLastName,doctorFirstName ,doctorLastName , doctor.nationalityID, doctorProfileImage , doctorRatingCount,doctorRatingAvg,hospitalName , hospitalNameArabic, degrees.degreeName, degrees.degreeNameArabic,countryFlagImage,nationalityName  ';
			// country, city, direction 	
	   
		
       if(!empty($patientID))
       {
            $Where .= ' AND appointment.patientID = "'.$patientID.'"';
       }
	   if(!empty($hospitalID))
       {
            $Where .= ' AND hospital.hospitalID = "'.$hospitalID.'"';
       }
	   
       

       $Re = array();

        $from = ' FROM appointment ';		
        //$ORDERBY = " hospital.hospitalName";
        $groupby = " GROUP BY appointment.appointmentID";
		if($sortBy=="normal")
			$order = " ORDER BY appointment.appointmentCreatedDate DESC";
		else
		{
			//Sorting needs by  speciality, month, year
			if ($sortBy=="date")
				$order = " ORDER BY appointment.appointmentCreatedDate ASC";
			if ($sortBy=="name")
				$order = " ORDER BY patientFirstName ASC,patientdepenFirstName ASC,";
			if ($sortBy=="status")
				$order = " ORDER BY appointmentStatus ASC,";
			if ($sortBy=="doctor")
				$order = " ORDER BY doctorFirstName ASC,";
			if ($sortBy=="hospital")
				$order = " ORDER BY hospitalName ASC,";
			
		}
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$join.$Where.$groupby.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		if(count($result)>0)
        {
			$doctorSpeciality = new Doctorspeciality;
			for($i=0;count($result)>$i; $i++) 
			{ 
				$Re[$i] = $result[$i];
				//- doctor speciality
				$Re[$i]["speciality"]= $doctorSpeciality->GetDoctorSpecialityList($result[$i]["doctorID"]);
				$Re[$i]["IsFavorite"]= "No";
				$favSQL =" SELECT * from patientfavoriite WHERE patientID='".$loginuserID."' AND  doctorID= ".$result[$i]["doctorID"];
				$connection = Yii::$app->getDb();
				$command = $connection->createCommand($favSQL);
				$favResult = $command->queryAll();
				if(count($favResult) >0 )
				{
					$Re[$i]["IsFavorite"]= "Yes";
				}
				
				
			}	
		
		}
        else
        {
           $Re = $result;
        }

           
       
       //return $result;   
	  return $Re;
    }
	 public function GetTotal($patientID=0,$sortBy="normal",$countType="",$hospitalID=0)
    {
		
       $Where = ' WHERE 1= 1  ';
		$join = '   inner join patient on appointment.patientID = patient.patientID
					left join patientdependent on patientdependent.patientdepenID = appointment.patientdepenID
					LEFT join doctor on doctor.doctorID = appointment.doctorID
					INNER Join degrees ON degrees.degreeID = doctor.degreeID
					INNER Join nationality ON nationality.nationalityID = doctor.nationalityID
					inner join hospital on doctor.hospitalID= hospital.hospitalID   
					INNER Join country ON country.countryID = hospital.countryID					';
		$para = ' SELECT count(hospital.hospitalID) as Total ';
			// country, city, direction 	
	   
		
       if(!empty($patientID))
       {
            $Where .= ' AND appointment.patientID = "'.$patientID.'"';
       }
	   if(!empty($countType))
       {
            $Where .= ' AND appointment.appointmentStatus = "'.$countType.'"';
       }
	   if(!empty($hospitalID))
       {
            $Where .= ' AND hospital.hospitalID = "'.$hospitalID.'"';
       }
	   
       

       $Re = array();

        $from = ' FROM appointment ';		
        //$ORDERBY = " hospital.hospitalName";
        $groupby = " GROUP BY hospital.hospitalID";
		if($sortBy=="normal")
			$order = " ORDER BY appointment.appointmentCreatedDate DESC";
		else
		{
			//Sorting needs by  speciality, month, year
			if ($sortBy=="date")
				$order = " ORDER BY appointment.appointmentCreatedDate ASC";
			if ($sortBy=="name")
				$order = " ORDER BY patientFirstName ASC,patientdepenFirstName ASC,";
			if ($sortBy=="status")
				$order = " ORDER BY appointmentStatus ASC,";
			if ($sortBy=="doctor")
				$order = " ORDER BY doctorFirstName ASC,";
			if ($sortBy=="hospital")
				$order = " ORDER BY hospitalName ASC,";
			
		}
        //$LIMIT = " LIMIT $pagesize";
        //$OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$join.$Where.$groupby.$order;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		if(count($result)>0)
        {			
			for($i=0;count($result)>$i; $i++) 
			{ 
				$Re[$i] = $result[$i];				
			}			
		}
        else
        {
           $Re = $result;
        }

           
       
       //return $result;   
	  return $Re;
    }
}
