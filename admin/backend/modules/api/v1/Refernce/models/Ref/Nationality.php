<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "nationality".
 *
 * @property int $nationalityID
 * @property string $nationalityName
 * @property string $nationalityNameArabic
 * @property string $nationalityRemarks
 * @property string $nationalityStatus
 * @property string $nationalityCreatedDate
 */
class Nationality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nationality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nationalityName', 'nationalityNameArabic'], 'required'],
            [['nationalityStatus'], 'string'],
            [['nationalityCreatedDate'], 'safe'],
            [['nationalityName', 'nationalityNameArabic'], 'string', 'max' => 100],
            [['nationalityRemarks'], 'string', 'max' => 200],
            [['nationalityName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nationalityID' => 'Nationality ID',
            'nationalityName' => 'Nationality Name',
            'nationalityNameArabic' => 'Nationality Name Arabic',
            'nationalityRemarks' => 'Nationality Remarks',
            'nationalityStatus' => 'Nationality Status',
            'nationalityCreatedDate' => 'Nationality Created Date',
        ];
    }
	
	public function GetNationalityList()
    {   
        $where = "nationality.nationalityStatus = 'Active'";
        /*if(!empty($stateID))
        {
            $where .= " AND city.stateID = '".$stateID."'";
        }
        if(!empty($searchkeyword))
        {
              $where .= " AND (city.cityName LIKE '%".$searchkeyword."%')";
        }*/
        
       $result = array();
       $query = new \yii\db\Query;
       $result = $query->select('nationality.nationalityID,
				nationality.nationalityName,
				nationality.nationalityNameArabic,
				nationality.nationalityRemarks')
       ->from('nationality')
       //->JOIN('INNER JOIN','state','city.stateID = state.stateID')
	   //->JOIN('INNER JOIN','country','state.countryID = country.countryID')
       ->where($where) 
	   //->limit($pagesize)
       ->GROUPBY('nationality.nationalityID')
	   ->ORDERBY('nationality.nationalityName ASC')
	   //->offset($pagesize*$page)
       ->all();
       //echo $query->createCommand()->sql; die;
       if(count($result)>0)
       {
		   $Re = $result;
       }
       else
       {
           $Re = $result;
       }
       return $Re; 
    }
}
