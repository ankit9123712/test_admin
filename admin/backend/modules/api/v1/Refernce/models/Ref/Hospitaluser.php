<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "hospitaluser".
 *
 * @property int $hospitaluserID
 * @property int $hospitalID
 * @property string $hospitaluserFirstName
 * @property string $hospitaluserLastName
 * @property string $hospitaluserEmail
 * @property string $hospitaluserMobile
 * @property int $countryID
 * @property int $cityID
 * @property string $hospitaluserImage
 * @property string $hospitaluserGender
 * @property string $hospitaluserPassword
 * @property string $hospitaluserType
 * @property string $hospitaluserStatus
 * @property string $hospitaluserCreatedDate
 * @property string $hospitaluserLastLoginDate
 */
class Hospitaluser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hospitaluser';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospitalID', 'hospitaluserFirstName', 'hospitaluserLastName', 'hospitaluserEmail', 'hospitaluserMobile', 'countryID', 'cityID', 'hospitaluserImage', 'hospitaluserGender', 'hospitaluserPassword', 'hospitaluserType', 'hospitaluserStatus', 'hospitaluserLastLoginDate'], 'required'],
            [['hospitalID', 'countryID', 'cityID'], 'integer'],
            [['hospitaluserGender', 'hospitaluserType', 'hospitaluserStatus'], 'string'],
            [['hospitaluserCreatedDate', 'hospitaluserLastLoginDate'], 'safe'],
            [['hospitaluserFirstName', 'hospitaluserLastName', 'hospitaluserEmail'], 'string', 'max' => 100],
            [['hospitaluserMobile'], 'string', 'max' => 20],
            [['hospitaluserImage'], 'string', 'max' => 255],
            [['hospitaluserPassword'], 'string', 'max' => 300],
            [['hospitalID', 'hospitaluserEmail', 'hospitaluserMobile'], 'unique', 'targetAttribute' => ['hospitalID', 'hospitaluserEmail', 'hospitaluserMobile']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hospitaluserID' => 'Hospitaluser ID',
            'hospitalID' => 'Hospital ID',
            'hospitaluserFirstName' => 'Hospitaluser First Name',
            'hospitaluserLastName' => 'Hospitaluser Last Name',
            'hospitaluserEmail' => 'Hospitaluser Email',
            'hospitaluserMobile' => 'Hospitaluser Mobile',
            'countryID' => 'Country ID',
            'cityID' => 'City ID',
            'hospitaluserImage' => 'Hospitaluser Image',
            'hospitaluserGender' => 'Hospitaluser Gender',
            'hospitaluserPassword' => 'Hospitaluser Password',
            'hospitaluserType' => 'Hospitaluser Type',
            'hospitaluserStatus' => 'Hospitaluser Status',
            'hospitaluserCreatedDate' => 'Hospitaluser Created Date',
            'hospitaluserLastLoginDate' => 'Hospitaluser Last Login Date',
        ];
    }
	public function CheckHospitalUsersForDuplication($hospitaluserEmail="",$hospitaluserID="",$hospitaluserMobile="")
    {
        if(!empty($hospitaluserEmail))
        {
            if(empty($hospitaluserID))
            {
                $Exist_hospitaluserEmail = Hospitaluser::find()->where("hospitaluserEmail = '".$hospitaluserEmail."'")->all();
            }
            else 
            {
                $Exist_hospitaluserEmail = Hospitaluser::find()->where("hospitaluserEmail = '".$hospitaluserEmail."' and hospitaluserID <> '".$hospitaluserID."'")->all();
            }
           
            if(count($Exist_hospitaluserEmail) == 0)
            {
                if(!empty($hospitaluserMobile))
                {
                    if(empty($hospitaluserID))
                    {
                        $Exist_hospitaluserMobile = Hospitaluser::find()->where("hospitaluserMobile = '".$hospitaluserMobile."'")->all();
                    }
                    else 
                    {
                        $Exist_hospitaluserMobile = Hospitaluser::find()->where("hospitaluserMobile = '".$hospitaluserMobile."' and hospitaluserID <> '".$hospitaluserID."'")->all();
                    }

                    if(count($Exist_hospitaluserMobile) == 0)
                    {
                       return 'true';
                    }
                    else
                    {
                        return 'hospitaluserMobile';
                    }
                }
                else
                {
                    return 'true';
                }
            }
            else
            {
                return 'hospitaluserEmail';
            }
        }
        else
        {
            if(!empty($hospitaluserMobile))
            {
                if(empty($hospitaluserID))
                {
                    $Exist_hospitaluserMobile = Hospitaluser::find()->where("hospitaluserMobile = '".$hospitaluserMobile."'")->all();
                }
                else 
                {
                    $Exist_hospitaluserMobile = Hospitaluser::find()->where("hospitaluserMobile = '".$hospitaluserMobile."' and hospitaluserID <> '".$hospitaluserID."'")->all();
                }

                if(count($Exist_hospitaluserMobile) == 0)
                {
                    return 'true';
                }
                else
                {
                    return 'hospitaluserMobile';
                }
            }
            else
            {
               return 'true';
            }
        }
    }
	public function GetHospitaluserDetails($hospitaluserID=0,$pagesize="10",$page="0",$hospitaluserType="",$hospitaluserStatus="",$hospitalID=0)
    {
       $Where = 'hospitaluserStatus = "Active"';
	   
       $Re = array();
	   
       if(!empty($hospitaluserID))
       {
           $Where .= ' AND hospitaluser.hospitaluserID = "'.$hospitaluserID.'"';
       }
	   if(!empty($hospitalID))
       {
           $Where .= ' AND hospitaluser.hospitalID = "'.$hospitalID.'"';
       }
	   if(!empty($hospitaluserID))
       {
           $Where .= ' AND hospitaluser.hospitaluserID = "'.$hospitaluserID.'"';
       }
	   if(!empty($hospitaluserType))
       {
           $Where .= ' AND hospitaluser.hospitaluserType = "'.$hospitaluserType.'"';
       }
	   if(!empty($hospitaluserStatus))
       {
           $Where .= ' AND hospitaluser.hospitaluserStatus = "'.$hospitaluserStatus.'"';
       }
       $result = array();
       $query = new \yii\db\Query;
       $result = $query->select('hospitaluser.*,
					country.countryName,
					country.countryNameArabic,
					city.cityName,
					city.cityNameArabic')
       ->from('hospitaluser')
       ->JOIN('LEFT JOIN','country','hospitaluser.countryID = country.countryID')
       ->JOIN('LEFT JOIN','city','hospitaluser.cityID = city.cityID')
       ->where($Where)
	   	->limit($pagesize)
		->offset($page*$pagesize)
		->orderby('hospitaluserFirstName ASC')->
       ->GROUPBY('hospitaluser.hospitaluserID')
       ->all();
       //echo $query->createCommand()->sql; die;
       if(count($result)>0)
       {
		  
           for($i=0;count($result)>$i; $i++) 
           { 
                
                $Re[$i] = $result[$i];
                $Re[$i]["appointments"]["total"]= "30";
				$Re[$i]["appointments"]["pending"]= "10";
				$Re[$i]["appointments"]["confirmed"]= "15";
				$Re[$i]["appointments"]["cancelled"]= "3";
				$Re[$i]["appointments"]["rejected"]= "2";
				$Re[$i]["userreview"]="200";
				$Re[$i]["invoices"]["month"]="August";
				$Re[$i]["invoices"]["count"]="150";
				$Re[$i]["users"]="10";
				
                
           }
       }
       else
       {
           $Re = $result;
       }
       return $Re;   
    }
}
