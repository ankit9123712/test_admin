<?php

namespace backend\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "institute".
 *
 * @property int $instituteID
 * @property string $instituteName
 * @property string $instituteContactName
 * @property string $instituteEmailExtension
 * @property string $institutePhone
 * @property string $instituteAddress
 * @property string $institutePincode
 * @property int $countryID
 * @property int $stateID
 * @property int $cityID
 * @property string $instituteStatus
 * @property string $instituteCreatedDate
 */
class Institute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'institute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['instituteName', 'instituteContactName', 'instituteEmailExtension', 'institutePhone', 'instituteAddress', 'institutePincode', 'countryID', 'stateID', 'cityID', 'instituteStatus'], 'required'],
            [['countryID', 'stateID', 'cityID'], 'integer'],
            [['instituteStatus'], 'string'],
            [['instituteCreatedDate'], 'safe'],
            [['instituteName', 'instituteContactName'], 'string', 'max' => 100],
            [['instituteEmailExtension'], 'string', 'max' => 50],
            [['institutePhone'], 'string', 'max' => 70],
            [['instituteAddress'], 'string', 'max' => 300],
            [['institutePincode'], 'string', 'max' => 10],
            [['instituteName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'instituteID' => 'Institute I D',
            'instituteName' => 'Institute Name',
            'instituteContactName' => 'Institute Contact Name',
            'instituteEmailExtension' => 'Institute Email Extension',
            'institutePhone' => 'Institute Phone',
            'instituteAddress' => 'Institute Address',
            'institutePincode' => 'Institute Pincode',
            'countryID' => 'Country I D',
            'stateID' => 'State I D',
            'cityID' => 'City I D',
            'instituteStatus' => 'Institute Status',
            'instituteCreatedDate' => 'Institute Created Date',
        ];
    }
	public function GetinstituteList($countryID=0,$stateID=0,$pagesize="10",$page="0")
    {
		$Where = ' WHERE instituteStatus = "Active"  ';
		if(!empty($countryID))
		{
			$Where .= ' AND countryID="'.$countryID.'"';
		}
		if(!empty($stateID))
		{
			$Where .= ' AND stateID="'.$stateID.'"';
		}
		$para = 'SELECT institute.*';
		$from = ' FROM institute';
        $ORDERBY = " institute.instituteName";
        $order = " ORDER BY $ORDERBY DESC";
        $LIMIT = " LIMIT $pagesize";
        $OFFSET = " OFFSET ".$page*$pagesize;                

        $sql = $para.$from.$Where.$order.$LIMIT.$OFFSET;
       // echo $sql; die;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
   
	   
       return $result; 
    }
}
