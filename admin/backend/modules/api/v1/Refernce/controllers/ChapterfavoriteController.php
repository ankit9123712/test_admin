<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Chapterfavorite;
class ChapterfavoriteController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		return $result;
	}
	

	public function actionChapterFavorite()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
							
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($coursechapterID ))
				{
					$Chapterfavorite = new Chapterfavorite;
					$Chapterfavorite_Res = Chapterfavorite::find()->where('userID = "'.$loginuserID.'" AND coursechapterID="'.$coursechapterID.'"')->one();
					//List of course
					if(!empty($Chapterfavorite_Res))
					{
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_FAVORITE");
					}
					else
					{
						$Chapterfavorite->userID = $loginuserID;
						$Chapterfavorite->coursechapterID = $coursechapterID;
						$Chapterfavorite->save();
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_FAVORITE");
						
					}
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionChapterUnFavorite()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
							
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($coursechapterID) )
				{
					$Chapterfavorite = new Chapterfavorite;
					$Chapterfavorite_Res = Chapterfavorite::find()->where('userID = "'.$loginuserID.'" AND coursechapterID="'.$coursechapterID.'"')->one();
					//List of course
					if(!empty($Chapterfavorite_Res))
					{
						//print_r($Chapterfavorite_Res);
						//die;
						//RestoFoods::deleteAll(['AND', 'restaurant_id = :restaurant_id', ['NOT IN', 'food_id', [1,2]]], [':restaurant_id' => $postData['resto_id']]);

						$Delete = Chapterfavorite::deleteAll('chapterfavoriteID = :chapterfavoriteID', [':chapterfavoriteID' => $Chapterfavorite_Res->chapterfavoriteID]);
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_UNFAVORITE");
					}
					else
					{
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_UNFAVORITE");
						
					}
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

}
