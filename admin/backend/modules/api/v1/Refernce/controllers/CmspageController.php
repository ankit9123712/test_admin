<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Cmspage;
class CmspageController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
        
        return $result;
    }

    public function actionGetCmspage()
    {
    	$result = array();
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
       	$postParams = \Yii::$app->request->post(); 
       	
        if ($Apilog->is_json($postParams["json"]))
        {
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           

        	$Cmspage = new Cmspage;
        	$Cmspage->scenario = Cmspage::SCENARIO_LIST;
        	$Cmspage->attributes = $jsonParams[0];
			
            extract($jsonParams[0]);
        	if(!empty($cmspageName))
	        {
	        	if(!empty($languageID))//($Cmspage->validate(array('languageID')))
		        {
		        	if(!empty($apiType))//if($Cmspage->validate(array('apiType')))
			        {
		        		if(!empty($apiVersion))//if($Cmspage->validate(array('apiVersion')))
				        {
					        
			        	   $Cmspage = new Cmspage;
		                   $res = $Cmspage->GetCmspage($cmspageName);  
		                   if(!empty($res))
		                   {
		                        $result[0]['data'] = $res;
		                        $result[0]['status'] = 'true';
		                        $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");//messages_constant::RECORDFOUND;
		                   }
		                   else
		                   {		                        
								$result[0]['status'] = 'false';
		                        $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");//messages_constant::NORECORD;
		                   }
					       
				        }
				        else
				        {
							$Required = 'apiVersion ';
			        		$result[0]['status'] = 'false';
			                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");//$Cmspage->getErrors('apiVersion')[0];
				        }
			        }
			        else
			        {
						$Required = 'apiType ';
		        		$result[0]['status'] = 'false';
		                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");//$Cmspage->getErrors('apiType')[0];
			        }
		        }
		        else
		        {
					$Required = 'languageID ';
	        		$result[0]['status'] = 'false';
	                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");//$Cmspage->getErrors('languageID')[0];
		        }
	        }
	        else
	        {
				$Required = 'cmspageName ';
        		$result[0]['status'] = 'false';
                $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");//$Cmspage->getErrors('cmspageName')[0];
	        }
            
        }
        $requestString = $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
        return $result; 
			
    }

}
