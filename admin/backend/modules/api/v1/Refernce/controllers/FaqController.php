<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Faq;
use backend\modules\api\v1\models\Settings;

class FaqController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
        $result[0]['message'] = "";
        return $result;
    }
	public function actionFaqList()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";				
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion)  )
				{
					$Faq = new Faq;					
					$result[0]['data'] = $Faq->GetFaqList();															
					$result[0]['status'] = 'true';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");//messages_constant::OTPRESEND_SUCCESS;									

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");//$Required.messages_constant::ISMISSING;
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}


}
