<?php

namespace backend\modules\api\v1\controllers;

use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Users;
use backend\modules\api\v1\models\Settings;
use backend\modules\api\v1\models\City;
use backend\modules\api\v1\models\Notification;
use backend\modules\api\v1\models\Template;
use backend\modules\api\v1\models\Homemessage;
use backend\modules\api\v1\models\Banner;
use backend\modules\api\v1\models\Courses;
use backend\modules\api\v1\models\Coursesubjects;
use backend\modules\api\v1\models\Exams;
use Yii;
class UsersController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		return $result;
	}

	public function actionUserRegistration()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{ 				
				extract($jsonParams[0]);  
				$userID=0;
				if(!empty($apiType) && !empty($languageID) && !empty($userFullName) && !empty($userEmail) && !empty($userMobile) && !empty($userDeviceType) && !empty($userDeviceID) )
				{
					/* User Email & Mobile NO Duplication check */
					

					$Usres = new Users;
					$DuplicationRes = $Usres->CheckUserDetailsForDuplication($userEmail,$userMobile);
					//echo $DuplicationRes; die;
					if($DuplicationRes == 'true')
					{
						$userPassword = \Yii::$app->security->generatePasswordHash($userPassword);
						$userReferCode = $Apilog->generateUniqueReferCode(6);
						$Errors = "";
						$userOTP = $Apilog->GenerateOTP(4);
						$Usres->userFullName = $userFullName;
						$Usres->userEmail = $userEmail;
						$Usres->userCountryCode = $userCountryCode;
						$Usres->userMobile = $userMobile;
						$Usres->userGender = $userGender;
						$Usres->streamID = $streamID;
						$Usres->coursestdID = $coursestdID;
						$Usres->languageID = $languageID;
						$Usres->userDeviceType = $userDeviceType;
						$Usres->userDeviceID = $userDeviceID;
						$Usres->userVerified = "No";																				
						$Usres->userPassword = $userPassword;	
						$Usres->userHobbies = $userHobbies;
						$Usres->instituteName = $instituteName;																				
						$Usres->instituteID = $instituteID;	
						$Usres->userInterest = $userInterest;	
						$Usres->userBirthDate = $userBirthDate;		
						$Usres->userReferKey = $userReferCode;								
						$Usres->userOTP = (string)$userOTP;
						$Usres->userProfilePicture = $userProfilePicture;
						$Usres->save(false);
						$Errors = $Usres->getErrors();	
						
						if(!empty($RefKey))
						{
							$UserRef_Res = Users::find()->where('userReferKey = "'.$userReferCode.'"')->one();
							if(!empty($UserRef_Res))
							{
								$Settings = new Settings;
								$Settings_Res = $Settings->GetUserSettings();
								
								//process rewards history
								$Rewardshistory = new Rewardshistory;
								$Rewardshistory->userID = $loginuserID;
								$Rewardshistory->rewardType = "Referral";
								$Rewardshistory->rewardSubType = "N/A";
								$Rewardshistory->rewardTitle = $userFullName;								
								$Rewardshistory->rewardPoints = $Settings_Res[0]["settingsReferralRewards"];
								$Rewardshistory->rewardDate = date('Y-m-d');
								$Rewardshistory->rewardNote = "Points earned for referring ".$userFullName;
								$Rewardshistory->save();
							}
						}
							
						if(empty($Errors))
						{ 
							$userID = $Usres->userID;
							$res = $Usres->GetUserDetails($userID);  
							if(!empty($res))
							{
								$result[0]['data'] = $res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","REGISTRATION_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}
						
					}
					else
					{
						/*----------------- Check same email & mobile with user registered but verification pending -------------*/

						$User_Res = Users::find()->where('userEmail = "'.$userEmail.'" AND userMobile = "'.$userMobile.'" AND userVerified = "No"')->one();
						if(!empty($User_Res))
						{
							$userPassword = \Yii::$app->security->generatePasswordHash($userPassword);
							$Users = new Users;
							$Errors = "";
							
							$userOTP = $Apilog->GenerateOTP(4);
							$User_Res->userFullName = $userFullName;
							$User_Res->userEmail = $userEmail;
							$User_Res->userCountryCode = $userCountryCode;
							$User_Res->userMobile = $userMobile;
							$User_Res->userGender = $userGender;
							$User_Res->streamID = $streamID;
							$User_Res->coursestdID = $coursestdID;
							$User_Res->languageID = $languageID;
							$User_Res->userDeviceType = $userDeviceType;
							$User_Res->userDeviceID = $userDeviceID;
							$User_Res->userVerified = "No";																				
							$User_Res->userPassword = $userPassword;	
							$User_Res->userHobbies = $userHobbies;
							$User_Res->instituteName = $instituteName;											
							$User_Res->userBirthDate = $userBirthDate;														
							$User_Res->instituteID = $instituteID;	
							$User_Res->userInterest = $userInterest;							
							$User_Res->userOTP = (string)$userOTP;
							$User_Res->userProfilePicture = $userProfilePicture;
							$User_Res->save(false);
							$Errors = $User_Res->getErrors();
							if(empty($Errors))
							{ 
								$userID = $User_Res->userID;
								$res = $Usres->GetUserDetails($userID);  
								if(!empty($res))
								{
									$result[0]['data'] = $res;
									$result[0]['status'] = 'true';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","REGISTRATION_SUCCESS");																						

								}
								else
								{
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
								}
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
							}

						}		
						else
						{
							if($DuplicationRes == 'userEmail')
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","EMAIL_Exist");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","MOBILE_Exist");
							}	
						}	                				
						
					}	
					if($userID>0)	
					{
						/*------------- Send OTP Code Start --------------*/
								
							$Extra_Para_Array =  array(
										'UserNotificationType' => "Admin",
										'userOTP' => $userOTP
										);
						//$Template =  new Template;												
						//$Template->SendNotification('000001','1',$userID,'User','0','Admin',$Extra_Para_Array);
						
						/*------------ Send OTP Code End --------------*/
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}																	

			}
		}
		else
		{
				$Required = 'JSON ';
				$result[0]['status'] = 'false';
				$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
		}
		//die;
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

	public function actionOtpVerification()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$stateID = "0";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($userOTP) && !empty($userDeviceID) )
				{			

					$Users = new Users;
					$Users_Res = Users::find()->select('userID,userVerified')->where('userID = "'.$loginuserID.'" AND userOTP = "'.$userOTP.'"')->one();
					if(!empty($Users_Res))
					{           		
						
						


						$Users_Res->userID = $loginuserID;
						$Users_Res->userDeviceID = $userDeviceID;
						if($Users_Res->userVerified = "No")
						{
							$Users_Res->userVerified = "Yes";
						}
						$Users_Res->userOTP = "";
						$Users_Res->save(false);
						$result[0]['data'] = $Users->GetUserDetails($loginuserID);
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPVERIFICATION_SUCCESS");
					}
					else
					{
						/*------------------- Check Mater Otp for verificaion code start --------------------------*/
						
						$Settings = new Settings;
						$Settings_Res = $Settings->GetUserSettings();
						
						/*if($Settings_Res[0]['settingsMasterOtp'] == $userOTP)
						{	
							$result[0]['data'] = $Users->GetUserDetails($loginuserID);
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPVERIFICATION_SUCCESS");

							$Users_Res = Users::find()->select('userID,userVerified')->where('userID = "'.$loginuserID.'"')->one();
							$Users_Res->userID = $loginuserID;
							$Users_Res->userDeviceID = $userDeviceID;
							if($Users_Res->userVerified = "No")
							{
								$Users_Res->userVerified = "Yes";
							}
							$Users_Res->userOTP = "";
							$Users_Res->save(false);
							
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPVERIFICATION_ERROR");
						}*/
						
						/*------------------- Check Mater Otp for verificaion code end --------------------------*/
						
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPVERIFICATION_ERROR");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
								

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

	public function actionOtpResend()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$stateID = "0";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($userMobile) )
				{

					$Users = new Users;
					$Users_Res = Users::find()->select('userID')->where('userID = "'.$loginuserID.'" AND userMobile = "'.$userMobile.'"')->one();
					if(!empty($Users_Res))
					{   
						
						$userOTP = $Apilog->GenerateOTP(4);
						$Users_Res->userID = $loginuserID;
						$Users_Res->userOTP = $userOTP;
						$Users_Res->save(false);

						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPRESEND_SUCCESS");
						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}

								
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionUserLoginOtp()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$stateID = "0";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($userMobile) && !empty($userDeviceID)  )
				{

					$Users = new Users;
					$Users_Res = Users::find()->select('userID')->where('userMobile = "'.$userMobile.'"')->one();
					if(!empty($Users_Res))
					{   
						$userID = $Users_Res->userID;
						$userOTP = $Apilog->GenerateOTP(4);
						$Users_Res->userID = $userID;
						$Users_Res->userOTP = $userOTP;
						$Users_Res->save(false);
						$res = array();
						$res[0]["userID"]= $userID;
						$res[0]["userOTP"]= $userOTP;
						$result[0]['status'] = 'true';
						//$result[0]['data']= $res;
						$result[0]['data'] = $Users->GetUserDetails($userID);
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","OTPRESEND_SUCCESS");
						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}

								
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionUserDetails()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$stateID = "0";
				extract($jsonParams[0]);
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) )
				{
					$Users = new Users;
					
					$res = $Users->GetUserDetails($loginuserID);  

					$result[0]['data']=$res;
					//$result[0]['data']["OTP"]=$res;
					$result[0]['status'] = 'true';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
					

				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionUserLoginPassword()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$stateID = "0";
				extract($jsonParams[0]);
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($userPassword) && !empty($userMobile) && !empty($userDeviceID) )
				{
					$Users = new Users;
					$Users_Res = Users::find()->select('userID,userMobile,userPassword')->where('userMobile = "'.$userMobile.'" ')->one();
					$Users_Res2 = Users::find()->select('userID,userMobile,userPassword')->where('userEmail = "'.$userMobile.'"')->one();
					//print_r($Users_Res);
					//die;
					if(!empty($Users_Res) )
					{   
						if(1 == \Yii::$app->security->validatePassword($userPassword,$Users_Res->userPassword))
						{
							$res = $Users->GetUserDetails($Users_Res->userID);  

							$result[0]['data']=$res;
							//$result[0]['data']["OTP"]=$res;
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","INVALIDUSERPASS");
						}
							
					}
					else
					{
						if(!empty($Users_Res2) )
						{ 
							if(1 == \Yii::$app->security->validatePassword($userPassword,$Users_Res2->userPassword))
							{
								$res = $Users->GetUserDetails($Users_Res2->userID);  

								$result[0]['data']=$res;
								//$result[0]['data']["OTP"]=$res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","INVALIDUSERPASS");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
						}
					}

				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

	/*-------------- User Home Sreen Data Get Code Start -----------------*/

	public function actionUserHome()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";				
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) )
				{
					$Banner = new Banner;
					$Homemessage = new Homemessage;
					$Courses = new Courses;
					$Coursesubjects = new Coursesubjects;
					$Exams = new Exams;
					
					//banners 
					$resBanner = $Banner->GetBannerList();
					//List of course //($courseID=0,$courseFree="",$page="0",$pagesize="10",$loginuserID=0)
					$resCourses = $Courses->GetCoursesList(0,"Paid",$page,$pagesize,$loginuserID);
					$resFree = $Courses->GetCoursesList(0,"Free",$page,$pagesize,$loginuserID);
					//List of subjects
					$resSubCourses = $Coursesubjects->GetCoursesubjectsList("",$loginuserID);
					//message center
					$res = $Homemessage->GetHomemessageList($loginuserID);
					$resMyCourses = $Courses->GetCoursesList(0,"Paid",$page,$pagesize,$loginuserID,"Yes");
					$myCoursesubjects= $Coursesubjects->GetCoursesubjectsList(0,$loginuserID,"Yes") ;
					
					// overall rank, 
					$totalStudent =0;
					$avgRank = 0;
					$sql = "SELECT planID from subscriptionplan 
inner join users on users.streamID = subscriptionplan.streamID AND users.coursestdID = subscriptionplan.coursestdID
WHERE users.userID=".$loginuserID;				   
					$connection = Yii::$app->getDb();
					$command = $connection->createCommand($sql);
					$resplanID = $command->queryAll();
					
					if(count($resplanID) >0 )
					{
						
						$plans="";
						for($i=0;count($resplanID)>$i; $i++) 
					   {
						   if($plans=="")
							   $plans = $resplanID[$i]["planID"];
						   else
							   $plans .= ",".$resplanID[$i]["planID"];								   
					   }
						
						$sql = "SELECT distinct userID from usersubscriptions WHERE planID in (".$plans.")";				   
						$connection = Yii::$app->getDb();
						$command = $connection->createCommand($sql);
						$resTotalStudent = $command->queryAll();
						$totalStudent=count($resTotalStudent);						
					}
					$sql = "select avg(userexamRank) as avgRank from userexams where userID='".$loginuserID."' AND userexamType='Scheduled' group by userexamType";				   
					$connection = Yii::$app->getDb();
					$command = $connection->createCommand($sql);
					$resRank = $command->queryAll();
					if(count($resRank) >0 )
					{
						$avgRank = $resRank[0]["avgRank"];
					}
					
					// exams attempted
					$resTotalExams = $Exams->GetMyTest(0,1000,$loginuserID);
					//echo "sdsd";die;
					$sql = "select count(userexamID) as cnt from userexams where userID='".$loginuserID."' AND userexamType='Scheduled' ";				   
					$connection = Yii::$app->getDb();
					$command = $connection->createCommand($sql);
					$resCnt = $command->queryAll();
					$examAttempted = 0;
					if(count($resCnt) >0 )
					{
						$examAttempted = $resCnt[0]["cnt"];
					}
					
					
					$result[0]['banners'] = $resBanner;	
					
					$result[0]['freecouse'] = $resFree;	
					$result[0]['myCourses'] = $resMyCourses;	
					$result[0]['mySubject'] = $myCoursesubjects;	
					$result[0]['overallrank'] = $avgRank;	
					$result[0]['overallrankoutof'] = $totalStudent;	
					$result[0]['examsAttempted'] = $examAttempted ;
					$result[0]['examsAttemptedoutoff'] = count($resTotalExams);
					$result[0]['courses'] = $resCourses;
					$result[0]['subjects'] = $resSubCourses;					
					$result[0]['homeMessage'] = $res;									
					$result[0]['status'] = 'true';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	/*-------------- User Home Sreen Data Get Code End -----------------*/

	public function actionUserUpdateProfile()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  				
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($userFullName) && !empty($userEmail) && !empty($userMobile) && !empty($userDeviceType) && !empty($userDeviceID) )
				{

					$User_Res = Users::find()->where('userID = "'.$loginuserID.'"')->one();
					if(!empty($User_Res))
					{
						$Usres = new Users;
						$DuplicationRes = $Usres->CheckUserDetailsForDuplication($userEmail,$userMobile,$loginuserID);
						if($DuplicationRes == 'true')
						{

							$Errors = "";
							//$userPassword = \Yii::$app->security->generatePasswordHash($userPassword);
							$User_Res->userFullName = $userFullName;
							$User_Res->userEmail = $userEmail;
							$User_Res->userCountryCode = $userCountryCode;
							$User_Res->userMobile = $userMobile;
							$User_Res->userGender = $userGender;
							$User_Res->streamID = $streamID;
							$User_Res->coursestdID = $coursestdID;
							$User_Res->languageID = $languageID;
							$User_Res->userDeviceType = $userDeviceType;
							$User_Res->userDeviceID = $userDeviceID;
							//$User_Res->userVerified = "No";																				
							//$User_Res->userPassword = $userPassword;	
							$User_Res->userHobbies = $userHobbies;
							$User_Res->userBirthDate = $userBirthDate;														
							$User_Res->instituteName = $instituteName;																				
							$User_Res->instituteID = $instituteID;	
							$User_Res->userInterest = $userInterest;							
							$User_Res->userProfilePicture = $userProfilePicture;
							$User_Res->userAddress	 = $userAddress	;
							$User_Res->userFather = $userFather	;	
							$User_Res->userFatherMobile = $userFatherMobile;
							$User_Res->userFatherEmail = $userFatherEmail;																				
							$User_Res->userMother = $userMother;	
							$User_Res->userMotherMobile = $userMotherMobile;							
							$User_Res->userMotherEmail	 = $userMotherEmail	;
							
							$User_Res->save(false);
							$Errors = $User_Res->getErrors();
							if(empty($Errors))
							{ 
								$userID = $User_Res->userID;
								$res = $Usres->GetUserDetails($userID);  
								if(!empty($res))
								{
									$result[0]['data'] = $res;
									$result[0]['status'] = 'true';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PROFILEUPDATE_SUCCESS");
								}
								else
								{
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
								}
							}
							else
							{
								//print_r($Errors);
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
							}

						}		
						else
						{
							
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","EMAIL_Exist");
							
						}
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
																	

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	public function actionUserUpdateProfilePicture()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($userProfilePicture)  )
				{
					$User_Res = Users::find()->where('userID = "'.$loginuserID.'"')->one();
					if(!empty($User_Res))
					{
						$Errors = "";
						$User_Res->userProfilePicture = $userProfilePicture;
						$User_Res->save(false);
						$Errors = $User_Res->getErrors();
						if(empty($Errors))
						{ 
							$userID = $User_Res->userID;
							$Usres = new Users;
							$res = $Usres->GetUserDetails($userID);  
							if(!empty($res))
							{
								$result[0]['data'] = $res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PROFILEPICTUREUPDATE_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] =$Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}

						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
								
							

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}


	public function actionFileUpload()
	{
		$ApiLog = new ApiLog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$postParams = \Yii::$app->request->post();	
		
		if ($ApiLog->is_json($postParams["json"]))
		{			
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);			
			if(count($jsonParams[0])>0)
			{
				$Key = "";
				$FilePath = "";
				$FileField = "";
				$subpath = "";
				
				extract($jsonParams[0]);
				
				$count = 1;
				if($count > 0)
				{
					
					$path=$_REQUEST['FilePath'];
					
					$filename=$_FILES['FileField']['name'];
					$system=explode(".",$filename);
/*print_r($system);
echo $system[count($system)-1];
die;*/
					$flg=0;							
					if(preg_match("/jpg|jpeg|JPG|JPEG|mp4|mp3/",$system[count($system)-1]))
						$flg=1;	

					if($flg==0)
					{
						if (preg_match("/gif|GIF/",$system[count($system)-1]))
						$flg=1;
					}	
					if($flg==0)
					{
						if (preg_match("/png|PNG/",$system[count($system)-1]))	
						$flg=1;
					}	

					if($flg==1)
					{	
						if($subpath=="")
						$destination = "uploads/".$path."/";
						else
						$destination = "uploads/".$path."/".$subpath."/";	
						if (!is_dir($destination)) 
						{
							mkdir($destination,0777,true);
						}	

						$file_path = $destination.$filename;

						if(move_uploaded_file($_FILES['FileField']['tmp_name'],$file_path))
						{
							$result[0]["status"]= "true";
							$result[0]["fileName"]=$filename;
							$result[0]["message"]= "File uploaded successfully.";
							
						}
						else
						{
							$result[0]["status"]= "false";
							$result[0]["message"]= "problem occurs while uploading ";
						}
					}
					else
					{
						$error = "Problem with file upload";
						$result[0]["fileName"]=$filename." ".$flg;
						$result[0]["status"]= "false";		
						$result[0]["message"]= $error;		
					}	

				}
				else
				{
					$result[0]["status"]= "false";
					$result[0]["message"]= "dont know what i do got here ";
				}	
			}
		}
		return $result;	
		
	}

	public function actionUserUpdateDeviceToken()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($userDeviceID) && !empty($userDeviceType)  )
				{

					$User_Res = Users::find()->where('userID = "'.$loginuserID.'"')->one();
					if(!empty($User_Res))
					{
						$Errors = "";
						$User_Res->userDeviceID = $userDeviceID;
						$User_Res->userDeviceType = $userDeviceType;
						$User_Res->save(false);
						$Errors = $User_Res->getErrors();
						if(empty($Errors))
						{ 
							$userID = $User_Res->userID;
							$Usres = new Users;
							$res = $Usres->GetUserDetails($userID);  
							if(!empty($res))
							{
								$Settings = new Settings;
								$usersettings = $Settings->GetUserSettings();  
								$result[0]['data'] = $res;
								$result[0]['usersettings'] = $usersettings;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","DEVICETOKENUPDATE_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}
						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = INVALIDDATA::ISMISSING;
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
							

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

	public function actionUserUpdateSettings()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				extract($jsonParams[0]);    
				if(!empty($apiType))
				{
					if(!empty($languageID))
					{
						if(!empty($apiVersion))
						{
							if(!empty($userNewsNotification))
							{
								
								if(!empty($userCourseNotification))
								{

									if(!empty($loginuserID))
									{

										$User_Res = Users::find()->where('userID = "'.$loginuserID.'"')->one();
										if(!empty($User_Res))
										{
											$Errors = "";
											$User_Res->languageID = $languageID;
											$User_Res->userNewsNotification = $userNewsNotification;
											$User_Res->userCourseNotification = $userCourseNotification;
											$User_Res->userOfferNotification	 = $userOfferNotification	;
											$User_Res->userExamNotification = $userExamNotification;
											$User_Res->save(false);
											$Errors = $User_Res->getErrors();
											if(empty($Errors))
											{ 
												$userID = $User_Res->userID;
												$Usres = new Users;
												$res = $Usres->GetUserDetails($userID);  
												if(!empty($res))
												{
													$result[0]['data'] = $res;
													$result[0]['status'] = 'true';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","USERUPDATESETTINGS_SUCCESS");
												}
												else
												{
													$result[0]['status'] = 'false';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
												}
											}
											else
											{
												$result[0]['status'] = 'false';
												$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
											}

											
										}
										else
										{
											$result[0]['status'] = 'false';
											$result[0]['message'] = INVALIDDATA::ISMISSING;
										}
									}
									else
									{
										$Required = 'loginuserID';
										$result[0]['status'] = 'false';
										$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
									}
								}
								else
								{
									$Required = 'userPushNotification';
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
								}
								
							}
							else
							{
								$Required = 'userEmailNotification';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
							}
							
						}
						else
						{
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
						}
					}
					else
					{
						$Required = 'languageID';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
					}
				}
				else
				{
					$Required = 'apiType';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}


	public function actionCheckUserDuplication()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$loginuserID = "";
				$userEmail = "";
				$userMobile = "";
				extract($jsonParams[0]);    
				if(!empty($apiType))
				{
					if(!empty($languageID))
					{
						if(!empty($apiVersion))
						{
							if((!empty($userEmail)) || (empty($userMobile)))
							{
								$Users = new Users;
								$DuplicationRes = $Users->CheckUserDetailsForDuplication($userEmail,$userMobile,$loginuserID);

								if($DuplicationRes == 'true')
								{
									$result[0]['status'] = 'true';
									$result[0]['message'] = '';
								}
								else if($DuplicationRes == 'userEmail')	
								{
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","EMAIL_Exist");
								}
								else
								{
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","MOBILE_Exist");
								}
							}
							else
							{
								$Required = 'userEmail OR userMobile';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
							}
							
						}
						else
						{
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
						}
					}
					else
					{
						$Required = 'languageID';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
					}
				}
				else
				{
					$Required = 'apiType';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

	public function actionUserForgotPassword()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  			  
			   extract($jsonParams[0]);    
			   if($userEmail!="")
			   {
				   $User_Res = Users::find()->select('userID,userMobile ')->where('userEmail = "'.$userEmail.'"')->one();
			   }
			   else
			   {
				   $User_Res = Users::find()->select('userID,userMobile ')->where('userMobile = "'.$userMobile.'"')->one();
			   }				   
			   if(!empty($User_Res))
				{ 
					$Users = new Users();
					$userOTP = $Apilog->GenerateOTP(4);
					
					$Errors = "";
					//$userPassword = \Yii::$app->security->generatePasswordHash($userPassword);		
					$User_Res->userOTP = $userOTP;
					$User_Res->save(false);
					$Errors = $User_Res->getErrors();
					if(empty($Errors))
					{ 
						$userID = $User_Res->userID;
										
						/*------------- Send OTP Code Start --------------*/

						$Extra_Para_Array =  array(
								'UserNotificationType' => "Admin",
								'userOTP' => $userOTP
								);
						$Template =  new Template;												
						$Template->SendNotification('0000021','1',$userID,'User','0','Admin',$Extra_Para_Array);

						/*------------- Send OTP Code End --------------*/
						
				
						$result[0]['data'][0]['userID'] = (string)$userID;
						$result[0]['data'][0]['userMobile'] = (string)$userMobile;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","SENT_OTP");
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
					}
						 
				}
				else
				{
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","USERNOTREGISTRED");
				}	
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionResetPassword()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
						
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($userNewPassword) && !empty($loginuserID))
				{
					$User_Res = Users::find()->select('userID,userPassword')->where('userID = "'.$loginuserID.'"')->one();
					if(!empty($User_Res))
					{ 
						 
						$Errors = "";
						$userPassword = \Yii::$app->security->generatePasswordHash($userNewPassword);		
						$User_Res->userPassword = $userPassword;
						$User_Res->save(false);
						$Errors = $User_Res->getErrors();
						if(empty($Errors))
						{ 
							$Users = new Users;
							$result[0]['data'] = $Users->GetUserDetails($loginuserID);
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PASSWORDCHANGE_SUCCESS");
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
						}
						
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionChangePassword()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
						
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($userCurrentPassword) && !empty($userNewPassword) && !empty($loginuserID))
				{
					$User_Res = Users::find()->select('userID,userPassword')->where('userID = "'.$loginuserID.'"')->one();
					if(!empty($User_Res))
					{ 
						 if(1 == \Yii::$app->security->validatePassword($userCurrentPassword,$User_Res->userPassword))
						 {
							$Errors = "";
							$userPassword = \Yii::$app->security->generatePasswordHash($userNewPassword);		
							$User_Res->userPassword = $userPassword;
							$User_Res->save(false);
							$Errors = $User_Res->getErrors();
							if(empty($Errors))
							{ 
								$Users = new Users;
								$result[0]['data'] = $Users->GetUserDetails($loginuserID);
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","PASSWORDCHANGE_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");
							}
						 }
						 else
						 {
							 $result[0]['status'] = 'false';
							 $result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CURRENTPASSWORD_ERROR");
						 }
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
}
