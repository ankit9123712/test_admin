<?php

namespace backend\modules\api\v1\controllers;
use Yii;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Notification;
use backend\modules\api\v1\models\Settings;
use common\models\Template;
class CronJobsController extends \yii\web\Controller
{
     public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
        $result[0]['message'] = messages_constant::JSON_ERROR;
        return $result;
    }
	public function actionNotificationCronJob()
	{
	
	$where = " WHERE 1 =1 AND notificationStatus='No' ";
		$join = '    ';				
		$para = ' Select *   ';				
		$groupby = "  "			;
		$from = ' FROM notification';	      
		$sql = $para.$from.$join.$where.$groupby;
			//echo $sql; die;
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		$processData = $command->queryAll();
		$template = new Template();	
		for ($x = 0; $x <= count($processData)-1; $x++)	
		{
			extract($processData[$x]);
			$template->push($userDeviceID,$notificationType,$notificationMessageText,$notificationReferenceKey,$userID,$userDeviceType,"",$notificationTitle,true,"",$notificationID);
		
		}
	
}
public function actionClearLogsCronJob()
{
	//Select * FROM apilog WHERE apiCallDate <= DATE_ADD('2019-08-30', INTERVAL -15 DAY)
		$settings = new Settings();
		$settings_arr = Settings::find()->select('settingsLogDeleteDays')->where(['settingsID' => '1'])->asArray()->all();	
		$sql= "DELETE FROM apilog WHERE apiCallDate <= DATE_ADD('".date('Y-m-d')."', INTERVAL -".$settings_arr[0]["settingsLogDeleteDays"]." DAY)";
		//echo $sql;die;
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql)->execute();
		
		//$processData = $command->queryAll();
	
}

}
