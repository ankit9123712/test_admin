<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Users;
use backend\modules\api\v1\models\Settings;
use backend\modules\api\v1\models\City;
use backend\modules\api\v1\models\Notification;
use backend\modules\api\v1\models\Template;
use backend\modules\api\v1\models\Homemessage;
use backend\modules\api\v1\models\Banner;
use backend\modules\api\v1\models\Courses;
use backend\modules\api\v1\models\Coursesubjects;
use backend\modules\api\v1\models\Uservisitedcourses;
use backend\modules\api\v1\models\Coursechapters;
class CoursesController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		return $result;
	}
	

	public function actionGetCourses()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				$courseFree="";
				$courseID=0;		
				$mycourse="";	
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) )
				{
					$Courses = new Courses;
					$Coursechapters = new Coursechapters;
					//List of course
					$resCourses = $Courses->GetCoursesList($courseID,$courseFree,$page,$pagesize,$loginuserID,$mycourse);
					//$resVisit = $Uservisitedcourses->GetVisitedCoursesList($loginuserID);
					$resGetCoursechaptersList = $Coursechapters->GetCoursechaptersList(0,$loginuserID,"","True");
					if(!empty($resCourses))
					{
						$result[0]['data'] = $resCourses;
						$result[0]['visiteddata'] = $resGetCoursechaptersList;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	/*-------------- User Home Sreen Data Get Code End -----------------*/

}
