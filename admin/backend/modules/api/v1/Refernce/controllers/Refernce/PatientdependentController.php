<?php

namespace backend\modules\api\v1\controllers;
use Yii;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Patient;
use backend\modules\api\v1\models\Patientdependent;
class PatientdependentController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
    {
		return $this->render('index');
		\Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		return $result;
    }
	
	public function actionAddPatientDependent()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($patientID) && !empty($patientdepenFirstName) && !empty($patientdepenLastName) && !empty($patientdepenDOB) && !empty($patientdepenGender) && !empty($countryID) && !empty($nationalityID) )
               {						  
					/*---------------- Check Duplication -----------------*/
					$Patientdependent = new Patientdependent;
					$Patientdependent->patientID = $patientID;
					$Patientdependent->patientdepenFirstName = $patientdepenFirstName;
					$Patientdependent->patientdepenLastName = $patientdepenLastName;
					$Patientdependent->patientdepenDOB = $patientdepenDOB;
					$Patientdependent->patientdepenGender = $patientdepenGender;
					$Patientdependent->countryID = $countryID;
					$Patientdependent->nationalityID = $nationalityID;
					$Patientdependent->save(false);
					$Errors = $Patientdependent->getErrors();
					if(empty($Errors))
					{
					   $patientdepenID = $Patientdependent->patientdepenID;
					   $res = $Patientdependent->GetPatientdependentDetails(0);  
					   if(!empty($res))
					   {  														   
							$result[0]['data'] = $res;
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"REGISTRATION_SUCCESS");
					   }
					   else
					   {
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
						   
					   }
						
					}				   
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
			   
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionEditPatientDependent()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($patientID) && !empty($patientdepenFirstName) && !empty($patientdepenLastName) && !empty($patientdepenDOB) && !empty($patientdepenGender) && !empty($countryID) && !empty($nationalityID) && !empty($patientdepenID) )
               {						  
					/*---------------- Check Duplication -----------------*/
					$Patientdependent = Patientdependent::find()->where('patientdepenID = "'.$patientdepenID.'"')->one();					
					$Patientdependent->patientID = $patientID;
					$Patientdependent->patientdepenFirstName = $patientdepenFirstName;
					$Patientdependent->patientdepenLastName = $patientdepenLastName;
					$Patientdependent->patientdepenDOB = $patientdepenDOB;
					$Patientdependent->patientdepenGender = $patientdepenGender;
					$Patientdependent->countryID = $countryID;
					$Patientdependent->nationalityID = $nationalityID;
					$Patientdependent->save(false);
					$Errors = $Patientdependent->getErrors();
					if(empty($Errors))
					{
					   $patientdepenID = $Patientdependent->patientdepenID;
					   $res = $Patientdependent->GetPatientdependentDetails(0);  
					   if(!empty($res))
					   {  														   
							$result[0]['data'] = $res;
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"PROFILEUPDATE_SUCCESS");
					   }
					   else
					   {
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
						   
					   }
						
					}				   
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
			   
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionPatientDependentList()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($patientID)  )
               {						  
					
					   $Patientdependent = new Patientdependent;
					   $res = $Patientdependent->GetPatientdependentDetails($patientID);  
					   if(!empty($res))
					   {  														   
							$result[0]['data'] = $res;
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"PROFILEUPDATE_SUCCESS");
					   }
					   else
					   {
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
						   
					   }
						
									   
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
			   
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	

}
