<?php

namespace backend\modules\api\v1\controllers;use Yii;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Hospital;
use backend\modules\api\v1\models\Doctor;
use backend\modules\api\v1\models\Doctorspeciality;
use backend\modules\api\v1\models\Doctortime;

class DoctorController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
	public function actionIndex()
    {
		return $this->render('index');
		\Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		return $result;
    }
	public function actionDoctorsSearchList()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{ 					   
				$languageID = "1";
				$page = "0";
				$pagesize = "10";	
				$countryID=0; 
				$cityID=0;
				$specialityID=0;
				$rating="";
				$subspecialityIDs="";
				$degreeIDs="";
				$hospitalIDs="";
				$insurancecompIDs="" ;
				$doctorGender="";
				$type ="";
			   extract($jsonParams[0]);    
			   if(!empty($languageID))
               {
			   
				   if(!empty($apiType))
				   {
					   if(!empty($apiVersion))
					   {
						   if($loginuserID >=0 )
						   {							   
								
								$Doctor = new Doctor;
								$doctorRes = $Doctor->GetDoctorsList($loginuserID,0, 0, 0, "", "", "", $hospitalIDs,"" ,"", $pagesize, $page, "",$type);
								//($loginuserID=0,$countryID=0, $cityID=0, $specialityID=0, $rating="", $subspecialityIDs="", $degreeIDs="", $hospitalIDs="",$insurancecompIDs="" ,$doctorGender="", $pagesize="0", $page="10", $sortBy="normal",$type="",$doctorID=0,$miniList="false")								
								
								$result[0]['data'] = $doctorRes;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");			
												
									   
						   }
						   else
						   {
								$Required = 'patientID';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
						   }
							
					   }
					   else
					   {
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
					   }
				   }
				   else
				   {
						$Required = 'apiType';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionDoctorDeleteUser()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($deleteDoctorID) && !empty($hospitaluserID) )
               {						  
					
				   $Doctor_Res = Doctor::find()->where('doctorID = "'.$deleteDoctorID.'"')->one();
				   if(!empty($Doctor_Res))
				   {
				
						$Doctor_Res->doctorStatus = "Inactive";
						$Doctor_Res->save(false);
						$Errors = $Doctor_Res->getErrors();
						if(empty($Errors))
						{
						   $doctorID = $Doctor_Res->doctorID;
						  	
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"DOCTOR_DELETE");
						   
							
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
						}
					
				   }
				   else
				   {
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
				   }		   
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionDoctorUserProfilePictureUpdate()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($doctorProfileImage) && !empty($hospitaluserID)  && !empty($doctorID))
               {						  
					
				   $Doctor_Res = Doctor::find()->where('doctorID	 = "'.$doctorID	.'"')->one();
				   if(!empty($Doctor_Res))
				   {
				
						$Doctor_Res->doctorProfileImage = $doctorProfileImage;
						$Doctor_Res->save(false);
						$Errors = $Doctor_Res->getErrors();
						if(empty($Errors))
						{
						    $doctorID	 = $Doctor_Res->doctorID	;
						  	$Doctor = new Doctor;
						   $result[0]['data'] = $Doctor->GetDoctorsList(0,0, 0, 0, "", "", "", "","" ,"", 10, 0, "","", $doctorID);
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
						   
							
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
						}
					
				   }
				   else
				   {
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
				   }		   
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionDoctorAdd()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{ 					   

				$page = "0";
				$pagesize = "10";	
				$type ="";
				//$hospitalID ="";
			   extract($jsonParams[0]);    
			  // print_r($jsonParams[0]);    die;
			   if(!empty($languageID) && !empty($hospitalID) && !empty($doctorFirstName) && !empty($doctorLastName) && !empty($degreeID) && !empty($nationalityID) && !empty($doctorGender) && !empty($doctorProfileImage) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID))
               {
			   
						   
								$Doctor = new Doctor;
								$Doctor->hospitalID = $hospitalID;
								$Doctor->doctorFirstName = $doctorFirstName;
								$Doctor->doctorLastName = $doctorLastName;
								$Doctor->doctorFirstNameArabic = $doctorFirstNameArabic;
								$Doctor->doctorLastNameArabic = $doctorLastNameArabic;
								$Doctor->degreeID = $degreeID;
								$Doctor->nationalityID = $nationalityID;
								$Doctor->doctorGender = $doctorGender;
								$Doctor->doctorProfileImage = $doctorProfileImage;
								$Doctor->doctorStatus = "Active";
								$Doctor->specialityIDs = "0";
								$Doctor->save();
		               			$Errors_Doctor = $Doctor->getErrors();
								$doctorspecialityIDs = array();
								$cnt=0;
								if(empty($Errors_Doctor))
								{
									$doctorID = $Doctor->doctorID;
									for($i=0;$i<count($doctorspeciality);$i++) 
									{ 
										$Doctorspeciality = new Doctorspeciality;
										$Doctorspeciality->doctorID = $doctorID;										
										$Doctorspeciality->specialityID	 = $doctorspeciality[$i]['specialityID'];
										$Doctorspeciality->subspecialityID = $doctorspeciality[$i]['subspecialityID'];										
										$Doctorspeciality->save();
										$doctorspecialityID = $Doctorspeciality->doctorspecialityID;
										$doctorspecialityIDs[$cnt]= $doctorspeciality[$i]['specialityID'];
										$cnt++;
										//print_r($Orderdetails);die;
										$Errors_Doctorspeciality = $Doctorspeciality->getErrors();
										if(!empty($Errors_Doctorspeciality))
										{
											print_r($Errors_Doctorspeciality);die;
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");		
										}
									}
									$cnt=0;
									/*Update doctors for specialityIDs*/
									$Doctor = Doctor::find()->where("doctorID = '".$doctorID."'")->one();
									$Doctor->specialityIDs = implode(",",$doctorspecialityIDs);
									$Doctor->save();

									for($i=0;$i<count($doctortime);$i++) 
									{ 
										$Doctortime = new Doctortime;
										$Doctortime->doctorID = $doctorID;										
										$Doctortime->doctortimeDay	 = $doctortime[$i]['doctortimeDay'];
										$Doctortime->doctortimeDayArabic = $doctortime[$i]['doctortimeDayArabic'];	
										$Doctortime->doctortimeShift	 = $doctortime[$i]['doctortimeShift'];
										$Doctortime->doctortimeStart = $doctortime[$i]['doctortimeStart'];	
										$Doctortime->doctortimeEnd	 = $doctortime[$i]['doctortimeEnd'];
										$Doctortime->doctortimeContactNumber = $doctortime[$i]['doctortimeContactNumber'];
										$Doctortime->save();
										//print_r($Orderdetails);die;
										$Errors_Doctortime = $Doctortime->getErrors();
										if(!empty($Errors_Doctortime))
										{
											print_r($Errors_Doctortime);die;
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");		
										}
									}
									if(empty($Errors_Doctorspeciality) && empty($Errors_Doctortime))
									{
										
										
										$result[0]['data'][0]['doctorID'] = (string)$doctorID;
										 $result[0]['data'] = $Doctor->GetDoctorsList(0,0, 0, 0, "", "", "", "","" ,"", 10, 0, "","", $doctorID);
										$result[0]['status'] = 'true';
										//$result[0]['totals'] = $res2;
										$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"DOCTOR_CREATE");
									}
									else
									{
										$result[0]['status'] = 'false';
										$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
									}
								}
								else
								{
									print_r($Errors_Doctor);die;
									$result[0]['status'] = 'false';
				                    $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
								}
					   
			   }
			   else
			   {
				   
				    if(empty($languageID) )
				    {  
						$Required = 'Language ';
					}
					if(empty($hospitalID) )
				    {  
						$Required = 'hospital ';
					}
					if(empty($doctorFirstName) )
				    {  
						$Required = 'doctorFirstName ';
					}
					if(empty($doctorLastName) )
				    {  
						$Required = 'doctorLastName ';
					}
					if(empty($degreeID) )
				    {  
						$Required = 'degreeID ';
					}
					if(empty($nationalityID) )
				    {  
						$Required = 'nationalityID ';
					}
					if(empty($doctorGender) )
				    {  
						$Required = 'doctorGender ';
					}
					if(empty($doctorProfileImage) )
				    {  
						$Required = 'doctorProfileImage ';
					}
					if(empty($apiType) )
				    {  
						$Required = 'apiType ';
					}
					
					if(empty($loginuserID) )
				    {  
						$Required = 'loginuserID ';
					}
					if(empty($apiVersion) )
				    {  
						$Required = 'apiVersion ';
					}
				   
					/*$Required = 'Required data ';*/
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionDoctorEdit()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{ 					   

				$page = "0";
				$pagesize = "10";	
				$type ="";
				//$hospitalID ="";
			   extract($jsonParams[0]);    
			  // print_r($jsonParams[0]);    die;
			   if(!empty($languageID) && !empty($hospitalID) && !empty($doctorFirstName) && !empty($doctorLastName) && !empty($degreeID) && !empty($nationalityID) && !empty($doctorGender) && !empty($doctorProfileImage) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID) && !empty($doctorID))
               {
			   
						   
								$Doctor = Doctor::find()->where("doctorID = '".$doctorID."'")->one();
								$Doctor->hospitalID = $hospitalID;
								$Doctor->doctorFirstName = $doctorFirstName;
								$Doctor->doctorLastName = $doctorLastName;
								$Doctor->doctorFirstNameArabic = $doctorFirstNameArabic;
								$Doctor->doctorLastNameArabic = $doctorLastNameArabic;
								$Doctor->degreeID = $degreeID;
								$Doctor->nationalityID = $nationalityID;
								$Doctor->doctorGender = $doctorGender;
								$Doctor->doctorProfileImage = $doctorProfileImage;
								$Doctor->doctorStatus = "Active";
								$Doctor->specialityIDs = "0";
								$Doctor->save();
		               			$Errors_Doctor = $Doctor->getErrors();
								$doctorspecialityIDs = array();
								$cnt=0;
								if(empty($Errors_Doctor))
								{
									$doctorID = $Doctor->doctorID;
									//echo "hee ";
									$Delete = Doctorspeciality::deleteAll('doctorID = :doctorID', [':doctorID' => $doctorID]);
									if($Delete == 1)
									{
										$result[0]['status'] = 'false';
											$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
											//print_r( $Delete->getErrors());
											///print_r( $result);
											//die;
									}
									//echo "heelo ";die;
									for($i=0;$i<count($doctorspeciality);$i++) 
									{ 
										$Doctorspeciality = new Doctorspeciality;
										$Doctorspeciality->doctorID = $doctorID;										
										$Doctorspeciality->specialityID	 = $doctorspeciality[$i]['specialityID'];
										$Doctorspeciality->subspecialityID = $doctorspeciality[$i]['subspecialityID'];										
										$Doctorspeciality->save();
										$doctorspecialityID = $Doctorspeciality->doctorspecialityID;
										$doctorspecialityIDs[$cnt]= $doctorspeciality[$i]['specialityID'];
										$cnt++;
										//print_r($Orderdetails);die;
										$Errors_Doctorspeciality = $Doctorspeciality->getErrors();
										if(!empty($Errors_Doctorspeciality))
										{
											print_r($Errors_Doctorspeciality);die;
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");		
										}
									}
									$cnt=0;
									/*Update doctors for specialityIDs*/
									$Doctor = Doctor::find()->where("doctorID = '".$doctorID."'")->one();
									$Doctor->specialityIDs = implode(",",$doctorspecialityIDs);
									$Doctor->save();
									$Delete = Doctortime::deleteAll('doctorID = :doctorID', [':doctorID' => $doctorID]);
									if($Delete == 1)
									{
										$result[0]['status'] = 'false';
											$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
											die;
									}
									for($i=0;$i<count($doctortime);$i++) 
									{ 
										$Doctortime = new Doctortime;
										$Doctortime->doctorID = $doctorID;										
										$Doctortime->doctortimeDay	 = $doctortime[$i]['doctortimeDay'];
										$Doctortime->doctortimeDayArabic = $doctortime[$i]['doctortimeDayArabic'];	
										$Doctortime->doctortimeShift	 = $doctortime[$i]['doctortimeShift'];
										$Doctortime->doctortimeStart = $doctortime[$i]['doctortimeStart'];	
										$Doctortime->doctortimeEnd	 = $doctortime[$i]['doctortimeEnd'];
										$Doctortime->doctortimeContactNumber = $doctortime[$i]['doctortimeContactNumber'];
										$Doctortime->save();
										//print_r($Orderdetails);die;
										$Errors_Doctortime = $Doctortime->getErrors();
										if(!empty($Errors_Doctortime))
										{
											print_r($Errors_Doctortime);die;
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");		
										}
									}
									if(empty($Errors_Doctorspeciality) && empty($Errors_Doctortime))
									{
										
										
										$result[0]['data'][0]['doctorID'] = (string)$doctorID;
										 $result[0]['data'] = $Doctor->GetDoctorsList(0,0, 0, 0, "", "", "", "","" ,"", 10, 0, "","", $doctorID);
										$result[0]['status'] = 'true';
										//$result[0]['totals'] = $res2;
										$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"DOCTOR_CREATE");
									}
									else
									{
										$result[0]['status'] = 'false';
										$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
									}
								}
								else
								{
									print_r($Errors_Doctor);die;
									$result[0]['status'] = 'false';
				                    $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
								}
					   
			   }
			   else
			   {
				   
				    if(empty($languageID) )
				    {  
						$Required = 'Language ';
					}
					if(empty($hospitalID) )
				    {  
						$Required = 'hospital ';
					}
					if(empty($doctorFirstName) )
				    {  
						$Required = 'doctorFirstName ';
					}
					if(empty($doctorLastName) )
				    {  
						$Required = 'doctorLastName ';
					}
					if(empty($degreeID) )
				    {  
						$Required = 'degreeID ';
					}
					if(empty($nationalityID) )
				    {  
						$Required = 'nationalityID ';
					}
					if(empty($doctorGender) )
				    {  
						$Required = 'doctorGender ';
					}
					if(empty($doctorProfileImage) )
				    {  
						$Required = 'doctorProfileImage ';
					}
					if(empty($apiType) )
				    {  
						$Required = 'apiType ';
					}
					
					if(empty($loginuserID) )
				    {  
						$Required = 'loginuserID ';
					}
					if(empty($apiVersion) )
				    {  
						$Required = 'apiVersion ';
					}
				   
					/*$Required = 'Required data ';*/
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	

}
