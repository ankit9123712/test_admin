<?php

namespace backend\modules\api\v1\controllers;use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Doctorreview;
class DoctorreviewController extends \yii\web\Controller
{
   public $enableCsrfValidation = false;
	public function actionIndex()
    {
		return $this->render('index');
		\Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		return $result;
    }
	public function actionDoctorsReviewList()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{ 					   
				$languageID = "1";
				$page = "0";
				$pagesize = "10";	
				$doctorID=0; 			   
			   extract($jsonParams[0]);    
			   if(!empty($languageID))
               {
			   
				   if(!empty($apiType))
				   {
					   if(!empty($apiVersion))
					   {
						   if($loginuserID >=0 )
						   {							   
								
								$Doctorreview = new Doctorreview;
								$doctorReviewRes = $Doctorreview->GetDoctorReviewList($doctorID, $pagesize, $page,$hospitalID);
								
								if(count($doctorReviewRes)>0)
								{
									$result[0]['data'] = $doctorReviewRes;
									$result[0]['status'] = 'true';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");			
								}
								else
								{
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
								}									
									   
						   }
						   else
						   {
								$Required = 'patientID';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
						   }
							
					   }
					   else
					   {
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
					   }
				   }
				   else
				   {
						$Required = 'apiType';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionAddDoctorsReview()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{ 				   
							   
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($loginuserID) && !empty($doctorID) && !empty($doctorreviewRating) && !empty($appointmentID))
               {
				   if($loginuserID >=0 )
				   {							   
						
						$Doctorreview = new Doctorreview;
						$Doctorreview->patientID = $loginuserID;
						$Doctorreview->appointmentID = $appointmentID;
						$Doctorreview->doctorID = $doctorID;
						$Doctorreview->doctorreviewRating = $doctorreviewRating;
						$Doctorreview->doctorreviewReview = $doctorreviewReview;
						$Doctorreview->doctorreviewDate = date('Y-m-d H:i:s');
						$Doctorreview->save(false);
						$doctorreviewID = $Doctorreview->doctorreviewID;						
						if($doctorreviewID>0)
						{
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","DOCTOR_REVIEWADD");	
							$doctorReviewRes = $Doctorreview->GetDoctorReviewList($doctorID, $pagesize, $page);
							if(count($doctorReviewRes)>0)
							{
								$result[0]['data'] = $doctorReviewRes;										
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");	
						}	   
				   }
				   else
				   {
						$Required = 'patientID';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
							
					  
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	public function actionApproveDoctorsReview()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{ 				   
							   
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($doctorreviewID) && !empty($doctorreviewPatientApproved) )
               {
				   if($loginuserID >=0 )
				   {							   
						
						$Doctorreview = Doctorreview::find()->where('doctorreviewID='.$doctorreviewID)->one();
						if(!empty($Doctorreview))
						{
							$Doctorreview->doctorreviewPatientApproved = $doctorreviewPatientApproved;							
							$Doctorreview->save(false);
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","DOCTOR_REVIEWAPPROVED");	
							$doctorReviewRes = $Doctorreview->GetDoctorReviewList("", 10,0,$doctorreviewID);
							if(count($doctorReviewRes)>0)
							{
								$result[0]['data'] = $doctorReviewRes;										
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");	
						}	   
				   }
				   else
				   {
						$Required = 'patientID';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
							
					  
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionDoctorsReviewReply()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{ 				   
							   
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($doctorreviewID) && !empty($doctorreviewReply) )
               {
				   if($loginuserID >=0 )
				   {							   
						
						$Doctorreview = Doctorreview::find()->where('doctorreviewID='.$doctorreviewID)->one();
						if(!empty($Doctorreview))
						{
							$Doctorreview->doctorreviewReply = $doctorreviewReply;
							$Doctorreview->doctorreviewReplyDate = date('Y-m-d H:i:s');
							$Doctorreview->doctorreviewApproved = "Pending";							
							$Doctorreview->save(false);
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","DOCTOR_REVIEWREPLIED");	
							$doctorReviewRes = $Doctorreview->GetDoctorReviewList("", 10,0,$doctorreviewID);
							if(count($doctorReviewRes)>0)
							{
								$result[0]['data'] = $doctorReviewRes;										
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","TRYAGAIN");	
						}	   
				   }
				   else
				   {
						$Required = 'patientID';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
							
					  
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
}
