<?php


namespace backend\modules\api\v1\controllers;
use Yii;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Patient;
use backend\modules\api\v1\models\Blog;
use backend\modules\api\v1\models\Offers;
use backend\modules\api\v1\models\Hospital;
use backend\modules\api\v1\models\Doctor;

class PatientController extends \yii\web\Controller
{

	public $enableCsrfValidation = false;
	public function actionIndex()
    {
		return $this->render('index');
		\Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		return $result;
    }
	
	public function actionPatientRegister()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID))
               {
			   
				   if(!empty($apiType))
				   {
					   if(!empty($apiVersion))
					   {
						   if(!empty($patientEmail))
						   {
							   if(!empty($patientPassword))
							   {
								   if(!empty($patientDeviceType))
								   {	
									   if(!empty($patientDeviceID))
									   {
										  
											/*---------------- Check Duplication -----------------*/
											$Patient = new Patient;
											$DuplicationRes = $Patient->CheckPatientDetailsForDuplication($patientEmail);
											if($DuplicationRes == 'true')
											{
											
												$Patient->patientEmail = $patientEmail;
												$Patient->patientPassword = Yii::$app->security->generatePasswordHash($patientPassword);
												$Patient->patientDeviceType = $patientDeviceType;
												$Patient->patientDeviceID = $patientDeviceID;
												$Patient->languageID = $languageID;
												$Patient->patientVerification = "Pending";
												$Patient->save(false);
												$Errors = $Patient->getErrors();
												if(empty($Errors))
												{
												   $patientID = $Patient->patientID;
												   $res = $Patient->GetPatientDetails($patientID,$patientID);  
												   if(!empty($res))
												   {  														   
														$result[0]['data'] = $res;
														$result[0]['status'] = 'true';
														$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"REGISTRATION_SUCCESS");
												   }
												   else
												   {
														$result[0]['status'] = 'false';
														$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
													   
												   }
													
												}
												else
												{
													$result[0]['status'] = 'false';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
												}
											}
											else
											{
												if($DuplicationRes == 'patientEmail')
												{
													$result[0]['status'] = 'false';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"EMAIL_EXIST");
												}
												else
												{
													$result[0]['status'] = 'false';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"MOBILE_EXIST");
												}
												
											}
												
									   }
									   else
									   {
											$Required = 'patientDeviceID';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   }
								   }
								   else
								   {
										$Required = 'patientDeviceType';
										$result[0]['status'] = 'false';
										$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
								   }
									
							   }
							   else
							   {
									$Required = 'patientPassword';
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
							   }
								
						   }
						   else
						   {
								$Required = 'patientEmail';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
						   }
							
					   }
					   else
					   {
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
					   }
				   }
				   else
				   {
						$Required = 'apiType';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	
	public function actionPatientIntroduceYourself()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID))
               {
			   
				   if(!empty($apiType))
				   {
					   if(!empty($apiVersion))
					   {
						   if(!empty($patientFirstName))
						   {
							   if(!empty($nationalityID))
							   {
								   if(!empty($patientDeviceType))
								   {	
									   if(!empty($patientDeviceID))
									   { 
										if(!empty($loginuserID))
									   {
									   if(!empty($patientMobile))
									   { 
										if(!empty($patientLastName))
									   {
									   if(!empty($patientDOB))
									   {
									   if(!empty($patientCountryCode))
									   { 
										if(!empty($patientGender))
									   {
									   if(!empty($countryID))
									   { 
										  
											/*---------------- Check Duplication -----------------*/
											$Patient = new Patient;
											$DuplicationRes = $Patient->CheckPatientDetailsForDuplication("",$patientMobile,$loginuserID);
											if($DuplicationRes == 'true')
											{
											   $Patient_Res = Patient::find()->where('patientID = "'.$loginuserID.'"')->one();
											   if(!empty($Patient_Res))
											   {
											
													$Patient_Res->patientFirstName = $patientFirstName;
													$Patient_Res->patientLastName = $patientLastName;
													$Patient_Res->patientDOB = $patientDOB;
													$Patient_Res->patientCountryCode = $patientCountryCode;
													$Patient_Res->patientMobile = $patientMobile;
													$Patient_Res->patientGender = $patientGender;
													$Patient_Res->countryID = $countryID;
													$Patient_Res->nationalityID = $nationalityID;
													$Patient_Res->patientDeviceType = $patientDeviceType;
													$Patient_Res->patientDeviceID = $patientDeviceID;
													$Patient_Res->save(false);
													$Errors = $Patient_Res->getErrors();
													if(empty($Errors))
													{
													   $patientID = $Patient_Res->patientID;
													   $res = $Patient->GetPatientDetails($patientID,$patientID);  
													   if(!empty($res))
													   {  														   
															$result[0]['data'] = $res;
															$result[0]['status'] = 'true';
															$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"PATIENTINTRODUCEYOURSELF_SUCCESS");
													   }
													   else
													   {
															$result[0]['status'] = 'false';
															$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
														   
													   }
														
													}
													else
													{
														$result[0]['status'] = 'false';
														$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
													}
												
											   }
											   else
											   {
													$result[0]['status'] = 'false';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
											   }
											}
											else
											{
												if($DuplicationRes == 'patientEmail')
												{
													$result[0]['status'] = 'false';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"EMAIL_EXIST");
												}
												else
												{
													$result[0]['status'] = 'false';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"MOBILE_EXIST");
												}
												
											}
										
									   }
									   else
									   {
											$Required = 'countryID';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   }   

									   }
									   else
									   {
											$Required = 'patientGender';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   }  
									   
									   }
									   else
									   {
											$Required = 'patientCountryCode';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   } 
									   }
									   else
									   {
											$Required = 'patientDOB';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   }  
									   
									   }
									   else
									   {
											$Required = 'patientLastName';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   }   

									   }
									   else
									   {
											$Required = 'patientMobile';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   } 
									   }
									   else
									   {
											$Required = 'loginuserID';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   }

									   }
									   else
									   {
											$Required = 'patientDeviceID';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   }
								   }
								   else
								   {
										$Required = 'patientDeviceType';
										$result[0]['status'] = 'false';
										$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
								   }
									
							   }
							   else
							   {
									$Required = 'nationalityID';
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
							   }
								
						   }
						   else
						   {
								$Required = 'patientFirstName';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
						   }
							
					   }
					   else
					   {
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
					   }
				   }
				   else
				   {
						$Required = 'apiType';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	public function actionPatientLogin()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID))
               {
			   
				   if(!empty($apiType))
				   {
					   if(!empty($apiVersion))
					   {
						   if(!empty($patientEmail))
						   {
							   if(!empty($patientPassword))
							   {
								   if(!empty($patientDeviceType))
								   {	
									   if(!empty($patientDeviceID))
									   { 
										   $Patient_Res = Patient::find()->select('patientID,patientStatus,patientEmail,patientMobile,patientPassword,patientVerification')->where('patientEmail = "'.$patientEmail.'"')->asArray()->one();
										   if(!empty($Patient_Res))
										   {
												 if(1 == Yii::$app->security->validatePassword($patientPassword,$Patient_Res['patientPassword']))
												 {
													 if($Patient_Res['patientStatus'] == "Active")
													 {
														 $patientID = $Patient_Res['patientID'];
														 
														if(($Patient_Res['patientVerification'] == "Verified") && (!empty($Patient_Res['patientMobile'])))
														{
															$Sql = "UPDATE patient SET patientDeviceType = '".$patientDeviceType."', patientDeviceID = '".$patientDeviceID."' WHERE patientID = '".$patientID."'";
															Yii::$app->db->createCommand($Sql)->execute();
															
															$Patient = new Patient;
															$result[0]['data'] = $Patient->GetPatientDetails($patientID,$patientID);
															$result[0]['status'] = 'true';
															$result[0]['message'] = '';
															
														}
														else if(($Patient_Res['patientVerification'] == "Pending") && (!empty($Patient_Res['patientMobile'])))
														{
															$result[0]['status'] = 'false';
															$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"PATIENTINTRODUCEYOURSELF_SUCCESS");
														}
														else if(($Patient_Res['patientVerification'] == "Pending") && (empty($Patient_Res['patientMobile'])))
														{
															$Patient = new Patient;
															$result[0]['data'] = $Patient->GetPatientDetails($patientID,$patientID);
															$result[0]['status'] = 'false';
															$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"PATIENTINTRODUCEYOURSELF_PENDING");;
														}
															
													 }
													 else
													 {
														$result[0]['status'] = 'false';
														$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"CONTACTADMINFORACCOUNTACTIVE");
													 }
												 }
												 else
												 {
													
													$result[0]['status'] = 'false';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"LOGIN_ERROR");
												 }
											
										   }
										   else
										   {
												$result[0]['status'] = 'false';
												$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"LOGIN_ERROR");
										   }
										

									   }
									   else
									   {
											$Required = 'patientDeviceID';
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
									   }
								   }
								   else
								   {
										$Required = 'patientDeviceType';
										$result[0]['status'] = 'false';
										$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
								   }
									
							   }
							   else
							   {
									$Required = 'patientPassword';
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
							   }
								
						   }
						   else
						   {
								$Required = 'patientEmail';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
						   }
							
					   }
					   else
					   {
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
					   }
				   }
				   else
				   {
						$Required = 'apiType';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	public function actionPatientAddFavorite()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
			
			   
			   $languageID = "1";
			   $page = "0";
               $pagesize = "10";		
			   extract($jsonParams[0]);    
			   if(!empty($languageID))
               {
			   
				   if(!empty($apiType))
				   {
					   if(!empty($apiVersion))
					   {
						   if($loginuserID >=0 )
						   {							   
								   
								$sql = "SELECT patientfavoriteID FROM patientfavoriite WHERE patientID=".$loginuserID." AND doctorID=".$doctorID;
								
								$connection = Yii::$app->getDb();
								$command = $connection->createCommand($sql);
								$result = $command->queryAll();

								if(count($result)>0)
								{
								}
								else
								{
									\Yii::$app->db->createCommand("INSERT patientfavoriite (patientID,doctorID)  values ('".$loginuserID."','".$doctorID."')")->execute(); 	
								}	
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ADD_FAVORITE");				
									   
						   }
						   else
						   {
								$Required = 'patientID';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
						   }
							
					   }
					   else
					   {
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
					   }
				   }
				   else
				   {
						$Required = 'apiType';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	public function actionPatientHome()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
			
			   
			   $languageID = "1";
			   $page = "0";
               $pagesize = "10";	
			   $countryID="";
			   $nationalityID="";
			   $offersGender="";
			   extract($jsonParams[0]);    
			   if(!empty($languageID))
               {
			   
				   if(!empty($apiType))
				   {
					   if(!empty($apiVersion))
					   {
						   if($loginuserID >=0 )
						   {							   
								   
								$Blog = new Blog;	
								$Offers= new Offers;	
								$Hospital = new Hospital;
								$Doctor = new Doctor;	
								$offer_Res = $Offers->GetOffersList($loginuserID,$nationalityID,$offersGender,$countryID,"","","",$pagesize,$page);
								$blog_Res = $Blog->GetBlogList($pagesize,$page);  								
								$doctorRes = $Doctor->GetDoctorsList($loginuserID,0, 0, 0, "", "", "", "","" ,"", $pagesize, $page, "");
								$hospital_Res = $Hospital->GetHospitalList($countryID,0 ,0,"",$pagesize,$page); 
									
								$result[0]['offers'] = $offer_Res ;
								$result[0]['hospitals'] = $hospital_Res ;
								$result[0]['doctors'] = $doctorRes ;
								$result[0]['blog'] = $blog_Res ;

								$result[0]['status'] = 'true';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ADD_FAVORITE");				
									   
						   }
						   else
						   {
								$Required = 'loginuserID';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
						   }
							
					   }
					   else
					   {
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
					   }
				   }
				   else
				   {
						$Required = 'apiType';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	
	public function actionPatientChangePassword()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
						
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($patientCurrentPassword) && !empty($patientNewPassword) && !empty($loginuserID))
				{
					$User_Res = Patient::find()->select('patientID,patientPassword')->where('patientID = "'.$loginuserID.'"')->one();
					if(!empty($User_Res))
					{ 
						 if(1 == \Yii::$app->security->validatePassword($patientCurrentPassword,$User_Res->patientPassword))
						 {
							$Errors = "";
							$patientPassword = \Yii::$app->security->generatePasswordHash($patientNewPassword);		
							$User_Res->patientPassword = $patientPassword;
							$User_Res->save(false);
							$Errors = $User_Res->getErrors();
							if(empty($Errors))
							{ 
								$Patient = new Patient;
								$result[0]['data'] = $Patient->GetPatientDetails($patientID,$patientID);
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"CHANGEPASSWORD_SUCCESS");
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
							}
						 }
						 else
						 {
							 $result[0]['status'] = 'false';
							 $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"CURRENTPASSWORD_ERROR");
						 }
							 
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
					}
					
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				}
					

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionPatientUpdateSettings()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				extract($jsonParams[0]);    
				if(!empty($apiType))
				{
					if(!empty($languageID))
					{
						if(!empty($apiVersion))
						{
							if(!empty($patientUpcomingPush))
							{
								
								if(!empty($patientStatusPush))
								{

									if(!empty($loginuserID))
									{

										$User_Res = Patient::find()->where('patientID = "'.$loginuserID.'"')->one();
										if(!empty($User_Res))
										{
											$Errors = "";
											$User_Res->patientStatusPush = $patientStatusPush;
											$User_Res->patientUpcomingPush = $patientUpcomingPush;
											$User_Res->patientRatingPush = $patientRatingPush;
											$User_Res->patientOfferPush = $patientOfferPush;
											$User_Res->patientAdminPush = $patientAdminPush;
											$User_Res->save(false);
											$Errors = $User_Res->getErrors();
											if(empty($Errors))
											{ 
												$patientID = $User_Res->patientID;
												$Patient = new Patient;
												$res = $Patient->GetPatientDetails($patientID,$patientID);
												if(!empty($res))
												{
													$result[0]['data'] = $res;
													$result[0]['status'] = 'true';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"USERUPDATESETTINGS_SUCCESS");// messages_constant::USERUPDATESETTINGS_SUCCESS;
												}
												else
												{
													$result[0]['status'] = 'false';
													$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");// messages_constant::NORECORD;
												}
											}
											else
											{
												$result[0]['status'] = 'false';
												$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");// messages_constant::TRYAGAIN;
											}

											
										}
										else
										{
											$result[0]['status'] = 'false';
											$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");// INVALIDDATA::ISMISSING;
										}
									}
									else
									{
										$Required = 'loginuserID';
										$result[0]['status'] = 'false';
										$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");// $Required.messages_constant::ISMISSING;
									}
								}
								else
								{
									$Required = 'userPushNotification';
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");// $Required.messages_constant::ISMISSING;
								}
								
							}
							else
							{
								$Required = 'userEmailNotification';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");// $Required.messages_constant::ISMISSING;
							}
							
						}
						else
						{
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");// $Required.messages_constant::ISMISSING;
						}
					}
					else
					{
						$Required = 'languageID';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");// 
					}
				}
				else
				{
					$Required = 'apiType';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");// 
				}

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

	public function actionPatientRemoveFavorite()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
			
			   
			   $languageID = "1";
			   $page = "0";
               $pagesize = "10";		
			   extract($jsonParams[0]);    
			   if(!empty($languageID))
               {
			   
				   if(!empty($apiType))
				   {
					   if(!empty($apiVersion))
					   {
						   if($loginuserID >=0 )
						   {							   
								   
								
								\Yii::$app->db->createCommand("DELETE FROM patientfavoriite WHERE patientID ='".$loginuserID."' AND doctorID='".$doctorID."'")->execute(); 									
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"REMOVE_FAVORITE");				
									   
						   }
						   else
						   {
								$Required = 'patientID';
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
						   }
							
					   }
					   else
					   {
							$Required = 'apiVersion';
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
					   }
				   }
				   else
				   {
						$Required = 'apiType';
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				   }
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionPatientUpdateProfilePicture()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($patientProfilePicture)  )
				{
					$User_Res = Patient::find()->where('patientID = "'.$loginuserID.'"')->one();
					//print_r($User_Res);die;
					if(!empty($User_Res))
					{
						$Errors = "";
						$User_Res->patientProfilePicture = $patientProfilePicture;
						$User_Res->save(false);
						$Errors = $User_Res->getErrors();
						if(empty($Errors))
						{ 
							$patientID = $User_Res->patientID;
							$Patient = new Patient;
							$res= $Patient->GetPatientDetails($loginuserID,$loginuserID);  
							//print_r($res);die;
							if(!empty($res))
							{
								$result[0]['data'] = $res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"PROFILEPICTUREUPDATE_SUCCESS");// messages_constant::PROFILEPICTUREUPDATE_SUCCESS;
							}
							else
							{
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");// messages_constant::NORECORD;
							}
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");// messages_constant::TRYAGAIN;
						}

						
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");// INVALIDDATA::ISMISSING;
					}
				}
				else
				{
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");// $Required.messages_constant::ISMISSING;
				}
								
							

			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}


	public function actionFileUpload()
	{
		$ApiLog = new ApiLog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$postParams = \Yii::$app->request->post();	
		
		if ($ApiLog->is_json($postParams["json"]))
		{			
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);			
			if(count($jsonParams[0])>0)
			{
				$Key = "";
				$FilePath = "";
				$FileField = "";
				$subpath = "";
				
				extract($jsonParams[0]);
				
				$count = 1;
				if($count > 0)
				{
					
					$path=$_REQUEST['FilePath'];
					
					$filename=$_FILES['FileField']['name'];
					$system=explode(".",$filename);
/*print_r($system);
echo $system[count($system)-1];
die;*/
					$flg=0;							
					if(preg_match("/jpg|jpeg|JPG|JPEG|mp4|mp3/",$system[count($system)-1]))
						$flg=1;	

					if($flg==0)
					{
						if (preg_match("/gif|GIF/",$system[count($system)-1]))
						$flg=1;
					}	
					if($flg==0)
					{
						if (preg_match("/png|PNG/",$system[count($system)-1]))	
						$flg=1;
					}	

					if($flg==1)
					{	
						if($subpath=="")
						$destination = "uploads/".$path."/";
						else
						$destination = "uploads/".$path."/".$subpath."/";	
						if (!is_dir($destination)) 
						{
							mkdir($destination,0777,true);
						}	

						$file_path = $destination.$filename;

						if(move_uploaded_file($_FILES['FileField']['tmp_name'],$file_path))
						{
							$result[0]["status"]= "true";
							$result[0]["fileName"]=$filename;
							$result[0]["message"]= "File uploaded successfully.";
							
						}
						else
						{
							$result[0]["status"]= "false";
							$result[0]["message"]= "problem occurs while uploading ";
						}
					}
					else
					{
						$error = "Problem with file upload";
						$result[0]["fileName"]=$filename." ".$flg;
						$result[0]["status"]= "false";		
						$result[0]["message"]= $error;		
					}	

				}
				else
				{
					$result[0]["status"]= "false";
					$result[0]["message"]= "dont know what i do got here ";
				}	
			}
		}
		return $result;	
		
	}
	
	public function actionPatientUpdateProfile()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($patientFirstName) && !empty($nationalityID) && !empty($patientDeviceType) && !empty($patientDeviceID)  && !empty($loginuserID) && !empty($patientMobile) && !empty($patientLastName) && !empty($patientDOB) && !empty($patientCountryCode) && !empty($patientGender) && !empty($countryID))
               {						  
					
				   $Patient_Res = Patient::find()->where('patientID = "'.$loginuserID.'"')->one();
				  // print_r($Patient_Res);
				  // die;
				   if(!empty($Patient_Res))
				   {
				
						$Patient_Res->patientFirstName = $patientFirstName;
						$Patient_Res->patientLastName = $patientLastName;
						$Patient_Res->patientDOB = $patientDOB;
						$Patient_Res->patientCountryCode = $patientCountryCode;
						$Patient_Res->patientMobile = $patientMobile;
						$Patient_Res->patientGender = $patientGender;
						$Patient_Res->countryID = $countryID;
						$Patient_Res->nationalityID = $nationalityID;
						$Patient_Res->patientDeviceType = $patientDeviceType;
						$Patient_Res->patientDeviceID = $patientDeviceID;
						$Patient_Res->save(false);
						$Errors = $Patient_Res->getErrors();
						//print_r($Errors);
				   //die;
						if(empty($Errors))
						{
						   $Patient = new Patient;
						   $patientID = $Patient_Res->patientID;
						   $res = $Patient->GetPatientDetails($patientID,$patientID);  
						   if(!empty($res))
						   {  														   
								$result[0]['data'] = $res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"PROFILEUPDATE_SUCCESS");
						   }
						   else
						   {
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
							   
						   }
							
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
						}
					
				   }
				   else
				   {
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
				   }		   
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
}

