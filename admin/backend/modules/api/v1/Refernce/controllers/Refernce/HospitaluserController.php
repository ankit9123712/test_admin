<?php

namespace backend\modules\api\v1\controllers;use Yii;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Patient;
use backend\modules\api\v1\models\Offers;
use backend\modules\api\v1\models\Hospital;
use backend\modules\api\v1\models\Hospitaluser;
use backend\modules\api\v1\models\Doctor;

class HospitaluserController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
    {
		return $this->render('index');
		\Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		return $result;
    }
	public function actionHospitalLogin()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($hospitaluserEmail) && !empty($hospitaluserPassword) && !empty($hospitaluserType) )
               {
				   $Hospital_Res = Hospitaluser::find()->select('hospitaluserID,hospitaluserStatus,hospitaluserEmail,hospitaluserPassword')->where('hospitaluserEmail = "'.$hospitaluserEmail.'"')->asArray()->one();
				   if(!empty($Hospital_Res))
				   {
						 if(1 == Yii::$app->security->validatePassword($hospitaluserPassword,$Hospital_Res['hospitaluserPassword']))
						 {
							 if($Hospital_Res['hospitaluserStatus'] == "Active")
							 {
								$hospitaluserID = $Hospital_Res['hospitaluserID'];
								$Sql = "UPDATE hospitaluser SET hospitaluserLastLoginDate = '".date('Y-m-d H:i:s')."' WHERE hospitaluserID = '".$hospitaluserID."'";
								Yii::$app->db->createCommand($Sql)->execute();
								
								$Hospitaluser = new Hospitaluser;
								$result[0]['data'] = $Hospitaluser->GetHospitaluserDetails($hospitaluserID);
								$result[0]['status'] = 'true';
								$result[0]['message'] = '';
							 }
							 else
							 {
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"CONTACTADMINFORACCOUNTACTIVE");
							 }
						 }
						 else
						 {
							
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"LOGIN_ERROR");
						 }
					
				   }
				   else
				   {
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"LOGIN_ERROR");
				   }
										

									   
				 
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionDashboard()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($hospitaluserID) && !empty($hospitaluserType) )
                {
								
						$Hospitaluser = new Hospitaluser;
						$result[0]['data'] = $Hospitaluser->GetHospitaluserDetails($hospitaluserID);
						$result[0]['status'] = 'true';
						$result[0]['message'] = '';						
					
			    }
			    else
			    {
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"LOGIN_ERROR");
			    }
										

									   
				 
			}
		   else
		   {
				$Required = 'languageID';
				$result[0]['status'] = 'false';
				$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
		   }
				
			
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionHospitalUserUpdateProfile()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($hospitaluserFirstName) && !empty($hospitaluserLastName) && !empty($hospitaluserEmail) && !empty($hospitaluserMobile)  && !empty($countryID) && !empty($cityID) && !empty($hospitaluserGender) && !empty($hospitaluserType) && !empty($hospitalID) && !empty($hospitaluserID) )
               {						  
					
				   $Hospital_Res = Hospitaluser::find()->where('hospitaluserID = "'.$hospitaluserID.'"')->one();
				   if(!empty($Hospital_Res))
				   {
				
						$Hospital_Res->hospitaluserFirstName = $hospitaluserFirstName;
						$Hospital_Res->hospitaluserLastName = $hospitaluserLastName;
						$Hospital_Res->hospitaluserEmail = $hospitaluserEmail;
						$Hospital_Res->hospitaluserMobile = $hospitaluserMobile;
						$Hospital_Res->countryID = $countryID;
						$Hospital_Res->cityID = $cityID;
						$Hospital_Res->hospitaluserGender = $hospitaluserGender;
						$Hospital_Res->hospitaluserType = $hospitaluserType;
						$Hospital_Res->hospitalID = $hospitalID;
						$Hospital_Res->save(false);
						$Errors = $Hospital_Res->getErrors();
						if(empty($Errors))
						{
						   $Hospitaluser = new Hospitaluser;
						   $hospitaluserID = $Hospital_Res->hospitaluserID;
						   $res = $Hospitaluser->GetHospitaluserDetails($hospitaluserID);  
						   if(!empty($res))
						   {  														   
								$result[0]['data'] = $res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"PROFILEUPDATE_SUCCESS");
						   }
						   else
						   {
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
							   
						   }
							
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
						}
					
				   }
				   else
				   {
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
				   }		   
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionHospitalCreateUser()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($hospitaluserFirstName) && !empty($hospitaluserLastName) && !empty($hospitaluserEmail) && !empty($hospitaluserMobile)  && !empty($countryID) && !empty($cityID) && !empty($hospitaluserGender) && !empty($hospitaluserType) && !empty($hospitalID) )
               {
										  
					/*---------------- Check Duplication -----------------*/
					$Hospitaluser = new Hospitaluser;
					$DuplicationRes = $Hospitaluser->CheckHospitalUsersForDuplication($hospitaluserEmail,"","");
					if($DuplicationRes == 'true')
					{
					    $Hospital_Res = new Hospitaluser;
						$Hospital_Res->hospitaluserFirstName = $hospitaluserFirstName;
						$Hospital_Res->hospitaluserLastName = $hospitaluserLastName;
						$Hospital_Res->hospitaluserEmail = $hospitaluserEmail;
						$Hospital_Res->hospitaluserMobile = $hospitaluserMobile;
						$Hospital_Res->countryID = $countryID;
						$Hospital_Res->cityID = $cityID;
						$Hospital_Res->hospitaluserImage = $hospitaluserImage;
						$Hospital_Res->hospitaluserGender = $hospitaluserGender;
						$Hospital_Res->hospitaluserPassword =  Yii::$app->security->generatePasswordHash($hospitaluserPassword);
						$Hospital_Res->hospitaluserType = $hospitaluserType;
						$Hospital_Res->hospitaluserStatus = "Active";
						$Hospital_Res->hospitalID = $hospitalID;
						
						$Hospital_Res->save(false);
						$Errors = $Hospital_Res->getErrors();
						if(empty($Errors))
						{						   
						   $hospitaluserID = $Hospital_Res->hospitaluserID;
						   $res = $Hospitaluser->GetHospitaluserDetails($hospitaluserID);
						   if(!empty($res))
						   {  														   
								$result[0]['data'] = $res;
								$result[0]['status'] = 'true';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"USER_CREATED");
						   }
						   else
						   {
								$result[0]['status'] = 'false';
								$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
							   
						   }
							
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
						}
						
					   
					}
					else
					{
						if($DuplicationRes == 'hospitaluserEmail')
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"EMAIL_EXIST");
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"MOBILE_EXIST");
						}
						
					}
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionHospitalUserList()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
	
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($pagesize) && !empty($hospitalID)  )
                {
								
						$Hospitaluser = new Hospitaluser;
						$result[0]['data'] = $Hospitaluser->GetHospitaluserDetails("",$pagesize,$page,$hospitaluserType,$hospitaluserStatus,$hospitalID);
						$result[0]['status'] = 'true';
						$result[0]['message'] = '';						
					
			    }
			    else
			    {
					$Required = 'languageID';
				$result[0]['status'] = 'false';
				$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			    }
										

									   
				 
			}
		   else
		   {
				$Required = 'languageID';
				$result[0]['status'] = 'false';
				$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
		   }
				
			
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionHospitalDeleteUser()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($deleteUserID) && !empty($hospitaluserID) )
               {						  
					
				   $Hospital_Res = Hospitaluser::find()->where('hospitaluserID = "'.$deleteUserID.'"')->one();
				   if(!empty($Hospital_Res))
				   {
				
						$Hospital_Res->hospitaluserStatus = "Inactive";
						$Hospital_Res->save(false);
						$Errors = $Hospital_Res->getErrors();
						if(empty($Errors))
						{
						   $hospitaluserID = $Hospital_Res->hospitaluserID;
						  	
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"USER_DELETE");
						   
							
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
						}
					
				   }
				   else
				   {
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
				   }		   
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionHospitalUserProfilePictureUpdate()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				$userSingupReferCode = "";
				$languageID = "1";
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($hospitaluserImage) && !empty($hospitaluserID) )
               {						  
					
				   $Hospital_Res = Hospitaluser::find()->where('hospitaluserID = "'.$hospitaluserID.'"')->one();
				   if(!empty($Hospital_Res))
				   {
				
						$Hospital_Res->hospitaluserImage = $hospitaluserImage;
						$Hospital_Res->save(false);
						$Errors = $Hospital_Res->getErrors();
						if(empty($Errors))
						{
						    $hospitaluserID = $Hospital_Res->hospitaluserID;
						  	$Hospitaluser = new Hospitaluser;
						   $result[0]['data'] = $Hospitaluser->GetHospitaluserDetails($hospitaluserID);
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
						   
							
						}
						else
						{
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"TRYAGAIN");
						}
					
				   }
				   else
				   {
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
				   }		   
			   }
			   else
			   {
					$Required = 'languageID';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
}
