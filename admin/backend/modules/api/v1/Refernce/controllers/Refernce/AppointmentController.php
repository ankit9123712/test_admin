<?php

namespace backend\modules\api\v1\controllers;use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Appointment;
use backend\modules\api\v1\models\Appointmenttime;

class AppointmentController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
	public function actionIndex()
    {
		return $this->render('index');
		\Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		return $result;
    }
	public function actionAddAppointment()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($patientID) && !empty($doctorID) && !empty($appointmentByCalendar) && !empty($appointmentPayment) && !empty($appointmentCharge) )
               {						  
					/*---------------- Check Duplication -----------------*/
					$Appointment = new Appointment;
					$Appointment->patientID = $patientID;
					$Appointment->patientdepenID = $patientdepenID;
					$Appointment->doctorID = $doctorID;
					$Appointment->appointmentPeriod = $appointmentPeriod;
					$Appointment->appointmentSlot = $appointmentSlot;
					$Appointment->appointmentByCalendar = $appointmentByCalendar;
					$Appointment->appointmentPayment = $appointmentPayment;
					$Appointment->insurancecompID = $insurancecompID;
					$Appointment->insuranceclassName = $insuranceclassName;					
					$Appointment->appointmentOfferCode = $appointmentOfferCode;
					$Appointment->appointmentCharge = $appointmentCharge;
					$Appointment->save(false);
					$Errors = $Appointment->getErrors();
					if(empty($Errors))
					{
					   $appointmentID = $Appointment->appointmentID;
					   for($i=0;$i<count($appointmenttime);$i++) 
					   { 
							$Appointmenttime = new Appointmenttime;
							$Appointmenttime->appointmentID	 = $appointmentID	;										
							$Appointmenttime->appointmenttimeDay = $appointmenttime[$i]['appointmenttimeDay'];
							$Appointmenttime->appointmenttimeStart = $appointmenttime[$i]['appointmenttimeStart'];										
							$Appointmenttime->appointmenttimeEnd = $appointmenttime[$i]['appointmenttimeEnd'];
							$Appointmenttime->save();
							$Errors_OrderDetails = $Appointmenttime->getErrors();
					   }
					   
					   
					   $res = $Appointment->GetAppointmentDetails($patientID);  
					   if(!empty($res))
					   {  														   
							$result[0]['data']["appointmentID"] = $appointmentID;
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"APPOINTMENT_SUCCESS");
					   }
					   else
					   {
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
						   
					   }
						
					}				   
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
			   
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	public function actionMyAppointments()
	{		
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				
			  $hospitalID=0;
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && ($patientID >= 0)  )
               {						  
					/*---------------- Check Duplication -----------------*/
					$Appointment = new Appointment;
					$res = $Appointment->GetAppointmentDetails($patientID,$pagesize,$page,$sortBy,$hospitalID);  
					if(!empty($res))
					{  														   
						$result[0]['data'] = $res;
						$res2 = $Appointment->GetTotal($patientID,$sortBy,"",$hospitalID);
						$total=$res2[0]["Total"];	
						//print_r($res2);
					//	die;
						$result[0]['count']["total"] = $total;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"APPOINTMENT_SUCCESS");
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
					   
					}
						
									   
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
			   
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionCancelAppointment()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
				
		
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($apiVersion) && !empty($patientID) && !empty($appointmentID) && !empty($appointmentCancellBy) && !empty($appointmentCancelRemarks)  )
               {						  
					/*---------------- Check Duplication -----------------*/
					$Appointment = Appointment::find()->where('appointmentID = "'.$appointmentID.'"')->one();
					$Appointment->appointmentCancellBy = $appointmentCancellBy;
					$Appointment->appointmentCancelRemarks = $appointmentCancelRemarks;
					$Appointment->appointmentStatus = "Cancel";
					$Appointment->save(false);
					$Errors = $Appointment->getErrors();
					if(empty($Errors))
					{  
					   
					   $res = $Appointment->GetAppointmentDetails($patientID);  
					   if(!empty($res))
					   {  														   
							//$Appointment = new Appointment;
							//$res = $Appointment->GetAppointmentDetails($patientID,$pagesize,$page,$sortBy);
							//$result[0]['data'] = $res;							
							$result[0]['status'] = 'true';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"APPOINTMENTCANCEL_SUCCESS");
					   }
					   else
					   {
							$result[0]['status'] = 'false';
							$result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORD");
						   
					   }
						
					}				   
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
			   
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	

}
