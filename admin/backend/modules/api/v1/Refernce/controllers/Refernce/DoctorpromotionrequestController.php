<?php

namespace backend\modules\api\v1\controllers;
use Yii;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Doctorpromotionrequest;
class DoctorpromotionrequestController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
	public function actionIndex()
    {
		return $this->render('index');
		\Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		return $result;
    }
	public function actionDoctorPromotionRequest()
	{
		$Apilog = new Apilog; 
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if($Apilog->is_json($postParams["json"]))
		{   
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)			
			{  
			   extract($jsonParams[0]);    
			   if(!empty($languageID) && !empty($apiType) && !empty($hospitalID) && !empty($doctorID) && !empty($specialityID) && !empty($subspecialityID) && !empty($apiVersion) && !empty($promoreqLength) && !empty($hospitaluserFirstName) && !empty($hospitaluserMobile) && !empty($hospitaluserEmail))
               {
			   
				  							   
								
								$Doctorpromotionrequest = new Doctorpromotionrequest;
								$Errors = "";
								$Doctorpromotionrequest->hospitalID	 = $hospitalID	;
								$Doctorpromotionrequest->doctorID = $doctorID;
								$Doctorpromotionrequest->specialityID = $specialityID;
								$Doctorpromotionrequest->subspecialityID = $subspecialityID;
								$Doctorpromotionrequest->promoreqLength = $promoreqLength;
								$Doctorpromotionrequest->promoreqConcatName = $hospitaluserFirstName;
								$Doctorpromotionrequest->promoreqContactNo = $hospitaluserMobile;
								$Doctorpromotionrequest->promoreqEmail = $hospitaluserEmail;
								$Doctorpromotionrequest->promoreqStatus = "Pending";
								$Doctorpromotionrequest->save(false);
								$Errors = $Doctorpromotionrequest->getErrors();
								if(empty($Errors))
								{ 		
								
									$result[0]['status'] = 'true';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");													
								}
								else
								{
									$result[0]['status'] = 'false';
									$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORD");
								}
			   }
			   else
			   {
					$Required = 'Required data ';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			   }
				
			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

}
