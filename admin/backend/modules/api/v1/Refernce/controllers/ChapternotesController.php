<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Chapternotes;
class ChapternotesController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		return $result;
	}
	

	public function actionAddChapterNotes()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
							
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($coursechapterID) && !empty($chapternoteNote))
				{
						$Chapternotes = new Chapternotes;
					
						$Chapternotes->userID = $loginuserID;
						$Chapternotes->coursechapterID = $coursechapterID;
						$Chapternotes->chapternoteNote = $chapternoteNote;								
						$Chapternotes->save();
						$resChapternotes = $Chapternotes->GetChapternotesList($coursechapterID,$loginuserID,0,10);
						$result[0]['data'] = $resChapternotes;						
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_NOTEADD");
						
					
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionEditChapterNotes()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
							
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($coursechapterID) && !empty($chapternoteNote) && !empty($chapternoteID))
				{
					$Chapternotes_Res = Chapternotes::find()->where('chapternoteID = "'.$chapternoteID.'"')->one();
					//List of course
					if(!empty($Chapternotes_Res))
					{
						$Chapternotes_Res->userID = $loginuserID;
						$Chapternotes_Res->coursechapterID = $coursechapterID;
						$Chapternotes_Res->chapternoteNote = $chapternoteNote;						
						$Chapternotes_Res->save();
						$resChapternotes = $Chapternotes->GetChapternotesList($coursechapterID,$loginuserID,0,10);
						$result[0]['data'] = $resChapternotes;						
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_NOTEEDITED");
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
						
					}	
					
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionDeleteChapterNotes()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
							
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($chapternoteID)  )
				{
					$Chapternotes = new Chapternotes;
					$Chapternotes_Res = Chapternotes::find()->where('chapternoteID = "'.$chapternoteID.'"')->one();
					//List of course
					if(!empty($Chapternotes_Res))
					{
						$Delete = Chapternotes::deleteAll('chapternoteID = :chapternoteID', [':chapternoteID' => $chapternoteID]);
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_NOTEDELETE");
					}
					else
					{
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_UNFAVORITE");
						
					}
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

	public function actionGetChapterNotes()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($coursechapterID) )
				{
					$Chapternotes = new Chapternotes;
					//List of course
					$resChapternotes = $Chapternotes->GetChapternotesList($coursechapterID,$loginuserID,$page,$pagesize);
					if(!empty($resChapternotes))
					{
						$result[0]['data'] = $resChapternotes;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");	
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}					

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
}
