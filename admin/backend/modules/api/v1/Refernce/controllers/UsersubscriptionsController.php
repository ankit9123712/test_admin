<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Usersubscriptions;
use backend\modules\api\v1\models\Usercourses;
use Yii;
class UsersubscriptionsController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		return $result;
	}
	

	public function actionGetMySubscriptions()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID))
				{
					$Usersubscriptions = new Usersubscriptions;
					//List of course
					$resSubscription = $Usersubscriptions->GetUserSubscriptionplanList($loginuserID,$page,$pagesize);
					if(!empty($resSubscription))
					{
						$result[0]['data'] = $resSubscription;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionAddMySubscriptions()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($planID) && !empty($subscriptionStartDate) && !empty($subscriptionEndDate))
				{
					$Usersubscriptions = new Usersubscriptions;
					$Usersubscriptions->planID = $planID;
					$Usersubscriptions->userID = $loginuserID;
					$Usersubscriptions->subscriptionStartDate = $subscriptionStartDate;
					$Usersubscriptions->subscriptionEndDate = $subscriptionEndDate;
					$Usersubscriptions->subscriptionStatus = "Active";
					$Usersubscriptions->save(false);
					// get courses based on plan ID 
					$sql= "SELECT courseID from courses inner join subscriptionplan on subscriptionplan.coursestdID = courses.coursestdID
						where planID=".$planID." AND planStatus='Active' AND courseStatus='Active' AND subscriptionplan.streamID = courses.streamID" ;
					$connection = Yii::$app->getDb();
					$command = $connection->createCommand($sql);
					$result = $command->queryAll();	
					
					if(count($result)>0)
					{
					   for($i=0;count($result)>$i; $i++) 
					   { 
							$Usercourses = new Usercourses;
							$Usercourses->userID = $loginuserID;
							$Usercourses->courseID= $result[$i]["courseID"];
							$Usercourses->save(false);
					   }
					}
					
					
					//List of course
					$resSubscription = $Usersubscriptions->GetUserSubscriptionplanList($loginuserID,$page,$pagesize);
					$result[0]['data'] = $resSubscription;
					$result[0]['status'] = 'true';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","SUBSCRIPTION_ADDED");									

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	/*-------------- User Home Sreen Data Get Code End -----------------*/
}
