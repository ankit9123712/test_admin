<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Userexamquestions;
use backend\modules\api\v1\models\Userexams;
use backend\modules\api\v1\models\Exams;
use backend\modules\api\v1\models\Grade;
use backend\modules\api\v1\models\Settings;
use backend\modules\api\v1\models\Rewardshistory;
use Yii;
class UserexamsController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		return $result;
	}
	public function actionGetQuestions()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				$coursesubjID=0;
				$courseID=0;		
				$coursechapterIDs="";	
				$examType="";
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) )
				{
					
					$db = \Yii::$app->getDb();
					
					if($userexamType!="Practice")
					{
						$cmd = $db->createCommand("CALL spGetUserExamQues(:pCourseID, :pCoursesubjID,:pCoursechapterIDs, :pUserID)") 
						  ->bindValue(':pCourseID' , $courseID )
						  ->bindValue(':pCoursesubjID', $coursesubjID)
						  ->bindValue(':pCoursechapterIDs', $coursechapterIDs)
						  ->bindValue(':pUserID', $loginuserID);	
$resultSet = $cmd->query();  
					
					$array1 = $resultSet->readAll();
					$resultSet->nextResult();						  
					}
					else
					{
						$exams = new Exams;
						$examRes = $exams->getPracticeLevelExams($courseID,$coursesubjID,$coursechapterIDs,$loginuserID) ;
						//print_r($examRes);die;
						if(!empty($examRes))
						{
							extract($examRes[0]);
							$cmd = $db->createCommand("CALL spGetUserPracticeQue(:pUserID, :pCourseID,:pCoursesubjID, :pCoursechapterID, :pExamName, :pExamCorrectAnswer,:pExamWrongAnswer, :pexamTotalQs,:pExamQualifyingMarks, :pexamQsL1,:pexamQsL2, :pexamQsL3)") 
							  ->bindValue(':pCourseID' , $courseID )
							  ->bindValue(':pCoursesubjID', $coursesubjID)
							  ->bindValue(':pCoursechapterID', $coursechapterIDs)
							  ->bindValue(':pUserID', $loginuserID)						  
							  ->bindValue(':pExamName' , $examName )
							  ->bindValue(':pExamCorrectAnswer', $examCorrectAnswer)
							  ->bindValue(':pExamWrongAnswer', $examWrongAnswer)
							  ->bindValue(':pexamTotalQs', $examTotalQs)
							  ->bindValue(':pExamQualifyingMarks' , $examQualifyingMarks )
							  ->bindValue(':pexamQsL1', $examQsL1)
							  ->bindValue(':pexamQsL2', $examQsL2)
							  ->bindValue(':pexamQsL3', $examQsL3);
							  
							  $resultSet = $cmd->query();  
					
								$array1 = $resultSet->readAll();
								$resultSet->nextResult();
						}
						else						
						{
							$array1 = array();
						}
						//echo $cmd->getRawSql();die;

					}
					/*$resultSet = $cmd->query();  
					
					$array1 = $resultSet->readAll();
					$resultSet->nextResult();*/
					if(!empty($array1))
					{
						$result[0]['data'] = $array1;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
						
						
						$Userexams = Userexams::find()->where("userexamID = '".$array1[0]['userexamID']."'")->one();
						$Userexams->userexamType = $userexamType;
						$Userexams->save();
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionPostAnswers()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				$coursesubjID=0;
				$courseID=0;		
				$coursechapterIDs="";	
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($userexamID) && !empty($userexamQualifyingMarks) && !empty($userexamCorrectAnswer) && !empty($userexamTotalQs) && !empty($answers) )
				{
					
					$queAnsweredCorrect="";
					$queMarks="";
					$totalMarks=0;
					$userexamPercentage=0;
					$userexamStatus="";
					$userexamAttempted	=0;
					$userexamCorrect	=0;
					$userexamIncorrect	=0;
					$userexamSkipped	=0;
	
					//print_r($answers);die;
					
					for($i=0;$i<count($answers);$i++) 
					{ 
						
						$Userexamquestions = Userexamquestions::find()->where("queID = '".$answers[$i]['queID']."'")->one();
						$Userexamquestions->queFlag = $answers[$i]['queFlag'];										
						$Userexamquestions->queReadTime = @$answers[$i]['queReadTime'];
						$Userexamquestions->queAnswered = $answers[$i]['queAnswered'];
						$Userexamquestions->queAnsweredOption = @$answers[$i]['queAnsweredOption'];
						//echo @$answers[$i]['queAnsweredOption']."<BR>";
						if($answers[$i]['queAnswered']=="Yes")
						{
							$userexamAttempted++;
							if($answers[$i]['queAnsweredOption'] != $answers[$i]['queCorrectAns'])
							{
								$queAnsweredCorrect = "No";
								$queMarks = $userexamWrongAnswer;
								$totalMarks += $queMarks;
								$userexamIncorrect++;
								
							}	
							else
							{
								$queAnsweredCorrect = "Yes";
								$queMarks = $userexamCorrectAnswer;
								$totalMarks += $queMarks;
								$userexamCorrect++;
							}
						}
						else
						{
							$queAnsweredCorrect = "No";
							$queMarks = $userexamWrongAnswer;
							$totalMarks += $queMarks;
						}
						
						$Userexamquestions->queMarks = $queMarks;
						$Userexamquestions->queAnsweredCorrect = $queAnsweredCorrect;
						
						$Userexamquestions->save();
						$Errors_OrderDetails = $Userexamquestions->getErrors();
						if(!empty($Errors_OrderDetails))
						{
						//echo $answers[$i]['queID'];
						//print_r($Errors_OrderDetails);die;
						}
					}
					
					$userexamPercentage = ($totalMarks*100) / ($userexamTotalQs*$userexamCorrectAnswer);
					if($totalMarks<$userexamQualifyingMarks)
					{
						$userexamStatus = "Fail";
					}
					else
					{
						$userexamStatus = "Pass";
					}
					$userexamSkipped = $userexamTotalQs-$userexamAttempted;
					
					// find grade
					/*$myGrade="";
					$myRank="";
					 $sql = "select gradeName from grade where ".$userexamPercentage." between gradeMin and gradeMax";				   
					$connection = Yii::$app->getDb();
					$command = $connection->createCommand($sql);
					$resGrade = $command->queryAll();
					if(count($resGrade)>0)
					{
						$myGrade = $resGrade[0]["gradeName"];
					}
					// calculate rank
					$sql = "select userexamID,userID from userexams where examID='".$examID."' order by userexamMarks DESC";				   
					$connection = Yii::$app->getDb();
					$command = $connection->createCommand($sql);
					$resRank = $command->queryAll();
					if(count($resRank)>0)
					{
						for($i=0;count($resRank)>$i; $i++) 
						{
							if($loginuserID==$resRank[0]["userID"])
							{
								$myRank=$i;
							}
							else
							{
								$Userexams = Userexams::find()->where("userexamID = '".$resRank[0]["userexamID"]."'")->one();
								$Userexams->userexamRank = $i;
								$Userexams->save();
							}								
							
						}
					}*/
					//Get reward points
					$Settings = new Settings;
					$Settings_Res = $Settings->GetUserSettings();
					
					
						
					
					
					$Userexams = Userexams::find()->where("userexamID = '".$userexamID."'")->one();
					$Userexams->userexamAttempted = $userexamAttempted;
					$Userexams->userexamCorrect = $userexamCorrect;
					$Userexams->userexamIncorrect = $userexamIncorrect;
					$Userexams->userexamSkipped = $userexamSkipped;
					if($userexamType=="Practice")
						$Userexams->userexamRewardPoint = $Settings_Res[0]["settingsPracticeTestRewards"];
					else
						$Userexams->userexamRewardPoint = $Settings_Res[0]["settingsMyTestRewards"];
					$Userexams->userexamPercentage = $userexamPercentage;
					$Userexams->userexamStatus = $userexamStatus;
//					$Userexams->userexamGrade = $myGrade;
//					$Userexams->userexamRank = $myRank;
					$Userexams->userexamType = $userexamType;
					$Userexams->userexamGiven = "Yes";
					
					$Userexams->save();
					$Errors_OrderDetails = $Userexams->getErrors();
						//print_r($Errors_OrderDetails);
					if(!empty($Errors_OrderDetails))
					{
					//echo $userexamID;
					//print_r($Errors_OrderDetails);die;
					}	

					//process rewards history
					$Rewardshistory = new Rewardshistory;
					$Rewardshistory->userID = $loginuserID;
					$Rewardshistory->rewardType = "Test";
					$Rewardshistory->rewardSubType = $userexamType;
					$Rewardshistory->rewardTitle = $Userexams->userexamName;
					
					$Rewardshistory->rewardPoints = $Userexams->userexamRewardPoint;
					$Rewardshistory->rewardDate = date('Y-m-d');
					$Rewardshistory->rewardNote = "Points earned for attempting ".$Userexams->userexamName;
					$Rewardshistory->save();
					
					$db = \Yii::$app->getDb();
					//$db->pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY,true);
					$cmd = $db->createCommand("CALL spUpateGradeRank(:pUserexamPercentage, :pUserexamID,:pUserID)") 
                      ->bindValue(':pUserexamPercentage' , $userexamPercentage )
                      ->bindValue(':pUserexamID', $userexamID)
					  ->bindValue(':pUserID', $loginuserID);					
					  
					$resultSet = $cmd->query();  
					
					$array1 = $resultSet->readAll();
					$resultSet->nextResult();
					
					$res = $Userexams->GetResult($userexamID);  
					if(!empty($res))
					{
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	
	public function actionGetResults()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				$coursesubjID=0;
				$courseID=0;		
				$coursechapterIDs="";	
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($userexamType) )
				{
					
					$Userexams = new Userexams; //($userexamID=0,$userexamType="",$page="0",$pagesize="10",$loginuserID=0)
					$res = $Userexams->GetResult(0,$userexamType,$page,$pagesize,$loginuserID);  
					if(!empty($res))
					{
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						
						/*Rank & AVG*/
						// overall rank, 
						$Exams = new Exams;
						$totalStudent =0;
						$avgRank = 0;
						$sql = "SELECT planID from subscriptionplan 
	inner join users on users.streamID = subscriptionplan.streamID AND users.coursestdID = subscriptionplan.coursestdID
	WHERE users.userID=".$loginuserID;				   
						$connection = Yii::$app->getDb();
						$command = $connection->createCommand($sql);
						$resplanID = $command->queryAll();
						
						if(count($resplanID) >0 )
						{
							
							$plans="";
							for($i=0;count($resplanID)>$i; $i++) 
						   {
							   if($plans=="")
								   $plans = $resplanID[$i]["planID"];
							   else
								   $plans .= ",".$resplanID[$i]["planID"];								   
						   }
							
							$sql = "SELECT distinct userID from usersubscriptions WHERE planID in (".$plans.")";				   
							$connection = Yii::$app->getDb();
							$command = $connection->createCommand($sql);
							$resTotalStudent = $command->queryAll();
							$totalStudent=count($resTotalStudent);						
						}
						$sql = "select avg(userexamRank) as avgRank from userexams where userID='".$loginuserID."' AND userexamType='Scheduled' group by userexamType";				   
						$connection = Yii::$app->getDb();
						$command = $connection->createCommand($sql);
						$resRank = $command->queryAll();
						if(count($resRank) >0 )
						{
							$avgRank = $resRank[0]["avgRank"];
						}
						
						// exams attempted
						$resTotalExams = $Exams->GetMyTest(0,1000,$loginuserID);
						$sql = "select count(userexamID) as cnt from userexams where userID='".$loginuserID."' AND userexamType='Scheduled' ";				   
						$connection = Yii::$app->getDb();
						$command = $connection->createCommand($sql);
						$resCnt = $command->queryAll();
						
						
						$result[0]['overallrank'] = $avgRank;	
						$result[0]['overallrankoutof'] = $totalStudent;	
						$result[0]['examsAttempted'] = count($resCnt);
						$result[0]['examsAttemptedoutoff'] = count($resTotalExams);
						
						
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
					}
					else
						{
							$result[0]['status'] = 'false';
					$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
						}
					 

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionGetResultDetails()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($userexamID) )
				{
					
					$Userexams = new Userexams; //($userexamID=0,$userexamType="",$page="0",$pagesize="10",$loginuserID=0)
					$res = $Userexams->GetResult($userexamID,"",$page,$pagesize,$loginuserID,"Yes"); 
					if(!empty($res))
					{						
						$result[0]['data'] = $res;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
					}
					 

				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	

}
