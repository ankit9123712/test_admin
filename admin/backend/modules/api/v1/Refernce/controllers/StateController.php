<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\State;
class StateController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
       // $result[0]['message'] = messages_constant::JSON_ERROR;
        return $result;
    }

     public function actionGetStateList()
    {
        $Apilog = new Apilog; 

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
        $postParams = \Yii::$app->request->post();    
        
        if ($Apilog->is_json($postParams["json"]))
        {           
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
            if(count($jsonParams[0])>0)
            {  
               $countryID = "0";
               extract($jsonParams[0]);    
               if(!empty($apiType))
               {
	                if(!empty($languageID))
	                {
		                if(!empty($apiVersion))
		                {

		                   $State = new State;
		                   $res = $State->GetStateList($countryID);  
		                   if(!empty($res))
		                   {
		                        $result[0]['data'] = $res;
		                        $result[0]['status'] = 'true';
		                        $result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");	
		                   }
		                   else
		                   {
		                        $result[0]['status'] = 'false';
		                        $result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");
		                   }
		               
		                }
		                else
		                {
		                    $Required = 'apiVersion';
		                    $result[0]['status'] = 'false';
		                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
		                }
	                }
	                else
	                {
	                    $Required = 'languageID';
	                    $result[0]['status'] = 'false';
	                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
	                }
                }
                else
                {
                    $Required = 'apiType';
                    $result[0]['status'] = 'false';
                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
                }

            }
        }
        $requestString= $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
        return $result; 
			
    }

}
