<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Notification;
class NotificationController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
       // $result[0]['message'] = messages_constant::JSON_ERROR;
        return $result;
    }

    public function actionGetNotificationList()
    {
        $Apilog = new Apilog; 
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
        $postParams = \Yii::$app->request->post();    
        
        if ($Apilog->is_json($postParams["json"]))
        {           
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
            if(count($jsonParams[0])>0)
            {  
            	$page="0";
            	$pagesize = "20";

               extract($jsonParams[0]);    
               if(!empty($apiType))
               {
	                if(!empty($languageID))
	                {
		                if(!empty($apiVersion))
		                {
		                	if(!empty($loginuserID))
		                	{

			                   $Notification = new Notification;
			                   $res = $Notification->GetNotificationList($loginuserID,$page,$pagesize);  
			                   if(!empty($res))
			                   {
			                        $result[0]['data'] = $res;
			                        $result[0]['status'] = 'true';
			                        $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"RECORDFOUND");
			                   }
			                   else
			                   {
			                        $result[0]['status'] = 'false';
			                        $result[0]['message'] = $Apilog->GetErrorSuccessMsg($languageID,"NORECORDFOUND");
			                   }
		                    }
			                else
			                {
			                    $Required = 'loginuserID';
			                    $result[0]['status'] = 'false';
			                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			                }
		               
		                }
		                else
		                {
		                    $Required = 'apiVersion';
		                    $result[0]['status'] = 'false';
		                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
		                }
	                }
	                else
	                {
	                    $Required = 'languageID';
	                    $result[0]['status'] = 'false';
	                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	                }
                }
                else
                {
                    $Required = 'apiType';
                    $result[0]['status'] = 'false';
                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
                }

            }
        }
        $requestString= $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
        return $result; 
			
    }

    public function actionUpdateNotificationReadStatus()
    {
        $Apilog = new Apilog; 
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
        $postParams = \Yii::$app->request->post();    
        
        if ($Apilog->is_json($postParams["json"]))
        {           
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
            if(count($jsonParams[0])>0)
            {  
               extract($jsonParams[0]);    
               if(!empty($apiType))
               {
	                if(!empty($languageID))
	                {
		                if(!empty($apiVersion))
		                {
		                	if(!empty($loginuserID))
		                	{
		                		if(!empty($notificationID))
		                		{
				                   $Notification = new Notification;
				                   $result = $Notification->UpdateNotificationReadStatus($loginuserID,$notificationID,$languageID);  
			                    }
				                else
				                {
				                    $Required = 'notificationID';
				                    $result[0]['status'] = 'false';
				                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				                }
			                 }
			                else
			                {
			                    $Required = 'loginuserID';
			                    $result[0]['status'] = 'false';
			                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			                }
		               
		                }
		                else
		                {
		                    $Required = 'apiVersion';
		                    $result[0]['status'] = 'false';
		                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
		                }
	                }
	                else
	                {
	                    $Required = 'languageID';
	                    $result[0]['status'] = 'false';
	                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	                }
                }
                else
                {
                    $Required = 'apiType';
                    $result[0]['status'] = 'false';
                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
                }

            }
        }
        $requestString= $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
        return $result; 
			
    }

     public function actionDeleteNotification()
    {
        $Apilog = new Apilog; 
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
        $postParams = \Yii::$app->request->post();    
        
        if ($Apilog->is_json($postParams["json"]))
        {           
            $jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
            if(count($jsonParams[0])>0)
            {  
               extract($jsonParams[0]);    
               if(!empty($apiType))
               {
	                if(!empty($languageID))
	                {
		                if(!empty($apiVersion))
		                {
		                	if(!empty($loginuserID))
		                	{
		                		if(!empty($notificationID))
		                		{
				                   $Notification = new Notification;
				                   $result = $Notification->DeleteNotification($loginuserID,$notificationID,$languageID);  
			                    }
				                else
				                {
				                    $Required = 'notificationID';
				                    $result[0]['status'] = 'false';
				                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
				                }
			                 }
			                else
			                {
			                    $Required = 'loginuserID';
			                    $result[0]['status'] = 'false';
			                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
			                }
		               
		                }
		                else
		                {
		                    $Required = 'apiVersion';
		                    $result[0]['status'] = 'false';
		                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
		                }
	                }
	                else
	                {
	                    $Required = 'languageID';
	                    $result[0]['status'] = 'false';
	                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
	                }
                }
                else
                {
                    $Required = 'apiType';
                    $result[0]['status'] = 'false';
                    $result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg($languageID,"ISMISSING");
                }

            }
        }
        $requestString= $postParams["json"];
        $Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
        return $result; 
			
    }


}
