<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Chapterlike;
use backend\modules\api\v1\models\Coursechapters;
class ChapterlikeController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		return $result;
	}
	

	public function actionChapterLike()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
							
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($coursechapterID ))
				{
					$Chapterlike = new Chapterlike;
					$Chapterlike_Res = Chapterlike::find()->where('userID = "'.$loginuserID.'" AND coursechapterID="'.$coursechapterID.'"')->one();
					//List of course
					if(!empty($Chapterlike_Res))
					{
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_LIKE");
					}
					else
					{
						$Chapterlike->userID = $loginuserID;
						$Chapterlike->coursechapterID = $coursechapterID;
						$Chapterlike->save();
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_LIKE");
						
					}
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
	public function actionChapterUnLike()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
							
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID) && !empty($coursechapterID) )
				{
					$Chapterlike = new Chapterlike;
					$Chapterlike_Res = Chapterlike::find()->where('userID = "'.$loginuserID.'" AND coursechapterID="'.$coursechapterID.'"')->one();
					//List of course
					if(!empty($Chapterlike_Res))
					{
						//print_r($Chapterlike_Res);
						//die;
						//RestoFoods::deleteAll(['AND', 'restaurant_id = :restaurant_id', ['NOT IN', 'food_id', [1,2]]], [':restaurant_id' => $postData['resto_id']]);

						$Delete = Chapterlike::deleteAll('chapterlikeID = :chapterlikeID', [':chapterlikeID' => $Chapterlike_Res->chapterlikeID]);
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_UNLIKE");
					}
					else
					{
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","CHAPTER_UNLIKE");
						
					}
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}

	public function actionMyLikedChapters()
	{

		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				$isFavorite = "";	
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID)  )
				{
					$Coursechapters = new Coursechapters;
					//List of course
					$resGetCoursechaptersList = $Coursechapters->GetCoursechaptersList(0,$loginuserID,"True");
					if(!empty($resGetCoursechaptersList))
					{
						$result[0]['data'] = $resGetCoursechaptersList;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
		
	}
}
