<?php

namespace backend\modules\api\v1\controllers;
use backend\modules\api\v1\models\Apilog;
use backend\modules\api\v1\models\Rewardshistory;
class RewardshistoryController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
	public function actionIndex()
	{
		return $this->render('index');
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result[0]['status'] = "false";
		//$result[0]['message'] = messages_constant::JSON_ERROR;
		return $result;
	}
	public function actionMyRewards()
	{
		$Apilog = new Apilog; 

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;            
		$postParams = \Yii::$app->request->post();    
		
		if ($Apilog->is_json($postParams["json"]))
		{           
			$jsonParams = \yii\helpers\Json::decode($postParams["json"]);           
			if(count($jsonParams[0])>0)
			{  
				$page = "0";
				$pagesize = "10";	
				
				extract($jsonParams[0]);    
				if(!empty($apiType) && !empty($languageID) && !empty($apiVersion) && !empty($loginuserID)  )
				{
					$Rewardshistory = new Rewardshistory;
					//List of course
					$resGetExams = $Rewardshistory->GetRewardsList($loginuserID,$page,$pagesize,$rewardType);
					$resGetTotal = $Rewardshistory->GetRewardsTotal($loginuserID,$page,$pagesize,$rewardType);
					if(!empty($resGetExams))
					{
						$result[0]['data'] = $resGetExams;
						$result[0]['total'] = $resGetTotal;
						$result[0]['status'] = 'true';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","RECORDFOUND");									
					}
					else
					{
						$result[0]['status'] = 'false';
						$result[0]['message'] = $Apilog->GetErrorSuccessMsg("1","NORECORDFOUND");	
					}
				}
				else
				{
					$Required = 'Required fileds';
					$result[0]['status'] = 'false';
					$result[0]['message'] = $Required.$Apilog->GetErrorSuccessMsg("1","ISMISSING");
				}


			}
		}
		$requestString= $postParams["json"];
		$Apilog->AddApiLog(\Yii::$app->controller->action->id,$requestString,$result,$apiType,$apiVersion);                       
		return $result; 
	}

}
