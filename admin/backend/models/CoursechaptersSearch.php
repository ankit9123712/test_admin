<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Coursechapters;

/**
 * CoursechaptersSearch represents the model behind the search form of `backend\models\Coursechapters`.
 */
class CoursechaptersSearch extends Coursechapters
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coursechapterNo'], 'integer'],
            [['coursechapterID', 'courseID', 'coursesubjID','coursechapterName', 'coursechapterDescription', 'coursechapterFile', 'coursechapterFileURL', 'coursechapterStatus', 'coursechapterCreatedDate','chapterfile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Coursechapters::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['pageSize' => $pagination ],
        ]);

        $this->load($params);
        $query->JoinWith('courses');
        $query->JoinWith('coursesubjects');
        //$query->JoinWith('chapterfiles');
		
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'coursechapterID' => $this->coursechapterID,
           // 'courseID' => $this->courseID,
           // 'coursesubjID' => $this->coursesubjID,
            'coursechapterNo' => $this->coursechapterNo,
            'coursechapterCreatedDate' => $this->coursechapterCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'coursechapterName', $this->coursechapterName])
            ->andFilterWhere(['like', 'courses.courseName', $this->courseID])
            ->andFilterWhere(['like', 'coursesubjects.coursesubjName', $this->coursesubjID])
			->andFilterWhere(['like', 'coursechapterDescription', $this->coursechapterDescription])
            ->andFilterWhere(['like', 'coursechapterFile', $this->coursechapterFile])
            ->andFilterWhere(['like', 'coursechapterFileURL', $this->coursechapterFileURL])
            ->andFilterWhere(['like', 'coursechapterStatus', $this->coursechapterStatus]);
		$query->orderby('coursechapterID DESC');
        return $dataProvider;
    }
}
