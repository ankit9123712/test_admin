<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property string $notificationID
 * @property integer $notificationReferenceKey
 * @property integer $userID
 * @property string $userDeviceType
 * @property string $userDeviceID
 * @property string $notificationType
 * @property string $notificationTitle
 * @property string $notificationMessageText
 * @property string $notificationResponse
 * @property string $notificationSendDate
 * @property string $notificationSendTime
 * @property string $notificationStatus
 * @property string $notificationReadStatus
 * @property string $notificationCreatedDate
 * @property string $notificationData
 * @property integer $dealerID
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID','userDeviceType','notificationTitle'], 'required'],
            [['notificationReferenceKey', 'userID', 'dealerID'], 'integer'],
            [['notificationSendDate', 'notificationSendTime', 'notificationCreatedDate'], 'safe'],
            [['notificationStatus', 'notificationReadStatus', 'notificationData'], 'string'],
            [['userDeviceType'], 'string', 'max' => 10],
            [['userDeviceID', 'notificationTitle'], 'string', 'max' => 500],
            [['notificationType'], 'string', 'max' => 50],
            [['notificationMessageText', 'notificationResponse'], 'string', 'max' => 4000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notificationID' => 'ID',
            'notificationReferenceKey' => 'Reference Key',
            'userID' => 'User',
            'userDeviceType' => 'Device Type',
            'userDeviceID' => 'User Device ID',
            'notificationType' => 'Notification Type',
            'notificationTitle' => 'Title',
            'notificationMessageText' => 'Message Text',
            'notificationResponse' => 'Response',
            'notificationSendDate' => 'Send Date',
            'notificationSendTime' => 'Send Time',
            'notificationStatus' => 'Status',
            'notificationReadStatus' => 'Read Status',
            'notificationCreatedDate' => 'Created Date',
            'notificationData' => 'Notification Data',
            'dealerID' => 'Dealer',
        ];
    }
	public function getUsers()
    {
        return $this->hasOne(Users::className(), ['userID' => 'userID']);
    }
}
