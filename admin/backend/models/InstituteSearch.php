<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Institute;

/**
 * InstituteSearch represents the model behind the search form of `app\models\Institute`.
 */
class InstituteSearch extends Institute
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instituteID', 'countryID', 'stateID', 'cityID'], 'integer'],
            [['instituteName', 'instituteContactName', 'instituteEmailExtension', 'institutePhone', 'instituteAddress', 'institutePincode', 'instituteStatus', 'instituteCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Institute::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'instituteID' => $this->instituteID,
            'countryID' => $this->countryID,
            'stateID' => $this->stateID,
            'cityID' => $this->cityID,
            'instituteCreatedDate' => $this->instituteCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'instituteName', $this->instituteName])
            ->andFilterWhere(['like', 'instituteContactName', $this->instituteContactName])
            ->andFilterWhere(['like', 'instituteEmailExtension', $this->instituteEmailExtension])
            ->andFilterWhere(['like', 'institutePhone', $this->institutePhone])
            ->andFilterWhere(['like', 'instituteAddress', $this->instituteAddress])
            ->andFilterWhere(['like', 'institutePincode', $this->institutePincode])
            ->andFilterWhere(['like', 'instituteStatus', $this->instituteStatus]);
		$query->orderby('instituteID DESC');
        return $dataProvider;
    }
}
