<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cmspage".
 *
 * @property integer $cmspageID
 * @property string $cmspageConstantCode
 * @property string $cmspageName
 * @property string $cmspageContents
 * @property string $cmspageIsSystemPage
 * @property string $cmspageStatus
 * @property string $cmspageCreatedDate
 */
class Cmspage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cmspage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cmspageConstantCode', 'cmspageContents','cmspageName'], 'required'],
            [['cmspageContents', 'cmspageIsSystemPage', 'cmspageStatus'], 'string'],
            [['cmspageCreatedDate'], 'safe'],
            [['cmspageConstantCode'], 'string', 'max' => 20],
            [['cmspageName'], 'string', 'max' => 100,'min' => 3],
            [['cmspageName'], 'unique'],
            [['cmspageConstantCode'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cmspageID' => 'ID',
            'cmspageConstantCode' => 'Constant Code',
            'cmspageName' => 'Name',
            'cmspageContents' => 'Contents',
            'cmspageIsSystemPage' => 'Is System Page',
            'cmspageStatus' => 'Status',
            'cmspageCreatedDate' => 'Created Date',
        ];
    }
}
