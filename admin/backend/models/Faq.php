<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property integer $faqID
 * @property integer $faqtypeID
 * @property string $faqQuestion
 * @property string $faqAnswer
 * @property integer $faqPosition
 * @property string $faqStatus
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['faqtypeID', 'faqQuestion', 'faqAnswer', 'faqStatus'], 'required'],
            [['faqtypeID', 'faqPosition'], 'integer'],
            [['faqAnswer', 'faqStatus'], 'string'],
            [['faqQuestion'], 'string', 'max' => 200,'min' => 2],
            [['faqtypeID', 'faqQuestion'], 'unique', 'targetAttribute' => ['faqtypeID', 'faqQuestion'], 'message' => 'The combination of Faq Type and Faq Question has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'faqID' => 'ID',
            'faqtypeID' => 'Faq Type',
            'faqQuestion' => 'Question',
            'faqAnswer' => 'Answer',
            'faqPosition' => 'Position',
            'faqStatus' => 'Status',
        ];
    }
	public function getFaqtype()
    {
        return $this->hasOne(Faqtype::className(),['faqtypeID'=>'faqtypeID']);
    }
}
