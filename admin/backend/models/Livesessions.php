<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "livesessions".
 *
 * @property int $liveID
 * @property int $facultyID
 * @property string $liveName
 * @property string $liveBatch
 * @property string $liveStartDate
 * @property string $liveStartTime
 * @property string $liveEndTime
 * @property int $moduleID
 * @property string $liveMeetingID
 * @property string $liveMeetingPassword
 * @property string $liveMeetingURL
 * @property string $liveStatus
 * @property string $liveCreatedDate
 */
class Livesessions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'livesessions';
    }
	public $sesRecurring, $sesRecurrance, $sesRecurranceEvery, $sesRecurranceDay , $sesEndDate;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['facultyID', 'liveName', 'liveBatch', 'liveStartDate', 'liveStartTime', 'liveEndTime', 'moduleID', 'liveMeetingID', 'liveMeetingPassword'], 'required'],
            [['facultyID', 'moduleID'], 'integer'],
            [['liveBatch', 'liveStatus'], 'string'],
            [['liveStartDate', 'liveStartTime', 'liveEndTime', 'liveCreatedDate','sesRecurring', 'sesRecurrance', 'sesRecurranceEvery', 'sesRecurranceDay' , 'sesEndDate'], 'safe'],
            [['liveName'], 'string', 'max' => 100],
            [['liveMeetingID'], 'string', 'max' => 15],
            [['liveMeetingPassword'], 'string', 'max' => 25],
            [['liveMeetingURL'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'liveID' => 'ID',
            'facultyID' => 'Faculty',
            'liveName' => 'Session Name',
            'liveBatch' => 'Batch',
            'liveStartDate' => 'Start Date',
            'liveStartTime' => 'Start Time',
            'liveEndTime' => 'End Time',
            'moduleID' => 'Module',
            'liveMeetingID' => 'Meeting ID',
            'liveMeetingPassword' => 'Meeting Password',
            'liveMeetingURL' => 'Meeting Url',
            'liveStatus' => 'Status',
            'liveCreatedDate' => 'Created Date',
			'sesRecurring' => 'Recurring meeting?',
            'sesRecurranceEvery' => 'Repeat everyRecurrence',
            'sesRecurranceDay' => 'Occurs on',
            'sesEndDate' => 'End Date By',
			'sesRecurrance' => 'Recurrence',
        ];
    }
	public function getCoursemodule()
    {
        return $this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
	public function getFaculty()
    {
        return $this->hasOne(Faculty::className(),['facultyID'=>'facultyID']);
    }
}
