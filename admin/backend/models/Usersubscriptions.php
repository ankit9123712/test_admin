<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "usersubscriptions".
 *
 * @property integer $usersubscriptionID
 * @property integer $userID
 * @property integer $subscriptionID
 * @property string $usersubscriptionAmount
 * @property string $usersubscriptionCoponCode
 * @property string $usersubscriptionStartDate
 * @property string $usersubscriptionEndDate
 * @property string $usersubscriptionCreatedDate
 */
class Usersubscriptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usersubscriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'subscriptionID', 'usersubscriptionAmount', 'usersubscriptionCoponCode'], 'required'],
            [['userID', 'subscriptionID'], 'integer'],
            [['usersubscriptionAmount'], 'number'],
            [['usersubscriptionStartDate', 'usersubscriptionEndDate', 'usersubscriptionCreatedDate'], 'safe'],
            [['usersubscriptionCoponCode'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usersubscriptionID' => 'ID',
            'userID' => 'User',
            'subscriptionID' => 'Sub Scription',
            'usersubscriptionAmount' => 'Amount',
            'usersubscriptionCoponCode' => 'Copon Code',
            'usersubscriptionStartDate' => 'Start Date',
            'usersubscriptionEndDate' => 'End Date',
            'usersubscriptionCreatedDate' => 'Created Date',
        ];
    }
	
	public function getUsers()
    {
        return $this->hasOne(Users::className(),['userID'=>'userID']);
    }
	public function getSubscriptionplan()
    {
        return $this->hasOne(Subscriptionplan::className(),['planID'=>'subscriptionID']);
    }
}
