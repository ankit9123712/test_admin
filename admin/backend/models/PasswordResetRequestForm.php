<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use common\models\User;
use backend\models\Settings;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $Admin_Email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['Admin_Email', 'trim'],
            ['Admin_Email', 'required'],
            ['Admin_Email', 'email'],
            ['Admin_Email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['Admin_Status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendResetLink()
    {
        $Admin_Res = Admin::find()->where('Admin_Email = "'.$this->Admin_Email.'"')->one();

        if(!empty($Admin_Res))
        {
			$settings = new Settings();
			$settings_arr = Settings::find()->select('settingsUserResetPinLinkExpHr')->where(['settingsID' => '1'])->asArray()->all();

			$settingsUserResetPinLinkExpHr = $settings_arr[0]['settingsUserResetPinLinkExpHr'];
			$Admin_Exp_CRP = date("Y-m-d H:i:s", strtotime('+'.$settingsUserResetPinLinkExpHr.' hours'));
			
			$Admin_Code_Reset_Password = $this->GenarateRandomString(20);
			
			$Admin_Res->Admin_Exp_CRP = $Admin_Exp_CRP;
            $Admin_Res->Admin_Code_Reset_Password = $Admin_Code_Reset_Password;
            $Admin_Res->save();
            $Errors = $Admin_Res->getErrors();
            if(empty($Errors))
            {
				 $arr = array();
				 $arr["Admin_Name"]=$Admin_Res['Admin_Name'];
				 $arr["Admin_Email"]=$Admin_Res['Admin_Email'];
				 $arr["Admin_Code_Reset_Password"]=$Admin_Code_Reset_Password;
				 $Template = new Template();
				 $Template->Sendmail("478512",$arr,$Admin_Res['Admin_Email'],$Admin_Res['Admin_Name']);
				  //Sendmail($templateConstantID=0,$fieldArray="",$toEmail,$toName)
                  Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                  return true;				
			}
            else
            {
               Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
                // return Yii::$app->response->redirect(Url::to(['site/login']));
            }

        }
        else
        {

            Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
             // return Yii::$app->response->redirect(Url::to(['site/login']));   
        }
       

        

        // return $this->Admin_Email;
   
    }


     public function GenarateRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}
