<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Cmspage;

/**
 * CmspageSearch represents the model behind the search form of `backend\models\Cmspage`.
 */
class CmspageSearch extends Cmspage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cmspageID'], 'integer'],
            [['cmspageConstantCode', 'cmspageName', 'cmspageContents', 'cmspageIsSystemPage', 'cmspageStatus', 'cmspageCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cmspage::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cmspageID' => $this->cmspageID,
            'cmspageCreatedDate' => $this->cmspageCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'cmspageConstantCode', $this->cmspageConstantCode])
            ->andFilterWhere(['like', 'cmspageName', $this->cmspageName])
            ->andFilterWhere(['like', 'cmspageContents', $this->cmspageContents])
            ->andFilterWhere(['like', 'cmspageIsSystemPage', $this->cmspageIsSystemPage])
            ->andFilterWhere(['like', 'cmspageStatus', $this->cmspageStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('cmspageID DESC');
        return $dataProvider;
    }
}
