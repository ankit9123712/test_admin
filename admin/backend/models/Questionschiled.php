<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property string $queID
 * @property int $courseID
 * @property int $coursesubjID
 * @property int $coursechapterID
 * @property string $queType
 * @property int $queDifficultyevel
 * @property string $queQuestion
 * @property string $queSolution
 * @property string $queDisplayType
 * @property string $queStatus
 * @property string $queOption1
 * @property string $queOption2
 * @property string $queOption3
 * @property string $queOption4
 * @property string $queCorrectAns
 */
class Questionschiled extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['queType', 'queDifficultyevel', 'queQuestion', 'queSolution', 'queDisplayType', 'queStatus',], 'required'],
            [['queDifficultyevel'], 'integer'],
            [['queType', 'queDisplayType', 'queStatus'], 'string'],
            //[['queQuestion', 'queSolution'], 'string', 'max' => 500],
            [['queOption1', 'queOption2', 'queOption3', 'queOption4'], 'string', 'max' => 50],
            [['queCorrectAns'], 'string', 'max' => 10],
			 [['queQuestion','queSolution'], 'string', 'max' => 300, 'min' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'queID' => 'ID',
            'courseID' => 'Course',
            'coursesubjID' => 'Course Subject',
            'coursechapterID' => 'Course Chapter',
            'queType' => 'Que Type',
            'queDifficultyevel' => 'Difficulty level',
            'queQuestion' => 'Question',
            'queSolution' => 'Solution',
            'queDisplayType' => 'Display Type',
            'queStatus' => 'Status',
            'queOption1' => 'Option1',
            'queOption2' => 'Option2',
            'queOption3' => 'Option3',
            'queOption4' => 'Option4',
            'queCorrectAns' => 'Correct Ans',
        ];
    }

}
