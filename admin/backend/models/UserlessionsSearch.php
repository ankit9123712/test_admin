<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Userlessions;

/**
 * UserlessionsSearch represents the model behind the search form of `backend\models\Userlessions`.
 */
class UserlessionsSearch extends Userlessions
{
    /**
     * @inheritdoc
     */
	
    public function rules()
    {
        return [
            [['userlessionID', 'userID', 'lessionID', 'userlessionQuestions', 'userlessionAnswered','userlessionBand','userlessionCorrectAnswers', 'userlessionWrongAnswers','cityID','stateID','moduleID','userType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Userlessions::find()->select('`userlessions`.*,users.cityID,users.stateID,lesson.moduleID');

        // add conditions that should always apply here
 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('users');
		$query->JoinWith('lesson');
		$query->join('left join','state','users.stateID = state.stateID');
		$query->join('left join','city','city.cityID = users.cityID');
		$query->join('left join','coursemodule','lesson.moduleID = coursemodule.moduleID');
		

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'userlessionID' => $this->userlessionID,
           // 'userID' => $this->userID,
           // 'lessionID' => $this->lessionID,
            'userlessionQuestions' => $this->userlessionQuestions,
            'userlessionAnswered' => $this->userlessionAnswered,
            'userlessionCorrectAnswers' => $this->userlessionCorrectAnswers,
			'userlessionBand' => $this->userlessionBand,
            'userlessionWrongAnswers' => $this->userlessionWrongAnswers,
        ]);
		
			$query->andFilterWhere(['like', 'users.userFullName', $this->userID])
            ->andFilterWhere(['like', 'coursemodule.moduleName', $this->moduleID])
			->andFilterWhere(['like', 'users.userType', $this->userType])
			->andFilterWhere(['like', 'state.stateName', $this->stateID])
			->andFilterWhere(['like', 'city.cityName', $this->cityID])
			->andFilterWhere(['like', 'lesson.LessonName', $this->lessionID]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('userlessionID DESC');
//		echo $query->createCommand()->sql;	die;

        return $dataProvider;
    }
}
