<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "notimessages".
 *
 * @property int $msgId
 * @property string $msgTitle
 * @property string $msgDescription
 * @property string $msgStatus
 * @property string $msgUserType
 * @property int $stateID
 * @property int $cityID
 * @property string $messageSendNow
 * @property string $msgDate
 * @property string $msgCreatedDate
 */
class Notimessages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notimessages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['msgTitle', 'msgDescription', 'msgStatus', 'msgDate'], 'required'],
            [['msgStatus', 'msgUserType', 'messageSendNow'], 'string'],
            [['stateID', 'cityID'], 'integer'],
            [['msgDate', 'msgCreatedDate'], 'safe'],
            [['msgTitle'], 'string', 'max' => 100],
            [['msgDescription'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'msgId' => 'Msg ID',
            'msgTitle' => 'Title',
            'msgDescription' => 'Description',
            'msgStatus' => 'Status',
            'msgUserType' => 'User Type',
            'stateID' => 'State',
            'cityID' => 'City',
            'messageSendNow' => 'Send Now',
            'msgDate' => 'Date',
            'msgCreatedDate' => 'Created Date',
        ];
    }
	public function getState()
    {
        return $this->hasOne(State::className(),['stateID'=>'stateID']);
    }
	public function getCity()
    {
        return $this->hasOne(City::className(),['cityID'=>'cityID']);
    }
}
