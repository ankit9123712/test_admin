<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Subject;

/**
 * SubjectSearch represents the model behind the search form of `app\models\Subject`.
 */
class SubjectSearch extends Subject
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subjectID'], 'integer'],
            [['subjectName', 'subjectLastMonth', 'subjectColor', 'subjectIcon', 'subjectRemarks', 'subjectStatus', 'subjectCreatedDate', 'coursestdID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subject::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
	$query->JoinWith('coursestd');
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'subjectID' => $this->subjectID,
           // 'coursestdID' => $this->coursestdID,
            'subjectCreatedDate' => $this->subjectCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'subjectName', $this->subjectName])
            ->andFilterWhere(['like', 'subjectLastMonth', $this->subjectLastMonth])
            ->andFilterWhere(['like', 'subjectColor', $this->subjectColor])
            ->andFilterWhere(['like', 'coursestd.coursestdName', $this->coursestdID])
			->andFilterWhere(['like', 'subjectIcon', $this->subjectIcon])
            ->andFilterWhere(['like', 'subjectRemarks', $this->subjectRemarks])
            ->andFilterWhere(['like', 'subjectStatus', $this->subjectStatus]);
		$query->orderby('subjectID DESC');
        return $dataProvider;
    }
}
