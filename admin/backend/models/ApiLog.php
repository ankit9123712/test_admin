<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "api_log".
 *
 * @property integer $apiID
 * @property string $apiName
 * @property string $apiIP
 * @property string $apiRequest
 * @property string $apiResponse
 * @property string $apiType
 * @property string $apiCallDate
 */
class ApiLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apilog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apiName', 'apiIP', 'apiRequest', 'apiResponse', 'apiType'], 'required'],
            [['apiRequest'], 'string'],
            [['apiCallDate'], 'safe'],
            [['apiName'], 'string', 'max' => 1000],
            [['apiIP'], 'string', 'max' => 50],
            [['apiResponse', 'apiType'], 'string', 'max' => 5000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'apiID' => 'ID',
            'apiName' => 'Name',
            'apiIP' => 'Ip',
            'apiRequest' => 'Request',
            'apiResponse' => 'Response',
            'apiType' => 'Type',
            'apiCallDate' => 'Date',
        ];
    }
}
