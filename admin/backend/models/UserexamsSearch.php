<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Userexams;

/**
 * UserexamsSearch represents the model behind the search form of `backend\models\Userexams`.
 */
class UserexamsSearch extends Userexams
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['userexamID', 'examID', 'userexamVerifiedBy', 'userID'], 'integer'],
            [['userexamID', 'examID', 'userexamVerifiedBy', 'userID','userexamDate', 'userexamGrade', 'userexamVerifiedDate', 'userexamVerifiedNotes'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Userexams::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('exams');
		$query->JoinWith('users');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'userexamID' => $this->userexamID,
            //'examID' => $this->examID,
            'userexamDate' => $this->userexamDate,
            'userexamVerifiedBy' => $this->userexamVerifiedBy,
            'userexamVerifiedDate' => $this->userexamVerifiedDate,
           // 'userID' => $this->userID,
        ]);

        $query->andFilterWhere(['like', 'userexamGrade', $this->userexamGrade])
        ->andFilterWhere(['like', 'exams.examName', $this->examID])
        ->andFilterWhere(['like', 'users.userFullName', $this->userID])
            ->andFilterWhere(['like', 'userexamVerifiedNotes', $this->userexamVerifiedNotes]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('userexamID DESC');
        return $dataProvider;
    }
}
