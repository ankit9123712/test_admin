<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "subscriptionplan".
 *
 * @property integer $planID
 * @property integer $moduleID
 * @property string $planName
 * @property string $planDescription
 * @property string $planPrice
 * @property string $planDiscountPrice
 * @property integer $planDuration
 * @property string $planStatus
 * @property string $planCreatedDate
 */
class Subscriptionplan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscriptionplan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['planName', 'planDescription', 'planPrice', 'planDiscountPrice', 'planDuration', 'planStatus'], 'required'],
            [['moduleID', 'planDuration'], 'integer'],
            [['planDescription', 'planStatus'], 'string'],
            [['planPrice', 'planDiscountPrice'], 'number'],
            [['planCreatedDate'], 'safe'],
            [['planName'], 'string', 'max' => 100,'min' => 3],
            [['moduleID', 'planName'], 'unique', 'targetAttribute' => ['moduleID', 'planName'], 'message' => 'The combination of Module I D and Plan Name has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'planID' => 'ID',
            'moduleID' => 'Module',
            'planName' => 'Name',
            'planDescription' => 'Description',
            'planPrice' => 'Price',
            'planDiscountPrice' => 'Discount Price',
            'planDuration' => 'Duration (Month)',
            'planStatus' => 'Status',
            'planCreatedDate' => 'Created Date',
        ];
    }
	
	public function getCoursemodule()
    {
        return $this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
}
