<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "subscriptiondetails".
 *
 * @property string $userFullName
 * @property string $userMobile
 * @property string $userEmail
 * @property string $planDescription
 * @property string $subscriptionStartDate
 * @property string $subscriptionEndDate
 * @property int $subscriptionID
 */
class Subscriptiondetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscriptiondetails';
    }
	public static function primaryKey()
	{
		return ['subscriptionID'];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userFullName', 'userMobile', 'userEmail', 'planDescription', 'subscriptionStartDate', 'subscriptionEndDate'], 'required'],
            [['planDescription'], 'string'],
            [['subscriptionStartDate', 'subscriptionEndDate'], 'safe'],
            [['subscriptionID'], 'integer'],
            [['userFullName', 'userEmail'], 'string', 'max' => 100],
            [['userMobile'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userFullName' => 'Name',
            'userMobile' => 'Mobile No',
            'userEmail' => 'EmailID',
            'planDescription' => 'Plan Purchased',
            'subscriptionStartDate' => 'Purchase Date',
            'subscriptionEndDate' => 'End Date',
            'subscriptionID' => 'Subscription I D',
        ];
    }
}
