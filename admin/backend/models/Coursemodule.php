<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "coursemodule".
 *
 * @property integer $moduleID
 * @property string $moduleName
 * @property string $moduleType
 * @property string $moduleRemarks
 * @property string $moduleStatus
 * @property string $moduleCreatedDate
 */
class Coursemodule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coursemodule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['moduleName', 'moduleType', 'moduleStatus'], 'required'],
            [['moduleType', 'moduleStatus'], 'string'],
            [['moduleCreatedDate'], 'safe'],
            [['moduleName'], 'string', 'max' => 100,'min' => 2],
            [['moduleRemarks'], 'string', 'max' => 200],
			[['moduleGuidelines','moduleMarking','moduleQuicktips'], 'string'],
			//['moduleName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => ' Module Name you entered is incorrect.'],
            [['moduleName', 'moduleType'], 'unique', 'targetAttribute' => ['moduleName', 'moduleType'], 'message' => 'The combination of Module Name and Module Type has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'moduleID' => 'ID',
            'moduleName' => 'Name',
            'moduleType' => 'Type',
            'moduleRemarks' => 'Remarks',
            'moduleStatus' => 'Status',
            'moduleCreatedDate' => 'Created Date',
			'moduleGuidelines'=> 'Reading Guidelines',
			'moduleMarking'=> 'Section Marking System',
			'moduleQuicktips'=> 'Quick Tips',
        ];
    }
}
