<?php

namespace backend\models;

use Yii;
use backend\models\Chapterfiles;
/**
 * This is the model class for table "coursechapters".
 *
 * @property int $coursechapterID
 * @property int $courseID
 * @property int $coursesubjID
 * @property int $coursechapterNo
 * @property string $coursechapterName
 * @property string $coursechapterDescription
 * @property string $coursechapterFile
 * @property string $coursechapterFileURL
 * @property string $coursechapterStatus
 * @property string $coursechapterCreatedDate
 */
class Coursechapters extends \yii\db\ActiveRecord
{
	
	public $chapterfileID;
	public $chapterfileFile;
	public $chapterfileFileType;
	//public $chapterfile;
	//public $Chapterfiles;
	
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coursechapters';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
		 if(isset($_REQUEST['id']))
         {
        return [
            [['courseID', 'coursesubjID', 'coursechapterNo', 'coursechapterName', 'coursechapterDescription'], 'required'],
            ['coursechapterName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'Course Name can only contain Alphabets and Spaces.'],
			[['coursechapterName'], 'string','min'=>3, 'max' => 100],
			[['courseID', 'coursesubjID', 'coursechapterNo'], 'integer'],
            [['coursechapterDescription', 'coursechapterStatus'], 'string'],
            [['coursechapterCreatedDate','coursechapeterVideoFileCount','coursechapeterContentFileCount','chapterfile'], 'safe'],
           [['coursechapterFile'], 'string', 'max' => 100],
            [['coursechapterFileURL'], 'string', 'max' => 300],
	        [['chapterfileFile'], 'file', 'extensions'=>'mp4,pdf'],
            [['courseID', 'coursesubjID', 'coursechapterName'], 'unique', 'targetAttribute' => ['courseID', 'coursesubjID', 'coursechapterName']],
        ];
		
		}
		else
		{
			
			return [
            [['courseID', 'coursesubjID', 'coursechapterNo', 'coursechapterName', 'coursechapterDescription'], 'required'],
            ['coursechapterName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'Course Name can only contain Alphabets and Spaces.'],
			[['coursechapterName'], 'string','min'=>3, 'max' => 100],
			[['courseID', 'coursesubjID', 'coursechapterNo'], 'integer'],
            [['coursechapterDescription', 'coursechapterStatus'], 'string'],
            [['coursechapterCreatedDate','coursechapeterVideoFileCount','coursechapeterContentFileCount','chapterfile'], 'safe'],
           [['coursechapterFile'], 'string', 'max' => 100],
		    [['chapterfileFile'], 'file', 'extensions'=>'mp4,pdf'],
            [['coursechapterFileURL'], 'string', 'max' => 300],
            [['courseID', 'coursesubjID', 'coursechapterName'], 'unique', 'targetAttribute' => ['courseID', 'coursesubjID', 'coursechapterName']],
        ];
     }
	}
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coursechapterID' => 'Course Chapter ID',
            'courseID' => 'Course',
            'coursesubjID' => 'Course Subject',
            'coursechapterNo' => 'Course Chapter No',
            'coursechapterName' => 'Name',
            'coursechapterDescription' => 'Description',
            'coursechapterFile' => 'Image',
             'chapterfileFile' => 'Content (Videos, Pdf)',
             'chapterfileFileType' => 'Chapter File FileType',
             'chapterfile' => 'File Name',
            'coursechapterStatus' => 'Status',
            'coursechapterCreatedDate' => 'Created Date',
            'coursechapeterVideoFileCount' => 'coursechapeterVideoFileCount',
            'coursechapeterContentFileCount' => 'coursechapeterContentFileCount',
        ];
    }

	
	public function getCourses()
	{
		return $this->hasOne(Courses::className(),['courseID'=>'courseID']);
	}
	public function getcoursesubjects()
	{
		return $this->hasOne(coursesubjects::className(),['coursesubjID'=>'coursesubjID']);
	}
	
	public function getChapterfiles()
	{
		return $this->hasMany(Chapterfiles::className(),['coursechapterID'=>'coursechapterID']);
	}
}
