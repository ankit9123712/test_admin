<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Homemessage;

/**
 * HomemessageSearch represents the model behind the search form of `backend\models\Homemessage`.
 */
class HomemessageSearch extends Homemessage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['messageID', 'instituteID', 'streamID', 'coursestdID'], 'integer'],
            [['messageMsg', 'title','messageStatus', 'startDate', 'endDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Homemessage::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'messageID' => $this->messageID,
            'instituteID' => $this->instituteID,
            'streamID' => $this->streamID,
            'coursestdID' => $this->coursestdID,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'title' => $this->title,
        ]);

        $query->andFilterWhere(['like', 'messageMsg', $this->messageMsg])
            ->andFilterWhere(['like', 'messageStatus', $this->messageStatus]);
		$query->orderby('messageID DESC');
        return $dataProvider;
    }
}
