<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "monkcategory".
 *
 * @property integer $monkcatID
 * @property string $monkcatName
 * @property string $monkcatStatus
 * @property string $monkcatCreatedDate
 */
class Monkcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monkcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['monkcatName'], 'required'],
            [['monkcatStatus'], 'string'],
            [['monkcatCreatedDate'], 'safe'],
            [['monkcatName'], 'string', 'max' => 100,'min' => 2],
            [['monkcatName'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'monkcatID' => 'ID',
            'monkcatName' => 'Name',
            'monkcatStatus' => 'Status',
            'monkcatCreatedDate' => 'Created Date',
        ];
    }
}
