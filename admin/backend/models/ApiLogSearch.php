<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ApiLog;

/**
 * ApiLogSearch represents the model behind the search form of `backend\models\ApiLog`.
 */
class ApiLogSearch extends ApiLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apiID'], 'integer'],
            [['apiName', 'apiIP', 'apiRequest', 'apiResponse', 'apiType', 'apiCallDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApiLog::find();

        if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);
		
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'apiID' => $this->apiID,
            'apiCallDate' => $this->apiCallDate ,
        ]);

        $query->andFilterWhere(['like', 'apiName', $this->apiName])
            ->andFilterWhere(['like', 'apiIP', $this->apiIP])
            ->andFilterWhere(['like', 'apiRequest', $this->apiRequest])
            ->andFilterWhere(['like', 'apiResponse', $this->apiResponse])			
            ->andFilterWhere(['like', 'apiType', $this->apiType]);
		$query->orderBy('apiID DESC');
        return $dataProvider;
    }
}
