<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $bannerID
 * @property string $bannerName
 * @property string $bannerScreen
 * @property string $bannerFileType
 * @property string $bannerStatus
 * @property string $bannerFile
 * @property integer $bannerPosition
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bannerName', 'bannerScreen', 'bannerStatus', 'bannerFile'], 'required'],
            [['bannerFileType', 'bannerStatus'], 'string'],
            [['bannerPosition'], 'number'],
            [['bannerPosition'], 'number',  'max' => 999, 'min' => 1],
            [['bannerName', 'bannerScreen',   'bannerFile','bannerRemarks'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bannerID' => 'ID',
            'bannerName' => 'Name',
            'bannerScreen' => 'Screen',
            'bannerFileType' => 'Type',
            'bannerFileTypeID' => 'Banner Type',
            'bannerRemarks' => 'Remark',
            'bannerStatus' => 'Status',
            'bannerFile' => 'Image/Video',
            'bannerPosition' => 'Position',
        ];
    }
}
