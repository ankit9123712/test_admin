<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Languagemsgs;

/**
 * LanguagemsgsSearch represents the model behind the search form of `app\models\Languagemsgs`.
 */
class LanguagemsgsSearch extends Languagemsgs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['langMsgID', 'languageID'], 'integer'],
            [['langMsgKey', 'langMsgValue', 'langMsgStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Languagemsgs::find();

        // add conditions that should always apply here

         if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'langMsgID' => $this->langMsgID,
            'languageID' => $this->languageID,
        ]);

        $query->andFilterWhere(['like', 'langMsgKey', $this->langMsgKey])
            ->andFilterWhere(['like', 'langMsgValue', $this->langMsgValue])
            ->andFilterWhere(['like', 'langMsgStatus', $this->langMsgStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
		$query->orderby('langMsgID DESC');	
        return $dataProvider;
    }
}
