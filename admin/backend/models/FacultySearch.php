<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Faculty;

/**
 * FacultySearch represents the model behind the search form of `backend\models\Faculty`.
 */
class FacultySearch extends Faculty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['facultyID', 'moduleID', 'countryID', 'stateID', 'cityID'], 'integer'],
            [['facultyID', 'moduleID', 'countryID', 'stateID', 'cityID','facultyFullName', 'facultyMobile', 'facultyEmail', 'facultyPassword', 'facultyBirthDate', 'facultyGender', 'facultyDOB', 'facultyJoinDate', 'facultyProfilePicture', 'facultyAddress', 'facultyPincode', 'facultyDeviceType', 'facultyDeviceID', 'facultyReferKey', 'facultyVerified', 'facultyStatus', 'facultyCreatedDate', 'facultyOTP', 'facultySignupOTPVerified'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Faculty::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('country');
		$query->JoinWith('state');
		$query->JoinWith('city');
		$query->JoinWith('coursemodule');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'facultyID' => $this->facultyID,
            'facultyBirthDate' => $this->facultyBirthDate,
            'facultyDOB' => $this->facultyDOB,
            'facultyJoinDate' => $this->facultyJoinDate,
           // 'moduleID' => $this->moduleID,
            //'countryID' => $this->countryID,
           // 'stateID' => $this->stateID,
           // 'cityID' => $this->cityID,
            'facultyCreatedDate' => $this->facultyCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'facultyFullName', $this->facultyFullName])
            ->andFilterWhere(['like', 'coursemodule.moduleName', $this->moduleID])
           ->andFilterWhere(['like', 'state.stateName', $this->stateID])
			->andFilterWhere(['like', 'city.cityName', $this->cityID])
			->andFilterWhere(['like', 'country.countryName', $this->countryID])
            ->andFilterWhere(['like', 'facultyMobile', $this->facultyMobile])
            ->andFilterWhere(['like', 'facultyEmail', $this->facultyEmail])
            ->andFilterWhere(['like', 'facultyPassword', $this->facultyPassword])
            ->andFilterWhere(['like', 'facultyGender', $this->facultyGender])
            ->andFilterWhere(['like', 'facultyProfilePicture', $this->facultyProfilePicture])
            ->andFilterWhere(['like', 'facultyAddress', $this->facultyAddress])
            ->andFilterWhere(['like', 'facultyPincode', $this->facultyPincode])
            ->andFilterWhere(['like', 'facultyDeviceType', $this->facultyDeviceType])
            ->andFilterWhere(['like', 'facultyDeviceID', $this->facultyDeviceID])
            ->andFilterWhere(['like', 'facultyReferKey', $this->facultyReferKey])
            ->andFilterWhere(['like', 'facultyVerified', $this->facultyVerified])
            ->andFilterWhere(['like', 'facultyStatus', $this->facultyStatus])
            ->andFilterWhere(['like', 'facultyOTP', $this->facultyOTP])
            ->andFilterWhere(['like', 'facultySignupOTPVerified', $this->facultySignupOTPVerified]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('facultyID DESC');
        return $dataProvider;
    }
}
