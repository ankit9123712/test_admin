<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Smslog;

/**
 * SmslogSearch represents the model behind the search form of `backend\models\Smslog`.
 */
class SmslogSearch extends Smslog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['smslogID'], 'integer'],
            [['smslogMobile', 'smslogMessage', 'smslogResponse', 'smslogDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Smslog::find();
		/*$session = Yii::$app->session;
		$language = $session->get('customerparams');
		if(!isset($langage['per-page']))
		{
			$session['customerparams'] = [
					'per-page' => 20,
			];
		}*/
        // add conditions that should always apply here

        if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'smslogID' => $this->smslogID,
            'smslogDate' => $this->smslogDate,
        ]);

        $query->andFilterWhere(['like', 'smslogMobile', $this->smslogMobile])
            ->andFilterWhere(['like', 'smslogMessage', $this->smslogMessage]);
            //->andFilterWhere(['like', 'smslogResponse', $this->smslogResponse]);
		$query->orderBy('smslogID DESC');
        return $dataProvider;
    }
}
