<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Usersubscriptions;

/**
 * UsersubscriptionsSearch represents the model behind the search form of `backend\models\Usersubscriptions`.
 */
class UsersubscriptionsSearch extends Usersubscriptions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['usersubscriptionID', 'userID', 'subscriptionID'], 'integer'],
           // [['usersubscriptionID', 'userID', 'subscriptionID','usersubscriptionAmount'], 'number'],
            [['usersubscriptionID', 'userID', 'subscriptionID','usersubscriptionAmount','usersubscriptionCoponCode', 'usersubscriptionStartDate', 'usersubscriptionEndDate', 'usersubscriptionCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usersubscriptions::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('users');
		$query->JoinWith('subscriptionplan');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'usersubscriptionID' => $this->usersubscriptionID,
         //   'userID' => $this->userID,
            //'subscriptionID' => $this->subscriptionID,
            'usersubscriptionAmount' => $this->usersubscriptionAmount,
            'usersubscriptionStartDate' => $this->usersubscriptionStartDate,
            'usersubscriptionEndDate' => $this->usersubscriptionEndDate,
            'usersubscriptionCreatedDate' => $this->usersubscriptionCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'usersubscriptionCoponCode', $this->usersubscriptionCoponCode])
		->andFilterWhere(['like', 'users.userFullName', $this->userID])
		->andFilterWhere(['like', 'subscriptionplan.planName', $this->subscriptionID]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('usersubscriptionID DESC');
        return $dataProvider;
    }
}
