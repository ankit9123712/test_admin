<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "exams".
 *
 * @property integer $examID
 * @property integer $moduleID
 * @property string $examName
 * @property string $examContains
 * @property string $examSections
 * @property string $examDuration
 * @property string $queIDs
 * @property integer $examTotalQs
 * @property integer $examQualifyingMarks
 * @property double $examCorrectAnswer
 * @property double $examWrongAnswer
 * @property string $examInstruction
 * @property string $examStatus
 */
class Exams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public $readingFile; 
	public $writingFile; 
	public $listeningFile; 
	public $speakingFile; 
    public static function tableName()
    {
        return 'exams';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'examName', 'examDuration','examTotalQs', 'examQualifyingMarks', 'examCorrectAnswer', 'examWrongAnswer', 'examInstruction', 'examStatus'], 'required'],
            [['moduleID', 'examTotalQs', 'examQualifyingMarks'], 'integer'],
            [['examDuration','queIDs','examType'], 'safe'],
            [['examCorrectAnswer', 'examWrongAnswer'], 'number'],
            [['examInstruction', 'examStatus'], 'string'],
            [['examName', 'examContains', 'examSections'], 'string', 'max' => 100],
            //[['queIDs'], 'string', 'max' => 3000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'examID' => 'ID',
            'moduleID' => 'Module',
            'examName' => 'Name',
            'examType' => 'Type',
            'examContains' => 'Contains',
            'examSections' => 'Sections',
            'examDuration' => 'Duration',
            'queIDs' => 'Questions',
            'examTotalQs' => 'Total Questions',
            'examQualifyingMarks' => 'Qualifying Marks',
            'examCorrectAnswer' => 'Correct Answer',
            'examWrongAnswer' => 'Wrong Answer',
            'examInstruction' => 'Instruction',
            'examStatus' => 'Status',
        ];
    }
	
	public function getCoursemodule()
    {
        return $this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
	public function getQuestioncount($examID)
    {
		$res = Questions::find('queID')->where('examID='.$examID)->asArray()->all();
        return count($res);//$this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
}
