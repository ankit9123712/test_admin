<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "homemessage".
 *
 * @property int $messageID
 * @property string $messageMsg
 * @property string $messageStatus
 * @property int $instituteID
 * @property int $streamID
 * @property int $coursestdID
 * @property string $startDate
 * @property string $endDate
 */
class Homemessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'homemessage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['messageMsg', 'title','messageStatus', 'instituteID', 'streamID', 'coursestdID'], 'required'],
            [['messageStatus'], 'string'],
            [['instituteID', 'streamID', 'coursestdID'], 'integer'],
            [['startDate', 'endDate'], 'safe'],
            [['messageMsg'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'messageID' => 'I D',
            'messageMsg' => 'Description',
            'messageStatus' => 'Status',
            'instituteID' => 'Institute',
            'streamID' => 'Stream',
            'coursestdID' => 'Course Std',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'title' => 'Title',
        ];
    }
	
		public function getStream()
	{
		return $this->hasOne(Stream::className(),['streamID'=>'streamID']);
	}
	public function getCoursestd()
	{
		return $this->hasOne(Coursestd::className(),['coursestdID'=>'coursestdID']);
	}
	public function getInstitute()
	{
		return $this->hasOne(Institute::className(),['instituteID'=>'instituteID']);
	}
}
