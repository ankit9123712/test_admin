<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "offer".
 *
 * @property integer $offerID
 * @property string $offerName
 * @property string $offerImage
 * @property string $offerDiscountType
 * @property string $offerDiscountValue
 * @property string $offerStartDate
 * @property string $offerEndDate
 * @property string $offerCodeUse
 * @property integer $offerMaxLimit
 * @property string $offerCodeType
 * @property integer $offerCodeQty
 * @property string $offerSalePrice
 * @property string $offerDescription
 * @property string $offerStatus
 * @property string $offerCreatedDate
 */
class Offer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offerName', 'offerDiscountType', 'offerDiscountValue', 'offerCodeUse', 'offerMaxLimit', 'offerCodeType', 'offerCodeQty', 'offerDescription','offerExclusive'], 'required'],
            [['offerDiscountType', 'offerCodeUse', 'offerCodeType', 'offerStatus','offerExclusive'], 'string'],
            [['offerDiscountValue', 'offerSalePrice'], 'number'],
            [['offerStartDate', 'offerEndDate', 'offerCreatedDate'], 'safe'],
            [['offerMaxLimit', 'offerCodeQty'], 'integer'],
            [['offerName'], 'string', 'max' => 100],
            [['offerImage'], 'string', 'max' => 300],
            [['offerDescription'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'offerID' => 'ID',
            'offerName' => 'Name',
            'offerImage' => 'Image',
            'offerDiscountType' => 'Discount Type',
            'offerDiscountValue' => 'Discount Value',
            'offerStartDate' => 'Start Date',
            'offerEndDate' => 'End Date',
            'offerCodeUse' => 'Code Use',
            'offerMaxLimit' => 'Max Limit',
            'offerCodeType' => 'Code Type',
            'offerCodeQty' => 'No of Coupon code to Generate',
            'offerSalePrice' => 'Sale Price',
            'offerDescription' => 'Description',
            'offerStatus' => 'Status',
            'offerCreatedDate' => 'Created Date',
			'offerExclusive'=> 'Is Exclusive?',
        ];
    }
}
