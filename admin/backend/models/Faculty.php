<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "faculty".
 *
 * @property integer $facultyID
 * @property string $facultyFullName
 * @property string $facultyMobile
 * @property string $facultyEmail
 * @property string $facultyPassword
 * @property string $facultyBirthDate
 * @property string $facultyGender
 * @property string $facultyDOB
 * @property string $facultyJoinDate
 * @property integer $moduleID
 * @property string $facultyProfilePicture
 * @property string $facultyAddress
 * @property string $facultyPincode
 * @property integer $countryID
 * @property integer $stateID
 * @property integer $cityID
 * @property string $facultyDeviceType
 * @property string $facultyDeviceID
 * @property string $facultyReferKey
 * @property string $facultyVerified
 * @property string $facultyStatus
 * @property string $facultyCreatedDate
 * @property string $facultyOTP
 * @property string $facultySignupOTPVerified
 */
class Faculty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faculty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['facultyFullName', 'facultyMobile', 'facultyEmail', 'facultyGender','moduleID', 'facultyPincode', 'countryID', 'stateID', 'cityID'], 'required'],
            [['facultyBirthDate', 'facultyDOB', 'facultyJoinDate', 'facultyCreatedDate','facultyType'], 'safe'],
            [['facultyGender', 'facultyVerified', 'facultyStatus', 'facultySignupOTPVerified'], 'string'],
            [['moduleID', 'countryID', 'stateID', 'cityID'], 'integer'],
            [['facultyEmail', 'facultyPassword', 'facultyProfilePicture'], 'string', 'max' => 100],
            [['facultyMobile'], 'string', 'max' => 10,'min' => 10],
            [['facultyPincode'], 'number', 'max' => 999999,'min' => 0],
            [['facultyFullName'], 'string', 'max' => 100,'min' => 3],
            [['facultyAddress'], 'string', 'max' => 300],
            [['facultyDeviceID'], 'string', 'max' => 500],
            [['facultyOTP'], 'string', 'max' => 6],
            [['facultyMobile'], 'unique'],
            [['facultyEmail'], 'unique'],
			['facultyFullName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => ' Faculty Name you entered is incorrect.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'facultyID' => 'ID',
            'facultyFullName' => 'Full Name',
            'facultyMobile' => 'Mobile',
            'facultyEmail' => 'Email',
            'facultyPassword' => 'Password',
            'facultyBirthDate' => 'Birth Date',
            'facultyGender' => 'Gender',
            'facultyDOB' => 'DOB',
            'facultyJoinDate' => 'Join Date',
            'moduleID' => 'Module',
            'facultyType' => 'Type',
            'facultyProfilePicture' => 'Profile Picture',
            'facultyAddress' => 'Address',
            'facultyPincode' => 'Pincode',
            'countryID' => 'Country',
            'stateID' => 'State',
            'cityID' => 'City',
            'facultyDeviceType' => 'Device Type',
            'facultyDeviceID' => 'Device ID',
            'facultyReferKey' => 'Refer Key',
            'facultyVerified' => 'Verified',
            'facultyStatus' => 'Status',
            'facultyCreatedDate' => 'Created Date',
            'facultyOTP' => 'OTP',
            'facultySignupOTPVerified' => 'Signup OTP Verified',
        ];
    }
	public function getCountry()
    {
        return $this->hasOne(Country::className(),['countryID'=>'countryID']);
    }
	public function getState()
    {
        return $this->hasOne(State::className(),['stateID'=>'stateID']);
    }
	public function getCity()
    {
        return $this->hasOne(City::className(),['cityID'=>'cityID']);
    }
	public function getCoursemodule()
    {
        return $this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
}
