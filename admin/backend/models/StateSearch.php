<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\State;

/**
 * StateSearch represents the model behind the search form of `backend\models\State`.
 */
class StateSearch extends State
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['stateID', 'countryID'], 'integer'],
            [['stateID', 'countryID','stateName', 'stateStatus', 'stateCreatedDate', 'stateLatitude', 'stateLongitude'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = State::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('country');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'stateID' => $this->stateID,
           // 'countryID' => $this->countryID,
            'stateCreatedDate' => $this->stateCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'stateName', $this->stateName])
            ->andFilterWhere(['like', 'country.countryName', $this->countryID])
            ->andFilterWhere(['like', 'stateStatus', $this->stateStatus])
            ->andFilterWhere(['like', 'stateLatitude', $this->stateLatitude])
            ->andFilterWhere(['like', 'stateLongitude', $this->stateLongitude]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
		$query->orderby('stateID DESC');
        return $dataProvider;
    }
}
