<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "useraddress".
 *
 * @property integer $addressID
 * @property integer $userID
 * @property string $addressTitle
 * @property string $addressContactDesgination
 * @property string $addressAddressLine1
 * @property string $addressAddressLine2
 * @property integer $cityID
 * @property integer $stateID
 * @property integer $countryID
 * @property string $addressPincode
 * @property string $addressGST
 * @property string $addressPAN
 * @property string $addressType
 * @property string $addressIsDefault
 * @property string $addressCreatedDate
 */
class Useraddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'useraddress';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID',  'addressAddressLine1', 'cityID', 'stateID', 'countryID', 'addressPincode', 'addressType'], 'required'],
            [['userID'], 'integer'],
            [['addressIsDefault', 'cityID', 'stateID', 'countryID'], 'string'],
            [['addressCreatedDate','addressTitle','addressGST', 'addressPAN'], 'safe'],
            [['addressTitle', 'addressContactDesgination', 'addressAddressLine1', 'addressAddressLine2'], 'string', 'max' => 100],
            [['addressPincode'], 'string', 'max' => 6, 'min' =>6],
            [['addressGST'], 'string', 'max' => 15 , 'min' => 15],
			[['addressPAN'], 'string', 'max' => 10 , 'min' => 10],
            [['addressType'], 'string', 'max' => 30],
			['addressPincode','match', 'pattern' => '/^[0-9.]+$/u', 'message' => 'Pincode can only contain Numbers.'],
			/*['addressContactDesgination','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'Designation can only contain Alphabets and Spaces.'],*/
			['addressGST','match', 'pattern' => '/^[A-Za-z0-9]+$/u', 'message' => 'GST can only contain Alphabets and Numbers.'],
			['addressPAN','match', 'pattern' => '/^[A-Za-z0-9]+$/u', 'message' => 'PAN can only contain Alphabets and Numbers.'],			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'addressID' => 'ID',
            'userID' => 'User',
            'addressTitle' => 'Title',
            'addressContactDesgination' => 'Desgination',
            'addressAddressLine1' => 'Address Line1',
            'addressAddressLine2' => 'Address Line2',
            'cityID' => 'City',
            'stateID' => 'State',
            'countryID' => 'Country',
            'addressPincode' => 'Pincode',
            'addressGST' => 'GST',
            'addressPAN' => 'PAN',
            'addressType' => 'Type',
            'addressIsDefault' => 'Is Default',
            'addressCreatedDate' => 'Created Date',
        ];
    }
}
