<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Helpdeskquery;

/**
 * HelpdeskquerySearch represents the model behind the search form of `backend\models\Helpdeskquery`.
 */
class HelpdeskquerySearch extends Helpdeskquery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['helpdeskqueryID', 'userID'], 'integer'],
            [['helpdeskqueryID', 'userID','heldeskqueryQuery', 'helpdeskqueryStatus', 'helpdeskqueryCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Helpdeskquery::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('users');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'helpdeskqueryID' => $this->helpdeskqueryID,
           // 'userID' => $this->userID,
            'helpdeskqueryCreatedDate' => $this->helpdeskqueryCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'heldeskqueryQuery', $this->heldeskqueryQuery])
			->andFilterWhere(['like', 'users.userFullName', $this->userID])
            ->andFilterWhere(['like', 'helpdeskqueryStatus', $this->helpdeskqueryStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('helpdeskqueryID DESC');
        return $dataProvider;
    }
}
