<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "smslog".
 *
 * @property integer $smslogID
 * @property string $smslogMobile
 * @property string $smslogMessage
 * @property string $smslogResponse
 * @property string $smslogDate
 */
class Smslog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'smslog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['smslogDate'], 'safe'],
            [['smslogMobile'], 'string', 'max' => 20],
            [['smslogMessage', 'smslogResponse'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'smslogID' => 'ID',
            'smslogMobile' => 'Mobile',
            'smslogMessage' => 'Message',
            'smslogResponse' => 'Response',
            'smslogDate' => 'Date',
        ];
    }
	
	/*-------------------- Add SMS Log Code Start -------------------------*/
	
	public function AddSMSLog($smslogMobile,$smslogMessage,$smslogResponse)
	{
		$Smslog =  new Smslog;
		$Smslog->smslogMobile = $smslogMobile;
		$Smslog->smslogMessage = $smslogMessage;
		$Smslog->smslogResponse = $smslogResponse;
		$Smslog->save(false);				//echo $Smslog->smslogID;die;
	}	
	
	/*-------------------- Add SMS Log Code End -------------------------*/
}
