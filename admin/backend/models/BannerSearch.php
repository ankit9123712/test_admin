<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Banner;

/**
 * BannerSearch represents the model behind the search form of `backend\models\Banner`.
 */
class BannerSearch extends Banner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bannerID', 'bannerPosition'], 'integer'],
            [['bannerName', 'bannerScreen', 'bannerFileType',  'bannerStatus', 'bannerFile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Banner::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bannerID' => $this->bannerID,
            'bannerPosition' => $this->bannerPosition,
        ]);

        $query->andFilterWhere(['like', 'bannerName', $this->bannerName])
            ->andFilterWhere(['like', 'bannerScreen', $this->bannerScreen])
            ->andFilterWhere(['like', 'bannerFileType', $this->bannerFileType])
            ->andFilterWhere(['like', 'bannerStatus', $this->bannerStatus])
            ->andFilterWhere(['like', 'bannerFile', $this->bannerFile]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('bannerID DESC');
        return $dataProvider;
    }
}
