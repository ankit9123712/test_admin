<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "subject".
 *
 * @property int $subjectID
 * @property int $coursestdID
 * @property string $subjectName
 * @property string $subjectLastMonth
 * @property string $subjectColor
 * @property string $subjectIcon
 * @property string $subjectRemarks
 * @property string $subjectStatus
 * @property string $subjectCreatedDate
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coursestdID', 'subjectName', 'subjectLastMonth', 'subjectColor','subjectStatus'], 'required'],
            [['coursestdID'], 'integer'],
            [['subjectStatus'], 'string'],
            [['subjectCreatedDate'], 'safe'],
            [['subjectName', 'subjectIcon'], 'string', 'max' => 100],
            [['subjectLastMonth', 'subjectColor'], 'string', 'max' => 20],
            [['subjectRemarks'], 'string', 'max' => 200],
            [['subjectName', 'coursestdID'], 'unique', 'targetAttribute' => ['subjectName', 'coursestdID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subjectID' => 'ID',
            'coursestdID' => 'Course Standard',
            'subjectName' => 'Subject',
            'subjectLastMonth' => 'Last Month',
            'subjectColor' => 'Color',
            'subjectIcon' => 'Icon',
            'subjectRemarks' => 'Remarks',
            'subjectStatus' => 'Status',
            'subjectCreatedDate' => 'Created Date',
        ];
    }
	public function getCoursestd()
    {
        return $this->hasOne(Coursestd::className(),['coursestdID'=>'coursestdID']);
    }
}
