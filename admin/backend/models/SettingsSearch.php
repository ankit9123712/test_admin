<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Settings;

/**
 * SettingsSearch represents the model behind the search form of `backend\models\Settings`.
 */
class SettingsSearch extends Settings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['settingsID', 'settingEmailPort', 'settingsDefCountry', 'settingsDefState', 'settingsDefCity', 'settingsUserResetPinLinkExpHr', 'settingsLogDeleteDays', 'settingsOrderReturnTimeMinutes'], 'integer'],
            [['settingsEmailFrom', 'settingsEmailTo', 'settingsEmailGateway', 'settingEmailID', 'settingsEmailPass', 'settingsSSL', 'settingsTLS', 'settingSenderName', 'settingsSMSURL', 'settingSMSUser', 'settingsSMSPassword', 'settingsSMSPort', 'settingsGAKey', 'settingsGMapKey', 'settinngsSalesNo', 'settingsTollFree', 'settingsMasterOtp', 'settingsGSTNo','settingsGSTOnReadymadeGarments','settingsGSTOnOtherService','settingsQtyAlertOfOutoffStock','settingsQtyAlertOfLowStock','settingsMoneyToBeUsedFromWalletForPayment','settingsMoneyEarnedByCustomerThroughReferAndEarn','settingsReferAndEarnStatus','settingsReferAndEarnValidityDays','settingsMinimumOrderValue'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Settings::find();

        // add conditions that should always apply here

        if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'settingsID' => $this->settingsID,
            'settingEmailPort' => $this->settingEmailPort,
            'settingsDefCountry' => $this->settingsDefCountry,
            'settingsDefState' => $this->settingsDefState,
            'settingsDefCity' => $this->settingsDefCity,
            'settingsUserResetPinLinkExpHr' => $this->settingsUserResetPinLinkExpHr,
            'settingsLogDeleteDays' => $this->settingsLogDeleteDays,
            'settingsOrderReturnTimeMinutes' => $this->settingsOrderReturnTimeMinutes,
        ]);

        $query->andFilterWhere(['like', 'settingsEmailFrom', $this->settingsEmailFrom])
            ->andFilterWhere(['like', 'settingsEmailTo', $this->settingsEmailTo])
            ->andFilterWhere(['like', 'settingsEmailGateway', $this->settingsEmailGateway])
            ->andFilterWhere(['like', 'settingEmailID', $this->settingEmailID])
            ->andFilterWhere(['like', 'settingsEmailPass', $this->settingsEmailPass])
            ->andFilterWhere(['like', 'settingsSSL', $this->settingsSSL])
            ->andFilterWhere(['like', 'settingsTLS', $this->settingsTLS])
            ->andFilterWhere(['like', 'settingSenderName', $this->settingSenderName])
            ->andFilterWhere(['like', 'settingsSMSURL', $this->settingsSMSURL])
            ->andFilterWhere(['like', 'settingSMSUser', $this->settingSMSUser])
            ->andFilterWhere(['like', 'settingsSMSPassword', $this->settingsSMSPassword])
            ->andFilterWhere(['like', 'settingsSMSPort', $this->settingsSMSPort])
            ->andFilterWhere(['like', 'settingsGAKey', $this->settingsGAKey])
            ->andFilterWhere(['like', 'settingsGMapKey', $this->settingsGMapKey])
            ->andFilterWhere(['like', 'settinngsSalesNo', $this->settinngsSalesNo])
            ->andFilterWhere(['like', 'settingsGSTOnReadymadeGarments', $this->settingsGSTOnReadymadeGarments])
            ->andFilterWhere(['like', 'settingsGSTOnOtherService', $this->settingsGSTOnOtherService])
            ->andFilterWhere(['like', 'settingsQtyAlertOfOutoffStock', $this->settingsQtyAlertOfOutoffStock])
            ->andFilterWhere(['like', 'settingsQtyAlertOfLowStock', $this->settingsQtyAlertOfLowStock])
            ->andFilterWhere(['like', 'settingsMoneyToBeUsedFromWalletForPayment', $this->settingsMoneyToBeUsedFromWalletForPayment])
            ->andFilterWhere(['like', 'settingsMoneyEarnedByCustomerThroughReferAndEarn', $this->settingsMoneyEarnedByCustomerThroughReferAndEarn])
            ->andFilterWhere(['like', 'settingsReferAndEarnStatus', $this->settingsReferAndEarnStatus])
            ->andFilterWhere(['like', 'settingsReferAndEarnValidityDays', $this->settingsReferAndEarnValidityDays])
            ->andFilterWhere(['like', 'settingsMinimumOrderValue	', $this->settingsMinimumOrderValue	])
            ->andFilterWhere(['like', 'settingsGSTNo', $this->settingsGSTNo]);
		$query->orderby('settingsID DESC');
        return $dataProvider;
    }
}
