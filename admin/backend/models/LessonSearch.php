<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Lesson;

/**
 * LessonSearch represents the model behind the search form of `backend\models\Lesson`.
 */
class LessonSearch extends Lesson
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['LessonID', 'moduleID', 'LessonNo', 'VideoFileCount', 'ContentFileCount'], 'integer'],
            [['LessonID', 'moduleID', 'LessonNo', 'VideoFileCount', 'ContentFileCount','LessonName', 'LessonDescription', 'lessionMarkingSystem', 'lessionQuickTips', 'LessonType', 'LessonFileType', 'LessonFile', 'LessonFileURL', 'LessonStatus', 'LessonCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lesson::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('coursemodule');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'LessonID' => $this->LessonID,
         //   'moduleID' => $this->moduleID,
            'LessonNo' => $this->LessonNo,
            'VideoFileCount' => $this->VideoFileCount,
            'ContentFileCount' => $this->ContentFileCount,
            'LessonCreatedDate' => $this->LessonCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'LessonName', $this->LessonName])
            ->andFilterWhere(['like', 'coursemodule.moduleName', $this->moduleID])
            ->andFilterWhere(['like', 'LessonDescription', $this->LessonDescription])
            ->andFilterWhere(['like', 'lessionMarkingSystem', $this->lessionMarkingSystem])
            ->andFilterWhere(['like', 'lessionQuickTips', $this->lessionQuickTips])
            ->andFilterWhere(['like', 'LessonType', $this->LessonType])
            ->andFilterWhere(['like', 'LessonFileType', $this->LessonFileType])
            ->andFilterWhere(['like', 'LessonFile', $this->LessonFile])
            ->andFilterWhere(['like', 'LessonFileURL', $this->LessonFileURL])
            ->andFilterWhere(['like', 'LessonStatus', $this->LessonStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('LessonID DESC');
        return $dataProvider;
    }
}
