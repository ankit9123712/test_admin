<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Faq;

/**
 * FaqSearch represents the model behind the search form of `backend\models\Faq`.
 */
class FaqSearch extends Faq
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           //[['faqID', 'faqtypeID', 'faqPosition'], 'integer'],
            [['faqID', 'faqtypeID', 'faqPosition','faqQuestion', 'faqAnswer', 'faqStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Faq::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('faqtype');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'faqID' => $this->faqID,
            //'faqtypeID' => $this->faqtypeID,
            'faqPosition' => $this->faqPosition,
        ]);

        $query->andFilterWhere(['like', 'faqQuestion', $this->faqQuestion])
            ->andFilterWhere(['like', 'faqtype.faqtypeName', $this->faqtypeID])
            ->andFilterWhere(['like', 'faqAnswer', $this->faqAnswer])
            ->andFilterWhere(['like', 'faqStatus', $this->faqStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('faqID DESC');
        return $dataProvider;
    }
}
