<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $countryID
 * @property string $countryName
 * @property string $countryShortCode
 * @property string $countryISO2Code
 * @property string $countryCurrencySymbol
 * @property string $countryDialCode
 * @property string $countryFlagImage
 * @property string $countryStatus
 * @property string $countryCreatedDate
 * @property string $countryLatitude
 * @property string $countryLongitude
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countryName'], 'required'],
            [['countryStatus'], 'string'],
            [['countryCreatedDate'], 'safe'],
            [['countryName', 'countryFlagImage'], 'string', 'max' => 100,'min' => 2],
            [['countryShortCode'], 'string', 'max' => 3],
            [['countryISO2Code'], 'string', 'max' => 2],
            [['countryCurrencySymbol'], 'string', 'max' => 10],
            [['countryDialCode'], 'string', 'max' => 5],
            [['countryLatitude', 'countryLongitude'], 'string', 'max' => 20],
            [['countryName'], 'unique'],
			['countryName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => ' Country Name you entered is incorrect.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'countryID' => 'ID',
            'countryName' => 'Name',
            'countryShortCode' => 'Short Code',
            'countryISO2Code' => 'ISO 2 Code',
            'countryCurrencySymbol' => 'Currency Symbol',
            'countryDialCode' => 'Dial Code',
            'countryFlagImage' => 'Flag Image',
            'countryStatus' => 'Status',
            'countryCreatedDate' => 'Created Date',
            'countryLatitude' => 'Latitude',
            'countryLongitude' => 'Longitude',
        ];
    }
}
