<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Useranswers;

/**
 * UseranswersSearch represents the model behind the search form of `backend\models\Useranswers`.
 */
class UseranswersSearch extends Useranswers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['answerID', 'userID', 'queID', 'facultyID', 'lessionID', 'examID'], 'integer'],
            [['answerID', 'userID', 'queID', 'facultyID', 'lessionID', 'examID','answerAnswer', 'answerCorrectAnswer', 'answerIsCorrect', 'answerIsVerified', 'answerSubmittedDate', 'answerFor'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Useranswers::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('users');
		$query->JoinWith('questions');
		$query->JoinWith('lesson');
		$query->JoinWith('exams');
		$query->JoinWith('faculty');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'answerID' => $this->answerID,
           // 'userID' => $this->userID,
           // 'queID' => $this->queID,
           // 'facultyID' => $this->facultyID,
            'answerSubmittedDate' => $this->answerSubmittedDate,
          //  'lessionID' => $this->lessionID,
          //  'examID' => $this->examID,
        ]);

        $query->andFilterWhere(['like', 'answerAnswer', $this->answerAnswer])
            ->andFilterWhere(['like', 'users.userFullName', $this->userID])
            ->andFilterWhere(['like', 'questions.queQuestion', $this->queID])
            ->andFilterWhere(['like', 'faculty.facultyFullName', $this->facultyID])
            ->andFilterWhere(['like', 'exams.examName', $this->examID])
            ->andFilterWhere(['like', 'lesson.LessonName', $this->lessionID])
            ->andFilterWhere(['like', 'answerCorrectAnswer', $this->answerCorrectAnswer])
            ->andFilterWhere(['like', 'answerIsCorrect', $this->answerIsCorrect])
            ->andFilterWhere(['like', 'answerIsVerified', $this->answerIsVerified])
            ->andFilterWhere(['like', 'answerFor', $this->answerFor]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
		$query->orderby('answerID DESC');
        return $dataProvider;
    }
}
