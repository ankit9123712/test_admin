<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $cityID
 * @property integer $countryID
 * @property integer $stateID
 * @property string $cityName
 * @property string $cityStatus
 * @property string $cityCreatedDate
 * @property string $cityLatitude
 * @property string $cityLongitude
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countryID', 'stateID'], 'integer'],
            [['countryID', 'stateID','cityName'], 'required'],
            [['cityStatus'], 'string'],
            [['cityCreatedDate'], 'safe'],
            [['cityName'], 'string', 'max' => 100,'min' => 3],
            [['cityLatitude', 'cityLongitude'], 'string', 'max' => 20],
			['cityName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => ' City Name you entered is incorrect.'],
            [['countryID', 'cityName'], 'unique', 'targetAttribute' => ['countryID', 'cityName'], 'message' => 'The combination of Country and City Name has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cityID' => 'ID',
            'countryID' => 'Country',
            'stateID' => 'State',
            'cityName' => 'Name',
            'cityStatus' => 'Status',
            'cityCreatedDate' => 'Created Date',
            'cityLatitude' => 'Latitude',
            'cityLongitude' => 'Longitude',
        ];
    }
	
	public function getCountry()
    {
        return $this->hasOne(Country::className(),['countryID'=>'countryID']);
    }
	public function getState()
    {
        return $this->hasOne(State::className(),['stateID'=>'stateID']);
    }
	
}
