<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reason;

/**
 * ReasonSearch represents the model behind the search form of `app\models\Reason`.
 */
class ReasonSearch extends Reason
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reasonID'], 'integer'],
            [['reasonName', 'reasonStatus', 'reasonCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reason::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'reasonID' => $this->reasonID,
            'reasonCreatedDate' => $this->reasonCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'reasonName', $this->reasonName])
            ->andFilterWhere(['like', 'reasonStatus', $this->reasonStatus]);
		$query->orderby('reasonID DESC');
        return $dataProvider;
    }
}
