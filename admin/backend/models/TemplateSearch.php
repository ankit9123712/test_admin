<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Template;

/**
 * TemplateSearch represents the model behind the search form of `backend\models\Template`.
 */
class TemplateSearch extends Template
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['templateID'], 'integer'],
            [['templateName', 'templateActionName', 'templatePush', 'templateEmail', 'templateSMS', 'templateConstantCode', 'templateSubject', 'templateEmailText', 'templateSMSText', 'templateNotificationText', 'templateStatus', 'templateCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Template::find();

        // add conditions that should always apply here

        if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'templateID' => $this->templateID,
            'templateCreatedDate' => $this->templateCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'templateName', $this->templateName])
            ->andFilterWhere(['like', 'templateActionName', $this->templateActionName])
            ->andFilterWhere(['like', 'templatePush', $this->templatePush])
            ->andFilterWhere(['like', 'templateEmail', $this->templateEmail])
            ->andFilterWhere(['like', 'templateSMS', $this->templateSMS])
            ->andFilterWhere(['like', 'templateConstantCode', $this->templateConstantCode])
            ->andFilterWhere(['like', 'templateSubject', $this->templateSubject])
            ->andFilterWhere(['like', 'templateEmailText', $this->templateEmailText])
            ->andFilterWhere(['like', 'templateSMSText', $this->templateSMSText])
            ->andFilterWhere(['like', 'templateNotificationText', $this->templateNotificationText])
            ->andFilterWhere(['like', 'templateStatus', $this->templateStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
        return $dataProvider;
    }
}
