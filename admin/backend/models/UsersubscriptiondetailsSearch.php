<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Usersubscriptiondetails;

/**
 * UsersubscriptiondetailsSearch represents the model behind the search form of `backend\models\Usersubscriptiondetails`.
 */
class UsersubscriptiondetailsSearch extends Usersubscriptiondetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['planName', 'subscriptionStartDate', 'subscriptionEndDate', 'streamName', 'coursestdName'], 'safe'],
            [['planDiscountPrice'], 'number'],
            [['planDuration', 'userID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usersubscriptiondetails::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'planDiscountPrice' => $this->planDiscountPrice,
            'planDuration' => $this->planDuration,
            'subscriptionStartDate' => $this->subscriptionStartDate,
            'subscriptionEndDate' => $this->subscriptionEndDate,
            'userID' => $this->userID,
        ]);

        $query->andFilterWhere(['like', 'planName', $this->planName])
            ->andFilterWhere(['like', 'streamName', $this->streamName])
            ->andFilterWhere(['like', 'coursestdName', $this->coursestdName]);
		
        return $dataProvider;
    }
}
