<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Chapterfiles;

/**
 * ChapterfilesSearch represents the model behind the search form of `backend\models\Chapterfiles`.
 */
class ChapterfilesSearch extends Chapterfiles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chapterfileID', 'coursechapterID'], 'integer'],
            [['chapterfileFileType', 'chapterfileFile', 'chapterfileStatus','chapterfile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Chapterfiles::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'chapterfileID' => $this->chapterfileID,
            'coursechapterID' => $this->coursechapterID,
            'chapterfile' => $this->chapterfile,
        ]);

        $query->andFilterWhere(['like', 'chapterfileFileType', $this->chapterfileFileType])
            ->andFilterWhere(['like', 'chapterfileFile', $this->chapterfileFile])
            ->andFilterWhere(['like', 'chapterfileStatus', $this->chapterfileStatus]);
		$query->orderby('chapterfileID DESC');
        return $dataProvider;
    }
}
