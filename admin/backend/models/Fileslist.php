<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "fileslist".
 *
 * @property int $fileID
 * @property string $fileFile
 */
class Fileslist extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fileslist';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fileFile'], 'required'],
            [['fileFile','fileActualName'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fileID' => 'File ID',
            'fileFile' => 'File File',
        ];
    }
}
