<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "template".
 *
 * @property integer $templateID
 * @property string $templateName
 * @property string $templateActionName
 * @property string $templatePush
 * @property string $templateEmail
 * @property string $templateSMS
 * @property string $templateConstantCode
 * @property string $templateSubject
 * @property string $templateEmailText
 * @property string $templateSMSText
 * @property string $templateNotificationText
 * @property string $templateStatus
 * @property string $templateCreatedDate
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'templatePush', 'templateEmail', 'templateSMS'], 'required'],
            [['templatePush', 'templateEmail', 'templateSMS', 'templateEmailText', 'templateStatus'], 'string'],
            [['templateActionName', 'templateEmailText','templateCreatedDate'], 'safe'],
            [['templateName', 'templateActionName'], 'string', 'max' => 100],
            [['templateConstantCode'], 'string', 'max' => 6],
            [['templateSubject'], 'string', 'max' => 250,'min' => 2],
            [['templateSMSText', 'templateNotificationText'], 'string', 'max' => 1000],
            [['templateConstantCode'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'templateID' => 'ID',
            'templateName' => 'Name',
            'templateActionName' => 'Action Name',
            'templatePush' => 'Push',
            'templateEmail' => 'Email',
            'templateSMS' => 'SMS',
            'templateConstantCode' => 'Constant Code',
            'templateSubject' => 'Subject',
            'templateEmailText' => 'Email Text',
            'templateSMSText' => 'SMS Text',
            'templateNotificationText' => 'Notification Text',
            'templateStatus' => 'Status',
            'templateCreatedDate' => 'Created Date',
        ];
    }
}
