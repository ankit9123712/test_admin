<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $userID
 * @property string $userFullName
 * @property string $userCountryCode
 * @property string $userMobile
 * @property string $userEmail
 * @property string $userPassword
 * @property integer $userAge
 * @property string $userDOB
 * @property string $userGender
 * @property integer $countryID
 * @property integer $stateID
 * @property integer $cityID
 * @property string $userProfilePicture
 * @property string $userUniChoice1
 * @property string $userUniChoice2
 * @property string $userUniChoice3
 * @property string $userLiveLectureNotify
 * @property string $userTestNotify
 * @property string $userResultNotify
 * @property string $userAddress
 * @property integer $languageID
 * @property string $userDeviceType
 * @property string $userDeviceID
 * @property string $userReferKey
 * @property string $userVerified
 * @property string $userNewsNotify
 * @property string $userEventsNotify
 * @property string $userNoticeNotify
 * @property string $userAssignmentNotify
 * @property string $userForumNotify
 * @property string $userAdminNotify
 * @property string $userType
 * @property string $userStatus
 * @property string $userOTP
 * @property string $userCreatedDate
 * @property string $userSignupOTPVerified
 * @property string $userIELTSMonth
 * @property string $userIELTSYear
 * @property string $userIELTSCity
 * @property string $userSecurityToken
 * @property string $useTokenExpirtyDate
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userFullName', 'userCountryCode', 'userMobile', 'userEmail', 'userPassword', 'userAge', 'userGender', 'countryID', 'stateID', 'cityID', 'languageID', 'userDeviceType', 'userDeviceID'], 'required'],
            [['userAge', 'countryID', 'stateID', 'cityID', 'languageID'], 'integer'],
            [['userDOB', 'userCreatedDate', 'useTokenExpirtyDate'], 'safe'],
            [['userGender', 'userLiveLectureNotify', 'userTestNotify', 'userResultNotify', 'userVerified', 'userNewsNotify', 'userEventsNotify', 'userNoticeNotify', 'userAssignmentNotify', 'userForumNotify', 'userAdminNotify', 'userType', 'userStatus', 'userSignupOTPVerified'], 'string'],
            [['userFullName', 'userEmail', 'userPassword', 'userProfilePicture', 'userUniChoice1', 'userUniChoice2', 'userUniChoice3', 'userIELTSCity', 'userSecurityToken'], 'string', 'max' => 100],
            [['userCountryCode'], 'string', 'max' => 5],
            [['userMobile', 'userDeviceType', 'userReferKey'], 'string', 'max' => 10],
            [['userAddress'], 'string', 'max' => 300],
            [['userDeviceID'], 'string', 'max' => 500],
            [['userOTP'], 'string', 'max' => 6],
            [['userIELTSMonth'], 'string', 'max' => 2],
            [['userIELTSYear'], 'string', 'max' => 4],
            [['userMobile'], 'unique'],
            [['userEmail'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userID' => 'ID',
            'userFullName' => 'Full Name',
            'userCountryCode' => 'Country Code',
            'userMobile' => 'Mobile',
            'userEmail' => 'Email',
            'userPassword' => 'Password',
            'userAge' => 'Age',
            'userDOB' => 'DOB',
            'userGender' => 'Gender',
            'countryID' => 'Country',
            'stateID' => 'State',
            'cityID' => 'City',
            'userProfilePicture' => 'Profile Picture',
            'userUniChoice1' => ' University Choice 1',
            'userUniChoice2' => 'University Choice 2',
            'userUniChoice3' => 'University Choice 3',
            'userLiveLectureNotify' => 'Live Lecture Notify',
            'userTestNotify' => 'Test Notify',
            'userResultNotify' => 'Result Notify',
            'userAddress' => 'Address',
            'languageID' => 'Language',
            'userDeviceType' => 'Device Type',
            'userDeviceID' => 'Device ID',
            'userReferKey' => 'Refer Key',
            'userVerified' => 'Verified',
            'userNewsNotify' => 'News Notify',
            'userEventsNotify' => 'Events Notify',
            'userNoticeNotify' => 'Notice Notify',
            'userAssignmentNotify' => 'Assignment Notify',
            'userForumNotify' => 'Forum Notify',
            'userAdminNotify' => 'Admin Notify',
            'userType' => 'Type',
            'userStatus' => 'Status',
            'userOTP' => 'OTP',
            'userCreatedDate' => 'Joined Date',
            'userSignupOTPVerified' => 'Signup OTP Verified',
            'userIELTSMonth' => 'IELTS Month',
            'userIELTSYear' => 'IELTS Year',
            'userIELTSCity' => 'IELTS City',
            'userSecurityToken' => 'Security Token',
            'useTokenExpirtyDate' => 'Token Expirty Date',
        ];
    }
	public function getCountry()
    {
        return $this->hasOne(Country::className(),['countryID'=>'countryID']);
    }
	public function getState()
    {
        return $this->hasOne(State::className(),['stateID'=>'stateID']);
    }
	public function getCity()
    {
        return $this->hasOne(City::className(),['cityID'=>'cityID']);
    }
	public function getUsersubscriptions()
    {
        return $this->hasOne(Usersubscriptions::className(),['userID'=>'userID']);
    }
}
