<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property integer $queID
 * @property integer $moduleID
 * @property integer $lessionID
 * @property integer $examID
 * @property string $queFor
 * @property string $queType
 * @property integer $queDifficultyevel
 * @property string $queQuestion
 * @property string $queSolution
 * @property string $queDisplayType
 * @property string $queStatus
 * @property string $queOption1
 * @property string $queOption2
 * @property string $queOption3
 * @property string $queOption4
 * @property string $queCorrectAns
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['moduleID', 'queType','queQuestion','queStatus','queCorrectAns'], 'required'],
            [['moduleID', 'lessionID', 'examID', 'queDifficultyevel'], 'integer'],
            [['queFile','queBankType','queVerificationRequred'], 'safe'],
            [['queFor', 'queType', 'queDisplayType', 'queStatus'], 'string'],
            [['queQuestion', 'queSolution'], 'string', 'max' => 500],
            [['queOption1', 'queOption2', 'queOption3', 'queOption4'], 'string', 'max' => 250],
            [['queCorrectAns'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'queID' => 'ID',
            'moduleID' => 'Module',
            'lessionID' => 'Lession',
            'examID' => 'Exam',
            'queFor' => 'For',
            'queBankType' => 'Bank Type',
            'queType' => 'Type',
            'queFile' => 'File',
            'queDifficultyevel' => 'Difficulty Level',
            'queQuestion' => 'Question',
            'queSolution' => 'Solution',
            'queDisplayType' => 'Display Type',
            'queStatus' => 'Status',
            'queOption1' => 'Option 1',
            'queOption2' => 'Option 2',
            'queOption3' => 'Option 3',
            'queOption4' => 'Option 4',
            'queCorrectAns' => 'Correct Ans',
            'queVerificationRequred' => 'Verification Requred',
        ];
    }
	
	public function getCoursemodule()
    {
        return $this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
	public function getLesson()
    {
        return $this->hasOne(Lesson::className(),['LessonID'=>'lessionID']);
    }
	public function getExams()
    {
        return $this->hasOne(Exams::className(),['examID'=>'examID']);
    }
}
