<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Lessonfiles;

/**
 * LessonfilesSearch represents the model behind the search form of `backend\models\Lessonfiles`.
 */
class LessonfilesSearch extends Lessonfiles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['LessonfileID', 'LessonID'], 'integer'],
            [['LessonfileID', 'LessonID','LessonfileFileType', 'LessonfileFile', 'Lessonfile', 'LessonfileStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lessonfiles::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'LessonfileID' => $this->LessonfileID,
            'LessonID' => $this->LessonID,
        ]);

        $query->andFilterWhere(['like', 'LessonfileFileType', $this->LessonfileFileType])
            ->andFilterWhere(['like', 'LessonfileFile', $this->LessonfileFile])
            ->andFilterWhere(['like', 'Lessonfile', $this->Lessonfile])
            ->andFilterWhere(['like', 'LessonfileStatus', $this->LessonfileStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
		$query->orderby('LessonfileID DESC');
        return $dataProvider;
    }
}
