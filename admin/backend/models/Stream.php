<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "stream".
 *
 * @property int $streamID
 * @property string $streamName
 * @property string $streamStatus
 * @property string $streamCreatedDate
 */
class Stream extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stream';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamName', 'streamStatus'], 'required'],
            [['streamStatus'], 'string'],
            [['streamCreatedDate','streamRemarks'], 'safe'],
            [['streamName'], 'string','min'=>3, 'max' => 100],
            [['streamRemarks'], 'string','min'=>3, 'max' => 100],
            [['streamName'], 'unique'],
			['streamName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'Stream Name can only contain Alphabets and Spaces.'],
			['streamRemarks','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'streamRemarks can only contain Alphabets and Spaces.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'streamID' => 'ID',
            'streamName' => 'Name',
            'streamStatus' => 'Status',
            'streamCreatedDate' => 'Created Date',
        ];
    }
}
