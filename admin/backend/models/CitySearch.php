<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\City;

/**
 * CitySearch represents the model behind the search form of `backend\models\City`.
 */
class CitySearch extends City
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['cityID', 'countryID', 'stateID'], 'integer'],
            [['cityID', 'countryID', 'stateID','cityName', 'cityStatus', 'cityCreatedDate', 'cityLatitude', 'cityLongitude'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('state');
		$query->JoinWith('country');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cityID' => $this->cityID,
          //  'countryID' => $this->countryID,
           // 'stateID' => $this->stateID,
            'cityCreatedDate' => $this->cityCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'cityName', $this->cityName])
            ->andFilterWhere(['like', 'country.countryName', $this->countryID])
            ->andFilterWhere(['like', 'state.stateName', $this->stateID])
            ->andFilterWhere(['like', 'cityStatus', $this->cityStatus])
            ->andFilterWhere(['like', 'cityLatitude', $this->cityLatitude])
            ->andFilterWhere(['like', 'cityLongitude', $this->cityLongitude]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('cityID DESC');
        return $dataProvider;
    }
}
