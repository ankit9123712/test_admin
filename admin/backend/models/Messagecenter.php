<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "messagecenter".
 *
 * @property integer $messagecenterID
 * @property string $messagecenterType
 * @property string $messagecenterTitle
 * @property string $messagecenter
 * @property string $messagecenterTo
 * @property string $messagecenterStartDate
 * @property string $messagecenterEndDate
 * @property string $messagecenterStatus
 * @property string $messagecenterCreatedDate
 */
class Messagecenter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messagecenter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['messagecenterType', 'messagecenterTitle', 'messagecenter'], 'required'],
            [['messagecenterType', 'messagecenterTo', 'messagecenterStatus'], 'string'],
            [['messagecenterStartDate', 'messagecenterEndDate', 'messagecenterCreatedDate'], 'safe'],
            [['messagecenterTitle'], 'string', 'max' => 100],
            [['messagecenter'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'messagecenterID' => 'ID',
            'messagecenterType' => 'Type',
            'messagecenterTitle' => 'Title',
            'messagecenter' => 'Message Center',
            'messagecenterTo' => 'To',
            'messagecenterStartDate' => 'Start Date',
            'messagecenterEndDate' => 'End Date',
            'messagecenterStatus' => 'Status',
            'messagecenterCreatedDate' => 'Created Date',
        ];
    }
}
