<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "useranswers".
 *
 * @property integer $answerID
 * @property integer $userID
 * @property integer $queID
 * @property string $answerAnswer
 * @property string $answerCorrectAnswer
 * @property string $answerIsCorrect
 * @property string $answerIsVerified
 * @property integer $facultyID
 * @property string $answerSubmittedDate
 * @property string $answerFor
 * @property integer $lessionID
 * @property integer $examID
 */
class Useranswers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'useranswers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'queID', 'answerAnswer', 'answerCorrectAnswer'], 'required'],
            [['userID', 'queID', 'facultyID', 'lessionID', 'examID'], 'integer'],
            [['answerIsCorrect', 'answerIsVerified', 'answerFor'], 'string'],
            [['answerSubmittedDate'], 'safe'],
            [['answerAnswer', 'answerCorrectAnswer'], 'string', 'max' => 4000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'answerID' => 'ID',
            'userID' => 'User',
            'queID' => 'Questions',
            'answerAnswer' => 'Answer',
            'answerCorrectAnswer' => 'Correct Answer',
            'answerIsCorrect' => 'Is Correct',
            'answerIsVerified' => 'Is Verified',
            'facultyID' => 'Faculty',
            'answerSubmittedDate' => 'Submitted Date',
            'answerFor' => 'For',
            'lessionID' => 'Lession',
            'examID' => 'Exam',
        ];
    }
	
	public function getQuestions()
    {
        return $this->hasOne(Questions::className(),['queID'=>'queID']);
    }
	public function getUsers()
    {
        return $this->hasOne(Users::className(),['userID'=>'userID']);
    }
	public function getLesson()
    {
        return $this->hasOne(Lesson::className(),['LessonID'=>'lessionID']);
    }
	public function getExams()
    {
        return $this->hasOne(Exams::className(),['examID'=>'examID']);
    }
	public function getFaculty()
    {
        return $this->hasOne(Faculty::className(),['facultyID'=>'facultyID']);
    }
}
