<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "courses".
 *
 * @property int $courseID  
 * @property int $streamID
 * @property int $coursestdID
 * @property string $courseName
 * @property string $courseImage
 * @property int $courseDuration
 * @property int $courseMinAge
 * @property int $courseMaxAge
 * @property string $courseFree
 * @property string $courseDescription
 * @property string $courseEligiblity
 * @property string $courseCareer
 * @property string $coursePublished
 * @property string $courseStatus
 * @property string $courseCreatedDate
 */
class Courses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamID', 'coursestdID', 'courseName','courseDuration', 'courseMinAge', 'courseMaxAge', 'courseFree', 'courseDescription', 'courseEligiblity', 'courseCareer'], 'required'],
           // [['courseName'], 'unique'],
			['courseName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'Course Name can only contain Alphabets and Spaces.'],
			[['courseName'], 'string','min'=>3, 'max' => 100],
			[['courseDuration', 'courseMinAge', 'courseMaxAge'], 'integer'],
            [['courseFree', 'courseDescription', 'courseEligiblity', 'courseCareer', 'coursePublished', 'courseStatus'], 'string'],
            [['courseCreatedDate'], 'safe'],
            [['courseImage'], 'string', 'max' => 100],
            [['streamID', 'coursestdID', 'courseName'], 'unique', 'targetAttribute' => ['streamID', 'coursestdID', 'courseName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'courseID' => 'CourseID',
            'streamID' => 'Stream',
            'coursestdID' => 'Course Std',
            'courseName' => 'Course Name',
            'courseImage' => 'Course Image',
            'courseDuration' => 'Duration (Months)',
            'courseMinAge' => 'Min Age',
            'courseMaxAge' => 'Max Age',
            'courseFree' => 'Course Free',
            'courseDescription' => 'Description',
            'courseEligiblity' => 'Eligiblity',
            'courseCareer' => 'Career',
            'coursePublished' => 'Published',
            'courseStatus' => 'Status',
            'courseCreatedDate' => 'Created Date',
        ];
    }
	public function getStream()
	{
		return $this->hasOne(Stream::className(),['streamID'=>'streamID']);
	}
	public function getCoursestd()
	{
		return $this->hasOne(Coursestd::className(),['coursestdID'=>'coursestdID']);
	}
}
