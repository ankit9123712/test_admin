<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Practicebank;

/**
 * PracticebankSearch represents the model behind the search form of `backend\models\Practicebank`.
 */
class PracticebankSearch extends Practicebank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['PracticeBankID', 'moduleID', 'PracticeBankNo'], 'integer'],
            [['PracticeBankID', 'moduleID', 'PracticeBankNo','PracticeBankName', 'PracticeBankDescription', 'PracticeBankType', 'PracticeBankQuestionFile', 'PracticeBankAnswerFile', 'PracticeBankStatus', 'PracticeBankCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Practicebank::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('coursemodule');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PracticeBankID' => $this->PracticeBankID,
         //   'moduleID' => $this->moduleID,
            'PracticeBankNo' => $this->PracticeBankNo,
            'PracticeBankCreatedDate' => $this->PracticeBankCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'PracticeBankName', $this->PracticeBankName])
		  ->andFilterWhere(['like', 'coursemodule.moduleName', $this->moduleID])
            ->andFilterWhere(['like', 'PracticeBankDescription', $this->PracticeBankDescription])
            ->andFilterWhere(['like', 'PracticeBankType', $this->PracticeBankType])
            ->andFilterWhere(['like', 'PracticeBankQuestionFile', $this->PracticeBankQuestionFile])
            ->andFilterWhere(['like', 'PracticeBankAnswerFile', $this->PracticeBankAnswerFile])
            ->andFilterWhere(['like', 'PracticeBankStatus', $this->PracticeBankStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
		$query->orderby('PracticeBankID DESC');
        return $dataProvider;
    }
}
