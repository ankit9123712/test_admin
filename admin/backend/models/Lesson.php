<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "lesson".
 *
 * @property integer $LessonID
 * @property integer $moduleID
 * @property integer $LessonNo
 * @property string $LessonName
 * @property string $LessonDescription
 * @property string $lessionMarkingSystem
 * @property string $lessionQuickTips
 * @property string $LessonType
 * @property string $LessonFileType
 * @property string $LessonFile
 * @property string $LessonFileURL
 * @property integer $VideoFileCount
 * @property integer $ContentFileCount
 * @property string $LessonStatus
 * @property string $LessonCreatedDate
 */
class Lesson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lesson';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['moduleID', 'LessonNo', 'LessonName', 'LessonDescription', 'LessonType','teachingfacultyIDs','gradingfacultyIDs'], 'required'],
            [['moduleID', 'LessonNo', 'VideoFileCount', 'ContentFileCount'], 'integer'],
            [['LessonDescription', 'lessionMarkingSystem', 'lessionQuickTips', 'LessonType', 'LessonFileType', 'LessonStatus'], 'string'],
            [['LessonCreatedDate','LessonType'], 'safe'],
            [['LessonName', 'LessonFile'], 'string', 'max' => 100,'min' => 2],
            [['LessonFileURL'], 'string', 'max' => 300],
            [['moduleID', 'LessonName'], 'unique', 'targetAttribute' => ['moduleID', 'LessonName'], 'message' => 'The combination of Module and Lesson Name has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'LessonID' => 'ID',
            'moduleID' => 'Module',
            'LessonNo' => 'Lesson No',
            'LessonName' => 'Name',
            'LessonDescription' => 'Description',
            'lessionMarkingSystem' => 'Marking System',
            'lessionQuickTips' => 'Quick Tips',
            'LessonType' => 'Type',
            'LessonType' => 'Type',
            'LessonFileType' => 'File Type',
            'LessonFile' => 'File',
            'LessonFileURL' => 'File URL',
            'VideoFileCount' => 'Video File Count',
            'ContentFileCount' => 'Content File Count',
            'LessonStatus' => 'Status',
            'LessonCreatedDate' => 'Created Date',
			 'teachingfacultyIDs' => 'Teaching Faculty',
            'gradingfacultyIDs' => 'Grading Faculty',
        ];
    }
	public function getCoursemodule()
    {
        return $this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
	public function getTeachingfaculty()
    {
     
        //return $this->hasMany(Dieticiancategory::className(),['dietcatID'=>'dietcatIDs']);
		$query = new \yii\db\Query();
		$query	->select(['facultyFullName'])  
				->from('lessonfaculty')
				->where(['lessionID' => $this->LessonID,'lessonfacultyType'=>'Teaching'])
				->join(	'INNER JOIN', 
						'faculty',
						'faculty.facultyID = lessonfaculty.facultyID'
				);
				
		$command = $query->createCommand();
		$data = $command->queryAll();
		for ($x = 0; $x <= count($data)-1; $x++)	
		{
			$categoryNames[] = $data[$x]["facultyFullName"];
		}
		return  implode(",",$categoryNames);
    }
    public function getGradingfaculty()
    {
     
        //return $this->hasMany(Dieticiancategory::className(),['dietcatID'=>'dietcatIDs']);
		$query = new \yii\db\Query();
		$query	->select(['facultyFullName'])  
				->from('lessonfaculty')
				->where(['lessionID' => $this->LessonID,'lessonfacultyType'=>'Grading'])
				->join(	'INNER JOIN', 
						'faculty',
						'faculty.facultyID = lessonfaculty.facultyID'
				);
				
		$command = $query->createCommand();
		$data = $command->queryAll();
		for ($x = 0; $x <= count($data)-1; $x++)	
		{
			$categoryNames[] = $data[$x]["facultyFullName"];
		}
		return  implode(",",$categoryNames);
    }
	public function getQuestioncount($LessonID)
    {
		$res = Questions::find('queID')->where('lessionID='.$LessonID)->asArray()->all();
        return count($res);//$this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
}
