<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property integer $languageID
 * @property string $languageName
 * @property string $languageStatus
 * @property string $languageCreatedDate
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['languageName'], 'required'],
            [['languageStatus'], 'string'],
            [['languageCreatedDate'], 'safe'],
            [['languageName'], 'string', 'max' => 100],
            [['languageName'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'languageID' => 'ID',
            'languageName' => 'Language Name',
            'languageStatus' => 'Status',
            'languageCreatedDate' => 'Created Date',
        ];
    }
}
