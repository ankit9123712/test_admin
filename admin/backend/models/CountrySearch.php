<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Country;

/**
 * CountrySearch represents the model behind the search form of `backend\models\Country`.
 */
class CountrySearch extends Country
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countryID'], 'integer'],
            [['countryName', 'countryShortCode', 'countryISO2Code', 'countryCurrencySymbol', 'countryDialCode', 'countryStatus', 'countryCreatedDate', 'countryLatitude', 'countryLongitude'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Country::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'countryID' => $this->countryID,
            'countryCreatedDate' => $this->countryCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'countryName', $this->countryName])
            ->andFilterWhere(['like', 'countryShortCode', $this->countryShortCode])
            ->andFilterWhere(['like', 'countryISO2Code', $this->countryISO2Code])
            ->andFilterWhere(['like', 'countryCurrencySymbol', $this->countryCurrencySymbol])
            ->andFilterWhere(['like', 'countryDialCode', $this->countryDialCode])
            ->andFilterWhere(['like', 'countryFlagImage', $this->countryFlagImage])
            ->andFilterWhere(['like', 'countryStatus', $this->countryStatus])
            ->andFilterWhere(['like', 'countryLatitude', $this->countryLatitude])
            ->andFilterWhere(['like', 'countryLongitude', $this->countryLongitude]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
		$query->orderby('countryID DESC');
        return $dataProvider;
    }
}
