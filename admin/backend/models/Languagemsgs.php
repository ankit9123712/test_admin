<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "languagemsgs".
 *
 * @property integer $langMsgID
 * @property integer $languageID
 * @property string $langMsgKey
 * @property string $langMsgValue
 * @property string $langMsgStatus
 */
class Languagemsgs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languagemsgs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['languageID', 'langMsgKey', 'langMsgValue'], 'required'],
            [['languageID'], 'integer'],
            [['langMsgStatus'], 'string'],
            [['langMsgKey'], 'string', 'max' => 250],
            [['langMsgValue'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'langMsgID' => 'ID',
            'languageID' => 'Language',
            'langMsgKey' => 'Message Key',
            'langMsgValue' => 'Message Value',
            'langMsgStatus' => 'Status',
        ];
    }
	public function getLanguage()
    {
        return $this->hasOne(Language::className(),['languageID'=>'languageID']);
    }
}
