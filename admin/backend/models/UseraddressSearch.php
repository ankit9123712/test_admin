<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Useraddress;

/**
 * UseraddressSearch represents the model behind the search form of `app\models\Useraddress`.
 */
class UseraddressSearch extends Useraddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['addressID', 'userID', 'cityID', 'stateID', 'countryID'], 'integer'],
            [['addressTitle', 'addressContactDesgination', 'addressAddressLine1', 'addressAddressLine2', 'addressPincode', 'addressGST', 'addressPAN', 'addressType', 'addressIsDefault', 'addressCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Useraddress::find();

        if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'addressID' => $this->addressID,
            'userID' => $this->userID,
            'cityID' => $this->cityID,
            'stateID' => $this->stateID,
            'countryID' => $this->countryID,
            'addressCreatedDate' => $this->addressCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'addressTitle', $this->addressTitle])
            ->andFilterWhere(['like', 'addressContactDesgination', $this->addressContactDesgination])
            ->andFilterWhere(['like', 'addressAddressLine1', $this->addressAddressLine1])
            ->andFilterWhere(['like', 'addressAddressLine2', $this->addressAddressLine2])
            ->andFilterWhere(['like', 'addressPincode', $this->addressPincode])
            ->andFilterWhere(['like', 'addressGST', $this->addressGST])
            ->andFilterWhere(['like', 'addressPAN', $this->addressPAN])
            ->andFilterWhere(['like', 'addressType', $this->addressType])
            ->andFilterWhere(['like', 'addressIsDefault', $this->addressIsDefault]);
		
        return $dataProvider;
    }
}
