<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Messagecenter;

/**
 * MessagecenterSearch represents the model behind the search form of `backend\models\Messagecenter`.
 */
class MessagecenterSearch extends Messagecenter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['messagecenterID'], 'integer'],
            [['messagecenterType', 'messagecenterTitle', 'messagecenter', 'messagecenterTo', 'messagecenterStartDate', 'messagecenterEndDate', 'messagecenterStatus', 'messagecenterCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Messagecenter::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'messagecenterID' => $this->messagecenterID,
            'messagecenterStartDate' => $this->messagecenterStartDate,
            'messagecenterEndDate' => $this->messagecenterEndDate,
            'messagecenterCreatedDate' => $this->messagecenterCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'messagecenterType', $this->messagecenterType])
            ->andFilterWhere(['like', 'messagecenterTitle', $this->messagecenterTitle])
            ->andFilterWhere(['like', 'messagecenter', $this->messagecenter])
            ->andFilterWhere(['like', 'messagecenterTo', $this->messagecenterTo])
            ->andFilterWhere(['like', 'messagecenterStatus', $this->messagecenterStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('messagecenterID DESC');
        return $dataProvider;
    }
}
