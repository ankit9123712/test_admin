<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Notimessages;

/**
 * NotimessagesSearch represents the model behind the search form of `backend\models\Notimessages`.
 */
class NotimessagesSearch extends Notimessages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msgId'], 'integer'],
            [['msgTitle', 'msgDescription', 'msgStatus', 'msgUserType', 'msgDate', 'msgCreatedDate', 'stateID', 'cityID' ], 'safe'],
			 
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notimessages::find();

        // add conditions that should always apply here

         if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'msgId' => $this->msgId,
            'msgDate' => $this->msgDate,
            'msgCreatedDate' => $this->msgCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'msgTitle', $this->msgTitle])
            ->andFilterWhere(['like', 'msgDescription', $this->msgDescription])
            ->andFilterWhere(['like', 'msgStatus', $this->msgStatus])
            ->andFilterWhere(['like', 'msgUserType', $this->msgUserType])
            ;		
        if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		
		return $dataProvider;
    }
}
