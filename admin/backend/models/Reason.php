<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reason".
 *
 * @property int $reasonID
 * @property string $reasonName
 * @property string $reasonStatus
 * @property string $reasonCreatedDate
 */
class Reason extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reason';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reasonName', 'reasonStatus'], 'required'],
            [['reasonStatus'], 'string'],
            [['reasonCreatedDate'], 'safe'],
            [['reasonName'], 'string', 'max' => 100],
            [['reasonName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reasonID' => 'ID',
            'reasonName' => 'Reason',
            'reasonStatus' => 'Status',
            'reasonCreatedDate' => 'Created Date',
        ];
    }
}
