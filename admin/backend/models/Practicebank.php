<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "practicebank".
 *
 * @property integer $PracticeBankID
 * @property integer $moduleID
 * @property integer $PracticeBankNo
 * @property string $PracticeBankName
 * @property string $PracticeBankDescription
 * @property string $PracticeBankType
 * @property string $PracticeBankQuestionFile
 * @property string $PracticeBankAnswerFile
 * @property string $PracticeBankStatus
 * @property string $PracticeBankCreatedDate
 */
class Practicebank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'practicebank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['moduleID', 'PracticeBankNo', 'PracticeBankName', 'PracticeBankDescription', 'PracticeBankType'], 'required'],
            [['moduleID', 'PracticeBankNo'], 'integer'],
            [['PracticeBankDescription', 'PracticeBankType', 'PracticeBankStatus'], 'string'],
            [['PracticeBankCreatedDate'], 'safe'],
			['PracticeBankName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => ' Practice Bank Name you entered is incorrect.'],
            [['PracticeBankName'], 'string', 'max' => 100,'min' => 2],
            [['PracticeBankQuestionFile', 'PracticeBankAnswerFile'], 'string', 'max' => 300],
            [['moduleID', 'PracticeBankName'], 'unique', 'targetAttribute' => ['moduleID', 'PracticeBankName'], 'message' => 'The combination of Module I D and Practice Bank Name has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'PracticeBankID' => 'ID',
            'moduleID' => 'Module',
            'PracticeBankNo' => 'Bank No',
            'PracticeBankName' => 'Bank Name',
            'PracticeBankDescription' => 'Bank Description',
            'PracticeBankType' => 'Bank Type',
            'PracticeBankQuestionFile' => 'Bank Question File',
            'PracticeBankAnswerFile' => 'Bank Answer File',
            'PracticeBankStatus' => 'Bank Status',
            'PracticeBankCreatedDate' => 'Bank Created Date',
        ];
    }
	public function getCoursemodule()
    {
        return $this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
}
