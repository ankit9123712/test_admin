<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Questions;

/**
 * QuestionsSearch represents the model behind the search form of `backend\models\Questions`.
 */
class QuestionsSearch extends Questions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['queID', 'moduleID', 'lessionID', 'examID', 'queDifficultyevel'], 'integer'],
            [['queID', 'moduleID', 'lessionID', 'examID', 'queDifficultyevel','queFor', 'queType', 'queQuestion', 'queSolution', 'queDisplayType', 'queStatus', 'queOption1', 'queOption2', 'queOption3', 'queOption4', 'queCorrectAns','queFile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Questions::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('coursemodule');
		$query->JoinWith('lesson');
		$query->JoinWith('exams');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'queID' => $this->queID,
          //  'moduleID' => $this->moduleID,
           // 'lessionID' => $this->lessionID,
           // 'examID' => $this->examID,
            'queDifficultyevel' => $this->queDifficultyevel,
        ]);

        $query->andFilterWhere(['like', 'queFor', $this->queFor])
            ->andFilterWhere(['like', 'coursemodule.moduleName', $this->moduleID])
            ->andFilterWhere(['like', 'lesson.LessonName', $this->lessionID])
            ->andFilterWhere(['like', 'exams.examName', $this->examID])
            ->andFilterWhere(['like', 'queType', $this->queType])
            ->andFilterWhere(['like', 'queQuestion', $this->queQuestion])
            ->andFilterWhere(['like', 'queSolution', $this->queSolution])
            ->andFilterWhere(['like', 'queDisplayType', $this->queDisplayType])
            ->andFilterWhere(['like', 'queStatus', $this->queStatus])
            ->andFilterWhere(['like', 'queOption1', $this->queOption1])
            ->andFilterWhere(['like', 'queOption2', $this->queOption2])
            ->andFilterWhere(['like', 'queOption3', $this->queOption3])
            ->andFilterWhere(['like', 'queOption4', $this->queOption4])
            ->andFilterWhere(['like', 'queCorrectAns', $this->queCorrectAns]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
		$query->orderby('queID ASC');
        return $dataProvider;
    }
}
