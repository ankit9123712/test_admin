<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "coursestd".
 *
 * @property int $coursestdID
 * @property int $streamID
 * @property string $coursestdName
 * @property string $coursestdDuration
 * @property string $coursestdRemarks
 * @property string $coursestdStatus
 * @property string $coursestdCreatedDate
 */
class Coursestd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coursestd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamID', 'coursestdName','coursestdStatus'], 'required'],
            [['streamID'], 'integer'],
            [['coursestdStatus'], 'string'],
            [['coursestdCreatedDate'], 'safe'],
           [['coursestdName'], 'string','min'=>3, 'max' => 100],
            [['coursestdDuration'], 'string', 'max' => 20],
            [['coursestdRemarks'], 'string', 'max' => 200],
            [['coursestdName', 'streamID'], 'unique', 'targetAttribute' => ['coursestdName', 'streamID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coursestdID' => 'ID',
            'streamID' => 'Stream',
            'coursestdName' => 'Standard',
            'coursestdDuration' => 'Duration',
            'coursestdRemarks' => 'Remarks',
            'coursestdStatus' => 'Status',
            'coursestdCreatedDate' => 'Created Date',
        ];
    }
	public function getStream()
    {
        return $this->hasOne(Stream::className(),['streamID'=>'streamID']);
    }
}
