<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "languagelabel".
 *
 * @property integer $langLabelID
 * @property string $langLabelKey
 * @property integer $languageID
 * @property string $langLabelValue
 * @property string $langLabelModule
 * @property string $langLabelStatus
 */
class Languagelabel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languagelabel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['langLabelKey', 'langLabelValue'], 'required'],
            [['languageID'], 'integer'],
            [['langLabelStatus'], 'string'],
            [['langLabelKey', 'langLabelValue'], 'string', 'max' => 250],
            [['langLabelModule'], 'string', 'max' => 100],
            [['languageID'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['languageID' => 'languageID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'langLabelID' => 'ID',
            'langLabelKey' => 'Label Key',
            'languageID' => 'Language',
            'langLabelValue' => 'Label Value',
            'langLabelModule' => 'Module',
            'langLabelStatus' => 'Status',
        ];
    }
	public function getLanguage()
    {
        return $this->hasOne(Language::className(),['languageID'=>'languageID']);
    }
}
