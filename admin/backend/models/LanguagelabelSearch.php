<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Languagelabel;

/**
 * LanguagelabelSearch represents the model behind the search form of `app\models\Languagelabel`.
 */
class LanguagelabelSearch extends Languagelabel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['langLabelID', 'languageID'], 'integer'],
            [['langLabelKey', 'langLabelValue', 'langLabelModule', 'langLabelStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Languagelabel::find();

        // add conditions that should always apply here

        if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'langLabelID' => $this->langLabelID,
            'languageID' => $this->languageID,
        ]);

        $query->andFilterWhere(['like', 'langLabelKey', $this->langLabelKey])
            ->andFilterWhere(['like', 'langLabelValue', $this->langLabelValue])
            ->andFilterWhere(['like', 'langLabelModule', $this->langLabelModule])
            ->andFilterWhere(['like', 'langLabelStatus', $this->langLabelStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
		$query->orderby('langLabelID DESC');	
        return $dataProvider;
    }
}
