<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "coursesubjects".
 *
 * @property int $coursesubjID
 * @property int $courseID
 * @property string $coursesubjName
 * @property string $coursesubjDescription
 * @property int $coursesubjChapters
 * @property string $coursesubjStatus
 * @property string $coursesubjCreatedDate
 */
class coursesubjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coursesubjects';
    }

    /**
     * {@inheritdoc}
     */
	  public function rules()
    {
      if(isset($_REQUEST['id']))
         {
        return [
            [['coursesubjName', 'coursesubjDescription','coursesubjColor'], 'required'],
           ['coursesubjName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'Course Name can only contain Alphabets and Spaces.'],
		   [['coursesubjName'], 'string','min'=>3, 'max' => 100],
		     //['coursesubjColor','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'Course Name can only contain Alphabets and Spaces.'],
		 //  [['coursesubjColor'], 'string','min'=>3, 'max' => 100],
		   [['courseID'], 'integer'],
            [['coursesubjDescription', 'coursesubjStatus'], 'string'],
            [['coursesubjCreatedDate'], 'safe'],
            [['coursesubjDescription'], 'string', 'max' => 100],
            [['courseID', 'coursesubjName'], 'unique', 'targetAttribute' => ['courseID', 'coursesubjName']],
        ];
    }else{
		        return [
            [[ 'coursesubjName', 'coursesubjDescription','coursesubjIcon','coursesubjColor'], 'required'],
           ['coursesubjName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'Course Name can only contain Alphabets and Spaces.'],
		   [['coursesubjName'], 'string','min'=>3, 'max' => 100],
		  //  ['coursesubjColor','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => 'Course Name can only contain Alphabets and Spaces.'],
		   //[['coursesubjColor'], 'string','min'=>3, 'max' => 100],
		  [['courseID'], 'integer'],
            [['coursesubjDescription', 'coursesubjStatus'], 'string'],
            [['coursesubjCreatedDate'], 'safe'],
            [['coursesubjDescription'], 'string', 'max' => 100],
            [['courseID', 'coursesubjName'], 'unique', 'targetAttribute' => ['courseID', 'coursesubjName']],
        ];
}
	}
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coursesubjID' => 'Coursesubj ID',
            'courseID' => 'Course',
            'coursesubjName' => 'Name',
            'coursesubjDescription' => 'Description',
            'coursesubjChapters' => 'Chapters',
            'coursesubjStatus' => 'Status',
            'coursesubjCreatedDate' => 'Created Date',
'coursesubjIcon'=>'Course Subject Icon',
'coursesubjColor'=>'Course SubjColor',
        ];
    }
	public function getCourses()
	{
		return $this->hasOne(Courses::className(),['courseID'=>'courseID']);
	}
}
