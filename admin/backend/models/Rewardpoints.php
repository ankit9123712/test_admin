<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "rewardpoints".
 *
 * @property string $userFullName
 * @property string $userMobile
 * @property string $userEmail
 * @property string $ExamRewordPoints
 * @property string $ReferralRewordPoints
 * @property int $userID
 */
class Rewardpoints extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rewardpoints';
    }
public static function primaryKey()
	{
		return ['userID'];
	}
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userFullName', 'userMobile', 'userEmail'], 'required'],
            [['ExamRewordPoints', 'ReferralRewordPoints'], 'number'],
            [['userID'], 'integer'],
            [['userFullName', 'userEmail'], 'string', 'max' => 100],
            [['userMobile'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userFullName' => 'Name',
            'userMobile' => 'Mobile No',
            'userEmail' => 'EmailID',
            'ExamRewordPoints' => 'Exam Reword Points',
            'ReferralRewordPoints' => 'Referral Reword Points',
            'userID' => 'User I D',
        ];
    }
}
