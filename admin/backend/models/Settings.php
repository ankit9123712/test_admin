<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $settingsID
 * @property string $settingsEmailFrom
 * @property string $settingsEmailTo
 * @property string $settingsEmailGateway
 * @property string $settingEmailID
 * @property string $settingsEmailPass
 * @property string $settingsSSL
 * @property string $settingsTLS
 * @property integer $settingEmailPort
 * @property string $settingSenderName
 * @property string $settingsSMSURL
 * @property string $settingSMSUser
 * @property string $settingsSMSPassword
 * @property string $settingsSMSPort
 * @property string $settingsGAKey
 * @property string $settingsGMapKey
 * @property string $settinngsSalesNo
 * @property integer $settingsDefCountry
 * @property integer $settingsDefState
 * @property integer $settingsDefCity
 * @property integer $settingsUserResetPinLinkExpHr
 * @property integer $settingsLogDeleteDays
 * @property string $settingsTollFree
 * @property integer $settingsOrderReturnTimeMinutes
 * @property string $settingsMasterOtp
 * @property string $settingsGSTNo
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['settingsEmailGateway', 'settingEmailID', 'settingsEmailPass', 'settingsSSL', 'settingsTLS', 'settingEmailPort', 'settingSenderName', 'settingSMSUser', 'settingsSMSPassword', 'settingsSMSPort', 'settingsGAKey', 'settingsGMapKey', 'settingsSalesNo', 'settingsDefCountry', 'settingsDefState', 'settingsDefCity','settingPGMode','settingsPGSandboxUrl','settingPGSandboxCustomerKey','settingsPGSandboxCustomerAuth','settingsPGLiveUrl','settingPGLiveCustomerKey','settingPGLiveCustomerAuth','settingsUserResetPinLinkExpHr','settingsLogDeleteDays','settingsOrderReturnTimeMinutes','settingsMasterOtp','settingsGST','settingsSupportEmail','settingsSupportMobile','settingsDefaultMarkup','settingsConvienceCharge','setting1USD'], 'required'],
            [['settingsSSL', 'settingsTLS'], 'string'],
            [['settingEmailPort', 'settingsDefCountry', 'settingsDefState', 'settingsDefCity', 'settingsUserResetPinLinkExpHr', 'settingsLogDeleteDays', 'settingsOrderReturnTimeMinutes'], 'integer'],
            [['settingsEmailFrom', 'settingsEmailTo', 'settingsEmailGateway', 'settingEmailID'], 'string', 'max' => 100],
            [['settingsEmailPass', 'settingsSalesNo', 'settingsTollFree','settingsSupportNo'], 'string', 'max' => 20],
            [['settingSenderName', 'settingSMSUser', 'settingsSMSPassword'], 'string', 'max' => 50],
            [['settingsSMSURL', 'settingsGAKey', 'settingsGMapKey'], 'string', 'max' => 255],
			[['settingsHomeAlert', 'settingsOtherMessage'], 'string', 'max' => 500],
			
            [['settingsSMSPort'], 'string', 'max' => 5],
            [['settingsMasterOtp'], 'string', 'max' => 6],
            [['settingsGSTNo'], 'string', 'max' => 25], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'settingsID' => 'ID',
            'settingsEmailFrom' => 'Email From',
            'settingsEmailTo' => 'Email To',
            'settingsEmailGateway' => 'Email Gateway',
            'settingEmailID' => 'Email ID',
            'settingsEmailPass' => 'Email Password',
            'settingsSSL' => 'SSL',
            'settingsTLS' => 'TLS',
            'settingEmailPort' => 'Email Port',
            'settingSenderName' => 'Sender Name',
			'settingPGMode' => 'SMS URL',
            'settingsPGSandboxUrl' => 'PGSandbox Url',
            'settingPGSandboxCustomerKey' => 'PGSandbox Customer Key',
            'settingsPGSandboxCustomerAuth' => 'PGSandbox Customer Auth',
            'settingsPGLiveUrl' => 'PGLive Url',
            'settingPGLiveCustomerKey' => 'PGLive Customer Key',
            'settingPGLiveCustomerAuth' => 'PGLive Customer Authentication',
            'settingsUserResetPinLinkExpHr' => 'User Reset PinLink ExpHr',
			'settingsDefaultMarkup' => 'Default Markup',
			'settingsConvienceCharge' => 'Convience Charge',
			'setting1USD' => '1 USD',
            'settingsLogDeleteDays' => 'Log Delete Days',
			'settingsSMSGateway' => 'SMS Gateway',
			'settingsSMSSenderName'=>'SMS Sender Name',
			'settingsSMSUsername'=>'SMS User Name',
			'settingsSMSPassword'=>'SMS Password',
			'settingsSalesNo'=>'Sales No', 
            'settingsSupportNo'=>'Support No',
            'settingsSupportEmail'=>'Support Email',
			'settingsMapKey'=>'Map Key',
			'settingsAnyalticsKey'=>'Anyaltics Key',
            'settingsPushKey' => 'Push Key',
            'settingsMasterOtp' => 'Master Otp',
            'settingsGSTPer' => 'GST Per',
            'settingsGSTNo' => 'GST No',
			'settingsHomeAlert' => 'Home Screen Alert',
            'settingsOtherMessage' => 'Other Screen Alert',
			
        ];
    }
}
