<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Ieltsfiles;

/**
 * IeltsfilesSearch represents the model behind the search form of `backend\models\Ieltsfiles`.
 */
class IeltsfilesSearch extends Ieltsfiles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ieltsfileID'], 'integer'],
            [['ieltsfileType', 'ieltsfileName', 'ieltsfileStatus','ieltsDisplayName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ieltsfiles::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ieltsfileID' => $this->ieltsfileID,
        ]);

        $query->andFilterWhere(['like', 'ieltsfileType', $this->ieltsfileType])
            ->andFilterWhere(['like', 'ieltsfileName', $this->ieltsfileName])
            ->andFilterWhere(['like', 'ieltsfileStatus', $this->ieltsfileStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
			$query->orderby('ieltsfileID DESC');
        return $dataProvider;
    }
}
