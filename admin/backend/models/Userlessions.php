<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "userlessions".
 *
 * @property integer $userlessionID
 * @property integer $userID
 * @property integer $lessionID
 * @property integer $userlessionQuestions
 * @property integer $userlessionAnswered
 * @property integer $userlessionCorrectAnswers
 * @property integer $userlessionWrongAnswers
 */
class Userlessions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userlessions';
    }
	public $cityID,$stateID,$moduleID,$userlessionBand,$userType; 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'lessionID', 'userlessionQuestions', 'userlessionAnswered', 'userlessionWrongAnswers'], 'required'],
            [['userID', 'lessionID', 'userlessionQuestions', 'userlessionAnswered', 'userlessionCorrectAnswers', 'userlessionWrongAnswers'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userlessionID' => 'ID',
            'userID' => 'User',
            'lessionID' => 'Lession',
            'userlessionQuestions' => 'Questions',
            'userlessionAnswered' => 'Answered',
            'userlessionCorrectAnswers' => 'Correct Answers',
            'userlessionWrongAnswers' => 'Wrong Answers',
			'userlessionBand' => 'Bands',
        ];
    }
	public function getUsers()
    {
        return $this->hasOne(Users::className(),['userID'=>'userID']);
    }
	public function getLesson()
    {
        return $this->hasOne(Lesson::className(),['LessonID'=>'lessionID']);
    }
	public function getState()
    {
        return $this->hasOne(State::className(),['stateID'=>'stateID']);
    }
	public function getCity()
    {
        return $this->hasOne(City::className(),['cityID'=>'cityID']);
    }
	public function getCoursemodule()
    {
        return $this->hasOne(Coursemodule::className(),['moduleID'=>'moduleID']);
    }
}
