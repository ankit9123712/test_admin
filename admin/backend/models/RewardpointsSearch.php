<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Rewardpoints;

/**
 * RewardpointsSearch represents the model behind the search form of `backend\models\Rewardpoints`.
 */
class RewardpointsSearch extends Rewardpoints
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userFullName', 'userMobile', 'userEmail'], 'safe'],
            [['ExamRewordPoints', 'ReferralRewordPoints'], 'number'],
            [['userID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rewardpoints::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ExamRewordPoints' => $this->ExamRewordPoints,
            'ReferralRewordPoints' => $this->ReferralRewordPoints,
            'userID' => $this->userID,
        ]);

        $query->andFilterWhere(['like', 'userFullName', $this->userFullName])
            ->andFilterWhere(['like', 'userMobile', $this->userMobile])
            ->andFilterWhere(['like', 'userEmail', $this->userEmail]);
		
        return $dataProvider;
    }
}
