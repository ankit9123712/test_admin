<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Subscriptionplan;

/**
 * SubscriptionplanSearch represents the model behind the search form of `backend\models\Subscriptionplan`.
 */
class SubscriptionplanSearch extends Subscriptionplan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['planID', 'moduleID', 'planDuration'], 'integer'],
            [['planID', 'moduleID', 'planDuration','planName', 'planDescription', 'planStatus', 'planCreatedDate'], 'safe'],
            [['planPrice', 'planDiscountPrice'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subscriptionplan::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('coursemodule');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'planID' => $this->planID,
        //    'moduleID' => $this->moduleID,
            'planPrice' => $this->planPrice,
            'planDiscountPrice' => $this->planDiscountPrice,
            'planDuration' => $this->planDuration,
            'planCreatedDate' => $this->planCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'planName', $this->planName])
            ->andFilterWhere(['like', 'planDescription', $this->planDescription])
            ->andFilterWhere(['like', 'coursemodule.moduleName', $this->moduleID])
            ->andFilterWhere(['like', 'planStatus', $this->planStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('planID DESC');
        return $dataProvider;
    }
}
