<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Courses;

/**
 * CoursesSearch represents the model behind the search form of `backend\models\Courses`.
 */
class CoursesSearch extends Courses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseID', 'streamID', 'coursestdID', 'courseDuration', 'courseMinAge', 'courseMaxAge'], 'integer'],
            [['courseName', 'courseImage', 'courseFree', 'courseDescription', 'courseEligiblity', 'courseCareer', 'coursePublished', 'courseStatus', 'courseCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Courses::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'courseID' => $this->courseID,
            'streamID' => $this->streamID,
            'coursestdID' => $this->coursestdID,
            'courseDuration' => $this->courseDuration,
            'courseMinAge' => $this->courseMinAge,
            'courseMaxAge' => $this->courseMaxAge,
            'courseCreatedDate' => $this->courseCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'courseName', $this->courseName])
            ->andFilterWhere(['like', 'courseImage', $this->courseImage])
            ->andFilterWhere(['like', 'courseFree', $this->courseFree])
            ->andFilterWhere(['like', 'courseDescription', $this->courseDescription])
            ->andFilterWhere(['like', 'courseEligiblity', $this->courseEligiblity])
            ->andFilterWhere(['like', 'courseCareer', $this->courseCareer])
            ->andFilterWhere(['like', 'coursePublished', $this->coursePublished])
            ->andFilterWhere(['like', 'courseStatus', $this->courseStatus]);
		$query->orderby('courseID DESC');
        return $dataProvider;
    }
}
