<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Faqtype;

/**
 * FaqtypeSearch represents the model behind the search form of `backend\models\Faqtype`.
 */
class FaqtypeSearch extends Faqtype
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['faqtypeID'], 'integer'],
            [['faqtypeName', 'faqtypeRemarks', 'faqtypeStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Faqtype::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'faqtypeID' => $this->faqtypeID,
        ]);

        $query->andFilterWhere(['like', 'faqtypeName', $this->faqtypeName])
            ->andFilterWhere(['like', 'faqtypeRemarks', $this->faqtypeRemarks])
            ->andFilterWhere(['like', 'faqtypeStatus', $this->faqtypeStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('faqtypeID DESC');
        return $dataProvider;
    }
}
