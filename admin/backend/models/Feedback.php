<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $feedbackID
 * @property string $feedbackName
 * @property string $feedbackEmail
 * @property string $feedbackFeedback
 * @property integer $userID
 * @property string $feedbackDate
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feedbackName', 'feedbackEmail', 'feedbackFeedback', 'userID'], 'required'],
            [['userID'], 'integer'],
            [['feedbackDate'], 'safe'],
            [['feedbackName', 'feedbackEmail'], 'string', 'max' => 100],
            [['feedbackFeedback'], 'string', 'max' => 4000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'feedbackID' => 'ID',
            'feedbackName' => 'Name',
            'feedbackEmail' => 'Email',
            'feedbackFeedback' => 'Feedback',
            'userID' => 'User',
            'feedbackDate' => 'Date',
        ];
    }
	
	public function getUsers()
    {
        return $this->hasOne(Users::className(),['userID'=>'userID']);
    }
}
