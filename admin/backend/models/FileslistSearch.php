<?php

namespace backend\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Fileslist;

/**
 * FileslistSearch represents the model behind the search form of `backend\models\Fileslist`.
 */
class FileslistSearch extends Fileslist
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fileID'], 'integer'],
            [['fileFile','fileActualName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fileslist::find();

        // add conditions that should always apply here

        if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fileID' => $this->fileID,
        ]);

        $query->andFilterWhere(['like', 'fileFile', $this->fileFile])
		->andFilterWhere(['like', 'fileActualName', $this->fileActualName]);
		
		$query->orderby('fileID DESC');
        return $dataProvider;
    }
}
