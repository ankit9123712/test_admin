<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "chapterfiles".
 *
 * @property int $chapterfileID
 * @property int $coursechapterID
 * @property string $chapterfileFileType
 * @property string $chapterfileFile
 * @property string $chapterfileStatus
 */
class Chapterfiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chapterfiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
		  if(isset($_REQUEST['id']))
         {
        return [
            [['coursechapterID', 'chapterfileFileType','chapterfileStatus'], 'required'],
            [['coursechapterID'], 'integer'],
		    [['chapterfile'], 'safe'],

            [['chapterfileFileType', 'chapterfileStatus'], 'string'],
            [['chapterfileFile'], 'string', 'max' => 300],
        ];
		 }else{
			 
			  return [
            [['coursechapterID', 'chapterfileFileType', 'chapterfileFile', 'chapterfileStatus'], 'required'],
            [['coursechapterID'], 'integer'],
		    [['chapterfile'], 'safe'],

            [['chapterfileFileType', 'chapterfileStatus'], 'string'],
            [['chapterfileFile'], 'string', 'max' => 300],
        ];
    }
	}
	

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'chapterfileID' => 'I D',
            'coursechapterID' => 'Coursechapter I D',
            'chapterfileFileType' => 'File Type',
            'chapterfileFile' => 'Content (Videos, Pdf)',
            'chapterfileStatus' => 'Status',
            'chapterfile' => 'Name',
        ];
    }


   
	
	
	
}
