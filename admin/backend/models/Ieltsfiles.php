<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ieltsfiles".
 *
 * @property integer $ieltsfileID
 * @property string $ieltsfileType
 * @property string $ieltsfileName
 * @property string $ieltsfileStatus
 */
class Ieltsfiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ieltsfiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ieltsfileType','ieltsDisplayName'], 'required'],
            [['ieltsfileType', 'ieltsfileStatus'], 'string'],
            [['ieltsfileName'], 'string', 'max' => 255],
			[['ieltsDisplayName'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ieltsfileID' => 'ID',
            'ieltsfileType' => 'Type',
            'ieltsfileName' => 'Name',
            'ieltsfileStatus' => 'Status',
			'ieltsDisplayName' => 'IELTS Display Name',
        ];
    }
}
