<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "helpdeskquery".
 *
 * @property integer $helpdeskqueryID
 * @property integer $userID
 * @property string $heldeskqueryQuery
 * @property string $helpdeskqueryStatus
 * @property string $helpdeskqueryCreatedDate
 */
class Helpdeskquery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'helpdeskquery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'heldeskqueryQuery'], 'required'],
            [['userID'], 'integer'],
            [['helpdeskqueryStatus'], 'string'],
            [['helpdeskqueryCreatedDate'], 'safe'],
            [['heldeskqueryQuery'], 'string', 'max' => 4000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'helpdeskqueryID' => 'ID',
            'userID' => 'User',
            'heldeskqueryQuery' => 'Query',
            'helpdeskqueryStatus' => 'Status',
            'helpdeskqueryCreatedDate' => 'Created Date',
        ];
    }
	public function getUsers()
    {
        return $this->hasOne(Users::className(),['userID'=>'userID']);
    }
}
