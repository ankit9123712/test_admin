<?php

namespace backend\models;

use Yii;
use backend\models\City;
use backend\models\State;
use backend\models\Country;
/**
 * This is the model class for table "institute".
 *
 * @property int $instituteID
 * @property string $instituteName
 * @property string $instituteContactName
 * @property string $instituteEmailExtension
 * @property string $institutePhone
 * @property string $instituteAddress
 * @property string $institutePincode
 * @property int $countryID
 * @property int $stateID
 * @property int $cityID
 * @property string $instituteStatus
 * @property string $instituteCreatedDate
 */
class Institute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'institute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['instituteName', 'instituteContactName', 'instituteEmailExtension', 'institutePhone', 'instituteAddress', 'institutePincode', 'countryID', 'stateID', 'cityID', 'instituteStatus'], 'required'],
           [['instituteName','instituteContactName'], 'string', 'max' => 100,'min' => 3],
		     ['instituteName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => ' Institute Name you entered is incorrect.'],
		     ['instituteContactName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => ' Institute Contact Name you entered is incorrect.'],
		   [['countryID', 'stateID', 'cityID'], 'integer'],
            [['institutePhone'], 'match', 'pattern' => '/^(\+\d{1,3}[- ]?)?\d{10}$/', 'message' => 'Invalid Phone Number .'],
			[['institutePhone'], 'string','max' => 10],
			[['institutePhone'], 'number'],
			[['instituteStatus'], 'string'],
            [['instituteCreatedDate'], 'safe'],
          //  [['instituteName', 'instituteContactName'], 'string', 'max' => 100],
            [['instituteEmailExtension'], 'string', 'max' => 50],
           // [['institutePhone'], 'string', 'max' => 70],
    		[['instituteAddress'], 'match', 'pattern' => '/^[A-Za-z-, 0-9_.]+$/','message' => 'Invalid Address.'],
			[['instituteAddress'], 'string', 'max' => 250, 'min' => 20],
             ['institutePincode', 'string', 'max' => 6,'min' => 5],
			//[['institutePincode'], 'string', 'max' => 10],
            [['instituteName','institutePincode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'instituteID' => 'ID',
            'instituteName' => 'Institute Name',
            'instituteContactName' => 'Contact Person Name',
            'instituteEmailExtension' => 'Email Extension',
            'institutePhone' => 'Phone',
            'instituteAddress' => 'Address',
            'institutePincode' => 'Pincode',
            'countryID' => 'Country',
            'stateID' => 'State',
            'cityID' => 'City',
            'instituteStatus' => 'Status',
            'instituteCreatedDate' => 'Created Date',
        ];
    }
	public function getCountry()
    {
        return $this->hasOne(Country::className(),['countryID'=>'countryID']);
    }
	public function getState()
    {
        return $this->hasOne(State::className(),['stateID'=>'stateID']);
    }
	public function getCity()
    {
        return $this->hasOne(City::className(),['cityID'=>'cityID']);
    }
}
