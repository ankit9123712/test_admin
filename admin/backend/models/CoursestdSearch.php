<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Coursestd;

/**
 * CoursestdSearch represents the model behind the search form of `app\models\Coursestd`.
 */
class CoursestdSearch extends Coursestd
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coursestdID'], 'integer'],
            [['coursestdName', 'coursestdDuration', 'coursestdRemarks', 'coursestdStatus', 'coursestdCreatedDate', 'streamID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Coursestd::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('stream');
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'coursestdID' => $this->coursestdID,
           // 'streamID' => $this->streamID,
            'coursestdCreatedDate' => $this->coursestdCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'coursestdName', $this->coursestdName])
            ->andFilterWhere(['like', 'coursestdDuration', $this->coursestdDuration])
            ->andFilterWhere(['like', 'coursestdRemarks', $this->coursestdRemarks])
			->andFilterWhere(['like', 'streamName', $this->streamID])
            ->andFilterWhere(['like', 'coursestdStatus', $this->coursestdStatus]);
		$query->orderby('coursestdID DESC');
        return $dataProvider;
    }
}
