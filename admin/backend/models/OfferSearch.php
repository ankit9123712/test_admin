<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Offer;

/**
 * OfferSearch represents the model behind the search form of `backend\models\Offer`.
 */
class OfferSearch extends Offer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['offerID', 'offerMaxLimit', 'offerCodeQty'], 'integer'],
            [['offerID', 'offerMaxLimit', 'offerCodeQty','offerName', 'offerImage', 'offerDiscountType', 'offerStartDate', 'offerEndDate', 'offerCodeUse', 'offerCodeType', 'offerDescription', 'offerStatus', 'offerCreatedDate','offerExclusive'], 'safe'],
            [['offerDiscountValue', 'offerSalePrice'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Offer::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'offerID' => $this->offerID,
            'offerDiscountValue' => $this->offerDiscountValue,
            'offerStartDate' => $this->offerStartDate,
            'offerEndDate' => $this->offerEndDate,
            'offerMaxLimit' => $this->offerMaxLimit,
            'offerCodeQty' => $this->offerCodeQty,
            'offerSalePrice' => $this->offerSalePrice,
            'offerCreatedDate' => $this->offerCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'offerName', $this->offerName])
            ->andFilterWhere(['like', 'offerImage', $this->offerImage])
            ->andFilterWhere(['like', 'offerDiscountType', $this->offerDiscountType])
            ->andFilterWhere(['like', 'offerCodeUse', $this->offerCodeUse])
            ->andFilterWhere(['like', 'offerCodeType', $this->offerCodeType])
            ->andFilterWhere(['like', 'offerDescription', $this->offerDescription])
			->andFilterWhere(['like', 'offerExclusive', $this->offerExclusive])
            ->andFilterWhere(['like', 'offerStatus', $this->offerStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('offerID DESC');
        return $dataProvider;
    }
}
