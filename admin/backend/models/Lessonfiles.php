<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "lessonfiles".
 *
 * @property integer $LessonfileID
 * @property integer $LessonID
 * @property string $LessonfileFileType
 * @property string $LessonfileFile
 * @property string $Lessonfile
 * @property string $LessonfileStatus
 */
class Lessonfiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lessonfiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LessonID', 'LessonfileFileType', 'LessonfileStatus'], 'required'],
            [['LessonID'], 'integer'],
            [['LessonfileFileType', 'LessonfileStatus'], 'string'],
            [['LessonfileFile', 'Lessonfile'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'LessonfileID' => 'ID',
            'LessonID' => 'Lesson',
            'LessonfileFileType' => 'File Type',
            'LessonfileFile' => 'File',
            'Lessonfile' => 'Lesson File',
            'LessonfileStatus' => 'Status',
        ];
    }
	
	public function getLesson()
    {
        return $this->hasOne(Lesson::className(),['LessonID'=>'LessonID']);
    }
}
