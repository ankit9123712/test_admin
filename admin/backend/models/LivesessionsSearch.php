<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Livesessions;

/**
 * LivesessionsSearch represents the model behind the search form of `backend\models\Livesessions`.
 */
class LivesessionsSearch extends Livesessions
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['liveID', 'facultyID', 'moduleID'], 'integer'],
            [['liveName', 'liveBatch', 'liveStartDate', 'liveStartTime', 'liveEndTime', 'liveMeetingID', 'liveMeetingPassword', 'liveMeetingURL', 'liveStatus', 'liveCreatedDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Livesessions::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'liveID' => $this->liveID,
            'facultyID' => $this->facultyID,
            'liveStartDate' => $this->liveStartDate,
            'liveStartTime' => $this->liveStartTime,
            'liveEndTime' => $this->liveEndTime,
            'moduleID' => $this->moduleID,
            'liveCreatedDate' => $this->liveCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'liveName', $this->liveName])
            ->andFilterWhere(['like', 'liveBatch', $this->liveBatch])
            ->andFilterWhere(['like', 'liveMeetingID', $this->liveMeetingID])
            ->andFilterWhere(['like', 'liveMeetingPassword', $this->liveMeetingPassword])
            ->andFilterWhere(['like', 'liveMeetingURL', $this->liveMeetingURL])
            ->andFilterWhere(['like', 'liveStatus', $this->liveStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
        return $dataProvider;
    }
}
