<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "userexams".
 *
 * @property integer $userexamID
 * @property integer $examID
 * @property string $userexamDate
 * @property string $userexamGrade
 * @property integer $userexamVerifiedBy
 * @property string $userexamVerifiedDate
 * @property string $userexamVerifiedNotes
 * @property integer $userID
 */
class Userexams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userexams';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['examID', 'userexamGrade', 'userexamVerifiedBy'], 'required'],
            [['examID', 'userexamVerifiedBy', 'userID'], 'integer'],
            [['userexamDate', 'userexamVerifiedDate'], 'safe'],
            [['userexamGrade'], 'string', 'max' => 15],
            [['userexamVerifiedNotes'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userexamID' => 'ID',
            'examID' => 'Exam',
            'userexamDate' => 'Date',
            'userexamGrade' => 'Grade',
            'userexamVerifiedBy' => 'Verified By',
            'userexamVerifiedDate' => 'Verified Date',
            'userexamVerifiedNotes' => 'Verified Notes',
            'userID' => 'User',
        ];
    }
	
	public function getExams()
    {
        return $this->hasOne(Exams::className(),['examID'=>'examID']);
    }
	public function getUsers()
    {
        return $this->hasOne(Users::className(),['userID'=>'userID']);
    }
	public function getFaculty()
    {
        return $this->hasOne(Faculty::className(),['facultyID'=>'facultyID']);
    }
	public function getVerifiedfaculty()
    {
        return $this->hasOne(Faculty::className(),['userexamVerifiedBy'=>'facultyID']);
    }
}
