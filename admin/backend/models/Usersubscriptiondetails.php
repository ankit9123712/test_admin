<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "usersubscriptiondetails".
 *
 * @property string $planName
 * @property string $planDiscountPrice
 * @property int $planDuration
 * @property string $subscriptionStartDate
 * @property string $subscriptionEndDate
 * @property string $streamName
 * @property string $coursestdName
 * @property int $userID
 */
class Usersubscriptiondetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usersubscriptiondetails';
    }
	public static function primaryKey()
	{
		return ['userID'];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['planName', 'planDiscountPrice', 'planDuration', 'subscriptionStartDate', 'subscriptionEndDate', 'streamName', 'coursestdName', 'userID'], 'required'],
            [['planDiscountPrice'], 'number'],
            [['planDuration', 'userID'], 'integer'],
            [['subscriptionStartDate', 'subscriptionEndDate'], 'safe'],
            [['planName', 'streamName', 'coursestdName'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'planName' => 'Plan Name',
            'planDiscountPrice' => 'Plan Discount Price',
            'planDuration' => 'Plan Duration',
            'subscriptionStartDate' => 'Subscription Start Date',
            'subscriptionEndDate' => 'Subscription End Date',
            'streamName' => 'Stream Name',
            'coursestdName' => 'Coursestd Name',
            'userID' => 'User I D',
        ];
    }
}
