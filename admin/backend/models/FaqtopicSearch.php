<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Faqtopic;

/**
 * FaqtopicSearch represents the model behind the search form of `app\models\Faqtopic`.
 */
class FaqtopicSearch extends Faqtopic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['faqtopicID'], 'integer'],
            [['faqtopicName', 'faqtopicStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Faqtopic::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'faqtopicID' => $this->faqtopicID,
        ]);

        $query->andFilterWhere(['like', 'faqtopicName', $this->faqtopicName])
            ->andFilterWhere(['like', 'faqtopicStatus', $this->faqtopicStatus]);
		$query->orderby('faqtopicID DESC');
        return $dataProvider;
    }
}
