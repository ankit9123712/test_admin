<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property integer $stateID
 * @property integer $countryID
 * @property string $stateName
 * @property string $stateStatus
 * @property string $stateCreatedDate
 * @property string $stateLatitude
 * @property string $stateLongitude
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countryID'], 'integer'],
            [['countryID','stateName'], 'required'],
            [['stateStatus'], 'string'],
            [['stateCreatedDate'], 'safe'],
            [['stateName'], 'string', 'max' => 100,'min' => 3],
            [['stateLatitude', 'stateLongitude'], 'string', 'max' => 20],
			['stateName','match', 'pattern' => '/^[A-Za-z ]+$/u', 'message' => ' State Name you entered is incorrect.'],
            [['countryID', 'stateName'], 'unique', 'targetAttribute' => ['countryID', 'stateName'], 'message' => 'The combination of Country  and State Name has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stateID' => 'ID',
            'countryID' => 'Country',
            'stateName' => 'Name',
            'stateStatus' => 'Status',
            'stateCreatedDate' => 'Created Date',
            'stateLatitude' => 'Latitude',
            'stateLongitude' => 'Longitude',
        ];
    }
	public function getCountry()
    {
        return $this->hasOne(Country::className(),['countryID'=>'countryID']);
    }
}
