<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Monkcategory;

/**
 * MonkcategorySearch represents the model behind the search form of `backend\models\Monkcategory`.
 */
class MonkcategorySearch extends Monkcategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['monkcatID'], 'integer'],
            [['monkcatName', 'monkcatStatus', 'monkcatCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Monkcategory::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'monkcatID' => $this->monkcatID,
            'monkcatCreatedDate' => $this->monkcatCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'monkcatName', $this->monkcatName])
            ->andFilterWhere(['like', 'monkcatStatus', $this->monkcatStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('monkcatID DESC');
        return $dataProvider;
    }
}
