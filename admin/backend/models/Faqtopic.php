<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faqtopic".
 *
 * @property int $faqtopicID
 * @property string $faqtopicName
 * @property string $faqtopicStatus
 */
class Faqtopic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faqtopic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['faqtopicName', 'faqtopicStatus'], 'required'],
            [['faqtopicStatus'], 'string'],
            [['faqtopicName'], 'string', 'max' => 100],
            [['faqtopicName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'faqtopicID' => 'ID',
            'faqtopicName' => 'Name',
            'faqtopicStatus' => 'Status',
        ];
    }
}
