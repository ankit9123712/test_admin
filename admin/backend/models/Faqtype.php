<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "faqtype".
 *
 * @property integer $faqtypeID
 * @property string $faqtypeName
 * @property string $faqtypeRemarks
 * @property string $faqtypeStatus
 */
class Faqtype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faqtype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['faqtypeName', 'faqtypeStatus'], 'required'],
            [['faqtypeStatus'], 'string'],
            [['faqtypeName'], 'string', 'max' => 100,'min' => 2],
            [['faqtypeRemarks'], 'string', 'max' => 400],
            [['faqtypeName'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'faqtypeID' => 'ID',
            'faqtypeName' => 'Name',
            'faqtypeRemarks' => 'Remarks',
            'faqtypeStatus' => 'Status',
        ];
    }
}
