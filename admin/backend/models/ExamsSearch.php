<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Exams;

/**
 * ExamsSearch represents the model behind the search form of `backend\models\Exams`.
 */
class ExamsSearch extends Exams
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          // [['examID', 'moduleID', 'examTotalQs', 'examQualifyingMarks'], 'integer'],
            [['examID', 'moduleID', 'examTotalQs', 'examQualifyingMarks','examName', 'examContains', 'examSections', 'examDuration', 'queIDs', 'examInstruction', 'examStatus'], 'safe'],
            [['examCorrectAnswer', 'examWrongAnswer'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Exams::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('coursemodule');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'examID' => $this->examID,
           // 'moduleID' => $this->moduleID,
            'examDuration' => $this->examDuration,
            'examTotalQs' => $this->examTotalQs,
            'examQualifyingMarks' => $this->examQualifyingMarks,
            'examCorrectAnswer' => $this->examCorrectAnswer,
            'examWrongAnswer' => $this->examWrongAnswer,
        ]);

        $query->andFilterWhere(['like', 'examName', $this->examName])
            ->andFilterWhere(['like', 'coursemodule.moduleName', $this->moduleID])
            ->andFilterWhere(['like', 'examContains', $this->examContains])
            ->andFilterWhere(['like', 'examSections', $this->examSections])
            ->andFilterWhere(['like', 'queIDs', $this->queIDs])
            ->andFilterWhere(['like', 'examInstruction', $this->examInstruction])
            ->andFilterWhere(['like', 'examStatus', $this->examStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('examID DESC');
        return $dataProvider;
    }
}
