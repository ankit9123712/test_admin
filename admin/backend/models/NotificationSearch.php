<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Notification;

/**
 * NotificationSearch represents the model behind the search form of `backend\models\Notification`.
 */
class NotificationSearch extends Notification
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['notificationID', 'notificationReferenceKey', 'userID', 'dealerID'], 'integer'],
            [['notificationID', 'notificationReferenceKey', 'userID', 'dealerID','userDeviceType', 'userDeviceID', 'notificationType', 'notificationTitle', 'notificationMessageText', 'notificationResponse', 'notificationSendDate', 'notificationSendTime', 'notificationStatus', 'notificationReadStatus', 'notificationCreatedDate', 'notificationData'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notification::find();

        // add conditions that should always apply here

         if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->JoinWith('users');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'notificationID' => $this->notificationID,
            'notificationReferenceKey' => $this->notificationReferenceKey,
           // 'userID' => $this->userID,
            'notificationSendDate' => $this->notificationSendDate,
            'notificationSendTime' => $this->notificationSendTime,
            'notificationCreatedDate' => $this->notificationCreatedDate,
           // 'dealerID' => $this->dealerID,
        ]);

        $query->andFilterWhere(['like', 'userDeviceType', $this->userDeviceType])
            ->andFilterWhere(['like', 'userDeviceID', $this->userDeviceID])
			->andFilterWhere(['like', 'users.userFullName', $this->userID])
            ->andFilterWhere(['like', 'notificationType', $this->notificationType])
            ->andFilterWhere(['like', 'notificationTitle', $this->notificationTitle])
            ->andFilterWhere(['like', 'notificationMessageText', $this->notificationMessageText])
            ->andFilterWhere(['like', 'notificationResponse', $this->notificationResponse])
            ->andFilterWhere(['like', 'notificationStatus', $this->notificationStatus])
            ->andFilterWhere(['like', 'notificationReadStatus', $this->notificationReadStatus])
            ->andFilterWhere(['like', 'notificationData', $this->notificationData]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
			$query->orderby('notificationID DESC');
        return $dataProvider;
    }
}
