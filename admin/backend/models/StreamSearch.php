<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Stream;

/**
 * StreamSearch represents the model behind the search form of `app\models\Stream`.
 */
class StreamSearch extends Stream
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['streamID'], 'integer'],
            [['streamName', 'streamStatus', 'streamCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stream::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'streamID' => $this->streamID,
            'streamCreatedDate' => $this->streamCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'streamName', $this->streamName])
            ->andFilterWhere(['like', 'streamStatus', $this->streamStatus]);
		$query->orderby('streamID DESC');
        return $dataProvider;
    }
}
