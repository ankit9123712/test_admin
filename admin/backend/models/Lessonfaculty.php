<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "lessonfaculty".
 *
 * @property int $lessonfacultyID
 * @property int $lessionID
 * @property int $facultyID
 * @property string $lessonfacultyType
 */
class Lessonfaculty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lessonfaculty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lessionID', 'facultyID', 'lessonfacultyType'], 'required'],
            [['lessionID', 'facultyID'], 'integer'],
            [['lessonfacultyType'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lessonfacultyID' => 'Lessonfaculty ID',
            'lessionID' => 'Lession ID',
            'facultyID' => 'Faculty ID',
            'lessonfacultyType' => 'Lessonfaculty Type',
        ];
    }
}
