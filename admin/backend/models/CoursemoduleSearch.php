<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Coursemodule;

/**
 * CoursemoduleSearch represents the model behind the search form of `backend\models\Coursemodule`.
 */
class CoursemoduleSearch extends Coursemodule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['moduleID'], 'integer'],
            [['moduleName', 'moduleType', 'moduleRemarks', 'moduleStatus', 'moduleCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Coursemodule::find();

        // add conditions that should always apply here
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'moduleID' => $this->moduleID,
            'moduleCreatedDate' => $this->moduleCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'moduleName', $this->moduleName])
            ->andFilterWhere(['like', 'moduleType', $this->moduleType])
            ->andFilterWhere(['like', 'moduleRemarks', $this->moduleRemarks])
            ->andFilterWhere(['like', 'moduleStatus', $this->moduleStatus]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else	
		$query->orderby('moduleID DESC');
        return $dataProvider;
    }
}
