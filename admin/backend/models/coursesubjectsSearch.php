<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\coursesubjects;

/**
 * coursesubjectsSearch represents the model behind the search form of `backend\models\coursesubjects`.
 */
class coursesubjectsSearch extends coursesubjects
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coursesubjID', 'courseID', 'coursesubjChapters'], 'integer'],
            [['coursesubjName', 'coursesubjDescription', 'coursesubjStatus', 'coursesubjCreatedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = coursesubjects::find();
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'coursesubjID' => $this->coursesubjID,
            'courseID' => $this->courseID,
            'coursesubjChapters' => $this->coursesubjChapters,
            'coursesubjCreatedDate' => $this->coursesubjCreatedDate,
        ]);

        $query->andFilterWhere(['like', 'coursesubjName', $this->coursesubjName])
            ->andFilterWhere(['like', 'coursesubjDescription', $this->coursesubjDescription])
            ->andFilterWhere(['like', 'coursesubjStatus', $this->coursesubjStatus]);
		$query->orderby('coursesubjID DESC');
        return $dataProvider;
    }
}
