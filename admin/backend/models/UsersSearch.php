<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Users;

/**
 * UsersSearch represents the model behind the search form of `backend\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'userAge', 'countryID', 'languageID'], 'integer'],
            [['userFullName', 'userCountryCode', 'userMobile', 'userEmail', 'userPassword', 'userDOB', 'userGender', 'userProfilePicture', 'userUniChoice1', 'userUniChoice2', 'userUniChoice3', 'userLiveLectureNotify', 'userTestNotify', 'userResultNotify', 'userAddress', 'userDeviceType', 'userDeviceID', 'userReferKey', 'userVerified', 'userNewsNotify', 'userEventsNotify', 'userNoticeNotify', 'userAssignmentNotify', 'userForumNotify', 'userAdminNotify', 'userType', 'userStatus', 'userOTP', 'userCreatedDate', 'userSignupOTPVerified', 'userIELTSMonth', 'userIELTSYear', 'userIELTSCity', 'userSecurityToken', 'useTokenExpirtyDate', 'stateID', 'cityID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();
		 if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
			$pagination =20;
		}
		else
		{
			$pagination = Yii::$app->session['customerparams']['per-page'];
		}
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 'pageSize' => $pagination ],
        ]);

        $this->load($params);
		$query->join('left join','usersubscriptions','usersubscriptions.userID = users.userID');
		$query->join('left join','state','users.stateID = state.stateID');
		$query->join('left join','city','city.cityID = users.cityID');
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'userID' => $this->userID,
            'userAge' => $this->userAge,
            'userDOB' => $this->userDOB,
            'countryID' => $this->countryID,
            //'stateID' => $this->stateID,
           // 'cityID' => $this->cityID,
            'languageID' => $this->languageID,
            'userCreatedDate' => $this->userCreatedDate,
            'useTokenExpirtyDate' => $this->useTokenExpirtyDate,
        ]);

        $query->andFilterWhere(['like', 'userFullName', $this->userFullName])
            ->andFilterWhere(['like', 'userCountryCode', $this->userCountryCode])
            ->andFilterWhere(['like', 'userMobile', $this->userMobile])
            ->andFilterWhere(['like', 'userEmail', $this->userEmail])
            ->andFilterWhere(['like', 'userPassword', $this->userPassword])
            ->andFilterWhere(['like', 'userGender', $this->userGender])
            ->andFilterWhere(['like', 'userProfilePicture', $this->userProfilePicture])
            ->andFilterWhere(['like', 'userUniChoice1', $this->userUniChoice1])
            ->andFilterWhere(['like', 'userUniChoice2', $this->userUniChoice2])
            ->andFilterWhere(['like', 'userUniChoice3', $this->userUniChoice3])
            ->andFilterWhere(['like', 'userLiveLectureNotify', $this->userLiveLectureNotify])
            ->andFilterWhere(['like', 'userTestNotify', $this->userTestNotify])
            ->andFilterWhere(['like', 'userResultNotify', $this->userResultNotify])
            ->andFilterWhere(['like', 'userAddress', $this->userAddress])
            ->andFilterWhere(['like', 'userDeviceType', $this->userDeviceType])
            ->andFilterWhere(['like', 'userDeviceID', $this->userDeviceID])
			->andFilterWhere(['like', 'state.stateName', $this->stateID])
			->andFilterWhere(['like', 'city.cityName', $this->cityID])
            ->andFilterWhere(['like', 'userReferKey', $this->userReferKey])
            ->andFilterWhere(['like', 'userVerified', $this->userVerified])
            ->andFilterWhere(['like', 'userNewsNotify', $this->userNewsNotify])
            ->andFilterWhere(['like', 'userEventsNotify', $this->userEventsNotify])
            ->andFilterWhere(['like', 'userNoticeNotify', $this->userNoticeNotify])
            ->andFilterWhere(['like', 'userAssignmentNotify', $this->userAssignmentNotify])
            ->andFilterWhere(['like', 'userForumNotify', $this->userForumNotify])
            ->andFilterWhere(['like', 'userAdminNotify', $this->userAdminNotify])
            ->andFilterWhere(['like', 'userType', $this->userType])
            ->andFilterWhere(['like', 'userStatus', $this->userStatus])
            ->andFilterWhere(['like', 'userOTP', $this->userOTP])
            ->andFilterWhere(['like', 'userSignupOTPVerified', $this->userSignupOTPVerified])
            ->andFilterWhere(['like', 'userIELTSMonth', $this->userIELTSMonth])
            ->andFilterWhere(['like', 'userIELTSYear', $this->userIELTSYear])
            ->andFilterWhere(['like', 'userIELTSCity', $this->userIELTSCity])
            ->andFilterWhere(['like', 'userSecurityToken', $this->userSecurityToken]);
		if(!empty($_REQUEST["dp-1-sort"]))	
		{	
			$str= $_REQUEST["dp-1-sort"];
			if($str[0]=="-")
			{
				
			}
			else
			{
				$query->orderby($_REQUEST["dp-1-sort"]);
			}
		}
		else
		$query->orderby('userID DESC');
        return $dataProvider;
    }
}
