<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\IeltsfilesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ieltsfiles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ieltsfileID') ?>

    <?= $form->field($model, 'ieltsfileType') ?>

    <?= $form->field($model, 'ieltsfileName') ?>

    <?= $form->field($model, 'ieltsfileStatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
