<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Ieltsfiles */

$this->title = 'Update IELTS: ' . $model->ieltsfileID;
$this->params['breadcrumbs'][] = ['label' => 'IELTS', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ieltsfileID, 'url' => ['view', 'id' => $model->ieltsfileID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ieltsfiles-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
