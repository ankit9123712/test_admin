<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\models\Ieltsfiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ieltsfiles-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-6">
	<?= $form->field($model, 'ieltsDisplayName')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'ieltsfileType')->dropDownList([ 'PDF' => 'PDF', 'URL' => 'URL', 'MP4' => 'MP4', ]) ?>
	 <?= $form->field($model, 'ieltsfileStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
<div class="col-sm-6">

<?php

echo $form->field($model, 'ieltsfileName')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'image/*'],
				  //'name'=>$model->ieltsfileName,	
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['jpg','png','jpeg','pdf','mp4','mp3'],
				  'showPreview' => true,
				  'showCaption' => true,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,
                  'initialPreview'=>!empty($model->ieltsfileName) ? "../../uploads/ieltsfiles/".$model->ieltsfileName : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>false,
				   'initialPreviewConfig' => [
					[
						'caption' => $model->ieltsfileName,
						//'width' => "120px",
						'url' => Url::toRoute(['ieltsfiles/delete-ieltsfiles-image','ieltsfileID'=>$model->ieltsfileID,'ieltsfileName'=>$model->ieltsfileName]),
						'key' => $model->ieltsfileID,
						'ieltsfileName' => $model->ieltsfileName,
						

					],

				],
               
	 ]
   ]);

?></div>

</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../ieltsfiles/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
