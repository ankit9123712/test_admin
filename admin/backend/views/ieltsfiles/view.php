<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Ieltsfiles */

$this->title = 'View IELTS: ' . $model->ieltsfileID;
$this->params['breadcrumbs'][] = ['label' => 'IELTS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ieltsfiles-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ieltsfileID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ieltsfileID',
			'ieltsDisplayName',
            'ieltsfileType',
            'ieltsfileName',
            'ieltsfileStatus',
        ],
    ]) ?>

</div>
