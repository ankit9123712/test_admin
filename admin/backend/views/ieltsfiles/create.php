<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Ieltsfiles */

$this->title = 'Create IELTS';
$this->params['breadcrumbs'][] = ['label' => 'IELTS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ieltsfiles-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
