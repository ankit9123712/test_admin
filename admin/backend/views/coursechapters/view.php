<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use backend\models\Chapterfiles;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use backend\models\Coursechapters;
//use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Coursechapters */

$this->title = 'View Course Chapter: ' . $model->coursechapterID;
$this->params['breadcrumbs'][] = ['label' => 'Course Chapters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

	

?>
<div class="coursechapters-view">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?php  echo Html::a('Update', ['update', 'id' => $model->coursechapterID], ['class' => 'btn btn-primary']);  
		//echo "<pre>";
		//print_r($model);die;
		?>
           </p>
		   
		   

    <?= DetailView::widget([
        'model' => $model,
       // 'dataProviderper' => $dataProviderper,
        'attributes' => [
            'coursechapterID',
			 'coursechapterNo',
           // 'courseID',
			[
				'label'=>'Course',
				'attribute'=>'courses.courseName'
			],
           //'coursesubjID',
			[
				'label'=>'Course Subject',
				'attribute'=>'coursesubjects.coursesubjName'
			],
           
            'coursechapterName',
            'coursechapterDescription:ntext',
            //'coursechapterFile',
			[                
                'attribute'=>'coursechapterFile',
                'value'=>   ((($model->coursechapterFile == NULL) || (($model['coursechapterFile'] == ''))) ? "../../../web/images/noimage.png": '../../../web/uploads/chapter'.'/'.$model->coursechapterFile),
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
          
			
	        'coursechapterCreatedDate',
			   'coursechapterStatus',
        ],
    ]) ?>
<h3>File Upload Detailes :</h3>
<?php
	/*------------------------------Start code Chapterfiles  display----------------------------------------*/
   $Chapterfiles_Billing_dataProvider = new ActiveDataProvider([
         'query' => Chapterfiles::find()->where('coursechapterID = "'.$model->coursechapterID.'"'),
         'pagination'=>false
		 
        ]);
		
     echo GridView::widget([
        'dataProvider' => $Chapterfiles_Billing_dataProvider,
        'columns' => [
		'chapterfileFileType',
		'chapterfileFile:url',
		//'<a href="http://www.yiiframework.com">http://www.yiiframework.com</a>',
		//'return Html::a("http://betaapplication.com/kendu/backend/web/uploads/chapter/")',

		
		'chapterfile',
		
		
		 ],
    ]); 
/*------------------------------End code Useraddress Fields display----------------------------------------*/
     ?>	 
	 
	 

</div>
