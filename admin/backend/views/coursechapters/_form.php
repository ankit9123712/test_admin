<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Courses;
use backend\models\coursesubjects;
use backend\models\Chapterfiles;
use kartik\file\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Coursechapters */
/* @var $form yii\widgets\ActiveForm */


$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Chapterfiles: " + (index + 1))
    });
});
jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Chapterfiles: " + (index + 1))

    });

});

';


$this->registerJs($js);
?>

<div class="coursechapters-form">
<?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => 'dynamic-form',
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
    
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

<?= $form->field($model, 'courseID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Courses::find()->where(['courseStatus' => 'Active'])->all(),'courseID','courseName'),
                    'options' => ['placeholder' => 'Select Courses'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

<?= $form->field($model, 'coursesubjID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(coursesubjects::find()->where(['coursesubjStatus' => 'Active'])->all(),'coursesubjID','coursesubjName'),
                    'options' => ['placeholder' => 'Select coursesubjects'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

    <?= $form->field($model, 'coursechapterNo')->textInput() ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'coursechapterName')->textInput(['maxlength' => true]) ?>


</div>
</div>
<div class="row">
<div class="col-sm-12">
	<?= $form->field($model, 'coursechapterFile')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],'pluginOptions' => [
                  'initialPreview'=>isset($model->coursechapterFile) ? "../../uploads/chapter/".$model->coursechapterFile : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>true,
                  'showPreview' => true,
                  'showCaption' => true,
                  'showRemove' => false,
                  'showUpload' => false,
					]]);
					?>				
				
</div>
</div>

<div class="row">
<div class="col-sm-12">
<?php DynamicFormWidget::begin([

        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]

        'widgetBody' => '.container-items', // required: css class selector

        'widgetItem' => '.item', // required: css class

        'limit' => 7, // the maximum times, an element can be cloned (default 999)

        'min' => 1, // 0 or 1 (default 1)

        'insertButton' => '.add-item', // css class

        'deleteButton' => '.remove-item', // css class

        'model' => $modelsDetails[0],

        'formId' => 'dynamic-form',

        'formFields' => [
            'chapterfileID',
            'coursechapterID',
            'chapterfileFile',
            'chapterfile',
           
                ],

    ]); ?>

<div class="panel panel-default">

        <div class="panel-heading">

            <i class="fa fa-envelope"></i> File Uploads

            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add More</button>

            <div class="clearfix"></div>

        </div>

        <div class="panel-body container-items">  <!-- widgetContainer -->

            <?php foreach ($modelsDetails as $index => $modelDetails): ?>

                <div class="item panel panel-default"><!-- widgetBody -->

                    <div class="panel-heading">

                        <span class="panel-title-address">File: <?= ($index + 1) ?></span>

                        <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>

                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

                        <?php

                            // necessary for update action.

                            if (!$modelDetails->isNewRecord) 
							{
                                echo Html::activeHiddenInput($modelDetails, "[{$index}]chapterfileID");
                            }
							else
							{	
							    echo $form->field($modelDetails,"[{$index}]chapterfileID")->hiddeninput(['value'=>'0'])->label(false); 	 
							}

                        ?>
<div class="row">

<div class="col-sm-4">
    <?= $form->field($modelDetails,"[{$index}]chapterfile")->label('Name')->textInput() ?>
</div>

<div class="col-sm-4">

<?php
  echo $form->field($modelDetails, "[{$index}]chapterfileFile[]")->widget(FileInput::classname(), [
                  'options' => ['accept' => 'all file/*'],'pluginOptions' => [
                  'initialPreview'=>isset($modelDetails->chapterfileFile) ? "../../uploads/chapter/".$modelDetails->chapterfileFile : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>true,
                  'showPreview' => true,
                  'showCaption' => true,
                  'showRemove' => false,
				  'allowedFileExtensions' => ['mp4','pdf'],	
                  'showUpload' => false
	    ]
    ]);
?>
</div>

           </div>
    </div>

            <?php endforeach; ?>

        </div>

    </div>

    <?php DynamicFormWidget::end(); ?>
  </div>

</div>
</div>


<div class="row">
<div class="col-sm-12">
    <?= $form->field($model, 'coursechapterDescription')->textarea(['rows' => 10]) ?>

</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'coursechapterStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
</div>
<div class="row">
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
