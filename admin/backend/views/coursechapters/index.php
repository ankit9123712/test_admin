<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CoursechaptersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Course Chapters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coursechapters-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Course Chapter', ['create'], ['class' => 'btn btn-success']) ?>&nbsp;&nbsp;<?= Html::a('Back To Course', ['courses/index'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'coursechapters','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'coursechapterID',
           // 'courseID',
			[
			'attribute'=>'courseID',
			'label'=>'Course Name',
			'value'=>'courses.courseName'
		   ],
           // 'coursesubjID',
			/*[
			'attribute'=>'coursesubjID',
			'label'=>'Subject Name',
			'value'=>'coursesubjects.coursesubjName'
		   ],*/
            //'coursechapterNo',
            //'coursechapterName',
			[
			'attribute'=>'coursechapterName',
			'label'=>'Course Subject Name'
		   ],
            // 'coursechapterDescription:ntext',
            // 'coursechapterFile',
            // 'coursechapterFileURL:url',
           // 'coursechapterStatus',
			[
			'attribute'=>'coursechapterStatus',
			'label'=>'Status'
		   ],
            // 'coursechapterCreatedDate',
	            [
                    'class' => 'yii\grid\ActionColumn',
					'header'=>'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->coursechapterStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->coursechapterID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->coursechapterID],['title' => 'Make Active']);
						}
					},
            ]
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
