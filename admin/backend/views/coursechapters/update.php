<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Coursechapters */

$this->title = 'Update Course Chapters: ' . $model->coursechapterID;
$this->params['breadcrumbs'][] = ['label' => 'Coursechapters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->coursechapterID, 'url' => ['view', 'id' => $model->coursechapterID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coursechapters-update">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'modelsDetails' => $modelsDetails,

    ]) ?>

</div>
