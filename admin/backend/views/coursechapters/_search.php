<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CoursechaptersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coursechapters-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'coursechapterID') ?>

    <?= $form->field($model, 'courseID') ?>

    <?= $form->field($model, 'coursesubjID') ?>

    <?= $form->field($model, 'coursechapterNo') ?>

    <?= $form->field($model, 'coursechapterName') ?>

    <?php // echo $form->field($model, 'coursechapterDescription') ?>

    <?php // echo $form->field($model, 'coursechapterFile') ?>

    <?php // echo $form->field($model, 'coursechapterFileURL') ?>

    <?php // echo $form->field($model, 'coursechapterStatus') ?>

    <?php // echo $form->field($model, 'coursechapterCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
