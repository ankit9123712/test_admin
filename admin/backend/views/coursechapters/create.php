<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Coursechapters */

$this->title = 'Create Course Chapters';
$this->params['breadcrumbs'][] = ['label' => 'Course Chapters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coursechapters-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
		 'modelsDetails' => $modelsDetails,
    ]) ?>

</div>
