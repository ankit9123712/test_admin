<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Messagecenter */

$this->title = 'Create Message Center';
$this->params['breadcrumbs'][] = ['label' => 'Message Centers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messagecenter-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
