<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;
/* @var $this yii\web\View */
/* @var $model backend\models\Messagecenter */

$this->title = 'View Message Center: ' . $model->messagecenterID;
$this->params['breadcrumbs'][] = ['label' => 'Message Centers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messagecenter-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->messagecenterID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'messagecenterID',
            'messagecenterType',
            'messagecenterTitle',
            //'messagecenter',
			 [
			   'attribute' => 'messagecenter',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->messagecenter,
           ],
            'messagecenterTo',
            'messagecenterStartDate',
            'messagecenterEndDate',
            'messagecenterStatus',
            'messagecenterCreatedDate',
        ],
    ]) ?>

</div>
