<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\tinymce\TinyMce;


/* @var $this yii\web\View */
/* @var $model backend\models\Messagecenter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="messagecenter-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'messagecenterType')->dropDownList([ 'News' => 'News', 'Event' => 'Event', 'Notice' => 'Notice', ]) ?>
	<?= $form->field($model, 'messagecenterTitle')->textInput(['maxlength' => true]) ?>
	 <?= $form->field($model, 'messagecenterTo')->dropDownList([ 'All' => 'All', 'Free Users' => 'Free Users', 'Premium Users' => 'Premium Users', ]) ?>
	 <label>Start Date</label>
	<?= DatePicker::widget([
	'model' => $model,
    //'form'=>$objActiveForm,	
	'name' => 'messagecenterStartDate',
//date blanck	
	'value' => date('d M Y', strtotime($model->messagecenterStartDate)),
	'options' => ['placeholder' => 'Select Start date'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'startDate' => date("Y-m-d"),
		'todayHighlight' => true
	]
]);?><br>
<label>End Date</label>
	<?= DatePicker::widget([
	'model' => $model,
    //'form'=>$objActiveForm,	
	'name' => 'messagecenterEndDate', 
	'value' => date('d M Y', strtotime($model->messagecenterEndDate)),
	'options' => ['placeholder' => 'Select End date'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'startDate' => date('Y-m-d', strtotime('+1 days')),
		'todayHighlight' => true
	]
]);?><br>
 <?= $form->field($model, 'messagecenterStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
<div class="col-sm-8">
	<?= $form->field($model, 'messagecenter')->widget(TinyMce::className(), [
        'options' => ['rows' => 20],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
     
</div>
 
</div>
 
 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../messagecenter/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
