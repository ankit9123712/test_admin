<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MessagecenterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="messagecenter-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'messagecenterID') ?>

    <?= $form->field($model, 'messagecenterType') ?>

    <?= $form->field($model, 'messagecenterTitle') ?>

    <?= $form->field($model, 'messagecenter') ?>

    <?= $form->field($model, 'messagecenterTo') ?>

    <?php // echo $form->field($model, 'messagecenterStartDate') ?>

    <?php // echo $form->field($model, 'messagecenterEndDate') ?>

    <?php // echo $form->field($model, 'messagecenterStatus') ?>

    <?php // echo $form->field($model, 'messagecenterCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
