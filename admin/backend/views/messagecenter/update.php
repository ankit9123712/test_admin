<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Messagecenter */

$this->title = 'Update Message Center: ' . $model->messagecenterID;
$this->params['breadcrumbs'][] = ['label' => 'Message Centers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->messagecenterID, 'url' => ['view', 'id' => $model->messagecenterID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="messagecenter-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
