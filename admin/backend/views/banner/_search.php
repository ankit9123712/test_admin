<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BannerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'bannerID') ?>

    <?= $form->field($model, 'bannerName') ?>

    <?= $form->field($model, 'bannerScreen') ?>

    <?= $form->field($model, 'bannerType') ?>

    <?= $form->field($model, 'bannerTypeID') ?>

    <?php // echo $form->field($model, 'bannerURL') ?>

    <?php // echo $form->field($model, 'bannerStatus') ?>

    <?php // echo $form->field($model, 'bannerImage') ?>

    <?php // echo $form->field($model, 'bannerPosition') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
