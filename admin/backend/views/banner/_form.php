<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'bannerName')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'bannerScreen')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'bannerFileType')->dropDownList([ 'Image' => 'Image', 'Video' => 'Video', ]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

<?php /*= $form->field($model, 'bannerTypeID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Category::find()->where(['categoryStatus' => 'Active'])->all(),'categoryID','categoryName'),
                    'options' => ['placeholder' => 'Select Category'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);*/
				?></div>


</div>
<div class="row">
<div class="col-sm-6">

<?php
	
	 echo $form->field($model, 'bannerFile')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'image/*'],
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['jpg','png','jpeg','mp4'],
				  'showPreview' => true,
				  'showCaption' => true,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  'maxFileCount' => 1,
                  'initialPreview'=>!empty($model->bannerFile) ? "../../uploads/banners/".$model->bannerFile : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>false,
				   'initialPreviewConfig' => [
					[
						'caption' => $model->bannerFile,
						'url' => Url::toRoute(['banner/delete-banner-image','bannerID'=>$model->bannerID,'bannerFile'=>$model->bannerFile]),
						'key' => $model->bannerID,
						'bannerFile' => $model->bannerFile,
						

					],

				],
               
	 ]
   ]);
   
 ?> 
</div>
<div class="col-sm-5">

    <?= $form->field($model, 'bannerPosition')->textInput() ?>
</div></br></br>
<div class="col-sm-5">

    <?= $form->field($model, 'bannerRemarks')->textArea(['maxlength' => true,'rows'=>3]) ?>
</div></br></br></br>
<div class="col-sm-5">

    <?= $form->field($model, 'bannerStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../banner/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
