<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Banner */

$this->title = 'View Banner: ' . $model->bannerID;
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->bannerID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'bannerID',
            'bannerName',
            'bannerScreen',
            'bannerFileType',
          //  'bannerTypeID',
            'bannerURL',
           
			 [                
                'attribute'=>'bannerFile',
                'value'=>   ((($model->bannerFile == NULL) || (($model['bannerFile'] == ''))) ? "../../../web/images/noimage.png": '../../../web/uploads/banners'.'/'.$model->bannerFile),
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
           // 'bannerImage',
            'bannerPosition',
			 'bannerStatus',
        ],
    ]) ?>

</div>
