<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Livesessions */

$this->title = 'Update Live Sessions: ' . $model->liveID;
$this->params['breadcrumbs'][] = ['label' => 'Live Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->liveID, 'url' => ['view', 'id' => $model->liveID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="livesessions-update">

     

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
