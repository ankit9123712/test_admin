<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Coursemodule;
use backend\models\Faculty;
use kartik\time\TimePicker;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\models\Livesessions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="livesessions-form">

        <?php $form = ActiveForm::begin([ 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="col-sm-3">

    <?= $form->field($model, 'liveName')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">
	 
	<?= $form->field($model, 'moduleID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Coursemodule::find()->where(['moduleStatus' => 'Active','moduleType' => 'Premium'])->all(),'moduleID','moduleName'),
                    'options' => ['placeholder' => 'Select Module' ,
					'onchange'=>'         
										$.get( "'.Url::toRoute(['lesson/facultylist']).'", { type: $(this).val() } )
											.done(function( data ) {
												$( "#'.Html::getInputId($model, 'facultyID').'" ).html( data );
											}
										  );'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
    
</div>
<div class="col-sm-3">
    
	<?= $form->field($model, 'facultyID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Faculty::find()->where("facultyStatus='Active' AND (facultyType='Teaching' OR facultyType='Both' )" )->all(),'facultyID','facultyFullName'),
                    'options' => ['placeholder' => 'Select Faculty'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>


<div class="col-sm-3">
	<?= $form->field($model, 'liveBatch')->dropDownList([ 'Morning' => 'Morning', 'Afternoon' => 'Afternoon', 'Evening' => 'Evening', ], ['prompt' => '']) ?>
    
</div>

</div>
<div class="row">
<div class="col-sm-2">
    
    <?= $form->field($model, 'liveMeetingID')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-2">
    <?= $form->field($model, 'liveMeetingPassword')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">
	
	<label>Start Date</label>
	<?= DatePicker::widget([
	'model' => $model,
    //'form'=>$objActiveForm,	
	'name' => 'liveStartDate',
//date blanck	
	'value' => (!$model->isNewRecord) ? date('d-m-Y', strtotime($model->liveStartDate))  :  date('d-m-Y'),
	'options' => ['placeholder' => 'Select Start date'],
	'pluginOptions' => [
		'format' => 'dd-mm-yyyy',		
		//'minDate'=> if($model->isNewRecord) {date("Y-m-d") } else {""},
		'startDate' => date("Y-m-d"),
		'todayHighlight' => true
	] 
]);?>
	</div>
<div class="col-sm-2">
    
	<?php
//echo $model->liveStartTime;
echo '<label>Start Time</label>';
echo TimePicker::widget([
			'model' => $model,
			'name' => 'liveStartTime', 
			'value' => (!$model->isNewRecord) ? date('g:i A',strtotime($model->liveStartTime))  :  date('g:i A'),
			'options' => ['placeholder' => 'Start Time ...'],
			'pluginOptions' => [
				'defaultTime' => false, 
				'showSeconds' => false
			]
		]);
	?>
</div>
<div class="col-sm-2">
    
	<?php
//echo $model->liveEndTime;
echo '<label>End Time</label>';
echo TimePicker::widget([
			'model' => $model,
			'name' => 'liveEndTime', 
			'value' => (!$model->isNewRecord) ? date('g:i A',strtotime($model->liveEndTime))  :  date('g:i A'),
			'options' => ['placeholder' => 'End Time ...'],
			'pluginOptions' => [
				'defaultTime' => false, 
				'showSeconds' => false
			]
		]);
	?>
</div>


</div>
<?php if($model->isNewRecord) { ?>
<div class="row">
	<div class="col-sm-2">
		<?= $form->field($model, 'sesRecurring')->dropDownList([ 'No' => 'No', 'Yes' => 'Yes', ], ['prompt' => '']) ?>	
	</div>	
	<?php /*<div class="col-sm-3">
		<?= $form->field($model, 'sesRecurrance')->dropDownList([ 'Daily' => 'Daily', 'Weekly' => 'Weekly', ], ['prompt' => '']) ?>	
	</div> */ ?>
</div>

<div class="row">
	<div class="col-sm-8">
		<?php
	echo $form->field($model,'sesRecurranceDay')->widget(Select2::classname(), [
	'data' => ["7" => "Sun", "1" => "Mon", "2" => "Tue", "3" => "Wed", "4" => "Thu", "5" => "Fri", "6" => "Sat"], 
	'options' => ['placeholder' => 'Select Occurs on'],
	'pluginOptions' => [
		'allowClear' => true,
		'multiple'=>true
	],
]);
	?> 
		 	
	</div>	
</div>
<div class="row">
	<div class="col-sm-3">
		<label>End Date</label>
	<?= DatePicker::widget([
	'model' => $model,
    //'form'=>$objActiveForm,	
	'name' => 'sesEndDate',
//date blanck	
	'value' => (!$model->isNewRecord) ? date('d-m-Y', strtotime($model->sesEndDate))  :  date('d-m-Y'),
	'options' => ['placeholder' => 'Select End date'],
	'pluginOptions' => [
		'format' => 'dd-mm-yyyy',		
		//'minDate'=> if($model->isNewRecord) {date("Y-m-d") } else {""},
		'startDate' => date("Y-m-d"),
		'todayHighlight' => true
	] 
]);?>
	</div>
</div>
<?php } ?>
<div class="row">	
	<div class="col-sm-2">
    <?= $form->field($model, 'liveStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => '']) ?>
</div>
</div>

    <?php // $form->field($model, 'liveCreatedDate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
