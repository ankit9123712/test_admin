<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LivesessionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="livesessions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'liveID') ?>

    <?= $form->field($model, 'facultyID') ?>

    <?= $form->field($model, 'liveName') ?>

    <?= $form->field($model, 'liveBatch') ?>

    <?= $form->field($model, 'liveStartDate') ?>

    <?php // echo $form->field($model, 'liveStartTime') ?>

    <?php // echo $form->field($model, 'liveEndTime') ?>

    <?php // echo $form->field($model, 'moduleID') ?>

    <?php // echo $form->field($model, 'liveMeetingID') ?>

    <?php // echo $form->field($model, 'liveMeetingPassword') ?>

    <?php // echo $form->field($model, 'liveMeetingURL') ?>

    <?php // echo $form->field($model, 'liveStatus') ?>

    <?php // echo $form->field($model, 'liveCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
