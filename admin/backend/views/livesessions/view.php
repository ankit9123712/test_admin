<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Livesessions */

$this->title = 'Live Sessions: '.$model->liveID;
$this->params['breadcrumbs'][] = ['label' => 'Live Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="livesessions-view">

     

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->liveID], ['class' => 'btn btn-primary']) ?>
       
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'liveID',
            [
				'label'=>'Faculty',            
				'attribute'=>'faculty.facultyFullName',
		   ],
			[
				'label'=>'Module',            
				'attribute'=>'coursemodule.moduleName',
		   ],
            'liveName',
            'liveBatch',
            'liveStartDate',
            'liveStartTime',
            'liveEndTime',
            
            'liveMeetingID',
            'liveMeetingPassword',
            //'liveMeetingURL',
            'liveStatus',
            'liveCreatedDate',
        ],
    ]) ?>

</div>
