<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Livesessions */

$this->title = 'Create Live Sessions';
$this->params['breadcrumbs'][] = ['label' => 'Live Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="livesessions-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
