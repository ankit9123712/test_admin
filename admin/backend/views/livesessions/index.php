<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LivesessionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Live Sessions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="livesessions-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Live Session', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'liveID',
			[
				'attribute'=>'moduleID',
				'label'=>'Module',
				'value'=>'coursemodule.moduleName'
		   ],
		   [
				'attribute'=>'facultyID',
				'label'=>'Faculty',
				'value'=>'faculty.facultyFullName'
		   ],
            //'facultyID',
            'liveName',
            'liveBatch',
            'liveStartDate',
            'liveStartTime',
            'liveEndTime',
            //'moduleID',
            //'liveMeetingID',
            //'liveMeetingPassword',
            //'liveMeetingURL',
            //'liveStatus',
            //'liveCreatedDate',

            [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus} }',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->liveStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->liveID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->liveID],['title' => 'Make Active']);
						}
					},
					
					
					]
            
			],
        ],
    ]); ?>
</div>
