<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Language */

$this->title = 'Update Language: ' . $model->languageID;
$this->params['breadcrumbs'][] = ['label' => 'Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->languageID, 'url' => ['view', 'id' => $model->languageID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="language-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
