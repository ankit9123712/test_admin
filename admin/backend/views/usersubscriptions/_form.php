<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Users;
use backend\models\Subscriptionplan;
use kartik\date\DatePicker;

/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Usersubscriptions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usersubscriptions-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

<?= $form->field($model, 'userID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Users::find()->where(['userStatus' => 'Active'])->all(),'userID','userFullName'),
                    'options' => ['placeholder' => 'Select Users'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

<?= $form->field($model, 'subscriptionID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Subscriptionplan::find()->where(['planStatus' => 'Active'])->all(),'planID','planName'),
                    'options' => ['placeholder' => 'Select Subscription'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

    <?= $form->field($model, 'usersubscriptionAmount')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'usersubscriptionCoponCode')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">
<label>Start Date</label>
	<?= DatePicker::widget([
	'model' => $model,
    //'form'=>$objActiveForm,	
	'name' => 'usersubscriptionStartDate',
//date blanck	
	'value' => date('d M Y', strtotime($model->usersubscriptionStartDate)),
	'options' => ['placeholder' => 'Select Start date'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'startDate' => date("Y-m-d"),
		'todayHighlight' => true
	]
]);?><br> 
</div>
<div class="col-sm-4">
<label>End Date</label>
	<?= DatePicker::widget([
	'model' => $model,
    //'form'=>$objActiveForm,	
	'name' => 'usersubscriptionEndDate', 
	'value' => date('d M Y', strtotime($model->usersubscriptionEndDate)),
	'options' => ['placeholder' => 'Select End date'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'startDate' => date('Y-m-d', strtotime('+1 days')),
		'todayHighlight' => true
	]
]);?><br>
</div>
</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../usersubscriptions/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
