<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Usersubscriptions */

$this->title = 'Update  User Subscription: ' . $model->usersubscriptionID;
$this->params['breadcrumbs'][] = ['label' => ' User Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usersubscriptionID, 'url' => ['view', 'id' => $model->usersubscriptionID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usersubscriptions-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
