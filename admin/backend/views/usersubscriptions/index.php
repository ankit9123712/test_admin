<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UsersubscriptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' User Subscriptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usersubscriptions-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Subscriptions', ['create'], ['class' => 'btn btn-success']) ?>&nbsp; <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'usersubscriptions','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'usersubscriptionID',
        [
			'attribute'=>'userID',
			'label'=>'User',
			'value'=>'users.userFullName'
		],
		[
			'attribute'=>'subscriptionID',
			'label'=>'Subscription Plan',
			'value'=>'subscriptionplan.planName'
		],
            'usersubscriptionAmount',
          //  'usersubscriptionCoponCode',
             'usersubscriptionStartDate',
             'usersubscriptionEndDate',
            // 'usersubscriptionCreatedDate',
				[
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',                    
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
