<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Usersubscriptions */

$this->title = 'Create User Subscription';
$this->params['breadcrumbs'][] = ['label' => ' User Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usersubscriptions-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
