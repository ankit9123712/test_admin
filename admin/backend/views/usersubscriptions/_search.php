<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UsersubscriptionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usersubscriptions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'usersubscriptionID') ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'subscriptionID') ?>

    <?= $form->field($model, 'usersubscriptionAmount') ?>

    <?= $form->field($model, 'usersubscriptionCoponCode') ?>

    <?php // echo $form->field($model, 'usersubscriptionStartDate') ?>

    <?php // echo $form->field($model, 'usersubscriptionEndDate') ?>

    <?php // echo $form->field($model, 'usersubscriptionCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
