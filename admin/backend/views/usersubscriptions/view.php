<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Usersubscriptions */

$this->title = 'View  User Subscription: ' . $model->usersubscriptionID;
$this->params['breadcrumbs'][] = ['label' => ' User Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usersubscriptions-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->usersubscriptionID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'usersubscriptionID',
            [
				'label'=>'User',            
				'attribute'=>'users.userFullName',
		   ],
		   [
				'label'=>'Subscription Plan',            
				'attribute'=>'subscriptionplan.planName',
		   ],
            'usersubscriptionAmount',
            'usersubscriptionCoponCode',
            'usersubscriptionStartDate',
            'usersubscriptionEndDate',
            'usersubscriptionCreatedDate',
        ],
    ]) ?>

</div>
