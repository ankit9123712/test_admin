<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\StateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="state-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'stateID') ?>

    <?= $form->field($model, 'countryID') ?>

    <?= $form->field($model, 'stateName') ?>

    <?= $form->field($model, 'stateStatus') ?>

    <?= $form->field($model, 'stateCreatedDate') ?>

    <?php // echo $form->field($model, 'stateLatitude') ?>

    <?php // echo $form->field($model, 'stateLongitude') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
