<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\State */

$this->title = 'View State';
$this->params['breadcrumbs'][] = ['label' => 'States', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="state-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->stateID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'stateID',
            [
				'label'=>'Country',            
				'attribute'=>'country.countryName',
		   ],
            'stateName',
            'stateStatus',
            'stateCreatedDate',
            //'stateLatitude',
           // 'stateLongitude',
        ],
    ]) ?>

</div>
