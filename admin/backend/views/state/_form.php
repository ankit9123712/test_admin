<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Country;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\State */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="state-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

<?= $form->field($model, 'countryID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Country::find()->where(['countryStatus' => 'Active'])->all(),'countryID','countryName'),
                    'options' => ['placeholder' => 'Select Country'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

    <?= $form->field($model, 'stateName')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'stateStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive']) ?>
</div>
</div>

<div class="row">
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../state/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
