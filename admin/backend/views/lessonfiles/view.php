<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Lessonfiles */

$this->title = 'View Lesson File: ' . $model->LessonfileID;
$this->params['breadcrumbs'][] = ['label' => 'Lesson Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lessonfiles-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->LessonfileID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'LessonfileID',
            'LessonID',
            'LessonfileFileType',
			[                
                'attribute'=>'LessonfileFile',
                'value'=>   ((($model->LessonfileFile == NULL) || (($model['LessonfileFile'] == ''))) ? "../../../web/images/noimage.png": '../../../web/uploads/lessonfiles'.'/'.$model->LessonfileFile),
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
           // 'LessonfileFile',
            'Lessonfile',
            'LessonfileStatus',
        ],
    ]) ?>

</div>
