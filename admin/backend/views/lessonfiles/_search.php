<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LessonfilesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lessonfiles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'LessonfileID') ?>

    <?= $form->field($model, 'LessonID') ?>

    <?= $form->field($model, 'LessonfileFileType') ?>

    <?= $form->field($model, 'LessonfileFile') ?>

    <?= $form->field($model, 'Lessonfile') ?>

    <?php // echo $form->field($model, 'LessonfileStatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
