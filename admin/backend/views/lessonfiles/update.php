<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Lessonfiles */

$this->title = 'Update Lesson File: ' . $model->LessonfileID;
$this->params['breadcrumbs'][] = ['label' => 'Lesson Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->LessonfileID, 'url' => ['view', 'id' => $model->LessonfileID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lessonfiles-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
