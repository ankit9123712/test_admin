<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use backend\models\Lesson;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\Lessonfiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lessonfiles-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-6">
<?= $form->field($model, 'LessonID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Lesson::find()->where(['LessonStatus' => 'Active'])->all(),'LessonID','LessonName'),
                    'options' => ['placeholder' => 'Select Lesson'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
 <?= $form->field($model, 'LessonfileFileType')->dropDownList([ 'Video' => 'Video', 'Contents' => 'Contents', 'Audio' => 'Audio', ]) ?>
 <?= $form->field($model, 'Lessonfile')->textInput(['maxlength' => true]) ?>
   <?= $form->field($model, 'LessonfileStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
<div class="col-sm-4">

   
</div>
<div class="col-sm-6">

<?php

echo $form->field($model, 'LessonfileFile')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'image/*'],
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['jpg','png','jpeg','pdf','docx','mp4','mp3'], 
				  'showPreview' => true,
				  'showCaption' => true,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,
                  'initialPreview'=>!empty($model->LessonfileFile) ? "../../uploads/lessonfiles/".$model->LessonfileFile : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>false,
				   'initialPreviewConfig' => [
					[
						'caption' => $model->LessonfileFile,
						//'width' => "120px",
						'url' => Url::toRoute(['lessonfiles/delete-lessonfiles-image','LessonfileID'=>$model->LessonfileID,'LessonfileFile'=>$model->LessonfileFile]),
						'key' => $model->LessonfileID,
						'LessonfileFile' => $model->LessonfileFile,
						

					],

				],
               
	 ]
   ]);
   
	?></div>
</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../lessonfiles/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
