<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Lessonfiles */

$this->title = 'Create Lesson File';
$this->params['breadcrumbs'][] = ['label' => 'Lesson Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lessonfiles-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
