<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Lesson;

/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Lesson */
/* @var $form yii\widgets\ActiveForm */
$instructions="";
$id=$_REQUEST["id"];
if(!empty($id))
{
	//echo $id;
	$res=Lesson::find()->select('*')->where('LessonID='.$id)->asArray()->one();
	//print_r($res);die;
	if(!empty($res))
	{
		if($res["moduleID"] == 6 || $res["moduleID"] == 8)
		{
			$instructions = "<h3>Reading File Uploads Instructions</h3><h4>Question types can be Single Text, MCQ, MMCQ <BR>In case of Single Text correction answer needs to put in Solution.<BR>In case of MCQ correct Option Number only needs to put in Correct Answer<BR>In cse of MMCQ correct option number needs to put comma seperated in Correct Answer.</h4><br>Find Sample file here to download <a href='http://13.235.206.122/englishmonk/backend/web/uploads/samplefiles/readinglession.xlsx'>Sample Reading Lession Question</a>";
		}
		else if($res["moduleID"] == 7 || $res["moduleID"] == 9)
		{
			$instructions = "<h3>Writing File Uploads Instructions</h3><h4>Question types can be Multiline Text.<BR></h4><br>Find Sample file here to download <a href='http://13.235.206.122/englishmonk/backend/web/uploads/samplefiles/writinglession.xlsx'>Sample Writing Lession Question</a>";
		}
		else if($res["moduleID"] == 10 || $res["moduleID"] == 11)			
		{
			$instructions = "<h3>Listening File Uploads Instructions</h3><h4>Question types can be Single Text, MCQ, MMCQ.<BR>Verification Required Yes Or No.<br>Question wise same or other file name need to define with complete URL where ever you have uploaded, make sure file name does not have any special character, space etc..</h4><br>Find Sample file here to download <a href='http://13.235.206.122/englishmonk/backend/web/uploads/samplefiles/listeninglession.xlsx'>Sample Listening Lession Question</a>";
		}
		else if($res["moduleID"] == 12 || $res["moduleID"] == 13)	
		{
			$instructions = "<h3>Speaking File Uploads Instructions</h3><h4>Question types can be Audio.<BR></h4><br>Find Sample file here to download <a href='http://13.235.206.122/englishmonk/backend/web/uploads/samplefiles/speakinglession.xlsx'>Sample Speaking Lession Question</a>";
		}
	}
}



?>

<div class="lesson-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
	'action'=> 'upload',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">
<div class="col-sm-12">
<?php echo $instructions."<BR><BR>"; ?>
</div>
</div>
<div class="row">
<div class="col-sm-6">

<?php

	echo $form->field($model, 'LessonFile')->widget(FileInput::classname(), [
                  
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['xlsx','xls'],
				  'showPreview' => false,
				  'showCaption' => false,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,

				]
               
	 
   ])->label('Upload Question Excel ');


					?>
</div>
</div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Upload' : 'Upload', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; 
    </div>

    <?php ActiveForm::end(); ?>

</div>
