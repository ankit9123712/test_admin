<?php
return [
	'bootstrap'    => ['assetsAutoCompress'],
    'components' => [
         'assetsAutoCompress' => [
            'class'   => '\skeeks\yii2\assetsAuto\AssetsAutoCompressComponent',
            'enabled' => false,

            'readFileTimeout' => 3,           //Time in seconds for reading each asset file

            'jsCompress'                => false,        //Enable minification js in html code
            'jsCompressFlaggedComments' => false,        //Cut comments during processing js

            'cssCompress' => false,        //Enable minification css in html code

            'cssFileCompile'        => false,        //Turning association css files
            'cssFileRemouteCompile' => false,       //Trying to get css files to which the specified path as the remote file, skchat him to her.
            'cssFileCompress'       => false,        //Enable compression and processing before being stored in the css file
            'cssFileBottom'         => false,       //Moving down the page css files
            'cssFileBottomLoadOnJs' => false,       //Transfer css file down the page and uploading them using js

            'jsFileCompile'                 => false,        //Turning association js files
            'jsFileRemouteCompile'          => false,       //Trying to get a js files to which the specified path as the remote file, skchat him to her.
            'jsFileCompress'                => false,        //Enable compression and processing js before saving a file
            'jsFileCompressFlaggedComments' => false,        //Cut comments during processing js

            'noIncludeJsFilesOnPjax' => false,        //Do not connect the js files when all pjax requests

            'htmlFormatter' => [
                //Enable compression html
                'class'         => 'skeeks\yii2\assetsAuto\formatters\html\TylerHtmlCompressor',
                'extra'         => false,       //use more compact algorithm
                'noComments'    => true,        //cut all the html comments
                'maxNumberRows' => 50000,       //The maximum number of rows that the formatter runs on

                //or

              //  'class' => 'skeeks\yii2\assetsAuto\formatters\html\MrclayHtmlCompressor',

                //or any other your handler implements skeeks\yii2\assetsAuto\IFormatter interface

                //or false
            ],
        ],
		'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=arjunapoc.cluster-cig2s0z9ecad.ap-south-1.rds.amazonaws.com;dbname=fsl',
            'username' => 'arjunadmin',
            'password' => 'qyvhUcC4rlzyXDxQzSE2',
            'charset' => 'utf8',
			'enableSchemaCache' => true,
            'attributes' => [
             PDO::NULL_TO_STRING => true,
//                PDO::ATTR_EMULATE_PREPARES=> false,
//                PDO::ATTR_STRINGIFY_FETCHES=>false
            ]
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
		'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
		/*'session' => [
			'class' => 'yii\web\DbSession',
		],*/
		
			
    ],
	/*'rules' => [
    'site/captcha/<refresh:\d+>' => 'site/captcha',
    'site/captcha/<v:\w+>' => 'site/captcha',
],*/
];
