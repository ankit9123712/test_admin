<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Faculty */

$this->title = 'View Faculty';
$this->params['breadcrumbs'][] = ['label' => 'Faculties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faculty-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->facultyID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'facultyID',
            'facultyFullName',
            'facultyMobile',
            'facultyEmail',
           // 'facultyPassword',
            //'facultyBirthDate',
            'facultyGender',
            'facultyDOB',
            'facultyJoinDate',
			
		   /*[
				'label'=>'Module',            
				'attribute'=>'coursemodule.moduleName',
			],*/
			'facultyType',
            //'moduleID',  
			[                
                'attribute'=>'facultyProfilePicture',
                'value'=>   ((($model->facultyProfilePicture == NULL) || (($model['facultyProfilePicture'] == ''))) ? "../../../web/images/noimage.png": '../../../web/uploads/faculty'.'/'.$model->facultyProfilePicture),
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            //'facultyProfilePicture',
            'facultyAddress',
            'facultyPincode',
			[
				'label'=>'Country',            
				'attribute'=>'country.countryName',
			],			
			[
				'label'=>'State',
				'attribute'=>'state.stateName',
			],
            [                
                'label'=>'City',
				'attribute'=>'city.cityName',                
            ],
          //  'facultyDeviceType',
          //  'facultyDeviceID',
          //  'facultyReferKey',
           // 'facultyVerified',
            'facultyStatus',
            'facultyCreatedDate',
            //'facultyOTP',
           // 'facultySignupOTPVerified',
        ],
    ]) ?>

</div>
