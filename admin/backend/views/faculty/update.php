<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Faculty */

$this->title = 'Update Faculty: ' . $model->facultyID;
$this->params['breadcrumbs'][] = ['label' => 'Faculties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->facultyID, 'url' => ['view', 'id' => $model->facultyID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="faculty-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
