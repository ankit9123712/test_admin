<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Coursemodule;
use backend\models\Country;
use backend\models\State;
use backend\models\City;
use kartik\date\DatePicker;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Faculty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faculty-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-3">

    <?= $form->field($model, 'facultyFullName')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">

    <?= $form->field($model, 'facultyMobile')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">

    <?= $form->field($model, 'facultyEmail')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">
<?= $form->field($model, 'facultyPassword')->passwordInput(['maxlength' => true,'readonly' => !$model->isNewRecord]) ?>
</div>
</div>
<div class="row">
 <div class="col-sm-3">

    <?= $form->field($model, 'facultyGender')->dropDownList([ 'Male' => 'Male', 'Female' => 'Female', ]) ?>
</div>
<div class="col-sm-3">
<label>Select Date Of Birth</label>
<?= DatePicker::widget([
	'model' => $model,	
	'name' => 'facultyDOB',
	'value' => $model->facultyDOB,
	'options' => ['placeholder' => 'Select Date Of Birth ...'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		//'endDate' => "0d",
		'todayHighlight' => true
	]
]);?>
</div>

<div class="col-sm-3">
<label>Select Join Date</label>
<?= DatePicker::widget([
	'model' => $model,	
	'name' => 'facultyJoinDate',
	'value' => $model->facultyJoinDate,
	'options' => ['placeholder' => 'Select Join Date ...'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		//'endDate' => "0d",
		'todayHighlight' => true
	]
]);?>

</div>
<div class="col-sm-3">
<?= $form->field($model, 'facultyType')->dropDownList([ 'Teaching' => 'Teaching', 'Grading' => 'Grading','Both' => 'Both', ]) ?>
</div>
</div>
<div class="row">

<div class="col-sm-3">

    <?= $form->field($model, 'facultyAddress')->textInput(['maxlength' => true]) ?></br>
	<?= $form->field($model, 'countryID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Country::find()->where(['countryStatus' => 'Active'])->all(),'countryID','countryName'),
                    'options' => ['placeholder' => 'Select country',
                                  'onchange'=>'         
                                        $.get( "'.Url::toRoute(['state/statelist']).'", { id: $(this).val() } )
                                            .done(function( data ) {
                                                $( "#'.Html::getInputId($model, 'stateID').'" ).html( data );
                                            }
                                        );'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?></br>
	<?= $form->field($model, 'cityID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(City::find()->where(['cityStatus' => 'Active'])->all(),'cityID','cityName'),
                    'options' => ['placeholder' => 'Select City'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?>
</div>
<div class="col-sm-3">

    <?= $form->field($model, 'facultyPincode')->textInput(['maxlength' => true]) ?></br>
	<?= $form->field($model, 'stateID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(State::find()->where(['stateStatus' => 'Active'])->all(),'stateID','stateName'),
                    'options' => ['placeholder' => 'Select State',
                                  'onchange'=>'         
                                        $.get( "'.Url::toRoute(['city/citylist']).'", { id: $(this).val() } )
                                            .done(function( data ) {
                                                $( "#'.Html::getInputId($model, 'cityID').'" ).html( data );
                                            }
                                        );'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?></br>
 <?= $form->field($model, 'facultyStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
<div class="col-sm-6">
<?php

	  echo $form->field($model, 'facultyProfilePicture')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'image/*'],
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['jpg','png','jpeg'],
				  'showPreview' => true,
				  'showCaption' => true,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,
                  'initialPreview'=>!empty($model->facultyProfilePicture) ? "../../uploads/faculty/".$model->facultyProfilePicture : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>false,
				   'initialPreviewConfig' => [
					[
						'caption' => $model->facultyProfilePicture,
						//'width' => "120px",
						'url' => Url::toRoute(['faculty/delete-faculty-image','facultyID'=>$model->facultyID,'facultyProfilePicture'=>$model->facultyProfilePicture]),
						'key' => $model->facultyID,
						'facultyProfilePicture' => $model->facultyProfilePicture,
						

					],

				],
               
	 ]
   ]);
   
 ?> 
</div>
</div>



	<?= $form->field($model, 'moduleID')->hiddenInput(['value' => "0"])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../faculty/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
