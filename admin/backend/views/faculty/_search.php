<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FacultySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faculty-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'facultyID') ?>

    <?= $form->field($model, 'facultyFullName') ?>

    <?= $form->field($model, 'facultyMobile') ?>

    <?= $form->field($model, 'facultyEmail') ?>

    <?= $form->field($model, 'facultyPassword') ?>

    <?php // echo $form->field($model, 'facultyBirthDate') ?>

    <?php // echo $form->field($model, 'facultyGender') ?>

    <?php // echo $form->field($model, 'facultyDOB') ?>

    <?php // echo $form->field($model, 'facultyJoinDate') ?>

    <?php // echo $form->field($model, 'moduleID') ?>

    <?php // echo $form->field($model, 'facultyProfilePicture') ?>

    <?php // echo $form->field($model, 'facultyAddress') ?>

    <?php // echo $form->field($model, 'facultyPincode') ?>

    <?php // echo $form->field($model, 'countryID') ?>

    <?php // echo $form->field($model, 'stateID') ?>

    <?php // echo $form->field($model, 'cityID') ?>

    <?php // echo $form->field($model, 'facultyDeviceType') ?>

    <?php // echo $form->field($model, 'facultyDeviceID') ?>

    <?php // echo $form->field($model, 'facultyReferKey') ?>

    <?php // echo $form->field($model, 'facultyVerified') ?>

    <?php // echo $form->field($model, 'facultyStatus') ?>

    <?php // echo $form->field($model, 'facultyCreatedDate') ?>

    <?php // echo $form->field($model, 'facultyOTP') ?>

    <?php // echo $form->field($model, 'facultySignupOTPVerified') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
