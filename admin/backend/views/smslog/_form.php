<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Smslog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="smslog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'smslogMobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smslogMessage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smslogResponse')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smslogDate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
