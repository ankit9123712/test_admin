<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Smslog */

$this->title = 'Update Smslog: ' . $model->smslogID;
$this->params['breadcrumbs'][] = ['label' => 'Smslogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->smslogID, 'url' => ['view', 'id' => $model->smslogID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="smslog-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
