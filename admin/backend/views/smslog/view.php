<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Smslog */

$this->title = 'View SMS Log: '.$model->smslogID;
$this->params['breadcrumbs'][] = ['label' => 'SMS Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smslog-view">

    

    <p>
        
        <?= Html::a('Delete', ['delete', 'id' => $model->smslogID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'smslogID',
            'smslogMobile',
            'smslogMessage',
            'smslogResponse',
            'smslogDate',
        ],
    ]) ?>

</div>
