<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Smslog */

$this->title = 'Create Smslog';
$this->params['breadcrumbs'][] = ['label' => 'Smslogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smslog-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
