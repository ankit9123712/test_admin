<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ReasonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reason-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'reasonID') ?>

    <?= $form->field($model, 'reasonName') ?>

    <?= $form->field($model, 'reasonStatus') ?>

    <?= $form->field($model, 'reasonCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
