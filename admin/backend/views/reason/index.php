<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReasonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reasons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reason-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Reason', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'reason','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'reasonID',
            'reasonName',
            'reasonStatus',
           // 'reasonCreatedDate',
	            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->reasonStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->reasonID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->reasonID],['title' => 'Make Active']);
						}
					},
            ]
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
