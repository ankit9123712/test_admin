<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Faqtopic */

$this->title = 'Update FAQ Topic: ' . $model->faqtopicID;
$this->params['breadcrumbs'][] = ['label' => 'FAQ Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->faqtopicID, 'url' => ['view', 'id' => $model->faqtopicID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="faqtopic-update">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
