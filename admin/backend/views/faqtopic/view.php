<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Faqtopic */

$this->title = 'View FAQ Topic: ' . $model->faqtopicID;
$this->params['breadcrumbs'][] = ['label' => 'FAQ Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faqtopic-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->faqtopicID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'faqtopicID',
            'faqtopicName',
            'faqtopicStatus',
        ],
    ]) ?>

</div>
