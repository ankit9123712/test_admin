<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Faqtopic */

$this->title = 'Create FAQ Topic';
$this->params['breadcrumbs'][] = ['label' => 'FAQ Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faqtopic-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
