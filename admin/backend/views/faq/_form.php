<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Faqtype;

/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-6">

<?= $form->field($model, 'faqtypeID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Faqtype::find()->where(['faqtypeStatus' => 'Active'])->all(),'faqtypeID','faqtypeName'),
                    'options' => ['placeholder' => 'Select Faq Type'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
<?= $form->field($model, 'faqQuestion')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'faqPosition')->textInput() ?>
<?= $form->field($model, 'faqStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>

<div class="col-sm-6">

<?= $form->field($model, 'faqAnswer')->widget(TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?></div>
</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
