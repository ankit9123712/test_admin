<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FaqSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'faqID') ?>

    <?= $form->field($model, 'faqtypeID') ?>

    <?= $form->field($model, 'faqQuestion') ?>

    <?= $form->field($model, 'faqAnswer') ?>

    <?= $form->field($model, 'faqPosition') ?>

    <?php // echo $form->field($model, 'faqStatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
