<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;
/* @var $this yii\web\View */
/* @var $model backend\models\Faq */

$this->title = 'View Faq';
$this->params['breadcrumbs'][] = ['label' => 'Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->faqID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'faqID',
            [
				'label'=>'Faq Type',            
				'attribute'=>'faqtype.faqtypeName',
		   ],
		   [
			   'attribute' => 'faqQuestion',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->faqQuestion,
           ],
           // 'faqQuestion',
          //  'faqAnswer',
			 [
			   'attribute' => 'faqAnswer',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->faqAnswer,
           ],
            'faqPosition',
            'faqStatus',
        ],
    ]) ?>

</div>
