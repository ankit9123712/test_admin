<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;use marekpetras\yii2ajaxboxwidget\Box;use backend\models\Users;use backend\models\Orders;use backend\models\Settings;
use practically\chartjs\Chart;

//use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$this->title = 'EnglishMonk';

 
?>
 <script type="text/javascript" src="../../js/jquery.min.js"></script>
<script type="text/javascript" src="../../js/Chart.min.js"></script> 
<div class="row">

		<div class="col-md-6 col-sm-6 col-xs-12">
			
			
          <div class="info-box" style="background-color: #ffffff !important;">
			<p>&nbsp;&nbsp;<i class="ion ion-ios-people"></i>&nbsp;&nbsp;<font size="4px">Users Statistics</font></p>
			<div id="registeredUser" style="width: 100%;">
				<?php 
	$sql =  "select userType,
	sum(if(`userCreatedDate` between date_format(NOW(),'%Y-%m-01 00:00:00') and date_format(last_day(NOW()),'%Y-%m-%d 23:59:59'),1,0)) AS '01M',
	sum(if(`userCreatedDate` between date_format(NOW() - interval 1 month,'%Y-%m-01 00:00:00') and date_format(last_day(NOW() - interval 1 month),'%Y-%m-%d 23:59:59'),1,0)) AS '02M' ,
	sum(if(`userCreatedDate` between date_format(NOW() - interval 2 month,'%Y-%m-01 00:00:00') and date_format(last_day(NOW() - interval 2 month),'%Y-%m-%d 23:59:59'),1,0)) AS  '03M',
	sum(if(`userCreatedDate` between date_format(NOW() - interval 3 month,'%Y-%m-01 00:00:00') and date_format(last_day(NOW() - interval 3 month),'%Y-%m-%d 23:59:59'),1,0)) '04M' ,
	sum(if(`userCreatedDate` between date_format(NOW() - interval 4 month,'%Y-%m-01 00:00:00') and date_format(last_day(NOW() - interval 4 month),'%Y-%m-%d 23:59:59'),1,0)) '05M' ,
	sum(if(`userCreatedDate` between date_format(NOW() - interval 5 month,'%Y-%m-01 00:00:00') and date_format(last_day(NOW() - interval 5 month),'%Y-%m-%d 23:59:59'),1,0)) '06M' 
	from `users` 
	group by `users`.`userType`";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		//echo $sql;die;
		$dt = date('Y-m-d');
		$lbls= array();
		$lbls[0]=  date("M,y") ;
		for($i=0;$i<5;$i++)
		{	$j=$i+1;
			$lbls[$j]  =  date("M,y", strtotime ( '-'.$j.' month' ,strtotime($dt) ))  ;
		}
				echo "<table width='100%' class='table'>
				<tr><td colspan='7' align='center'><h4>Last 6 Months Free Vs Paid</h4></td></tr>
				<tr>
					<th>User Type</th>
					<th>".$lbls[0]."</th>
					<th>".$lbls[1]."</th>
					<th>".$lbls[2]."</th>
					<th>".$lbls[3]."</th>
					<th>".$lbls[4]."</th>
					<th>".$lbls[5]."</th>
				</tr>";
				for($i=0;$i<count($result);$i++)
				{
					//print_r($result[$i]);die;
				?>		
					<tr>
						<td><?=$result[$i]["userType"]?></td>
						<td><?=$result[$i]["01M"]?></td>
						<td><?=$result[$i]["02M"]?></td>
						<td><?=$result[$i]["03M"]?></td>
						<td><?=$result[$i]["04M"]?></td>
						<td><?=$result[$i]["05M"]?></td>
						<td><?=$result[$i]["06M"]?></td>						
					</tr>
				<?php 
				} echo "<tr><td colspan='7' align='center'>&nbsp;</td></tr></table>"; ?> 
				  
			</div>
			
			<div id="registeredUser" style="width: 100%;">
				<?php 
	$sql =  "Select concat(cityName,', ',stateName) as stateCity, 
			SUM(CASE WHEN userType = 'Free' THEN 1 ELSE 0 END) AS freeCount,
			SUM(CASE WHEN userType != 'Free' THEN 1 ELSE 0 END) AS paidCount,
			SUM(CASE WHEN userStatus = 'Active' THEN 1 ELSE 0 END) AS activeCount,
			SUM(CASE WHEN userStatus != 'Active' THEN 1 ELSE 0 END) AS inActiveCount
			from users
			inner join state on state.stateID = users.stateID
			inner join city on city.cityID = users.cityID
			group by users.stateID, users.cityID";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		//echo $sql;die;
		 
				echo "<table width='100%' class='table'>
				<tr><td colspan='5' align='center'><h4>Statewise Free Vs Paid</h4></td></tr>
				<tr>
					<th>City, State</th>
					<th>Free Users</th>
					<th>Paid Users</th>
					<th>Active Users</th>	
					<th>Inactive Users</th>					
				</tr>";
				for($i=0;$i<count($result);$i++)
				{
					//print_r($result[$i]);die;
				?>		
					<tr>
						<td><?=$result[$i]["stateCity"]?></td>
						<td><?=$result[$i]["freeCount"]?></td>
						<td><?=$result[$i]["paidCount"]?></td>
						<td><?=$result[$i]["activeCount"]?></td>
						<td><?=$result[$i]["inActiveCount"]?></td>
						 						
					</tr>
				<?php 
				} echo "<tr><td colspan='5' align='center'>&nbsp;</td></tr></table>"; ?> 
				  
			</div>
			
          </div>
		   
          <!-- /.info-box -->
        </div>
		
		
		
		<div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
			<p>&nbsp;&nbsp;<i class="ion ion-ios-book"></i>&nbsp;&nbsp;<font size="4px">Lesson Statistics</font></p>
			<div id="registeredUser" style="width: 100%;">
				<?php 
	$sql =  "Select max(questionbankID) as 'Questionbank', count(questionbankID) as cnt,
case 
when max(questionbankCreatedDate) is not null then to_days(current_timestamp()) - to_days(max(questionbankCreatedDate)) 
when max(questionbankCreatedDate) is null then to_days(current_timestamp()) - to_days('2021-05-01') end AS `purchaseDays` 
FROM questionbank";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		//echo $sql;die;
		
				echo "<table width='100%' class='table'>
				<tr><td colspan='3' align='center'><h4>Questionbank Vs Practice Bank</h4></td></tr>
				<tr>
					<th>Information</th>
					<th>Total Added</th>
					<th>Last Uploaded</th>
					 
					
				</tr>";
				for($i=0;$i<count($result);$i++)
				{
					//print_r($result[$i]);die;
				?>		
					<tr>
						<td>Question Bank</td>
						<td><?=$result[$i]["cnt"]-1?></td>
						<td><?=$result[$i]["purchaseDays"]-1?></td>
						 
												
					</tr>
				<?php 
				} 
				
				$sql =  "Select count(PracticeBankID) as cnt,
case 
when max(PracticeBankCreatedDate) is not null then to_days(current_timestamp()) - to_days(max(PracticeBankCreatedDate)) 
when max(PracticeBankCreatedDate) is null then to_days(current_timestamp()) - to_days('2021-05-01') end AS `purchaseDays` 
FROM practicebank
		";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
				for($i=0;$i<count($result);$i++)
				{
					//print_r($result[$i]);die;
				?>
				<tr>
						<td>Practice Bank</td>
						<td><?=$result[$i]["cnt"]-1?></td>
						<td><?=$result[$i]["purchaseDays"]-1?></td>
						 
												
					</tr>
				<?php
				}
				echo "<tr><td colspan='3' align='center'>&nbsp;</td></tr></table>"; ?> 
				  
			</div>
			

			<?php 
	$sql =  "SELECT 
			COUNT( DISTINCT (CASE WHEN moduleName = 'Reading' THEN lesson.LessonID ELSE 0 END)) AS `ReadingCount`,
			COUNT( DISTINCT (CASE WHEN moduleName = 'Writing' THEN lesson.LessonID ELSE 0 END)) AS `WritingCount`,
			COUNT( DISTINCT (CASE WHEN moduleName = 'Listening' THEN lesson.LessonID ELSE 0 END)) AS `ListeningCount`,
			COUNT( DISTINCT (CASE WHEN moduleName = 'Speaking' THEN lesson.LessonID ELSE 0 END)) AS `SpeakingCount`
			FROM lesson
			INNER JOIN coursemodule on coursemodule.moduleID= lesson.moduleID 
			WHERE moduleType='Premium'";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		//echo $sql;die;
		
				echo "<table width='100%' class='table'>
				<tr><td colspan='6' align='center'><h4>Active Lesson Vs Tought</h4></td></tr>
				<tr>
					<th>Information</th>
					<th>Reading</th>
					<th>Writing</th>
					<th>Speaking</th>
					<th>Listening</th>
					<th>Total</th>
					
				</tr>";
				for($i=0;$i<count($result);$i++)
				{
					//print_r($result[$i]);die;
				?>		
					<tr>
						<td>Premium Lesson</td>
						<td><?=$result[$i]["ReadingCount"]-1?></td>
						<td><?=$result[$i]["WritingCount"]-1?></td>
						<td><?=$result[$i]["SpeakingCount"]-1?></td>
						<td><?=$result[$i]["ListeningCount"]-1?></td>
						<td><?=($result[$i]["ReadingCount"]+$result[$i]["WritingCount"]+$result[$i]["SpeakingCount"]+$result[$i]["ListeningCount"])-4?></td>
												
					</tr>
				<?php 
				} 
				
				$sql =  "SELECT 
			COUNT( DISTINCT (CASE WHEN moduleName = 'Reading' THEN livesessiondetails.lessonID ELSE 0 END)) AS `ReadingCount`,
			COUNT( DISTINCT (CASE WHEN moduleName = 'Writing' THEN livesessiondetails.lessonID ELSE 0 END)) AS `WritingCount`,
			COUNT( DISTINCT (CASE WHEN moduleName = 'Listening' THEN livesessiondetails.lessonID ELSE 0 END)) AS `ListeningCount`,
			COUNT( DISTINCT (CASE WHEN moduleName = 'Speaking' THEN livesessiondetails.lessonID ELSE 0 END)) AS `SpeakingCount`
			FROM livesessiondetails
			INNER JOIN coursemodule on coursemodule.moduleID= livesessiondetails.moduleID 
			WHERE moduleType='Premium'";
		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
				for($i=0;$i<count($result);$i++)
				{
					//print_r($result[$i]);die;
				?>
				<tr>
						<td>Tought Lesson</td>
						<td><?=$result[$i]["ReadingCount"]-1?></td>
						<td><?=$result[$i]["WritingCount"]-1?></td>
						<td><?=$result[$i]["SpeakingCount"]-1?></td>
						<td><?=$result[$i]["ListeningCount"]-1?></td>
						<td><?=($result[$i]["ReadingCount"]+$result[$i]["WritingCount"]+$result[$i]["SpeakingCount"]+$result[$i]["ListeningCount"])-4?></td>
												
					</tr>
				<?php
				}
				echo "<tr><td colspan='6' align='center'>&nbsp;</td></tr></table>"; ?> 
				  
			</div>
					
          </div>
          <!-- /.info-box -->
        </div>

        
		 
		 
      </div>
	  
<script type="text/javascript">
        window.setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
        });
        }, 1500);
    </script>
       
    <script>
        $(document).ready(function () {
            //showGraph();
        });


        function showGraph()
        {
            {
                $.get("./getmonthlyfreepaidusers",
                function (data)
                {
					console.log(data);
					var myJSON = JSON.stringify(data);

					var chartdata = JSON.parse(data);
                    //console.log(myJSON);
                    /*var name = [];
                    var marks = [];
					
					 
					var a1 = new Array();
					a1=data.split("|");
					

					for (i = 0; i < a1.length; i++) 
					{
						//console.log(a1[i]);
						a2=a1[i].split("~");
						//document.getElementById("updateAvailable_" + a[i]).style.visibility
						 name.push(a2[0]);
                        marks.push(a2[1]); 														
					}
	*/
    /*                for (var i in data) {
						console.log(data[i].categoryName);
                        name.push(data[i].categoryName);
                        marks.push(data[i].s); 
                    }
*/
                   /* var chartdata = {
                        labels: name,
                        datasets: [
                            {
                                label: 'Recent Six Months Users',
                                backgroundColor: '#49e2ff',
                                borderColor: '#46d5f1',
                                hoverBackgroundColor: '#CCCCCC',
                                hoverBorderColor: '#666666',
                                data: marks
                            }
                        ]
                    };
*/
                    var graphTarget = $("#registeredUsers");

                    var barGraph = new Chart(graphTarget, {
                        type: 'bar',
                        data: chartdata
                    });
                });
            }
        }
        </script>



<style>
    .file-upload-indicator
    {
        display: none;
    }
	caption
	{
		font-size:22px;
		padding:10px;
	}
</style>