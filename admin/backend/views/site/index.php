<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;use marekpetras\yii2ajaxboxwidget\Box;use backend\models\Users;use backend\models\Orders;use backend\models\Settings;
//use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$this->title = 'EnglishMonk';


$totalCustomers=0;
$activeCustomers=0;
$inactiveCustomers=0;

$freeLessonCount=0;
$premiumLessonCount=0;
$totalLessonSubmittedCount=0;

$freeExamCount=0;
$paidExamCount=0;
$totalExamCount=0;




$time = new \DateTime('now');
$today = $time->format('Y-m-d');

$sql = " SELECT 
       SUM(CASE WHEN userType = 'Free' THEN 1 ELSE 0 END) AS `activeCount`,
       SUM(CASE WHEN userType = 'Premium' THEN 1 ELSE 0 END) AS `inactiveCount`,
       SUM(CASE WHEN userType  IS NOT NULL THEN 1 ELSE 0 END) AS `totalCount`
from users
";
$connection = Yii::$app->getDb();
$command = $connection->createCommand($sql);
$result = $command->queryAll();
if(count($result)>0)
{
   for($i=0;count($result)>$i; $i++) 
   {
		$totalCustomers=$result[$i]["totalCount"];
		$activeCustomers=$result[$i]["activeCount"];
		$inactiveCustomers=$result[$i]["inactiveCount"];	
   }
}	   
/*Lessons*/
$sql = " SELECT 
       COUNT(DISTINCT CASE WHEN userType = 'Free' THEN users.userID ELSE NULL END) AS `freeLessonCount`,
       COUNT(DISTINCT CASE WHEN userType = 'Premium' THEN users.userID ELSE NULL END) AS `paidLessonCount`,
       COUNT(DISTINCT CASE WHEN userType  IS NOT NULL THEN users.userID ELSE NULL END) AS `totalCount`
from users
INNER JOIN userlessions on users.userID = userlessions.userID

";
$connection = Yii::$app->getDb();
$command = $connection->createCommand($sql);
$result = $command->queryAll();
if(count($result)>0)
{
   for($i=0;count($result)>$i; $i++) 
   {
		$freeLessonCount=$result[$i]["freeLessonCount"];
		$premiumLessonCount=$result[$i]["paidLessonCount"];
		$totalLessonSubmittedCount=$result[$i]["totalCount"];	
   }
}
/*Orders*/
$sql = " SELECT 
       COUNT(DISTINCT CASE WHEN userType = 'Free' THEN users.userID ELSE NULL END) AS `freeExamCount`,
       COUNT(DISTINCT CASE WHEN userType = 'Premium' THEN users.userID ELSE NULL END) AS `paidExamCount`,
       COUNT(CASE WHEN userType = 'Premium'  THEN users.userID ELSE NULL END) AS `totalExamCount`
from users
INNER JOIN userexams on users.userID = userexams.userID
";
$connection = Yii::$app->getDb();
$command = $connection->createCommand($sql);
$result = $command->queryAll();
if(count($result)>0)
{
   for($i=0;count($result)>$i; $i++) 
   {
		$freeExamCount=$result[$i]["freeExamCount"];
		$paidExamCount=$result[$i]["paidExamCount"];
		$totalExamCount=$result[$i]["totalExamCount"];
 
   }
}

/*Revenue*/
/*$sql = " SELECT 
        SUM(CASE WHEN statusID IN (7,17) AND orderDate =  CURDATE() THEN orderOurCharges ELSE 0 END) AS `todayRevenue`,
       SUM(CASE WHEN statusID IN (7,17) AND orderDate =  subdate(curdate(), 1) THEN orderOurCharges ELSE 0 END) AS `yesterdayRevenue`,
       SUM(CASE WHEN statusID IN (7,17) AND yearweek(orderDate) =  yearweek(curdate()) THEN orderOurCharges ELSE 0 END) AS `thisWeeRevenue`,
       SUM(CASE WHEN statusID IN (7,17) AND MONTH(orderDate) = MONTH(CURRENT_DATE()) AND YEAR(orderDate) = YEAR(CURRENT_DATE()) THEN orderOurCharges ELSE 0 END) AS `thisMonthRevenue`,
       SUM(CASE WHEN statusID IN (7,17) THEN orderOurCharges ELSE 0 END) AS `totalRevenue`
from orders
";
$connection = Yii::$app->getDb();
$command = $connection->createCommand($sql);
$result = $command->queryAll();
if(count($result)>0)
{
   for($i=0;count($result)>$i; $i++) 
   {
		$todayRevenue=$result[$i]["todayRevenue"];
		$yesterdayRevenue=$result[$i]["yesterdayRevenue"];
		$thisWeeRevenue=$result[$i]["thisWeeRevenue"];
		$thisMonthRevenue=$result[$i]["thisMonthRevenue"];
		$totalRevenue=$result[$i]["totalRevenue"];	
   }
}*/

?>
<legend>
<fieldset><i class="ion ion-ios-people"></i>&nbsp;&nbsp;Users Statistics</fieldset>
<section class="content" style="min-height: 137px! important;">
<div class="row">

		<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people"></i></span>

            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="../users/index">Total Users</span></a>
              <span class="info-box-number"> <?=$totalCustomers?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
            <span class="info-box-icon bg-maroon"><i class="ion ion-ios-people"></i></span>

            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="../users/index">Free</span></a>
              <span class="info-box-number"> <?=$activeCustomers?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		
		<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
           
		<span class="info-box-icon bg-green"><i class="ion ion-ios-people"></i></span>
            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="../users/index">Premium</span></a>
              <span class="info-box-number"> <?=$inactiveCustomers?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div> 
        <!-- /.col -->

        <!-- /.col -->
      </div>
</section>
</legend>
<legend>
<fieldset><i class="ion ion-ios-person"></i>&nbsp;&nbsp;Practice Statistics</fieldset>
<section class="content" style="min-height: 137px! important;">
<div class="row">

		<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="info-box" style="background-color: #ffffff !important;">
         
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-person"></i></span>

            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="../userlessions/index">Total Practice</span></a>
              <span class="info-box-number"> <?=$totalLessonSubmittedCount?>/<?=$totalCustomers?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          </div>
		
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
            <span class="info-box-icon bg-maroon"><i class="ion ion-ios-person"></i></span>

            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="../userlessions/index?userType=Free">Free</span></a>
              <span class="info-box-number"> <?=$freeLessonCount?>/<?=$activeCustomers?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		
		<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
           
		<span class="info-box-icon bg-green"><i class="ion ion-ios-person"></i></span>
            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="../userlessions/index?userType=Premium">Premium</span></a>
              <span class="info-box-number"> <?=$premiumLessonCount?>/<?=$inactiveCustomers?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div> 
        <!-- /.col -->

        <!-- /.col -->
      </div>
</section>
<legend>

<legend>
<fieldset><i class="ion ion-ios-book"></i>&nbsp;&nbsp;Exam Statistics</fieldset>
<section class="content" style="min-height: 137px! important;">
<div class="row">

		<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="info-box" style="background-color: #ffffff !important;">
         
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-book"></i></span>

            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="../userexams/index">Total</span></a>
              <span class="info-box-number"> <?=$totalExamCount?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          </div>
		
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
            <span class="info-box-icon bg-green"><i class="ion ion-ios-book"></i></span>

            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="../userexams/index">Given By</span></a>
              <span class="info-box-number"> <?=$paidExamCount?>/<?=$inactiveCustomers?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		
		 		
		
      </div>
</section>
</legend>
<?php /*
<legend>
<fieldset><i class="ion ion-logo-usd"></i>&nbsp;&nbsp;Revenue Statistics</fieldset>
<section class="content" style="min-height: 137px! important;">
<div class="row">

		<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="info-box" style="background-color: #ffffff !important;">
         
            <span class="info-box-icon bg-aqua"><i class="ion ion-logo-usd"></i></span>

            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="#">Total</span></a>
              <span class="info-box-number"> <?=$totalRevenue?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          </div>
		
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
            <span class="info-box-icon bg-maroon"><i class="ion ion-logo-usd"></i></span>

            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="#">Today</span></a>
              <span class="info-box-number"> <?=$todayRevenue?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		
		<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
           
		<span class="info-box-icon bg-green"><i class="ion ion-logo-usd"></i></span>
            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="#">Yesterday</span></a>
              <span class="info-box-number"> <?=$yesterdayRevenue?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div> 
		
		<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
           
		<span class="info-box-icon bg-purple"><i class="ion ion-logo-usd"></i></span>
            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="#">This Week</span></a>
              <span class="info-box-number"> <?=$thisWeeRevenue?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div> 
		
		<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #ffffff !important;">
         
           
		<span class="info-box-icon bg-olive"><i class="ion ion-logo-usd"></i></span>
            <div class="info-box-content">
              <span style="font-size: 150%;"><a href="#">This Month</span></a>
              <span class="info-box-number"> <?=$thisMonthRevenue?> <small style="font-size: 70%;"></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div> 
        <!-- /.col -->

        <!-- /.col -->
      </div>
</section>
</legend>
*/?>