<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cityID') ?>

    <?= $form->field($model, 'countryID') ?>

    <?= $form->field($model, 'stateID') ?>

    <?= $form->field($model, 'cityName') ?>

    <?= $form->field($model, 'cityStatus') ?>

    <?php // echo $form->field($model, 'cityCreatedDate') ?>

    <?php // echo $form->field($model, 'cityLatitude') ?>

    <?php // echo $form->field($model, 'cityLongitude') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
