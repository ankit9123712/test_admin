<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\City */

$this->title = 'View City';
$this->params['breadcrumbs'][] = ['label' => 'Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->cityID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cityID',
          //  'countryID',
			[
				'label'=>'Country',            
				'attribute'=>'country.countryName',
		   ],
		   [
				'label'=>'State',            
				'attribute'=>'state.stateName',
		   ],
           // 'stateID',
            'cityName',
            'cityStatus',
            'cityCreatedDate',
           // 'cityLatitude',
           // 'cityLongitude',
        ],
    ]) ?>

</div>
