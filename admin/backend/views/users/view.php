<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Users */

$this->title = 'View Student: ' . $model->userID;
$this->params['breadcrumbs'][] = ['label' => 'Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

 
 $query = new \yii\db\Query;
 $row = $query->select('usersubscriptions.usersubscriptionID,
						usersubscriptions.usersubscriptionAmount,
						usersubscriptions.usersubscriptionCoponCode,
						usersubscriptions.usersubscriptionStartDate,
						usersubscriptions.usersubscriptionEndDate,
						usersubscriptionShift,
						usersubscriptionBatch,
						users.userID,
						users.userFullName')
                    ->from('usersubscriptions')
					->join(' INNER JOIN','users',"usersubscriptions.userID = users.userID")
                    ->Where('usersubscriptions.userID = '.$model->userID)
                    ->all();
					//echo $query->createCommand()->sql;die;
					//echo "<pre>";
					//print_r($row);die;


?>
<div class="users-view">

<ul class="nav nav-tabs" role="tablist"> 
	  
	  <li role="presentation" class="active"><a href="#basicinfo" aria-controls="basicinfo" role="tab" data-toggle="tab">Basic Info</a></li>
      <li role="presentation"><a href="#membership" aria-controls="membership" role="tab" data-toggle="tab">Membership Purchased</a></li>
	      
 
 </ul>
<div class="tab-content">
   <div role="tabpanel" class="tab-pane active " id="basicinfo"><br/>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'userID',
            'userFullName',
            //'userCountryCode',
            'userMobile',
            'userEmail',
          //  'userPassword',
          //  'userAge',
            'userDOB',
            'userGender',
             [
            'label'=>'Country',            
            'attribute'=>'country.countryName',
			],			
			[
				'label'=>'State',
				'attribute'=>'state.stateName',
			],
            [                
                'label'=>'City',
				'attribute'=>'city.cityName',                
            ],
            'userProfilePicture',
            'userUniChoice1',
            'userUniChoice2',
            'userUniChoice3',
			'userType',
        /*    'userLiveLectureNotify',
            'userTestNotify',
            'userResultNotify',
            'userAddress',
            'languageID',
            'userDeviceType',
            'userDeviceID',
            'userReferKey',
            'userVerified',
            'userNewsNotify',
            'userEventsNotify',
            'userNoticeNotify',
            'userAssignmentNotify',
            'userForumNotify',
            'userAdminNotify',
            'userType',*/
            'userStatus',
           
        ],
    ]) ?>

</div>
 <div role="tabpanel" class="tab-pane" id="membership"><br/>
 
<div class="container">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Sr.No</th>
        <th>Amount</th>
        <th>Coupon Code</th>
		<th>Shift</th>
		<th>Batch</th>
        <th>Purchased On</th>
        <th>Period</th>
		<th></th>
      </tr>
    </thead>
    <tbody>
	<?php for($i=0;$i<count($row);$i++) { ?>
      <tr>
	    
        <td><?=$i+1?></td>
        <td><?=$row[$i]['usersubscriptionAmount']?></td>
        <td><?=$row[$i]['usersubscriptionCoponCode']?></td>
		<td><?=$row[$i]['usersubscriptionBatch']?></td>
		<td><?=$row[$i]['usersubscriptionShift']?></td>
        <td><?=$row[$i]['usersubscriptionStartDate']?></td>
        <td><?=$row[$i]['usersubscriptionEndDate']?></td>
		
      </tr>
	<?php } ?>  
    </tbody>
  </table>
</div>
 
 
</div>
</div>

</div>
