<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'userFullName') ?>

    <?= $form->field($model, 'userCountryCode') ?>

    <?= $form->field($model, 'userMobile') ?>

    <?= $form->field($model, 'userEmail') ?>

    <?php // echo $form->field($model, 'userPassword') ?>

    <?php // echo $form->field($model, 'userAge') ?>

    <?php // echo $form->field($model, 'userDOB') ?>

    <?php // echo $form->field($model, 'userGender') ?>

    <?php // echo $form->field($model, 'countryID') ?>

    <?php // echo $form->field($model, 'stateID') ?>

    <?php // echo $form->field($model, 'cityID') ?>

    <?php // echo $form->field($model, 'userProfilePicture') ?>

    <?php // echo $form->field($model, 'userUniChoice1') ?>

    <?php // echo $form->field($model, 'userUniChoice2') ?>

    <?php // echo $form->field($model, 'userUniChoice3') ?>

    <?php // echo $form->field($model, 'userLiveLectureNotify') ?>

    <?php // echo $form->field($model, 'userTestNotify') ?>

    <?php // echo $form->field($model, 'userResultNotify') ?>

    <?php // echo $form->field($model, 'userAddress') ?>

    <?php // echo $form->field($model, 'languageID') ?>

    <?php // echo $form->field($model, 'userDeviceType') ?>

    <?php // echo $form->field($model, 'userDeviceID') ?>

    <?php // echo $form->field($model, 'userReferKey') ?>

    <?php // echo $form->field($model, 'userVerified') ?>

    <?php // echo $form->field($model, 'userNewsNotify') ?>

    <?php // echo $form->field($model, 'userEventsNotify') ?>

    <?php // echo $form->field($model, 'userNoticeNotify') ?>

    <?php // echo $form->field($model, 'userAssignmentNotify') ?>

    <?php // echo $form->field($model, 'userForumNotify') ?>

    <?php // echo $form->field($model, 'userAdminNotify') ?>

    <?php // echo $form->field($model, 'userType') ?>

    <?php // echo $form->field($model, 'userStatus') ?>

    <?php // echo $form->field($model, 'userOTP') ?>

    <?php // echo $form->field($model, 'userCreatedDate') ?>

    <?php // echo $form->field($model, 'userSignupOTPVerified') ?>

    <?php // echo $form->field($model, 'userIELTSMonth') ?>

    <?php // echo $form->field($model, 'userIELTSYear') ?>

    <?php // echo $form->field($model, 'userIELTSCity') ?>

    <?php // echo $form->field($model, 'userSecurityToken') ?>

    <?php // echo $form->field($model, 'useTokenExpirtyDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
