<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
       &nbsp; <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'users','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'userID',
            'userFullName',
            //'userCountryCode',
            'userMobile',
            'userEmail',
			[
			'attribute'=>'stateID',
			'label'=>'State',
			'value'=>'state.stateName'
		],
		[
			'attribute'=>'cityID',
			'label'=>'City',
			'value'=>'city.cityName'
		],
			'userType',
			[
				'label'=>'Shift',
				'value'=>'usersubscriptions.usersubscriptionBatch',
			],
			[
				'label'=>'Batch',
				'value'=>'usersubscriptions.usersubscriptionShift',
			],
			
			//'usersubscriptionBatch',
			//'usersubscriptionShift',
            // 'userPassword',
            // 'userAge',
            // 'userDOB',
            // 'userGender',
            // 'countryID',
            // 'stateID',
            // 'cityID',
            // 'userProfilePicture',
            // 'userUniChoice1',
            // 'userUniChoice2',
            // 'userUniChoice3',
            // 'userLiveLectureNotify',
            // 'userTestNotify',
            // 'userResultNotify',
            // 'userAddress',
            // 'languageID',
            // 'userDeviceType',
            // 'userDeviceID',
            // 'userReferKey',
            // 'userVerified',
            // 'userNewsNotify',
            // 'userEventsNotify',
            // 'userNoticeNotify',
            // 'userAssignmentNotify',
            // 'userForumNotify',
            // 'userAdminNotify',
            // 'userType',
             'userStatus',
            // 'userOTP',
             'userCreatedDate',
            // 'userSignupOTPVerified',
            // 'userIELTSMonth',
            // 'userIELTSYear',
            // 'userIELTSCity',
            // 'userSecurityToken',
            // 'useTokenExpirtyDate',
	            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}&nbsp;&nbsp;&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->userStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->userID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->userID],['title' => 'Make Active']);
						}
					},
            ]
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
