<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CoursesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'courseID') ?>

    <?= $form->field($model, 'streamID') ?>

    <?= $form->field($model, 'coursestdID') ?>

    <?= $form->field($model, 'courseName') ?>

    <?= $form->field($model, 'courseImage') ?>

    <?php // echo $form->field($model, 'courseDuration') ?>

    <?php // echo $form->field($model, 'courseMinAge') ?>

    <?php // echo $form->field($model, 'courseMaxAge') ?>

    <?php // echo $form->field($model, 'courseFree') ?>

    <?php // echo $form->field($model, 'courseDescription') ?>

    <?php // echo $form->field($model, 'courseEligiblity') ?>

    <?php // echo $form->field($model, 'courseCareer') ?>

    <?php // echo $form->field($model, 'coursePublished') ?>

    <?php // echo $form->field($model, 'courseStatus') ?>

    <?php // echo $form->field($model, 'courseCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
