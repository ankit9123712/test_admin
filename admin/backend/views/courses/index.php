<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CoursesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Courses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Courses', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'courses','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'courseID',
           // 'coursestdID',
		  [
			'attribute'=>'coursestdID',
			'label'=>'Category',
			'value'=>'coursestd.coursestdName'
		   ],
			'courseName',
			//'courseDuration',
		
		   //'streamID',
            
            'courseStatus',
            //'courseImage',
            // 'courseDuration',
            // 'courseMinAge',
            // 'courseMaxAge',
            // 'courseFree',
            // 'courseDescription:ntext',
            // 'courseEligiblity:ntext',
            // 'courseCareer:ntext',
            // 'coursePublished',
            // 'courseStatus',
            // 'courseCreatedDate',
	            [
                    'class' => 'yii\grid\ActionColumn',
	                'header'=>'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}&nbsp;&nbsp;{addcoursesubject}&nbsp;&nbsp;&nbsp;&nbsp;{addcoursechapters}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->courseStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->courseID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->courseID],['title' => 'Make Active']);
						}
					},	
					'addcoursesubject' => function ($url, $model, $key) 
					{			
						return  Html::a('<span><u>Add Subject</u></span>', ['coursesubjects/index', 'courseID' => $model->courseID],['title' => 'Add Subject']);
					},
					'addcoursechapters' => function ($url, $model, $key) 
					{			
						return  Html::a('<span><u>Add Chapter</u></span>', ['coursechapters/index', 'courseID' => $model->courseID],['title' => 'Add Chapter']);
					}
			
            ]
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
