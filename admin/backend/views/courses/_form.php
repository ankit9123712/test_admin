<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Stream;
use backend\models\Coursestd;
use kartik\file\FileInput;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Courses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

<?= $form->field($model, 'streamID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Stream::find()->where(['streamStatus' => 'Active'])->all(),'streamID','streamName'),
                    'options' => ['placeholder' => 'Select Stream'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
<div class="col-sm-4">

<?= $form->field($model, 'coursestdID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Coursestd::find()->where(['coursestdStatus' => 'Active'])->all(),'coursestdID','coursestdName'),
                    'options' => ['placeholder' => 'Select Coursestd'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'courseName')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-3">
<?= $form->field($model, 'courseDuration')->textInput() ?>
</div>

<div class="col-sm-3">
  <?= $form->field($model, 'courseMinAge')->textInput() ?>
</div>
<div class="col-sm-3">

    <?= $form->field($model, 'courseMaxAge')->textInput() ?>
</div>
<div class="col-sm-3">
<?= $form->field($model, 'courseFree')->dropDownList([ 'Free' => 'Free', 'Paid' => 'Paid', ]) ?>

</div>
</div>

<div class="row">
<div class="col-sm-6">
	<?= $form->field($model, 'courseImage')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],'pluginOptions' => [
                  'initialPreview'=>isset($model->courseImage) ? "../../uploads/course/".$model->courseImage : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>true,
                  'showPreview' => true,
                  'showCaption' => true,
                  'showRemove' => false,
                  'showUpload' => false
					]]);
					?>				

</div>


<div class="col-sm-6">
<?= $form->field($model, 'courseDescription')->widget(TinyMce::className(), [
        'options' => ['rows' => 8],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
    
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">

    <?= $form->field($model, 'courseEligiblity')->textarea(['rows' => 6]) ?>
</div>
<div class="col-sm-6">

    <?= $form->field($model, 'courseCareer')->textarea(['rows' => 6]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'coursePublished')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'courseStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
