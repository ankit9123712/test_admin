<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;
use backend\models\coursesubjects;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model backend\models\Courses */

$this->title = 'View Courses: ' . $model->courseID;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->courseID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'courseID',
            //'streamID',
			[
				'label'=>'Stream',
				'attribute'=>'stream.streamName'
			],
            //'coursestdID',
			[
				'label'=>'Coursestd',
				'attribute'=>'coursestd.coursestdName'
			],
            'courseName',
            //'courseImage',
			[                
                'attribute'=>'courseImage',
                'value'=>   ((($model->courseImage == NULL) || (($model['courseImage'] == ''))) ? "../../../web/images/noimage.png": '../../../web/uploads/course'.'/'.$model->courseImage),
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'courseDuration',
            'courseMinAge',
            'courseMaxAge',
            'courseFree',
            [
				'attribute' => 'courseDescription',
				'format' => 'raw',
				'type' => 'widget',
				'widgetOptions' => ['class' => TinyMce::classname()],
				'value' => $model->courseDescription,
			 ],
            'courseEligiblity:ntext',
            'courseCareer:ntext',
            'coursePublished',
          
            'courseCreatedDate',
			  'courseStatus',
        ],
    ]) ?>
<h3>Course Subject Detailes :</h3>
<?php
	/*------------------------------Start code coursechapters  display----------------------------------------*/
   $Coursesubjects_Billing_dataProvider = new ActiveDataProvider([
         'query' => Coursesubjects::find()->where('courseID = "'.$model->courseID.'"'),
         'pagination'=>false
		 
        ]);
		
     echo GridView::widget([
        'dataProvider' => $Coursesubjects_Billing_dataProvider,
        'columns' => [
		//'coursechapterNo',
		[
				'label'=>'Course Subject Name',
				'attribute'=>'coursesubjName'
			],
		//'coursesubjName',
		'coursesubjChapters',
	//	'coursechapterFile',
		'coursesubjStatus',
	
		/*[
                    'class' => 'yii\grid\ActionColumn',
					'header'=>'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->coursesubjStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->coursesubjID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->coursesubjID],['title' => 'Make Active']);
						}
					},
            ]
			],*/
		 ],
    ]); 
/*------------------------------End code Useraddress Fields display----------------------------------------*/
     ?>	 
	 
	 

</div>
