<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Coursestd;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model app\models\Subject */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subject-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

<?= $form->field($model, 'coursestdID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Coursestd::find()->where(['coursestdStatus' => 'Active'])->all(),'coursestdID','coursestdName'),
                    'options' => ['placeholder' => 'Select Course Standard'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>

<div class="col-sm-4">

    <?//= $form->field($model, 'subjectLastMonth')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">
     <?= $form->field($model, 'subjectName')->textInput(['maxlength' => true]) ?>    
    <?//= $form->field($model, 'subjectColor')->textInput(['maxlength' => true]) ?>
	    <?= $form->field($model, 'subjectRemarks')->textArea(['maxlength' => true,'rows'=> 3]) ?>
		<?= $form->field($model, 'subjectStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
<div class="col-sm-4">

<?//= $form->field($model, 'subjectIcon')->widget(FileInput::classname(), [
              //      'options' => ['accept' => 'image/*'],'pluginOptions' => [
              ///    'initialPreview'=>'',
              //    'initialPreviewAsData'=>true,
              //    'overwriteInitial'=>true,
              //    'showPreview' => true,
              //    'showCaption' => true,
              //    'showRemove' => false,
               //   'showUpload' => false
				//	]]);
				//	?></div>

</div>
<div class="row">
<div class="col-sm-4">

    
</div>

</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
