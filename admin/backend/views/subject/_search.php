<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SubjectSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subject-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'subjectID') ?>

    <?= $form->field($model, 'coursestdID') ?>

    <?= $form->field($model, 'subjectName') ?>

    <?= $form->field($model, 'subjectLastMonth') ?>

    <?= $form->field($model, 'subjectColor') ?>

    <?php // echo $form->field($model, 'subjectIcon') ?>

    <?php // echo $form->field($model, 'subjectRemarks') ?>

    <?php // echo $form->field($model, 'subjectStatus') ?>

    <?php // echo $form->field($model, 'subjectCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
