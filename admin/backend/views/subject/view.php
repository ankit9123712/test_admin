<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Subject */

$this->title = 'View Subject: ' . $model->subjectID;
$this->params['breadcrumbs'][] = ['label' => 'Subjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->subjectID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'subjectID',
            
				[    'label'=>'Course Standard',
                
                'attribute'=>'coursestd.coursestdName'
            ],
			
		
            'subjectName',
           // 'subjectLastMonth',
           // 'subjectColor',
          /*  [                
                'attribute'=>'subjectIcon',
                'value'=>   ((($model->subjectIcon == NULL) || (($model['subjectIcon'] == ''))) ? "../../../web/images/noimage.png": '../../../web/uploads/subject'.'/'.$model->subjectIcon),
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],*/
            'subjectRemarks',
            'subjectStatus',
           // 'subjectCreatedDate',
        ],
    ]) ?>

</div>
