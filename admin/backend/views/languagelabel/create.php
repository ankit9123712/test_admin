<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Languagelabel */

$this->title = 'Create Languagelabel';
$this->params['breadcrumbs'][] = ['label' => 'Languagelabels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="languagelabel-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
