<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Languagelabel */

$this->title = 'View Language Wise Label: '.$model->langLabelID;
$this->params['breadcrumbs'][] = ['label' => 'Language Wise Labels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="languagelabel-view">

    

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->langLabelID], ['class' => 'btn btn-primary']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'langLabelID',
            'langLabelKey',
            [
				'attribute' =>  'language.languageName',
				'label' => 'Language Name',
			],
            'langLabelValue',
            'langLabelModule',
            'langLabelStatus',
        ],
    ]) ?>

</div>
