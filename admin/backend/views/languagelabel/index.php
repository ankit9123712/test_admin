<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\LanguagelabelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Language Wise Labels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="languagelabel-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'languagelabel','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'langLabelID',
            'langLabelKey',
            'language.languageName',
            'langLabelValue',
            'langLabelModule',
            // 'langLabelStatus',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}',                    
             ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
