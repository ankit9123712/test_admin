<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Language;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Languagelabel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="languagelabel-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>

<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'langLabelKey')->textInput(['maxlength' => true,'readonly' => !$model->isNewRecord]) ?>

</div>

<div class="col-sm-4">


	<?= $form->field($model, 'languageID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Language::find()->where(['languageStatus' => 'Active'])->all(),'languageID','languageName'),
                    
					'disabled' => !$model->isNewRecord,
					'options' => ['placeholder' => 'Select Language'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'langLabelValue')->textInput(['maxlength' => true]) ?>

</div>

</div>

<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'langLabelModule')->textInput(['maxlength' => true,'readonly' => !$model->isNewRecord]) ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'langLabelStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => '']) ?>

</div>

</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
