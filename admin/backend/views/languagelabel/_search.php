<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LanguagelabelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="languagelabel-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'langLabelID') ?>

    <?= $form->field($model, 'langLabelKey') ?>

    <?= $form->field($model, 'languageID') ?>

    <?= $form->field($model, 'langLabelValue') ?>

    <?= $form->field($model, 'langLabelModule') ?>

    <?php // echo $form->field($model, 'langLabelStatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
