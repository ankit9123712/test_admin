<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Languagelabel */

$this->title = 'Update Language Wise Label: ' . $model->langLabelID;
$this->params['breadcrumbs'][] = ['label' => 'Language Wise Labels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->langLabelID, 'url' => ['view', 'id' => $model->langLabelID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="languagelabel-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
