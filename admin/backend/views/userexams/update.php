<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Userexams */

$this->title = 'Update User Exam: ' . $model->userexamID;
$this->params['breadcrumbs'][] = ['label' => 'User Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userexamID, 'url' => ['view', 'id' => $model->userexamID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="userexams-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
