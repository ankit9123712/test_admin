<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserexamsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userexams-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'userexamID') ?>

    <?= $form->field($model, 'examID') ?>

    <?= $form->field($model, 'userexamDate') ?>

    <?= $form->field($model, 'userexamGrade') ?>

    <?= $form->field($model, 'userexamVerifiedBy') ?>

    <?php // echo $form->field($model, 'userexamVerifiedDate') ?>

    <?php // echo $form->field($model, 'userexamVerifiedNotes') ?>

    <?php // echo $form->field($model, 'userID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
