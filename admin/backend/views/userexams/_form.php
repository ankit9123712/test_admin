<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Exams;
use backend\models\Users;
use kartik\date\DatePicker;

/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Userexams */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userexams-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">
<?= $form->field($model, 'examID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Exams::find()->where(['examStatus' => 'Active'])->all(),'examID','examName'),
                    'options' => ['placeholder' => 'Select Exams'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
<div class="col-sm-4">
<label>Exam Date</label>
<?php
echo DatePicker::widget([
	'name' => 'userexamDate', 
	'value' => $model->isNewRecord ? date('Y-M-d') : $model->userexamDate,
	'options' => ['placeholder' => 'Select Exam Date ...'],
	'pluginOptions' => [
		'format' => 'yyyy-M-dd',	
		'todayHighlight' => true
	]
]);?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'userexamGrade')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'userexamVerifiedBy')->textInput() ?>
</div>
<div class="col-sm-4">
<label>Verified Date</label>
<?php
echo DatePicker::widget([
	'name' => 'userexamVerifiedDate', 
	'value' => $model->isNewRecord ? date('Y-M-d') : $model->userexamVerifiedDate,
	'options' => ['placeholder' => 'Select Verified Date ...'],
	'pluginOptions' => [
		'format' => 'yyyy-M-dd',	
		'todayHighlight' => true
	]
]);?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'userexamVerifiedNotes')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

<?= $form->field($model, 'userID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Users::find()->where(['userStatus' => 'Active'])->all(),'userID','userFullName'),
                    'options' => ['placeholder' => 'Select Users'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../userexams/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
