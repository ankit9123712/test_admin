<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Userexams */

$this->title = 'Create User Exam';
$this->params['breadcrumbs'][] = ['label' => 'User Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userexams-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
