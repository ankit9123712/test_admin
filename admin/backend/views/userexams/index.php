<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserexamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Evalute Exams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userexams-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'userexams','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'userexamID',
        [
			'attribute'=>'examID',
			'label'=>'Exams',
			'value'=>'exams.examName'
		],
          //  'userexamDate',
           // 'userexamGrade',
		[
			'attribute'=>'userID',
			'label'=>'Users',
			'value'=>'users.userFullName'
		],
		[
			'attribute'=>'facultyID',
			'label'=>'Faculty',
			'value'=>'faculty.facultyFullName'
		],	
		[
				
				'label'=>'Verification',
				
				'value' => function ($model) {
					
					if(empty($model->userexamEvaluationDate))
					{
						return "Pending";
					}
					else
						return "Verified";
					
					
				},
				'format' => 'raw',
		   ],   
		/*[
			'attribute'=>'userexamVerifiedBy',
			'label'=>'Verified By',
			'value'=>'verifyfaculty.facultyFullName'
		],  */ 
            
		
            // 'userexamVerifiedDate',
            // 'userexamVerifiedNotes',
            // 'userID',
				[
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}',                    
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
