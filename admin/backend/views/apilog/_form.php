<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\models\Apilog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apilog-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'apiName')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'apiIP')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'apiRequest')->textarea(['rows' => 6]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'apiResponse')->textarea(['rows' => 6]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'apiType')->dropDownList([ 'iphone' => 'Iphone', 'android' => 'Android', 'web' => 'Web', ], ['prompt' => '']) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'apiVersion')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'apiCallDate')->textInput() ?>
</div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
