<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ApilogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Api Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apilog-index">

   <!-- <h1><?//= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        <?//= Html::a('Create Api Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'apilog','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'Sr.No'],

            //'apiID',
            'apiName',
            'apiIP',
            'apiRequest:ntext',
            'apiResponse:ntext',
            // 'apiType',
            // 'apiVersion',
            // 'apiCallDate',
				[
					'header' => 'Action',
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}&nbsp;&nbsp;&nbsp;&nbsp;',                    
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
