<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Apilog */

$this->title = 'View Api Log: ' . $model->apiID;
$this->params['breadcrumbs'][] = ['label' => 'Api Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apilog-view">

   <!-- <h1><?//= Html::encode($this->title) ?></h1>-->

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'apiID',
            'apiName',
            'apiIP',
            'apiRequest:ntext',
            'apiResponse:ntext',
            'apiType',
            'apiVersion',
            'apiCallDate',
        ],
    ]) ?>

</div>
