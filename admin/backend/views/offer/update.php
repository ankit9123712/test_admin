<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Offer */

$this->title = 'Update Offer: ' . $model->offerID;
$this->params['breadcrumbs'][] = ['label' => 'Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->offerID, 'url' => ['view', 'id' => $model->offerID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="offer-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
