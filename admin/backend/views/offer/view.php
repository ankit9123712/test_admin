<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Offer */

$this->title = 'View Offer: ' . $model->offerID;
$this->params['breadcrumbs'][] = ['label' => 'Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offer-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->offerID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'offerID',
            'offerName',
             [                
                'attribute'=>'offerImage',
                'value'=>   ((($model->offerImage == NULL) || (($model['offerImage'] == ''))) ? "../../../web/images/noimage.png": '../../../web/uploads/offer'.'/'.$model->offerImage),
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],	
            'offerDiscountType',
            'offerDiscountValue',
            'offerStartDate',
            'offerEndDate',
            'offerCodeUse',
            'offerMaxLimit',
            'offerCodeType',
            'offerCodeQty',
            'offerSalePrice',
            'offerDescription',
            'offerStatus',
            'offerCreatedDate',
        ],
    ]) ?>

</div>
