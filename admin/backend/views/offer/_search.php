<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OfferSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'offerID') ?>

    <?= $form->field($model, 'offerName') ?>

    <?= $form->field($model, 'offerImage') ?>

    <?= $form->field($model, 'offerDiscountType') ?>

    <?= $form->field($model, 'offerDiscountValue') ?>

    <?php // echo $form->field($model, 'offerStartDate') ?>

    <?php // echo $form->field($model, 'offerEndDate') ?>

    <?php // echo $form->field($model, 'offerCodeUse') ?>

    <?php // echo $form->field($model, 'offerMaxLimit') ?>

    <?php // echo $form->field($model, 'offerCodeType') ?>

    <?php // echo $form->field($model, 'offerCodeQty') ?>

    <?php // echo $form->field($model, 'offerSalePrice') ?>

    <?php // echo $form->field($model, 'offerDescription') ?>

    <?php // echo $form->field($model, 'offerStatus') ?>

    <?php // echo $form->field($model, 'offerCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
