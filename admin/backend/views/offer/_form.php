<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model backend\models\Offer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offer-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<?= $form->field($model, 'offerName')->textInput(['maxlength' => true]) ?>
	</div>
	
	<div class="col-sm-3">
		<label>Start Date</label>
	<?= DatePicker::widget([
	'model' => $model,
    //'form'=>$objActiveForm,	
	'name' => 'offerStartDate',
//date blanck	
	'value' => date('d M Y', strtotime($model->offerStartDate)),
	'options' => ['placeholder' => 'Select Start date'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'startDate' => date("Y-m-d"),
		'todayHighlight' => true
	]
]);?><br>
	</div>
	<div class="col-sm-3">
		<label>End Date</label>
	<?= DatePicker::widget([
	'model' => $model,
    //'form'=>$objActiveForm,	
	'name' => 'offerEndDate', 
	'value' => date('d M Y', strtotime($model->offerEndDate)),
	'options' => ['placeholder' => 'Select End date'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'startDate' => date('Y-m-d', strtotime('+1 days')),
		'todayHighlight' => true
	]
]);?><br>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'offerDiscountType')->dropDownList([ 'Percentage' => 'Percentage', 'Fixed' => 'Fixed', ]) ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-2">
		 <?= $form->field($model, 'offerDiscountValue')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-sm-2">
		<?= $form->field($model, 'offerCodeUse')->dropDownList([ 'One Time' => 'One Time', 'Multiple' => 'Multiple', ]) ?>
	</div>
	<div class="col-sm-1">
		<?= $form->field($model, 'offerMaxLimit')->textInput() ?>
	</div>
	<div class="col-sm-2">
		<?= $form->field($model, 'offerCodeType')->dropDownList([ 'Static' => 'Static', 'Dynamic' => 'Dynamic', ]) ?>
	</div>
	<div class="col-sm-3">
		<?= $form->field($model, 'offerCodeQty')->textInput() ?>
	</div>
	<div class="col-sm-2">
		<?= $form->field($model, 'offerStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
	</div>
</div>

<div class="row">
<div class="col-sm-4">
<?php								
			echo $form->field($model, 'offerImage')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'image/*'],	
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['jpg','png','jpeg'],
				  'showPreview' => true,
				  'showCaption' => true,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,
                  'initialPreview'=>!empty($model->offerImage) ? "../../uploads/offer/".$model->offerImage : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>false,
				   'initialPreviewConfig' => [
					[
						'caption' => $model->offerImage,
						//'width' => "120px",
						'url' => Url::toRoute(['offer/delete-offers-image','	offerID'=>$model->	offerID,'offerImage'=>$model->offerImage]),
						'key' => $model->	offerID,
						'offerImage' => $model->offerImage,
						

					],

				],
               
	 ]
   ]);
					
					?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'offerDescription')->textarea(['rows' => '8']) ?>
</div>
<div class="col-sm-4">

    		<?= $form->field($model, 'offerExclusive')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ]) ?>
</div>
</div>
 
 
 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../offer/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">

    $(function () {
        $("#offer-offerdiscounttype").change(function () {
            var selectedText = $(this).find("option:selected").text();
            var selectedValue = $(this).val();
			if(selectedValue == "Percentage")
			{
				$('#offer-offerdiscountvalue').keyup(function()
				{
				  if ($(this).val() > 100){
					//alert("No numbers above 100");
					$(this).val('100');
				  }
				});
			}
			else
			{
				$('#offer-offerdiscountvalue').keyup(function()
				{
				  if ($(this).val() > 10000){
					//alert("No numbers above 10000");
					$(this).val('10000');
				  }
				});
			}
			
        });
    });
</script>