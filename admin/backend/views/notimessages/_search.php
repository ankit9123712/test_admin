<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NotimessagesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notimessages-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'msgId') ?>

    <?= $form->field($model, 'msgTitle') ?>

    <?= $form->field($model, 'msgDescription') ?>

    <?= $form->field($model, 'msgStatus') ?>

    <?= $form->field($model, 'msgUserType') ?>

    <?php // echo $form->field($model, 'msgDate') ?>

    <?php // echo $form->field($model, 'msgCreatedDate') ?>

    <?php // echo $form->field($model, 'msgSendType') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
