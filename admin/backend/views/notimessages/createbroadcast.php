<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Notimessages */

$this->title = 'Create Broadcast';
$this->params['breadcrumbs'][] = ['label' => 'Notification Messages Management', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notimessages-create">

    

    <?= $this->render('createbroadcastform', [
        'model' => $model,
    ]) ?>

</div>
