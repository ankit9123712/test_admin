<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use backend\models\Country;
use backend\models\State;
use backend\models\City;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\Notimessages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notimessages-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'msgUserType')->dropDownList(['All' => 'All','Free' => 'Free', 'Premium' => 'Premium',]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'msgTitle')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">
 <?php echo '<label class="control-label">Date</label>';
        echo DatePicker::widget([
            'model' => $model,
            'name' => 'msgDate','attribute' => 'msgDate',
            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
            'value' => $model->msgDate,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
                'startDate'=> '0d',
                'todayHighlight' => true
        ]
    ]);?><br/>

   
</div>
</div>


 
<div class="row">

<div class="col-sm-4">

<?= $form->field($model, 'stateID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(State::find()->where(['stateStatus' => 'Active'])->all(),'stateID','stateName'),
                    'options' => ['placeholder' => 'Select State',
                                  'onchange'=>'         
                                        $.get( "'.Url::toRoute(['city/citylist']).'", { id: $(this).val() } )
                                            .done(function( data ) {
                                                $( "#'.Html::getInputId($model, 'cityID').'" ).html( data );
                                            }
                                        );'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?>
	<label style="font-size:11px!important;font-weight:normal!important;color:gray!important">Select State if you want to Send broadcast to entier state or state's city</label>
	<?= $form->field($model, 'msgStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive',]) ?>
</div>
<div class="col-sm-4">
	<?= $form->field($model, 'cityID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(City::find()->where(['cityStatus' => 'Active'])->all(),'cityID','cityName'),
                    'options' => ['placeholder' => 'Select City'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?>
    <label style="font-size:11px!important;font-weight:normal!important;color:gray!important">Select State if you want to Send broadcast to state's city</label>
</div>				
<div class="col-sm-4">
	<?= $form->field($model, 'msgDescription')->textArea(['rows' => 3]) ?>
    
</div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
