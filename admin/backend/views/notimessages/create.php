<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Notimessages */

$this->title = 'Create Broadcast Push Notification';
$this->params['breadcrumbs'][] = ['label' => 'Broadcast Push Notification', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notimessages-create">

   <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
