<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Notimessages */

$this->title = 'Update Broadcast Push Notification: ' . $model->msgId;
$this->params['breadcrumbs'][] = ['label' => 'Broadcast Push Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->msgId, 'url' => ['view', 'id' => $model->msgId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notimessages-update">

   <!-- <h1><?//= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
