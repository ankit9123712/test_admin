<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\NotimessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Broadcast Push Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notimessages-index">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Broadcast Push Notification', ['create'], ['class' => 'btn btn-success']) ?> &nbsp; <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'notimessages','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'msgId',
            'msgTitle',
			['class' => 'yii\grid\ActionColumn','header' => 'Broadcast','template' => '{modal_userotp}',            
                'buttons' => [       
                     'modal_userotp' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-bullhorn"></span>', Url::to('createbroadcast?id='.$model->msgId) , ['class' => 'btn btn-success view1', 'data-pjax' => '0', 'data-toggle'=>'tooltip','title'=>'Message Broadcast']);
                    },  
            ]], 
            'msgDescription',
            'msgStatus',
            'msgUserType',
            // 'msgDate',
            // 'msgCreatedDate',
            // 'msgSendType',
	            [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->msgStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->msgId],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->msgId],['title' => 'Make Active']);
						}
					},
            ]
			],
		
        ],
    ]);
	
	$this->registerJs(
        "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
          $('.view1').click(function(e){
          
            e.preventDefault(); 
            
            $('#pModal').modal('show')
                .find('.modal-body')
                .load($(this).attr('href'));  
         });
      });
    ");
    yii\bootstrap\Modal::begin([
        'id'=>'pModal',
        'header' => '<h4><b>Boradcast Detail</b></h4>',
    ]);
    yii\bootstrap\Modal::end(); 

	?>
    <?php Pjax::end(); ?>
</div>
