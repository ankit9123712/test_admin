<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Notimessages */

$this->title = 'View Broadcast Push Notification: ' . $model->msgId;
$this->params['breadcrumbs'][] = ['label' => 'Broadcast Push Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notimessages-view">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->msgId], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'msgId',
            'msgUserType',
			'msgTitle',
            'msgDate',
			[
            'label'=>'State',
            'attribute'=>'state.stateName',
			],
            [                
                'label'=>'City',
				'attribute'=>'city.cityName',                
            ],
			'msgDescription',
			'msgStatus',
            'msgCreatedDate',
           
        ],
    ]) ?>

</div>
