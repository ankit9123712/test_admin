<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Subscriptiondetails */

$this->title = 'Update Subscriptiondetails: ' . $model->subscriptionID;
$this->params['breadcrumbs'][] = ['label' => 'Subscriptiondetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->subscriptionID, 'url' => ['view', 'id' => $model->subscriptionID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subscriptiondetails-update">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
