<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model backend\models\Subscriptiondetails */

$this->title = 'View Subscribers: ' . $model->subscriptionID;
$this->params['breadcrumbs'][] = ['label' => 'Subscribers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriptiondetails-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

  

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'userFullName',
            'userMobile',
            'userEmail:email',
            'planDescription:ntext',
            'subscriptionStartDate',
            'subscriptionEndDate',
            //'subscriptionID',
        ],
    ]) ?>

</div>
