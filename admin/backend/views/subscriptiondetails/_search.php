<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SubscriptiondetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscriptiondetails-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'userFullName') ?>

    <?= $form->field($model, 'userMobile') ?>

    <?= $form->field($model, 'userEmail') ?>

    <?= $form->field($model, 'planDescription') ?>

    <?= $form->field($model, 'subscriptionStartDate') ?>

    <?php // echo $form->field($model, 'subscriptionEndDate') ?>

    <?php // echo $form->field($model, 'subscriptionID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
