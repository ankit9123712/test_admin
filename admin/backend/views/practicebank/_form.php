<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Coursemodule;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Practicebank */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="practicebank-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">
<div class="col-sm-6">
<?php //= $form->field($model, 'PracticeBankType')->dropDownList([ 'Free' => 'Free', 'Premium' => 'Premium',]) ?>

<?php

$PracticeBankType_Option = array('Free' => 'Free', 'Premium' => 'Premium');
echo $form->field($model, 'PracticeBankType')->widget(Select2::classname(), [
                    'data' => $PracticeBankType_Option,
                    'options' => ['placeholder' => 'Select Bank Type',
					'onchange'=>'         
										$.get( "'.Url::toRoute(['coursemodule/coursemodulelist']).'", { type: $(this).val() } )
											.done(function( data ) {
												$( "#'.Html::getInputId($model, 'moduleID').'" ).html( data );
											}
										  );'],
                      'pluginOptions' => [
                      'allowClear' => true
     ],
  ]);
 ?>
</div>
<div class="col-sm-6">

<?= $form->field($model, 'moduleID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Coursemodule::find()->where(['moduleStatus' => 'Active'])->all(),'moduleID','moduleName'),
                    'options' => ['placeholder' => 'Select Module'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
</div>
<div class="row">
<div class="col-sm-6">

    <?= $form->field($model, 'PracticeBankNo')->textInput() ?>
</div>
<div class="col-sm-6">

    <?= $form->field($model, 'PracticeBankName')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-6">

<?php

	echo $form->field($model, 'PracticeBankQuestionFile')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'jpg','png','jpeg','docx','pdf'],	
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['jpg','png','jpeg','docx','pdf'],
				  'showPreview' => true,
				  'showCaption' => true,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,
                  'initialPreview'=>!empty($model->PracticeBankQuestionFile) ? "../../uploads/practicebank/".$model->PracticeBankQuestionFile : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>false,
				   'initialPreviewConfig' => [
					[
						'caption' => $model->PracticeBankQuestionFile,
						//'width' => "120px",
						'url' => Url::toRoute(['practicebank/delete-practicebankquestion-image','PracticeBankID'=>$model->	PracticeBankID,'PracticeBankQuestionFile'=>$model->PracticeBankQuestionFile]),
						'key' => $model->PracticeBankID,
						'PracticeBankQuestionFile' => $model->PracticeBankQuestionFile,
						

					],

				],
               
	 ]
   ]);

?>
</div>

<div class="col-sm-6">

<?php

	echo $form->field($model, 'PracticeBankAnswerFile')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'jpg','png','jpeg','docx','pdf'],	
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['jpg','png','jpeg','docx','pdf'],
				  'showPreview' => true,
				  'showCaption' => true,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,
                  'initialPreview'=>!empty($model->PracticeBankAnswerFile) ? "../../uploads/practicebank/".$model->PracticeBankAnswerFile : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>false,
				   'initialPreviewConfig' => [
					[
						'caption' => $model->PracticeBankAnswerFile,
						//'width' => "120px",
						'url' => Url::toRoute(['practicebank/delete-practicebankanswer-image','PracticeBankID'=>$model->	PracticeBankID,'PracticeBankAnswerFile'=>$model->PracticeBankAnswerFile]),
						'key' => $model->PracticeBankID,
						'PracticeBankAnswerFile' => $model->PracticeBankAnswerFile,
						

					],

				],
               
	 ]
   ]);

?>

</div>
</div>

<div class="row">
<div class="col-sm-6">

    <?= $form->field($model, 'PracticeBankDescription')->textarea(['rows' => 6]) ?>
</div>
<div class="col-sm-6">

    
	<?= $form->field($model, 'PracticeBankStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>

</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../practicebank/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
