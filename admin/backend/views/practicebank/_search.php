<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PracticebankSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="practicebank-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'PracticeBankID') ?>

    <?= $form->field($model, 'moduleID') ?>

    <?= $form->field($model, 'PracticeBankNo') ?>

    <?= $form->field($model, 'PracticeBankName') ?>

    <?= $form->field($model, 'PracticeBankDescription') ?>

    <?php // echo $form->field($model, 'PracticeBankType') ?>

    <?php // echo $form->field($model, 'PracticeBankQuestionFile') ?>

    <?php // echo $form->field($model, 'PracticeBankAnswerFile') ?>

    <?php // echo $form->field($model, 'PracticeBankStatus') ?>

    <?php // echo $form->field($model, 'PracticeBankCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
