<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Practicebank */

$this->title = 'View Practice Bank: ' . $model->PracticeBankID;
$this->params['breadcrumbs'][] = ['label' => 'Practice Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="practicebank-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->PracticeBankID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PracticeBankID',
           [
				'label'=>'Module',            
				'attribute'=>'coursemodule.moduleName',
		   ],
            'PracticeBankNo',
            'PracticeBankName',
            'PracticeBankDescription',
            'PracticeBankType',
			
			[
				 'attribute'=> 'PracticeBankQuestionFile',
				 'value' => function($data) { 
				  if(!empty($data["PracticeBankQuestionFile"]))
				  {
					$exp = $data["PracticeBankQuestionFile"];
					$system=explode(".",$exp);
					if(preg_match("/jpg|jpeg|JPG|JPEG|PNG|png/",$system[count($system)-1]))
					{
						return "<img src='../../../web/uploads/practicebank/".$data["PracticeBankQuestionFile"]."' width=100 height=100 />";
						
					}
					else
					{
						return "<a href='../../../web/uploads/practicebank/".$data["PracticeBankQuestionFile"]."'>View</a>";
					}  
				  }
				  else
				  {
					  return "No File Uploaded";
				  }
						
						
				  
			 },'format' => 'html',
			 ],
			
			 [
				 'attribute'=> 'PracticeBankAnswerFile',
				 'value' => function($data) { 
				  if(!empty($data["PracticeBankAnswerFile"]))
				  {
					$exp = $data["PracticeBankAnswerFile"];
					$system=explode(".",$exp);
					if(preg_match("/jpg|jpeg|JPG|JPEG|PNG|png/",$system[count($system)-1]))
					{
						return "<img src='../../../web/uploads/practicebank/".$data["PracticeBankAnswerFile"]."' width=100 height=100 />";
						
					}
					else
					{
						return "<a href='../../../web/uploads/practicebank/".$data["PracticeBankAnswerFile"]."'>View</a>";
					}  
				  }
				  else
				  {
					  return "No File Uploaded";
				  }
						
						
				  
			 },'format' => 'html',
			 ],
			 
           // 'PracticeBankQuestionFile',
           // 'PracticeBankAnswerFile',
            'PracticeBankStatus',
            'PracticeBankCreatedDate',
        ],
    ]) ?>

</div>
