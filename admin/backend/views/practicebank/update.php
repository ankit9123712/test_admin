<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Practicebank */

$this->title = 'Update Practice Bank: ' . $model->PracticeBankID;
$this->params['breadcrumbs'][] = ['label' => 'Practice Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PracticeBankID, 'url' => ['view', 'id' => $model->PracticeBankID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="practicebank-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
