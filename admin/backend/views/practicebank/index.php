<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PracticebankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Practice Banks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="practicebank-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Practice Bank', ['create'], ['class' => 'btn btn-success']) ?>&nbsp; <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'practicebank','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'PracticeBankID',
		   'PracticeBankType',
             [
				'attribute'=>'moduleID',
				'label'=>'Module',
				'value'=>'coursemodule.moduleName'
		   ],
            'PracticeBankNo',
            'PracticeBankName',
           // 'PracticeBankDescription:ntext',
            // 'PracticeBankType',
            // 'PracticeBankQuestionFile',
            // 'PracticeBankAnswerFile',
             'PracticeBankStatus',
            // 'PracticeBankCreatedDate',
	            [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->PracticeBankStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->PracticeBankID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->PracticeBankID],['title' => 'Make Active']);
						}
					},
            ]
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
