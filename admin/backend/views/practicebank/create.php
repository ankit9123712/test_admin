<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Practicebank */

$this->title = 'Create Practice Bank';
$this->params['breadcrumbs'][] = ['label' => 'Practice Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="practicebank-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
