<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChapterfilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chapterfiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chapterfiles-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Chapterfiles', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'chapterfileID',
          //  'coursechapterID',
           // 'chapterfileFileType',
           // 'chapterfileFile',
            'chapterfileStatus',
	            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->chapterfileStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->chapterfileID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->chapterfileID],['title' => 'Make Active']);
						}
					},
            ]
			],
		
        ],
    ]); ?>
</div>
