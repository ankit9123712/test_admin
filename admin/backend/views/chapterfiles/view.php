<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model backend\models\Chapterfiles */

$this->title = 'View Chapterfiles: ' . $model->chapterfileID;
$this->params['breadcrumbs'][] = ['label' => 'Chapterfiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chapterfiles-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->chapterfileID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'chapterfileID',
            'coursechapterID',
            'chapterfileFileType',
            'chapterfileFile',
            'chapterfileStatus',
        ],
    ]) ?>

</div>
