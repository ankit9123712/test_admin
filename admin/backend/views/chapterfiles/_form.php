<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use backend\models\Coursechapters;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\Chapterfiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chapterfiles-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">
<?= $form->field($model, 'coursechapterID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Coursechapters::find()->where(['coursechapterStatus' => 'Active'])->all(),'coursechapterID','coursechapterName'),
                    'options' => ['placeholder' => 'Select Courses'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'chapterfileFileType')->dropDownList([ 'Video' => 'Video', 'Contents' => 'Contents', ], ['prompt' => '']) ?>
</div>
</div>
<div class="row">
<div class="col-sm-12">
	<?= $form->field($model, 'chapterfileFile')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],'pluginOptions' => [
                  'initialPreview'=>isset($model->chapterfileFile) ? "../../uploads/chapter/".$model->chapterfileFile : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>true,
                  'showPreview' => true,
                  'showCaption' => true,
                  'showRemove' => false,
                  'showUpload' => false,
				  'allowedFileExtensions' => ['mp4','pdf']
					]]);
					?>				
				
</div>
</div>

<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'chapterfileStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => '']) ?>
</div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
