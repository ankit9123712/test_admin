<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Chapterfiles */

$this->title = 'Create Chapterfiles';
$this->params['breadcrumbs'][] = ['label' => 'Chapterfiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chapterfiles-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
