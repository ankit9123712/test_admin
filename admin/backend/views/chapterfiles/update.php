<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Chapterfiles */

$this->title = 'Update Chapterfiles: ' . $model->chapterfileID;
$this->params['breadcrumbs'][] = ['label' => 'Chapterfiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->chapterfileID, 'url' => ['view', 'id' => $model->chapterfileID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="chapterfiles-update">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
