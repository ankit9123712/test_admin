<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Coursemodule */

$this->title = 'Create Module';
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coursemodule-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
