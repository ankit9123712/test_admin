<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;
/* @var $this yii\web\View */
/* @var $model backend\models\Coursemodule */

$this->title = 'View Module';
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coursemodule-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->moduleID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'moduleID',
            'moduleName',
            'moduleType',
            'moduleRemarks',
			[
			   'attribute' => 'moduleGuidelines',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->moduleGuidelines,
           ],
		   [
			   'attribute' => 'moduleMarking',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->moduleMarking,
           ],
		   [
			   'attribute' => 'moduleQuicktips',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->moduleQuicktips,
           ],
            'moduleStatus',
            'moduleCreatedDate',
        ],
    ]) ?>

</div>
