<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Coursemodule */

$this->title = 'Update Module: ' . $model->moduleID;
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->moduleID, 'url' => ['view', 'id' => $model->moduleID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coursemodule-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
