<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CoursemoduleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coursemodule-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'moduleID') ?>

    <?= $form->field($model, 'moduleName') ?>

    <?= $form->field($model, 'moduleType') ?>

    <?= $form->field($model, 'moduleRemarks') ?>

    <?= $form->field($model, 'moduleStatus') ?>

    <?php // echo $form->field($model, 'moduleCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
