<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Homemessage */

$this->title = 'Update Home Message: ' . $model->messageID;
$this->params['breadcrumbs'][] = ['label' => 'Homemessages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->messageID, 'url' => ['view', 'id' => $model->messageID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="homemessage-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
