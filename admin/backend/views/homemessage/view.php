<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model backend\models\Homemessage */

$this->title = 'View Home Messages: ' . $model->messageID;
$this->params['breadcrumbs'][] = ['label' => 'Home Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="homemessage-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->messageID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'messageID',
		   'title',
            'messageMsg',
            [
				'label'=>'Institute',
				'attribute'=>'institute.instituteName'
			],
			[
				'label'=>'Stream',
				'attribute'=>'stream.streamName'
			],
           // 'coursestdID',
			[
				'label'=>'Coursestd',
				'attribute'=>'coursestd.coursestdName'
			],
            //'instituteID',
            
            'startDate',
            'endDate',
			'messageStatus',
        ],
    ]) ?>

</div>
