<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\HomemessageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="homemessage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'messageID') ?>

    <?= $form->field($model, 'messageMsg') ?>

    <?= $form->field($model, 'messageStatus') ?>

    <?= $form->field($model, 'instituteID') ?>

    <?= $form->field($model, 'streamID') ?>

    <?php // echo $form->field($model, 'coursestdID') ?>

    <?php // echo $form->field($model, 'startDate') ?>

    <?php // echo $form->field($model, 'endDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
