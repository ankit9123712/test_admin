<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Institute;
use backend\models\Stream;
use backend\models\Coursestd;
use kartik\date\DatePicker;
use dosamigos\tinymce\TinyMce;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Homemessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="homemessage-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<?= $form->field($model, 'messageMsg')->widget(TinyMce::className(), [
        'options' => ['rows' => 10],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
   
</div>
</div>

<div class="row">
<div class="col-sm-4">

<?= $form->field($model, 'instituteID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Institute::find()->where(['instituteStatus' => 'Active'])->all(),'instituteID','instituteName'),
                    'options' => ['placeholder' => 'Select Institute'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>


<div class="col-sm-4">

<?= $form->field($model, 'streamID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Stream::find()->where(['streamStatus' => 'Active'])->all(),'streamID','streamName'),
                    'options' => ['placeholder' => 'Select Stream'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
				</div>
<div class="col-sm-4">

<?= $form->field($model, 'coursestdID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Coursestd::find()->where(['coursestdStatus' => 'Active'])->all(),'coursestdID','coursestdName'),
                    'options' => ['placeholder' => 'Select Coursestd'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
				</div>
				
				
<div class="row">				
<div class="col-sm-4">
<label>Start Date</label>
<?php	echo DatePicker::widget([
'name' => 'startDate', 
'value' => $model->isNewRecord ? date('Y-M-d') : $model->startDate,
'options' => ['placeholder' => 'Select Start Date ...'],
'pluginOptions' => [
'format' => 'yyyy-M-dd',	
'todayHighlight' => true
]
]);?>
</div>
<div class="col-sm-4">
<label>End Date</label>
<?php	echo DatePicker::widget([
'name' => 'endDate', 
'value' => $model->isNewRecord ? date('Y-M-d') : $model->endDate,
'options' => ['placeholder' => 'Select Start Date ...'],
'pluginOptions' => [
'format' => 'yyyy-M-dd',	
'todayHighlight' => true
]
]);?>
</div>
</div>
<br/>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'messageStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => '']) ?>
</div>
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
