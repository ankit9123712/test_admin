<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Homemessage */

$this->title = 'Create Home Message';
$this->params['breadcrumbs'][] = ['label' => 'Home Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="homemessage-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
