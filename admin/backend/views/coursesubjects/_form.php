<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;
use backend\models\Courses;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\file\FileInput;
use kartik\widgets\ColorPicker;


/* @var $this yii\web\View */
/* @var $model backend\models\coursesubjects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coursesubjects-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

   <?= $form->field($model, 'courseID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Courses::find()->where(['courseStatus' => 'Active'])->all(),'courseID','courseName'),
                    'options' => ['placeholder' => 'Select Courses'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
				</div>
<div class="col-sm-4">	
			
	<?= $form->field($model, 'coursesubjName')->textInput(['maxlength' => true]) ?>
  </div>
  <div class="col-sm-4">	
  
   <?= $form->field($model, 'coursesubjChapters')->textInput(['disabled' => true]) ?>

</div>
</div>
<div class="row">
<div class="col-sm-6">

<?= $form->field($model, 'coursesubjDescription')->widget(TinyMce::className(), [
        'options' => ['rows' => 10],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
	</div>
<div class="col-sm-6">	
	<?= $form->field($model, 'coursesubjIcon')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],'pluginOptions' => [
                  'initialPreview'=>isset($model->coursesubjIcon) ? "../../uploads/subject/".$model->coursesubjIcon : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>true,
                  'showPreview' => true,
                  'showCaption' => true,
                  'showRemove' => false,
                  'showUpload' => false
					]]);
					?>	
</div>
</div>
<div class="row">
<div class="col-sm-4">
<?= $form->field($model, 'coursesubjColor')->widget(alexantr\colorpicker\ColorPicker::className()) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'coursesubjStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
</div>
<div class="row">
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
