<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\coursesubjectsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coursesubjects-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'coursesubjID') ?>

    <?= $form->field($model, 'courseID') ?>

    <?= $form->field($model, 'coursesubjName') ?>

    <?= $form->field($model, 'coursesubjDescription') ?>

    <?= $form->field($model, 'coursesubjChapters') ?>

    <?php // echo $form->field($model, 'coursesubjStatus') ?>

    <?php // echo $form->field($model, 'coursesubjCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
