<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\coursesubjects */

$this->title = 'Create Course Subjects';
$this->params['breadcrumbs'][] = ['label' => 'Course Subjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coursesubjects-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
