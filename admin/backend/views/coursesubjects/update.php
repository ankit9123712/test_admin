<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\coursesubjects */

$this->title = 'Update Course Subjects: ' . $model->coursesubjID;
$this->params['breadcrumbs'][] = ['label' => 'Coursesubjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->coursesubjID, 'url' => ['view', 'id' => $model->coursesubjID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coursesubjects-update">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
