<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\coursesubjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Course Subjects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coursesubjects-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Course Subject', ['create'], ['class' => 'btn btn-success']) ?>&nbsp;&nbsp;<?= Html::a('Back To Course', ['courses/index'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'coursesubjects','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'coursesubjID',
			 [
			'attribute'=>'coursesubjName',
			'label'=>'Subject Name',
		   ],
		   
		    [
			'attribute'=>'coursesubjChapters',
			'label'=>'Chapters',
		   ],
           // 'courseID',
		  [
			'attribute'=>'courseID',
			'label'=>'Course',
			'value'=>'courses.courseName'
		   ],
           // 'coursesubjName',
			
            //'coursesubjDescription:ntext',
           // 'coursesubjChapters',
            // 'coursesubjStatus',
              [
			'attribute'=>'coursesubjStatus',
			'label'=>'Status',
		   ],
			// 'coursesubjCreatedDate',
	            [
                    'class' => 'yii\grid\ActionColumn',
					'header'=>'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}&nbsp;&nbsp;{addcoursechapters}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->coursesubjStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->coursesubjID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->coursesubjID],['title' => 'Make Active']);
						}
					},
					'addcoursechapters' => function ($url, $model, $key) 
					{			
						return  Html::a('<span><u>Add Chapter</u></span>', ['coursechapters/index', 'courseID' => $model->courseID],['title' => 'Add Chapter']);
					}
            ]
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
