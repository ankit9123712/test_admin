<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;
use backend\models\Coursechapters;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model backend\models\coursesubjects */

$this->title = 'View Course Subjects: ' . $model->coursesubjID;
$this->params['breadcrumbs'][] = ['label' => 'Course Subjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coursesubjects-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->coursesubjID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'coursesubjID',
            //'courseID',
			[
				'label'=>'Course',
				'attribute'=>'courses.courseName'
			],
            'coursesubjName',
            [
				'attribute' => 'coursesubjDescription',
				'format' => 'raw',
				'type' => 'widget',
				'widgetOptions' => ['class' => TinyMce::classname()],
				'value' => $model->coursesubjDescription,
			 ],
			 [                
                'attribute'=>'coursesubjIcon',
                'value'=>   ((($model->coursesubjIcon == NULL) || (($model['coursesubjIcon'] == ''))) ? "../../../web/images/noimage.png": '../../../web/uploads/subject'.'/'.$model->coursesubjIcon),
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'coursesubjChapters',
			'coursesubjColor',
            
            'coursesubjCreatedDate',
			'coursesubjStatus',
        ],
    ]) ?>
<h3>Course Chapters Detailes :</h3>
<?php
	/*------------------------------Start code coursechapters  display----------------------------------------*/
   $Coursechapters_Billing_dataProvider = new ActiveDataProvider([
         'query' => Coursechapters::find()->where('courseID = "'.$model->courseID.'"'),
         'pagination'=>false
		 
        ]);
		
     echo GridView::widget([
        'dataProvider' => $Coursechapters_Billing_dataProvider,
        'columns' => [
		//'coursechapterNo',
		
		'coursechapterName',
		'coursechapterFileType',
	//	'coursechapterFile',
		'coursechapterStatus',
	
		/*[
                    'class' => 'yii\grid\ActionColumn',
					'header'=>'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->coursechapterStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->coursechapterID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->coursechapterID],['title' => 'Make Active']);
						}
					},
            ]
			],*/
		 ],
    ]); 
/*------------------------------End code Useraddress Fields display----------------------------------------*/
     ?>	 
	 
	 

</div>
