<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Rewardpoints */

$this->title = 'Update Rewardpoints: ' . $model->userID;
$this->params['breadcrumbs'][] = ['label' => 'Rewardpoints', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userID, 'url' => ['view', 'id' => $model->userID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rewardpoints-update">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
