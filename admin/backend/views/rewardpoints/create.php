<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Rewardpoints */

$this->title = 'Create Rewardpoints';
$this->params['breadcrumbs'][] = ['label' => 'Rewardpoints', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rewardpoints-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
