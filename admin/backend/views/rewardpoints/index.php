<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\RewardpointsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reward Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rewardpoints-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

 
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'rewardpoints','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'userFullName',
            'userMobile',
            'userEmail:email',
            'ExamRewordPoints',
            'ReferralRewordPoints',
            // 'userID',
				[
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}&nbsp;&nbsp',                    
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
