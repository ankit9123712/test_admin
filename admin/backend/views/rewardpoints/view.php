<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model backend\models\Rewardpoints */

$this->title = 'View Reward Points: ' . $model->userID;
$this->params['breadcrumbs'][] = ['label' => 'Reward Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rewardpoints-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'userFullName',
            'userMobile',
            'userEmail:email',
            'ExamRewordPoints',
            'ReferralRewordPoints',
           // 'userID',
        ],
    ]) ?>

</div>
