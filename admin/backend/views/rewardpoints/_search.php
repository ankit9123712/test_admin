<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RewardpointsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rewardpoints-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'userFullName') ?>

    <?= $form->field($model, 'userMobile') ?>

    <?= $form->field($model, 'userEmail') ?>

    <?= $form->field($model, 'ExamRewordPoints') ?>

    <?= $form->field($model, 'ReferralRewordPoints') ?>

    <?php // echo $form->field($model, 'userID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
