<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Notification */

$this->title = 'View Notification: ' . $model->notificationID;
$this->params['breadcrumbs'][] = ['label' => 'Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-view">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

   <!-- <p>
        <?//= Html::a('Update', ['update', 'id' => $model->notificationID], ['class' => 'btn btn-primary']) ?>
           </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'notificationID',
            'notificationReferenceKey',
            [                
                'label'=>'users',
				'attribute'=>'users.userFullName',                
            ],
            'userDeviceType',
            'userDeviceID',
            'notificationType',
            'notificationTitle',
            'notificationMessageText',
            'notificationResponse',
            'notificationSendDate',
            'notificationSendTime',
            'notificationStatus',
            'notificationReadStatus',
            'notificationCreatedDate',
            'notificationData:ntext',
          //  'dealerID',
        ],
    ]) ?>

</div>
