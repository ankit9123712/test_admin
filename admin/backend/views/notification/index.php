<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<p>
	
		<?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
		
	</p>
    <!--<p>
        <?//= Html::a('Create Notification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'notification','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'notificationID',
           // 'notificationReferenceKey',
           // 'userID',
		   [
                'attribute'=>'userID',
                'label'=>'Users',
                'value'=>'users.userFullName'
            ],
		   'notificationType',
		    'notificationTitle',
            'userDeviceType',
           // 'userDeviceID',
            // 'notificationMessageText',
            // 'notificationResponse',
            // 'notificationSendDate',
            // 'notificationSendTime',
            // 'notificationStatus',
            // 'notificationReadStatus',
            // 'notificationCreatedDate',
            // 'notificationData:ntext',
            // 'dealerID',
	           /* [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{delete}&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->notificationReadStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->notificationID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->notificationID],['title' => 'Make Active']);
						}
					},
            ]
			],*/
			[
  'label' =>'Action', 
  'value' => function ($model) 
  { 
	
  return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->notificationID]).''.Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->notificationID], ['data' => ['confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),'method' => 'post',],]); 
  }  
  ,
  'format'=>'raw', 
],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
