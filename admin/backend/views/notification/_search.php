<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NotificationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notification-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'notificationID') ?>

    <?= $form->field($model, 'notificationReferenceKey') ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'userDeviceType') ?>

    <?= $form->field($model, 'userDeviceID') ?>

    <?php // echo $form->field($model, 'notificationType') ?>

    <?php // echo $form->field($model, 'notificationTitle') ?>

    <?php // echo $form->field($model, 'notificationMessageText') ?>

    <?php // echo $form->field($model, 'notificationResponse') ?>

    <?php // echo $form->field($model, 'notificationSendDate') ?>

    <?php // echo $form->field($model, 'notificationSendTime') ?>

    <?php // echo $form->field($model, 'notificationStatus') ?>

    <?php // echo $form->field($model, 'notificationReadStatus') ?>

    <?php // echo $form->field($model, 'notificationCreatedDate') ?>

    <?php // echo $form->field($model, 'notificationData') ?>

    <?php // echo $form->field($model, 'dealerID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
