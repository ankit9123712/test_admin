<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\models\Notification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notification-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'notificationReferenceKey')->textInput() ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'userID')->textInput() ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'userDeviceType')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'userDeviceID')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'notificationType')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'notificationTitle')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'notificationMessageText')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'notificationResponse')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'notificationSendDate')->textInput() ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'notificationSendTime')->textInput() ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'notificationStatus')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'notificationReadStatus')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'notificationCreatedDate')->textInput() ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'notificationData')->textarea(['rows' => 6]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'dealerID')->textInput() ?>
</div>
</div>
<div class="row">
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
