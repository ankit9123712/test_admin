<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Institute */

$this->title = 'Update Institute: ' . $model->instituteID;
$this->params['breadcrumbs'][] = ['label' => 'Institutes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->instituteID, 'url' => ['view', 'id' => $model->instituteID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="institute-update">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
