<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Country;
use backend\models\State;
use backend\models\City;
use yii\helpers\Url;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model app\models\Institute */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="institute-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'instituteName')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'instituteContactName')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'instituteEmailExtension')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'institutePhone')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">

<div class="col-sm-8">

    <?= $form->field($model, 'instituteAddress')->textarea(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'institutePincode')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

<?= $form->field($model, 'countryID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Country::find()->where(['countryStatus' => 'Active'])->all(),'countryID','countryName'),
                    'options' => ['placeholder' => 'Select country',
                                  'onchange'=>'         
                                        $.get( "'.Url::toRoute(['state/statelist']).'", { id: $(this).val() } )
                                            .done(function( data ) {
                                                $( "#'.Html::getInputId($model, 'stateID').'" ).html( data );
                                            }
                                        );'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?></div>
<div class="col-sm-4">

<?= $form->field($model, 'stateID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(State::find()->where(['stateStatus' => 'Active'])->all(),'stateID','stateName'),
                    'options' => ['placeholder' => 'Select State',
                                  'onchange'=>'         
                                        $.get( "'.Url::toRoute(['city/citylist']).'", { id: $(this).val() } )
                                            .done(function( data ) {
                                                $( "#'.Html::getInputId($model, 'cityID').'" ).html( data );
                                            }
                                        );'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?></div>
<div class="col-sm-4">

<?= $form->field($model, 'cityID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(City::find()->where(['cityStatus' => 'Active'])->all(),'cityID','cityName'),
                    'options' => ['placeholder' => 'Select City'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'instituteStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>

</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
