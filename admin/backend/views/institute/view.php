<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Institute */

$this->title = 'View Institute: ' . $model->instituteID;
$this->params['breadcrumbs'][] = ['label' => 'Institutes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institute-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->instituteID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'instituteID',
            'instituteName',
            'instituteContactName',
            'instituteEmailExtension:email',
            'institutePhone',
            'instituteAddress',
            'institutePincode',
             [
                 'label'=> 'Country',
                
                'attribute'=>'country.countryName'
            ],
            [
                 'label'=> 'State',
                
                'attribute'=>'state.stateName'
            ],
			[
                //'attribute'=>'cityID',
                'label'=> 'City',
                'attribute'=>'city.cityName'
            ],
           
            'instituteCreatedDate',
			 'instituteStatus',
        ],
    ]) ?>

</div>
