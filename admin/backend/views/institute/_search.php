<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\InstituteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="institute-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'instituteID') ?>

    <?= $form->field($model, 'instituteName') ?>

    <?= $form->field($model, 'instituteContactName') ?>

    <?= $form->field($model, 'instituteEmailExtension') ?>

    <?= $form->field($model, 'institutePhone') ?>

    <?php // echo $form->field($model, 'instituteAddress') ?>

    <?php // echo $form->field($model, 'institutePincode') ?>

    <?php // echo $form->field($model, 'countryID') ?>

    <?php // echo $form->field($model, 'stateID') ?>

    <?php // echo $form->field($model, 'cityID') ?>

    <?php // echo $form->field($model, 'instituteStatus') ?>

    <?php // echo $form->field($model, 'instituteCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
