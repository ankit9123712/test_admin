<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Fileslist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fileslist-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
	'action'=> 'upload',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="col-sm-12">
	<label>Please make sure you upload maximum 80 MB total files in one go</label>
	<?php
        echo FileInput::widget([
            'model' => $model,
            'attribute' => 'fileFile[]',
            'name' => 'fileFile[]',
            'options' => [
                'multiple' => true,
                //'accept' => ['mp3','mp4','pdf','png','jpg']
            ],
            'pluginOptions' => [
                'showCaption' => false,
                'showRemove' => false,
                'showUpload' => false,
                'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' =>  'Add Files (.mp3 , .pdf, .png, .jpg, .mp4)',
                'allowedFileExtensions' => ['jpg','gif','png','pdf','mp3','mp4'],
                'overwriteInitial' => false
            ],
        ]);
    ?>
    
</div>
</div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
