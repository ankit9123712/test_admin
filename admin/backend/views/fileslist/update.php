<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Fileslist */

$this->title = 'Update Fileslist: ' . $model->fileID;
$this->params['breadcrumbs'][] = ['label' => 'Fileslists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fileID, 'url' => ['view', 'id' => $model->fileID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fileslist-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
