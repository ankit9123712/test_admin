<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\FileslistSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Files Management';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fileslist-index">

     

    <p>
        <?= Html::a('Upload Files', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'exams','enablePushState' => false, 'enableReplaceState' => false, ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'fileID',
            'fileFile',
			'fileActualName',
			[
				'label'=>'URL',
				'format'=>'raw',
				'value'=>function($data)
				{	//print_r($data);die;
					return '<a href="../../uploads/filelist/'.$data["fileFile"].'" data-pjax="0" target="_blank">Link</a>';
				},
			],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

	<?php Pjax::end(); ?>
</div>
