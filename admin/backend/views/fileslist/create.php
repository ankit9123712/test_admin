<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Fileslist */

$this->title = 'Upload Files';
$this->params['breadcrumbs'][] = ['label' => 'Files Management', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fileslist-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
