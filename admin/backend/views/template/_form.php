<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;


/* @var $this yii\web\View */
/* @var $model backend\models\Template */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="template-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'templateName')->textInput(['maxlength' => true,'readonly' => !$model->isNewRecord]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'templateActionName')->textInput(['maxlength' => true,'readonly' => !$model->isNewRecord]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'templateConstantCode')->textInput(['maxlength' => true,'readonly' => !$model->isNewRecord]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'templateEmail')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'templateSMS')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'templatePush')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>
</div>

</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'templateSubject')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-12">

<?= $form->field($model, 'templateEmailText')->widget(TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<?= $form->field($model, 'templateSMSText')->textArea(['rows' => 3]) ?>

</div>
</div>
<div class="row">
<div class="col-sm-12">
<?= $form->field($model, 'templateNotificationText')->textArea(['rows' => 3]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'templateStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>

</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>  &nbsp;&nbsp; <button class="btn bcolor"><a href="../template/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
