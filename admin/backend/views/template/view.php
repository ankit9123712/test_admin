<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Template */

$this->title = 'View Template';
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="template-view">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->templateID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'templateID',
            'templateName',
            'templateActionName',
            'templatePush',
            'templateEmail',
            'templateSMS',
            'templateConstantCode',
            'templateSubject',
            'templateEmailText:ntext',
            'templateSMSText',
            'templateNotificationText',
            'templateStatus',
            'templateCreatedDate',
        ],
    ]) ?>

</div>
