<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TemplateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="template-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'templateID') ?>

    <?= $form->field($model, 'templateName') ?>

    <?= $form->field($model, 'templateActionName') ?>

    <?= $form->field($model, 'templatePush') ?>

    <?= $form->field($model, 'templateEmail') ?>

    <?php // echo $form->field($model, 'templateSMS') ?>

    <?php // echo $form->field($model, 'templateConstantCode') ?>

    <?php // echo $form->field($model, 'templateSubject') ?>

    <?php // echo $form->field($model, 'templateEmailText') ?>

    <?php // echo $form->field($model, 'templateSMSText') ?>

    <?php // echo $form->field($model, 'templateNotificationText') ?>

    <?php // echo $form->field($model, 'templateStatus') ?>

    <?php // echo $form->field($model, 'templateCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
