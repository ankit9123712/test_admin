<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<?php /*
<fieldset>
  <legend>Email Settings</legend>
<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'settingsEmailFrom')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingsEmailTo')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingsEmailGateway')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'settingEmailID')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingsEmailPass')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingsSSL')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'settingsTLS')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingEmailPort')->textInput() ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingSenderName')->textInput(['maxlength' => true]) ?>
</div>
</div>
</fieldset>
 */ ?>
 <?php /* 
<fieldset>
  <legend>Payment Gateway Settings</legend>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'settingPGMode')->dropDownList([ 'Sandbox' => 'Sandbox', 'Live' => 'Live', ], ['prompt' => '']) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingsPGSandboxUrl')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingPGSandboxCustomerKey')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'settingsPGSandboxCustomerAuth')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingsPGLiveUrl')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingPGLiveCustomerKey')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'settingPGLiveCustomerAuth')->textInput(['maxlength' => true]) ?>
</div>

</div>

</div>


<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'settingsSMSGateway')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingsSMSSenderName')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'settingsSMSUsername')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'settingsSMSPassword')->textInput(['maxlength' => true]) ?>
</div>
</div>
</fieldset>
*/ ?>
<fieldset>
<legend>Other Settings</legend>
<div class="row">
<div class="col-sm-3">
    <?= $form->field($model, 'settingsSalesNo')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">
    <?= $form->field($model, 'settingsSupportNo')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">
    <?= $form->field($model, 'settingsSupportEmail')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">
    <?= $form->field($model, 'settingsMasterOtp')->textInput(['maxlength' => true]) ?>
</div>


</div>
<div class="row">
<div class="col-sm-4">
    <?= $form->field($model, 'settingsGSTPer')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">
    <?= $form->field($model, 'settingsGSTNo')->textInput() ?>
</div>
<div class="col-sm-4">
    <?= $form->field($model, 'settingsLogDeleteDays')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-6">	
	<?= $form->field($model, 'settingsHomeAlert')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>
<div class="col-sm-6">	
<?= $form->field($model, 'settingsOtherMessage')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>
</div>
</fieldset>
<fieldset>


<!--<div class="col-sm-4">

    <?//= $form->field($model, 'settingsPaymentSuccessUrl')->textInput() ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?//= $form->field($model, 'settingsPaymentErrorUrl')->textInput() ?>
</div>

<div class="col-sm-4">

    <?//= $form->field($model, 'settingsGSTOnFabric')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?//= $form->field($model, 'settingsGSTOnStitchingservice')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?//= $form->field($model, 'settingsGSTOnReadymadeGarments')->textInput(['maxlength' => true]) ?>
</div>

<div class="col-sm-4">

    <?//= $form->field($model, 'settingsGSTOnOtherService')->textInput(['maxlength' => true]) ?>
</div>
</div>-->
<fieldset>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
