<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Settings */

$this->title = 'View Settings: ' . $model->settingsID;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-view">

   <!--<h1><?//= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->settingsID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'settingsID',
            'settingsEmailFrom:email',
            'settingsEmailTo:email',
            'settingsEmailGateway:email',
            'settingEmailID:email',
            'settingsEmailPass:email',
            'settingsSSL',
            'settingsTLS',
            'settingEmailPort:email',
            'settingSenderName',
            'settingsSMSURL:url',
            'settingSMSUser',
            'settingsSMSPassword',
            'settingsSMSPort',
            'settingsGAKey',
            'settingsGMapKey',
            'settinngsSalesNo',
            'settingsDefCountry',
            'settingsDefState',
            'settingsDefCity',
            'settingsUserResetPinLinkExpHr',
            'settingsLogDeleteDays',
            'settingsTollFree',
            'settingsOrderReturnTimeMinutes:datetime',
            'settingsMasterOtp',
            'settingsGSTNo',
        ],
    ]) ?>

</div>
