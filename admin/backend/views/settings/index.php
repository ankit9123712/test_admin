<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <!--<h1><?//= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <!-- <p>
        <?//= Html::a('Create Settings', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'settings','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'settingsID',
            'settingsEmailFrom:email',
            'settingsEmailTo:email',
            'settingsEmailGateway:email',
            'settingEmailID:email',
            // 'settingsEmailPass:email',
            // 'settingsSSL',
            // 'settingsTLS',
            // 'settingEmailPort:email',
            // 'settingSenderName',
            // 'settingsSMSURL:url',
            // 'settingSMSUser',
            // 'settingsSMSPassword',
            // 'settingsSMSPort',
            // 'settingsGAKey',
            // 'settingsGMapKey',
            // 'settinngsSalesNo',
            // 'settingsDefCountry',
            // 'settingsDefState',
            // 'settingsDefCity',
            // 'settingsUserResetPinLinkExpHr',
            // 'settingsLogDeleteDays',
            // 'settingsTollFree',
            // 'settingsOrderReturnTimeMinutes:datetime',
            // 'settingsMasterOtp',
            // 'settingsGSTNo',
				[
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',                    
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
