<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SettingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'settingsID') ?>

    <?= $form->field($model, 'settingsEmailFrom') ?>

    <?= $form->field($model, 'settingsEmailTo') ?>

    <?= $form->field($model, 'settingsEmailGateway') ?>

    <?= $form->field($model, 'settingEmailID') ?>

    <?php // echo $form->field($model, 'settingsEmailPass') ?>

    <?php // echo $form->field($model, 'settingsSSL') ?>

    <?php // echo $form->field($model, 'settingsTLS') ?>

    <?php // echo $form->field($model, 'settingEmailPort') ?>

    <?php // echo $form->field($model, 'settingSenderName') ?>

    <?php // echo $form->field($model, 'settingsSMSURL') ?>

    <?php // echo $form->field($model, 'settingSMSUser') ?>

    <?php // echo $form->field($model, 'settingsSMSPassword') ?>

    <?php // echo $form->field($model, 'settingsSMSPort') ?>

    <?php // echo $form->field($model, 'settingsGAKey') ?>

    <?php // echo $form->field($model, 'settingsGMapKey') ?>

    <?php // echo $form->field($model, 'settinngsSalesNo') ?>

    <?php // echo $form->field($model, 'settingsDefCountry') ?>

    <?php // echo $form->field($model, 'settingsDefState') ?>

    <?php // echo $form->field($model, 'settingsDefCity') ?>

    <?php // echo $form->field($model, 'settingsUserResetPinLinkExpHr') ?>

    <?php // echo $form->field($model, 'settingsLogDeleteDays') ?>

    <?php // echo $form->field($model, 'settingsTollFree') ?>

    <?php // echo $form->field($model, 'settingsOrderReturnTimeMinutes') ?>

    <?php // echo $form->field($model, 'settingsMasterOtp') ?>

    <?php // echo $form->field($model, 'settingsGSTNo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
