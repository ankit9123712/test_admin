<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\models\Cmspage */

$this->title = 'View Dynamic Web Page';
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Web Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cmspage-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->cmspageID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cmspageID',
            'cmspageConstantCode',
            'cmspageName',
			[
			   'attribute' => 'cmspageContents',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->cmspageContents,
           ],
           // 'cmspageContents',
            'cmspageIsSystemPage',
            'cmspageStatus',
            'cmspageCreatedDate',
        ],
    ]) ?>

</div>
