<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;


/* @var $this yii\web\View */
/* @var $model backend\models\Cmspage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cmspage-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">
<div class="col-sm-6">

    <?= $form->field($model, 'cmspageName')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-6">

    <?= $form->field($model, 'cmspageConstantCode')->textInput(['maxlength' => true,'readonly' => !$model->isNewRecord]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-12">

<?= $form->field($model, 'cmspageContents')->widget(TinyMce::className(), [
        'options' => ['rows' => 18],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>
</div>
<div class="row">
<div class="col-sm-6">

    <?= $form->field($model, 'cmspageIsSystemPage')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No'],['readonly' => !$model->isNewRecord]) ?>
</div>
<div class="col-sm-6">

    <?= $form->field($model, 'cmspageStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>

</div>
<div class="row">
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../cmspage/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
