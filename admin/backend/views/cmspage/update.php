<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Cmspage */

$this->title = 'Update Dynamic Web Page: ' . $model->cmspageID;
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Web Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cmspageID, 'url' => ['view', 'id' => $model->cmspageID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cmspage-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
