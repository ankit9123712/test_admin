<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CmspageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cmspage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cmspageID') ?>

    <?= $form->field($model, 'cmspageConstantCode') ?>

    <?= $form->field($model, 'cmspageName') ?>

    <?= $form->field($model, 'cmspageContents') ?>

    <?= $form->field($model, 'cmspageIsSystemPage') ?>

    <?php // echo $form->field($model, 'cmspageStatus') ?>

    <?php // echo $form->field($model, 'cmspageCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
