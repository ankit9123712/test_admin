<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Cmspage */

$this->title = 'Create Dynamic Web Pages';
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Web Page', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cmspage-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
