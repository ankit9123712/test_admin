<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ExamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Exams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exams-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Exam', ['create'], ['class' => 'btn btn-success']) ?>&nbsp; <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'exams','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'examID',
        /*[
			'attribute'=>'moduleID',
			'label'=>'Module',
			'value'=>'coursemodule.moduleName'
		],*/
            'examName',
           // 'examContains',
           // 'examSections',
          //   'examDuration',
            // 'queIDs',
             'examTotalQs',
            // 'examQualifyingMarks',
            // 'examCorrectAnswer',
            // 'examWrongAnswer',
            // 'examInstruction:ntext',
			[
				
				'label'=>'Questions Uploaded',
				
				'value' => function ($model) {
					return Html::a(
						  $model->getQuestioncount($model->examID),
						  ['questions/index', 'eid' => $model->examID],
						  [
							 'title' => 'View Questions',
						  ]
					  );
					//return $model->getQuestioncount($model->LessonID);
				},
				'format' => 'raw',
		   ],
             'examStatus',
	            [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}&nbsp;&nbsp;{upload}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->examStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->examID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->examID],['title' => 'Make Active']);
						}
					},
					'upload' => function ($url, $model, $key) 
					{
					  
						return  Html::a('<span class="glyphicon glyphicon-upload"></span>', ['import', 'id' => $model->examID],['title' => 'Upload Questions']);
						
					},
            ]
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
