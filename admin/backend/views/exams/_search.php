<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ExamsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="exams-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'examID') ?>

    <?= $form->field($model, 'moduleID') ?>

    <?= $form->field($model, 'examName') ?>

    <?= $form->field($model, 'examContains') ?>

    <?= $form->field($model, 'examSections') ?>

    <?php // echo $form->field($model, 'examDuration') ?>

    <?php // echo $form->field($model, 'queIDs') ?>

    <?php // echo $form->field($model, 'examTotalQs') ?>

    <?php // echo $form->field($model, 'examQualifyingMarks') ?>

    <?php // echo $form->field($model, 'examCorrectAnswer') ?>

    <?php // echo $form->field($model, 'examWrongAnswer') ?>

    <?php // echo $form->field($model, 'examInstruction') ?>

    <?php // echo $form->field($model, 'examStatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
