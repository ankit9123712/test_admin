<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Exams */

$this->title = 'Update Exam: ' . $model->examID;
$this->params['breadcrumbs'][] = ['label' => 'Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->examID, 'url' => ['view', 'id' => $model->examID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="exams-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
