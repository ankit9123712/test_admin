<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Exams */

$this->title = 'Create Exam';
$this->params['breadcrumbs'][] = ['label' => 'Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exams-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
