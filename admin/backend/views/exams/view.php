<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Questions;
use yii\helpers\ArrayHelper;
use dosamigos\tinymce\TinyMce;
/* @var $this yii\web\View */
/* @var $model backend\models\Exams */

$this->title = 'View Exam: ' . $model->examID;
$this->params['breadcrumbs'][] = ['label' => 'Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


 


?>
<div class="exams-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->examID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'examID',
            //'moduleID',
            'examName',
            //'examContains',
            //'examSections',
           // 'examDuration',
			
            'examTotalQs',
           // 'examQualifyingMarks',
           // 'examCorrectAnswer',
           // 'examWrongAnswer',
            //'examInstruction:ntext',
			[
			   'attribute' => 'examInstruction',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->examInstruction,
           ],
            'examStatus',
        ],
    ]) ?>

</div>
