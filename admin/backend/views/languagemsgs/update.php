<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Languagemsgs */

$this->title = 'Update Language Wise Message: ' . $model->langMsgID;
$this->params['breadcrumbs'][] = ['label' => 'Language Wise Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->langMsgID, 'url' => ['view', 'id' => $model->langMsgID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="languagemsgs-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
