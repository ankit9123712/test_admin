<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LanguagemsgsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="languagemsgs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'langMsgID') ?>

    <?= $form->field($model, 'languageID') ?>

    <?= $form->field($model, 'langMsgKey') ?>

    <?= $form->field($model, 'langMsgValue') ?>

    <?= $form->field($model, 'langMsgStatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
