<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\LanguagemsgsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Language Wise Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="languagemsgs-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'languagemsgs','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'langMsgID',
             'language.languageName',
            'langMsgKey',
            'langMsgValue',
            'langMsgStatus',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} ',                    
             ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
