<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Languagemsgs */

$this->title = 'View Language Wise Message: '.$model->langMsgID;
$this->params['breadcrumbs'][] = ['label' => 'Language Wise Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="languagemsgs-view">

    

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->langMsgID], ['class' => 'btn btn-primary']) ?>
       
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'langMsgID',
            [
				'attribute' =>  'language.languageName',
				'label' => 'Language Name',
			],
            'langMsgKey',
            'langMsgValue',
            'langMsgStatus',
        ],
    ]) ?>

</div>
