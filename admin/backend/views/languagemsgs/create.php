<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Languagemsgs */

$this->title = 'Create Language Wise Message';
$this->params['breadcrumbs'][] = ['label' => 'Language Wise Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="languagemsgs-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
