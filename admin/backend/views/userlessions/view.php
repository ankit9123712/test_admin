<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Userlessions */

$this->title = 'View Evaluate Lesson: ' . $model->userlessionID;
$this->params['breadcrumbs'][] = ['label' => 'Evaluate Lessons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userlessions-view">

    
	<div class="row">	
	<div class="col-sm-6">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'userlessionID',
             [
				'label'=>'Users',            
				'attribute'=>'users.userFullName',
		   ],
		    [
				'label'=>'Lesson',            
				'attribute'=>'lesson.LessonName',
		   ],
           /* 'userlessionQuestions',
            'userlessionAnswered',
            'userlessionCorrectAnswers',
            'userlessionWrongAnswers',*/
        ],
    ]) ?>
	</div>
	<div class="col-sm-6">
	 <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          /*  'userlessionID',
             [
				'label'=>'Users',            
				'attribute'=>'users.userFullName',
		   ],
		    [
				'label'=>'Lesson',            
				'attribute'=>'lesson.LessonName',
		   ],*/
            'userlessionQuestions',
            'userlessionAnswered',
            'userlessionCorrectAnswers',
            'userlessionWrongAnswers',
        ],
    ]) ?>
	</div>
	</div>
	
		<?php 
		$sql = " Select useranswers.*,questions.queQuestion,questions.queSolution,queType,facultyFullName 
			FROM 
			useranswers
			INNER JOIN questions on questions.queID = useranswers.queID
			LEFT JOIN faculty on faculty.facultyID = useranswers.facultyID			
			WHERE useranswers.lessionID	='".$model->lessionID	."' AND useranswers.userID = '".$model->userID."'
			Group by useranswers.queID ORDER BY questions.queID ASC";
			//echo $sql;
			 $connection = Yii::$app->getDb();
			$command = $connection->createCommand($sql);
			$result = $command->queryAll();
			if(count($result)>0)
			{
				for($i=0;count($result)>$i; $i++) 
				{ 
					
		?>	
					 	<div class="row" style="padding:10;margin:10" >	
						<div class="col-sm-12">
							<b>Question: </b><?=$result[$i]["queQuestion"]?>
						</div>
						<div class="col-sm-12">
						<b>Given Answer: </b><?php 
						 $fArr = explode(",",$result[$i]["answerAnswer"]);
						 $nofile=false;
						 if(count($fArr)>0)
						 {
							 for($j=0;count($fArr)>$j; $j++) 
							 {	
								$system=explode(".",$fArr[$j]);
								if(preg_match("/jpg|jpeg|JPG|JPEG|png|PNG/",$system[count($system)-1]))
								{
									echo "<img src='../../../web/uploads/users/".$fArr[$j]."' width=100 height=100/>";
								}
								else if(preg_match("/mp4|MP4/",$system[count($system)-1]))
								{
									echo "<video width=\"320\" height=\"240\" controls>
									  <source src='../../../web/uploads/users/".$fArr[$j]."' type=\"video/mp4\">
									</video>";
								}
								else if(preg_match("/mp3|MP3/",$system[count($system)-1]))
								{
									echo "<audio controls>
										<source src='../../../web/uploads/users/".$fArr[$j]."' type=\"audio/mpeg\">
										</audio>";
								}
								else if(preg_match("/m4a|M4A/",$system[count($system)-1]))
								{
									echo "<audio controls>
										<source src='../../../web/uploads/users/".$fArr[$j]."' type=\"audio/mp4\">
										</audio>";
								}
								else if(preg_match("/pdf|PDF/",$system[count($system)-1]))
								{
									echo "<a href='../../../web/uploads/users/".$fArr[$j]."'>View PDF</a>";
								}
								else
								{
										$nofile = true;
										break;
								}									
							 }
						 }
						 else
							echo $result[$i]["answerAnswer"];
						 if($nofile)
							 echo $result[$i]["answerAnswer"];
						?>
						</div>
						<div class="col-sm-12">
						<b>Solution: </b><?php if(empty($result[$i]["queSolution"])) { 
						echo $result[$i]["answerCorrectAnswer"];
						}
						else
						{
						  echo $result[$i]["queSolution"];		
						}
						?>
						</div>						
						</div>
						<div class="row">	
						<div class="col-sm-4">
							<b>Answer Verified: </b><?=$result[$i]["answerIsVerified"]?>
						</div>
						<div class="col-sm-4">
							<b>Answer Correct: </b><?=$result[$i]["answerIsCorrect"]?>
						</div>	
						<div class="col-sm-4">
							<b>Faculty: </b><?=$result[$i]["facultyFullName"]?>
						</div>			
						</div>
						<div class="row">
						<br><br>
						</div>
		<?php 
				}
			}
		?>	
	
</div>
