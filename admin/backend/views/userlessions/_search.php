<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserlessionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userlessions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'userlessionID') ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'lessionID') ?>

    <?= $form->field($model, 'userlessionQuestions') ?>

    <?= $form->field($model, 'userlessionAnswered') ?>

    <?php // echo $form->field($model, 'userlessionCorrectAnswers') ?>

    <?php // echo $form->field($model, 'userlessionWrongAnswers') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
