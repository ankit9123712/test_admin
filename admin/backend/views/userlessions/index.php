<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserlessionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Evaluate Lessons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userlessions-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'userlessions','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'userlessionID',
        [
			'attribute'=>'userID',
			'label'=>'Users',
			'value'=>'users.userFullName'
		],
		[
			'attribute'=>'lessionID',
			'label'=>'Lesson',
			'value'=>'lesson.LessonName'
		],
		[
			'attribute'=>'moduleID',
			'label'=>'Module',
			'value'=>'coursemodule.moduleName'
		],
		[
			'attribute'=>'userType',
			'label'=>'Type',
			'value'=>'users.userType'
		],
		[
			'attribute'=>'stateID',
			'label'=>'State',
			'value'=>'state.stateName'
		],
		[
			'attribute'=>'cityID',
			'label'=>'City',
			'value'=>'city.cityName'
		],
		'userlessionBand',
            'userlessionQuestions',
            'userlessionAnswered',
		[
				
				'label'=>'Verification',
				
				'value' => function ($model) {
					
					if(empty($model->userlessionEvaluationDate))
					{
						return "Pending";
					}
					else
						return "Verified";
					
					
				},
				'format' => 'raw',
		   ],  	
		   
            // 'userlessionCorrectAnswers',
            // 'userlessionWrongAnswers',
				[
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}',                    
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
