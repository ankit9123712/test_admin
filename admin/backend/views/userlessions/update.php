<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Userlessions */

$this->title = 'Update User Lession: ' . $model->userlessionID;
$this->params['breadcrumbs'][] = ['label' => 'User Lessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userlessionID, 'url' => ['view', 'id' => $model->userlessionID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="userlessions-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
