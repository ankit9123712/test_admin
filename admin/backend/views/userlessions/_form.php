<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Users;
use backend\models\Lesson;

/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Userlessions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userlessions-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

<?= $form->field($model, 'userID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Users::find()->where(['userStatus' => 'Active'])->all(),'userID','userFullName'),
                    'options' => ['placeholder' => 'Select Users'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

<?= $form->field($model, 'lessionID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Lesson::find()->where(['LessonStatus' => 'Active'])->all(),'LessonID','LessonName'),
                    'options' => ['placeholder' => 'Select Lesson'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

    <?= $form->field($model, 'userlessionQuestions')->textInput() ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'userlessionAnswered')->textInput() ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'userlessionCorrectAnswers')->textInput() ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'userlessionWrongAnswers')->textInput() ?>
</div>
</div>
<div class="row">
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
