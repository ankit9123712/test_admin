<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Userlessions */

$this->title = 'Create User Lession';
$this->params['breadcrumbs'][] = ['label' => 'User Lessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userlessions-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
