<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UseranswersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="useranswers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'answerID') ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'queID') ?>

    <?= $form->field($model, 'answerAnswer') ?>

    <?= $form->field($model, 'answerCorrectAnswer') ?>

    <?php // echo $form->field($model, 'answerIsCorrect') ?>

    <?php // echo $form->field($model, 'answerIsVerified') ?>

    <?php // echo $form->field($model, 'facultyID') ?>

    <?php // echo $form->field($model, 'answerSubmittedDate') ?>

    <?php // echo $form->field($model, 'answerFor') ?>

    <?php // echo $form->field($model, 'lessionID') ?>

    <?php // echo $form->field($model, 'examID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
