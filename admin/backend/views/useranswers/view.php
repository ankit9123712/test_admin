<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\models\Useranswers */

$this->title = 'View Evaluate Test: ' . $model->answerID;
$this->params['breadcrumbs'][] = ['label' => 'Evaluate Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="useranswers-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->answerID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'answerID',
			 [
				'label'=>'Users',            
				'attribute'=>'users.userFullName',
		   ],
		    [
				'label'=>'Questions',            
				'attribute'=>'questions.queQuestion',
		   ],
           // 'userID',
          //  'queID',
		  [
           'attribute' => 'answerAnswer',
           'format' => 'raw',
           'type' => 'widget',
           'widgetOptions' => ['class' => TinyMce::classname()],
           'value' => $model->answerAnswer,
        ],
		[
           'attribute' => 'answerCorrectAnswer',
           'format' => 'raw',
           'type' => 'widget',
           'widgetOptions' => ['class' => TinyMce::classname()],
           'value' => $model->answerCorrectAnswer,
        ],
            //'answerAnswer',
            //'answerCorrectAnswer',
            'answerIsCorrect',
            'answerIsVerified',
			 [
				'label'=>'Faculty',            
				'attribute'=>'faculty.facultyFullName',
		   ],
         //   'facultyID',
            'answerSubmittedDate',
            'answerFor',
			 [
				'label'=>'Lesson',            
				'attribute'=>'lesson.LessonName',
		   ],
		    [
				'label'=>'Exams',            
				'attribute'=>'exams.examName',
		   ],
           // 'lessionID',
          //  'examID',
        ],
    ]) ?>

</div>
