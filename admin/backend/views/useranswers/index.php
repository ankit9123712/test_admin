<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UseranswersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Evaluate Tests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="useranswers-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Evaluate Test', ['create'], ['class' => 'btn btn-success']) ?>&nbsp; <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'useranswers','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'answerID',
		[
			'attribute'=>'userID',
			'label'=>'Users',
			'value'=>'users.userFullName'
		],
		[
			'attribute'=>'queID',
			'label'=>'Questions',
			'value'=>'questions.queQuestion'
		],
           // 'userID',
          //  'queID',
          //  'answerAnswer',
           'answerCorrectAnswer',
            // 'answerIsCorrect',
             'answerIsVerified',
           //  'facultyID',
           // // 'answerSubmittedDate',
            /// 'answerFor',
             //'lessionID',
          //  'examID',
				[
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',                    
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
