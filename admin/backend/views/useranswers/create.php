<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Useranswers */

$this->title = 'Create Evaluate Test';
$this->params['breadcrumbs'][] = ['label' => 'Evaluate Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="useranswers-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
