<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Useranswers */

$this->title = 'Update Evaluate Test: ' . $model->answerID;
$this->params['breadcrumbs'][] = ['label' => 'Evaluate Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->answerID, 'url' => ['view', 'id' => $model->answerID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="useranswers-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
