<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Users;
use backend\models\Exams;
use backend\models\Lesson;
use backend\models\Faculty;
use backend\models\Questions;
use kartik\date\DatePicker;

/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Useranswers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="useranswers-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-6">

<?= $form->field($model, 'userID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Users::find()->where(['userStatus' => 'Active'])->all(),'userID','userFullName'),
                    'options' => ['placeholder' => 'Select Users'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
<div class="col-sm-6">

<?= $form->field($model, 'queID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Questions::find()->where(['queStatus' => 'Active'])->all(),'queID','queQuestion'),
                    'options' => ['placeholder' => 'Select Questions'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>

</div>
<div class="row">
<div class="col-sm-6">

<?= $form->field($model, 'answerAnswer')->widget(TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>
<div class="col-sm-6">

<?= $form->field($model, 'answerCorrectAnswer')->widget(TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>

</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'answerIsCorrect')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'answerIsVerified')->dropDownList([ 'Pending' => 'Pending', 'Verified' => 'Verified', ]) ?>
</div>
<div class="col-sm-4">
<?= $form->field($model, 'facultyID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Faculty::find()->where(['facultyStatus' => 'Active'])->all(),'facultyID','facultyFullName'),
                    'options' => ['placeholder' => 'Select Faculty'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
</div>
<div class="row">
<div class="col-sm-4">
<label>Start Date</label>
	<?= DatePicker::widget([
	'model' => $model,
    //'form'=>$objActiveForm,	
	'name' => 'answerSubmittedDate',
//date blanck	
	'value' => date('d M Y', strtotime($model->answerSubmittedDate)),
	'options' => ['placeholder' => 'Select Start date'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'startDate' => date("Y-m-d"),
		'todayHighlight' => true
	]
]);?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'answerFor')->dropDownList([ 'Lession' => 'Lession', 'Exams' => 'Exams', ]) ?>
</div>

<div class="col-sm-4">

<?= $form->field($model, 'lessionID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Lesson::find()->where(['LessonStatus' => 'Active'])->all(),'LessonID','LessonName'),
                    'options' => ['placeholder' => 'Select Lesson'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
<div class="col-sm-4">

<?= $form->field($model, 'examID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Exams::find()->where(['examStatus' => 'Active'])->all(),'examID','examName'),
                    'options' => ['placeholder' => 'Select Exams'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../useranswers/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
