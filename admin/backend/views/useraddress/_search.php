<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UseraddressSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="useraddress-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'addressID') ?>

    <?= $form->field($model, 'userID') ?>

    <?= $form->field($model, 'addressTitle') ?>

    <?= $form->field($model, 'addressContactDesgination') ?>

    <?= $form->field($model, 'addressAddressLine1') ?>

    <?php // echo $form->field($model, 'addressAddressLine2') ?>

    <?php // echo $form->field($model, 'cityID') ?>

    <?php // echo $form->field($model, 'stateID') ?>

    <?php // echo $form->field($model, 'countryID') ?>

    <?php // echo $form->field($model, 'addressPincode') ?>

    <?php // echo $form->field($model, 'addressGST') ?>

    <?php // echo $form->field($model, 'addressPAN') ?>

    <?php // echo $form->field($model, 'addressType') ?>

    <?php // echo $form->field($model, 'addressIsDefault') ?>

    <?php // echo $form->field($model, 'addressCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
