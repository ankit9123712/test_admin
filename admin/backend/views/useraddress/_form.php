<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Useraddress */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="useraddress-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo Html::errorSummary($model); ?>
</div>
</div>
<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'userID')->textInput() ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'addressTitle')->textInput(['maxlength' => true]) ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'addressContactDesgination')->textInput(['maxlength' => true]) ?>

</div>

</div>

<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'addressAddressLine1')->textInput(['maxlength' => true]) ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'addressAddressLine2')->textInput(['maxlength' => true]) ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'cityID')->textInput() ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'stateID')->textInput() ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'countryID')->textInput() ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'addressPincode')->textInput(['maxlength' => true]) ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'addressGST')->textInput(['maxlength' => true]) ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'addressPAN')->textInput(['maxlength' => true]) ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'addressType')->textInput(['maxlength' => true]) ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'addressIsDefault')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ]) ?>

</div>

<div class="col-sm-4">

    <?= $form->field($model, 'addressCreatedDate')->textInput() ?>

</div>

</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
