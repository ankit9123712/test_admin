<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Useraddress */

$this->title = 'Update Useraddress: ' . $model->addressID;
$this->params['breadcrumbs'][] = ['label' => 'Useraddresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->addressID, 'url' => ['view', 'id' => $model->addressID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="useraddress-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
