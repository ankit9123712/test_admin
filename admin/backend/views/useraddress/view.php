<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Useraddress */

$this->title = $model->addressID;
$this->params['breadcrumbs'][] = ['label' => 'Useraddresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="useraddress-view">

    

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->addressID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->addressID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'addressID',
            'userID',
            'addressTitle',
            'addressContactDesgination',
            'addressAddressLine1',
            'addressAddressLine2',
            'cityID',
            'stateID',
            'countryID',
            'addressPincode',
            'addressGST',
            'addressPAN',
            'addressType',
            'addressIsDefault',
            'addressCreatedDate',
        ],
    ]) ?>

</div>
