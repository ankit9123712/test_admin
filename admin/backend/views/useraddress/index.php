<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UseraddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Useraddresses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="useraddress-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Useraddress', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'useraddress','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]', 	
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'addressID',
            'userID',
            'addressTitle',
            'addressContactDesgination',
            'addressAddressLine1',
            // 'addressAddressLine2',
            // 'cityID',
            // 'stateID',
            // 'countryID',
            // 'addressPincode',
            // 'addressGST',
            // 'addressPAN',
            // 'addressType',
            // 'addressIsDefault',
            // 'addressCreatedDate',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',                    
             ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
