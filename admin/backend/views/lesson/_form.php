<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Coursemodule;
use backend\models\Faculty;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Lesson */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lesson-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">
<div class="col-sm-3">
<?php

$Lesson_Option = array('Free' => 'Free', 'Premium' => 'Premium');
echo $form->field($model, 'LessonType')->widget(Select2::classname(), [
                    'data' => $Lesson_Option,
                    'options' => ['placeholder' => 'Select Bank Type',
					'onchange'=>'         
										$.get( "'.Url::toRoute(['coursemodule/coursemodulelist']).'", { type: $(this).val() } )
											.done(function( data ) {
												$( "#'.Html::getInputId($model, 'moduleID').'" ).html( data );
											}
										  );'],
                      'pluginOptions' => [
                      'allowClear' => true
     ],
  ]);
 ?>
</div>
<div class="col-sm-3">

<?= $form->field($model, 'moduleID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Coursemodule::find()->where(['moduleStatus' => 'Active'])->all(),'moduleID','moduleName'),
                    'options' => ['placeholder' => 'Select Module'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
 
<div class="col-sm-3">

    <?= $form->field($model, 'LessonNo')->textInput() ?>
</div>
<div class="col-sm-3">

    <?= $form->field($model, 'LessonName')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

<?= $form->field($model, 'LessonDescription')->widget(TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'en_CA',
		
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link preview anchor",
                "visualblocks code fullscreen",
                "contextmenu paste"
            ],
			'menubar'=> false,
            'toolbar' => "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>
<div class="col-sm-4">
<?= $form->field($model, 'lessionMarkingSystem')->widget(TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link preview anchor",
                "visualblocks code fullscreen",
                "contextmenu paste"
            ],
			'menubar'=> false,
            'toolbar' => "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>
<div class="col-sm-4">
<?= $form->field($model, 'lessionQuickTips')->widget(TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link preview anchor",
                "visualblocks code fullscreen",
                "contextmenu paste"
            ],
			'menubar'=> false,
            'toolbar' => "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>

</div>
<div class="row">
<div class="col-sm-6">

<?php

	echo $form->field($model, 'LessonFile')->widget(FileInput::classname(), [
                   
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['jpg','png','jpeg','pdf','mp4','mp3'],
				  'showPreview' => true,
				  'showCaption' => true,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,
                  'initialPreview'=>!empty($model->LessonFile) ? "../../uploads/lesson/".$model->LessonFile : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>false,
				   'initialPreviewConfig' => [
					[
						'caption' => $model->LessonFile,
						//'width' => "120px",
						'url' => Url::toRoute(['lesson/delete-lesson-image','LessonID'=>$model->LessonID,'LessonFile'=>$model->LessonFile]),
						'key' => $model->LessonID,
						'LessonFile' => $model->LessonFile,
						

					],

				],
               
	 ]
   ]);


					?>
</div>
 

<div class="col-sm-3">
	
	<?= $form->field($model, 'gradingfacultyIDs')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Faculty::find()->where("facultyStatus='Active' AND (facultyType='Grading' OR facultyType='Both' )" )->all(),'facultyID','facultyFullName'),
                    'options' => ['placeholder' => 'Select Grading Faculty','multiple' => true],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
	
	<br>
    <?= $form->field($model, 'LessonFileType')->dropDownList([ 'Video' => 'Video', 'Content' => 'Content', 'Audio' => 'Audio', ], ['prompt' => '']) ?>
	<br>
	<?= $form->field($model, 'LessonStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
 <div class="col-sm-3">
	<?= $form->field($model, 'teachingfacultyIDs')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Faculty::find()->where("facultyStatus='Active' AND (facultyType='Teaching' OR facultyType='Both' )" )->all(),'facultyID','facultyFullName'),
                    'options' => ['placeholder' => 'Select Teching Faculty','multiple' => true],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
	<br>			
    <?= $form->field($model, 'LessonFileURL')->textInput(['maxlength' => true]) ?>


    
</div>
</div>
<div class="row">

<div class="col-sm-4">

    <?php //= $form->field($model, 'VideoFileCount')->textInput() ?>
</div>
<div class="col-sm-4">

    <?php //= $form->field($model, 'ContentFileCount')->textInput() ?>
</div>

</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../lesson/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
