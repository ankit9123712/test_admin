<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\models\Lesson */

$this->title = 'View Lesson';
$this->params['breadcrumbs'][] = ['label' => 'Lessons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->LessonID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'LessonID',
           [
				'label'=>'Module',            
				'attribute'=>'coursemodule.moduleName',
		   ],
            'LessonNo',
            'LessonName',
			[
			   'attribute' => 'LessonDescription',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->LessonDescription,
           ],
		   [
			   'attribute' => 'lessionMarkingSystem',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->lessionMarkingSystem,
           ],
		   [
			   'attribute' => 'lessionQuickTips',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->lessionQuickTips,
           ],
           // 'LessonDescription',
           // 'lessionMarkingSystem',
          //  'lessionQuickTips',
            'LessonType',
			
			[
				 'attribute'=> 'LessonFile',
				 'value' => function($data) { 
				  if(!empty($data["LessonFile"]))
				  {
					$exp = $data["LessonFile"];
					$system=explode(".",$exp);
					if(preg_match("/jpg|jpeg|JPG|JPEG|PNG|png/",$system[count($system)-1]))
					{
						return "<img src='../../../web/uploads/lesson/".$data["LessonFile"]."' width=100 height=100 />";
						
					}
					else
					{
						return "<a href='../../../web/uploads/lesson/".$data["LessonFile"]."'>View</a>";
					}  
				  }
				  else
				  {
					  return "No File Uploaded";
				  }
						
						
				  
			 },'format' => 'html',
			 ],
			[
			    'format' => 'raw',
                'attribute' => 'teachingfacultyIDs',
                 'value' => $model->teachingfaculty,
			],
			[
			    'format' => 'raw',
                'attribute' => 'gradingfacultyIDs',
                 'value' => $model->gradingfaculty,
			],
			// 'gradingfacultyIDs',
			//'teachingfacultyIDs',
            'LessonFileType',
           
            'LessonFileURL',
            //'VideoFileCount',
            //'ContentFileCount',
            'LessonStatus',
            'LessonCreatedDate',
        ],
    ]) ?>

</div>
