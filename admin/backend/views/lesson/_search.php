<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LessonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lesson-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'LessonID') ?>

    <?= $form->field($model, 'moduleID') ?>

    <?= $form->field($model, 'LessonNo') ?>

    <?= $form->field($model, 'LessonName') ?>

    <?= $form->field($model, 'LessonDescription') ?>

    <?php // echo $form->field($model, 'lessionMarkingSystem') ?>

    <?php // echo $form->field($model, 'lessionQuickTips') ?>

    <?php // echo $form->field($model, 'LessonType') ?>

    <?php // echo $form->field($model, 'LessonFileType') ?>

    <?php // echo $form->field($model, 'LessonFile') ?>

    <?php // echo $form->field($model, 'LessonFileURL') ?>

    <?php // echo $form->field($model, 'VideoFileCount') ?>

    <?php // echo $form->field($model, 'ContentFileCount') ?>

    <?php // echo $form->field($model, 'LessonStatus') ?>

    <?php // echo $form->field($model, 'LessonCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
