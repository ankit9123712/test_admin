<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Lesson */

$this->title = 'Import Lesson Questions';
$this->params['breadcrumbs'][] = ['label' => 'Lessons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-create">

    <?= $this->render('_import', [
        'model' => $model,
    ]) ?>

</div>
