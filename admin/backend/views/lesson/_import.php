<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Lesson;

/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Lesson */
/* @var $form yii\widgets\ActiveForm */
$instructions="";
$id=$_REQUEST["id"];
if(!empty($id))
{
	//echo $id;
	$res=Lesson::find()->select('*')->where('LessonID='.$id)->asArray()->one();
	//print_r($res);die;
	if(!empty($res))
	{
		if($res["moduleID"] == 6 || $res["moduleID"] == 8)
		{
			$instructions = "<h3>File Uploads Instructions for the Reading Module</h3>
			<h5>Please make sure the column name. <strong>Question type</strong> can be either Single Text, MCQ, OR MMCQ. (Case Sensitive)<br />In the case of a Single Text, the correct answer needs to put in the Solution column.<br />In the case of MCQ correct Option's heading needs to put in the Correct Answer column.<br />In the case of MMCQ correct Option(s), the heading needs to put a comma separated in the Correct Answer column.</h5>
			<p><br />Find Sample file here to download&nbsp;<a href='../../uploads/samplefiles/readinglession.xlsx'>Sample Excel</a></p>";
		}
		else if($res["moduleID"] == 7 || $res["moduleID"] == 9)
		{
			$instructions = "<h3>Writing File Uploads Instructions</h3><h5>Please make sure the column name. <strong>Question type</strong> must be Multiline Text<BR></h5><br>Find Sample file here to download&nbsp;<a href='../..//uploads/samplefiles/writinglession.xlsx'>Sample Excel</a>";
		}
		else if($res["moduleID"] == 10 || $res["moduleID"] == 11)			
		{
			$instructions = "<h3>Listening File Uploads Instructions</h3><h5>Please make sure the column name. <strong>Question type</strong> can be either Single Text, MCQ, OR MMCQ. (Case Sensitive)<br />In the case of a Single Text, the correct answer needs to put in the Solution column.<br />In the case of MCQ correct Option's heading needs to put in the Correct Answer column.<br />In the case of MMCQ correct Option(s), the heading needs to put a comma separated in the Correct Answer column.</h5>
			<p><br />Find Sample file here to download <a href='../../uploads/samplefiles/listeninglession.xlsx'>Sample Excel</a>";
		}
		else if($res["moduleID"] == 12 || $res["moduleID"] == 13)	
		{
			$instructions = "<h3>Speaking File Uploads Instructions</h3><h5>Please make sure the column name. <strong>Question type</strong> must be Audio Text<BR></h5>Find Sample file here to download <a href='../../uploads/samplefiles/speakinglession.xlsx'>Sample Excel</a>";
		}
	}
}



?>

<div class="lesson-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data'], 'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
	'action'=> 'upload',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">
<div class="col-sm-12">
<h3><?php echo $res["LessonName"]?></h3>
<?php echo $instructions."<BR><BR>"; ?>
</div>
</div>
<div class="row">
<div class="col-sm-6">

<?php

	echo $form->field($model, 'LessonFile')->widget(FileInput::classname(), [
                  
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['xlsx','xls'],
				  'showPreview' => true,
				  'showCaption' => false,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,

				]
               
	 
   ])->label('Upload Question Excel ');


					?>
</div>
</div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Upload' : 'Upload', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; 
    </div>

    <?php ActiveForm::end(); ?>

</div>
