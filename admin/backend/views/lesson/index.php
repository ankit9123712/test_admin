<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\LessonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lessons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Lesson', ['create'], ['class' => 'btn btn-success']) ?>&nbsp; <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'lesson','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'LessonID',
           [
				'attribute'=>'moduleID',
				'label'=>'Module',
				'value'=>'coursemodule.moduleName'
		   ],
            'LessonNo',
            'LessonName',
			[
			'attribute'=>'LessonCreatedDate',
			'label'=>'Uploaded Date'
		],
          //  'LessonDescription:ntext',
            // 'lessionMarkingSystem:ntext',
            // 'lessionQuickTips:ntext',
            // 'LessonType',
            // 'LessonFileType',
            // 'LessonFile',
            // 'LessonFileURL:url',
            // 'VideoFileCount',
            // 'ContentFileCount',
			[
				
				'label'=>'Questions Uploaded',
				
				'value' => function ($model) {
					return Html::a(
						  $model->getQuestioncount($model->LessonID),
						  ['questions/index', 'Lid' => $model->LessonID],
						  [
							 'title' => 'View Questions',
						  ]
					  );
					//return $model->getQuestioncount($model->LessonID);
				},
				'format' => 'raw',
		   ],
             'LessonStatus',
            // 'LessonCreatedDate',
	            [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}&nbsp;&nbsp;{upload}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->LessonStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->LessonID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->LessonID],['title' => 'Make Active']);
						}
					},
					
					'upload' => function ($url, $model, $key) 
					{
					  
						return  Html::a('<span class="glyphicon glyphicon-upload"></span>', ['import', 'id' => $model->LessonID],['title' => 'Upload Questions']);
						
					},
					]
            
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
