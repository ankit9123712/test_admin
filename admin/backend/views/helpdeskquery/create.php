<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Helpdeskquery */

$this->title = 'Create Help Desk';
$this->params['breadcrumbs'][] = ['label' => 'Help Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="helpdeskquery-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
