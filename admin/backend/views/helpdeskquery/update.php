<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Helpdeskquery */

$this->title = 'Update Help Desk: ' . $model->helpdeskqueryID;
$this->params['breadcrumbs'][] = ['label' => 'Help Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->helpdeskqueryID, 'url' => ['view', 'id' => $model->helpdeskqueryID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="helpdeskquery-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
