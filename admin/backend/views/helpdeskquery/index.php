<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\HelpdeskquerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Help Desks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="helpdeskquery-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        &nbsp; <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'helpdeskquery','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           //'helpdeskqueryID',
            [
				'attribute'=>'userID',
				'label'=>'Users',
				'value'=>'users.userFullName'
		    ],
            'heldeskqueryQuery',
            'helpdeskqueryStatus',
          //  'helpdeskqueryCreatedDate',
	            [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->helpdeskqueryStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->helpdeskqueryID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->helpdeskqueryID],['title' => 'Make Active']);
						}
					},
            ]
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
