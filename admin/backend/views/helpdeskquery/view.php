<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Helpdeskquery */

$this->title = 'View Help Desk';
$this->params['breadcrumbs'][] = ['label' => 'Help Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="helpdeskquery-view">

     
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'helpdeskqueryID',
            [
				'label'=>'Users',            
				'attribute'=>'users.userFullName',
		   ],
            'heldeskqueryQuery',
            'helpdeskqueryStatus',
            'helpdeskqueryCreatedDate',
        ],
    ]) ?>

</div>
