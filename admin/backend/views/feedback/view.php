<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Feedback */

$this->title = 'View Feedback';
$this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-view">

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'feedbackID',
            'feedbackName',
            'feedbackEmail',
            'feedbackFeedback',
            [
				'label'=>'User',            
				'attribute'=>'users.userFullName',
		   ],
            'feedbackDate',
        ],
    ]) ?>

</div>
