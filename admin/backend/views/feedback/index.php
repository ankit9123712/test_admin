<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feedbacks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Export Excel', ['export'], ['class' => 'btn btn-info']) ?>
    </p>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'feedback','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'feedbackID',
		    [
				'attribute'=>'userID',
				'label'=>'User',
				'value'=>'users.userFullName'
		    ],
            'feedbackName',
            'feedbackEmail',
            //'feedbackFeedback',
            
            // 'feedbackDate',
				[
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;',                    
			],
		
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
