<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FeedbackSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'feedbackID') ?>

    <?= $form->field($model, 'feedbackName') ?>

    <?= $form->field($model, 'feedbackEmail') ?>

    <?= $form->field($model, 'feedbackFeedback') ?>

    <?= $form->field($model, 'userID') ?>

    <?php // echo $form->field($model, 'feedbackDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
