<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MonkcategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monkcategory-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'monkcatID') ?>

    <?= $form->field($model, 'monkcatName') ?>

    <?= $form->field($model, 'monkcatStatus') ?>

    <?= $form->field($model, 'monkcatCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
