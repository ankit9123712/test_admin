<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\models\Monkcategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monkcategory-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-5">

    <?= $form->field($model, 'monkcatName')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-5">

    <?= $form->field($model, 'monkcatStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>

</div>
<div class="row">
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../monkcategory/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
