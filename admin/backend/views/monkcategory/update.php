<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Monkcategory */

$this->title = 'Update Category: ' . $model->monkcatID;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->monkcatID, 'url' => ['view', 'id' => $model->monkcatID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="monkcategory-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
