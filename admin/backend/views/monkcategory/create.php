<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Monkcategory */

$this->title = 'Create Category';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monkcategory-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
