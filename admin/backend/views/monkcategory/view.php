<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Monkcategory */

$this->title = 'View Category: ' . $model->monkcatID;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monkcategory-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->monkcatID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'monkcatID',
            'monkcatName',
            'monkcatStatus',
            'monkcatCreatedDate',
        ],
    ]) ?>

</div>
