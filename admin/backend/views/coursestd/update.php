<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Coursestd */

$this->title = 'Update Standard: ' . $model->coursestdID;
$this->params['breadcrumbs'][] = ['label' => 'Standards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->coursestdID, 'url' => ['view', 'id' => $model->coursestdID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coursestd-update">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
