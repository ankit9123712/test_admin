<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Stream;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model app\models\Coursestd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coursestd-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

<?= $form->field($model, 'streamID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Stream::find()->where(['streamStatus' => 'Active'])->all(),'streamID','streamName'),
                    'options' => ['placeholder' => 'Select Stream'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

    <?= $form->field($model, 'coursestdName')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'coursestdRemarks')->textArea(['maxlength' => true,'rows'=>3]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'coursestdStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>

</div>
<div class="row">
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
