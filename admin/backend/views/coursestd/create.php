<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Coursestd */

$this->title = 'Create Standard';
$this->params['breadcrumbs'][] = ['label' => 'Standards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coursestd-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
