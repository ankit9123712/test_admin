<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Coursestd */

$this->title = 'View Standard: ' . $model->coursestdID;
$this->params['breadcrumbs'][] = ['label' => 'Standards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coursestd-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->coursestdID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'coursestdID',
            
			[
				'label'=>'Stream',
				'attribute'=>'stream.streamName'
			],
            'coursestdName',
			
           // 'coursestdDuration',
            'coursestdRemarks',
            
            'coursestdCreatedDate',
			'coursestdStatus',
        ],
    ]) ?>

</div>
