<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CoursestdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coursestd-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'coursestdID') ?>

    <?= $form->field($model, 'streamID') ?>

    <?= $form->field($model, 'coursestdName') ?>

    <?= $form->field($model, 'coursestdDuration') ?>

    <?= $form->field($model, 'coursestdRemarks') ?>

    <?php // echo $form->field($model, 'coursestdStatus') ?>

    <?php // echo $form->field($model, 'coursestdCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
