<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Coursemodule;
use dosamigos\tinymce\TinyMce;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Subscriptionplan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscriptionplan-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-3">
<?= $form->field($model, 'planName')->textInput(['maxlength' => true]) ?>
 <?= $form->field($model, 'planPrice')->textInput(['maxlength' => true]) ?>
 <?= $form->field($model, 'planStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
<div class="col-sm-3">
<?= $form->field($model, 'planDuration')->textInput() ?>
 <?= $form->field($model, 'planDiscountPrice')->textInput(['maxlength' => true]) ?>   

    	
</div>
<div class="col-sm-6">

    
	<?= $form->field($model, 'planDescription')->widget(TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'en_CA',
		
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link preview anchor",
                "visualblocks code fullscreen",
                "contextmenu paste"
            ],
			'menubar'=> false,
            'toolbar' => "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
</div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../subscriptionplan/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
