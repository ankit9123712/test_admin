<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Subscriptionplan */

$this->title = 'Update Subscription Plan: ' . $model->planID;
$this->params['breadcrumbs'][] = ['label' => 'Subscription Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->planID, 'url' => ['view', 'id' => $model->planID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subscriptionplan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
