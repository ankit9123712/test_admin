<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;
/* @var $this yii\web\View */
/* @var $model backend\models\Subscriptionplan */

$this->title = 'View Subscription Plan: ' . $model->planID;
$this->params['breadcrumbs'][] = ['label' => 'Subscriptionplans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriptionplan-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->planID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'planID',
           /* [
				'label'=>'Module',            
				'attribute'=>'coursemodule.moduleName',
		   ],*/
            'planName',
            [
			   'attribute' => 'planDescription',
			   'format' => 'raw',
			   'type' => 'widget',
			   'widgetOptions' => ['class' => TinyMce::classname()],
			   'value' => $model->planDescription,
           ],
            'planPrice',
            'planDiscountPrice',
            'planDuration',
            'planStatus',
            'planCreatedDate',
        ],
    ]) ?>

</div>
