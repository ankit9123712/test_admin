<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Subscriptionplan */

$this->title = 'Create Subscription Plan';
$this->params['breadcrumbs'][] = ['label' => 'Subscription Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriptionplan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
