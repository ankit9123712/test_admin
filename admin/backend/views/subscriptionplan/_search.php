<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SubscriptionplanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscriptionplan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'planID') ?>

    <?= $form->field($model, 'moduleID') ?>

    <?= $form->field($model, 'planName') ?>

    <?= $form->field($model, 'planDescription') ?>

    <?= $form->field($model, 'planPrice') ?>

    <?php // echo $form->field($model, 'planDiscountPrice') ?>

    <?php // echo $form->field($model, 'planDuration') ?>

    <?php // echo $form->field($model, 'planStatus') ?>

    <?php // echo $form->field($model, 'planCreatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
