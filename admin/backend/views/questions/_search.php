<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\QuestionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'queID') ?>

    <?= $form->field($model, 'moduleID') ?>

    <?= $form->field($model, 'lessionID') ?>

    <?= $form->field($model, 'examID') ?>

    <?= $form->field($model, 'queFor') ?>

    <?php // echo $form->field($model, 'queType') ?>

    <?php // echo $form->field($model, 'queDifficultyevel') ?>

    <?php // echo $form->field($model, 'queQuestion') ?>

    <?php // echo $form->field($model, 'queSolution') ?>

    <?php // echo $form->field($model, 'queDisplayType') ?>

    <?php // echo $form->field($model, 'queStatus') ?>

    <?php // echo $form->field($model, 'queOption1') ?>

    <?php // echo $form->field($model, 'queOption2') ?>

    <?php // echo $form->field($model, 'queOption3') ?>

    <?php // echo $form->field($model, 'queOption4') ?>

    <?php // echo $form->field($model, 'queCorrectAns') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
