<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \nterms\pagesize;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$readingerrorCount=0;
if(!empty($_REQUEST["Lid"]))
{
$this->title = 'View Lesson Questions';
$this->params['breadcrumbs'][] = ['label' => 'Lessons', 'url' => ['lesson/index']];
$this->params['breadcrumbs'][] = $this->title;	
}
else if(!empty($_REQUEST["eid"]))
{
$this->title = 'View Exam Questions';
$this->params['breadcrumbs'][] = ['label' => 'Exams', 'url' => ['exams/index']];
$this->params['breadcrumbs'][] = $this->title;	
}
else
{
	$this->title = 'Questions';
	$this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="questions-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]);
	if(empty($_REQUEST["Lid"]) && empty($_REQUEST["eid"]))
{ ?>

     
<?php } else { 
	if(!empty($_REQUEST["Lid"]) )
	{
		echo "<div class=\"\"><div class=\"col-sm-12\">";
		$errorCnt = @$_SESSION["errorCount"];
		$successCnt=@$_SESSION["successCount"];
		$errorArray=@$_SESSION["errorData"];
		$totalCnt= $successCnt+$errorCnt;
		//echo $successCnt;
		//echo $errorCnt;
		if(!empty($successCnt) || !empty($errorCnt))
		{
			echo "<div ><div class=\"col-sm-12\"><h4>Total Questions in file are:".$totalCnt."<br><font color='green'>successfully imported questions are:".$successCnt."</font><br><font color='red'>errors in: ".$errorCnt."</font></h4></div></div>";
		}
		if(!empty($errorCnt) && count($errorArray)>0)
		{
			$str="<div ><div class=\"col-sm-8\"><b>Question</b></div><div class=\"col-sm-4\"><b>Reason</b></div></div>";
			for($i=0;count($errorArray)>$i; $i++) 
			{   
				
				$str.="<div ><div class=\"col-sm-8\">".$errorArray[$i]["Question"]."</div><div class=\"col-sm-4\">".$errorArray[$i]["Reason"]."</div></div>";
				 
			}
			echo $str;
			 
			$_SESSION["errorCount"]=0;
			$_SESSION["successCount"]=0;
			$_SESSION["errorData"]= array();
		}
		echo "</div></div><br><br>";
		echo "<div ><div class=\"col-sm-8\">&nbsp;</div><div class=\"col-sm-4\">".
		 \yii\helpers\Html::a( 'Re-upload', Yii::$app->request->referrer,['class' => 'btn btn-warning'])."&nbsp;&nbsp;".\yii\helpers\Html::a( 'Confirm and Proceed', Url::toRoute('lesson/index'),['class' => 'btn btn-success'])."</div></div>";
		
	}
	else if(!empty($_REQUEST["eid"]))
	{
		echo "<div class=\"\"><div class=\"col-sm-12\">";
		$errorCnt = @$_SESSION["readingerrorCount"];
		$successCnt=@$_SESSION["readingsuccessCount"];
		$errorArray=@$_SESSION["readingerrorData"];
		$totalCnt= $successCnt+$errorCnt;
		//echo $successCnt;
		//echo $errorCnt;
		if(!empty($successCnt) || !empty($errorCnt))
		{
			echo "<div ><div class=\"col-sm-12\"><h4>Total Reading Questions in file are:".$totalCnt."<br><font color='green'>successfully imported questions are:".$successCnt."</font><br><font color='red'>errors in: ".$errorCnt."</font></h4></div></div>";
		}
		if(!empty($errorArray) &&  count($errorArray)>0)
		{
			$str="<div ><div class=\"col-sm-8\"><b>Question</b></div><div class=\"col-sm-4\"><b>Reason</b></div></div>";
			for($i=0;count($errorArray)>$i; $i++) 
			{   
				
				$str.="<div ><div class=\"col-sm-8\">".$errorArray[$i]["Question"]."</div><div class=\"col-sm-4\">".$errorArray[$i]["Reason"]."</div></div>";
				 
			}
			echo $str;
			
			$_SESSION["readingerrorCount"]=0;
			$_SESSION["readingsuccessCount"]=0;
			$_SESSION["readingerrorData"]= array();
		}
		echo "</div></div><br>";
		echo "<div class=\"\"><div class=\"col-sm-12\">";
		$errorCnt = @$_SESSION["writingerrorCount"];
		$successCnt=@$_SESSION["writingsuccessCount"];
		$errorArray=@$_SESSION["writingerrorData"];
		$totalCnt= $successCnt+$errorCnt;
		//echo $successCnt;
		//echo $errorCnt;
		if(!empty($successCnt) || !empty($errorCnt))
		{
			echo "<div ><div class=\"col-sm-12\"><h4>Total Writing Questions in file are:".$totalCnt."<br><font color='green'>successfully imported questions are:".$successCnt."</font><br><font color='red'>errors in: ".$errorCnt."</font></h4></div></div>";
		}
		if(!empty($errorArray) && count($errorArray)>0)
		{
			$str="<div ><div class=\"col-sm-8\"><b>Question</b></div><div class=\"col-sm-4\"><b>Reason</b></div></div>";
			for($i=0;count($errorArray)>$i; $i++) 
			{   
				
				$str.="<div ><div class=\"col-sm-8\">".$errorArray[$i]["Question"]."</div><div class=\"col-sm-4\">".$errorArray[$i]["Reason"]."</div></div>";
				 
			}
			echo $str;
			
			$_SESSION["writingerrorCount"]=0;
			$_SESSION["writingsuccessCount"]=0;
			$_SESSION["writingerrorData"]= array();
		}
		echo "</div></div><br>";
		
		echo "<div class=\"\"><div class=\"col-sm-12\">";
		$errorCnt = @$_SESSION["speakingerrorCount"];
		$successCnt=@$_SESSION["speakingsuccessCount"];
		$errorArray=@$_SESSION["speakingerrorData"];
		$totalCnt= $successCnt+$errorCnt;
		//echo $successCnt;
		//echo $errorCnt;
		if(!empty($successCnt) || !empty($errorCnt))
		{
			echo "<div ><div class=\"col-sm-12\"><h4>Total Speaking Questions in file are:".$totalCnt."<br><font color='green'>successfully imported questions are:".$successCnt."</font><br><font color='red'>errors in: ".$errorCnt."</font></h4></div></div>";
		}
		if(!empty($errorArray) && count($errorArray)>0)
		{
			$str="<div ><div class=\"col-sm-8\"><b>Question</b></div><div class=\"col-sm-4\"><b>Reason</b></div></div>";
			for($i=0;count($errorArray)>$i; $i++) 
			{   
				
				$str.="<div ><div class=\"col-sm-8\">".$errorArray[$i]["Question"]."</div><div class=\"col-sm-4\">".$errorArray[$i]["Reason"]."</div></div>";
				 
			}
			echo $str;
			
			$_SESSION["speakingerrorCount"]=0;
			$_SESSION["speakingsuccessCount"]=0;
			$_SESSION["speakingerrorData"]= array();
		}
		echo "</div></div><br>";
		
		echo "<div class=\"\"><div class=\"col-sm-12\">";
		$errorCnt = @$_SESSION["listeningerrorCount"];
		$successCnt=@$_SESSION["listeningsuccessCount"];
		$errorArray=@$_SESSION["listeningerrorData"];
		$totalCnt= $successCnt+$errorCnt;
		//echo $successCnt;
		//echo $errorCnt;
		if(!empty($successCnt) || !empty($errorCnt))
		{
			echo "<div ><div class=\"col-sm-12\"><h4>Total Listening Questions in file are:".$totalCnt."<br><font color='green'>successfully imported questions are:".$successCnt."</font><br><font color='red'>errors in: ".$errorCnt."</font></h4></div></div>";
		}
		if(!empty($errorArray) && count($errorArray)>0)
		{
			$str="<div ><div class=\"col-sm-8\"><b>Question</b></div><div class=\"col-sm-4\"><b>Reason</b></div></div>";
			for($i=0;count($errorArray)>$i; $i++) 
			{   
				
				$str.="<div ><div class=\"col-sm-8\">".$errorArray[$i]["Question"]."</div><div class=\"col-sm-4\">".$errorArray[$i]["Reason"]."</div></div>";
				 
			}
			echo $str;
			
			$_SESSION["listeningerrorCount"]=0;
			$_SESSION["listeningsuccessCount"]=0;
			$_SESSION["listeningerrorData"]= array();
		}
		echo "</div></div><br>";
		echo "<div ><div class=\"col-sm-8\">&nbsp;</div><div class=\"col-sm-4\">".
		 \yii\helpers\Html::a( 'Re-upload', Yii::$app->request->referrer,['class' => 'btn btn-warning'])."&nbsp;&nbsp;".\yii\helpers\Html::a( 'Confirm and Proceed', Url::toRoute('exams/index'),['class' => 'btn btn-success'])."</div></div>";
	}
?>

<?php } ?>
	Records Per Page :&nbsp; <?php echo  \nterms\pagesize\PageSize::widget(); ?>
    <?php Pjax::begin(['timeout' => 5000,'id' => 'questions','enablePushState' => false, 'enableReplaceState' => false, ]); ?>
<?php	if(!empty($_REQUEST["Lid"]))
{
?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'queID',
		/*[
			'attribute'=>'moduleID',
			'label'=>'Module',
			'value'=>'coursemodule.moduleName'
		],*/
		[
			'attribute'=>'lessionID',
			'label'=>'Lesson',
			'value'=>'lesson.LessonName'
		],
		/*[
			'attribute'=>'examID',
			'label'=>'Exam',
			'value'=>'exams.examName'
		],*/
        //    'moduleID',
            //'lessionID',
           // 'examID',
            'queQuestion',
             'queType',
            // 'queDifficultyevel',
            // 'queQuestion',
             
            // 'queDisplayType',
             'queStatus',
             'queOption1',
             'queOption2',
             'queOption3',
             'queOption4',
             'queCorrectAns',
			 'queSolution',
	        /*    [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}',                    
					
			],*/
		
        ],
    ]); ?>
<?php } else if(!empty($_REQUEST["eid"]))
{
?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'queID',
		[
			'attribute'=>'moduleID',
			'label'=>'Module',
			'value'=>'coursemodule.moduleName'
		],
		/*[
			'attribute'=>'lessionID',
			'label'=>'Lesson',
			'value'=>'lesson.LessonName'
		],*/
		[
			'attribute'=>'examID',
			'label'=>'Exam',
			'value'=>'exams.examName'
		],
        //    'moduleID',
            //'lessionID',
           // 'examID',
            'queQuestion',
             'queType',
            // 'queDifficultyevel',
            // 'queQuestion',
             
            // 'queDisplayType',
             'queStatus',
             'queOption1',
             'queOption2',
             'queOption3',
             'queOption4',
             'queCorrectAns',
			 'queSolution',
	        /*    [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}',                    
					
			],*/
		
        ],
    ]); ?>
<?php
} else {?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'queID',
		[
			'attribute'=>'moduleID',
			'label'=>'Module',
			'value'=>'coursemodule.moduleName'
		],
		[
			'attribute'=>'lessionID',
			'label'=>'Lesson',
			'value'=>'lesson.LessonName'
		],
		[
			'attribute'=>'examID',
			'label'=>'Exam',
			'value'=>'exams.examName'
		],
        //    'moduleID',
            //'lessionID',
           // 'examID',
            'queFor',
            // 'queType',
            // 'queDifficultyevel',
            // 'queQuestion',
            // 'queSolution',
            // 'queDisplayType',
             'queStatus',
            // 'queOption1',
            // 'queOption2',
            // 'queOption3',
            // 'queOption4',
            // 'queCorrectAns',
	            [
                    'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
                    'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{changestatus}',                    
					'buttons' => [
					'changestatus' => function ($url, $model, $key) 
					{
					 if( $model->queStatus == 'Active') 						{
							return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['changestatus', 'id' => $model->queID],['title' => 'Make Inactive']);
						}
						else
						{
							return  Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', ['changestatus', 'id' => $model->queID],['title' => 'Make Active']);
						}
					},
            ]
			],
		
        ],
    ]); ?>
<?php }
 Pjax::end(); ?>
</div>
