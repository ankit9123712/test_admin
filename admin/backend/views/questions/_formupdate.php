<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Courses;
use backend\models\Questionschiled;
use backend\models\coursesubjects;
use backend\models\Coursechapters;
use wbraganca\dynamicform\DynamicFormWidget;
/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Questions */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="questions-form">

  <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">

<?= $form->field($model, 'courseID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Courses::find()->where(['courseStatus' => 'Active'])->all(),'courseID','courseName'),
                    'options' => ['placeholder' => 'Select Courses'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

<?= $form->field($model, 'coursesubjID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(coursesubjects::find()->where(['coursesubjStatus' => 'Active'])->all(),'coursesubjID','coursesubjName'),
                    'options' => ['placeholder' => 'Select Coursesubjects'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
<div class="col-sm-4">

<?= $form->field($model, 'coursechapterID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Coursechapters::find()->where(['coursechapterStatus' => 'Active'])->all(),'coursechapterID','coursechapterName'),
                    'options' => ['placeholder' => 'Select Coursechapters'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?></div>
</div>




<div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'queType')->dropDownList([ 'MCQ' => 'MCQ', 'MMCQ' => 'MMCQ', ], ['prompt' => '']) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'queDifficultyevel')->textInput() ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'queQuestion')->textInput(['maxlength' => true]) ?>
</div>
</div>
   <div class="row">
<div class="col-sm-4">

    <?= $form->field($model, 'queSolution')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'queDisplayType')->dropDownList([ 'Normal' => 'Normal', 'Table' => 'Table', ], ['prompt' => '']) ?>
</div>
<div class="col-sm-4">

    <?= $form->field($model, 'queStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => '']) ?>
</div>
</div>                     
<div class="row">
<div class="col-sm-3">

    <?= $form->field($model, 'queOption1')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">

    <?= $form->field($model, 'queOption2')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">

    <?= $form->field($model, 'queOption3')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-sm-3">

    <?= $form->field($model, 'queOption4')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">

<div class="col-sm-4">

    <?= $form->field($model, 'queCorrectAns')->textInput(['maxlength' => true]) ?>
</div>
</div>
   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
