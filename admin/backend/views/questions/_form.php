<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Coursemodule;
use backend\models\Lesson;
use backend\models\Exams;
use yii\helpers\Url;
use kartik\file\FileInput;
use dosamigos\tinymce\TinyMce;

/*please add dropdown related models here*/


/* @var $this yii\web\View */
/* @var $model backend\models\Questions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-4">
<?php

$Questions_Option = array('Free' => 'Free', 'Premium' => 'Premium');
echo $form->field($model, 'queBankType')->widget(Select2::classname(), [
                    'data' => $Questions_Option,
                    'options' => ['placeholder' => 'Select Bank Type',
					'onchange'=>'         
										$.get( "'.Url::toRoute(['coursemodule/coursemodulelist']).'", { type: $(this).val() } )
											.done(function( data ) {
												$( "#'.Html::getInputId($model, 'moduleID').'" ).html( data );
											}
										  );'],
                      'pluginOptions' => [
                      'allowClear' => true
     ],
  ]);
 ?>
</div>
<div class="col-sm-4">
<?= $form->field($model, 'moduleID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Coursemodule::find()->where(['moduleStatus' => 'Active'])->all(),'moduleID','moduleName'),
                    'options' => ['placeholder' => 'Select Module',
					'onchange'=>'         
										$.get( "'.Url::toRoute(['lesson/lessonlist']).'", { id: $(this).val() } )
											.done(function( data ) {
												$( "#'.Html::getInputId($model, 'lessionID').'" ).html( data );
											}
										  );'
					],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
<div class="col-sm-4">
<?= $form->field($model, 'lessionID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Lesson::find()->where(['LessonStatus' => 'Active'])->all(),'LessonID','LessonName'),
                    'options' => ['placeholder' => 'Select Lesson'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
</div>
</div>
<div class="row">
<div class="col-sm-6">
	<?= $form->field($model, 'examID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Exams::find()->where(['examStatus' => 'Active'])->all(),'examID','examName'),
                    'options' => ['placeholder' => 'Select Exams'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
				?>
    
	<?= $form->field($model, 'queType')->dropDownList([ 'MCQ' => 'MCQ', 'MMCQ' => 'MMCQ', 'Audio' => 'Audio', 'Single Text' => 'Single Text', 'Multiline Text' => 'Multiline Text', ]) ?>
   <?= $form->field($model, 'queVerificationRequred')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ]) ?>
	<?= $form->field($model, 'queQuestion')->widget(TinyMce::className(), [
        'options' => ['rows' => 20],
        'language' => 'en_CA',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);
  ?>
	
	<?= $form->field($model, 'queSolution')->textarea(['rows' => '8']) ?>
</div>
<div class="col-sm-6">


	<?= $form->field($model, 'queFile')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],'pluginOptions' => [
                  'initialPreview'=>isset($model->queFile) ? "../../uploads/questions/".$model->queFile : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>true,
                  'showPreview' => true,
                  'showCaption' => true,
				  'allowedFileExtensions' => ['jpg','png','docx','mp3','pdf','mp4'],
                  'showRemove' => false,
                  'showUpload' => false
					]]);
					?>

<?= $form->field($model, 'queOption1')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'queOption2')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'queOption3')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'queOption4')->textInput(['maxlength' => true]) ?>
 
	<?= $form->field($model, 'queCorrectAns')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'queStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>
</div>

<div class="row">

<div class="col-sm-6">

   
</div>

</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../questions/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
