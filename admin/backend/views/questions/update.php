<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Questions */

$this->title = 'Update Question: ' . $model->queID;
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->queID, 'url' => ['view', 'id' => $model->queID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="questions-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
