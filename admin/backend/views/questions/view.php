<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\models\Questions */

$this->title = 'View Question: ' . $model->queID;
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->queID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'queID',
            [
				'label'=>'Module',            
				'attribute'=>'coursemodule.moduleName',
		   ],
		   [
				'label'=>'Lesson',            
				'attribute'=>'lesson.LessonName',
		   ],
		   [
				'label'=>'Exam',            
				'attribute'=>'exams.examName',
		   ],
		   
            'queFor',
            'queType',
            'queFile',
            'queDifficultyevel',
            //'queQuestion',
			[
           'attribute' => 'queQuestion',
           'format' => 'raw',
           'type' => 'widget',
           'widgetOptions' => ['class' => TinyMce::classname()],
           'value' => $model->queQuestion,
         ],
            'queSolution',
            'queDisplayType',
            'queStatus',
            'queOption1',
            'queOption2',
            'queOption3',
            'queOption4',
            'queCorrectAns',
        ],
    ]) ?>

</div>
