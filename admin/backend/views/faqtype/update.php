<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Faqtype */

$this->title = 'Update Faq Type: ' . $model->faqtypeID;
$this->params['breadcrumbs'][] = ['label' => 'Faq Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->faqtypeID, 'url' => ['view', 'id' => $model->faqtypeID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="faqtype-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
