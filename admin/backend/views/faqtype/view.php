<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Faqtype */

$this->title = 'View Faq Type';
$this->params['breadcrumbs'][] = ['label' => 'Faq Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faqtype-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->faqtypeID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'faqtypeID',
            'faqtypeName',
            'faqtypeRemarks',
            'faqtypeStatus',
        ],
    ]) ?>

</div>
