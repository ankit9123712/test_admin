<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FaqtypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faqtype-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'faqtypeID') ?>

    <?= $form->field($model, 'faqtypeName') ?>

    <?= $form->field($model, 'faqtypeRemarks') ?>

    <?= $form->field($model, 'faqtypeStatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
