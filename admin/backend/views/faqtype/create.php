<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Faqtype */

$this->title = 'Create Faq Type';
$this->params['breadcrumbs'][] = ['label' => 'Faq Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faqtype-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
