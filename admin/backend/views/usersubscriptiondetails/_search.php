<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UsersubscriptiondetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usersubscriptiondetails-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'planName') ?>

    <?= $form->field($model, 'planDiscountPrice') ?>

    <?= $form->field($model, 'planDuration') ?>

    <?= $form->field($model, 'subscriptionStartDate') ?>

    <?= $form->field($model, 'subscriptionEndDate') ?>

    <?php // echo $form->field($model, 'streamName') ?>

    <?php // echo $form->field($model, 'coursestdName') ?>

    <?php // echo $form->field($model, 'userID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
