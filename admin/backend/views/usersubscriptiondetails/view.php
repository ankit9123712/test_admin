<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model backend\models\Usersubscriptiondetails */

$this->title = 'View Usersubscriptiondetails: ' . $model->userID;
$this->params['breadcrumbs'][] = ['label' => 'Usersubscriptiondetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usersubscriptiondetails-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->userID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'planName',
            'planDiscountPrice',
            'planDuration',
            'subscriptionStartDate',
            'subscriptionEndDate',
            'streamName',
            'coursestdName',
            'userID',
        ],
    ]) ?>

</div>
