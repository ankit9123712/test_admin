<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Usersubscriptiondetails */

$this->title = 'Create Usersubscriptiondetails';
$this->params['breadcrumbs'][] = ['label' => 'Usersubscriptiondetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usersubscriptiondetails-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
