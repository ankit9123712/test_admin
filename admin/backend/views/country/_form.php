<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Country */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-form">

    <?php $form = ActiveForm::begin([  'id' => $model->formName(),
           'enableAjaxValidation' => false,
       ],[
	'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
	]); ?>
<div class="row">
<div class="help-block help-block-error" style="color:red!important">
<?php echo  Html::errorSummary($model); ?> 
</div>
</div>
<div class="row">

<div class="col-sm-6">

    <?= $form->field($model, 'countryName')->textInput(['maxlength' => true]) ?>
	 <?= $form->field($model, 'countryStatus')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ]) ?>
</div>

<div class="col-sm-6">

<?php
	
	 echo $form->field($model, 'countryFlagImage')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'image/*'],
				  //'name'=>$model->countryFlagImage,	
				  'pluginOptions' => [
				  'initialPreviewShowDelete' => true,
				  'allowedFileExtensions' => ['jpg','png','jpeg'],
				  'showPreview' => true,
				  'showCaption' => true,
				  'showRemove' => false,
				  'showUpload' => false,
				  'validateInitialCount' => true,
				  'required' => true,
				  //'minFileCount' => 1,
				  'maxFileCount' => 1,
                  'initialPreview'=>!empty($model->countryFlagImage) ? "../../uploads/flag/".$model->countryFlagImage : "",
                  'initialPreviewAsData'=>true,
                  'overwriteInitial'=>false,
				   'initialPreviewConfig' => [
					[
						'caption' => $model->countryFlagImage,
						//'width' => "120px",
						'url' => Url::toRoute(['country/delete-flag-image','countryID'=>$model->countryID,'countryFlagImage'=>$model->countryFlagImage]),
						'key' => $model->countryID,
						'countryFlagImage' => $model->countryFlagImage,
						

					],

				],
               
	 ]
   ]);
	
	
	
	?>
</div>


</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>&nbsp;&nbsp; <button class="btn bcolor"><a href="../country/index">Cancel</a></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
