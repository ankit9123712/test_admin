<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Country */

$this->title = 'View Country';
$this->params['breadcrumbs'][] = ['label' => 'Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->countryID], ['class' => 'btn btn-primary']) ?>
           </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'countryID',
            'countryName',
            //'countryShortCode',
           // 'countryISO2Code',
          //  'countryCurrencySymbol',
          //  'countryDialCode',
             [                
                'attribute'=>'countryFlagImage',
                'value'=>   ((($model->countryFlagImage == NULL) || (($model['countryFlagImage'] == ''))) ? "../../../web/images/noimage.png": '../../../web/uploads/flag'.'/'.$model->countryFlagImage),
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'countryStatus',
            'countryCreatedDate',
           // 'countryLatitude',
           // 'countryLongitude',
        ],
    ]) ?>

</div>
