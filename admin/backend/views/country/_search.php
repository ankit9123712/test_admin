<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CountrySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'countryID') ?>

    <?= $form->field($model, 'countryName') ?>

    <?= $form->field($model, 'countryShortCode') ?>

    <?= $form->field($model, 'countryISO2Code') ?>

    <?= $form->field($model, 'countryCurrencySymbol') ?>

    <?php // echo $form->field($model, 'countryDialCode') ?>

    <?php // echo $form->field($model, 'countryFlagImage') ?>

    <?php // echo $form->field($model, 'countryStatus') ?>

    <?php // echo $form->field($model, 'countryCreatedDate') ?>

    <?php // echo $form->field($model, 'countryLatitude') ?>

    <?php // echo $form->field($model, 'countryLongitude') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
