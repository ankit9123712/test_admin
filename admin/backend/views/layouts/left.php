<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
      <!--  <div class="user-panel">
            <div class="pull-left image">
                <img src="../../../web/images/blue_user.png" class="img-circle" alt="User Image"/>
            </div>-->
           <!-- <div class="pull-left info">
                <p><?//=Yii::$app->user->identity->Admin_Name?></p>
            </div>
        </div>-->

        <!-- search form -->
        <?php /*<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> */ ?>
        <!-- /.search form -->

        <?php echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'MAIN NAVIGATION', 'options' => ['class' => 'header']],
                    ['label' => 'Dashboard', 'icon' => 'home', 'url' => ['site/index']],
					['label' => 'Analytics', 'icon' => 'home', 'url' => ['site/index2']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],		
					
					[
                        'label' => 'Location Management',
                        'icon' => 'map-marker',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Countries', 'icon' => 'fa fa-bullseye', 'url' => ['/country/index'],'active' => ($this->context->route == 'country/create' || $this->context->route == 'country/index' || $this->context->route == 'country/update' || $this->context->route == 'country/view')], 

							['label' => 'States', 'icon' => 'fa fa-bullseye', 'url' => ['/state/index'],'active' => ($this->context->route == 'state/create' || $this->context->route == 'state/index' || $this->context->route == 'state/update' || $this->context->route == 'state/view')],
							
							['label' => 'Cities', 'icon' => 'fa fa-bullseye', 'url' => ['/city/index'],'active' => ($this->context->route == 'city/create' || $this->context->route == 'city/index' || $this->context->route == 'city/update' || $this->context->route == 'city/view')],
														
													
                        ],
                    ],
					
					[
                        'label' => 'Masters',
                        'icon' => 'bookmark',
                        'url' => '#',
                        'items' => [	
							['label' => 'Modules', 'icon' => 'fa fa-bullseye', 'url' => ['/coursemodule/index'],'active' => ($this->context->route == 'coursemodule/create' || $this->context->route == 'coursemodule/index' || $this->context->route == 'coursemodule/update' || $this->context->route == 'coursemodule/view')],
							
							 
							
							 
							['label' => 'Practice Banks', 'icon' => 'fa fa-bullseye', 'url' => ['/practicebank/index'],'active' => ($this->context->route == 'practicebank/create' || $this->context->route == 'practicebank/index' || $this->context->route == 'practicebank/update' || $this->context->route == 'practicebank/view')],
							
							
							['label' => 'Monk Categories', 'icon' => 'fa fa-bullseye', 'url' => ['/monkcategory/index'],'active' => ($this->context->route == 'monkcategory/create' || $this->context->route == 'monkcategory/index' || $this->context->route == 'monkcategory/update' || $this->context->route == 'monkcategory/view')],
							
							
							['label' => 'Subscription Plans', 'icon' => 'fa fa-bullseye', 'url' => ['/subscriptionplan/index'],'active' => ($this->context->route == 'subscriptionplan/create' || $this->context->route == 'subscriptionplan/index' || $this->context->route == 'subscriptionplan/update' || $this->context->route == 'subscriptionplan/view')], 
							
							
                        ],
                    ],
					[
                        'label' => 'Lesson Management',
                        'icon' => 'bookmark',
                        'url' => '#',
                        'items' => [	
							 
							
							['label' => 'Lessons', 'icon' => 'fa fa-bullseye', 'url' => ['/lesson/index'],'active' => ($this->context->route == 'lesson/create' || $this->context->route == 'lesson/index' || $this->context->route == 'lesson/update' || $this->context->route == 'lesson/view')],
							
							 
							
							
                        ],
                    ],
					[
                        'label' => 'Exams Management',
                        'icon' => 'bookmark',
                        'url' => '#',
                        'items' => [	
														
							['label' => 'Exams', 'icon' => 'fa fa-bullseye', 'url' => ['/exams/index'],'active' => ($this->context->route == 'exams/create' || $this->context->route == 'exams/index' || $this->context->route == 'exams/update' || $this->context->route == 'exams/view')],
							
							
                        ],
                    ],
					[
					'label' => 'User Management',
                        'icon' => 'fa  fa-user',
                        'url' => '#',
                        'items' => [
					
					['label' => 'Students', 'icon' => 'fa fa-bullseye', 'url' => ['/users/index'],'active' => ($this->context->route == 'users/create' || $this->context->route == 'users/index' || $this->context->route == 'users/update' || $this->context->route == 'users/view')],
					['label' => 'Faculties', 'icon' => 'fa fa-bullseye', 'url' => ['/faculty/index'],'active' => ($this->context->route == 'faculty/create' || $this->context->route == 'faculty/index' || $this->context->route == 'faculty/update' || $this->context->route == 'faculty/view')],
				 
					
					],
				],
					
					
					[
					'label' => 'Evalutes Management',
                        'icon' => 'fa  fa-user',
                        'url' => '#',
                        'items' => [
					 
					
					['label' => 'Lessons', 'icon' => 'fa fa-bullseye', 'url' => ['/userlessions/index'],'active' => ($this->context->route == 'userlessions/create' || $this->context->route == 'userlessions/index' || $this->context->route == 'userlessions/update' || $this->context->route == 'userlessions/view')],
					
					['label' => 'Exams', 'icon' => 'fa fa-bullseye', 'url' => ['/userexams/index'],'active' => ($this->context->route == 'userexams/create' || $this->context->route == 'userexams/index' || $this->context->route == 'userexams/update' || $this->context->route == 'userexams/view')],
					
					
					],
				],
					
					[
                        'label' => 'Live Sessions Management',
                        'icon' => 'fa fa-bookmark',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Live Sessions', 'icon' => 'fa fa-bullseye', 'url' => ['/livesessions/index'],'active' => ($this->context->route == 'livesessions/create' || $this->context->route == 'livesessions/index' || $this->context->route == 'livesessions/update' || $this->context->route == 'livesessions/view')],
                           
							
														
                        ],
                    ],
					[
                        'label' => 'Promocode Management',
                        'icon' => 'fa fa-bookmark',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Offers', 'icon' => 'fa fa-bullseye', 'url' => ['/offer/index'],'active' => ($this->context->route == 'offer/create' || $this->context->route == 'offer/index' || $this->context->route == 'offer/update' || $this->context->route == 'offer/view')],
                           
							
														
                        ],
                    ],
					
					
				
										
					[
                        'label' => 'Contents Management',
                        'icon' => 'envelope',
                        'url' => '#',
                        'items' => [
						
							['label' => 'Dynamic Pages', 'icon' => 'fa fa-bullseye', 'url' => ['/cmspage/index'],'active' => ($this->context->route == 'cmspage/create' || $this->context->route == 'cmspage/index' || $this->context->route == 'cmspage/update' || $this->context->route == 'cmspage/view')],
							
					    	['label' => 'Faq Types', 'icon' => 'fa fa-bullseye', 'url' => ['/faqtype/index'],'active' => ($this->context->route == 'faqtype/create' || $this->context->route == 'faqtype/index' || $this->context->route == 'faqtype/update' || $this->context->route == 'faqtype/view')],
						
					      ['label' => 'FAQs', 'icon' => 'fa fa-bullseye', 'url' => ['/faq/index'],'active' => ($this->context->route == 'faq/create' || $this->context->route == 'faq/index' || $this->context->route == 'faq/update' || $this->context->route == 'faq/view')],
														
						   ['label' => 'IELTS', 'icon' => 'fa fa-bullseye', 'url' => ['/ieltsfiles/index'],'active' => ($this->context->route == 'ieltsfiles/create' || $this->context->route == 'ieltsfiles/index' || $this->context->route == 'ieltsfiles/update' || $this->context->route == 'ieltsfiles/view')],
						
							['label' => 'Message Centers', 'icon' => 'fa fa-bullseye', 'url' => ['/messagecenter/index'],'active' => ($this->context->route == 'messagecenter/create' || $this->context->route == 'messagecenter/index' || $this->context->route == 'messagecenter/update' || $this->context->route == 'messagecenter/view')],
							
							
							['label' => 'Broadcast Push Notification', 'icon' => 'fa fa-bullseye', 'url' => ['/notimessages/index'],'active' => ($this->context->route == 'notimessages/create' || $this->context->route == 'notimessages/index' || $this->context->route == 'notimessages/update' || $this->context->route == 'notimessages/view')],
							
							['label' => 'Banners', 'icon' => 'fa fa-bullseye', 'url' => ['/banner/index'],'active' => ($this->context->route == 'banner/create' || $this->context->route == 'banner/index' || $this->context->route == 'banner/update' || $this->context->route == 'banner/view')],	
							
							['label' => 'Files Management', 'icon' => 'fa fa-bullseye', 'url' => ['/fileslist/index'],'active' => ($this->context->route == 'fileslist/create' || $this->context->route == 'fileslist/index' || $this->context->route == 'fileslist/update' || $this->context->route == 'fileslist/view')],	

							['label' => 'Templates Management', 'icon' => 'fa fa-bullseye', 'url' => ['/template/index'],'active' => ($this->context->route == 'template/create' || $this->context->route == 'template/index' || $this->context->route == 'template/update' || $this->context->route == 'template/view')],	
                        ],
                    ],
					
					[
					'label' => 'Messages from Apps',
                        'icon' => 'fa  fa-user',
                        'url' => '#',
                        'items' => [
					['label' => 'Help Desks', 'icon' => 'fa fa-bullseye', 'url' => ['/helpdeskquery/index'],'active' => ($this->context->route == 'helpdeskquery/create' || $this->context->route == 'helpdeskquery/index' || $this->context->route == 'helpdeskquery/update' || $this->context->route == 'helpdeskquery/view')],
					['label' => 'Feedback', 'icon' => 'fa fa-bullseye', 'url' => ['/feedback/index'],'active' => ($this->context->route == 'feedback/create' || $this->context->route == 'feedback/index' || $this->context->route == 'feedback/update' || $this->context->route == 'feedback/view')],
					],
					
				],
					
					
					
					[
                        'label' => 'Settings',
                        'icon' => 'cog',
                        'url' => '#',
                        'items' => [
							['label' => 'General Settings', 'icon' => 'fa fa-bullseye', 'url' => ['/settings/update?id=1'],'active' => ($this->context->route == 'settings/update')],
							//['label' => 'Payment Gateway', 'icon' => 'circle-o', 'url' => ['/settings/update?id=1'],],
							//['label' => 'Delivery Charges', 'icon' => 'circle-o', 'url' => ['/settings/update?id=1'],],
							//['label' => 'GST Settings', 'icon' => 'circle-o', 'url' => ['/settings/update?id=1'],],							
//							['label' => 'Language', 'icon' => 'circle-o', 'url' => ['/language/index'],],
//							['label' => 'Language Wise Labels', 'icon' => 'circle-o', 'url' => ['/languagelabel/index'],],
//							['label' => 'Language Wise Messages', 'icon' => 'circle-o', 'url' => ['/languagemsgs/index'],],
							['label' => 'Change Password', 'icon' => 'fa fa-bullseye', 'url' => ['/admin/changepassword?id='.Yii::$app->user->identity->Admin_ID],],
							
						
                        ],
                    ],
					
					/*[
                        'label' => 'Logs',
                        'icon' => 'info-circle',
                        'url' => '#',
                        'items' => [
						 //   ['label' => 'Api Logs', 'icon' => 'fa fa-bullseye', 'url' => ['/apilog/index'],'active' => ($this->context->route == 'apilog/create' || $this->context->route == 'apilog/index' || $this->context->route == 'apilog/update' || $this->context->route == 'apilog/view')],
						//	['label' => 'SMS Logs', 'icon' => 'fa fa-bullseye', 'url' => ['/smslog/index'],'active' => ($this->context->route == 'smslog/create' || $this->context->route == 'smslog/index' || $this->context->route == 'smslog/update' || $this->context->route == 'smslog/view')],
							
						//	['label' => 'Notifications', 'icon' => 'fa fa-bullseye', 'url' => ['/notification/index'],'active' => ($this->context->route == 'notification/create' || $this->context->route == 'notification/index' || $this->context->route == 'notification/update' || $this->context->route == 'notification/view')],
							//['label' => 'Notifications', 'icon' => 'fa fa-bullseye', 'url' => ['/notification/index'],'active' => ($this->context->route == 'notification/create' || $this->context->route == 'notification/index' || $this->context->route == 'notification/update' || $this->context->route == 'notification/view')],
                        ],
                    ],*/
                ],
            ]
        ) ?>

    </section>

</aside>


