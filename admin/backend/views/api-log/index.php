<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \nterms\pagesize;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ApiLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Api Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-log-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?php Pjax::begin(['timeout' => 5000,'id' => 'apilog','enablePushState' => true, 'enableReplaceState' => false, ]); ?>
	Records Per Page :&nbsp; <?php echo \nterms\pagesize\PageSize::widget(); ?>	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'apiID',
            'apiName',
            'apiIP',
            //'apiRequest:ntext',
           // 'apiResponse',
             'apiType',
             'apiCallDate',

             [
  'label' =>'Action', 
  'value' => function ($model) 
  { 
	
  return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->apiID]).'   '.Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->apiID], ['data' => ['confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),'method' => 'post',],]);      
  },
  'format'=>'raw', 
],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]);?>
    <?php Pjax::end(); ?>
</div>
