<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ApiLog */

$this->title = 'Create Api Log';
$this->params['breadcrumbs'][] = ['label' => 'Api Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-log-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
