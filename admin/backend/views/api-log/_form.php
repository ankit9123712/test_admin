<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ApiLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="api-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'apiName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apiIP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apiRequest')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'apiResponse')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apiType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apiCallDate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
