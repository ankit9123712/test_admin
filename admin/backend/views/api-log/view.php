<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ApiLog */

$this->title = 'View Api Log: '.$model->apiID;
$this->params['breadcrumbs'][] = ['label' => 'Api Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-log-view">

    

    <p>
        
        <?= Html::a('Delete', ['delete', 'id' => $model->apiID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'apiID',
            'apiName',
            'apiIP',
			 [
                'attribute'=>'apiRequest',
                 'value'=> function ($model)
                {
                    return unserialize($model['apiRequest']);
                }
            ],
            'apiType',
            'apiCallDate',
        ],
    ]) ?>

</div>
