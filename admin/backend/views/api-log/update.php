<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ApiLog */

$this->title = 'Update Api Log: ' . $model->apiID;
$this->params['breadcrumbs'][] = ['label' => 'Api Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->apiID, 'url' => ['view', 'id' => $model->apiID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="api-log-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
