<?php

namespace backend\controllers;

use Yii;
use backend\models\Ieltsfiles;
use backend\models\IeltsfilesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * IeltsfilesController implements the CRUD actions for Ieltsfiles model.
 */
class IeltsfilesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ieltsfiles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IeltsfilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ieltsfiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ieltsfiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Ieltsfiles();

        if ($model->load(Yii::$app->request->post()) ) 
		{
			$model->ieltsfileName = UploadedFile::getInstance($model, 'ieltsfileName');
			if($model->ieltsfileName != '')
            {
                $Filename = date("YmdHis").'.' . $model->ieltsfileName->extension;
                $model->ieltsfileName->saveAs('uploads/ieltsfiles/'.$Filename);
                $model->ieltsfileName = $Filename;
            }
			else
			{
				$model->ieltsfileName="";
			}	
		
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->ieltsfileID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Ieltsfiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		 $ieltsfileName = $model->ieltsfileName;

        if ($model->load(Yii::$app->request->post())) 
		{
			
			 $model->ieltsfileName = UploadedFile::getInstance($model, 'ieltsfileName');
			if($model->ieltsfileName != '')
            {
                $Filename = date("YmdHis").'.' . $model->ieltsfileName->extension;
                $model->ieltsfileName->saveAs('uploads/ieltsfiles/'.$Filename);
                $model->ieltsfileName = $Filename;
				if(!empty($ieltsfileName))
				{

					if(file_exists('uploads/ieltsfiles/'.$ieltsfileName)) 
					{
					   @unlink('uploads/ieltsfiles/'.$ieltsfileName);	
					}
			
					//unlink('uploads/ieltsfiles/'.$ieltsfileName);
				}
            }
			
			else
			{
				$model->ieltsfileName=$ieltsfileName;
			}	
			
			
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->ieltsfileID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Ieltsfiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	public function actionExport()
	{
			$model = Ieltsfiles::find()->all();
            if (!$model) 
			{
                $model = new Ieltsfiles();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'IELTS_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['ieltsfileID','ieltsfileType','ieltsfileName','ieltsfileStatus'], 
			'headers' => ['ieltsfileID' => 'ID','ieltsfileType' => 'Type','ieltsfileName' => 'Name','ieltsfileStatus' => 'Status'], 

		]);
    }
		/**
     * Update Status of existing Ieltsfiles model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->ieltsfileStatus != 'Active') 
        {
        $model->ieltsfileStatus = 'Active';
        }
        else
        {            
		$model->ieltsfileStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	
   

    /**
     * Finds the Ieltsfiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ieltsfiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ieltsfiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
