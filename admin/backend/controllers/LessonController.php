<?php

namespace backend\controllers;

use Yii;
use backend\models\Lesson;
use backend\models\LessonSearch;
use backend\models\Questions;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;
/**
 * LessonController implements the CRUD actions for Lesson model.
 */
class LessonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view','import'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
						[
                            'actions' => ['import'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
						
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lesson models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LessonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lesson model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Lesson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Lesson();

        if ($model->load(Yii::$app->request->post()) ) 
		{
			if(empty($model->LessonFileType))
				$model->LessonFileType = "Content";
			$model->LessonFile = UploadedFile::getInstance($model, 'LessonFile');
			if($model->LessonFile != '')
            {
                $Filename = date("YmdHis").'.' . $model->LessonFile->extension;
                $model->LessonFile->saveAs('uploads/lesson/'.$Filename);
                $model->LessonFile = $Filename;
            }
			else
			{
				$model->LessonFile="";
			}
			$model->teachingfacultyIDs = implode(",",$model->teachingfacultyIDs);
			$model->gradingfacultyIDs = implode(",",$model->gradingfacultyIDs);
		    if($model->validate())
			{
				$model->save(false);
				$this->manageFaculties($model->teachingfacultyIDs,$model->LessonID,"Teaching");
				$this->manageFaculties($model->gradingfacultyIDs,$model->LessonID,"Grading");
				return $this->redirect(['import', 'id' => $model->LessonID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Lesson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		$LessonFile = $model->LessonFile;

        if ($model->load(Yii::$app->request->post())) 
		{
			
			$model->LessonFile = UploadedFile::getInstance($model, 'LessonFile');
			
            if($model->LessonFile != '')
            {
                if(empty($model->LessonFileType))
					$model->LessonFileType = "Content";
				$Filename = date("YmdHis").'.' . $model->LessonFile->extension;
                $model->LessonFile->saveAs('uploads/lesson/'.$Filename);
                $model->LessonFile = $Filename;
				
				if(!empty($LessonFile))
				{	
			
					if(file_exists('uploads/lesson/'.$LessonFile)) 
					{
					   @unlink('uploads/lesson/'.$LessonFile);	
					}
				}
				
            }
			else
			{
				$model->LessonFile = $LessonFile;
				
			}
			
			$model->teachingfacultyIDs = implode(",",$model->teachingfacultyIDs);
			$model->gradingfacultyIDs = implode(",",$model->gradingfacultyIDs);
		    if($model->validate())
			{
				$model->save(false);
				$this->manageFaculties($model->teachingfacultyIDs,$model->LessonID,"Teaching");
				$this->manageFaculties($model->gradingfacultyIDs,$model->LessonID,"Grading");
				return $this->redirect(['view', 'id' => $model->LessonID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->teachingfacultyIDs = explode(",",$model->teachingfacultyIDs);
			$model->gradingfacultyIDs = explode(",",$model->gradingfacultyIDs);
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }
	
	
	/*------------- Delete image from lesson folder & update db in blank code start------------------*/
	
	public function actionDeleteLessonImage()
    {
		
		$model = $this->findModel($_REQUEST['LessonID']);
		if(!empty($model))
		{
			if(!empty($_REQUEST['LessonFile']))
			{	
				if(file_exists('uploads/lesson/'.$_REQUEST['LessonFile'])) 
				{
				   @unlink('uploads/lesson/'.$_REQUEST['LessonFile']);	
				}	
			}
			
			$model->LessonFile = "";
			$model->save(false);
		}
		
		return true;
    }
	
	/*------------- Delete image from lesson folder & update db in blank code end ------------------*/
	
	
    /**
     * Deletes an existing Lesson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Lesson model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->LessonStatus != 'Active') 
        {
        $model->LessonStatus = 'Active';
        }
        else
        {            
		$model->LessonStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	
   public function actionExport()
	{
			$model = Lesson::find()->all();
            if (!$model) 
			{
                $model = new Lesson();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'Lessons_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['LessonID', 'coursemodule.moduleName','LessonNo','LessonName','LessonDescription','lessionMarkingSystem','lessionQuickTips','LessonType','LessonFileType','LessonFile','LessonFileURL','VideoFileCount','ContentFileCount','LessonStatus'], 
			'headers' => ['LessonID' => 'ID','coursemodule.moduleName' => 'Module','LessonNo' => 'NO','LessonName' => 'Name','LessonDescription' => 'Description','lessionMarkingSystem' => 'Marking System','lessionQuickTips' => 'Quick Tips','LessonType' => 'Type','LessonFileType' => 'File Type','LessonFile' => 'File','LessonFileURL' => 'File URL','VideoFileCount' => 'File Count','ContentFileCount' => 'File Count','LessonStatus' => 'Status'], 

		]);
    }

    /**
     * Finds the Lesson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lesson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lesson::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	public function actionLessonlist($id)
    {
        $rows = \backend\models\Lesson::find()->where(['moduleID' => $id,"LessonStatus"=>"Active"])->all();
        echo "<option value=''>Select Lesson </option>";
        if(count($rows)>0){
            foreach($rows as $row){
                echo "<option value='$row->LessonID'>$row->LessonName</option>";
            }
        }
         
    }
	public function actionImport($id)
    {
		$model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) ) 
		{
			$model->LessonFile = UploadedFile::getInstance($model, 'LessonFile');
			if($model->LessonFile != '')
            {
                $Filename = date("YmdHis").'.' . $model->LessonFile->extension;
                $model->LessonFile->saveAs('uploads/lessions/'.$Filename);
                
				$data = \moonland\phpexcel\Excel::import('uploads/lessions/'.$Filename, ['setFirstRecordAsKeys' => true]);
				
				$errorCnt=0;
				$successCnt=0;
				$errorArray = array();
				$isValid=true;
				foreach ($data as $item) 
				{
					//echo $model->moduleID;die;
					if($model->moduleID == 6 || $model->moduleID == 8)
					{
						//Reading
						if($item['Question Type']=="Single Text" || $item['Question Type']=="MCQ" || $item['Question Type']=="MMCQ")
						{
							if($item['Question Type']=="Single Text" && $item['Solution']=="")
							{
								$isValid = false;
								$errorArray[$errorCnt]["Question"] = $item['Question'];
								$errorArray[$errorCnt]["Reason"]= "Solution can not be empty when Question Type is Single Text";
								
							}
							if($item['Question Type']=="MCQ" && $item['Correct Answer']=="")
							{
								$isValid = false;
								$errorArray[$errorCnt]["Question"] = $item['Question'];
								$errorArray[$errorCnt]["Reason"]= "Correct Answer can not be empty when Question Type is MCQ";
								
							}
							if($item['Question Type']=="MMCQ" && $item['Correct Answer']=="")
							{
								$isValid = false;
								$errorArray[$errorCnt]["Question"] = $item['Question'];
								$errorArray[$errorCnt]["Reason"]= "Correct Answer can not be empty when Question Type is MMCQ";
								
							}
						}
						else
						{
							$isValid=false;
							$errorArray[$errorCnt]["Question"] = $item['Question'];
							$errorArray[$errorCnt]["Reason"]= "Question Type can be Single Text, MCQ or MMCQ (Case Sensitive)";
						}						 
					}
					else if($model->moduleID == 7 || $model->moduleID == 9)
					{
						if($item['Question Type']!="Multiline Text")
						{							
							$isValid=false;
							$errorArray[$errorCnt]["Question"] = $item['Question'];
							$errorArray[$errorCnt]["Reason"]= "Question Type can be Multiline Text (Case Sensitive)";
						}
					}
					else if($model->moduleID == 10 || $model->moduleID == 11)			
					{
						if($item['Question Type']=="Single Text" || $item['Question Type']=="MCQ" || $item['Question Type']=="MMCQ" || $item['Question Type']!="Multiline Text")
						{
							if($item['Question Type']=="Single Text" && $item['Solution']=="")
							{
								$isValid = false;
								$errorArray[$errorCnt]["Question"] = $item['Question'];
								$errorArray[$errorCnt]["Reason"]= "Solution can not be empty when Question Type is Single Text";
								
							}
							if($item['Question Type']=="MCQ" && $item['Correct Answer']=="")
							{
								$isValid = false;
								$errorArray[$errorCnt]["Question"] = $item['Question'];
								$errorArray[$errorCnt]["Reason"]= "Correct Answer can not be empty when Question Type is MCQ";
								
							}
							if($item['Question Type']=="MMCQ" && $item['Correct Answer']=="")
							{
								$isValid = false;
								$errorArray[$errorCnt]["Question"] = $item['Question'];
								$errorArray[$errorCnt]["Reason"]= "Correct Answer can not be empty when Question Type is MMCQ";
								
							}
						}
						else
						{
							$isValid=false;
							$errorArray[$errorCnt]["Question"] = $item['Question'];
							$errorArray[$errorCnt]["Reason"]= "Question Type can be Single Text, MCQ or MMCQ (Case Sensitive)";
						}	
					}
					else if($model->moduleID == 12 || $model->moduleID == 13)	
					{
						if($item['Question Type']!="Audio")
						{							
							$isValid=false;
							$errorArray[$errorCnt]["Question"] = $item['Question'];
							$errorArray[$errorCnt]["Reason"]= "Question Type can be Audio (Case Sensitive)";
						}
					}
					if($item['Question']=="" || $item['Question Type']=="" || $item['Verification Required']=="")
					{
						$isValid=false;
						$errorArray[$errorCnt]["Question"] = $item['Question'];
						$errorArray[$errorCnt]["Reason"]= "Question or Question Type or Verification Required can not be empty.";
					}
					//print_r($errorArray);
					if($item['Question']=="________ The benefits of optimism on health have been long known.")
					{
						//print_r($item);
						//die;
					}
					if($isValid)
					{
						if($successCnt==0)
						{
							\Yii::$app
							->db
							->createCommand()
							->delete('questions', ['lessionID' => $model->LessonID])
							->execute();
						}
						
						
						$queModel = new Questions;
						$queModel->moduleID   = $model->moduleID;
						$queModel->lessionID   = $model->LessonID;
						$queModel->queFor   = "Lesson";
						$queModel->queBankType   = $model->LessonType;
						$queModel->queType   = $item['Question Type'];
						$queModel->queFile   = $item['Question file name'];
						$queModel->queDifficultyevel   = "1";
						$queModel->queQuestion   = $item['Question'];
						$queModel->queSolution   = $item['Solution'];
						$queModel->queDisplayType   = "Normal";
						$queModel->queStatus   = "Active";
						
						if($item['Question Type']=="MCQ")
						{
							$queModel->queOption1   = (string)$item['Option1'];
							$queModel->queOption2   = (string)$item['Option2'];
							
							if($item['Option1']=="1" && strtolower($item['Option3'])=="not given" )
								$queModel->queOption1 = "True";
							if($item['Option1']==""  && strtolower($item['Option3'])=="not given")
								$queModel->queOption1 = "False";
							if($item['Option2']=="1" && strtolower($item['Option3'])=="not given")
								$queModel->queOption2 = "True";
							if($item['Option2']==""  && strtolower($item['Option3'])=="not given")
								$queModel->queOption2 = "False";
							if($item['Option1']=="1" && strtolower($item['Option3'])=="not give" )
								$queModel->queOption1 = "True";
							if($item['Option1']==""  && strtolower($item['Option3'])=="not give")
								$queModel->queOption1 = "False";
							if($item['Option2']=="1" && strtolower($item['Option3'])=="not give")
								$queModel->queOption2 = "True";
							if($item['Option2']==""  && strtolower($item['Option3'])=="not give")
								$queModel->queOption2 = "False";
							
							$queModel->queOption3   = (string)$item['Option3'];
							$queModel->queOption4   = (string)$item['Option4'];	
						}
						else
						{
							$queModel->queOption1   = (string)$item['Option1'];
							$queModel->queOption2   = (string)$item['Option2'];
							$queModel->queOption3   = (string)$item['Option3'];
							$queModel->queOption4   = (string)$item['Option4'];
						}
						$queModel->queAnswerExplanation   = $item['Answer Explanation'];
						$queModel->queAdditionalInstructions   = $item['Question Additional Instructions'];
						$queModel->queDiagramLink   = $item['Question Diagram Link'];
						
						
						
						$queModel->queCorrectAns   = $item['Correct Answer'];
						$queModel->queVerificationRequred   = $item['Verification Required'];
						$queModel->save(false);
						$successCnt++;
					}
					else
					{
						$errorCnt++;
					}
				}
				$_SESSION["errorCount"]=$errorCnt;
				$_SESSION["successCount"]=$successCnt;
				$_SESSION["errorData"]=$errorArray;
				/*return $this->render('view', [
				'model' => $this->findModel($id),
			]);*/
				return Yii::$app->response->redirect(Url::to(['questions/index', 'Lid' => $model->LessonID]));

				
            }
		}
		else
		{
			return $this->render('importlessionquestions', [
				'model' => $this->findModel($id),
			]);
		}
    }
	protected function manageFaculties($ids,$lessionID,$lessonfacultyType)
	{
		$delSQL="DELETE FROM lessonfaculty WHERE lessionID=".$lessionID." AND lessonfacultyType='".$lessonfacultyType."'";
		\Yii::$app->db->createCommand($delSQL)->execute();
		
		$catNames = explode(",",$ids);
		for ($x = 0; $x <= count($catNames)-1; $x++)	
		{
			$insSQL="INSERT INTO lessonfaculty (lessionID,facultyID,lessonfacultyType) values('".$lessionID."','".$catNames[$x]."','".$lessonfacultyType."')";
			\Yii::$app->db->createCommand($insSQL)->execute();
		}

	} 
	public function actionFacultylist($type)
    {
        $rows = \backend\models\Faculty::find()->select('faculty.facultyID,faculty.facultyFullName')->join('LEFT join','lessonfaculty','faculty.facultyID= lessonfaculty.facultyID')->join('LEFT join','lesson','lesson.LessonID= lessonfaculty.lessionID')->where(['lesson.moduleID' => $type,"facultyStatus"=>"Active"])->all();
        echo "<option value=''>Select Faculty</option>";
        if(count($rows)>0){
            foreach($rows as $row){
                echo "<option value='$row->facultyID'>$row->facultyFullName</option>";
            }
        }
         
    }
	
}
