<?php

namespace backend\controllers;

use Yii;
use backend\models\Offer;
use backend\models\OfferSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\UploadedFile;
/**
 * OfferController implements the CRUD actions for Offer model.
 */
class OfferController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Offer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OfferSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Offer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Offer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Offer();

        if ($model->load(Yii::$app->request->post()) ) 
		{
			
			 $model->offerImage = UploadedFile::getInstance($model, 'offerImage');

				if($model->offerImage != '')
				{
					$Filename = date("YmdHis").'.' . $model->offerImage->extension;
					$model->offerImage->saveAs('uploads/offer/'.$Filename);
					$model->offerImage = $Filename;
				}
				else
				{
					$model->offerImage="";
				}
		    if($model->validate())
			{
				$model->offerStartDate = date('Y-m-d',strtotime($_REQUEST["offerStartDate"]));				
				$model->offerEndDate = date('Y-m-d',strtotime($_REQUEST["offerEndDate"])); 
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->offerID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->offerStartDate = date('Y-m-d');				
			$model->offerEndDate = date('Y-m-d',strtotime(date("Y-m-d")."+1 days"));
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Offer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		 $offerImage = $model->offerImage;

        if ($model->load(Yii::$app->request->post())) 
		{
			$model->offerImage = UploadedFile::getInstance($model, 'offerImage');
			

				if($model->offerImage != '')
			{
				$Filename = date("YmdHis").'.' . $model->offerImage->extension;
				$model->offerImage->saveAs('uploads/offer/'.$Filename);
				
				$model->offerImage = $Filename;
				
				if(!empty($offerImage))
				{	
				
					if(file_exists('uploads/offer/'.$offerImage)) 
					{
					   @unlink('uploads/offer/'.$offerImage);	
					}
				}
				
			}
				else
			{
				$model->offerImage=$offerImage;
			}
			
		    if($model->validate())
			{
				$model->offerStartDate = date('Y-m-d',strtotime($_REQUEST["offerStartDate"]));				
				$model->offerEndDate = date('Y-m-d',strtotime($_REQUEST["offerEndDate"])); 
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->offerID]);
			}
			else
			{
				$model->offerStartDate = date('Y-m-d',strtotime($model->offerStartDate));				
				$model->offerEndDate = date('Y-m-d',strtotime($model->offerEndDate)); 
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->offerStartDate = date('Y-m-d',strtotime($model->offerStartDate));				
				$model->offerEndDate = date('Y-m-d',strtotime($model->offerEndDate)); 
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }
	
	
	
	/*------------- Delete image from country folder & update db in blank code start------------------*/

	public function actionDeleteOffersImage()
	{

		$model = $this->findModel($_REQUEST['offerID']);
		if(!empty($model))
		{
			if(!empty($_REQUEST['offerImage']))
			{	
			if(file_exists('uploads/offer/'.$_REQUEST['offerImage'])) 
			{
			@unlink('uploads/offer/'.$_REQUEST['offerImage']);	
			}	
		}

		$model->offerImage = "";
		$model->save(false);
		}

		return true;
		}

/*------------- Delete image from country folder & update db in blank code end ------------------*/
	
	
    /**
     * Deletes an existing Offer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Offer model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->offerStatus != 'Active') 
        {
        $model->offerStatus = 'Active';
        }
        else
        {            
		$model->offerStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	public function actionExport()
	{
			$model = Offer::find()->all();
            if (!$model) 
			{
                $model = new Offer();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'Offers_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['offerID', 'offerName','offerImage','offerDiscountType','offerDiscountValue','offerStartDate','offerEndDate','offerCodeUse','offerMaxLimit','offerCodeType','offerCodeQty','offerSalePrice','offerDescription','offerStatus'], 
			'headers' => ['offerID' => 'ID','offerName' => 'Name','offerImage' => 'Image','offerDiscountType' => 'Discount Type','offerDiscountValue' => 'Discount Value','offerStartDate' => 'Start Date','offerEndDate' => 'End Date','offerCodeUse' => 'Code Use','offerMaxLimit' => 'Max Limit','offerCodeType' => 'Code Type','offerCodeQty' => 'Code Qty','offerSalePrice' => 'Sale Price','offerDescription' => 'Description','offerStatus' => 'Status'], 

		]);
    }
   

    /**
     * Finds the Offer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Offer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Offer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
