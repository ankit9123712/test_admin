<?php

namespace backend\controllers;

use Yii;
use backend\models\Lessonfiles;
use backend\models\LessonfilesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * LessonfilesController implements the CRUD actions for Lessonfiles model.
 */
class LessonfilesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lessonfiles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LessonfilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lessonfiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Lessonfiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Lessonfiles();

        if ($model->load(Yii::$app->request->post()) ) 
		{
			$model->LessonfileFile = UploadedFile::getInstance($model, 'LessonfileFile');
			if($model->LessonfileFile != '')
            {
                $Filename = date("YmdHis").'.' . $model->LessonfileFile->extension;
                $model->LessonfileFile->saveAs('uploads/lessonfiles/'.$Filename);
                $model->LessonfileFile = $Filename;
            }
			else
			{
				$model->LessonfileFile="";
			}
		
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->LessonfileID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Lessonfiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		 $LessonfileFile = $model->LessonfileFile;

        if ($model->load(Yii::$app->request->post())) 
		{
			
			 $model->LessonfileFile = UploadedFile::getInstance($model, 'LessonfileFile');
			if($model->LessonfileFile != '')
            {
                $Filename = date("YmdHis").'.' . $model->LessonfileFile->extension;
                $model->LessonfileFile->saveAs('uploads/lessonfiles/'.$Filename);
                $model->LessonfileFile = $Filename;
				if(!empty($LessonfileFile))
				{

					if(file_exists('uploads/lessonfiles/'.$LessonfileFile)) 
					{
					   @unlink('uploads/lessonfiles/'.$LessonfileFile);	
					}
			
					//unlink('uploads/colors/'.$LessonfileFile);
				}
            }
			
			else
			{
				$model->LessonfileFile=$LessonfileFile;
			}	
			
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->LessonfileID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }
	
	
	
	
    /**
     * Deletes an existing Lessonfiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Lessonfiles model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->LessonfileStatus != 'Active') 
        {
        $model->LessonfileStatus = 'Active';
        }
        else
        {            
		$model->LessonfileStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	public function actionExport()
	{
			$model = Lessonfiles::find()->all();
            if (!$model) 
			{
                $model = new Lessonfiles();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'Lesson Files_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['LessonfileID', 'lesson.LessonName','LessonfileFileType','LessonfileFile','Lessonfile','LessonfileStatus'], 
			'headers' => ['LessonfileID' => 'ID','lesson.LessonName' => 'Lesson','LessonfileFileType' => 'File Type','LessonfileFile' => 'File','Lessonfile' => 'File','LessonfileStatus' => 'Status'], 

		]);
    }
   
   
   
   /*------------- Delete image from country folder & update db in blank code start------------------*/

		public function actionDeleteLessonfilesImage()
		{

			$model = $this->findModel($_REQUEST['LessonfileID']);
			if(!empty($model))
			{
			if(!empty($_REQUEST['LessonfileFile']))
			{	
				if(file_exists('uploads/lessonfiles/'.$_REQUEST['LessonfileFile'])) 
			{
				@unlink('uploads/lessonfiles/'.$_REQUEST['LessonfileFile']);	
			}	
			}

				$model->LessonfileFile = "";
				$model->save(false);
			}

		return true;
		}

/*------------- Delete image from country folder & update db in blank code end ------------------*/
   

    /**
     * Finds the Lessonfiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lessonfiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lessonfiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
