<?php

namespace backend\controllers;

use Yii;
use backend\models\Usersubscriptions;
use backend\models\UsersubscriptionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * UsersubscriptionsController implements the CRUD actions for Usersubscriptions model.
 */
class UsersubscriptionsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usersubscriptions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersubscriptionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usersubscriptions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usersubscriptions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Usersubscriptions();

        if ($model->load(Yii::$app->request->post()) ) 
		{
			
		
		    if($model->validate())
			{
				
				$model->usersubscriptionStartDate = date('Y-m-d',strtotime($_REQUEST["usersubscriptionStartDate"]));				
				$model->usersubscriptionEndDate = date('Y-m-d',strtotime($_REQUEST["usersubscriptionEndDate"])); 
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->usersubscriptionID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->usersubscriptionStartDate = date('Y-m-d');				
			$model->usersubscriptionEndDate = date('Y-m-d',strtotime(date("Y-m-d")."+1 days")); 
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Usersubscriptions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) 
		{
			
		    if($model->validate())
			{
				$model->usersubscriptionStartDate = date('Y-m-d',strtotime($_REQUEST["usersubscriptionStartDate"]));				
				$model->usersubscriptionEndDate = date('Y-m-d',strtotime($_REQUEST["usersubscriptionEndDate"])); 
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->usersubscriptionID]);
			}
			else
			{
				$model->usersubscriptionStartDate = date('Y-m-d',strtotime($model->usersubscriptionStartDate));				
				$model->usersubscriptionEndDate = date('Y-m-d',strtotime($model->usersubscriptionEndDate)); 
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->usersubscriptionStartDate = date('Y-m-d',strtotime($model->usersubscriptionStartDate));				
				$model->usersubscriptionEndDate = date('Y-m-d',strtotime($model->usersubscriptionEndDate)); 
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Usersubscriptions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		
	public function actionExport()
	{
			$model = Usersubscriptions::find()->all();
            if (!$model) 
			{
                $model = new Usersubscriptions();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'User Subscriptions_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['usersubscriptionID', 'users.userFullName','subscriptionplan.planName','usersubscriptionAmount','usersubscriptionCoponCode','usersubscriptionStartDate','usersubscriptionEndDate'], 
			'headers' => ['usersubscriptionID' => 'ID','users.userFullName' => 'Users','subscriptionplan.planName' => 'Sub scriptions','usersubscriptionAmount' => 'Amount','usersubscriptionCoponCode' => 'Copon Code','usersubscriptionStartDate' => 'Start Date','usersubscriptionEndDate' => 'End Date'], 

		]);
    }
   

    /**
     * Finds the Usersubscriptions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usersubscriptions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usersubscriptions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
