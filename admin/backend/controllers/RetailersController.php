<?php

namespace backend\controllers;

use Yii;
use app\models\Retailers;
use backend\models\RetailersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use common\models\Template;
use yii\filters\AccessControl;
use app\models\Useraddress;
use app\models\Usercategories;
use yii\web\UploadedFile;
use yii\helpers\Html;
/**
 * RetailersController implements the CRUD actions for Retailers model.
 */
class RetailersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view','export'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
						[
                            'actions' => ['export'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Retailers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RetailersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Retailers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
			'modelUserAddress' => $model->userAddress,
        ]);
    }

    /**
     * Creates a new Retailers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Retailers();
		
        $modelUserAddress = new Useraddress();
        if ($model->load(Yii::$app->request->post()) ) 
		{
			$model->userProfilePicture = UploadedFile::getInstance($model, 'userProfilePicture');
			if($model->userProfilePicture != '')
            {
                $Filename = date("YmdHis").'.' . $model->userProfilePicture->extension;
                $model->userProfilePicture->saveAs('uploads/retailer/'.$Filename);
                $model->userProfilePicture = $Filename;
            }
			else
			{
				$model->userProfilePicture = " ";
			}
				
			
			$model->userCountryCode ="91";	
			$model->userDeviceType = "Web";
			$model->userDeviceID = "any";
			$model->userType = "Retailer";
			$model->userPassword ="123456";
			$model->userPassword= \Yii::$app->security->generatePasswordHash($model->userPassword);
			$model->categoryIDs = implode(",",$model->categoryIDs);
			if($model->validate())
			{
				$model->save();
				/*Categories*/			
				$modelUserAddress->load(Yii::$app->request->post());
				$modelUserAddress->userID = $model->userID;
				$modelUserAddress->addressType = "Registered";
				if($modelUserAddress->save())
				{
					
					$this->manageCategories($model->categoryIDs,$model->userID);		
				
					//return $this->redirect(['view', 'id' => $model->userID, ]);
					return $this->redirect(['index']);
				}
				else
				{
					print_r($modelUserAddress->getErrors());
				}				
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				'modelUserAddress' => $modelUserAddress,
				]);
			}			
        } else {
            return $this->render('create', [
                'model' => $model,
				'modelUserAddress' => $modelUserAddress,
            ]);
        }
    }

    /**
     * Updates an existing Retailers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$modelUserAddress = $model->userAddress;
		$oldProfileImage = $model->userProfilePicture;
        if ($model->load(Yii::$app->request->post()) )
		{
			$model->userProfilePicture = UploadedFile::getInstance($model, 'userProfilePicture');
			if($model->userProfilePicture != '')
            {
                $Filename = date("YmdHis").'.' . $model->userProfilePicture->extension;
                $model->userProfilePicture->saveAs('uploads/manufactures/'.$Filename);
                $model->userProfilePicture = $Filename;
            }
			else
			{
				$model->userProfilePicture = $oldProfileImage;
			}
			$model->categoryIDs = implode(",",$model->categoryIDs);
			if($model->save())
			{
				/*Categories*/
			
				$modelUserAddress->load(Yii::$app->request->post());
				$modelUserAddress->addressType = "Registered";
				if($modelUserAddress->save())
				{
					
					$this->manageCategories($model->categoryIDs,$model->userID);		
				
					//return $this->redirect(['view', 'id' => $model->userID, ]);
					return $this->redirect(['index']);
				}
				else
				{
					print_r($modelUserAddress->getErrors());
				}
			}	
			else
				print_r($model->getErrors());
		
            
        } else {
			$model->categoryIDs = explode(",",$model->categoryIDs);	
            return $this->render('update', [
                'model' => $model,
				'modelUserAddress' => $modelUserAddress,
            ]);
        }
    }

    /**
     * Deletes an existing Retailers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	public function actionExport()
    {			
			
			//print_r($modelUserAddress);
			$query = new \yii\db\Query();
			$query	->select('users.*,addressTitle,addressContactDesgination,addressAddressLine1,addressGST,addressPAN,languageName')  
				->from('users ')
				->where(['addressType' => 'Registered','userType' => 'Retailer'])
				->join(	'INNER JOIN', 
						'language',
						'language.languageID = users.languageID'
				)
				->join(	'INNER JOIN', 
						'useraddress',
						'useraddress.userID = users.userID'
				);
				
			$command = $query->createCommand();
			$data = $command->queryAll();
			
            
            \moonland\phpexcel\Excel::export(['models' => $data, 'fileName' => 'retailer'.date("Ymdhis").'.xlsx', 'format' => 'Excel2007', 
			
			'columns' => [        
            'userFullName',
            'userEmail',
			'userCountryCode',
			'userMobile',
            'userAlternateContact',
            'userProfilePicture',
            'languageName',            
			'addressTitle',
			'addressContactDesgination',
			'addressAddressLine1',
			'addressGST',
			'addressPAN',		
            'userVerified',
            'userEmailNotification',
            'userPushNotification',
            'userStatus',
        ], 
		'headers' => [
			'userFullName'=> 'Full Name',
            'userEmail'=> 'Email',
			'userCountryCode'=> 'Dial Code',
			'userMobile'=> 'Mobile',
            'userAlternateContact'=> 'Alternate Contact',
            'userProfilePicture'=> 'Logo',
            'languageName'=> 'Language',            
			'addressTitle' => 'Company Name',
			'addressContactDesgination'=>'Desgination',
			'addressAddressLine1' => 'Address Line1',
			'addressGST'=>'GST',
			'addressPAN'=>'PAN',
            'userVerified'=> 'Verified',
            'userEmailNotification'=> 'Email Notification',
            'userPushNotification'=> 'Push Notification',
            'userStatus'=> 'Status',
		],
			
		]);
        
    }
    /**
     * Finds the Retailers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Retailers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
	public function actionChangestatus($id)
    {
        $model = $this->findModel($id);

        if($model->userStatus != 'Active')
        {
            $model->userStatus = 'Active';
        }
        else
        {
            $model->userStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
    }
	public function actionChangeverification($id)
    {
        $model = $this->findModel($id);

        if($model->userVerified != 'Yes')
        {
            $model->userVerified = 'Yes';
        }
        else
        {
            $model->userVerified = 'No';
        }
        $model->save(false);
        return $this->redirect(['index']);
    } 
	public function actionChangegstitr($id)
    {
        $model = $this->findModel($id);

        if($model->userGSTITR == 'Requested')
        {
            $model->userGSTITR = 'Verified';
			$model->save(false);
			$pushStr = "Your GST ITR request is verified"	;
			$subject = "GST ITR Verified";
			$template = new Template();
			$template->push($model->userDeviceID,"ITR",$pushStr,$model->userID,$model->userID,$model->userDeviceType,$model->userID,$subject);
        }
        
        return $this->redirect(['index']);
    }
    protected function findModel($id)
    {
        if (($model = Retailers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	protected function manageCategories($ids,$userID)
	{
		$delSQL="DELETE FROM usercategories WHERE userID=".$userID;
		\Yii::$app->db->createCommand($delSQL)->execute();
		
		$categoryNames = explode(",",$ids);
		for ($x = 0; $x <= count($categoryNames)-1; $x++)	
		{
			$insSQL="INSERT INTO usercategories (userID,categoryID) values('".$userID."','".$categoryNames[$x]."')";
			\Yii::$app->db->createCommand($insSQL)->execute();
		}
		
		$processData=Usercategories::find()
					->select('GROUP_CONCAT(`categoryID`) as usercategoryID')
					->where(['userID' => $userID])
					->groupby('userID')
					->asArray()
					->all();
		//print_r($processData);
		$SQL= "UPDATE  users SET categoryIDs='".$processData[0]["usercategoryID"]."'  WHERE userID='".$userID."'";
		\Yii::$app->db->createCommand($SQL)->execute();
		

	}
}
