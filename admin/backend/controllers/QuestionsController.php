<?php

namespace backend\controllers;

use Yii;
use backend\models\Questions;
use backend\models\QuestionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\UploadedFile;
/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		//print_r($_SESSION);
		//die;
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		
			
		$dataProvider = $searchModel->search($params);	
		if(!empty($_REQUEST["Lid"]))
		    $dataProvider->query->where('lessionID = '.$_REQUEST["Lid"]);
		if(!empty($_REQUEST["eid"]))
		    $dataProvider->query->where('questions.examID = '.$_REQUEST["eid"]);
			
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Questions();

        if ($model->load(Yii::$app->request->post()) )
		{
			$model->queFile = UploadedFile::getInstance($model, 'queFile');
			if($model->queFile != '')
            {
                $Filename = date("YmdHis").'.' . $model->queFile->extension;
                $model->queFile->saveAs('uploads/questions/'.$Filename);
                $model->queFile = $Filename;
            }
			else
			{
				$model->queFile="";
			}
		
		    if($model->validate())
			{
				$model->examID = "0";
				$model->queFor = "Lesson";
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->queID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Questions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		  $queFile = $model->queFile;

        if ($model->load(Yii::$app->request->post())) 
		{
			
			$model->queFile = UploadedFile::getInstance($model, 'queFile');
			if($model->queFile != '')
            {
                $Filename = date("YmdHis").'.' . $model->queFile->extension;
                $model->queFile->saveAs('uploads/questions/'.$Filename);
                $model->queFile = $Filename;
				if(!empty($queFile))
				{

					if(file_exists('uploads/questions/'.$queFile)) 
					{
					   @unlink('uploads/questions/'.$queFile);	
					}
				}
            }
			
			else
			{
				$model->queFile=$queFile;
			}	
			
		    if($model->validate())
			{
				$model->queDifficultyevel = "1";
				$model->queDisplayType = "Normal";
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->queID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Questions model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->queStatus != 'Active') 
        {
        $model->queStatus = 'Active';
        }
        else
        {            
		$model->queStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	
   public function actionExport()
	{
			$model = Questions::find()->all();
            if (!$model) 
			{
                $model = new Questions();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'Zip Codes_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['queID','queFor','queType','queFile','queDifficultyevel','queQuestion','queSolution','queDisplayType','queStatus','queOption1','queOption2','queOption3','queOption4','queCorrectAns'], 
			'headers' => ['queID' => 'ID','queFor' => 'For','queType' => 'Type','queFile' => 'File','queDifficultyevel' =>'Difficulty Level','queQuestion' => 'Question','queSolution' => 'Solution','queDisplayType' => 'Display Type','queStatus' => 'Status','queOption1' => 'Option 1','queOption2' => 'Option 2','Option 3' => 'queOption 3','queOption4' => 'Option4','queCorrectAns' => 'Correct Answer'], 

		]);
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	public function actionQuestionslist($id)
    {
		
        $rows = \backend\models\Questions::find()->where(['queID' => $id,"queStatus"=>"Active"])->all();
        echo "<option value=''>Select Questions</option>";
        if(count($rows)>0){
            foreach($rows as $row){
                echo "<option value='$row->queID'>$row->queQuestion</option>";
            }
        }
         
    }
	
	
}
