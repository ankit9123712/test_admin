<?php

namespace backend\controllers;

use Yii;
use backend\models\Banner;
use backend\models\BannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\UploadedFile;
/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Banner();

        if ($model->load(Yii::$app->request->post()) ) 
		{
			
			$model->bannerFile = UploadedFile::getInstance($model, 'bannerFile');
			if($model->bannerFile != '')
            {
                $Filename = date("YmdHis").'.' . $model->bannerFile->extension;
                $model->bannerFile->saveAs('uploads/banners/'.$Filename);
                $model->bannerFile = $Filename;
            }
			else
			{
				$model->bannerFile="";
			}
			
		
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->bannerID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		$bannerFile = $model->bannerFile;

        if ($model->load(Yii::$app->request->post())) 
		{
			
			$model->bannerFile = UploadedFile::getInstance($model, 'bannerFile');
			
            if($model->bannerFile != '')
            {
                $Filename = date("YmdHis").'.' . $model->bannerFile->extension;
                $model->bannerFile->saveAs('uploads/banners/'.$Filename);
                $model->bannerFile = $Filename;
				
				if(!empty($bannerFile))
				{	
			
					if(file_exists('uploads/banners/'.$bannerFile)) 
					{
					   @unlink('uploads/banners/'.$bannerFile);	
					}
				}
				
            }
			else
			{
				$model->bannerFile = $bannerFile;
				
			}
			
			
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->bannerID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Banner model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->bannerStatus != 'Active') 
        {
        $model->bannerStatus = 'Active';
        }
        else
        {            
		$model->bannerStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	
	public function actionExport()
	{
			$model = Banner::find()->all();
            if (!$model) 
			{
                $model = new Banner();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'Banners_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['bannerID', 'bannerName','bannerScreen','bannerType','bannerURL','bannerStatus','bannerFile','bannerPosition'], 
			'headers' => ['bannerID' => 'ID','bannerName' => 'Name','bannerScreen' => 'Screen','bannerType' => 'Type','bannerURL' => 'URL','bannerStatus' => 'Status','bannerFile' => 'Image','bannerPosition' => 'Position'], 

		]);
    }
	
	
	
	/*------------- Delete image from country folder & update db in blank code start------------------*/
	
	public function actionDeleteBannerImage()
    {
		
		$model = $this->findModel($_REQUEST['bannerID']);
		if(!empty($model))
		{
			if(!empty($_REQUEST['bannerFile']))
			{	
				if(file_exists('uploads/banner/'.$_REQUEST['bannerFile'])) 
				{
				   @unlink('uploads/banner/'.$_REQUEST['bannerFile']);	
				}	
			}
			
			$model->bannerFile = "";
			$model->save(false);
		}
		
		return true;
    }
	
	/*------------- Delete image from country folder & update db in blank code end ------------------*/
	
   

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
