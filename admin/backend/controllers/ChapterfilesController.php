<?php

namespace backend\controllers;

use Yii;
use backend\models\Chapterfiles;
use backend\models\ChapterfilesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * ChapterfilesController implements the CRUD actions for Chapterfiles model.
 */
class ChapterfilesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view','export'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
						 [
                            'actions' => ['export'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Chapterfiles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChapterfilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Chapterfiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Chapterfiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Chapterfiles();

        if ($model->load(Yii::$app->request->post()) ) {
			
			$model->chapterfileFile = UploadedFile::getInstances($model, 'chapterfileFile');		
				$fileNames = array();			
				$fileNames1 = array();			
				$i=0;
				$coursechapeterVideoFileCount = 0;
				$coursechapeterContentFileCount = 0;
				foreach ($model->chapterfileFile as $key => $file) 
				{
					$Filename = $i.date("YmdHis").'.' . $file->extension;
					$file->saveAs('uploads/chapter/'.$Filename);
					
					if($file->extension == "pdf")
					{
						$chapterfileFileType = "Contents";
						$coursechapeterContentFileCount++;
					}
					else
					{
						$coursechapeterVideoFileCount++;
						$chapterfileFileType = "Video";
					}
					
					$fileNames1[$i]['chapterfileFileType'] =  $chapterfileFileType;
					$fileNames1[$i]['chapterfileFile'] =  $Filename;
					$fileNames[$i] =  $fileNames;
					$i++;
				}
			 // echo "<pre>";
			 // print_r($fileNames1);die;	
				$model->chapterfileFile = implode(",",$fileNames);
	      	    $model->save(false);
				$coursechapterID = $model->coursechapterID;
				$chapterfileFile_Array = explode(',',$model->coursechapterFile);
				
				//$Chapterfiles = new Chapterfiles;
				//$Chapterfiles->Addchapterfiles($coursechapterID,$chapterfileFile_Array);
				
	           

			   for($x=0;$x<count($fileNames1);$x++)	
			   {
					$Chapterfiles = new Chapterfiles();
					$Chapterfiles->coursechapterID = $model->coursechapterID;				
					$Chapterfiles->chapterfileFileType = $fileNames1[$x]['chapterfileFileType'];
					$Chapterfiles->chapterfileFile = $fileNames1[$x]['chapterfileFile'];
					$Chapterfiles->chapterfile = $fileNames1[$x]['chapterfileFile'];
					$Chapterfiles->save(false);
				
				}
		
		    if($model->validate())
			{
								$model->save(false);
				return $this->redirect(['view', 'id' => $model->chapterfileID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Chapterfiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
	        if ($model->load(Yii::$app->request->post())) 
		{
			
			
			$model->chapterfileFile = UploadedFile::getInstances($model, 'chapterfileFile');		
				$fileNames = array();			
				$fileNames1 = array();			
				$i=0;
				$coursechapeterVideoFileCount = 0;
				$coursechapeterContentFileCount = 0;
				foreach ($model->chapterfileFile as $key => $file) 
				{
					$Filename = $i.date("YmdHis").'.' . $file->extension;
					$file->saveAs('uploads/chapter/'.$Filename);
					
					if($file->extension == "pdf")
					{
						$chapterfileFileType = "Contents";
						$coursechapeterContentFileCount++;
					}
					else
					{
						$coursechapeterVideoFileCount++;
						$chapterfileFileType = "Video";
					}
					
					$fileNames1[$i]['chapterfileFileType'] =  $chapterfileFileType;
					$fileNames1[$i]['chapterfileFile'] =  $Filename;
					$fileNames[$i] =  $fileNames;
					$i++;
				}
			 // echo "<pre>";
			 // print_r($fileNames1);die;	
				$model->chapterfileFile = implode(",",$fileNames);
	      	    $model->save(false);
				$coursechapterID = $model->coursechapterID;
				$chapterfileFile_Array = explode(',',$model->coursechapterFile);
				
				//$Chapterfiles = new Chapterfiles;
				//$Chapterfiles->Addchapterfiles($coursechapterID,$chapterfileFile_Array);
				
	           

			   for($x=0;$x<count($fileNames1);$x++)	
			   {
					$Chapterfiles = new Chapterfiles();
					$Chapterfiles->coursechapterID = $model->coursechapterID;				
					$Chapterfiles->chapterfileFileType = $fileNames1[$x]['chapterfileFileType'];
					$Chapterfiles->chapterfileFile = $fileNames1[$x]['chapterfileFile'];
					$Chapterfiles->chapterfile = $fileNames1[$x]['chapterfileFile'];
					$Chapterfiles->save(false);
				
				}
		    if($model->validate())
			{
							$model->save(false);
				return $this->redirect(['view', 'id' => $model->chapterfileID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Chapterfiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Chapterfiles model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->chapterfileStatus != 'Active') 
        {
        $model->chapterfileStatus = 'Active';
        }
        else
        {            
		$model->chapterfileStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	public function actionExport()
    {
			
			$model = Chapterfiles::find()->all();
			
            if (!$model) {
                $model = new Chapterfiles();
            }
            /*\moonland\phpexcel\Excel::export(['models' => $model, 'fileName' => 'feedback_'.date("Ymdhis").'.xlsx', 'format' => 'Excel2007', 'columns' => ['feedbackName', 'feedbackEmail', 'feedbackFeedback'], 'headers' => ['feedbackName' => 'Name', 'feedbackEmail' => 'Email', 'feedbackFeedback' => 'Feedback']]); */
        
    }
   

    /**
     * Finds the Chapterfiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chapterfiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chapterfiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
