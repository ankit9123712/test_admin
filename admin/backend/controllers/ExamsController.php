<?php

namespace backend\controllers;

use Yii;
use backend\models\Exams;
use backend\models\ExamsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\UploadedFile;
use backend\models\Questions;
use yii\helpers\Url;
/**
 * ExamsController implements the CRUD actions for Exams model.
 */
class ExamsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Exams models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExamsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Exams model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Exams model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Exams();

        if ($model->load(Yii::$app->request->post()) ) 
		{
		
		    if($model->validate())
			{
				
				$model->moduleID = 0;//implode(",",$model->queIDs);
				
				$model->examDuration = $_REQUEST['Exams']['examDuration'];
				
				$model->save(false);
				return $this->redirect(['import', 'id' => $model->examID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Exams model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) 
		{
		    if($model->validate())
			{
				$model->moduleID = 0;//implode(",",$model->queIDs);
				
				$model->examDuration = $_REQUEST['Exams']['examDuration'];
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->examID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->queIDs = explode(',',$model->queIDs);	
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Exams model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Exams model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->examStatus != 'Active') 
        {
        $model->examStatus = 'Active';
        }
        else
        {            
		$model->examStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	
   

    /**
     * Finds the Exams model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Exams the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Exams::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	public function actionImport($id)
    {
		$model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) ) 
		{
			$model->readingFile = UploadedFile::getInstance($model, 'readingFile');
			if($model->readingFile != '')
            {
                $Filename = "reading".date("YmdHis").'.' . $model->readingFile->extension;
                $model->readingFile->saveAs('uploads/lessions/'.$Filename);
                
				$data = \moonland\phpexcel\Excel::import('uploads/lessions/'.$Filename, ['setFirstRecordAsKeys' => true]);
				
				$errorCnt=0;
				$successCnt=0;
				$errorArray = array();
				$isValid=true;
				foreach ($data as $item) 
				{
					//echo $model->moduleID;die;
					 
					//Reading
					if($item['Question Type']=="Single Text" || $item['Question Type']=="MCQ" || $item['Question Type']=="MMCQ")
					{
						if($item['Question Type']=="Single Text" && $item['Solution']=="")
						{
							$isValid = false;
							$errorArray[$errorCnt]["Question"] = $item['Question'];
							$errorArray[$errorCnt]["Reason"]= "Solution can not be empty when Question Type is Single Text";
							
						}
						if($item['Question Type']=="MCQ" && $item['Correct Answer']=="")
						{
							$isValid = false;
							$errorArray[$errorCnt]["Question"] = $item['Question'];
							$errorArray[$errorCnt]["Reason"]= "Correct Answer can not be empty when Question Type is MCQ";
							
						}
						if($item['Question Type']=="MMCQ" && $item['Correct Answer']=="")
						{
							$isValid = false;
							$errorArray[$errorCnt]["Question"] = $item['Question'];
							$errorArray[$errorCnt]["Reason"]= "Correct Answer can not be empty when Question Type is MMCQ";
							
						}
					}
					else
					{
						$isValid=false;
						$errorArray[$errorCnt]["Question"] = $item['Question'];
						$errorArray[$errorCnt]["Reason"]= "Question Type can be Single Text, MCQ or MMCQ (Case Sensitive)";
					}						 
					
					if($item['Question']=="" || $item['Question Type']=="" || $item['Verification Required']=="")
					{
						$isValid=false;
						$errorArray[$errorCnt]["Question"] = $item['Question'];
						$errorArray[$errorCnt]["Reason"]= "Question or Question Type or Verification Required can not be empty.";
					}					 
					if($isValid)
					{
						if($successCnt==0)
						{
							\Yii::$app
							->db
							->createCommand()
							->delete('questions', ['examID' => $model->examID])
							->execute();
						}
						
						
						$queModel = new Questions;
						$queModel->moduleID   = "8";
						$queModel->examID   = $model->examID;
						$queModel->queFor   = "Exam";
						$queModel->queBankType   = "Premium";
						$queModel->queType   = $item['Question Type'];
						$queModel->queFile   = $item['Question file name'];
						$queModel->queDifficultyevel   = "1";
						$queModel->queQuestion   = $item['Question'];
						$queModel->queSolution   = $item['Solution'];
						$queModel->queDisplayType   = "Normal";
						$queModel->queStatus   = "Active";
						if($item['Question Type']=="MCQ")
						{
							$queModel->queOption1   = (string)$item['Option1'];
							$queModel->queOption2   = (string)$item['Option2'];
							
							if($item['Option1']=="1" && strtolower($item['Option3'])=="not given" )
								$queModel->queOption1 = "True";
							if($item['Option1']==""  && strtolower($item['Option3'])=="not given")
								$queModel->queOption1 = "False";
							if($item['Option2']=="1" && strtolower($item['Option3'])=="not given")
								$queModel->queOption2 = "True";
							if($item['Option2']==""  && strtolower($item['Option3'])=="not given")
								$queModel->queOption2 = "False";
							if($item['Option1']=="1" && strtolower($item['Option3'])=="not give" )
								$queModel->queOption1 = "True";
							if($item['Option1']==""  && strtolower($item['Option3'])=="not give")
								$queModel->queOption1 = "False";
							if($item['Option2']=="1" && strtolower($item['Option3'])=="not give")
								$queModel->queOption2 = "True";
							if($item['Option2']==""  && strtolower($item['Option3'])=="not give")
								$queModel->queOption2 = "False";
							
							$queModel->queOption3   = (string)$item['Option3'];
							$queModel->queOption4   = (string)$item['Option4'];	
						}
						else
						{
							$queModel->queOption1   = (string)$item['Option1'];
							$queModel->queOption2   = (string)$item['Option2'];
							$queModel->queOption3   = (string)$item['Option3'];
							$queModel->queOption4   = (string)$item['Option4'];
						}
						$queModel->queAnswerExplanation   = $item['Answer Explanation'];
						$queModel->queAdditionalInstructions   = $item['Question Additional Instructions'];
						$queModel->queDiagramLink   = $item['Question Diagram Link'];
						$queModel->queCorrectAns   = $item['Correct Answer'];
						$queModel->queVerificationRequred   = $item['Verification Required'];
						$queModel->save(false);
						$successCnt++;
					}
					else
					{
						$errorCnt++;
					}
				}
				$_SESSION["readingerrorCount"]=$errorCnt;
				$_SESSION["readingsuccessCount"]=$successCnt;
				$_SESSION["readingerrorData"]=$errorArray;
			}	
			/*Writing*/
			$model->writingFile = UploadedFile::getInstance($model, 'writingFile');
			if($model->writingFile != '')
            {
                $Filename = "writing".date("YmdHis").'.' . $model->writingFile->extension;
                $model->writingFile->saveAs('uploads/lessions/'.$Filename);
                
				$data = \moonland\phpexcel\Excel::import('uploads/lessions/'.$Filename, ['setFirstRecordAsKeys' => true]);
				
				$errorCnt=0;
				$successCnt=0;
				$errorArray = array();
				$isValid=true;
				foreach ($data as $item) 
				{
					//echo $model->moduleID;die;
					 
						//Writing
						 
					if($item['Question Type']!="Multiline Text")
					{							
						$isValid=false;
						$errorArray[$errorCnt]["Question"] = $item['Question'];
						$errorArray[$errorCnt]["Reason"]= "Question Type can be Multiline Text (Case Sensitive)";
					}						
					 					 
					
					if($item['Question']=="" || $item['Question Type']=="" || $item['Verification Required']=="")
					{
						$isValid=false;
						$errorArray[$errorCnt]["Question"] = $item['Question'];
						$errorArray[$errorCnt]["Reason"]= "Question or Question Type or Verification Required can not be empty.";
					}					 
					if($isValid)
					{
						
						$queModel = new Questions;
						$queModel->moduleID   = "9";
						$queModel->examID   = $model->examID;
						$queModel->queFor   = "Exam";
						$queModel->queBankType   = "Premium";
						$queModel->queType   = $item['Question Type'];
						$queModel->queFile   = $item['Question file name'];
						$queModel->queDifficultyevel   = "1";
						$queModel->queQuestion   = $item['Question'];
						$queModel->queSolution   = $item['Solution'];
						$queModel->queDisplayType   = "Normal";
						$queModel->queStatus   = "Active";
						$queModel->queOption1   = $item['Option1'];
						$queModel->queOption2   = $item['Option2'];
						$queModel->queOption3   = $item['Option3'];
						$queModel->queOption4   = $item['Option4'];
						$queModel->queAnswerExplanation   = $item['Answer Explanation'];
						$queModel->queAdditionalInstructions   = $item['Question Additional Instructions'];
						$queModel->queDiagramLink   = $item['Question Diagram Link'];
						$queModel->queCorrectAns   = $item['Correct Answer'];
						$queModel->queVerificationRequred   = $item['Verification Required'];
						$queModel->save(false);
						$successCnt++;
					}
					else
					{
						$errorCnt++;
					}
				}
				$_SESSION["writingerrorCount"]=$errorCnt;
				$_SESSION["writingsuccessCount"]=$successCnt;
				$_SESSION["writingerrorData"]=$errorArray;
			}
			/*speaking*/
			$model->speakingFile = UploadedFile::getInstance($model, 'speakingFile');
			if($model->speakingFile != '')
            {
                $Filename = "speaking".date("YmdHis").'.' . $model->speakingFile->extension;
                $model->speakingFile->saveAs('uploads/lessions/'.$Filename);
                
				$data = \moonland\phpexcel\Excel::import('uploads/lessions/'.$Filename, ['setFirstRecordAsKeys' => true]);
				
				$errorCnt=0;
				$successCnt=0;
				$errorArray = array();
				$isValid=true;
				foreach ($data as $item) 
				{
					//echo $model->moduleID;die;
					 
						//Writing
						 
					if($item['Question Type']!="Audio")
					{							
						$isValid=false;
						$errorArray[$errorCnt]["Question"] = $item['Question'];
						$errorArray[$errorCnt]["Reason"]= "Question Type can be Audio (Case Sensitive)";
					}
					if($item['Question']=="" || $item['Question Type']=="" || $item['Verification Required']=="")
					{
						$isValid=false;
						$errorArray[$errorCnt]["Question"] = $item['Question'];
						$errorArray[$errorCnt]["Reason"]= "Question or Question Type or Verification Required can not be empty.";
					}					 
					if($isValid)
					{
						
						
						$queModel = new Questions;
						$queModel->moduleID   = "13";
						$queModel->examID   = $model->examID;
						$queModel->queFor   = "Exam";
						$queModel->queBankType   = "Premium";
						$queModel->queType   = $item['Question Type'];
						$queModel->queFile   = $item['Question file name'];
						$queModel->queDifficultyevel   = "1";
						$queModel->queQuestion   = $item['Question'];
						$queModel->queSolution   = $item['Solution'];
						$queModel->queDisplayType   = "Normal";
						$queModel->queStatus   = "Active";
						$queModel->queOption1   = $item['Option1'];
						$queModel->queOption2   = $item['Option2'];
						$queModel->queOption3   = $item['Option3'];
						$queModel->queOption4   = $item['Option4'];
						$queModel->queAnswerExplanation   = $item['Answer Explanation'];
						$queModel->queAdditionalInstructions   = $item['Question Additional Instructions'];
						$queModel->queDiagramLink   = $item['Question Diagram Link'];
						$queModel->queCorrectAns   = $item['Correct Answer'];
						$queModel->queVerificationRequred   = $item['Verification Required'];
						$queModel->save(false);
						$successCnt++;
					}
					else
					{
						$errorCnt++;
					}
				}
				$_SESSION["speakingerrorCount"]=$errorCnt;
				$_SESSION["speakingsuccessCount"]=$successCnt;
				$_SESSION["speakingerrorData"]=$errorArray;
			}/*listening*/
			$model->listeningFile = UploadedFile::getInstance($model, 'listeningFile');
			if($model->listeningFile != '')
            {
                $Filename = "listen".date("YmdHis").'.' . $model->listeningFile->extension;
                $model->listeningFile->saveAs('uploads/lessions/'.$Filename);
                
				$data = \moonland\phpexcel\Excel::import('uploads/lessions/'.$Filename, ['setFirstRecordAsKeys' => true]);
				
				$errorCnt=0;
				$successCnt=0;
				$errorArray = array();
				$isValid=true;
				foreach ($data as $item) 
				{
					//echo $model->moduleID;die;
					 
						//Writing
						 
					if($item['Question Type']=="Single Text" || $item['Question Type']=="MCQ" || $item['Question Type']=="MMCQ" || $item['Question Type']!="Multiline Text")
						{
							if($item['Question Type']=="Single Text" && $item['Solution']=="")
							{
								$isValid = false;
								$errorArray[$errorCnt]["Question"] = $item['Question'];
								$errorArray[$errorCnt]["Reason"]= "Solution can not be empty when Question Type is Single Text";
								
							}
							if($item['Question Type']=="MCQ" && $item['Correct Answer']=="")
							{
								$isValid = false;
								$errorArray[$errorCnt]["Question"] = $item['Question'];
								$errorArray[$errorCnt]["Reason"]= "Correct Answer can not be empty when Question Type is MCQ";
								
							}
							if($item['Question Type']=="MMCQ" && $item['Correct Answer']=="")
							{
								$isValid = false;
								$errorArray[$errorCnt]["Question"] = $item['Question'];
								$errorArray[$errorCnt]["Reason"]= "Correct Answer can not be empty when Question Type is MMCQ";
								
							}
						}
						else
						{
							$isValid=false;
							$errorArray[$errorCnt]["Question"] = $item['Question'];
							$errorArray[$errorCnt]["Reason"]= "Question Type can be Single Text, MCQ or MMCQ (Case Sensitive)";
						}	
					if($item['Question']=="" || $item['Question Type']=="" || $item['Verification Required']=="")
					{
						$isValid=false;
						$errorArray[$errorCnt]["Question"] = $item['Question'];
						$errorArray[$errorCnt]["Reason"]= "Question or Question Type or Verification Required can not be empty.";
					}					 
					if($isValid)
					{
						
						
						
						$queModel = new Questions;
						$queModel->moduleID   = "11";
						$queModel->examID   = $model->examID;
						$queModel->queFor   = "Exam";
						$queModel->queBankType   = "Premium";
						$queModel->queType   = $item['Question Type'];
						$queModel->queFile   = $item['Question file name'];
						$queModel->queDifficultyevel   = "1";
						$queModel->queQuestion   = $item['Question'];
						$queModel->queSolution   = $item['Solution'];
						$queModel->queDisplayType   = "Normal";
						$queModel->queStatus   = "Active";
						$queModel->queOption1   = $item['Option1'];
						$queModel->queOption2   = $item['Option2'];
						$queModel->queOption3   = $item['Option3'];
						$queModel->queOption4   = $item['Option4'];
						$queModel->queAnswerExplanation   = $item['Answer Explanation'];
						$queModel->queAdditionalInstructions   = $item['Question Additional Instructions'];
						$queModel->queDiagramLink   = $item['Question Diagram Link'];
						$queModel->queCorrectAns   = $item['Correct Answer'];
						$queModel->queVerificationRequred   = $item['Verification Required'];
						$queModel->save(false);
						$successCnt++;
					}
					else
					{
						$errorCnt++;
					}
				}
				$_SESSION["listeningerrorCount"]=$errorCnt;
				$_SESSION["listeningsuccessCount"]=$successCnt;
				$_SESSION["listeningerrorData"]=$errorArray;	
				
				
				return Yii::$app->response->redirect(Url::to(['questions/index', 'eid' => $model->examID]));

				
            }
		}
		else
		{
			return $this->render('importlessionquestions', [
				'model' => $this->findModel($id),
			]);
		}
    }
}
