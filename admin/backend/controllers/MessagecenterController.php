<?php

namespace backend\controllers;

use Yii;
use backend\models\Messagecenter;
use backend\models\MessagecenterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * MessagecenterController implements the CRUD actions for Messagecenter model.
 */
class MessagecenterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Messagecenter models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MessagecenterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Messagecenter model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Messagecenter model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Messagecenter();

        if ($model->load(Yii::$app->request->post()) ) 
		{
			
		
		    if($model->validate())
			{
				$model->messagecenterStartDate = date('Y-m-d',strtotime($_REQUEST["messagecenterStartDate"]));				
				$model->messagecenterEndDate = date('Y-m-d',strtotime($_REQUEST["messagecenterEndDate"])); 
				
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->messagecenterID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->messagecenterStartDate = date('Y-m-d');				
			$model->messagecenterEndDate = date('Y-m-d',strtotime(date("Y-m-d")."+1 days")); 
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Messagecenter model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) 
		{
		    if($model->validate())
			{
				$model->messagecenterStartDate = date('Y-m-d',strtotime($_REQUEST["messagecenterStartDate"]));				
				$model->messagecenterEndDate = date('Y-m-d',strtotime($_REQUEST["messagecenterEndDate"]));
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->messagecenterID]);
			}
			else
			{
				$model->messagecenterStartDate = date('Y-m-d',strtotime($model->messagecenterStartDate));				
				$model->messagecenterEndDate = date('Y-m-d',strtotime($model->messagecenterEndDate));
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->messagecenterStartDate = date('Y-m-d',strtotime($model->messagecenterStartDate));				
				$model->messagecenterEndDate = date('Y-m-d',strtotime($model->messagecenterEndDate));
			
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Messagecenter model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Messagecenter model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->messagecenterStatus != 'Active') 
        {
        $model->messagecenterStatus = 'Active';
        }
        else
        {            
		$model->messagecenterStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	public function actionExport()
	{
			$model = Messagecenter::find()->all();
            if (!$model) 
			{
                $model = new Messagecenter();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'Message Centers_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['messagecenterID', 'messagecenterType','messagecenterTitle','messagecenter','messagecenterTo','messagecenterStartDate','messagecenterEndDate','messagecenterStatus'], 
			'headers' => ['messagecenterID' => 'ID','messagecenterType' => 'Type','messagecenterTitle' => 'Title','messagecenter' => 'Message Center','messagecenterTo' => 'To','messagecenterStartDate' => 'Start Date','	messagecenterEndDate' => 'End Date', 'messagecenterStatus' => 'Status'], 

		]);
    }
   

    /**
     * Finds the Messagecenter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Messagecenter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Messagecenter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
