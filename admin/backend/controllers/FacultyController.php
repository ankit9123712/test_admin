<?php

namespace backend\controllers;

use Yii;
use backend\models\Faculty;
use backend\models\FacultySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * FacultyController implements the CRUD actions for Faculty model.
 */
class FacultyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Faculty models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FacultySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faculty model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Faculty model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Faculty();
		
        if ($model->load(Yii::$app->request->post()) ) 
		{
			//print_r($_REQUEST);die;
			$model->facultyDOB = date('Y-m-d',strtotime($_REQUEST["facultyDOB"]));
			$model->facultyJoinDate = date('Y-m-d',strtotime($_REQUEST["facultyJoinDate"]));
			$model->facultyBirthDate = date('Y-m-d',strtotime($_REQUEST["facultyDOB"]));
			
			
			$model->facultyProfilePicture = UploadedFile::getInstance($model, 'facultyProfilePicture');
			if($model->facultyProfilePicture != '')
            {
                $Filename = date("YmdHis").'.' . $model->facultyProfilePicture->extension;
                $model->facultyProfilePicture->saveAs('uploads/faculty/'.$Filename);
                $model->facultyProfilePicture = $Filename;
            }
			else
			{
				$model->facultyProfilePicture="";
			}
		
		    if($model->validate())
			{
				$model->facultyPassword = Yii::$app->security->generatePasswordHash($model->facultyPassword);
				//$model->facultyDOB = date('Y-m-d',strtotime($_REQUEST["facultyDOB"]));//$_REQUEST['Seller']['facultyDOB'];
				//$model->facultyJoinDate =  date('Y-m-d',strtotime($_REQUEST["facultyJoinDate"]));//$_REQUEST['Seller']['facultyJoinDate'];
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->facultyID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Faculty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		$facultyProfilePicture = $model->facultyProfilePicture;

        if ($model->load(Yii::$app->request->post())) 
		{	
			//print_r($_REQUEST);die;
			$model->facultyDOB = date('Y-m-d',strtotime($_REQUEST["facultyDOB"]));
			$model->facultyJoinDate = date('Y-m-d',strtotime($_REQUEST["facultyJoinDate"]));
			$model->facultyProfilePicture = UploadedFile::getInstance($model, 'facultyProfilePicture');
			
            if($model->facultyProfilePicture != '')
            {
                $Filename = date("YmdHis").'.' . $model->facultyProfilePicture->extension;
                $model->facultyProfilePicture->saveAs('uploads/faculty/'.$Filename);
                $model->facultyProfilePicture = $Filename;
				
				if(!empty($facultyProfilePicture))
				{	
			
					if(file_exists('uploads/faculty/'.$facultyProfilePicture)) 
					{
					   @unlink('uploads/faculty/'.$facultyProfilePicture);	
					}
				}
				
            }
			else
			{
				$model->facultyProfilePicture = $facultyProfilePicture;
				
			}
			
			
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->facultyID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Faculty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Faculty model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->facultyStatus != 'Active') 
        {
        $model->facultyStatus = 'Active';
        }
        else
        {            
		$model->facultyStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	/*------------- Delete image from Faculty folder & update db in blank code start------------------*/
	
	public function actionDeleteFacultyImage()
    {
		
		$model = $this->findModel($_REQUEST['facultyID']);
		if(!empty($model))
		{
			if(!empty($_REQUEST['facultyProfilePicture']))
			{	
				if(file_exists('uploads/faculty/'.$_REQUEST['facultyProfilePicture'])) 
				{
				   @unlink('uploads/faculty/'.$_REQUEST['facultyProfilePicture']);	
				}	
			}
			
			$model->facultyProfilePicture = "";
			$model->save(false);
		}
		
		return true;
    }
	
	/*------------- Delete image from Faculty folder & update db in blank code end ------------------*/
   
   
   public function actionExport()
	{
			$model = Faculty::find()->all();
            if (!$model) 
			{
                $model = new Faculty();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'Faculties_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['facultyID','facultyFullName','facultyMobile','facultyEmail','facultyGender','coursemodule.moduleName','facultyProfilePicture','facultyAddress','facultyPincode','country.countryName','state.stateName', 'city.cityName','facultyStatus'], 
			'headers' => ['facultyID' => 'ID','facultyFullName' => 'Full Name','facultyMobile' => 'Mobile','facultyEmail' => 'Email','facultyGender' => 'Gender','coursemodule.moduleName' => 'Module','facultyProfilePicture' => 'Profile Picture','facultyAddress' => 'Address','facultyPincode' => 'Pincode','country.countryName' => 'Country','state.stateName' => 'State', 'city.cityName' => 'City','facultyStatus' => 'Status'], 

		]);
    }
   
   

    /**
     * Finds the Faculty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faculty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faculty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
