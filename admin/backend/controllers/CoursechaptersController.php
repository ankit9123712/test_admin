<?php

namespace backend\controllers;

use Yii;
use common\models\MultipleModel as Model;
use backend\models\Coursechapters;
use backend\models\Courses;
use backend\models\Chapterfiles;
use backend\models\Subject;
use yii\web\UploadedFile;
use backend\models\CoursechaptersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * CoursechaptersController implements the CRUD actions for Coursechapters model.
 */
class CoursechaptersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view','export'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
						 [
                            'actions' => ['export'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Coursechapters models.
     * @return mixed
     */
    public function actionIndex($courseID="")
    {
        $searchModel = new CoursechaptersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		if(!empty($courseID))
		{
			$dataProvider->query->andWhere(['coursechapters.courseID' => $courseID]);
		}
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Coursechapters model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Coursechapters model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
		
        $model = new Coursechapters();
		$modelsDetails = [new Chapterfiles];

         if($model->load(Yii::$app->request->post()))
		 {
			$modelsDetails = Model::createMultiple(Chapterfiles::className(), Yii::$app->request->post(''),'coursechapterID'); 			
			
			Model::loadMultiple($modelsDetails, Yii::$app->request->post());
			$model->save(false);
			$coursechapterID = $model->coursechapterID;	
			
			
				if($model->validate())
				{
					$model->coursechapterFile = UploadedFile::getInstance($model, 'coursechapterFile');
					if($model->coursechapterFile != '')
					{
						$Filename = date("YmdHis").'.' . $model->coursechapterFile->extension;
						$model->coursechapterFile->saveAs('uploads/chapter/'.$Filename);
						$model->coursechapterFile = $Filename;
						
					}
					else
					{
						$model->coursechapterFile="";
					}
				     
					$model->save(false);
		            $coursechapterID = $model->coursechapterID;	
					
					$i=0;	
					$p=0;
					$coursechapeterVideoFileCount = 0;
				    $coursechapeterContentFileCount = 0;
					foreach ($modelsDetails as $index => $modelDetails) 
					{
						
						$modelDetails->coursechapterID = $model->coursechapterID;
						$modelDetails->chapterfile = $modelDetails->chapterfile;
						$modelDetails->chapterfileFile = UploadedFile::getInstances($modelDetails, "[{$index}]chapterfileFile");
						
						$fileNames = array();			
						
						foreach ($modelDetails->chapterfileFile as $key => $file) 
						{
							$FilenamePDF = $p.date("YmdHis").'.' . $file->extension;
							$file->saveAs('uploads/chapter/'.$FilenamePDF);
							$fileNames[$p] = $FilenamePDF;
							if($file->extension == "mp4"){
								$modelDetails->chapterfileFileType = "Video";
                                $coursechapeterVideoFileCount++;
							}else{
	                            $coursechapeterContentFileCount++;
								$modelDetails->chapterfileFileType = "Contents";
							}
							$modelDetails->chapterfileFile = $fileNames[$p];
							$p++;
						}
						$modelDetails->chapterfileFile = implode(',',$fileNames);
						$modelDetails->save(false); 
						$i++;
					}
				
				return $this->redirect(['view', 'id' => $model->coursechapterID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				'modelsDetails' => (empty($modelsDetails)) ? [new Chapterfiles] : $modelsDetails
              
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
				 'modelsDetails' => (empty($modelsDetails)) ? [new Chapterfiles] : $modelsDetails
			]);
		}
    }

    /**
     * Updates an existing Coursechapters model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
	public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$modelsDetails = $model->chapterfiles;
        $oldImage = $model->chapterfileFile	;
		 $coursechapterFile = $model->coursechapterFile;
        if($model->load(Yii::$app->request->post())) 
		{
		
            $oldIDs = ArrayHelper::map($modelsDetails, 'chapterfileID', 'chapterfileID');
			$modelsDetails = Model::createMultiple(Chapterfiles::className(), $modelsDetails,'chapterfileID');

			Model::loadMultiple($modelsDetails, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsDetails, 'chapterfileID', 'chapterfileID')));
			
            $valid = $model->validate();
            $valid = 1;
            if ($valid) 
			{
				
                $transaction = \Yii::$app->db->beginTransaction();
                try 
				{
					
				
				
					        $model->coursechapterFile = UploadedFile::getInstance($model, 'coursechapterFile');
                            if($model->coursechapterFile != '')
							{
								$Filename = date("YmdHis").'.' . $model->coursechapterFile->extension;
								$model->coursechapterFile->saveAs('uploads/chapter/'.$Filename);
								$model->coursechapterFile = $Filename;
								
							}
							else
							{
								$model->coursechapterFile= $coursechapterFile;
							}
							 
							$model->save(false);
							$coursechapterID = $model->coursechapterID;                             
                            $i=0;	
					        $p=0;
					
								

							
						            if ($flag = $model->save(false)) 
									{
										if (!empty($deletedIDs)) 
										{                             
											Chapterfiles::deleteAll(['chapterfileID' => $deletedIDs]);                              
										}
										
										foreach ($modelsDetails as $index => $modelDetails) 
											{
												
												$modelDetails->coursechapterID = $model->coursechapterID;
												$modelDetails->chapterfile = $modelDetails->chapterfile;
												$modelDetails->chapterfileFile = UploadedFile::getInstances($modelDetails, "[{$index}]chapterfileFile");
												
												$fileNames = array();			
												
												foreach ($modelDetails->chapterfileFile as $key => $file) 
												{
													$FilenamePDF = $p.date("YmdHis").'.' . $file->extension;
													$file->saveAs('uploads/chapter/'.$FilenamePDF);
													$fileNames[$p] = $FilenamePDF;
													if($file->extension == "mp4"){
														$modelDetails->chapterfileFileType = "Video";
													}else{
														$modelDetails->chapterfileFileType = "Contents";
													}
													$modelDetails->chapterfileFile = $chapterfileFile[$p];
													$p++;
												}
												$modelDetails->chapterfileFile = implode(',',$fileNames);
												$modelDetails->save(false); 
												$i++;
											}
										
									}
									if ($flag) 
									{
										$transaction->commit();
										
											return $this->redirect(['view', 'id' => $modelDetails->coursechapterID]);
										
									}
                } 
				catch (Exception $e) 
				{
                    $transaction->rollBack();
                }			
			
			}
				return $this->redirect(['view', 'id' => $model->coursechapterID]);
        }
		else
		{
			return $this->render('update', [
				'model' => $model,
				'modelsDetails' => (empty($modelsDetails)) ? [new Chapterfiles] : $modelsDetails
			]);
		}
		
   }
    /**
     * Deletes an existing Coursechapters model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Coursechapters model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->coursechapterStatus != 'Active') 
        {
        $model->coursechapterStatus = 'Active';
        }
        else
        {            
		$model->coursechapterStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	public function actionExport()
    {
			
			$model = Coursechapters::find()->all();
			
            if (!$model) {
                $model = new Coursechapters();
            }
            /*\moonland\phpexcel\Excel::export(['models' => $model, 'fileName' => 'feedback_'.date("Ymdhis").'.xlsx', 'format' => 'Excel2007', 'columns' => ['feedbackName', 'feedbackEmail', 'feedbackFeedback'], 'headers' => ['feedbackName' => 'Name', 'feedbackEmail' => 'Email', 'feedbackFeedback' => 'Feedback']]); */
        
    }
   

    /**
     * Finds the Coursechapters model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Coursechapters the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Coursechapters::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
