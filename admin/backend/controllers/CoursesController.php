<?php

namespace backend\controllers;

use Yii;
use backend\models\Courses;
use backend\models\CoursesSearch;
use app\models\Stream;
use app\models\Coursestd;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * CoursesController implements the CRUD actions for Courses model.
 */
class CoursesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view','export'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
						 [
                            'actions' => ['export'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Courses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CoursesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Courses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Courses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Courses();

        if ($model->load(Yii::$app->request->post()) ) {
		
		$model->courseImage = UploadedFile::getInstance($model, 'courseImage');
			if($model->courseImage != '')
            {
                $Filename = date("YmdHis").'.' . $model->courseImage->extension;
                $model->courseImage->saveAs('uploads/course/'.$Filename);
                $model->courseImage = $Filename;
            }
			else
			{
				$model->courseImage="";
			}	
			
			
		
		    if($model->validate())
			{
								$model->save(false);
				return $this->redirect(['view', 'id' => $model->courseID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Courses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
	 public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $courseImage = $model->courseImage;
        if ($model->load(Yii::$app->request->post())) 
		{
			
			  $model->courseImage = UploadedFile::getInstance($model, 'courseImage');
			if($model->courseImage != '')
            {
                $Filename = date("YmdHis").'.' . $model->courseImage->extension;
                $model->courseImage->saveAs('uploads/course/'.$Filename);
                $model->courseImage = $Filename;
				if(!empty($courseImage))
				{

					if(file_exists('uploads/course/'.$courseImage)) 
					{
					   @unlink('uploads/course/'.$courseImage);	
					}
			
					//unlink('uploads/colors/'.$courseImage);
				}
            }
			
			else
			{
				$model->courseImage=$courseImage;
			}	
			
		    
			$model->save(false);
				return $this->redirect(['view', 'id' => $model->courseID]);
        }
		
		else
		{
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Courses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Courses model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->courseStatus != 'Active') 
        {
        $model->courseStatus = 'Active';
        }
        else
        {            
		$model->courseStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	public function actionExport()
    {
			
			$model = Courses::find()->all();
			
            if (!$model) {
                $model = new Courses();
            }
            /*\moonland\phpexcel\Excel::export(['models' => $model, 'fileName' => 'feedback_'.date("Ymdhis").'.xlsx', 'format' => 'Excel2007', 'columns' => ['feedbackName', 'feedbackEmail', 'feedbackFeedback'], 'headers' => ['feedbackName' => 'Name', 'feedbackEmail' => 'Email', 'feedbackFeedback' => 'Feedback']]); */
        
    }
   

    /**
     * Finds the Courses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Courses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Courses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
