<?php

namespace backend\controllers;

use Yii;
use backend\models\Userexams;
use backend\models\UserexamsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * UserexamsController implements the CRUD actions for Userexams model.
 */
class UserexamsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Userexams models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserexamsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Userexams model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Userexams model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Userexams();

        if ($model->load(Yii::$app->request->post()) )
		{
			
		
		    if($model->validate())
			{
				$model->userexamDate = date('Y-m-d',strtotime($_REQUEST["userexamDate"]));
				$model->userexamVerifiedDate = date('Y-m-d',strtotime($_REQUEST["userexamVerifiedDate"]));
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->userexamID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->userexamDate = date('Y-m-d');
			$model->userexamVerifiedDate = date('Y-m-d');
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Userexams model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) 
		{
		    if($model->validate())
			{
				$model->userexamDate = date('Y-m-d',strtotime($_REQUEST["userexamDate"]));
				$model->userexamVerifiedDate = date('Y-m-d',strtotime($_REQUEST["userexamVerifiedDate"]));
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->userexamID]);
			}
			else
			{
				$model->userexamDate = date('Y-m-d',strtotime($model->userexamDate));
				$model->userexamVerifiedDate = date('Y-m-d',strtotime($model->userexamVerifiedDate));
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
			$model->userexamDate = date('Y-m-d',strtotime($model->userexamDate));
			$model->userexamVerifiedDate = date('Y-m-d',strtotime($model->userexamVerifiedDate));
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Userexams model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		
	public function actionExport()
	{
			$model = Userexams::find()->all();
            if (!$model) 
			{
                $model = new Userexams();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'User Exams_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['userexamID', 'exams.examName','userexamDate','userexamGrade','userexamVerifiedBy','userexamVerifiedDate','userexamVerifiedNotes','users.userFullName'], 
			'headers' => ['userexamID' => 'ID','exams.examName' => 'Exam','userexamDate' => 'Exam Date','userexamGrade' => 'Grade','userexamVerifiedBy' => 'Verified By','userexamVerifiedDate' => 'Verified Date','userexamVerifiedNotes' => 'Verified Notes','users.userFullName' => 'Users'], 

		]);
    }
   

    /**
     * Finds the Userexams model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Userexams the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Userexams::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
