<?php

namespace backend\controllers;

use Yii;
use backend\models\Practicebank;
use backend\models\PracticebankSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\UploadedFile;
/**
 * PracticebankController implements the CRUD actions for Practicebank model.
 */
class PracticebankController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Practicebank models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PracticebankSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Practicebank model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Practicebank model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Practicebank();

        if ($model->load(Yii::$app->request->post()) ) 
		{
			$model->PracticeBankQuestionFile = UploadedFile::getInstance($model, 'PracticeBankQuestionFile');
			if($model->PracticeBankQuestionFile != '')
            {
                $Filename = date("YmdHis").mt_rand(100000, 999999).'.' . $model->PracticeBankQuestionFile->extension;
                $model->PracticeBankQuestionFile->saveAs('uploads/practicebank/'.$Filename);
                $model->PracticeBankQuestionFile = $Filename;
            }
			else
			{
				$model->PracticeBankQuestionFile="";
			}
			
			$model->PracticeBankAnswerFile = UploadedFile::getInstance($model, 'PracticeBankAnswerFile');
			if($model->PracticeBankAnswerFile != '')
            {
                $Filename = date("YmdHis").mt_rand(100000, 999999).'.' . $model->PracticeBankAnswerFile->extension;
                $model->PracticeBankAnswerFile->saveAs('uploads/practicebank/'.$Filename);
                $model->PracticeBankAnswerFile = $Filename;
            }
			else
			{
				$model->PracticeBankAnswerFile="";
			}
		
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->PracticeBankID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Practicebank model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		$PracticeBankQuestionFile = $model->PracticeBankQuestionFile;
		
		$PracticeBankAnswerFile = $model->PracticeBankAnswerFile;

        if ($model->load(Yii::$app->request->post())) 
		{
			
			$model->PracticeBankQuestionFile = UploadedFile::getInstance($model, 'PracticeBankQuestionFile');
			
            if($model->PracticeBankQuestionFile != '')
            {
                $Filename = date("YmdHis").mt_rand(100000, 999999).'.' . $model->PracticeBankQuestionFile->extension;
                $model->PracticeBankQuestionFile->saveAs('uploads/practicebank/'.$Filename);
                $model->PracticeBankQuestionFile = $Filename;
				
				if(!empty($PracticeBankQuestionFile))
				{	
			
					if(file_exists('uploads/practicebank/'.$PracticeBankQuestionFile)) 
					{
					   @unlink('uploads/practicebank/'.$PracticeBankQuestionFile);	
					}
				}
				
            }
			else
			{
				$model->PracticeBankQuestionFile = $PracticeBankQuestionFile;
				
			}
			
			
			
			$model->PracticeBankAnswerFile = UploadedFile::getInstance($model, 'PracticeBankAnswerFile');
			
            if($model->PracticeBankAnswerFile != '')
            {
                $Filename = date("YmdHis").mt_rand(100000, 999999).'.' . $model->PracticeBankAnswerFile->extension;
                $model->PracticeBankAnswerFile->saveAs('uploads/practicebank/'.$Filename);
                $model->PracticeBankAnswerFile = $Filename;
				
				if(!empty($PracticeBankAnswerFile))
				{	
			
					if(file_exists('uploads/practicebank/'.$PracticeBankAnswerFile)) 
					{
					   @unlink('uploads/practicebank/'.$PracticeBankAnswerFile);	
					}
				}
				
            }
			else
			{
				$model->PracticeBankAnswerFile = $PracticeBankAnswerFile;
				
			}
			
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->PracticeBankID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }

    /**
     * Deletes an existing Practicebank model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Practicebank model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->PracticeBankStatus != 'Active') 
        {
        $model->PracticeBankStatus = 'Active';
        }
        else
        {            
		$model->PracticeBankStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	/*------------- Delete image from practicebank folder & update db in blank code start------------------*/
	
	public function actionDeletePracticebankquestionImage()
    {
		
		$model = $this->findModel($_REQUEST['PracticeBankID']);
		if(!empty($model))
		{
			if(!empty($_REQUEST['PracticeBankQuestionFile']))
			{	
				if(file_exists('uploads/practicebank/'.$_REQUEST['PracticeBankQuestionFile'])) 
				{
				   @unlink('uploads/practicebank/'.$_REQUEST['PracticeBankQuestionFile']);	
				}	
			}
			
			$model->PracticeBankQuestionFile = "";
			$model->save(false);
		}
		
		return true;
    }
	
	/*------------- Delete image from practicebank folder & update db in blank code end ------------------*/
	
	
	/*------------- Delete image from practicebank folder & update db in blank code start------------------*/
	
	public function actionDeletePracticebankanswerImage()
    {
		
		$model = $this->findModel($_REQUEST['PracticeBankID']);
		if(!empty($model))
		{
			if(!empty($_REQUEST['PracticeBankAnswerFile']))
			{	
				if(file_exists('uploads/practicebank/'.$_REQUEST['PracticeBankAnswerFile'])) 
				{
				   @unlink('uploads/practicebank/'.$_REQUEST['PracticeBankAnswerFile']);	
				}	
			}
			
			$model->PracticeBankAnswerFile = "";
			$model->save(false);
		}
		
		return true;
    }
	
	/*------------- Delete image from practicebank folder & update db in blank code end ------------------*/
   
	
	public function actionExport()
	{
			$model = Practicebank::find()->all();
            if (!$model) 
			{
                $model = new Practicebank();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'Practice Banks_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['PracticeBankID', 'coursemodule.moduleName','PracticeBankNo','PracticeBankName','PracticeBankDescription','PracticeBankType','PracticeBankQuestionFile','PracticeBankAnswerFile','PracticeBankStatus'], 
			'headers' => ['PracticeBankID' => 'ID','coursemodule.moduleName' => 'Module','PracticeBankNo' => 'Bank No','PracticeBankName' => 'Bank Name','PracticeBankDescription' => 'Description','PracticeBankType' => 'Bank Type','PracticeBankQuestionFile' => 'Question File','PracticeBankAnswerFile' => 'Answer File','PracticeBankStatus' => 'Status'], 

		]);
    }
	
	
	
    /**
     * Finds the Practicebank model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Practicebank the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Practicebank::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
