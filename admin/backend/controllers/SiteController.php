<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\helpers\Url;
use backend\models\Product;
use backend\models\ProductSearch;
use yii\data\Pagination;
use backend\models\PasswordResetRequestForm;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','logout','request-password-reset','captcha'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','index2','logout','request-password-reset','getmonthlyfreepaidusers'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
			'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',                
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
      public function actionIndex()
		{
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
		Yii:$app->session['customerparams']['per-page'] = "10";
		}
		//$searchModel = new ProductSearch();
		//$productData = $searchModel->search(Yii::$app->request->queryParams);
		//$productData->pagination->pageSize=5;
		
		//$inventoryData = $searchModel->search(Yii::$app->request->queryParams);
		//$inventoryData->pagination->pageSize=5;




		return $this->render('index', [
		//'productData' => $productData,
		//'inventoryData' => $inventoryData,
		]);

		// return $this->render('index');
		}
		public function actionIndex2()
		{
		if(!isset(Yii::$app->session['customerparams']['per-page']))
		{
		Yii:$app->session['customerparams']['per-page'] = "10";
		}
		//$searchModel = new ProductSearch();
		//$productData = $searchModel->search(Yii::$app->request->queryParams);
		//$productData->pagination->pageSize=5;
		
		//$inventoryData = $searchModel->search(Yii::$app->request->queryParams);
		//$inventoryData->pagination->pageSize=5;


		 return $this->render('index2');

		return $this->render('index2', [
		//'productData' => $productData,
		//'inventoryData' => $inventoryData,
		]);

		
		}
    /**
     * Login action.
     *
     * @return string
     */


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
			return Yii::$app->response->redirect(Url::to(['site/index']));
        } else {
            $model->Admin_Password = '';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	   public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->sendResetLink();


            // if ($model->sendResetLink()) {

            //     Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

            //     // return $this->goHome();
            // } else {
            //     Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            // }
        }

        return $this->render('requestPasswordResetLink', [
            'model' => $model,
        ]);
    }
	public function actionGetmonthlyfreepaidusers()
	{
		
		$sql =  "select count(userID) NoOfusers,userType,
sum(if(`userCreatedDate` between date_format(current_timestamp(),'%Y-%m-01 00:00:00') and date_format(last_day(current_timestamp()),'%Y-%m-%d 23:59:59'),1,0)) AS '01M',
sum(if(`userCreatedDate` between date_format(current_timestamp() - interval 1 month,'%Y-%m-01 00:00:00') and date_format(last_day(current_timestamp() - interval 1 month),'%Y-%m-%d 23:59:59'),1,0)) AS '02M' ,
sum(if(`userCreatedDate` between date_format(current_timestamp() - interval 2 month,'%Y-%m-01 00:00:00') and date_format(last_day(current_timestamp() - interval 2 month),'%Y-%m-%d 23:59:59'),1,0)) AS  '03M',
sum(if(`userCreatedDate` between date_format(current_timestamp() - interval 3 month,'%Y-%m-01 00:00:00') and date_format(last_day(current_timestamp() - interval 3 month),'%Y-%m-%d 23:59:59'),1,0)) '04M' ,
sum(if(`userCreatedDate` between date_format(current_timestamp() - interval 4 month,'%Y-%m-01 00:00:00') and date_format(last_day(current_timestamp() - interval 4 month),'%Y-%m-%d 23:59:59'),1,0)) '05M' ,
sum(if(`userCreatedDate` between date_format(current_timestamp() - interval 5 month,'%Y-%m-01 00:00:00') and date_format(last_day(current_timestamp() - interval 5 month),'%Y-%m-%d 23:59:59'),1,0)) '06M' 
from `users` 
group by `users`.`userType`";
		 

		$connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
		//echo $sql;die; 
		$data = array();
		$SplitStr="";
		
		$lblFree = array();
		$lblPaid = array();
		
		for($i=0;$i<count($result);$i++)
		{
			//print_r($result[$i]);die;
			
			$lblFree[0] = $result[$i]["01M"];
			$lblFree[1] = $result[$i]["02M"];
			$lblFree[2] = $result[$i]["03M"];
			$lblFree[3] = $result[$i]["04M"];
			$lblFree[4] = $result[$i]["05M"];			
			$lblFree[5] = $result[$i]["06M"];
			$i++;
			$lblPaid[0] = $result[$i]["01M"];
			$lblPaid[1] = $result[$i]["02M"];
			$lblPaid[2] = $result[$i]["03M"];
			$lblPaid[3] = $result[$i]["04M"];
			$lblPaid[4] = $result[$i]["05M"];			
			$lblPaid[5] = $result[$i]["06M"];
			break;
		}	
		$dt = date('Y-m-d');
		$lbls= array();
		$lbls[0]= '"'. date("M,y").'"';
		for($i=0;$i<5;$i++)
		{	$j=$i+1;
			$lbls[$j]  = '"'.date("M,y", strtotime ( '-'.$j.' month' ,strtotime($dt) )).'"' ;
		}
		//echo implode(",",$lbls);
		//print_r($lbls)		;die;
		/*
		
		datasets: [
        {
            label: "Blue",
            fillColor: "blue",
            data: [3,7,4]
        },
        {
            label: "Red",
            fillColor: "red",
            data: [4,3,5]
        },
        {
            label: "Green",
            fillColor: "green",
            data: [7,2,6]
        }
    ]
		*/
		
		$data["labels"][0]=implode(",",$lbls);
		$datasets = array();
		$datasets[0]["label"] ="Blue";
		$datasets[0]["fillColor"] ="blue";
		$datasets[0]["data"][0] =implode(",",$lblFree);
		$datasets[1]["label"] ="Green";
		$datasets[1]["fillColor"] ="green";
		$datasets[1]["data"][0] =implode(",",$lblPaid);
		$data["datasets"][0]= $datasets;
		//print_r($data);
		
		//die;
		echo  stripslashes(json_encode($data));
// json_encode($data)  ;
	}
}
