<?php

namespace backend\controllers;

use Yii;
use backend\models\Notification;
use backend\models\NotificationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\Html;
/**
 * NotificationController implements the CRUD actions for Notification model.
 */
class NotificationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notification model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Notification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
        public function actionCreate()
    {
        $model = new Notification();

        if ($model->load(Yii::$app->request->post()) ) {
		
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->notificationID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Notification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) 
		{
		    if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->notificationID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
        }
		else
		{
        return $this->render('update', [
            'model' => $model,
        ]);
		}
    }
	
	
	public function actionSave($id, $userDeviceType,$userType,$stateID,$cityID)
    {
		
        $model = new Notification();
		//echo $id."<BR>".$userDeviceType."<BR>".$userType."<BR>".$stype;
        $uid =  base64_decode($id);
        $dtype =  base64_decode($userDeviceType);
		$utype =  base64_decode($userType);
        $query = new \yii\db\Query();
        $where = " 1=1 ";
		if($dtype!="All")
			$where.= " AND userDeviceType='".$dtype."'";
		if($utype!= "All")
			$where.= " AND userType='".$utype."'";
		if($stateID!="0"  && !empty($stateID))
			$where.= " AND stateID='".base64_decode($stateID)."'";
		if($cityID!= "0" && !empty($cityID))
			$where.= " AND cityID='".base64_decode($cityID)."'";
		//echo $where;
		//die;
		
           $totalUsers = $query->select(['userID','userDeviceType','userType','userDeviceID'])
                    ->from('users')
                    ->WHERE ($where)
                    ->all();  
          
		   $Totaluservote1 = $query->select(['msgTitle','msgDescription','msgDate'])
                    ->from('notimessages')
                    ->WHERE ("notimessages.msgId = '".$uid."'")
                    ->all();
        //print_r($totalUsers);die;
        //echo $Totaluservote[0]['userDeviceType'];
        $count = count($totalUsers);
        //echo $count;die;
        $arra=array();
        for($i=0;$i<$count;$i++)
        {            
             $arra[$i] = array($uid,$totalUsers[$i]['userID'],'SC',$totalUsers[$i]['userDeviceType'],$totalUsers[$i]['userDeviceID'],$Totaluservote1[0]['msgTitle'],strip_tags($Totaluservote1[0]['msgDescription']),'No');          
        }
        
              $connection = \Yii::$app->db;
                        $transaction1 = $connection->beginTransaction();
                        $connection ->createCommand()
                                    ->batchInsert('notification', ['notificationReferenceKey', 'userID','notificationType','userDeviceType', 'userDeviceID', 'notificationTitle','notificationMessageText','notificationStatus'],$arra)
                                    ->execute();
                        $transaction1->commit();
                     return $this->redirect(['notification/index']);    
                        

    }
	
	
    /**
     * Deletes an existing Notification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		/**
     * Update Status of existing Notification model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->notificationReadStatus != 'Active') 
        {
        $model->notificationReadStatus = 'Active';
        }
        else
        {            
		$model->notificationReadStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
		
	public function actionExport()
	{
			$model = Notification::find()->all();
            if (!$model) 
			{
                $model = new Notification();
            }			
			 \moonland\phpexcel\Excel::widget([
			'models' => $model,
			'mode' => 'export',
			'fileName' => 'Notification_'.date("Ymdhis"),
			'format' => 'Xlsx', 
			'asAttachment' => true, 
			'columns' => ['notificationID','notificationReferenceKey', 'users.userFullName','userDeviceType','userDeviceID','notificationType','notificationTitle','notificationMessageText','notificationResponse','notificationSendDate','notificationSendTime','notificationStatus','notificationReadStatus','notificationData','notificationCreatedDate','dealerID'], 
			'headers' => ['notificationID' => 'ID','notificationReferenceKey' => 'ReferenceKey','users.userFullName','userDeviceType' => 'DeviceType','userDeviceID' => 'DeviceID','notificationType' => 'Notification Type','notificationTitle' => 'Title','notificationMessageText' => 'Message Text','notificationResponse' => 'Response','notificationSendDate' => 'Send Date','notificationSendTime' => 'Send Time','notificationStatus' => 'Status','notificationReadStatus' => 'Read Status','notificationData' => 'Notification Data','notificationCreatedDate' => 'CreatedDate','dealerID' => 'Dealer',], 

		]);
    }
   

    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
