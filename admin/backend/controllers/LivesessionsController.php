<?php

namespace backend\controllers;

use Yii;
use backend\models\Livesessions;
use backend\models\LivesessionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use common\components\AccessRule;
use common\models\User;
use yii\filters\AccessControl;
/**
 * LivesessionsController implements the CRUD actions for Livesessions model.
 */
class LivesessionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
             'access' => [
                    'class' => AccessControl::className(),
                    // We will override the default rule config with the new AccessRule class
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'only' => ['create', 'update', 'delete', 'index', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            // Allow users, moderators and admins to create
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => true,
                            // Allow moderators and admins to update
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            // Allow admins to delete
                            'roles' => [
                                User::ROLE_ADMIN,
                            ],
                        ],
                    ],
                ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    /**
     * Lists all Livesessions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LivesessionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		/*Pagination, per page & search param store in memory and ressign when index is reloaded*/
		$params = Yii::$app->request->queryParams;		
		if (count($params) <= 1) 
		{
			$params = Yii::$app->session['customerparams'];
			if(isset(Yii::$app->session['customerparams']['page']))
				$_GET['page'] = Yii::$app->session['customerparams']['page'];
			if(isset(Yii::$app->session['customerparams']['per-page']))
				$_GET['per-page'] = Yii::$app->session['customerparams']['per-page'];	
		} 
		else 
		{
			Yii::$app->session['customerparams'] = $params;
		}
		$dataProvider = $searchModel->search($params);	
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Livesessions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Livesessions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Livesessions();

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->liveID]);
        }
		*/
		if ($model->load(Yii::$app->request->post()) ) {
			
			//$model->fromDate = date('Y-m-d',strtotime($_REQUEST["fromDate"]));		
			$model->liveStartDate = date('Y-m-d',strtotime(@$_REQUEST["liveStartDate"]));
			$model->liveStartTime = date('H:i:s',strtotime($_REQUEST["liveStartTime"]));
			  
			$model->liveEndTime = date('H:i:s',strtotime(@$_REQUEST["liveEndTime"]));
		    if($model->validate())
			{
				$model->save(false);
				
				if($model->sesRecurring=="Yes")
				{
					$startDate=date('Y-m-d',strtotime(@$_REQUEST["liveStartDate"]));
					$endDate=date('Y-m-d',strtotime(@$_REQUEST["sesEndDate"]));
					$dyArr = $model->sesRecurranceDay;
					//print_r($dyArr);
					//echo count($dyArr);
					//die;
					for ($i = strtotime($startDate); $i <= strtotime($endDate); $i = strtotime('+1 day', $i)) 
					{
					  for($j=0;$j<count($dyArr);$j++)
					  {
						if (date('N', $i) == $dyArr[$j]) //Monday == 1
						{
							$rMdl = new Livesessions();
							$rMdl->facultyID = $model->facultyID;
							$rMdl->liveName = $model->liveName;
							$rMdl->liveBatch = $model->liveBatch;
							$rMdl->liveStartDate = date('Y-m-d', $i);;
							$rMdl->liveStartTime = $model->liveStartTime;
							$rMdl->liveEndTime = $model->liveEndTime;
							$rMdl->moduleID = $model->moduleID;
							$rMdl->liveMeetingID = $model->liveMeetingID;
							$rMdl->liveMeetingPassword = $model->liveMeetingPassword;
							$rMdl->liveMeetingURL = "";//$model->liveMeetingURL;
							$rMdl->liveStatus = $model->liveStatus;
							$rMdl->save(false);
							
						}	
					  }						  
						
					  
						/*echo date('l Y-m-d', $i); //prints the date only if it's a Monday
					  if (date('N', $i) == 2) //Monday == 1
						echo date('l Y-m-d', $i); 
					  if (date('N', $i) == 3) //Monday == 1
						echo date('l Y-m-d', $i); 
                      if (date('N', $i) == 4) //Monday == 1
						echo date('l Y-m-d', $i); 
					  if (date('N', $i) == 5) //Monday == 1
						echo date('l Y-m-d', $i); 
					  if (date('N', $i) == 6) //Monday == 1
						echo date('l Y-m-d', $i); 		
					  if (date('N', $i) == 7) //Monday == 1
						echo date('l Y-m-d', $i);*/

					  	

						
					}

					
					
				}
				
				
				
				return $this->redirect(['view', 'id' => $model->liveID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				//print_r($data);die;
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('create', [
                'model' => $model,
				]);
			}
        }
		else
		{
			return $this->render('create', [
            'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Livesessions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ){
			
			$model->liveStartDate = date('Y-m-d',strtotime(@$_REQUEST["liveStartDate"]));
			$model->liveStartTime = date('H:i:s',strtotime($_REQUEST["liveStartTime"]));
			  
			$model->liveEndTime = date('H:i:s',strtotime(@$_REQUEST["liveEndTime"]));
			if($model->validate())
			{
				$model->save(false);
				return $this->redirect(['view', 'id' => $model->liveID]);
			}
			else
			{
				$data = Html::errorSummary($model);
				//print_r($data);die;
				Yii::$app->session->setFlash('message',$data);//
				return $this->render('update', [
                'model' => $model,
				]);
			}
           // return $this->redirect(['view', 'id' => $model->liveID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
	public function actionChangestatus($id)
    {
		$model= $this->findModel($id);
	 if( $model->liveStatus != 'Active') 
        {
        $model->liveStatus = 'Active';
        }
        else
        {            
		$model->liveStatus = 'Inactive';
        }
        $model->save(false);
        return $this->redirect(['index']);
        
    }
    /**
     * Deletes an existing Livesessions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Livesessions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Livesessions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Livesessions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
